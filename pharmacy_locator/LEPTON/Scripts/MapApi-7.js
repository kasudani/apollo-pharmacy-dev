
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        console.log('Not supported');
    }
}

function showPosition(position) {
    console.log(position.coords.latitude); 
    console.log(position.coords.longitude); 
}

console.log('Get Loaction Called');

getLocation();