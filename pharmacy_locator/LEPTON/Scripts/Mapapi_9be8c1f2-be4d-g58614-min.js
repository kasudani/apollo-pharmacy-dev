function success(position){
	var latitude  = position.coords.latitude;	
	var longitude = position.coords.longitude;	
	var accuracy  = position.coords.accuracy;
	document.getElementById("lat").value  = latitude;
	document.getElementById("lng").value  = longitude;
	document.getElementById("acc").value  = accuracy;
}
function LoadMap() {


  //center_pt = new google.maps.LatLng(document.getElementById("lat"), document.getElementById("lng"));
     center_pt = new google.maps.LatLng(17.41460128,78.40882326);


 var e = {
    zoom: 5,
    center: center_pt,
    mapTypeControl: !1,
    streetViewControl: !0,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.SMALL
    },
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, "tehgrayz"]
    }
  };
  map = new google.maps.Map(document.getElementById("MapCanvas"), e);
  var t = [{
    featureType: "all",
    elementType: "all",
    stylers: [{
      saturation: -0
    }]
  }],
      i = new google.maps.StyledMapType(t, {
      name: "Grayscale"
    });
  map.mapTypes.set("tehgrayz", i), map.setMapTypeId("tehgrayz"), google.maps.event.addListener(map, "tilesloaded", function() {
    $(".PoweredInfo").css("display", "block")
  }), google.maps.event.addListener(map, "drag", function() {
    distanceWidget && distanceWidget.set("position", map.getCenter())
  }), google.maps.event.addListener(map, "dragend", function() {
    distanceWidget && getData()
  }), isMapLoadingFirstTime = !0, google.maps.event.addListener(map, "tilesloaded", function() {
    DoAfterLoadingTiles()
  }), geocoder = new google.maps.Geocoder, $("#btnSearch").bind("click", function() {
    SearchByInputText()
  })
}

function DoAfterLoadingTiles() {
	// alert("&&" + gvBSID + "&&");
  if (isMapLoadingFirstTime) {
    gvBSID > 0 && SelectBranch(), gvASID > 0 && SelectATM(), 0 == gvBSID && 0 == gvASID && SelectATM(), SearchPlaces(), SearchDirPlaces();
    var e = $(window).width();
    980 >= e ? (loadNavigatorMobile(), $(".Pageloader").fadeOut("fast")) : e > 980 && (loadNavigator(), $(".Pageloader").fadeOut("fast"))
  }
  isMapLoadingFirstTime = !1
}

function ReLoadMap() {
  isReloading = !0, ResetMap(), isATMSelected = !1, isBranchSelected = !1, drillLevel = "Country", StateName = "", setButtonSelction(isATMSelected, "btnATM", "ATM"), isBranchSelected = !isBranchSelected, 1 == IsServiceUnchecked() && LoadProductAndServices(), $("#btnATM").hasClass("selected") || ($("#btnATM").addClass("selected"), $("#btnATMChk").show()), $("#btnBranch").removeClass("selected"), $("#btnBranchChk").hide(), setTimeout(function() {
    $("#mv").hasClass("OrangeButton") ? $("#mv").siblings().removeClass("OrangeButton") : ($("#mv").addClass("OrangeButton"), $("#mv").siblings().removeClass("OrangeButton")), ChangeBaseLayer("roadmap")
  }, 100), setTimeout(function() {
    $("#ml").hasClass("OrangeButton") ? $("#ml").siblings().removeClass("OrangeButton") : ($("#ml").addClass("OrangeButton"), $("#ml").siblings().removeClass("OrangeButton")), ChangeBaseLayer("roadmap")
  }, 100), isReloading = !1
}

function ResetMap() {
  map.setCenter(center_pt), map.setZoom(5), clearResult(), distanceWidget && (distanceWidget.set("map", null), distanceWidget = null, distnaceInfoWin && (distnaceInfoWin.close(), distnaceInfoWin = null));
  for (var e = 0; e < BMArray.length; e++) BMArray[e].setMap(null);
  BMArray = new Array;
  for (var e = 0; e < AMArray.length; e++) AMArray[e].setMap(null);
  AMArray = new Array, clickedMarker && (clickedMarker.setMap(null), clickedMarker = null), CloseInfWin(), RemoveBranchATMDetail(), clearBranchATM(), removeRoutePath(), BackDir(), $("#searchTextField").val("")
}
function roundNumber(e, t) {
  {
    var i = Math.round(e * Math.pow(10, t)) / Math.pow(10, t) + "";
    i.indexOf(".")
  }
  return i
}
function ChangeBaseLayer(e) {
  switch (e) {
  case "roadmap":
    map.getStreetView() && map.getStreetView().setVisible(!1), map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
    break;
  case "satellite":
    map.getStreetView() && map.getStreetView().setVisible(!1), map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
    break;
  case "hybrid":
    map.getStreetView() && map.getStreetView().setVisible(!1), map.setMapTypeId(google.maps.MapTypeId.HYBRID);
    break;
  case "terrain":
    map.getStreetView() && map.getStreetView().setVisible(!1), map.setMapTypeId(google.maps.MapTypeId.TERRAIN);
    break;
  case "reload":
    ClearDrivingdirection(), $("#directions").css({
      display: "none"
    }), $("#dircontent").css({
      display: "none"
    }), $(".filterContainer").removeClass("open"), $("#btnBranchService").removeClass("ATMBtn"), $("#btnBranchService").addClass("ServiceSelected"), $("#btnATMService").removeClass("ServiceSelected"), $("#btnATMService").addClass("ATMBtn"), ReLoadMap()
  }
}
function ClearDrivingdirection() {
  $("#address").html(""), $("#servicesdrawer1").html(""), $("#destinationH4").html("")
}

function DoInServ(e, t) {
  {
	//  alert("@@" + e + "@@" + t + "@@");
    var i = "";
    $.ajax({
      url: "LEPTON/Handlers/handler.php?Task=" + e,
      type: "post",
      async: !1,
      data: t,
      success: function() { },
      error: function() {}
    }).done(function(e) {
		 //alert("**" + e + "**");
      "" != e && (i = e)
    })
  }
	  //alert("@@" + i + "@@");
  return i
}
function SearchPlaces() {
  $("#searchTextField").val("");

  var e = document.getElementById("searchTextField");

  // alert("EP" + e + "EP");

  autocomplete = new google.maps.places.Autocomplete(e), autocomplete.bindTo("bounds", map), google.maps.event.addListener(autocomplete, "place_changed", function() {
    var e = autocomplete.getPlace();

	//alert("ES" + e + "ES");

    e.geometry && (gLatLng = e.geometry.location, isAuto = !0, Search())
  })
}

function SearchByInputText() {
  var e = $("#searchTextField").val();

 //  alert("EE" + e +"EE");

  "" != e && (Incompletetext = !0, geocoder.geocode({
    address: e
  }, function(e, t) {
    t == google.maps.GeocoderStatus.OK ? (gLatLng = e[0].geometry.location, showSerachResultNew(gLatLng), void(isAuto = !1)) : void 0;

	// alert("$%" + t + "$%" + gLatLng + "$%");
	
	return t;
  }))
}

function Search() {
  if ($("#directions").css({
    display: "none"
  }), isSearchDone && -1 != navigator.appName.indexOf("Microsoft")) return void(isSearchDone = !1);
  if (isAuto) showSerachResult(gLatLng);
  else {
    var e = !1,
        t = autocomplete.getPlace(),
        i = $("#searchTextField").val();
    void 0 != t && t.geometry && "" != i ? gLatLng = t.geometry.location : "" != i && (e = !0, geocoder.geocode({
      address: i
    }, function(e, t) {
      return t == google.maps.GeocoderStatus.OK ? (gLatLng = e[0].geometry.location, showSerachResultNew(gLatLng), void(isAuto = !1)) : void 0
    })), e || showSerachResult(gLatLng)
  }
  isAuto = !1
}


function showSerachResultNew(e) {
  var t = $("#searchTextField").val();
  
  // alert("TT" + t + "TT");

  $("#ulBranchATMDetail").html(""), $("#totRes").html("");
  var i = DoInServ("Search", {
    SearchText: t,
    IsBranch: isBranchSelected,
    IsATM: isATMSelected,
    lat: "0",
    lng: "0"
  });

 //  alert("II" + i + "II");

  if ("" != i) {
    var r = i.split("$");
    if (1 == r.length) return void showSerachResult(e);
    $("#ulBranchATMDetail").html(""), rc = 0;
    for (var n = 0; n < r.length; n++) {
      var a = r[n].split("^"),
          s = a[0],
          o = a[1],
          l = a[2],
          c = a[3];
      SetResultDetail(s, o, l, c)
    }
  } else showSerachResult(e)
}
function SetResultDetail(e, t, i, r) {
  var n = $("#ulBranchATMDetail").html(),
      a = $("#searchTextField").val(),
      s = "SearchR";
  s = rc % 2 == 0 ? "SearchR lblue" : "SearchR";
  var o = n + '<li  class="' + s + '" onclick="ShowWidget(\'' + i + "','" + r + '\');"><a href="#"> ' + a + "-" + e + "," + t + "</a> </li>";
  $("#ulBranchATMDetail").html(o), rc += 1
}
function ShowWidget(e, t) {
  var i = new google.maps.LatLng(e, t, !0);
  i ? (map.setCenter(i), map.setZoom(14), clearResult(), initWidget(map), drillLevel = "City") : ($("#ulBranchATMDetail").html(""), $("#searchTextField").val(""), $("#searchTextField").focus())
}
function showSerachResult(e) {
  if (e) {
    var t = ValidateIndiaLatLng(e.lat(), e.lng());
    "true" == t ? (map.setCenter(e), map.setZoom(14), clearResult(), initWidget(map), drillLevel = "City") : ($("#searchTextField").val(""), $("#searchTextField").focus())
  } else $("#searchTextField").val(""), $("#searchTextField").focus()
}
function ValidateIndiaDirLatLng(e, t, i, r) {
  var n = ValidateIndiaLatLng(e, t),
      a = ValidateIndiaLatLng(i, r),
      s = "false";
  return "true" == n && "true" == a && (s = "true"), s
}
function ValidateIndiaLatLng(e, t) {
  var i = 6,
      r = 68,
      n = 38,
      a = 100,
      s = !1,
      o = !1;
  e >= i && n >= e && (s = !0), t >= r && a >= t && (o = !0);
  var l = "false";
  return s && o && (l = "true"), l
}
function clearResult() {
  if (StateMarkers) for (i = 0; i < StateMarkers.length; i++) StateMarkers[i].setVisible(!1);
  if (CityMarkers) for (i = 0; i < CityMarkers.length; i++) CityMarkers[i].setVisible(!1);
  StateMarkers = new Array, CityMarkers = new Array
}
function loadNavigator() {
  function e(e) {
    var t = e.coords.latitude,
        i = e.coords.longitude;
    navigatorLatLng = new google.maps.LatLng(t, i, !0), geoFindPhysicalLocDefault()
  }
  function t() {}
  navigator.geolocation && navigator.geolocation.getCurrentPosition(e, t)
}
function loadNavigatorMobile() {
  function e(e) {
    var t = e.coords.latitude,
        i = e.coords.longitude;
    navigatorLatLng = new google.maps.LatLng(t, i, !0), geoFindPhysicalLocDefault()
  }
  function t() {}
  navigator.geolocation && navigator.geolocation.getCurrentPosition(e, t)
}
function geoFindPhysicalLoc() {
  navigatorLatLng && (map.setCenter(navigatorLatLng), map.setZoom(14), clearResult(), initWidget(map))
}
function geoFindPhysicalLocDefault() {
  navigatorLatLng && (map.setCenter(navigatorLatLng), map.setZoom(14), clearResult(), initWidget(map), drillLevel = "City")
}
function ToggleDispalyHideTab(e) {
  $("#" + e).toggle()
}
function CustomMarker(e) {
  var t = e || {};
  void 0 == e.visible && (e.visible = !0), void 0 == e.anchor && (e.anchor = CustomMarkerPosition.BOTTOM), this.setValues(t), this.Level = t.level, this.centerll = t.centerll, this.State = t.id, this.BC = t.bc, this.AC = t.ac
}
function ShowAllMarkers() {
  drillLevel = "Country", currentMode = "showall", ResetMap(), LoadStMarkers()
}
function LoadStMarkers() {
  try {
    if (StateMarkers) for (i = 0; i < StateMarkers.length; i++) StateMarkers[i].setVisible(!1);
    StateMarkers = new Array;
    var e = DoInServ("GetStCount", {
      BranchServices: gvSelectedBranchServices,
      ATMServices: gvSelectedATMServices
    });
    if ("" != e) {
      for (var t = e.split("$"), i = 0; i < t.length; i++) {
        var r = t[i].split("^"),
            n = (r[0], r[1]),
            a = r[2],
            s = r[3],
            o = r[4].split(",")[0],
            l = r[4].split(",")[1],
            c = r[5].split(",")[0],
            d = r[5].split(",")[1],
            p = new google.maps.LatLng(c, d, !0),
            h = n;
        "Andhra Pradesh" == n && (h = "Andhra Pradesh/Telangana");
        var g = '<div class="" style="background-color: #666666;position: absolute;z-index: 500; white-space:nowrap;"><div style="padding:0px; 10px 0px 10px;"><div class="content" style="padding-top:10px!important;"><span class="InfoWindowArrow"></span><h3>' + h + '</h3><h4 style="border:0px!important;"> 16 Hrs Pharmacy:' + a + "<br/>24 Hrs Pharmacy:" + s + "<br/><br/>Click marker to view city wise count.</h4> </div> </div></div>",
            v = CreateStateMarker(o, l, g, n, a, s, p, "1");
        StateMarkers.push(v)
      }
      SelectBranchAtmFilterData()
    }
  } catch (u) {}
}
function CreateStateMarker(e, t, i, r, n, a, s, o) {
  var l = new CustomMarker({
    position: new google.maps.LatLng(e, t, !0),
    map: map,
    name: i,
    id: r,
    visible: !0,
    level: o,
    centerll: s,
    bc: n,
    ac: a,
    content: '<div style="color: #ffffff !important; font-weight:bold !important; font-family: Arial,sans-serif !important; font-size: 13px !important; text-align: center; text-decoration: none; text-shadow:0px 1px 0px #333; width:48px !important; height:48px !important; line-height:30px !important; zoom:1; opacity:0.9; background-image:url(LEPTON/Images/cpin.png); background-repeat:no-repeat;" id="rt">' + n + " </div>"
  }),
      c = navigator.appName,
      d = navigator.appVersion,
      p = !1;
  return -1 != c.indexOf("Microsoft") && (0 == d.indexOf("3.") || 0 == d.indexOf("4.")) && (p = !0), google.maps.event.addListener(l, "mouseover", function() {
    if (0 == p) {
      ttInfoWin && ttInfoWin.close();
      var e = {
        content: "<div class='tiptoolhover'><div class='arrowhover'></div>" + l.get("name") + "<br /></div>",
        disableAutoPan: !0,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(23, -30),
        boxStyle: {
          opacity: 1
        },
        closeBoxMargin: "2 2px 2px 2px",
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: !1,
        pane: "floatPane",
        enableEventPropagation: !1
      };
      ttInfoWin = new InfoBox(e), ttInfoWin.open(map, l)
    } else {
      infoWin && infoWin.close();
      var t = "<b>" + l.get("id") + "</b><br/>16 Hrs Pharmacy:" + l.get("bc") + "<br /> 24 hrs Pharmacy:" + l.get("ac") + "<br/>Click marker to view city wise count.";
      "2" == o && (t = "<b>" + l.get("id") + "</b><br/>16 Hrs Pharmacy:" + l.get("bc") + "<br /> 24 Hrs Pharmacy:" + l.get("ac") + "<br/>Click marker to view " + l.get("id") + " branches/ATMs."), infoWin = new google.maps.InfoWindow({
        content: t,
        pixelOffset: new google.maps.Size(1, -42)
      }), infoWin.open(map, l)
    }
  }), google.maps.event.addListener(l, "mouseout", function() {
    ttInfoWin && ttInfoWin.close(), infoWin && infoWin.close()
  }), google.maps.event.addListener(l, "click", MarkerClickHandler), l
}
function MarkerClickHandler() {
  try {
    var e = this.level;
    if ("1" == e) {
      if (StateMarkers) for (i = 0; i < StateMarkers.length; i++) StateMarkers[i].setVisible(!1);
      CityMarkers = new Array;
      var t = this.id;
      drillLevel = "State", StateName = t, LoadCtMarkers(t);
      var r = this.centerll;
      map.setCenter(r), map.setZoom(7)
    } else if ("2" == e) {
      this.setVisible(!1);
      var r = this.position;
      CityName = this.id;
      var n = 1;
      !isBranchSelected && isATMSelected && (n = 0);
      var a = DoInServ("GetCityBranchATMPosition", {
        CityName: CityName,
        IsBranch: n
      });
      if ("" != a) {
        var s = a.split("^");
	//	r = new google.maps.LatLng(22.167058, 82.08641, !0);
       r = new google.maps.LatLng(s[0], s[1], !0)
      }
      if (map.setCenter(r), map.setZoom(14), CityMarkers) for (i = 0; i < CityMarkers.length; i++) CityMarkers[i].setVisible(!1);
      initWidget(map), drillLevel = "City"
    }
  } catch (o) {}
}

function LoadCtMarkers(e) {
	// alert("@#" + e +"#@");
  try {
    if (CityMarkers) for (r = 0; r < CityMarkers.length; r++) CityMarkers[r].setVisible(!1);
    CityMarkers = new Array;
    var t = DoInServ("GetCtCount", {
      StateName: e,
      BranchServices: gvSelectedBranchServices,
      ATMServices: gvSelectedATMServices
    });
    if ("" != t) {
      for (var i = t.split("$"), r = 0; r < i.length; r++) {
        var n = i[r].split("^"),
            a = n[0],
            s = n[1],
            o = n[2],
            l = n[3],
            c = n[4],
            d = new google.maps.LatLng(a, s, !0),
            
			p = '<div class="" style="background-color: #666666;position: absolute;z-index: 500; white-space:nowrap;"><div style="padding:0px; 10px 0px 10px;"><div class="content" style="padding-top:10px!important;"><span class="InfoWindowArrow"></span><h3>' + c + '</h3><h4 style="border:0px!important;"> 24 Hrs Pharmacy:' + o + "<br/>16 Hrs Pharmacy:" + l + "<br/><br/>Click marker to view " + c + " branches/ATMs.</h4></div> </div></div>",
            h = CreateStateMarker(a, s, p, c, o, l, d, "2");
        CityMarkers.push(h)
      }
      SelectBranchAtmFilterData()
    }
  } catch (g) {}
}
function SelectBranch() {
  setButtonSelction(isBranchSelected, "btnBranch", "24 Hrs Pharmacy"), isBranchSelected = !isBranchSelected, SelectBranchAtmFilterData()
}
function SelectATM() {
  setButtonSelction(isATMSelected, "btnATM", "16 Hrs Pharmacy"), isATMSelected = !isATMSelected, SelectBranchAtmFilterData()
}

function SelectBranchAtmFilterData() {
  isBranchSelected && !isATMSelected ? showBCAC("BC") : !isBranchSelected && isATMSelected ? showBCAC("AC") : isBranchSelected || isATMSelected ? isBranchSelected && isATMSelected && showBCAC("ACBC") : showBCAC("NOT")
}
function showBCAC(e) {
	
	// alert("#!" + e + "#!");

  if ("BC" == e) if ("Country" == drillLevel) for (i = 0; i < StateMarkers.length; i++) if (0 != parseInt(StateMarkers[i].BC)) {
    var t = GetMarkerContent(StateMarkers[i].BC);
    StateMarkers[i].setContent(t), StateMarkers[i].setVisible(!0)
  } else StateMarkers[i].setVisible(!1);
  else if ("State" == drillLevel) for (i = 0; i < CityMarkers.length; i++) if (0 != parseInt(CityMarkers[i].BC)) {
    var t = GetMarkerContent(CityMarkers[i].BC);
    CityMarkers[i].setContent(t), CityMarkers[i].setVisible(!0)
  } else CityMarkers[i].setVisible(!1);
  else "City" == drillLevel && getData();
  else if ("AC" == e) if ("Country" == drillLevel) for (i = 0; i < StateMarkers.length; i++) if (0 != parseInt(StateMarkers[i].AC)) {
    var t = GetMarkerContent(StateMarkers[i].AC);
    StateMarkers[i].setContent(t), StateMarkers[i].setVisible(!0)
  } else StateMarkers[i].setVisible(!1);
  else if ("State" == drillLevel) for (i = 0; i < CityMarkers.length; i++) if (0 != parseInt(CityMarkers[i].AC)) {
    var t = GetMarkerContent(CityMarkers[i].AC);
    CityMarkers[i].setContent(t), CityMarkers[i].setVisible(!0)
  } else CityMarkers[i].setVisible(!1);
  else "City" == drillLevel && getData();
  else if ("NOT" == e) if ("Country" == drillLevel) for (i = 0; i < StateMarkers.length; i++) StateMarkers[i].setVisible(!1);
  else if ("State" == drillLevel) for (i = 0; i < CityMarkers.length; i++) CityMarkers[i].setVisible(!1);
  else "City" == drillLevel && getData();
  else if ("ACBC" == e) if ("Country" == drillLevel) for (i = 0; i < StateMarkers.length; i++) if (0 != parseInt(StateMarkers[i].AC) || 0 != parseInt(StateMarkers[i].BC)) {
    var r = parseInt(StateMarkers[i].AC) + parseInt(StateMarkers[i].BC),
        t = GetMarkerContent(r);
    StateMarkers[i].setContent(t), StateMarkers[i].setVisible(!0)
  } else StateMarkers[i].setVisible(!1);
  else if ("State" == drillLevel) for (i = 0; i < CityMarkers.length; i++) if (0 != parseInt(CityMarkers[i].AC) || 0 != parseInt(CityMarkers[i].BC)) {
    var r = parseInt(CityMarkers[i].AC) + parseInt(CityMarkers[i].BC),
        t = GetMarkerContent(r);
    CityMarkers[i].setContent(t), CityMarkers[i].setVisible(!0)
  } else CityMarkers[i].setVisible(!1);
  else "City" == drillLevel && getData()
}

function setButtonSelction(e, t, i) {

	// alert("ETI" + e + "ETI" + t + "ETI" + i + "ETI");

  e ? (e = !1, $("#" + t).attr("title", "Click To Select " + i + " Filter"), $("#" + t).removeClass("selected"), $("#" + t + "Chk").hide()) : (e = !0, $("#" + t).attr("title", "Click To Unselect " + i + " Filter"), $("#" + t).addClass("selected"), $("#" + t + "Chk").show())
}

function GetMarkerContent(e) {
//	 alert("MARK" + e + "MARK");

  var t = '<div style="color: #ffffff !important; font-weight:bold !important; font-family: Arial,sans-serif !important; font-size: 13px !important; text-align: center; text-decoration: none; text-shadow:0px 1px 0px #333; width:48px !important; height:48px !important; line-height:30px !important; zoom:1; opacity:0.9; background-image:url(LEPTON/Images/cpin.png); background-repeat:no-repeat;" id="rt">' + e + " </div>";

  alert(t);

  return t
}

function ReadQS() {
  gvBSID = parseInt(GetQueryParamByName("bsid")) || 0, gvASID = parseInt(GetQueryParamByName("asid")) || 0, 0 == gvBSID && 0 == gvASID && window.location.replace("index.html"), gvBSID > 0 && (gvSelectedBranchServices = gvBSID.toString()), gvASID > 0 && (gvSelectedATMServices = gvASID.toString())
}

function GetQueryParamByName(e) {
  e = e.toLowerCase(), e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var t = new RegExp("[\\?&]" + e + "=([^&#]*)"),
      i = t.exec(location.search);
  return null === i ? "" : decodeURIComponent(i[1].replace(/\+/g, " "))
}
  
function InfoBox(e) {
  e = e || {}, google.maps.OverlayView.apply(this, arguments), this.content_ = e.content || "", this.disableAutoPan_ = e.disableAutoPan || !1, this.maxWidth_ = e.maxWidth || 0, this.pixelOffset_ = e.pixelOffset || new google.maps.Size(0, 0), this.position_ = e.position || new google.maps.LatLng(0, 0), this.zIndex_ = e.zIndex || null, this.boxClass_ = e.boxClass || "infoBox", this.boxStyle_ = e.boxStyle || {}, this.closeBoxMargin_ = e.closeBoxMargin || "2px", this.closeBoxURL_ = e.closeBoxURL || "http://www.google.com/intl/en_us/mapfiles/close.gif", "" === e.closeBoxURL && (this.closeBoxURL_ = ""), this.infoBoxClearance_ = e.infoBoxClearance || new google.maps.Size(1, 1), this.isHidden_ = e.isHidden || !1, this.alignBottom_ = e.alignBottom || !1, this.pane_ = e.pane || "floatPane", this.enableEventPropagation_ = e.enableEventPropagation || !1, this.div_ = null, this.closeListener_ = null, this.eventListener1_ = null, this.eventListener2_ = null, this.eventListener3_ = null, this.moveListener_ = null, this.contextListener_ = null, this.fixedWidthSet_ = null
}
function initWidget(e) {
  distanceWidget && (distanceWidget.set("map", null), distanceWidget = null, removeRoutePath(), distnaceInfoWin && (distnaceInfoWin.close(), distnaceInfoWin = null), CloseInfWin()), distanceWidget = new DistanceWidget(e), infoWin && infoWin.close(), getData()
}
function DistanceWidget(e) {
  this.set("map", e), this.set("position", e.getCenter());
  var t = {
    url: "LEPTON/Images/Center.png",
    size: new google.maps.Size(24, 24),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(12, 12)
  },
      i = new google.maps.Marker({
      draggable: !0,
      icon: t,
      title: "Drag to move new location!",
      raiseOnDrag: !1
    });
  i.bindTo("map", this), i.bindTo("position", this);
  var r = this;
  google.maps.event.addListener(i, "dragend", function() {
    r.fitCenter(), getData()
  });
  var n = new RadiusWidget;
  n.bindTo("map", this), n.bindTo("center", this, "position"), this.bindTo("distance", n), this.bindTo("bounds", n)
}
function RadiusWidget() {
  var e = {
    fillOpacity: .2,
    fillColor: "#A7A7A7",
    strokeColor: "#000000",
    strokeWeight: 1,
    strokeOpacity: .8,
    zIndex: -1
  },
      t = new google.maps.Circle(e),
      i = 2;
  $(document).ready(function() {
    var e = $(window).width();
    980 >= e && (i = 1.2)
  }), this.set("distance", i), this.bindTo("bounds", t), t.bindTo("center", this), t.bindTo("map", this), t.bindTo("radius", this), this.addSizer_(), google.maps.event.addListener(t, "click", function(e) {
    mapPicClickHandler && (source = e.latLng, GetDirections(source, destination), ResetPicFromMap())
  })
}
function ShowDistanceInfo(e, t) {
  if (!distnaceInfoWin) {
    var i = getdistantCont(t),
        r = {
        content: i,
        disableAutoPan: !0,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(23, 36),
        boxStyle: {
          opacity: 1
        },
        closeBoxMargin: "2 2px 2px 2px",
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: !1,
        pane: "floatPane",
        enableEventPropagation: !1
        };
    distnaceInfoWin = new InfoBox(r), distnaceInfoWin.open(map, e)
  }
}
function getdistantCont(e) {
  var t = "";
  return e && (t = '<div class="tiptoolhover"><div class="arrowhover"></div><div class="" style="background-color: #666666;position: absolute;z-index: 500; white-space:nowrap;"><div style="padding:0px; 2px 0px 2px;"><div class="content" style="padding-top:5px!important;"><span class="InfoWinArrow"></span><h4 style="border:0px!important;">' + roundNumber(e, 2) + " Km  </h4> </div> </div></div></div>"), t
}
function HideControlafterradiusMove() {
  $(document).ready(function() {
    var e = $(window).width();
    980 >= e && ($("#findatm").removeClass("hidden"), $("#drivingdirections").addClass("hidden"), $("#directions").hide(), $("#dircontent").css({
      display: "none"
    }))
  })
}
function getData1() {}

function getData() {

 //alert("Hai");

  HideControlafterradiusMove();
  
  var e = distanceWidget.get("position"),
      t = e.lat() + "," + e.lng(),
      i = distanceWidget.get("distance");
  RemoveBranchATMDetail(), removeRoutePath(), source = null, destination = null, mapclickHdler = null, $("#sourceTextField").val(""), $("#sourceTextField").focus(), $("#ulRoutes").html(""), $("#destinationH4").html(""), $("#ToalDistance").html(""), $("#ulRoutes").html(""), CloseInfWin(), clickedMarker && (clickedMarker.setMap(null), clickedMarker = null), totalB = 0, totalA = 0;
  var r = "";
  if (isBranchSelected && !isATMSelected) {
    if (GetBaranchATM(t, i, "branch"), parseInt(totalB) > 0) {
      var n = "24 Hrs Pharmacy found.";
      1 == parseInt(totalB) && (n = " 24 Hrs Pharmacy found."), r = " Total : " + parseInt(totalB) + " --> " + n
    }
  } else if (!isBranchSelected && isATMSelected) {
    if (GetBaranchATM(t, i, "atm"), parseInt(totalA) > 0) {
      var n = " 16 Hrs Pharmacy found.";
      1 == parseInt(totalA) && (n = " 16 Hrs Pharmacy found."), r = " Total : " + parseInt(totalA) + " --> " + n
    }
  } else if (isBranchSelected || isATMSelected) {
    if (isBranchSelected && isATMSelected && (GetBaranchATM(t, i, "both"), parseInt(totalB) > 0 || parseInt(totalA) > 0)) {
      var a = " 24 Hrs Pharmacy ",
          s = " 16 Hrs Pharmacy ";
      1 == parseInt(totalB) && (a = " 24 Hrs Pharmacy "), 1 == parseInt(totalA) && (s = " 16 Hrs Pharmacy ");
      var o = parseInt(totalB);
      0 == o && (o = "No", a = " 24 Hrs Pharmacy ");
      var l = parseInt(totalA);
      0 == l && (l = "No", s = " 16 Hrs Pharmacy "), r = " " + o + a + "and " + l + s + " found."
    }
  } else clearBranchATM(), RemoveBranchATMDetail();
  $("#totRes").html(""), $("#totRes").html(r)
}
function GetBaranchATM(e, t, i) {

	// alert("#)" + e + "#)" + t + "#)" + i +"#)");
  var r = "";
  switch (currentMode = "search", i) {
  case "atm":
    r = "GetATMData";
    break;
  case "branch":
    r = "GetBranchData";
    break;
  case "both":
    r = "GetBothData"
  }
  $.ajax({
   // url: "http://maps.icicibank.com/mobile/LEPTON/Handlers/SVCHandler.ashx?Task=" + r,
   url:"LEPTON/Handlers/handler.php?Task=" + r,
    type: "post",
    async: !1,
    data: {
      latLongString: e,
      Radius: t,
      BranchServices: gvSelectedBranchServices,
      ATMServices: gvSelectedATMServices
    },
    success: function() {},
    error: function() {}
  }).done(function(e) {
    switch (i) {
    case "atm":
      ProcessATMData(e);
      break;
    case "branch":
      ProcessBranchData(e);
      break;
    case "both":
      items = e.split("*"), ProcessBranchData(items[0]), ProcessATMData(items[1])
    }
  })
}
function ProcessBranchData(e) {
  clearBranchATM();
  for (var t = 0; t < BMArray.length; t++) BMArray[t].setMap(null);
  if (BMArray = new Array, "" != e) {
    var i = "LEPTON/Images/Branch1.png",
        r = e.split("$");
    totalB = r.length;
    for (var n = 0; n < r.length; n++) {
      var a = r[n];
      if ("" != a) {
        var s = a.split("^"),
            o = s[0],
            l = s[1],
            c = s[2],
            d = s[3],
            p = s[4],
            h = s[5],
            g = s[6],
            v = s[7],
            u = s[8],
            m = s[9],
            f = s[10],
            y = s[11],
            S = s[12],
            M = s[13],
            C = c + " : " + d,
            k = CreateMarker(o, l, C, i),
            A = "";
        "" != d && (A = A + d + ",</br>"), "" != p && (A = A + p + ", "), "" != h && (A = A + h + ", "), "" != u && (A = A + u + ", "), "" != v && (A = A + v + ", "), "" != m && (A = A + m + ""), k.address = A, k.services = g, k.peru = c, k.ifsc = f;
        var b = y + S + M;
        k.banktiming = b, BMArray.push(k)
      }
    }
  }
}
function getServiceById(e) {
  var t = "";
  if ("" != e) for (var i = e.split(","), r = 0; r < i.length; r++) for (var n = 0; n < gvCurrentDispServicesArray.length; n++) gvCurrentDispServicesArray[n].id == i[r] && (t += gvCurrentDispServicesArray[n].name + "</br>");
  return t
}
function ProcessATMData(e) {
  clearBranchATM();
  for (var t = 0; t < AMArray.length; t++) AMArray[t].setMap(null);
  if (AMArray = new Array, "" != e) {
    var i = "LEPTON/Images/ATM1.png",
        r = e.split("$");
    totalA = r.length;
    for (var n = 0; n < r.length; n++) {
      var a = r[n];
      if ("" != a) {
        var s = a.split("^"),
            o = s[0],
            l = s[1],
            c = s[2],
            d = s[3],
            p = s[4],
            h = s[5],
            g = s[6],
            v = s[7],
            u = s[8],
            m = s[9],
            f = s[10],
            y = c + " : " + d,
            S = CreateMarker(o, l, y, i),
            M = "</br>";
        "" != d && (M = M + d + ",</br>"), "" != p && (M = M + p + ", "), "" != h && (M = M + h + ", "), "" != v && (M = M + v + ", "), "" != g && (M = M + g + ", "), "" != u && (M = M + u + ""), S.address = M, S.services = m, S.peru = c, S.banktiming = "", S.siteid = f, AMArray.push(S)
      }
    }
  }
}
function clearBranchATM() {
  if (isBranchSelected && !isATMSelected) {
    for (var e = 0; e < AMArray.length; e++) AMArray[e].setMap(null);
    AMArray = new Array
  } else if (!isBranchSelected && isATMSelected) {
    for (var e = 0; e < BMArray.length; e++) BMArray[e].setMap(null);
    BMArray = new Array
  } else if (!isBranchSelected && !isATMSelected) {
    for (var e = 0; e < BMArray.length; e++) BMArray[e].setMap(null);
    BMArray = new Array;
    for (var e = 0; e < AMArray.length; e++) AMArray[e].setMap(null);
    AMArray = new Array
  }
}
function CreateMarker(e, t, i, r) {
  var n = {
    url: r + "?rndid=" + imgRandomIndex,
    size: new google.maps.Size(48, 48),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(32, 32)
  },
      a = new google.maps.Marker({
      raiseOnDrag: !1,
      position: new google.maps.LatLng(e, t, !0),
      icon: n,
      title: i
    }),
      s = SetBranchATMDetail(i, r);
  return a.liID = "li" + s, google.maps.event.addListener(a, "click", function() {
    DetailDetailClick(null, a, "")
  }), a.setMap(map), a
}
function SetBranchATMDetail(e, t) {
  var i, r = $("#ulBranchATMDetail").html(),
      n = "";
  t.indexOf("LEPTON/Images/Branch1.png") > -1 ? (i = "list bank", repCout % 2 == 1 && (i = "list bank", n = "background-color:#c5cbd7;")) : (i = "list atm", repCout % 2 == 1 && (i = "list atm", n = "background-color:#c5cbd7;"));
  var a = r + '<li class="' + i + '" style="' + n + '" id="li' + repCout + '"onclick="DetailDetailClick(this,null,\'' + e + '\');"><a href="javascript:"> ' + e + "</a> </li>";
  $("#ulBranchATMDetail").html(a);
  var s = 0;
  return s = repCout, repCout += 1, s
}
function RemoveBranchATMDetail() {
  $("#ulBranchATMDetail").html(""), $("#totRes").html(""), repCout = 0
}
function openMobileInfoWin() {
  $("#findatm").removeClass("hidden"), $("#drivingdirections").addClass("hidden"), $("#directiontext").show(), $("#directions").css({
    display: "block"
  }), $("#dircontent").css({
    display: "block"
  }), $(".filterContainer").removeClass("open")
}

function DetailDetailClick(e, t, i) {

	//alert("(@@)" + e + "(@@)" + t + "(@@)" + i + "(@@)");
  var r = !1,
      n = !1,
      a = $(window).width();
  if (460 >= a ? (n = !0, openMobileInfoWin()) : a > 460 && 980 >= a ? (n = !0, openMobileInfoWin()) : a > 980 && (r = !0), t || (t = fetchMarcker(i)), t) {
    if (clickInfoWin && clickInfoWin.close(), riseImage = t.icon.url, clickedMarker && (riseImage = clickedMarker.icon.url.indexOf("LEPTON/Images/Branch64.png") > -1 ? "LEPTON/Images/Branch1.png" : "LEPTON/Images/ATM1.png", clickedMarker.icon.url = riseImage + "?rndid=" + imgRandomIndex, clickedMarker.icon.size = new google.maps.Size(48, 48), clickedMarker.icon.anchor = new google.maps.Point(32, 32), clickedMarker.setMap(null), clickedMarker.setMap(map)), clickedMarker = t, destination = clickedMarker.position, r) {
      if ($("#infoTitle").html(""), $("#infoTitle").html(t.peru), $("#infoIfsc").html(""), void 0 != t.ifsc && t.ifsc.indexOf("APOLLO") >= 0 && $.isNumeric(t.ifsc.substring(4)) ? ($("#infoIfsc").css("display", "block"), $("#infoIfsc").html("IFSC CODE : " + t.ifsc)) : $("#infoIfsc").css("display", "none"), $("#infoAddress").html(""), $("#infoAddress").html(t.address), "" == t.banktiming) $("#workinghours").css("display", "none");
      else {
        var s = t.banktiming.toLowerCase();
        $("#workinghours").css("display", "block"), $("#workinghours").html(""), $("#workinghours").html(s.indexOf("24 x 7") > -1 || s.indexOf("operational") > -1 || s.indexOf("only") > -1 ? t.banktiming : t.banktiming + ' <span style="font-size:11px">Open<span>')
      }
      if (t.icon.url.indexOf("LEPTON/Images/ATM1.png") > -1) {
        $("#infoStatus").css("display", "block");
		
		//alert("%%" + t.siteid + "%%");
        
		var o = t.siteid,
            l = DoInServ("GetESQATMStatus", {
            siteid: o
          });
        "" != l ? ($("#infoStatus").html(""), $("#infoStatus").html("Current Status : " + l)) : $("#infoStatus").css("display", "none")
      } else $("#infoStatus").css("display", "none")
    }
    if ($("#ServiceListView").html(""), $("servicesdrawer1").html(""), t.icon.url.indexOf("LEPTON/Images/Branch1.png") > -1 && "" != t.services) {
      for (var c = t.services.split(","), d = "", p = "", h = 0; h < c.length; h++) {
        var g = getBranchServiceById(c[h]);
        if (g) {
          var v = g.name,
              u = g.image;
          d = d + '<div class="ListItem"><img src="' + u + '" width="24" height="24"><span>' + v + "</span></div>", p = p + '<li class="field"><div class="ListItem"><img src="' + u + '" width="24" height="24"><span>' + v + "</span></div></li>"
        }
      }
      if (r && ($("#ServiceListView").html(d), $("#divScotainer").css({
        display: "block"
      }), $(".SizeDiv").css({
        height: "300px"
      })), n) {
        var m = "<h6>" + t.peru + "</h6><span>" + t.address + "</span>";
        t.ifsc.indexOf("ICIC") >= 0 && $.isNumeric(t.ifsc.substring(4)) && (m = "<h6>" + t.peru + "</h6><span>" + t.address + "<br />IFSC CODE : " + t.ifsc + "</span>"), $("#address").html(""), $("#address").html(m), $("#servicesdrawer1").html(p), $("#dirservices").css({
          display: "block"
        });
        var s = t.banktiming.toLowerCase();
        $("#MobileTiming").html("");
        var f = "<li>" + t.banktiming + " (Closed on 2nd and 4th Saturday)</li>";
        (s.indexOf("24 x 7") > -1 || s.indexOf("operational") > -1 || s.indexOf("only") > -1) && (f = "<li>" + t.banktiming + "</li>"), $("#MobileTiming").html(f)
      }
      riseImage = "LEPTON/Images/Branch64.png"
    } else {
      if ($("#ServiceListView").html(""), $("servicesdrawer1").html(""), t.icon.url.indexOf("LEPTON/Images/ATM1.png") > -1 && "" != t.services) for (var c = t.services.split(","), d = "", p = "", h = 0; h < c.length; h++) {
        var g = getATMServiceById(c[h]);
        if (g) {
          var v = g.name,
              u = g.image;
          "ALL ATM" != v && (d = d + '<div class="ListItem"><img src="' + u + '" width="24" height="24"><span>' + v + "</span></div>", p = p + '<li class="field"><div class="ListItem"><img src="' + u + '" width="24" height="24"><span>' + v + "</span></div></li>")
        }
      }
      if (n) {
        var m = "<h6>" + t.peru + "</h6><span>" + t.address + "</span>";
        if ($("#address").html(""), $("#address").html(m), $("#servicesdrawer1").html(p), $("#dirservices").css(t.icon.url.indexOf("LEPTON/Images/ATM1.png") > -1 && t.services ? {
          display: "block"
        } : {
          display: "none"
        }), t.icon.url.indexOf("LEPTON/Images/ATM1.png") > -1) {
          $("#dirATMStatus").css("display", "block");
          var o = t.siteid,
              l = DoInServ("GetESQATMStatus", {
              siteid: o
            });
          if ("" != l) {
            $("#ATMStatus1").html("");
            var y = "<li>Current Status : " + l + "</li>";
            $("#ATMStatus1").html(y)
          } else $("#dirATMStatus").css("display", "none")
        } else $("#dirATMStatus").css("display", "none")
      }
      r && (t.icon.url.indexOf("LEPTON/Images/ATM1.png") > -1 && "" != t.services ? ($("#ServiceListView").html(d), $("#divScotainer").css({
        display: "block"
      }), $(".SizeDiv").css({
        height: "300px"
      })) : ($("#divScotainer").css({
        display: "none"
      }), $(".SizeDiv").css({
        height: "225px"
      }))), riseImage = riseImage = "LEPTON/Images/ATM64.png"
    }
    if (r) {
      $("#btnGetDir").css({
        name: t.title
      });
      var p = document.getElementById("InfWin");
      p = p.innerHTML;
      var S = {
        content: p,
        disableAutoPan: !0,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(20, -20),
        boxStyle: {
          opacity: 1
        },
        closeBoxMargin: "2 2px 2px 2px",
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: !1,
        pane: "floatPane",
        enableEventPropagation: !1
      };
      clickInfoWin = new InfoBox(S), clickInfoWin.open(map, t)
    }
    t.icon.url = riseImage + "?rndid=" + imgRandomIndex, t.icon.size = new google.maps.Size(64, 64), t.icon.anchor = new google.maps.Point(40, 45), t.setMap(null), t.setMap(map), map.setCenter(t.position), $("#destinationH4").html(""), $("#destinationH4").html(clickedMarker.title)
  }
  $(".ServiceList").length > 0;
  var M;
  if (void 0 != e) var M = e.id;
  else if (null == e && null != t && "" == i) var M = t.liID;
  $("#" + M).hasClass("list bank") ? $("#" + M).addClass("POPBRANCH") : $("#" + M).hasClass("list atm") && $("#" + M).addClass("POPATM"), $("#" + M).siblings().removeClass("POPBRANCH"), $("#" + M).siblings().removeClass("POPATM");
  var C = $(".ResultContainer"),
      k = $("#" + M);
  C.animate({
    scrollTop: k.offset().top - C.offset().top + C.scrollTop()
  }), n && ("" == t.banktiming ? $("#dirlandmarks").css("display", "none") : $("#dirlandmarks").css("display", "block"))
}
function CloseInfWin() {
  clickInfoWin && clickInfoWin.close()
}
function fetchMarcker(e) {
  var t = null,
      r = !1;
  for (i = 0; i < BMArray.length; i++) if (BMArray[i].title == e) {
    t = BMArray[i], r = !0;
    break
  }
  if (0 == r) for (i = 0; i < AMArray.length; i++) if (AMArray[i].title == e) {
    t = AMArray[i];
    break
  }
  return t
}
function SHSPanel() {
  (0 == gvAllATMServicesArray.length && 0 == gvAllBranchServicesArray.length || "all" == gvSelectedATMServices || "all" == gvSelectedBranchServices) && ("all" == gvSelectedATMServices && (gvSelectedATMServices = ""), "all" == gvSelectedBranchServices && (gvSelectedBranchServices = ""), LoadProductAndServices());
  var e = ($(".fullContainer #filteritems #filterservices a img").attr("src"), $("#servicesdrawer"));
  e.is(":visible") ? (e.hide("fast"), $(".fullContainer #filteritems #filterservices a img").attr("src", "img/DisplayImage_DropdownArrow.png")) : (e.show("fast"), $(".fullContainer #filteritems #filterservices a img").attr("src", "img/DisplayImage_DropdownArrowUp.png"))
}
function LoadProductAndServices() {
  if (0 == gvAllBranchServicesArray.length && 0 == gvAllBranchServicesArray.length) {
    var e = DoInServ("GetServices", {});

	// alert("%%" + e + "%%");

    if ("" != e) {
      var t = e.split("*"),
          i = t[0].split("$"),
          r = t[1].split("$"),
          n = !1;
      ("" == gvSelectedBranchServices || "0" == gvSelectedBranchServices || "all" == gvSelectedBranchServices) && (gvSelectedBranchServices = "", n = !0);
      for (var a = 0; a < i.length; a++) {
        var s = i[a].split("^"),
            o = imgPath + s[2];
        gvAllBranchServicesArray[a] = {
          id: s[0],
          name: s[1],
          image: o
        }, n && (gvSelectedBranchServices += s[0] + ",")
      }
      n && (gvSelectedBranchServices = gvSelectedBranchServices.substring(0, gvSelectedBranchServices.length - 1)), n = !1, ("" == gvSelectedATMServices || "0" == gvSelectedATMServices || "all" == gvSelectedATMServices) && (gvSelectedATMServices = "", n = !0);
      for (var a = 0; a < r.length; a++) {
        var s = r[a].split("^"),
            o = imgPath + s[2];
        gvAllATMServicesArray[a] = {
          id: s[0],
          name: s[1],
          image: o
        }, n && (gvSelectedATMServices += s[0] + ",")
      }
      n && (gvSelectedATMServices = gvSelectedATMServices.substring(0, gvSelectedATMServices.length - 1)), ShowBranchServices()
    }
  }
}
function ShowBranchServices() {
  if (gvAllBranchServicesArray.length > 0) {
    gvCurrentDispServicesArray = new Array, $("ul#IdServiceList").empty(), $("#IdServiceList").html("");
    var e = "";
    e = "" == gvSelectedBranchServices || "0" == gvSelectedBranchServices ? '<input type="checkbox" name="services1" id="chkSD"   value="Select All"> <span></span></label></li>' : gvAllBranchServicesArray.length != gvSelectedBranchServices.split(",").length ? '<input type="checkbox" name="services1" id="chkSD"   value="Select All"> <span></span></label></li>' : '<input type="checkbox" name="services1" id="chkSD"   value="Select All" checked> <span><i class="img-ok" ></i></span></label></li>';
    for (var t = '<li class="field" style="padding-bottom: 10px; border-bottom: 2px solid #d7d7d7;"><label class="checkbox" style="cursor:pointer;"  for="Select All" ><Strong>Select All</Strong>' + e, i = 0; i < gvAllBranchServicesArray.length; i++) {
      var r = gvAllBranchServicesArray[i];
      if ("" == gvSelectedBranchServices || "0" == gvSelectedBranchServices) t = t + '<li class="field" style="background: url(' + r.image + ')  no-repeat 10px center #fff; visibility:hide"><label style="cursor:pointer;" class="checkbox" for="' + r.name + '" >' + r.name + '<input type="checkbox"  name="services" id="chk' + i + '"  value="' + r.name + '" checked="checked"><span></span></label></li>';
      else {
        var n = '<input type="checkbox"  name="services" id="chk' + i + '"  value="' + r.name + '"><span></span></label></li>';
        IsServiceThere(gvSelectedBranchServices, r.id) && (n = '<input type="checkbox"  name="services" id="chk' + i + '"  value="' + r.name + '" checked="checked"><span><i class="img-ok" ></i></span></label></li>'), t = t + '<li class="field" style="background: url(' + r.image + ')  no-repeat 10px center #fff; visibility:hide"><label style="cursor:pointer;" class="checkbox" for="' + r.name + '" >' + r.name + n
      }
      gvCurrentDispServicesArray[i] = {
        id: r.id,
        name: r.name,
        image: r.image
      }
    }
    $("#IdServiceList").html(t), IsBranchService = !0
  }
}
function ShowATMServices() {
  if (gvAllATMServicesArray.length > 0) {
    gvCurrentDispServicesArray = new Array, $("ul#IdServiceList").empty(), $("#IdServiceList").html("");
    var e = "";
    e = "" == gvSelectedATMServices || "0" == gvSelectedATMServices ? '<input type="checkbox" name="services1" id="chkSD"   value="Select All"> <span></span></label></li>' : gvAllATMServicesArray.length != gvSelectedATMServices.split(",").length ? '<input type="checkbox" name="services1" id="chkSD"   value="Select All"> <span></span></label></li>' : '<input type="checkbox" name="services1" id="chkSD"   value="Select All" checked> <span><i class="img-ok" ></i></span></label></li>';
    for (var t = '<li class="field" style="padding-bottom: 10px; border-bottom: 2px solid #d7d7d7;"><label class="checkbox" style="cursor:pointer;"  for="Select All" ><Strong>Select All</Strong>' + e, i = 0; i < gvAllATMServicesArray.length; i++) {
      var r = gvAllATMServicesArray[i];
      if ("" == gvSelectedATMServices || "0" == gvSelectedATMServices) t = t + '<li class="field" style="background: url(' + r.image + ')  no-repeat 10px center #fff; visibility:hide"><label style="cursor:pointer;" class="checkbox" for="' + r.name + '" >' + r.name + '<input type="checkbox"  name="services" id="chk' + i + '"  value="' + r.name + '" checked="checked"><span></span></label></li>';
      else {
        var n = '<input type="checkbox"  name="services" id="chk' + i + '"  value="' + r.name + '"><span></span></label></li>';
        IsServiceThere(gvSelectedATMServices, r.id) && (n = '<input type="checkbox"  name="services" id="chk' + i + '"  value="' + r.name + '" checked="checked"><span><i class="img-ok" ></i></span></label></li>'), t = t + '<li class="field" style="background: url(' + r.image + ')  no-repeat 10px center #fff; visibility:hide"><label style="cursor:pointer;" class="checkbox" for="' + r.name + '" >' + r.name + n
      }
      gvCurrentDispServicesArray[i] = {
        id: r.id,
        name: r.name,
        image: r.image
      }
    }
    gvAllATMServicesArray.length < 11 && (t += '<li class="field" style="height:200px;"></li>')
  }
  IsBranchService = !1, $("#IdServiceList").html(t)
}
function Loadservice() {
  var e = "",
      t = !1;
  if (headerCheckbox) {
    var i = $("#servicesdrawer").find("input")[0].checked;
    if (i) for (var r = $("#servicesdrawer").find("span"), n = 0; n < gvCurrentDispServicesArray.length; n++) 0 == r.eq(n + 1).find(".img-ok").length && $(r).eq(n + 1).append('<i class="img-ok"></i>'), $("#servicesdrawer").find("input").attr("checked", "checked")[n + 1], $("#servicesdrawer").find("input").prop("checked", !0)[n + 1], e += gvCurrentDispServicesArray[n].id + ",";
    else {
      for (var n = 0; n < gvCurrentDispServicesArray.length; n++) {
        var a = $("#servicesdrawer").find("span");
        a.find(".img-ok").remove()[n + 1], a.removeAttr("checked")[n + 1], a.prop("checked", !1)[n + 1]
      }
      e = ""
    }
  } else {
    for (var r = $("#servicesdrawer").find("span"), n = 0; n < gvCurrentDispServicesArray.length; n++) 0 == r.eq(n + 1).find(".img-ok").length ? t = !0 : e += gvCurrentDispServicesArray[n].id + ",";
    if (1 == t) {
      var s = $("#servicesdrawer").find("span").first();
      s.find(".img-ok").remove(), s.removeAttr("checked"), s.prop("checked", !1)
    } else 0 == $("#servicesdrawer").find("span").first().find(".img-ok").length && $("#servicesdrawer").find("span").first().append('<i class="img-ok"></i>')[0], $("#servicesdrawer").find("input").attr("checked", "checked")[0], $("#servicesdrawer").find("input").prop("checked", !0)[0]
  }
  "" != e && (e = e.substring(0, e.length - 1)), IsBranchService ? gvSelectedBranchServices = e : gvSelectedATMServices = e
}
function IsServiceThere(e, t) {
  var i = !1;
  if (e.indexOf(",") >= 0) {
    for (var r = e.split(","), n = 0; n < r.length; n++) if (r[n] == t) {
      i = !0;
      break
    }
  } else e == t && (i = !0);
  return i
}
function SelectBranchTab() {
  $("#btnBranchService").hasClass("ATMBtn") && ($("#btnBranchService").removeClass("ATMBtn"), $("#btnBranchService").addClass("ServiceSelected"), $("#btnATMService").removeClass("ServiceSelected"), $("#btnATMService").addClass("ATMBtn"), ShowBranchServices())
}
function SelectATMTab() {
  $("#btnATMService").hasClass("ATMBtn") && ($("#btnATMService").removeClass("ATMBtn"), $("#btnATMService").addClass("ServiceSelected"), $("#btnBranchService").removeClass("ServiceSelected"), $("#btnBranchService").addClass("ATMBtn"), ShowATMServices())
}
function LoadProductAndServices_old() {
  var e = DoInServ("GetServices", {});
  if ("" != e) {
    var t = e.split("*");
    BranchItems = t[0].split("$"), ATMIItems = t[1].split("$"), serviceCount = BranchItems.length, servicesArrary = new Array, $("#IdServiceList").html("");
    for (var i = '<li class="field" style="padding-bottom: 10px; border-bottom: 2px solid #d7d7d7;"><label class="checkbox" style="cursor:pointer;"  for="Select All" ><Strong>Select All</Strong><input type="checkbox" name="services1" id="chkSD"   value="Select All" checked><span><i class="img-ok" ></i></span></label></li>', r = 0; serviceCount > r; r++) {
      var n = BranchItems[r].split("^"),
          a = imgPath + n[2];
      i = i + '<li class="field" style="background: url(' + a + ')  no-repeat 10px center #fff; visibility:hide"><label style="cursor:pointer;" class="checkbox" for="' + n[1] + '" >' + n[1] + '<input type="checkbox"  name="services" id="chk' + r + '"  value="' + n[1] + '" checked="checked"><span><i class="img-ok" ></i></span></label></li>', servicesArrary[r] = {
        id: n[0],
        name: n[1],
        image: n[2]
      }
    }
    $("#IdServiceList").html(i), IsBranchService = !0, Loadservice()
  }
}
function LoadSelectedServices(e) {
  $("ul#IdServiceList").empty(), $("#IdServiceList").html(""), e ? ShowBranchServices() : ShowATMServices()
}
function IsServiceUnchecked() {
  for (var e = !1, t = 0; t < gvCurrentDispServicesArray.length; t++) if ($("#servicesdrawer").find("input")[t + 1].checked) {
    e = !0;
    break
  }
  return e
}
function ApplyService() {
  gvServiceChanged && (Loadservice(), "Country" == drillLevel ? ("showall" == currentMode && LoadStMarkers(), SelectBranchAtmFilterData()) : "State" == drillLevel ? (LoadCtMarkers(StateName), SelectBranchAtmFilterData()) : "City" == drillLevel && getData(), gvServiceChanged = !1, headerCheckbox = !1)
}
function GetServiceById(e) {
  for (var t, i = 0; i < gvCurrentDispServicesArray.length; i++) if (gvCurrentDispServicesArray[i].id == e) {
    t = gvCurrentDispServicesArray[i];
    break
  }
  return t
}
function getATMServiceById(e) {
  return 0 == gvAllATMServicesArray.length && LoadProductAndServices(), findService(gvAllATMServicesArray, e)
}
function getBranchServiceById(e) {
  return 0 == gvAllBranchServicesArray.length && LoadProductAndServices(), findService(gvAllBranchServicesArray, e)
}
function findService(e, t) {
  for (var i, r = 0; r < e.length; r++) if (e[r].id == t.trim()) {
    i = e[r];
    break
  }
  return i
}
function getDestination() {
  $("#drivedetails").css({
    display: "block"
  }), clickedMarker && ($("#sourceTextField").val(""), $("#sourceTextField").focus(), $("#ToalDistance").html(""), $("#ulRoutes").html(""), removeRoutePath(), destination = clickedMarker.position, $("#destinationH4").html(""), $("#destinationH4").html(clickedMarker.title), GetDir(), CloseInfWin())
}
function GetDirections(e, t) {
  if (void 0 != e && void 0 != t) {
    var i = ValidateIndiaDirLatLng(e.lat(), e.lng(), t.lat(), t.lng());
    if ("" == i || "false" == i) return $("#sourceTextField").val(""), void $("#sourceTextField").focus();
    var r = new google.maps.DirectionsService,
        n = {
        draggable: !0
        };
    removeRoutePath(), directionsDisplay = new google.maps.DirectionsRenderer(n), directionsDisplay.setMap(map);
    var a = {
      origin: e,
      destination: t,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    r.route(a, function(e, t) {
      t == google.maps.DirectionsStatus.OK && (directionsDisplay.setDirections(e), showSteps(e))
    });
    var s = !1;
    google.maps.event.addListener(directionsDisplay, "directions_changed", function() {
      s && directionsDisplay.directions.status == google.maps.DirectionsStatus.OK && showSteps(directionsDisplay.directions), s = !0
    }), CloseInfWin()
  }
}
function showSteps(e) {
  $("#drivedetails").css("display", "block");
  var t = e.routes[0].legs[0];
  $("#sourceTextField").val(t.start_address), $("#ToalDistance").html("");
  var i = t.distance.text + " , " + t.duration.text;
  $("#ToalDistance").html(i), $("#ulRoutes").html("");
  for (var r = "", n = 0; n < t.steps.length; n++) {
    var a = "<li>";
    n % 2 == 1 && (a = '<li class="lblue">'), r = r + a + '<p class="number">' + n + '. </p><div class="DirectionBlockDiv"> <p class="directions">' + t.steps[n].instructions + '</p></div><p class="distance">' + t.steps[n].distance.text + "," + t.steps[n].duration.text + "</p></li>"
  }
  $("#ulRoutes").html(r)
}
function SearchDirPlaces() {
  $("#sourceTextField").val("");
  var e = document.getElementById("sourceTextField");
  sourceAutoComplete = new google.maps.places.Autocomplete(e), sourceAutoComplete.bindTo("bounds", map), google.maps.event.addListener(sourceAutoComplete, "place_changed", function() {
    var e = sourceAutoComplete.getPlace();
    e.geometry && (newSource = e.geometry.location, isSearAuto = !0)
  })
}
function geoPhysicalLocAddress() {
  navigatorLatLng && (source = navigatorLatLng, destination && GetDirections(source, destination))
}
function removeRoutePath() {
  directionsDisplay && directionsDisplay.setMap(null)
}
function Reset() {
  $("#sourceTextField").val(""), $("#sourceTextField").focus(), $("#ToalDistance").html(""), $("#ulRoutes").html(""), removeRoutePath(), $("#destinationH4").html(""), clickedMarker && ($("#destinationH4").html(clickedMarker.title), destination = clickedMarker.position), ResetPicFromMap(), $("#directions").hide()
}
function Reverse() {
  var e = $("#sourceTextField").val();
  if ("" != e && "Enter a location" != e) {
    var t = $("#destinationH4").html();
    $("#destinationH4").html(""), $("#destinationH4").html(e), $("#sourceTextField").val(t);
    var i = source;
    source = destination, destination = i
  }
}
function GetNewDir() {
  $("#ulRoutes").html(""), $("#ToalDistance").html("");
  var e = $("#sourceTextField").val();
  return "" != e ? (isSearAuto && (source = newSource, isSearAuto = !1), source ? void(destination && (GetDirections(source, destination), $("#directions").css({
    display: "block"
  }), $("#drivedetails").css("display", "block"))) : void geocoder.geocode({
    address: e
  }, function(e, t) {
    return t == google.maps.GeocoderStatus.OK ? (source = e[0].geometry.location, void(source && destination && GetDirections(source, destination))) : void 0
  })) : void 0
}
function GetDir() {
  $("#findatm").addClass("hidden"), $("#drivingdirections").removeClass("hidden"), DirWinOpen = !0
}
function BackDir() {
  $("#drivingdirections").addClass("hidden"), $("#findatm").removeClass("hidden"), DirWinOpen = !1
}
function GenerateAlertMessage(e) {
  null != document.getElementById("divMsgAlert") && HideAlert_PU();
  var t, i;
  t = window.innerWidth / 2 - 111, i = window.innerHeight / 2 - 77.5;
  var r = document.createElement("div");
  r.innerHTML = AlertMessage_PU(e, t, i), document.body.appendChild(r.firstChild)
}
function AlertMessage_PU(e, t, i) {
  var r = "<div id ='divMsgAlert' class='AlertMsg' style='top: " + i + "px; left: " + t + "px; position: fixed; z-index: 10000; display: block; background-repeat: no-repeat;'><a class='AlertClose' onclick='HideAlert_PU();' ></a><p style='margin:0px 0pt;'><strong>Alert message</strong></p><hr style='color:#fff; margin:0px;'/><p>" + e + "</p>";
  return r
}
function HideAlert_PU() {
  var e = document.getElementById("divMsgAlert");
  document.body.removeChild(e)
}
function HighlighResult() {
  $(".MapTypeBtns a").on("click", function() {
    $(this).hasClass("OrangeButton") ? $(this).siblings().removeClass("OrangeButton") : ($(this).addClass("OrangeButton"), $(this).siblings().removeClass("OrangeButton"))
  })
}
function PicFromMap() {
  mapPicClickHandler || (map.setOptions({
    draggableCursor: "crosshair"
  }), $("#picB").removeClass("picButton"), $("#picB").addClass("picButton1"), mapPicClickHandler = google.maps.event.addListener(map, "click", function(e) {
    source = e.latLng, GetDirections(source, destination), ResetPicFromMap()
  }))
}
function ResetPicFromMap() {
  mapPicClickHandler && (google.maps.event.removeListener(mapPicClickHandler), mapPicClickHandler = null, map.setOptions({
    draggableCursor: ""
  }), $("#picB").hasClass("picButton1") && ($("#picB").removeClass("picButton1"), $("#picB").addClass("picButton"))), $("#drivedetails").css("display", "block"), $("#directions").css({
    display: "block"
  })
}
google.maps.visualRefresh = !0;
var map, geocoder, center_pt, checkData, checkData1, ttInfoWin, clickInfoWin, headerCheckbox = !1,
    imgPath = "LEPTON/Handlers/SeriveceImage/index.html",
    isSearchDone = !1,
    isReloading = !1,
    isMapLoadingFirstTime = !1;
document.onkeydown = function(e) {
  var t = document.all ? event.keyCode : e.which;
  13 == t && (DirWinOpen ? $("#getdirections").click() : (isSearchDone = !1, $("#btnSearch").click(), isSearchDone = !0))
}, $.ajaxSetup({
  beforeSend: function() {
    $(".Pageloader").fadeIn("slow")
  },
  complete: function() {
    $(".Pageloader").fadeOut("fast")
  }
}), $(document).ready(function() {
  $(".MapTypeBtns a").on("click", function() {
    $(this).hasClass("OrangeButton") ? $(this).siblings().removeClass("OrangeButton") : ($(this).addClass("OrangeButton"), $(this).siblings().removeClass("OrangeButton"))
  }), $(".MobileMapButton ").on("click", function(e) {
    $(".MobileMapButton").toggleClass("openSlider"), e.preventDefault(), $(this).parents(".filterContainer").toggleClass("open")
  }), $(".SelectMaptype a").on("click", function() {
    $(this).hasClass("OrangeButton") ? $(this).siblings().removeClass("OrangeButton") : ($(this).addClass("OrangeButton"), $(this).siblings().removeClass("OrangeButton"))
  })
});
var gLatLng = "",
    isAuto = !1,
    autocomplete, rc = 0,
    navigatorLatLng;
CustomMarker.prototype = new google.maps.OverlayView, window.CustomMarker = CustomMarker, CustomMarker.prototype.getVisible = function() {
  return this.get("visible")
}, CustomMarker.prototype.getVisible = CustomMarker.prototype.getVisible, CustomMarker.prototype.setVisible = function(e) {
  this.set("visible", e)
}, CustomMarker.prototype.setVisible = CustomMarker.prototype.setVisible, CustomMarker.prototype.visible_changed = function() {
  this.ready_ && (this.markerWrapper_.style.display = this.getVisible() ? "" : "none", this.draw())
}, CustomMarker.prototype.visible_changed = CustomMarker.prototype.visible_changed, CustomMarker.prototype.getWidth = function() {
  return this.get("width")
}, CustomMarker.prototype.getWidth = CustomMarker.prototype.getWidth, CustomMarker.prototype.getHeight = function() {
  return this.get("height")
}, CustomMarker.prototype.getHeight = CustomMarker.prototype.getHeight, CustomMarker.prototype.setZIndex = function(e) {
  this.set("zIndex", e)
}, CustomMarker.prototype.setZIndex = CustomMarker.prototype.setZIndex, CustomMarker.prototype.getZIndex = function() {
  return this.get("zIndex")
}, CustomMarker.prototype.getZIndex = CustomMarker.prototype.getZIndex, CustomMarker.prototype.zIndex_changed = function() {
  this.getZIndex() && this.ready_ && (this.markerWrapper_.style.zIndex = this.getZIndex())
}, CustomMarker.prototype.zIndex_changed = CustomMarker.prototype.zIndex_changed, CustomMarker.prototype.getPosition = function() {
  return this.get("position")
}, CustomMarker.prototype.getPosition = CustomMarker.prototype.getPosition, CustomMarker.prototype.setPosition = function(e) {
  this.set("position", e)
}, CustomMarker.prototype.setPosition = CustomMarker.prototype.setPosition, CustomMarker.prototype.position_changed = function() {
  this.draw()
}, CustomMarker.prototype.position_changed = CustomMarker.prototype.position_changed, CustomMarker.prototype.getAnchor = function() {
  return this.get("anchor")
}, CustomMarker.prototype.getAnchor = CustomMarker.prototype.getAnchor, CustomMarker.prototype.setAnchor = function(e) {
  this.set("anchor", e)
}, CustomMarker.prototype.setAnchor = CustomMarker.prototype.setAnchor, CustomMarker.prototype.anchor_changed = function() {
  this.draw()
}, CustomMarker.prototype.anchor_changed = CustomMarker.prototype.anchor_changed, CustomMarker.prototype.htmlToDocumentFragment_ = function(e) {
  var t = document.createElement("DIV");
  if (t.innerHTML = e, 1 == t.childNodes.length) return t.removeChild(t.firstChild);
  for (var i = document.createDocumentFragment(); t.firstChild;) i.appendChild(t.firstChild);
  return i
}, CustomMarker.prototype.removeChildren_ = function(e) {
  if (e) for (var t; t = e.firstChild;) e.removeChild(t)
}, CustomMarker.prototype.setContent = function(e) {
  this.set("content", e)
}, CustomMarker.prototype.setContent = CustomMarker.prototype.setContent, CustomMarker.prototype.getContent = function() {
  return this.get("content")
}, CustomMarker.prototype.getContent = CustomMarker.prototype.getContent, CustomMarker.prototype.content_changed = function() {
  if (this.markerContent_) {
    this.removeChildren_(this.markerContent_);
    var e = this.getContent();
    if (e) {
      "string" == typeof e && (e = e.replace(/^\s*([\S\s]*)\b\s*$/, "$1"), e = this.htmlToDocumentFragment_(e)), this.markerContent_.appendChild(e);
      for (var t, i = this, r = this.markerContent_.getElementsByTagName("IMG"), n = 0; t = r[n]; n++) google.maps.event.addDomListener(t, "load", function() {
        i.draw()
      });
      google.maps.event.trigger(this, "domready")
    }
    this.ready_ && this.draw()
  }
}, CustomMarker.prototype.content_changed = CustomMarker.prototype.content_changed, CustomMarker.prototype.getOffset_ = function() {
  var e = this.getAnchor();
  if ("object" == typeof e) return e;
  var t = new google.maps.Size(0, 0);
  if (!this.markerContent_) return t;
  var i = this.markerContent_.offsetWidth,
      r = this.markerContent_.offsetHeight;
  switch (e) {
  case CustomMarkerPosition.TOP_LEFT:
    break;
  case CustomMarkerPosition.TOP:
    t.width = -i / 2;
    break;
  case CustomMarkerPosition.TOP_RIGHT:
    t.width = -i;
    break;
  case CustomMarkerPosition.LEFT:
    t.height = -r / 2;
    break;
  case CustomMarkerPosition.MIDDLE:
    t.width = -i / 2, t.height = -r / 2;
    break;
  case CustomMarkerPosition.RIGHT:
    t.width = -i, t.height = -r / 2;
    break;
  case CustomMarkerPosition.BOTTOM_LEFT:
    t.height = -r;
    break;
  case CustomMarkerPosition.BOTTOM:
    t.width = -i / 2, t.height = -r;
    break;
  case CustomMarkerPosition.BOTTOM_RIGHT:
    t.width = -i, t.height = -r
  }
  return t
}, CustomMarker.prototype.onAdd = function() {
  if (this.markerWrapper_ || (this.markerWrapper_ = document.createElement("DIV"), this.markerWrapper_.style.position = "absolute"), this.getZIndex() && (this.markerWrapper_.style.zIndex = this.getZIndex()), this.markerWrapper_.style.display = this.getVisible() ? "" : "none", !this.markerContent_) {
    this.markerContent_ = document.createElement("DIV"), this.markerWrapper_.appendChild(this.markerContent_);
    var e = this;
    google.maps.event.addDomListener(this.markerContent_, "click", function() {
      google.maps.event.trigger(e, "click")
    }), google.maps.event.addDomListener(this.markerContent_, "mouseover", function() {
      google.maps.event.trigger(e, "mouseover")
    }), google.maps.event.addDomListener(this.markerContent_, "mouseout", function() {
      google.maps.event.trigger(e, "mouseout")
    })
  }
  this.ready_ = !0, this.content_changed();
  var t = this.getPanes();
  t && (this.markerWrapper_.style.cursor = "pointer", t.overlayMouseTarget.appendChild(this.markerWrapper_)), google.maps.event.trigger(this, "ready")
}, CustomMarker.prototype.onAdd = CustomMarker.prototype.onAdd, CustomMarker.prototype.draw = function() {
  if (this.ready_ && !this.dragging_) {
    var e = this.getProjection();
    if (e) {
      var t = this.get("position"),
          i = e.fromLatLngToDivPixel(t),
          r = this.getOffset_();
      this.markerWrapper_.style.top = i.y + r.height + "px", this.markerWrapper_.style.left = i.x + r.width + "px";
      var n = this.markerContent_.offsetHeight,
          a = this.markerContent_.offsetWidth;
      a != this.get("width") && this.set("width", a), n != this.get("height") && this.set("height", n)
    }
  }
}, CustomMarker.prototype.draw = CustomMarker.prototype.draw, CustomMarker.prototype.onRemove = function() {
  this.markerWrapper_ && this.markerWrapper_.parentNode && this.markerWrapper_.parentNode.removeChild(this.markerWrapper_), this.removeDragListeners_()
}, CustomMarker.prototype.onRemove = CustomMarker.prototype.onRemove;
var CustomMarkerPosition = {
  TOP_LEFT: 1,
  TOP: 2,
  TOP_RIGHT: 3,
  LEFT: 4,
  MIDDLE: 5,
  RIGHT: 6,
  BOTTOM_LEFT: 7,
  BOTTOM: 8,
  BOTTOM_RIGHT: 9
};
window.CustomMarkerPosition = CustomMarkerPosition;
var drillLevel = "Country",
    StateMarkers = new Array,
    CityMarkers = new Array,
    infoWin, StateName = "",
    CityName = "",
    currentMode = "",
    isBranchSelected = !1,
    isATMSelected = !1,
    gvBSID = 0,
    gvASID = 0;
InfoBox.prototype = new google.maps.OverlayView, InfoBox.prototype.createInfoBoxDiv_ = function() {
  var e, t = this,
      i = function(e) {
      e.cancelBubble = !0, e.stopPropagation && e.stopPropagation()
      },
      r = function(e) {
      e.returnValue = !1, e.preventDefault && e.preventDefault(), t.enableEventPropagation_ || i(e)
      };
  this.div_ || (this.div_ = document.createElement("div"), this.setBoxStyle_(), "undefined" == typeof this.content_.nodeType ? this.div_.innerHTML = this.getCloseBoxImg_() + this.content_ : (this.div_.innerHTML = this.getCloseBoxImg_(), this.div_.appendChild(this.content_)), this.getPanes()[this.pane_].appendChild(this.div_), this.addClickHandler_(), this.div_.style.width ? this.fixedWidthSet_ = !0 : 0 !== this.maxWidth_ && this.div_.offsetWidth > this.maxWidth_ ? (this.div_.style.width = this.maxWidth_, this.div_.style.overflow = "auto", this.fixedWidthSet_ = !0) : (e = this.getBoxWidths_(), this.div_.style.width = this.div_.offsetWidth - e.left - e.right + "px", this.fixedWidthSet_ = !1), this.panBox_(this.disableAutoPan_), this.enableEventPropagation_ || (this.eventListener1_ = google.maps.event.addDomListener(this.div_, "mousedown", i), this.eventListener2_ = google.maps.event.addDomListener(this.div_, "click", i), this.eventListener3_ = google.maps.event.addDomListener(this.div_, "dblclick", i)), this.contextListener_ = google.maps.event.addDomListener(this.div_, "contextmenu", r), google.maps.event.trigger(this, "domready"))
}, InfoBox.prototype.getCloseBoxImg_ = function() {
  var e = "";
  return "" !== this.closeBoxURL_ && (e = "<img", e += " src='" + this.closeBoxURL_ + "'", e += " align=right", e += " style='", e += " position: relative;", e += " cursor: pointer;", e += " margin: " + this.closeBoxMargin_ + ";", e += "'>"), e
}, InfoBox.prototype.addClickHandler_ = function() {
  var e;
  "" !== this.closeBoxURL_ ? (e = this.div_.firstChild, this.closeListener_ = google.maps.event.addDomListener(e, "click", this.getCloseClickHandler_())) : this.closeListener_ = null
}, InfoBox.prototype.getCloseClickHandler_ = function() {
  var e = this;
  return function(t) {
    t.cancelBubble = !0, t.stopPropagation && t.stopPropagation(), e.close(), closeinfowindow(!0), google.maps.event.trigger(e, "closeclick")
  }
}, InfoBox.prototype.panBox_ = function(e) {
  var t, i, r = 0,
      n = 0;
  if (!e && (t = this.getMap(), t instanceof google.maps.Map)) {
    t.getBounds().contains(this.position_) || t.setCenter(this.position_), i = t.getBounds();
    var a = t.getDiv(),
        s = a.offsetWidth,
        o = a.offsetHeight,
        l = this.pixelOffset_.width,
        c = this.pixelOffset_.height,
        d = this.div_.offsetWidth,
        p = this.div_.offsetHeight,
        h = this.infoBoxClearance_.width,
        g = this.infoBoxClearance_.height,
        v = this.getProjection().fromLatLngToContainerPixel(this.position_);
    if (v.x < -l + h ? r = v.x + l - h : v.x + d + l + h > s && (r = v.x + d + l + h - s), this.alignBottom_ ? v.y < -c + g + p ? n = v.y + c - g - p : v.y + c + g > o && (n = v.y + c + g - o) : v.y < -c + g ? n = v.y + c - g : v.y + p + c + g > o && (n = v.y + p + c + g - o), 0 !== r || 0 !== n) {
      {
        t.getCenter()
      }
      t.panBy(r, n)
    }
  }
}, InfoBox.prototype.setBoxStyle_ = function() {
  var e, t;
  if (this.div_) {
    this.div_.className = this.boxClass_, this.div_.style.cssText = "", t = this.boxStyle_;
    for (e in t) t.hasOwnProperty(e) && (this.div_.style[e] = t[e]);
    "undefined" != typeof this.div_.style.opacity && "" !== this.div_.style.opacity && (this.div_.style.filter = "alpha(opacity=" + 100 * this.div_.style.opacity + ")"), this.div_.style.position = "absolute", this.div_.style.visibility = "hidden", null !== this.zIndex_ && (this.div_.style.zIndex = this.zIndex_)
  }
}, InfoBox.prototype.getBoxWidths_ = function() {
  var e, t = {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
      i = this.div_;
  return document.defaultView && document.defaultView.getComputedStyle ? (e = i.ownerDocument.defaultView.getComputedStyle(i, ""), e && (t.top = parseInt(e.borderTopWidth, 10) || 0, t.bottom = parseInt(e.borderBottomWidth, 10) || 0, t.left = parseInt(e.borderLeftWidth, 10) || 0, t.right = parseInt(e.borderRightWidth, 10) || 0)) : document.documentElement.currentStyle && i.currentStyle && (t.top = parseInt(i.currentStyle.borderTopWidth, 10) || 0, t.bottom = parseInt(i.currentStyle.borderBottomWidth, 10) || 0, t.left = parseInt(i.currentStyle.borderLeftWidth, 10) || 0, t.right = parseInt(i.currentStyle.borderRightWidth, 10) || 0), t
}, InfoBox.prototype.onRemove = function() {
  this.div_ && (this.div_.parentNode.removeChild(this.div_), this.div_ = null)
}, InfoBox.prototype.draw = function() {
  this.createInfoBoxDiv_();
  var e = this.getProjection().fromLatLngToDivPixel(this.position_);
  this.div_.style.left = e.x + this.pixelOffset_.width + "px", this.alignBottom_ ? this.div_.style.bottom = -(e.y + this.pixelOffset_.height) + "px" : this.div_.style.top = e.y + this.pixelOffset_.height + "px", this.div_.style.visibility = this.isHidden_ ? "hidden" : "visible"
}, InfoBox.prototype.setContent = function(e) {
  this.content_ = e, this.div_ && (this.closeListener_ && (google.maps.event.removeListener(this.closeListener_), this.closeListener_ = null), this.fixedWidthSet_ || (this.div_.style.width = ""), "undefined" == typeof e.nodeType ? this.div_.innerHTML = this.getCloseBoxImg_() + e : (this.div_.innerHTML = this.getCloseBoxImg_(), this.div_.appendChild(e)), this.fixedWidthSet_ || (this.div_.style.width = this.div_.offsetWidth + "px", this.div_.innerHTML = this.getCloseBoxImg_() + e), this.addClickHandler_()), google.maps.event.trigger(this, "content_changed")
}, InfoBox.prototype.setPosition = function(e) {
  this.position_ = e, this.div_ && this.draw(), google.maps.event.trigger(this, "position_changed")
}, InfoBox.prototype.setZIndex = function(e) {
  this.zIndex_ = e, this.div_ && (this.div_.style.zIndex = e), google.maps.event.trigger(this, "zindex_changed")
}, InfoBox.prototype.getContent = function() {
  return this.content_
}, InfoBox.prototype.getPosition = function() {
  return this.position_
}, InfoBox.prototype.getZIndex = function() {
  return this.zIndex_
}, InfoBox.prototype.show = function() {
  this.isHidden_ = !1, this.div_ && (this.div_.style.visibility = "visible")
}, InfoBox.prototype.hide = function() {
  this.isHidden_ = !0, this.div_ && (this.div_.style.visibility = "hidden")
}, InfoBox.prototype.open = function(e, t) {
  var i = this;
  t && (this.position_ = t.getPosition(), this.moveListener_ = google.maps.event.addListener(t, "position_changed", function() {
    i.setPosition(this.getPosition())
  })), this.setMap(e), this.div_ && this.panBox_()
}, InfoBox.prototype.close = function() {
  this.closeListener_ && (google.maps.event.removeListener(this.closeListener_), this.closeListener_ = null), this.eventListener1_ && (google.maps.event.removeListener(this.eventListener1_), google.maps.event.removeListener(this.eventListener2_), google.maps.event.removeListener(this.eventListener3_), this.eventListener1_ = null, this.eventListener2_ = null, this.eventListener3_ = null), this.moveListener_ && (google.maps.event.removeListener(this.moveListener_), this.moveListener_ = null), this.contextListener_ && (google.maps.event.removeListener(this.contextListener_), this.contextListener_ = null), this.setMap(null)
};
var distanceWidget, imgRandomIndex = Math.round(1e4 * Math.random());
DistanceWidget.prototype = new google.maps.MVCObject, DistanceWidget.prototype.fitCenter = function() {
  var e = this.get("position");
  map.setCenter(e)
}, RadiusWidget.prototype = new google.maps.MVCObject, RadiusWidget.prototype.distance_changed = function() {
  this.set("radius", 1e3 * this.get("distance"))
}, RadiusWidget.prototype.addSizer_ = function() {
  var e = {
    url: "LEPTON/Images/Resize.png",
    size: new google.maps.Size(24, 24),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(12, 12)
  },
      t = new google.maps.Marker({
      draggable: !0,
      icon: e,
      cursor: "ew-resize",
      title: "Drag to resize the cicle!",
      raiseOnDrag: !1
    });
  t.bindTo("map", this), t.bindTo("position", this, "sizer_position");
  var i = this;
  google.maps.event.addListener(t, "drag", function() {
    i.setDistance();
    var e = getdistantCont(i.distance);
    distnaceInfoWin && distnaceInfoWin.setContent(e)
  }), google.maps.event.addListener(t, "dragend", function() {
    i.fitCircle(), getData();
    var e = i.get("distance");
    ShowDistanceInfo(t, e)
  })
}, RadiusWidget.prototype.center_changed = function() {
  var e = this.get("bounds");
  if (e) {
    var t = e.getNorthEast().lng(),
        i = new google.maps.LatLng(this.get("center").lat(), t);
    this.set("sizer_position", i)
  }
}, RadiusWidget.prototype.distanceBetweenPoints_ = function(e, t) {
  if (!e || !t) return 0;
  var i = 6371,
      r = (t.lat() - e.lat()) * Math.PI / 180,
      n = (t.lng() - e.lng()) * Math.PI / 180,
      a = Math.sin(r / 2) * Math.sin(r / 2) + Math.cos(e.lat() * Math.PI / 180) * Math.cos(t.lat() * Math.PI / 180) * Math.sin(n / 2) * Math.sin(n / 2),
      s = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)),
      o = i * s;
  return o > 10 && (o = 10), .5 > o && (o = .5), o
}, RadiusWidget.prototype.setDistance = function() {
  var e = this.get("sizer_position"),
      t = this.get("center"),
      i = this.distanceBetweenPoints_(t, e);
  this.set("distance", i);
  var r = this.get("bounds");
  if (r) {
    var n = r.getNorthEast().lng(),
        a = new google.maps.LatLng(this.get("center").lat(), n);
    this.set("sizer_position", a)
  }
}, RadiusWidget.prototype.fitCircle = function() {
  var e = this.get("bounds");
  if (e) {
    map.fitBounds(e);
    var t = e.getNorthEast().lng(),
        i = new google.maps.LatLng(this.get("center").lat(), t);
    this.set("sizer_position", i)
  }
};
var distnaceInfoWin, totalB = 0,
    totalA = 0,
    BMArray = new Array,
    AMArray = new Array,
    repCout = 0,
    clickedMarker, gvSelectedATMServices = "all",
    gvSelectedBranchServices = "all",
    gvAllATMServicesArray = new Array,
    gvAllBranchServicesArray = new Array,
    gvCurrentDispServicesArray = new Array,
    IsBranchService = !0,
    gvServiceChanged = !1,
    serviceCount = 0,
    servicesArrary = new Array,
    ATMservicesArray = new Array,
    serviceChanged = !1,
    selectedServices = "",
    BranchItems, ATMIItems;
$(document).ready(function() {
  $("#Dir").click(function() {
    $("#DirP").show("slide", {
      direction: "right"
    }, 1e3)
  }), $("#DirC").click(function() {
    $("#DirP").hide("slide", {
      direction: "right"
    }, 1e3)
  }), $("#Batm").click(function() {
    $("#BatmP").show("slide", {
      direction: "right"
    }, 1e3)
  }), $("#BatmC").click(function() {
    $("#BatmP").hide("slide", {
      direction: "right"
    }, 1e3)
  }), $("#Services_Btn").click(function() {
    return $(".ProductSer_Panel").toggle("slow"), !1
  }), $("#Region_Btn").click(function() {
    return $(".Region_Panel").toggle("slow"), !1
  })
});
var source, destination, mapclickHdler, directionsDisplay, sourceAutoComplete, newSource, isSearAuto = !1,
    DirWinOpen = !1,
    mapPicClickHandler;