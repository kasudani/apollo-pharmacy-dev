(function($){
	
	var $window = $(window);
	var $floatingSidebar = $('.LeftPanel');
	$window.on('scroll', function(){	
		
		if($(document).width() > 980){
			var scrollTop = $(this).scrollTop();
			var top = scrollTop+30;
			if(scrollTop<=100){
				top = 130;
			};
			/*if(scrollTop>=1060){
				top = 1060;
			};*/
			
			var bodyHeight = $('body').height();
			
			if (top >= (bodyHeight - (404 + 480 + 60))) {
				top = bodyHeight - 404 - 480 - 60;
			}

			if(scrollTop<=100 || top >= (bodyHeight - (404 + 480 + 60))){	
				$floatingSidebar.css({
					position: 'absolute',
					top: top
				})
			}else{
				$floatingSidebar.css({
					position: 'fixed',
					top: 92
				})
			}

			/*$floatingSidebar
				.stop(false,false)
				.animate({
					top:top
				},200);*/
		}
	});
	

	//$window.on('resize', function () {  
	//    alert('as');

	//});

	$.ajaxSetup({
	    beforeSend: function () {
	        // show image here
	        $(".Pageloader").fadeIn("slow");
	    },
	    complete: function () {
	        // hide image here
	        //$(".Pageloader").fadeOut("slow");
	        $(".Pageloader").fadeOut("fast");
	    }
	});
    /******End Progress*********/

    /********infowindow open according to device*******************/
	$(document).ready(function () {
	   
	    var ScrSize = $(window).width();
	    if (ScrSize <= 460)

	    {
	       // alert("screen size is 460px");
	        $('#Test1').css({ display: "none" });
	        $('#Test2').css({ display: "none" });
       }
	    else
	        if (ScrSize > 460 && ScrSize <= 980)
	        { //alert("screen size is between 460px and 980px");
	            $('#Test1').css({ display: "none" });
	            $('#Test2').css({ display: "none" });
	        }
	        else
	            if (ScrSize > 980)
	            {
	              //  alert("screen size is bigger than 980px");
	                $('#Test1').css({ display: "block" });
	                $('#Test2').css({ display: "none" });
	            }

	});

    /**********end****************/

    // block for open privilage menu

	//$('.menu li').hover(function(){
	//	$(this).find('.arrow-up').css('display', 'block');
	//	$(this).find('.nav-child').show();
	//}, function(){
	//	$(this).find('.arrow-up').hide();
	//	$(this).find('.nav-child').hide();
	//});

	$('ul.fourNav li').on('mouseover', function(){
		$('.info-container').hide();
		var con = $(this).attr('rel');
		$(this).addClass('active');
		
		$(con).show();
		
		/*if(con){
			if($(this).hasClass('active')){
				$('ul.fourNav li').removeClass('active');
				$(con).hide();
			}else{
				$('ul.fourNav li').removeClass('active');
				$(this).addClass('active');
				$(con).show();
			}
		}*/
	}).on('mouseout', function(){
		var con = $(this).attr('rel');
		$(this).removeClass('active');
		$(con).hide();
	});

	$('.info-container').on('mouseover', function(){
		var id = $(this).attr('id');
		$('ul.fourNav li[rel=#' + id + ']').addClass('active');
		$(this).show();
	}).on('mouseout', function(){
		var id = $(this).attr('id');
		$('ul.fourNav li[rel=#' + id + ']').removeClass('active');
		$(this).hide();
	});

	$('a.close').click(function(){
		var con = '';
		$(this).closest('.info-container').hide();
		con = $(this).attr('rel');
		$(con).removeClass('active');
	});

	$('.LeftPanel .buttons').on('mouseover', function(){
		name = $(this).attr('href');
		var displayvalue = $(name).css('display');
		$('.right-nav-child').hide();
		$(this).append('<span></span>');
		$(name).show();
		//return false;
	}).on('mouseout', function(){
		name = $(this).attr('href');
		var displayvalue = $(name).css('display');
		$('.LeftPanel .buttons').children('span').remove();
		$('.right-nav-child').hide();
	});

	$('.right-nav-child').on('mouseover', function(){
		name = $(this).attr('id');
		$(this).show();
		$('.LeftPanel .buttons[href="#' + name + '"]').append('<span></span>');

	}).on('mouseout', function(){
		$(this).hide();
		$('.LeftPanel .buttons[href="#' + name + '"]').children('span').remove();
	});

    $(window).load(function(){
		if($(".ResultContainer").length>0){
        //	$(".ResultContainer").customScrollbar();
		}
		if($(".DirectionsContainer").length>0){
			//$('.DirectionsContainer').customScrollbar();
		}
	});

	$(".check-box").uniform();
	$(".radio-box").uniform();
	$(".select-box").uniform();
	$("footer .containers li select").uniform();
	$('.fullContainer .ButtonAlignment li.Services ul.checkboxes li .checkboxContainer input').uniform();
	$('.fullContainer .ButtonAlignment li.Services ul.checkboxes').hide();

	var imgsrc = $('.fullContainer #filteritems #filterservices a img').attr('src');
	var imgreplace = $('.fullContainer #filteritems #filterservices a img').attr('src');
	$(document).ready(function () {
	    var ScrSize = $(window).width();
	    if (ScrSize <= 980) {
	        $('.fullContainer #filteritems #filterservices a').attr('class', 'toggle');
	      //  alert('a1');
	        }
	        else
	    {
	       // alert('a2');
	        $('.fullContainer #filteritems #filterservices a').removeAttr('class');
	        //newly added
	        $('.fullContainer #filteritems #filterservices').hover(function () {
	            //Satya CR9: $('.fullContainer #filteritems #filterservices a img').attr('src', imgreplace.replace('DisplayImage_DropdownArrow', 'DisplayImage_DropdownArrowUp'));
	            //Satya CR9: $("#findatm .filterContainer #filterops #servicesdrawer").show();
	        }, function () {
	            //Satya CR9:  $('.fullContainer #filteritems #filterservices a img').attr('src', imgsrc);
	            //Satya CR9:  $("#findatm .filterContainer #filterops #servicesdrawer").hide();
	        });

	        $("#findatm .filterContainer #filterops #servicesdrawer").hover(function () {
	            //Satya CR9:  $("#findatm .filterContainer #filterops #servicesdrawer").show();
	        }, function () {
	            //Satya CR9: $("#findatm .filterContainer #filterops #servicesdrawer").hide();
	        });
	        //end
	        
	    }

	});

	
		
	
	$('.fullContainer .ButtonAlignment li.Services').hover(function(){
		$('.fullContainer .ButtonAlignment li.Services img').attr('src',imgreplace.replace('DisplayImage_DropdownArrow','DisplayImage_DropdownArrowUp'));
		$('.fullContainer .ButtonAlignment li.Services ul.checkboxes').show();
	}, function(){
		$('.fullContainer .ButtonAlignment li.Services img').attr('src',imgsrc);
		$('.fullContainer .ButtonAlignment li.Services ul.checkboxes').hide();
	});

/* action moved to setButtonSelction in drillDownManager.js
if ($('.fullContainer #filteritems #btnATM').length > 0) {
$('.fullContainer #filteritems #btnATM').click(function (e) {
if($(this).hasClass('selected')){
$(this).removeClass('selected');
}else{
$(this).addClass('selected');
}
});
}
	
if ($('.fullContainer #filteritems #btnBranch').length > 0) {
$('.fullContainer #filteritems #btnBranch').click(function (e) {
if($(this).hasClass('selected')){
$(this).removeClass('selected');
}else{
$(this).addClass('selected');
}
});
}
*/

	if($('.BigBanner').length>0){
		$('.BigBanner .banner li').css('width',$('.BigBanner').width()+'px');
	}
	
	if($('.BigBanner').length>0){
		$('.BigBanner .banner').cycle({
			fx:	'scrollRight',
 				before: function(){
       				$(this).parent().find('li.current').removeClass();
					$('.pointers').removeClass('selected');
				},
				after: function(){								
       				$(this).addClass('current');
					var id = $(this).parent().find('li.current').attr('id').replace('image','pointer');
					$('#'+id).addClass('selected');
				},
				containerResize: false,
				slideResize: false,
				fit: 1
		});
		
		$('ul.pagination').css('left',(($('.BigBanner').width()-960)/2)+'px');
		
		$('.pointers').click(function(){
			var pointerId = $(this).attr('id').replace('pointer','')*1; 
			$('.BigBanner .banner').cycle(pointerId-1); 
			return false; 
		}); 
	}
	
	if($('.breadCrumbsContainer').length>0){
		$('.breadCrumbsContainer').css('left',(($('.BigBanner').width()-960)/2)+'px');
		$('.reviewContainer').css('left',(($('.BigBanner').width()-960)/2)+'px');
	}

	$('.LeftPanel').on('click', function(){
		if($(document).width() < 980){
		    //if($('.open-right').length == 0){
		    //to open the left menu comment the window.location.href and uncomment the  $('body').

		 //   window.open("https://infinity.icicibank.com/corp/AuthenticationController?FORMSGROUP_ID__=AuthenticationFG&amp;__START_TRAN_FLAG__=Y&amp;FG_BUTTONS__=LOAD&amp;ACTION.LOAD=Y&amp;AuthenticationFG.LOGIN_FLAG=1&amp;BANK_ID=ICI&amp;ACCESS_DEV=WAP&amp;_ga=1.7652858.97892247.1402396153");

		  //  window.location.href = "https://m.icicibank.co.in/BANKAWAY?Action.RetUser.WAPInit.001=Y&AppSignonBankId=ICI&AppType=corporate&abrdPrf=N";
            //$('body').toggleClass('open-right').removeClass('open-left');
		//};
		}
	});

	$('#Other').on('change', function () {
	    var urlT = $('#Other').val();
	    window.open(urlT);
	});

	$('#Select1').on('change', function () {
	    var urlT = $('#Select1').val();
	    window.open(urlT);
	});

	

	$('.LeftPanel > div').on('click', function(e){
		e.stopImmediatePropagation();
	});

	$('.LeftPanel a').on('click', function(e){
		
	});

	$('.nav-header .menu').on('click', function(){
		if($(document).width() < 980){
		    //if($('.open-right').length == 0){
		    //to open the left menu comment the window.location.href and uncomment the  $('body').
		    window.open("http://www.apollopharmacy.in/");
			//  $('body').toggleClass('open-left').removeClass('open-right');
		//};
		}
	});

	$('.leftColumnContainer').on('click', function(){
		if($(document).width() < 980 && !$(this).parents('body').hasClass('social')){
			var $this = $(this);
			$this.toggleClass('open');
			if($this.hasClass('open')){
				$this.find('li').show('fast');
			}else{
				$this.find('li').hide('fast', function(){
					$(this).attr('style','');
				});
			};
		}
	});

	$('.leftColumnContainer li').on('click', function(e){
		e.stopImmediatePropagation();
	})

	$('.leftColumnContainer li a').on('click', function(e){
		if($(document).width() < 980){
			//e.preventDefault();
			var $this = $(this);
			$this.addClass('selected').parent().siblings().find('a').removeClass('selected').parents('.leftColumnContainer').attr('data-content', $.trim($this.text().replace('›', ''))).toggleClass('open').find('li').hide('fast');
		};
	})

	$('.social-filter nav').on('click', function(){
		if($(document).width() < 980){
			var $this = $(this);
			$this.parent().toggleClass('open');
			if($this.parent().hasClass('open')){
				$this.find('a').show('fast').css('display', 'block');
			}else{
				$this.find('a').hide('fast');
			};
		}
	});

	$('.social-filter a').on('click', function(e){
		if($(document).width() < 980){
			e.stopImmediatePropagation();
			e.preventDefault();

			var $this = $(this);
			$this.addClass('selected').siblings().removeClass('selected').parent().attr('data-content', $.trim($this.text().replace('›', ''))).parent().toggleClass('open').find('a').hide('fast');
		};
	})

	$('.result-boxes .box ul').on('click', function(){
		var $this = $(this);
		$this.toggleClass('open');
		if($this.hasClass('open')){
			$this.find('li:gt(5)').show('fast');
			$this.attr('data-content', 'Less');
		}else{
			$this.find('li:gt(5)').hide('fast');
			$this.attr('data-content', 'More');
		};
	});

	$('.result-boxes .box li').on('click', function(e){
		e.stopImmediatePropagation();
	});

	$('.visa-video').on('click', function(e){
		if($(document).width() < 980){
			e.stopImmediatePropagation();
			e.stopPropagation();
			if($(this).find('iframe').length == 0){
				$(this).append('<iframe width="100%" height="100%" src="//www.youtube.com/embed/X-49VAwHBx4?autoplay=1" frameborder="0" allowfullscreen></iframe>');
			};
		}
	});

	$('#servicesdrawer').on('click', ".field .checkbox", function () {
	   // try{alert($(this).find("input").prop("id"));}catch (err) { alert("ERROR"); }

	    var $this = $(this);
	    if ($(this).find("input").prop("id") == 'chkSD') {
	        headerCheckbox = true;
	    }
	    if ($this.find('span').find('.img-ok').length > 0) {
	        $this.find('span').find('.img-ok').remove();
	        $this.find('input').removeAttr('checked');
	        $this.find('input').prop("checked", false);
	    } else {
	        $this.find('span').append('<i class="img-ok"></i>')
	        $this.find('input').attr('checked', 'checked');
	        $this.find('input').prop("checked", true);
	    }

	    gvServiceChanged = true;
		//Loadservice();
		ApplyService();
	
	});

	$('.locationButton').on('click', function(e){
		if($(document).width() < 980){
			//$('#directiontext').hide();
		//	$('#drivingdirections').addClass('hidden');
		};
	});

$('.toggle').on('click', function (e) {
   	e.preventDefault();
		var $this = $(this);
		var $drawer = $($this.attr('gumby-trigger'));

		if ($drawer.selector == "#drivedetails") {
		    $('#drivedetails').css({ display: "block" });
		    var input = $('#sourceTextField').val();
		    if (input == '') {
		        return;
		    }
		}
		if ($drawer.selector == "#drivingdirections1") {
		    var input = $('#sourceTextField').val();
		    if (input == '') {
		        $('#findatm').removeClass('hidden');
		        $('#drivingdirections').addClass('hidden');
		      //  $('#directions').hide();
		    }
		    //else {
		    //    $('#findatm').removeClass('hidden');
		    //    $('#drivingdirections').addClass('hidden');
		    //}
		}
		else {
		    //Service Panel		   

		    if ($drawer.is(':visible')) {
		        //$drawer.hide('fast');
		        //$('.fullContainer #filteritems #filterservices a img').attr('src', imgsrc); //Satya CR9
		    } else {
		        //$drawer.show('fast');
		        //$('.fullContainer #filteritems #filterservices a img').attr('src', imgreplace.replace('DisplayImage_DropdownArrow', 'DisplayImage_DropdownArrowUp')); //Satya CR9
		    }
		    if ($drawer.selector == "#drivedetails") {
		        $('#drivedetails').css({ display: "block" });
		        $drawer.show('fast');
		    }
		    if ($drawer.selector == "#drivingdirections") {
		        $('#directions').css({ display: "none" });
		        $('#dircontent').css({ display: "none" });
		    }

		    if ($drawer.selector == "#directiontext") {
		        $('#directions').css({ display: "none" });
		        $('#dircontent').css({ display: "none" });
		    }
		}
		
	});

	$('#dirget .toggle').on('click', function (e) {
	  //   alert("asd");

	    e.preventDefault();

	    //  getDestination();
	     $('#sourceTextField').val('');
	     $('#sourceTextField').focus();
	     $("#ToalDistance").html('');
	     $("#ulRoutes").html('');
	     //$("#ddtable").html('');
	     //$("#ddtable").html('<tbody><tr><td class="dirdetails" style="height:295px; width:100%;"></td></tr></tbody>');
	     //$("#ddtable").addClass("BlankGrid");
	     removeRoutePath();

		var $this = $(this);
		var $drawer = $($this.attr('gumby-trigger')); 
		$('#findatm').addClass('hidden');
		//$('#drivedetails').hide();
		if($drawer.is(':visible')){
			$drawer.addClass('hidden');
			$this.parents('#directiontext').show();
			//$('#ShowRoute').hide();
			//$('#ShowBottom').show();
		}else{
			$drawer.removeClass('hidden');
			$this.parents('#directiontext').hide();
			//$('#ShowRoute').show();
			//$('#ShowBottom').hide();
		}
		
	});

	$('#SideRight #ShowRoute').on('click', function () {
	    $('#findatm').toggleClass('hidden');
	    $('#drivingdirections').toggleClass('hidden');
	    $('#directions').show();

	});

	$('#directions .toggle').on('click', function(e){
		//if($('#findatm .filterContainer').hasClass('open') || !$('#drivingdirections').hasClass('hidden') ){
		//	$('#findatm .filterContainer').removeClass('open');
		//	$('#findatm').removeClass('hidden');
		//	$('#drivingdirections').addClass('hidden');
	    //};

	    if ($('#findatm').hasClass('hidden')) {
	        $('#directions .toggle i').removeClass('img-angle-down').addClass('img-angle-up');


	        $('#findatm .filterContainer').removeClass('open');
	        $('#findatm').removeClass('hidden');
	        $('#drivingdirections').addClass('hidden');
	    } else if ($('#drivingdirections').hasClass('hidden')) {
	        $('#directions .toggle i').removeClass('img-angle-up').addClass('img-angle-down');
	       // $('#findatm .filterContainer').addClass('open');
	        $('#findatm').addClass('hidden');
	        $('#drivingdirections').removeClass('hidden');
	    };
	});

	$('.bank-answers-page .answers-article h2').on('click', function(){
		var $this = $(this);
		$this.parent().toggleClass('close');
	});

	$('.filter-dropdown select').on('change', function(){
		var $this = $(this);
		$this.siblings('span').find('.filter-dropdown-value').text($this.val());
	});

	$('.discuss-write-review').on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$('.comment-article').parents('.answers-content').show('fast');
	})

	$('.comment-article .close-btn').on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.parents('.answers-content').hide('fast');
	}).trigger('click');

	$('.discuss-nav span').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		if($(document).width() < 980){
			$this.parent().toggleClass('open');
		}
	});

	$('.discuss-nav a:not(.discuss-write-review)').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		$this.addClass('selected').siblings().removeClass('selected').parent().removeClass('open').find('span').html($this.html());
	}).filter('.selected').trigger('click');

	$('.review-dropdown > a').on('click', function(e){
		e.preventDefault();
	});

	$('.open-modal').on('click', function(e){
		var $this = $(this);
		if($this.parents('offer-zone') && $(document).width() > 980){
			$('#modal').css('top', $(window).scrollTop() + 40)
		}else{
			e.stopImmediatePropagation();
			window.location = $this.data('url');
		};
	});

	$('body').on('click', '.tncs-details', function(e){
		e.preventDefault();
		$(this).addClass('selected').parent().siblings().find('a').removeClass('selected');
		$(this).parents('.offer-wrapper').find('.offer-terms').hide().siblings('.offer-right').show();
	});

	$('body').on('click', '.tncs-link', function(e){
		e.preventDefault();
		$(this).addClass('selected').parent().siblings().find('a').removeClass('selected');
		$(this).parents('.offer-wrapper').find('.offer-right').hide().siblings('.offer-terms').show();
	});

	if($('.offer-subnav-wrapper').length){
		var wrapperWidth = 0;
		$.each($('.offer-subnav-wrapper').find('li'), function(){
			wrapperWidth+=$(this).outerWidth(true);
		})
		$('.offer-subnav-wrapper ul').css('width', wrapperWidth);
	};
	

	$('.offer-subnav-prev, .offer-subnav-next').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		var movePos = $this.siblings('.offer-subnav-wrapper').width();
		var $ul = $this.siblings('.offer-subnav-wrapper').find('ul');

		if($ul.width() > movePos){
			if($this.hasClass('offer-subnav-prev')){

				if($ul.position().left + movePos - 46 >= 0){
					movePos = -$ul.position().left + 46;
				}

				$ul.stop(true, true).animate({
					left: '+=' + movePos
				}, 300)
			}else{

				if(($ul.position().left - movePos*2 - 46)*-1 >= $this.siblings('.offer-subnav-wrapper').find('ul').width()){
					movePos = $this.siblings('.offer-subnav-wrapper').find('ul').width() - $this.siblings('.offer-subnav-wrapper').width() + $ul.position().left - 46;
				}

				$ul.stop(true, true).animate({
					left: '-=' + movePos
				}, 300)
			}
		}
	});

	if($(".question .slider").length > 0){
		$( ".question .slider" ).slider({
			value: 80,
			slide: function(event, ui){
				
				var leftPos = ui.value;
				if ($(window).width() > 980) leftPos -= 10;
				else {
					if (ui.value > 90) leftPos -= 25;
					else leftPos -= 10;
				}
				
				$('.question.active .perc-indicator')
					.css({left: leftPos + '%'})
					.find('span').html(ui.value);
			}
		});
	}else if($('.slider').length > 0){
		$(".slider").slider({
			value:20,
			min: 0,
			max: 100,
			step: 10,
			slide: function( event, ui ) {
					$(".slider a").html('<div class="slider-hover"><input type="text" class="amount"/><span>%</span></div>');
					$( ".amount" ).val( ui.value);
				}
		});
		$(".slider a").html('<div class="slider-hover"><input type="text" class="amount"/><span>%</span></div>');
		$(".amount").val( $(".slider").slider("value"));
	};

	$('#expand-all').on('click', function(e){
		e.preventDefault();
		$('.answers-article').removeClass('close');
	});
	$('#close-all').on('click', function(e){
		e.preventDefault();
		$('.answers-article').removeClass('close');
		$('.answers-article').addClass('close');
	});

	$('.logged-out a').on('click', function(e){
		e.preventDefault();

		var $this = $(this);
		$this.parents('.ImageContainer').removeClass('logged-out').addClass('loading');

		setTimeout(function(){
			$this.parents('.ImageContainer').removeClass('loading').addClass('logged-in');
		}, 1500);
	})

	var scrollWidth = 0;
	var newsTicker = $(".news-ticker");
	var newsTickerUl = newsTicker.find('ul');
	var newsTickerWrap = newsTicker.find(".news-ticker-content");
	var move = 1;
	var moveTimeout;

	$.each(newsTicker.find('li'), function(){
		scrollWidth += $(this).outerWidth(true);
	});

	newsTickerUl.css('width', scrollWidth);

	setTimeout(function(){ scroll(); }, 1000);

	function scroll() {
		if(move){
			var newsTickerMargin = parseFloat(newsTickerUl.css('margin-left'));
			var firstChild = newsTicker.find('li:first-child');

			if(parseFloat(newsTickerMargin) == -firstChild.outerWidth()){			
				//newsTickerUl.css('margin-left', newsTickerMargin + firstChild.outerWidth() );
				newsTickerUl.css('margin-left', 0)
				newsTickerUl.append(firstChild);
			}
			newsTickerUl.animate({'margin-left': '-=1' }, 30, 'linear', scroll);
		};
	};

	$('.news-ticker-next').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		var newsTickerMargin = parseFloat(newsTickerUl.css('margin-left'));
		var firstChild = newsTicker.find('li:first-child');

		newsTickerUl.stop(true);
		clearTimeout(moveTimeout);

		newsTickerUl.animate({'margin-left': -firstChild.outerWidth()}, 1000, function(){
			newsTickerUl.append(firstChild);
			newsTickerUl.css('margin-left', 0);
			moveTimeout = setTimeout(function(){ scroll(); }, 1000)
		});
	});

	$('.news-ticker-prev').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		var newsTickerMargin = parseFloat(newsTickerUl.css('margin-left'));
		var lastChild = newsTicker.find('li:last-child');

		newsTickerUl.stop(true);
		clearTimeout(moveTimeout);

		newsTickerUl.prepend(lastChild).css('margin-left', newsTickerMargin-lastChild.outerWidth());
		
		newsTickerUl.animate({'margin-left': 0}, 1000, function(){
			moveTimeout = setTimeout(function(){ scroll(); }, 1000)
		});

	});

	$('#resultlist a').on('click', function(e){
		e.preventDefault();
		$(this).parents('#findatm').addClass('hidden').siblings('#drivingdirections').removeClass('hidden');
	});

	$('#driveheading a').on('click', function(e){
		e.preventDefault();
		$(this).parents('#drivingdirections').addClass('hidden').siblings('#findatm').removeClass('hidden');
	});

	$('.locationButton').on('click', function (e) {
	    e.preventDefault();
		var $this = $(this);
		$this.parents('.filterContainer').toggleClass('open');

	});

    //Show hide login button for mobile version
	if ($(document).width() < 980) {	    
	    if (GetQueryParamByName('m') == 1) {
	        $("header .menu li.selected").addClass("selected selectedhide");
	        $(".LeftPanel").addClass("LeftPanel LeftPanelhide");
	        $('.nav-header .menu').unbind("click");
	        $('.LeftPanel').unbind("click");
	    }
	}
	
	
	

})(jQuery);