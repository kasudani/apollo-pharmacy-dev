#!/bin/bash
LDIR="var/log/"
FILENAME=$(date +%d-%m-%Y).tgz
 
tar --exclude="*.php" --exclude="*.html" --exclude="*.phtml" --exclude="*.tgz" --create --gzip --file=$LDIR$FILENAME -C $LDIR .

find $LDIR* -name "*.log" -type f -exec sh -c '>{}' \;

find $LDIR -mtime +60 -name '*.tgz' -exec rm -rf {} \;

echo "All logs are cleared and "$FILENAME" backup is saved in "$LDIR
