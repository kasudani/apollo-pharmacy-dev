<?php
 
use \Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
 
$obj = $bootstrap->getObjectManager();
$state = $obj->get('\Magento\Framework\App\State');
$state->setAreaCode('adminhtml');
 
$configurationHelper = $obj->get('Tagalys\Sync\Helper\Configuration');
$syncHelper = $obj->get('Tagalys\Sync\Helper\Sync');
$queueHelper = $obj->get('Tagalys\Sync\Helper\Queue');
 
foreach ($configurationHelper->getStoresForTagalys() as $storeId) {
    $syncHelper->triggerFeedForStore($storeId);
}
$queueHelper->truncate();

?>