<?php
/**
 * TEST CRM ORDERS CRON
 * --------------- 
 */
ini_set('display_errors', 1); 
error_reporting(E_ALL); 
use Magento\Framework\App\Bootstrap; 
require dirname(__DIR__) . '/app/bootstrap.php'; 
$bootstrap = Bootstrap::create(BP, $_SERVER);

/* Logger Started */
$writer = new \Zend\Log\Writer\Stream(BP.'/var/log/crmCronTest.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);

$date = new DateTime();
$timeStamp = $date->getTimestamp();
$cron_file_name = 'crmCron-'.$date->getTimestamp().'.log';

$logger->info('logger started => ' . $date->getTimestamp());
$logger->info("create  => " . $cron_file_name);
$logger->info(" ");

// echo __DIR__. "\n";
// echo $timeStamp . "\n";
// // println
// echo("some");