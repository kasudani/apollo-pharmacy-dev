<?php
	/**
	 * CRM ORDERS CRON
	 * --------------- 
	 */

// 	ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
	# start time
	$time_start = microtime(true); 
	//echo("execution start-> " . $time_start);

	use Magento\Framework\App\Bootstrap; 
	require dirname(__DIR__) . '/app/bootstrap.php'; 
	$bootstrap = Bootstrap::create(BP, $_SERVER);

	/* Get Object Manager */
	$objectManager = $bootstrap->getObjectManager();

	/* Set Area Code */
	$objectManager->get('\Magento\Framework\App\State')->setAreaCode('frontend');

	/* Class Instances */
	$storeManager 				= $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
	$resource_connection		= $objectManager->get('Magento\Framework\App\ResourceConnection');
	$crmOrdersLogFactory 		= $objectManager->create('\Apollo\CrmOrders\Model\CrmOrdersLogFactory');
	$orderRepository 			= $objectManager->create('\Magento\Sales\Model\OrderRepository');
	$product 					= $objectManager->create('\Magento\Catalog\Model\Product');
	$categoryFactory 	 		= $objectManager->create('\Magento\Catalog\Model\CategoryFactory');
	
	$orderRepository 	 		= $objectManager->create('\Magento\Sales\Model\OrderRepository');
	$orderInterface 	 		= $objectManager->create('\Magento\Sales\Api\Data\OrderInterface');
	$searchCriteriaBuilder 		= $objectManager->create('\Magento\Framework\Api\SearchCriteriaBuilder');
         
         
	# get connection
	$connection 				= $resource_connection->getConnection();
	# CRM API End point 
	$CRM_API_EndPoint 			= "http://fakeurl.apollopharmacy.org:51/Onlineorder.svc/PLACE_ORDERS";

	/* Declarations */
	$base_url = $storeManager->getStore()->getBaseUrl();
	$admin_username 	= "sujith";
	$admin_password 	= "Retail@123*";
	$admin_token_url 	= "rest/V1/integration/admin/token";
	$admin_credentials 	= [
		"username" => $admin_username, 
		"password" => $admin_password
	];
	$unsuccessful_order_ids = [];
		
	$date = new DateTime();
	$timeStamp = $date->getTimestamp();

	$failed_orders_query 	= "SELECT * FROM `crm_orders_log` where crm_update_status IN ('','failure')";
	$failed_orders_result 	= $connection->fetchall($failed_orders_query);
	$flag=1;
	

	/**	
	 * Get Admin Token
	 * --------------- 
	 */
	$ch = curl_init($base_url.$admin_token_url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($admin_credentials));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		"Content-Type: application/json", 
		"Content-Lenght: " . strlen(json_encode($admin_credentials))
	));
	$admin_token = curl_exec($ch);
	//print_r($admin_token);


	if (count($failed_orders_result) <= 0) {
		# stop if not failed orders
		exit();
	}
	$push_crm_count = 0;
	foreach ($failed_orders_result as $key => $failed_order) 
	{ 
		//$orderObj = $orderInterface->loadByQuoteId($failed_order['quote_id']); 

			
		$order_id = 0;
		$pending_orders  = $orderInterface->getCollection()
            ->addFieldToFilter("quote_id", $failed_order['quote_id']);

        if(sizeof($pending_orders)>0){
	        foreach ($pending_orders as $key => $value) {
	        	if(isset($value['entity_id'])){
	        		$order_id = $value['entity_id'];
	        		if((int)$failed_order['push_count'] >1){
	        			//echo "fdfdffd";
	        			if($failed_order['email_sent']=='sent'){
	        				continue;
	        			}
						require_once 'PHPMailer/PHPMailerAutoload.php';	
						//echo "mail connection";
						 $m = new PHPMailer;
						 $m->isSMTP();
						 $m->SMTPAuth = true;
						 $m->Host = 'smtp.gmail.com';
						$m->Username = 'sales@apollopharmacy.org'; 
						 $m->Password = 'pharmacy@456';
						 $m->SMTPSecure = 'ssl';
						 $m->Port = 465;
						 $m->From = 'sales@apollopharmacy.org';
						$m->FromName = 'Appolo';
						 $m->Subject = "CRM log";
						 $m->Body = 'Order '.$value['increment_id'].'failed to insert more than 2 times.';
						 $m->addReplyTo('sales@apollopharmacy.org','Reply address');
						 $m->addAddress("sales@apollopharmacy.org");
						 $m->addAddress("nagarjuna_r@apollopharmacy.org");
						 $m->send();
						$update = "UPDATE crm_orders_log SET email_sent = 'sent' WHERE id =".$failed_order['id']." AND quote_id = '".$failed_order['quote_id']."'";
						
						$connection->query($update);
	        			//echo "mail sent";
	        			continue;
					} else {
		        		$push_crm_count = $failed_order['push_count'];
		     			$orderObj = $orderRepository->get($order_id);
		        		$ch2 = curl_init($base_url."/rest/V1/orders/".$order_id);  // to display particluar order
						curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
						curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch2, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($admin_token)));
						$resultt = curl_exec($ch2);
						//print_r($resultt);die;
						curl_close($ch2);
						$resultt = json_decode($resultt, 1);
						$sales_order_grid_sql = "SELECT order_types,delivery_info FROM sales_order_grid WHERE entity_id='".$order_id."'";
						$roword=$connection->fetchrow($sales_order_grid_sql);
						$rowcount=count($roword);				
						$orderType = $roword['order_types'];
						$deliveryInfo = $roword['delivery_info'];
						if ($rowcount <= 0) {
							/* test code here */
							$skus = array(); $orderTypes = array();
					        $orderedItems = $orderObj->getAllVisibleItems();
					        foreach ($orderedItems as $orderedItem){  
					            $skus[] = $orderedItem->getSku();
					            $productId = $orderedItem->getProductId();
					            $productData = $product->load($productId);
						        $category_id_ary = $productData->getCategoryIds();	        
						        $category_names_array = array();
						        if(count($category_id_ary)){
						            foreach($category_id_ary as $category_id){
						                $_category = $categoryFactory->create()->load($category_id);
						                $category_names_array[] = $_category->getName();
						            }
						        }
						        $pharma = false;
						        $fmcg = false;
						        $order_type = "";
						        $category_names = array_unique($category_names_array);
						        if(in_array("PHARMA",$category_names))    {
						            $pharma = true; $order_type = "Pharma";
						        }
						        if(in_array("FMCG",$category_names)) {
						            $fmcg = true; $order_type = "Fmcg";
						        }
						        if($pharma == true && $fmcg == true){
						            $order_type = "Both";
						        }
						        /* assing product type for each item */
						        $orderTypes[] = (!empty($order_type) ? $order_type : "Fmcg");
					        }
					        $orderTypes = array_unique($orderTypes);  
					        if(count($orderTypes) > 1){
					            $orderType = "Both";
					        }else if(isset($orderTypes[0])){
					            $orderType = $orderTypes[0];
					        }else{
					            $orderType = "Pharma";
					        }
					        /* final order type and delivery info */
					        $orderType = $orderType;
							$deliveryInfo = '';
						}
						$presimg="";
						$x="";
						$y="";
						$z="";
						$i=0;
						$j=0;
						if($rowcount>=1 && $deliveryInfo!="") {		    
						    $bb=explode('"previous_prescriptions";',$deliveryInfo);
							$regex = '/".*?"/';
							$match=array();		
					   		preg_match_all($regex, $bb[1], $match);			
							if(count($match[0])>=1) {
								for($i=0;$i<count($match[0]);$i++) {
						 	   	    $sqlimg = 'select uploaded_file from ordermedicine_data where id='.$match[0][$i];
						 	       	$rowimg=$connection->fetchrow($sqlimg);
						 	       	$presimg .=$base_url."pub/media/medicine_prescription/".$rowimg['uploaded_file'].",";
						 	    }
							}		
						} else {
							$presimg="";
						}
						$i=0;
						foreach($resultt['items'] as $itms) {
							$dt=explode(" ",$itms['created_at']);
							$dtf=date_create($dt[0]);
							$date=date_format($dtf,"Y-M-d");
						    $x[$i] =array(
						    	"ItemID"=>$itms['sku'],
						    	"ItemName"=>$itms['name'],
						    	"Qty"=>$itms['qty_ordered'],
						    	"orddt"=>$date,
						    	"Price"=>$itms['base_original_price']
						    );
							$i++;
						}
						if ($resultt['payment']['method']=="ccavenue") {
							$z =array(
								"Totalamount"		=> $resultt['base_total_invoiced'],
								"Paymentsource"		=> $resultt['payment']['method'],
								"Paymentstatus"		=> $resultt['payment']['additional_information'][3],
								"Paymentorderid"	=> $resultt['payment']['last_trans_id']
							);
						} else if ($resultt['payment']['method']=="paytm") {
							$z =array(
								"Totalamount"		=> $resultt['payment']['amount_ordered'],
								"Paymentsource"		=> $resultt['payment']['method'],
								"Paymentstatus"		=> "",
								"Paymentorderid"	=> ""
							);
						} else if($resultt['payment']['method']=="healingcard") {
							if (strpos($resultt['status_histories'][0]['comment'],'|')!==false) {
								$hcard=explode("|",$resultt['status_histories'][0]['comment']);			
								if ($hcard[3]!="") {
									$hcardtid=$hcard[3];
								} else {
									$hcardtid="";
								}
								if ($hcard[0]!="" && $hcard[3]!="") {
									$hcardsts=substr($hcard[0],strlen($hcard[0])-8);
								}	
							}
							else {
								$hcardsts=$resultt['status_histories'][0]['comment'];
								$hcardtid="";
							}		
							$z =array(
								"Totalamount"		=> $resultt['payment']['amount_ordered'],
								"Paymentsource"		=> $resultt['payment']['method'],
								"Paymentstatus"		=> $hcardsts,
								"Paymentorderid"	=> $hcardtid
							); 
						} else {
							$z =array(
								"Totalamount"=>$resultt['payment']['amount_ordered'],
								"Paymentsource"=>$resultt['payment']['method'],
								"Paymentstatus"=>'',"Paymentorderid"=>''
							);
						}

						foreach($resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['street'] as $addr) {
							$y.=$addr." ";
						}
						$cust=array(
							"Mobileno"	=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['telephone'],
							"Comm_addr"	=>$y,
							"Del_addr"	=>$y,
							"FirstName"	=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'],
							"LastName"	=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['lastname'],
							"City"		=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['city'],	
							"Postcode"	=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['postcode'],
							"Mailid"	=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['email'],
							"Age"		=>"30",	
							"Cardno"	=>"00",
							"PatientName"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'].$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['lastname']
						);
						$tpdetails=array(
							"Orderid"=>$resultt['increment_id'],
							"Shopid"=>"16001",
							"ShippingMethod"=>$resultt['shipping_description'],
							"PaymentMethod"=>$resultt['payment']['method'],	
							"Vendornanme"=>"online",
							"DotorName"=>"Apollo",
							"url"=>$presimg,
							"Ordertype"=>$orderType,
							"Customerdetails"=>$cust,
							"Paymentdetails"=>$z,
							"itemdetails"=>$x
						);
						$arr=array("tpdetails"=>$tpdetails);

						

						/**
						 *  PUSH details to CRM API ENDPOINT
						 *	--------------------------------
						 */
						$curl=curl_init($CRM_API_EndPoint);
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($arr));
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($curl, CURLOPT_HTTPHEADER, array(
							"Content-Type: application/json", 
							"Content-Lenght: " . strlen(json_encode($arr)))
						);
						$tokenn = curl_exec($curl);
						$res=json_decode($tokenn, TRUE);

				
						/**
						 *  push status  	# check the status if Orders was uploaded successfully to CRM Orders API.
						 *	0 => Success	1 => Failure
						 *
						 *	push_count 		# How many times the order was pushed to CRM Orders.
						 */
						# Get current push count
						// $crm_orders_track_sql = "SELECT push_count FROM `crmorders_track` WHERE entityid = '".$order_id."'";
						// $crmorder_push_count =$connection->fetchrow($crm_orders_track_sql);
						
						# increase the count by one
						$push_count = $push_crm_count + 1;

						/**
						 *	Update CRM ORDERS LOG Table
						 *	-----------------------------
						 *	
						 *	Check respose status and verify orders is pushed or not.
						 *	Set the push status and push count for the order.	 
						 */

						$missing_orders_query 	= "select quote_id,entity_id  FROM sales_order where quote_id='".$failed_order['quote_id']."' and entity_id not in (select order_id from crm_orders_log)";
						$sql='select quote_id,order_id from crm_orders_log where order_id='.$value['entity_id'];
						$missing_orders_result 	= $connection->fetchall($sql);
						$temp=count($missing_orders_result);
						if($temp==0){

									 	 $insert_query = "INSERT INTO crm_orders_log (quote_id, order_id, order_number,payment_status,crm_update_status,push_count)VALUES ('".$value['quote_id']."','".$value['entity_id']."','".$orderObj->getIncrementId()."','failure','failure','1')";


										try {
												$connection->query($insert_query);		
											} 
										catch (Exception $e) {

										}
									}
						else{
								
								   $update = "UPDATE crm_orders_log SET  order_number='".$orderObj->getIncrementId()."', crm_update_status='failure',message='".$res['ordersResult']['Message']."',push_count='".$push_count."' WHERE id ='".$failed_order['id']."' AND quote_id = '".$value['quote_id']."'";
							
										
										try {
											$connection->query($update);
										} catch (Exception $e) {
											
										}

						}
			
			 				
			 
						if ($res['ordersResult']['Status']=="Success" || $res['ordersResult']['Status']=="success") {
							//echo "success";
							# If Upload Success than updated push status to 0.


							$update = "UPDATE crm_orders_log SET order_number='".$orderObj->getIncrementId()."', crm_update_status='success',message='".$res['ordersResult']['Message']."', push_count='".$push_count."' WHERE id =".$failed_order['id']." AND quote_id = '".$value['quote_id']."'";

							try {
								$connection->query($update);		
							} catch (Exception $e) {
								//print("something went wrong => " . $order_id);
							}

						}


							/**
							 *	Store unsuccessful orders id.
							 *  -----------------------------
							 */
							$order_state = ["order_id" => $value['entity_id'],"push_count" => $push_count];
							array_push($unsuccessful_order_ids, $order_state);
						
					}
				}
			}
	    }
	}


	
?>
