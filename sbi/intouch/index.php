<!DOCTYPE HTML>
<html>
	<head>
		<title>Apollo Pharmacy</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body id="top" style="background-color:#fff !important;">

			<!-- Banner -->
		 	<section>
					<div  class="inner"	align="center">
							<span style="padding-top:-10px;padding-right:40% !important;"><img src="images/sbi.png" width="110" alt="" /></span>
							<span width="20%">&nbsp;</span>
							<span style="padding-top:20px;padding-right:1%"><img src="images/apollo.jpg" width="90" height="90" alt="" /></span>
					</div>
					
            </section>                 

 			<!-- Main -->
				<div id="main">
				<div class="inner">

					<!-- Boxes -->
						<div class="thumbnails">

							<div class="box">
								<a href="https://www.apollopharmacy.in" class="image fit"><img src="images/pic01.jpg" alt="" /></a>
								<div class="inner">
									<h3>COPOUN CODE : SBIMED</h3>
									
								</div>
							</div>

							<div class="box">
								<a href="https://www.apollopharmacy.in" class="image fit"><img src="images/pic02.jpg" alt="" /></a>
								<div class="inner">
									<h3>COPOUN CODE : SBIFMCG</h3>									
								</div>
							</div>

							<div class="box">
								<a href="https://www.apollopharmacy.in" class="image fit"><img src="images/pic03.jpg" alt="" /></a>
								<div class="inner">
									<h3>COPOUN CODE : SBIAP</h3>
									
								</div>
							</div>							

						</div>

					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<div class="inner">
						<h2>Terms & Conditions</h2>
						<p>1. That you will use the services provided by Apollopharmacy.in, its affiliates, consultants and contracted companies, for lawful purposes only and comply with all applicable laws and regulations while using the Site and transacting on the Site.</p>
						<p align="justify">2. You will provide true, accurate, complete and current information in all instances where such information is requested of you. Apollopharmacy.in reserves the right to confirm and validate the information and other details provided by you at any point of time. If upon confirmation your details are found not to be true (wholly or partly), Apollopharmacy.in has the right in its sole discretion to reject the registration and debar you from using the Services of Apollopharmacy.in and / or other affiliated websites without prior intimation whatsoever.</p>
						<p>3. To compensate Apollopharmacy.in for any extra cost incurred for redelivery in the event of a non-delivery in the first instance on account of a mistake by you (i.e. wrong name or address or any other wrong information).</p>
						<p>4. That you using your best and prudent judgment before entering into any transaction through this Site doing so at your sole risk.</p>
						<p>5. Offer valid on selected products only.</p>
						<p>6. Offers/coupons cannot be clubbed together.</p>
						<p>7. Expiry: Medicines supplied will have sufficient expiry period.</p>
						<p>8. No Return: We have a no return policy of products.</p>
						<p>9. Only one discount shall be offered to client -in a single/one transaction.</p>
						<p>10. Prescription - The customer will have to show valid prescription at the time of delivery of the medicines.</p>
						<p>11. For any questions/queries please call Apollo Toll - 1860 500 0101.</p>
						<p>12. The Offer given is non transferable, non binding & non en cashable.</p>
						<p>13. Valid for usage through SBI Marketplace only.</p>
						<p>14. Other terms & conditions apply.


						<h3>Shipping</h3>
						<p><b>How much are the delivery charges?</b></p>
						<p>Apollo Pharmacy provides free delivery on all items if your total order amount is Rs. 600/- or more. Rs. 55/- is charge as delivery charges if the order value is less than Rs 600.</p>
						<p>For Kerala & MP, delivery charges are applicable as per actual.</p>
						<p><b>What is the estimated delivery time?</b></p>
						<p align="justify">Order confirmation happens for online payments once payment authorization and verification is obtained. For COD, order is confirmed once our customer service representative verifies your order and shipping address. For other Offline payments, order confirmation happens once the payment is cleared.</p>
						<p>We will inform you once the order is confirmed and when the items are shipped with an update on the approximate delivery time</p>
						<p><u>For Metros</u></p>
						<p>For all in stock items the delivery is within 3 to 8 business days of the order being confirmed.</p>
						<p><u>For Non Metros:</u></p>
						<p>All products will reach you within a time span of 3 to 12 days. For certain remote geographies the time for deliveries may exceed 12 days.</p>
						<p align="justify">We make our best efforts to ship each item in your order within 3 working days of the order. However in some cases, we may take longer, up to 7 working days, to ship the order as we may have to procure it from some other stores or our suppliers. In the unlikely event that we are not able to ship your order completely within 7 days of the order, we shall cancel the remaining unshipped part of the order, and send you an email informing you about the same. In such cases, your payment against the unshipped part of the order shall be refunded, in the manner you have made the payment. </p>


						<p><b>Do you deliver internationally?</b></p> 
						<p>International delivery is not available currently. You can order items from anywhere in the world, but the shipping address has to be in India.</p>
						<p><b>Pricing Information</b></p>
						<p align="justify">While Apollo Pharmacy shall endeavor to provide accurate product and pricing information, yet typographical errors might occur. Apollo pharmacy cannot confirm the price of a product until after you order. In the event that a product is listed at an incorrect price or with incorrect information due to an error in pricing or product information, Apollo Pharmacy shall have the right, at our sole discretion, to refuse or cancel any orders placed for that product, unless the product has already been dispatched. In the event that an item is mis-priced, Apollo Pharmacy may, at its discretion, either contact you for instructions or cancel your order and notify you of such cancellation. Unless the product ordered by you has been dispatched, your offer will not be deemed accepted and Apollo Pharmacy will have the right to modify the price of the product and contact you for further instructions using the e-mail address/ contact no. provided by you during the time of registration, or cancel the order and notify you of such cancellation.</p>
						<p><b>Cancellation by Apollopharmacy.in</b></p>
						<p align="justify">Please note that there may be certain orders that we are unable to accept and must cancel. We reserve the right, at our sole discretion, to refuse or cancel any order for any reason. Some situations that may result in your order being cancelled include limitations on quantities available for purchase, inaccuracies or errors in product or pricing information, or problems identified by our credit and fraud avoidance department. We may also require additional verifications or information before accepting any order. We will contact you if all or any portion of your order is cancelled or if additional information is required to accept your order. If your order is cancelled after your account has been charged, the said amount will be reversed back in your account.</p>
						<p><b>Cancellations by the Customer</b></p>
						<p align="justify">In case we receive a cancellation notice and the order has not been shipped by us, we shall cancel the order and refund the entire amount. The orders that have already been shipped out by us will not be cancelled and you will have to check our return policy on those orders. The customer agrees not to dispute the decision made by Apollopharmacy.in and accept Apollopharmacy.in's decision regarding the cancellation.</p>

						<h3>Cancellation and Returns</h3>
						<p><b>Cancellation</b></p>  
						<p align="justify">An order cannot be cancelled once the payment is complete. Only exception will be inventory mismatch at our end. If you end up buying a product that is out of stock we will cancel and refund complete payment. Return of payment will take 7 working days once we receive the bank account details from the buyer.</p> 
						<p><b>Return Policy </b></p>
						<p align="justify">We replace the product only if it is damaged during shipping.It is subject to the recipient mailing us the photo within 8 hours of delivery. In case the same product is not available for shipment we mail a "Credit Note" and you can redeem it against any of our products. Credit note does not have any expiry.</p> 
						<p><b>Exchange</b></p>  
						<p>There is no exchange policy. A creation once sold will not be exchanged. </p>
						<p><b>Refund Policy</b></p>                       
						<p align="justify">Payment will be refunded in case there is an inventory mismatch from our side. In no other condition there will be refund. Refund will be to same debit/credit card/account ( payment method) from which original payment was made. Time duration for refund is 7 working days.</p>
						<p><b>Medicine Delivery Policy</b></p>
						<p>Important Instructions: </p>
						<p>1. Valid prescription from a Doctor is mandatory for all medicine orders </p>
						<p>2. Kindly display the original prescription at the time of picking up medicines </p>
						<p>3. We will not be able to process the orders without a valid prescription</p> 
						<p>4. Please show prescription at the time of delivery</p>
						<p>Note: </p>
						<p>1. You are required to send a scan copy of valid prescription from an authorized Doctor which is mandatory </p>
						<p>2. No medicine will be delivered if the order amount is below Rs. 200 /-</p>
						<p>3. Payment method only COD (Cash on Delivery) is available depending on the pin code. </p>
						<p>4. For all in stock items the delivery time is within 24 to 48 hours of order being confirmed subject to availability of Apollo Pharmacy store near the delivery address</p>

					</div>
				</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
<!-- 			<script src="assets/js/skel.min.js"></script>
 -->			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>