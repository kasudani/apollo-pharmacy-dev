<!DOCTYPE HTML>
<html>
	<head>
		<title>Apollo Pharmacy</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body id="top" style="background-color:#fff !important;">

			<!-- Banner -->
		 	<section>
					<div  class="inner"	align="center">
							<span style="padding-top:-10px;padding-right:40% !important;"><img src="images/sbi.png" width="110" alt="" /></span>
							<span width="20%">&nbsp;</span>
							<span style="padding-top:20px;padding-right:1%"><img src="images/apollo.jpg" width="90" height="90" alt="" /></span>
					</div>
					
            </section>                 

 			<!-- Main -->
				<div id="main">
				<div class="inner">

					<!-- Boxes -->
						<div class="thumbnails">

							<div class="box">
								<a href="https://www.apollopharmacy.in" class="image fit"><img src="images/pic01.jpg" alt="" /></a>
								<div class="inner">
									<h3>COPOUN CODE : SBIMED</h3>
									
								</div>
							</div>

							<div class="box">
								<a href="https://www.apollopharmacy.in" class="image fit"><img src="images/pic02.jpg" alt="" /></a>
								<div class="inner">
									<h3>COPOUN CODE : SBIFMCG</h3>									
								</div>
							</div>

							<div class="box">
								<a href="https://www.apollopharmacy.in" class="image fit"><img src="images/pic03.jpg" alt="" /></a>
								<div class="inner">
									<h3>COPOUN CODE : SBIAP</h3>
									
								</div>
							</div>							

						</div>

					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<div class="inner">
						<h2>Terms & Conditions</h2>
						<p>1. That you will use the services provided by Apollopharmacy.in, its affiliates, consultants and contracted companies, for lawful purposes only and comply with all applicable laws and regulations while using the Site and transacting on the Site.</p>
						<p>2. You will provide true, accurate, complete and current information in all instances where such information is requested of you. Apollopharmacy.in reserves the right to confirm and validate the information and other details provided by you at any point of time. If upon confirmation your details are found not to be true (wholly or partly), Apollopharmacy.in has the right in its sole discretion to reject the registration and debar you from using the Services of Apollopharmacy.in and / or other affiliated websites without prior intimation whatsoever.</p>
						<p>3. To compensate Apollopharmacy.in for any extra cost incurred for redelivery in the event of a non-delivery in the first instance on account of a mistake by you (i.e. wrong name or address or any other wrong information).</p>
						<p>4. That you using your best and prudent judgment before entering into any transaction through this Site doing so at your sole risk.</p>
						<p>5. Offer valid on selected products only.</p>
						<p>6. Offers/coupons cannot be clubbed together.</p>
						<p>7. Expiry: Medicines supplied will have sufficient expiry period.</p>
						<p>8. No Return: We have a no return policy of products.</p>
						<p>9. Only one discount shall be offered to client -in a single/one transaction.</p>
						<p>10. Prescription - The customer will have to show valid prescription at the time of delivery of the medicines.</p>
						<p>11. For any questions/queries please call Apollo Toll - 1860 500 0101.</p>
						<p>12. The Offer given is non transferable, non binding & non en cashable.</p>
						<p>13. Valid for usage through SBI Marketplace only.</p>
						<p>14. Other terms & conditions apply.
					</div>
				</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
<!-- 			<script src="assets/js/skel.min.js"></script>
 -->			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>