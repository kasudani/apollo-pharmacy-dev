<?php
ini_set('display_errors',1);
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';
$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$count = 100;
$page_id = (isset($_REQUEST['page']) ? $_REQUEST['page'] : 1);
$orderCollectionFactory = $obj->get('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
$orders = $orderCollectionFactory->create()->addFieldToSelect('*')
                        ->setOrder('created_at','desc')
                        ->setPageSize($count)
                        ->setCurPage($page_id);

//echo "<pre>";print_R($orders->getData());die;
foreach ($orders as $order){
    $orderId = $order->getId();
    $phone = getPhone($order);
    $SkusAndOrdertypes = getSkusAndOrdertypes($order,$obj);
    $skus = $SkusAndOrdertypes['skus'];
    $orderTypes = $SkusAndOrdertypes['order_types'];
    $orderInfo['order_types'] = $orderTypes;
    $orderInfo['order_skus'] = $skus;
    $orderInfo['order_mobile'] = $phone;
    //echo $orderId;
    //print_r($orderInfo);die;
    updateOrderInf($orderId,$orderInfo,$obj);
}
echo "Updated";die;

function getSkusAndOrdertypes($order,$obj){
    $skus = array(); $orderTypes = array();
    $orderedItems = $order->getAllVisibleItems();
    foreach ($orderedItems as $orderedItem){
        $skus[] = $orderedItem->getSku();
        $productId = $orderedItem->getProductId();
        $orderTypes[] = getOrderTypes($productId,$obj);
    }
    $orderTypes = array_unique($orderTypes);
    if(count($orderTypes) > 1){
        $orderType = "Both";
    }else if(isset($orderTypes[0])){
        $orderType = $orderTypes[0];
    }else{
        $orderType = "Pharma";
    }
    return array('skus' => implode(", ",$skus),'order_types' => $orderType);
}

function getPhone($order){
    $billingAddress = $order->getBillingAddress();
    return $billingAddress->getTelephone();
}

function getOrderTypes($productId,$obj){
    $productData = $obj->create('Magento\Catalog\Model\Product')->load($productId);
    $categoryFactory = $obj->create('Magento\Catalog\Model\CategoryFactory');
    $category_id_ary = $productData->getCategoryIds();
    $category_names_array = array();
    if(count($category_id_ary)){
        foreach($category_id_ary as $category_id){
            $_category = $categoryFactory->create()->load($category_id);
            $category_names_array[] = $_category->getName();
        }
    }
    $pharma = false;
    $fmcg = false;
    $order_type = "";
    $category_names = array_unique($category_names_array);

    if(in_array("PHARMA",$category_names))    {
        $pharma = true; $order_type = "Pharma";
    }
    if(in_array("FMCG",$category_names)) {
        $fmcg = true; $order_type = "Fmcg";
    }

    if($pharma == true && $fmcg == true){
        $order_type = "Both";
    }
    return (!empty($order_type) ? $order_type : "Fmcg");
}

function updateOrderInf($orderId,$orderInfo,$obj){
    $resource = $obj->get('Magento\Framework\App\ResourceConnection');
    $connection = $resource->getConnection();
    $tableName = $resource->getTableName('sales_order_grid');
    $sql = "Update  $tableName Set order_types = '".$orderInfo['order_types']."',order_skus = '".$orderInfo['order_skus']."',
                            order_mobile = '".$orderInfo['order_mobile']."' where entity_id = $orderId";
    $connection->query($sql);
    return true;
}

?>