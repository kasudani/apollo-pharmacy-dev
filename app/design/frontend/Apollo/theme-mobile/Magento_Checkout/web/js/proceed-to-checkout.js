/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

 define([
         'jquery',
         'Magento_Customer/js/model/authentication-popup',
         'Magento_Customer/js/customer-data'
     ],
     function ($, authenticationPopup, customerData) {
         'use strict';

         return function (config, element) {
             $(element).click(function (event) {
                 var cart = customerData.get('cart'),
                     customer = customerData.get('customer'),
                     selectShippingUrl = $('#selectShippingUrl').val();


                 event.preventDefault();
                 /*alert('hi');

                 alert($('input[name=selectDeliveryOption]:checked').val());
                 alert($('input[name=address]:checked').val());*/
                 var deliveryOption = $('input[name=selectDeliveryOption]:checked').val();

                 if(deliveryOption == 'homeDelivery'){
                 	var addressId = $('input[name=address]:checked').val();
                 	if(!addressId){
                 		$("#selectAddressError").show();
                 		setTimeout(function(){ $("#selectAddressError").hide(); }, 10000);
                 		return false;
                 	}
                 } else if(deliveryOption == 'storeDelivery'){
                 	var storeId = $('#store_available').val();
                 	if(!storeId){
                 		$("#selectStoreError").show();
                 		setTimeout(function(){ $("#selectStoreError").hide(); }, 10000);
                 		return false;
                 	}
                 }


                 var data = {};
			      data["deliveryOption"] = deliveryOption;
			      data["address"] = addressId;
			      data["store"] = storeId;
			      $.ajax({ 
			            url: selectShippingUrl,
			            type: 'POST',
			            data: data,
			            showLoader: true
			        }).done(function (response) {
			          console.log(response);
			          if(response=='Done'){
			          	location.href = config.checkoutUrl;
			          }else{
			          	$("#selectAddressError").show();
                 		setTimeout(function(){ $("#selectAddressError").hide(); }, 10000);
			          }
			         
			        }).fail(function () {
			           console.log("Error");
			           showLoader: false;
			        }); 


                 /*if (!customer().firstname && cart().isGuestCheckoutAllowed === false) {
                     authenticationPopup.showModal();

                     return false;
                 }*/
                 //location.href = config.checkoutUrl;
             });

         };
     }
 );
