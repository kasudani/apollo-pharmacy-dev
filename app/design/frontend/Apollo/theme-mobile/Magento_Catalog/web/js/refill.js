require([ 'jquery', 'mage/url'], function($,url){

   jQuery(document).ready(function(){
    jQuery("#show").click(function(){
        jQuery("#refill_reminder").toggle();
    });
    jQuery("#refill-reminder-popup").click(function(){
     jQuery(".social-login").trigger('click');
    });
    jQuery( "#refill-submit" ).click(function() {
    	  var custom_url = url.build('reminder/index/index');  
                                var qty=jQuery("#refill_reminder #refill-qty").val();
                                var fre= jQuery("#refill_reminder #refill-fre").val();

                                var product_id= jQuery("#refill_reminder #refill-pid").val();
                       
					            if(qty!='' && fre!='' && !isNaN(qty))
					            {
								  $.ajax({
									url : custom_url,
									type: 'POST',
									dataType: "json",
									data:{
										 qty: qty,
										 fre:  fre,
										 product_id: product_id,
										 },
									success : function(data) 
									{ 
									 
	                                    if(data==1)
										{ 	    
                                         
								    jQuery("#refill_reminder #error-qty").css("display","none");
									jQuery("#refill_reminder #succ-qty").css("display","block");									
				                        							
					                    }
				                    }
								  });
							}
							else{
								    jQuery("#refill_reminder #error-qty").css("display","block");
									jQuery("#refill_reminder #succ-qty").css("display","none");
							}
                        });
});	
})
