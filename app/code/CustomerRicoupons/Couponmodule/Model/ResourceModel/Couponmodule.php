<?php
namespace CustomerRicoupons\Couponmodule\Model\ResourceModel;

class Couponmodule extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_custom_coupons', 'id');
    }
}
?>