<?php

namespace CustomerRicoupons\Couponmodule\Block\Adminhtml\Couponmodule\Edit\Tab;

/**
 * Couponmodule edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \CustomerRicoupons\Couponmodule\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \CustomerRicoupons\Couponmodule\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \CustomerRicoupons\Couponmodule\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('couponmodule');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

		
        $fieldset->addField(
            'coupon_code',
            'text',
            [
                'name' => 'coupon_code',
                'label' => __('Coupon Code'),
                'title' => __('Coupon Code'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'coupon_description',
            'text',
            [
                'name' => 'coupon_description',
                'label' => __('Coupon Description'),
                'title' => __('Coupon Description'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'coupon_name',
            'text',
            [
                'name' => 'coupon_name',
                'label' => __('Coupon Name'),
                'title' => __('Coupon Name'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'product_skus',
            'textarea',
            [
                'name' => 'product_skus',
                'label' => __('Product skus'),
                'title' => __('Product skus'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'discount_percent',
            'text',
            [
                'name' => 'discount_percent',
                'label' => __('Discount Percent'),
                'title' => __('Discount Percent'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'from_date',
            'date',
            [
                'name' => 'from_date',
                'label' => __('From Date'),
                'title' => __('To Date'),
                'required' => true,
                'disabled' => $isElementDisabled,
                  'date_format' => \Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT,
              'time_format' => 'HH:mm:ss'
            ]
        );
        $fieldset->addField(
            'to_date',
            'date',
            [
                'name' => 'to_date',
                'label' => __('To Date'),
                'title' => __('To Date'),
                'required' => true,
                'disabled' => $isElementDisabled,
                'date_format' => \Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT,
              'time_format' => 'HH:mm:ss'
            ]
        );
             $fieldset->addField(
            'uses_per_cpn',
            'text',
            [
                'name' => 'uses_per_cpn',
                'label' => __('Uses Per Coupon'),
                'title' => __('Uses Per Coupon'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
                 $fieldset->addField(
            'uses_per_cust',
            'text',
            [
                'name' => 'uses_per_cust',
                'label' => __('Uses Per Customer'),
                'title' => __('Uses Per Customer'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);
		
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
