<?php
namespace CustomerRicoupons\Couponmodule\Block\Adminhtml\Couponmodule\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('couponmodule_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Coupon Information'));
    }
}