<?php
namespace CustomerRicoupons\Couponmodule\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class UpgradeSchema implements  UpgradeSchemaInterface
{
public function upgrade(
	SchemaSetupInterface $setup,
ModuleContextInterface $context)
{
$setup->startSetup();
// Get module table
$tableName = $setup->getTable('customer_custom_coupons');
// Check if the table already exists
if ($setup->getConnection()->isTableExists($tableName) == true) {
// Declare data
$columns = [
'from_date' => [
'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
'nullable' => true,
'comment' => 'From Date',
],
'to_date' => [
'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
'nullable' => true,
'comment' => 'To Date',
],
];
$connection = $setup->getConnection();
foreach ($columns as $name => $definition) {
$connection->addColumn($tableName, $name, $definition);
}
} 
$setup->endSetup();

}

}