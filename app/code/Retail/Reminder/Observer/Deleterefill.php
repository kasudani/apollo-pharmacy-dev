<?php
namespace Retail\Reminder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface; 

class Deleterefill implements ObserverInterface
{
protected $_session;	
protected $_reminderFactory; 
protected $_coreSession;
	
	
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\Session\SessionManagerInterface $coreSession
		) {
			
				$this->_coreSession = $coreSession; 
			$this->_session = $session;
			$this->_reminderFactory = $cu;
			

		  }
	public function setRefillReminder($value)
	{
    $this->_coreSession->start();
    $this->_coreSession->setRefillReminder($value);
     }
	 public function getRefillReminder()
	 {
    $this->_coreSession->start();
    return $this->_coreSession->getRefillReminder();
     }
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
	 
			  $quoteItem = $observer->getQuoteItem();
			  $id = $quoteItem->getProduct()->getId();
			   try{
		  
						   $model=$this->_reminderFactory->create();
							   $collection=$model->getCollection();
							   $collection->addFieldToFilter('product_id',$id);
							   $collection->addFieldToFilter('customer_id',$this->_session->getCustomer()->getId());
							   $collection->addFieldToFilter('start_date',array('null' => true));
							   $collection->addFieldToFilter('notify_date',array('null' => true));
							   $result=$collection->getData();

							  if(!empty($result))
							  { 
								$model->load($result[0]['id'],'id');
								 $model->delete();
							  }								 
				} 
		   
	          catch(\Exception $e)
				{
					  echo $e->getMessage();
				}
	 } 

}