<?php
    /**
     * Webkul Hello CustomPrice Observer
     *
     * @category    Webkul
     * @package     Webkul_Hello
     * @author      Webkul Software Private Limited
     *
     */
    namespace Retail\Reminder\Observer;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\RequestInterface;
 
    class Reminder implements ObserverInterface
    {
protected $_session;
		
    protected $_reminderFactory; 
	protected $_coreSession;
	protected $category;
	protected $_carttotaldisc;

	protected $_categoryFactory;
	
	public function __construct(
	\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\Session\SessionManagerInterface $coreSession,
	\Magento\Catalog\Model\CategoryFactory $category,
	\Magento\Catalog\Model\CategoryFactory $categoryFactory
		) {
			   $this->_categoryFactory = $categoryFactory;
			   $this->category=$category;
				$this->_coreSession = $coreSession; 
			$this->_session = $session;
			$this->_reminderFactory = $cu;
			

		  }
		public function execute(\Magento\Framework\Event\Observer $observer) 
		{
			
            $item = $observer->getEvent()->getData('quote_item');
                  $catids=$item->getProduct()->getCategoryIds();

				    
	            try{ 	
				 if($item->getProduct()->getId())
					{
					$model = $this->_reminderFactory->create();
						  $collection=$model->getCollection();
						  $collection->addFieldToFilter('product_id',$item->getProduct()->getId());
						  
							$res=$collection->getData(); 

							 if(!empty($res))
							 {				 
							$model->load($res[0]['id']);
							$model->setData('is_in_cart',1);
							$model->save(); 
							$catcollection = $this->_categoryFactory->create()->getCollection();
							$catcollection->addAttributeToFilter('entity_id',array('in'=>$catids));
							$catcollection->addAttributeToSelect('refill_discountnew');
							$catcollection->setOrder('refill_discountnew','DESC');
							$catcollection = $catcollection->getFirstItem();
						
							if($this->_session->getCustomer()->getId()!='' || $observer->getEvent()->getData('customer_id')!='')
							{
							
						$final_price=$item->getProduct()->getFinalPrice();			 
							$discount=$catcollection->getData('refill_discountnew');
							
						$discount_amount= ($final_price*$discount)/100;
						$final_price=$final_price-$discount_amount;
						
						$item->setCustomPrice($final_price);
						$item->setOriginalCustomPrice($final_price);
						$item->getProduct()->setIsSuperMode(true);
						  
						}
						
						   
						   
						    
							 }
					}
		        }
				 catch(\Exception $e)
				 {
					  echo $e->getMessage();
				 }	 
		
		}
    }
