<?php

namespace Retail\Reminder\Controller\Customer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Refill extends \Magento\Framework\App\Action\Action
{
protected $_session;

    protected $_reminderFactory; 
	 protected $_resultPageFactory;
public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	PageFactory $resultPageFactory
) {
    $this->_session = $session;
    $this->_reminderFactory = $cu;
	$this->_resultPageFactory = $resultPageFactory;
    parent::__construct($context);

  }
    public function execute()
    {
        

        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Refill Reminder'));
        $this->_view->renderLayout();
		
    }

}