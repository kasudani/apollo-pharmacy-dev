<?php
namespace Retail\Reminder\Controller\Cronjob;
class Refillcron extends \Magento\Framework\App\Action\Action
{
    protected $_messageManager;
    protected $cart;
    protected $product;
    protected $resultPageFactory;
    protected $customerRepositoryInterface;
    protected $quoteModel;
    protected $productRepository;
    protected $cartManagementInterface;
    protected $cartRepositoryInterface;
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context, 
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
       \Retail\RefillReminder\Model\ReminderFactory $cu,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Quote\Model\Quote $quoteModel,
         \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
         \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\UrlInterface $urlInterface, 
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Checkout\Model\Cart $cart) {
        parent::__construct($context);
		
			$this->_reminderFactory = $cu;
        $this->_messageManager = $messageManager;
        $this->storeManager = $storeManager;
        $this->resultPageFactory = $resultPageFactory;
        $this->cart = $cart;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->quoteModel = $quoteModel;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
         $this->_urlInterface = $urlInterface;
         $this->quoteFactory = $quoteFactory;
    }
    public function execute()
    {
		
        $model = $this->_reminderFactory->create();
              $collection=$model->getCollection();
			  $collection->addFieldToSelect('customer_id');
			  $collection->getSelect()->group('customer_id'); 
        $collection->addFieldToFilter('flag', array('neq' => '10'));
      
            foreach($collection->getData() as $coll)
				{


		             			
					$customer= $this->_customerRepositoryInterface->getById($coll['customer_id']);
                  $collectionnew=$model->getCollection();
                  $collectionnew->addFieldToFilter('customer_id',$coll['customer_id']);
		          $collectionnew->addFieldToFilter('flag', array('neq' => '10'));
				  $collectionnew->addFieldToFilter('notify_date',date("Y-m-d"));
                   $customer= $this->_customerRepositoryInterface->getById($coll['customer_id']);
                        		 		  
             
                 if(!empty($collectionnew->getData()))
                   {
                        
                    $cartId = $this->cartManagementInterface->createEmptyCart(); 
                    $quote = $this->cartRepositoryInterface->get($cartId); 
                    $quote->setStoreId($this->storeManager->getStore()->getId());
                    $quote->setCurrency();
                    $quote->assignCustomer($customer);
                 
                               foreach($collectionnew->getData() as $data)
        				       {  
                                $params=array();
        						$params['qty'] = 1;//product quantity
        						
        						$_product = $this->productRepository->getById($data['product_id']);
        						$quoteItem=$quote->addProduct($_product,$data['quantity']);
        				        $this->_eventManager->dispatch('checkout_cart_product_add_after', ['quote_item' => $quoteItem, 'product' => $_product,'customer_id' =>$data['customer_id']]);   
                                $flag=$data['flag']; 
                                if($flag==0 || $flag==5)
                             {
                               $refill_reminder="Reminder will expire in 5days";

                             } 
                         if($flag==4)
                         {
                           $refill_reminder="Reminder will expire today";
                         }
                         if($flag==1)
                         {
                           $refill_reminder="Reminder will expire in 3days";
                         }  
                         if($flag==2)
                         {
                           $refill_reminder="Reminder will expire in 2days";
                         } 
                         if($flag==3)
                         {
                           $refill_reminder="Reminder will expire in 1days";
                         }       
        $store = $this->_storeManager->getStore()->getId();

            $url=$this->_urlInterface->getBaseUrl();
        $transport = $this->_transportBuilder->setTemplateIdentifier(10)
            ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
            ->setTemplateVars(
                [
                    'store' => $this->_storeManager->getStore(),
                    'checkout_url'=>$url."checkout/cart",
                    'product_name'=>$_product->getName(),
                    'refill_reminder'=>$refill_reminder,
                ]
            )
            ->setFrom('general')
            // you can config general email address in Store -> Configuration -> General -> Store Email Addresses
            ->addTo('varun@theretailinsights.com', 'Siva Varun')
            ->getTransport();
        $transport->sendMessage(); 
                              }

                             $quote->setTotalsCollectedFlag(false);
                             $quote->collectTotals()->save();       
                    }
           
                            
       }

       //magento code to send mail to customer
  }
 }	 
