<?php
namespace Retail\Subscribe\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Retail\Subscribe\Helper\CalculateDiscount;
 
class CartItemUpdateComplete implements ObserverInterface
{
      
    protected $_calculateDiscount;
    
    public function __construct(CalculateDiscount $calculateDiscount) {
        $this->_calculateDiscount = $calculateDiscount;
    } 
    
    public function execute(\Magento\Framework\Event\Observer $observer) {
        // $quoteItem = $observer->getEvent()->getData('item');

        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/cartUpdateItem.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        /* Code here */
        $logger->info(__CLASS__);
        $logger->info(__FUNCTION__);
        // $logger->info($quoteItem);

    }
}
?>