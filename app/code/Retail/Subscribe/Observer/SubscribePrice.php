<?php
namespace Retail\Subscribe\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Bootstrap;
use \Magento\Framework\App\Config\ScopeConfigInterface;
 
class SubscribePrice implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager; 

    protected $_subscribeFactory;  
    
    protected $_calculateDiscount;

    protected $_customerSession;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Retail\Subscribe\Model\SubscribeFactory $cu,
        \Retail\Subscribe\Helper\CalculateDiscount $calculateDiscount,
        ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_subscribeFactory = $cu;
        $this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
        $this->_calculateDiscount = $calculateDiscount;
        $this->_customerSession = $customerSession;
    }
 
    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {  
        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/subscriber.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        /* Code here */
        $logger->info(__CLASS__);

        # call the calculate helper once here and than update the dicounted price whereever necessary.
        $quoteItem = $observer->getEvent()->getData('quote_item');
        $productObject = $quoteItem->getProduct();
        $pro_id = $productObject->getId();
        $pro_qty = $quoteItem->getQty();
        $isQuantityPerDiscountInUse = $productObject->getUseQuantityPerDiscount();

        # discounted price value inital set to null
        $discountedPrice = '';

        # logging
        $logger->info("isQuantityPerDicountInUse ==> " . $isQuantityPerDiscountInUse);
        $logger->info("product id ==> " . $pro_id);
        $logger->info("product qty ==> " . $quoteItem->getQty());

        # get discount calculated
        $logger->info("called calculateDiscount helper...");
        # if not healing card product
        if($pro_id != "53923") {
            $discountedPrice = $this->_calculateDiscount->calculate($pro_id,$pro_qty);
            $logger->info("calculateDiscount helper returned");
            $logger->info("Final Discount value ==> ");
        }
        
        

        // $customerSession = $this->_objectManager->create('Magento\Customer\Model\Session');
        if($this->_customerSession->isLoggedIn()) 
        {
            $logger->info("customer is logged in...");
            $cust_id = $this->_customerSession->getCustomer()->getId();
            $model      = $this->_subscribeFactory->create();
            $collection = $model->getCollection();
            // $item = $observer->getEvent()->getData('quote_item');
            $_product = $quoteItem->getProduct();
            $collection->addFieldToFilter('customer_id', $cust_id);
            $collection->addFieldToFilter('product_id', $_product->getId());
            $collection->addFieldToFilter('flag', 0);
            if (!empty($collection->getData())) 
            {
                foreach ($collection as $rtu) {
                $model->load($rtu['id'], 'id');
                $dis = $model->getDiscount();
                }
                $custom_price = $quoteItem->getCustomPrice();
                if($custom_price == ""){
                    $custom_price = $_product->getFinalPrice();
                }
                $setting_price = ($custom_price*$dis)/100;
                $setting_price = $custom_price - $setting_price;

                // calulate discounted price if applide and update the price here
               
                $logger->info("If customer logged in and macthed condition...");
                $logger->info("Final Discount value ==> ");
                if($discountedPrice != '') {
                    $setting_price = $discountedPrice['final_price'];
                }

                $quoteItem->setCustomPrice($setting_price);
                $quoteItem->setOriginalCustomPrice($setting_price);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }
            else {
                # if not in subscribe logic
                
                $logger->info("If customer logged in and do not macthes condition...");
                $logger->info("Final Discount value ==> ");
                if($discountedPrice != '') {
                    $quoteItem->setCustomPrice($discountedPrice['final_price']);
                    $quoteItem->setOriginalCustomPrice($discountedPrice['final_price']);
                    $quoteItem->getProduct()->setIsSuperMode(true);
                }
            }
        }
        else {
            # if user not logged in
            
            $logger->info("If customer not logged in...");
            $logger->info("Final Discount value ==> ");
            if($discountedPrice != '') {
                $logger->info($discountedPrice);
                # update price
                $quoteItem->setCustomPrice($discountedPrice['final_price']);
                $quoteItem->setOriginalCustomPrice($discountedPrice['final_price']);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }
            else{
                $logger->info("cannot apply discount..");
            } 

        }   
    }
}
?>