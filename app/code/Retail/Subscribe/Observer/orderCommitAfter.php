<?php
namespace Retail\Subscribe\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Bootstrap;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order;
 
class orderCommitAfter implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    protected $storeManager;
    protected $order;  
    protected $_transportBuilder;
    protected $_subscribeFactory;  
    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Retail\Subscribe\Model\SubscribeFactory $cu,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager      = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->_subscribeFactory = $cu;
        $this->order = $order;
        $this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
    }
 
    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {  
        $current_date = date("Y/m/d");
        $model = $this->_subscribeFactory->create();
        $collection = $model->getCollection();
        $blockIns = $this->_objectManager->get('Retail\Subscribe\Block\Rewrite\Customer\Account\Customer');
        $cust_id = $blockIns->getLoggedinCustomerId();
        $blockInstance = $this->_objectManager->get('Retail\Subscribe\Block\Index\Index');
        $orderId = $observer->getEvent()->getOrderIds();
        $order = $this->order->load($orderId);
        $orderItems = $order->getAllItems();
        foreach ($orderItems as $item) {
            $product_id = $item->getProductId();
            $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
            $collection->addFieldToFilter('customer_id', $cust_id);
            // $collection->addFieldToFilter('product_id', $product_id);
            $collection->addFieldToFilter('flag', 0);
            foreach($collection as $rtu){
            $end_date = date('Y/m/d', strtotime("+".$rtu['sub_period']." months", strtotime($current_date)));
            $model->load($rtu['id'],'id');
            $model->setFlag(1);
            $model->setData('start_date',$current_date);
            $model->setData('end_date',$end_date);
            $model->save();

            $transport = $this->_transportBuilder->setTemplateIdentifier(5)
                ->setTemplateOptions(['area' => 'frontend', 'store' =>1])
                ->setTemplateVars(
                    [
                         'product_name'=>$product->getName(),
                         'base_url'=>"baseUrl",
                         'checkout_url'=>"checkout/cart"
                    ]
                )
                ->setFrom('general')
                ->addTo('manish@theretailinsights.com')
                ->getTransport();
                $transport->sendMessage();
            }
        }
    }
}