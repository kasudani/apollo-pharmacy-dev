<?php
namespace Retail\Subscribe\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesEventQuoteSubmitBefor implements ObserverInterface
{
    /**
     * Set gift messages to order from quote address
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $observer->getEvent()->getOrder()->setGuestVerification($observer->getEvent()->getQuote()->getGuestVerification());

        return $this;
    }
}
