require([ 'jquery', 'jquery/ui'], function($){

    var count=0;

    function getRatingsHtml(low, high, count)
    {
        var html = '';

        html += '<span id="rating-changed" class="rating-changed" style="display:none;">yes</span>';
        html += '<input class="rating-changed-input" style="display:none;" value="yes"/>';
        html += '<div class="rating-summary" style="display: inline-block;margin-top: -5px;">';
          html += '<div class="rating-result" title="'+low+'% - '+high+'%">';
            html += '<span style="width:'+high+'%; display: block !important; margin-top: -20px; overflow: hidden; float:left;" class="rating_orange_star"></span>';
          html += '</div>';
        html += '</div>';
        html += '<span class="count">'+count+'</span>';

        return html;
    }

    function getRatingsData()
    {
        
        jQuery('#tagalys-filter-custom_rating_tag_set').find('.tagalys-managed-list-link').each(function() {

              count = count + 1;

              var is_rating_text_changed_input = jQuery(this).find('.rating-changed-input').val();
              var is_rating_text_changed = jQuery(this).find('.rating-changed').text();
              var is_rating_text_changed2 = jQuery('#rating-changed').text();
              var rating_and_count = jQuery(this).text();
              var rating_htmlText = jQuery(this).html();
              var rating_count = jQuery(this).find('.count').text();
              var rating_htmlText_array = rating_htmlText.split('<');
              var rating = rating_htmlText_array[0];

              console.log("is_rating_text_changed_input => "+is_rating_text_changed_input);
              console.log("is_rating_text_changed => "+is_rating_text_changed);
              console.log("is_rating_text_changed2 => "+is_rating_text_changed2);
              console.log(rating_htmlText);
              console.log(rating_htmlText_array);
              console.log(rating_htmlText_array[0]);
              console.log(rating_count);
              console.log('count => '+count);

              if (is_rating_text_changed2 === '' || is_rating_text_changed_input === undefined) 
              {

                  if (rating >= 0 && rating <= 20) {
                      console.log("rating is 0 - 20");

                      var htmlData = getRatingsHtml(0, 20, rating_count);
                      console.log(htmlData);
                      jQuery(this).html(htmlData);

                  }
                  else if (rating >= 21 && rating <= 40) {
                      console.log("rating is 20 - 40");
                      var htmlData = getRatingsHtml(0, 20, rating_count);
                      console.log(htmlData);
                      jQuery(this).html(htmlData);
                  }
                  else if (rating >= 41 && rating <= 60) {
                      console.log("rating is 40 - 60");
                      var htmlData = getRatingsHtml(40, 60, rating_count);
                      console.log(htmlData);
                      jQuery(this).html(htmlData);
                  }
                  else if (rating >= 61 && rating <= 80) {
                      console.log("rating is 60 - 80");
                      var htmlData = getRatingsHtml(60, 80, rating_count);
                      console.log(htmlData);
                      jQuery(this).html(htmlData);
                  }
                  else if (rating >= 81 && rating <= 100) {
                      console.log("rating is 80 - 100");
                      var htmlData = getRatingsHtml(80, 100, rating_count);
                      console.log(htmlData);
                      jQuery(this).html(htmlData);
                  }
                  else 
                  {
                      console.log("rating is null");
                  }

              }
              else
              {
                  console.log("skipped rating text if condition");
              }

        });

    }

    jQuery('#tagalys-namespace').bind("DOMNodeInserted", function(){
        
        console.log('tagalys-namespace DOMNodeInserted');

        if (jQuery('#tagalys-filter-custom_rating_tag_set').length > 0 )
        {
            console.log(" Calling get ratings data ");
            /*
                $('.filter-heading').click(function(){
                    $(this).toggleClass('open');
                    $(this).next().slideToggle();
                });*/

            getRatingsData()

        }        
        
    });


    /*$('#tagalys-namespace').on("DOMNodeInserted", '.footer', function(evt){
       if($('#tagalys-filter-custom_rating_tag_set').is(':visible')){
            $('.filter-heading').click(function(){
                $(this).toggleClass('open');
                $(this).next().slideToggle();
                console.log('success');
            });

            console.log('Success Success');

        }

        console.log('eleemnt is not visible');
    });
*/

    /*jQuery('#tagalys-namespace').click(function(){
            
        if(jQuery('#tagalys-filter-custom_rating_tag_set').is(':visible')){
            $('.filter-heading').click(function(){
                $(this).toggleClass('open');
                $(this).next().slideToggle();
                console.log('success');
            });

            console.log('success');

        }

        console.log('document clicked');
    });*/

    

    /*jQuery('#tagalys-namespace').on('DOMSubtreeModified', function()
    {
        var state = "false";
        
        console.log("STATE start: "+state);   

        console.log("DOMSubtreeModified");   
          
        if (state === "false") {

              if (jQuery('#tagalys-filter-custom_rating_tag_set').length > 0 )
              {
                  console.log(" Ratings Length greater than zero ");

                  getRatingsData(); 

                  state = "true";

                  console.log("STATE changed to : "+state);
              }  
        }
          
        console.log("STATE end : "+state);     
    });*/

    /*var myRatings = setInterval(function()
    {

        console.log("MY RATINGS ");

        if($('.filter-heading').is(':visible'))
        {
            console.log("Calling get ratiing data !");
            //getRatingsData();

            if (jQuery('#tagalys-filter-custom_rating_tag_set').length > 0 ) 
            {

                console.log("inside #tagalys-filter-custom_rating_tag_set");            
                getRatingsData(); 

            }

        }

        // if (jQuery('#tagalys-filter-custom_rating_tag_set').length > 0 ) 
        // {

        //     console.log("inside #tagalys-filter-custom_rating_tag_set");            
        //     getRatingsData();            
        //     clearTimeout(myRatings);

        // }
        clearTimeout(myRatings);

    }, 1000);*/


   jQuery(document).ready(function(){ 

     jQuery(".block-viewed-products-grid .block-title strong").css("font-size","24px");
     jQuery(".block-viewed-products-grid .block-title strong").css("color","#007C9D");
     jQuery(".block-viewed-products-grid .block-title strong").css("text-transform","uppercase");
     jQuery(".block-viewed-products-grid .block-title strong").css("text-decoration","underline");
     jQuery(".block-viewed-products-grid .block-title strong").css("font-weight","200");
     jQuery(".widget-viewed-grid li .product-item-info a .product-image-container").css("border","1px solid #cccccc");

    jQuery(document).on('click','.checkout-methods-items .item .primary',function(evt){
     evt.stopPropagation();
      var cust_flag = jQuery('#cust_flag').val();
      var base_url = jQuery('#base_url').val();
      var otp_telephone = jQuery('#co-shipping-form').find('input[name="telephone"]').val();
     if(cust_flag == ""){
        jQuery(".social-login").trigger("click");
        jQuery("#guest_otp").show();
     }else{
      window.location.href = base_url+"checkout";
     }
    });
    jQuery(document).on('click','#guest_otp',function(){
     jQuery(".verify_otp").show();
     jQuery(".main_login_otp").hide();
});
    jQuery(document).on('click','.verify-back-btn',function(){
     jQuery(".verify_otp").hide();
     jQuery(".main_login_otp").show();
});
jQuery(document).on('click','#get_my_otp',function(evt){
      var otp_telephone = jQuery('#mobile_no').val();
      var my_otp_url = jQuery('#my_otp_url').val();
        $.ajax({
                showLoader: true,
                url: my_otp_url,
                data: {"my_mobile":otp_telephone},
                type: "POST"
            }).done(function (data) {
              if(data == "sent"){
              jQuery('#my_otp').show();
              jQuery('.my_sent').show();
              jQuery('.my_sent').delay(10000).fadeOut();
              jQuery('#get_my_otp').hide();
              jQuery('#my_verify').show();
              }
            });
    });

  jQuery(document).on('click','.my_resend',function(){
            var otp_telephone = jQuery('#mobile_no').val();
            var my_otp_url = jQuery('#my_otp_url').val();
            $.ajax({
                showLoader: true,
                url: my_otp_url,
                data: {"my_mobile":otp_telephone},
                type: "POST"
               }).done(function (data) {
              if(data == "sent"){
              jQuery('.my_resent').show();
              jQuery('.my_resent').delay(4000).fadeOut();
              }
                            });
    });

    jQuery(document).on('click','#my_verify',function(evt){
          var otp_telephone = jQuery('#mobile_no').val();
          var my_check_otp = jQuery('#my_check_otp').val();
          var my_verify_url = jQuery('#my_verify_url').val();
          var base_url = jQuery('#base_url').val();
                        $.ajax({
                                showLoader: true,
                                url: my_verify_url,
                                data: {"otp_telephone":otp_telephone,"my_check_otp":my_check_otp},
                                type: "POST"
                            }).done(function (data) {
              if(data == "not matched"){
              jQuery('.my_not_match').show();
              jQuery('.my_not_match').delay(4000).fadeOut();
              }else if(data == ""){
              jQuery('.my_logged').show();
              window.location.href = base_url+"checkout";
              }
                            });
    });
/*guest checkout verification ends here*/
        jQuery(document).on('click','#get-otp',function(){
          var mobile = jQuery('#mobile').val();
          var mobile_url = jQuery('#mobile_url').val();
                        $.ajax({
                                showLoader: true,
                                url: mobile_url,
                                data: {"mobile":mobile},
                                type: "POST"
                            }).done(function (data) {
              if(data == 'not'){
              jQuery('.not-exist').show();
              jQuery('.not-exist').delay(9000).fadeOut();
              }else if(data == "sent"){
              jQuery('.otp').show();
              jQuery('.sent').show();
              jQuery('.sent').delay(10000).fadeOut();
              jQuery('#get-otp').hide();
              jQuery('#verify').show();
              }
                            });
    });
  jQuery(document).on('click','.resend',function(){
          var mobile = jQuery('#mobile').val();
          var mobile_url = jQuery('#mobile_url').val();
                        $.ajax({
                                showLoader: true,
                                url: mobile_url,
                                data: {"mobile":mobile},
                                type: "POST"
                            }).done(function (data) {
              if(data == "not"){
              jQuery('.not-exist').show();
              jQuery('.not-exist').delay(4000).fadeOut();
              }else if(data == "sent"){
              jQuery('.resent').show();
              jQuery('.resent').delay(4000).fadeOut();
              }
                            });
    });
  jQuery(document).on('click','#verify',function(){
          var mobile = jQuery('#mobile').val();
          var otp = jQuery('#otp').val();
          var verify_url = jQuery('#verify_url').val();
                        $.ajax({
                                showLoader: true,
                                url: verify_url,
                                data: {"mobile":mobile,"otp":otp},
                                type: "POST"
                            }).done(function (data) {
              if(data == "not matched"){
              jQuery('.not-match').show();
              jQuery('.not-match').delay(4000).fadeOut();
              }else if(data == "matched"){
              jQuery('.logged').show();

              /* Redirect user to the current page after successful login */

              // window.location.href = jQuery('#base_url').val();
              console.log(window.location.href);
              window.location.href = window.location.href;
              
              }
          });
    });

       jQuery(document).on('change','#mobile_number',function(){
            var mob = jQuery('.cont').val();
            var url_cont = jQuery('.url_cont').val();
                jQuery.ajax({
                    showLoader: true,
                    url: url_cont,
                    data: {"mobile":mob},
                    type: "POST"
                }).done(function (data) {
                if(data == 'no'){
                }else if(data == "yes"){
                jQuery('.mob-error').show();
                setTimeout(showpanel1, 5000);       
                }
                });
      })
     function showpanel1() { 
        jQuery('.mob-error').fadeOut();
        jQuery(".cont").val('');    
     }

    jQuery("#log_otp").click(function() {
      jQuery(".fieldset.login").hide();
      jQuery(".fieldset.login-otp").show();
    });
    jQuery(".back-btn").click(function() {
      jQuery(".fieldset.login").show();
      jQuery(".fieldset.login-otp").hide();
    });

        jQuery(".showcart").click(function() {
          console.log('hello');
            var retail_url = $("#retail_url").val(); 
                $.ajax({
                    showLoader: true,
                    url: retail_url,
                    data: {},
                    type: "POST"
                  }).done(function (data) {
                    if(data == '1'){
                      var obj = $(".error");
                      if(obj.text() == "")
                      {
                  jQuery("#minicart-content-wrapper .block-content .actions:first").prepend('<span class="error" style="color: red;font-size: 13px;">Cart Value should be minimum Rs. 600. please add more products.</span>');
                  jQuery('#top-cart-btn-checkout').prop('disabled', true);
                      }
                    }else{}
                  });
        });

/* guest checkout user autofill mobile*/
    
  jQuery(document).on("change",'input[name="telephone"]', function() {
    var tele = jQuery('input[name="telephone"]').val();
    console.log(tele);
    jQuery('input[name="mobile_otp"]').val(tele);
});

/* appending % in filter values */
  // jQuery(".Rating .filter-options-content .items li .count").hide();

    jQuery(".Discount .filter-options-content .items li a .price").each(function() {
       var values = jQuery(this).text();
       var element = values.split("₹");
       var per = '%';
       jQuery(this).text(element['1']+per);
    });
    jQuery(".Package  .filter-options-content .items li a span.price").each(function() {
    var values1 = jQuery(this).text();
    var stat1 = /\d/.test(values1);
      if(stat1 == true){
       var element1 = values1.split("₹");
       var per1 = 'gm';
       jQuery(this).text(element1['1'] +per1);
     }
    });
/* code ends here */   


$("#om ul li:nth-child(6)").append('<ul style="display:none;"><li class="level1 nav-1-2 parent"><a href="'+jQuery('#base_url').val()+'subscribe/product/prodlist"><span>Subscription Products</span></a></li></ul>');
    })
})

