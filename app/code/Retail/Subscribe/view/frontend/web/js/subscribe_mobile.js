require([ 'jquery', 'jquery/ui'], function($){

  function jsfunction(my_val){
    var key = _.findKey(Apo.month, function(v) {
      return v == my_val;
    });

    var discount_value = Apo.discount[key];
    jQuery(".per_discount").text(discount_value+ "%");
    jQuery(".per_discounts").text(discount_value+ "%");
    jQuery("#discount").val(discount_value);
  }

  jQuery(document).ready(function(){ 
    
    jQuery(document).on('change','#period',function(){
      var my_val = this.value;
      jsfunction(my_val);
    });

    /*showing subscribe method based on product */
    var min_qty = "";
    var max_qty = "";
    var login_url = "";
    jQuery(document).on('change','input[name="subscribe"]',function(){
        var sub = $("input[name='subscribe']:checked").val();
        if(sub == 'ss'){
          var cust_flag = $("#cust_flag").val();
          var login_url = $("#login_url").val();
          if(cust_flag == ""){
            window.location = login_url;
          } else {
            var min_qty = $("#min_qty").val();
            var max_qty = $("#max_qty").val();
            jQuery(".subscribe_period").show();
            jQuery("#subscribe_btn").show();
            jQuery("#qty").val(min_qty);
            //jQuery("#product-addtocart-button").hide();
          }
        } else if(sub == 'otp') {
          jQuery(".subscribe_period").hide();
          jQuery("#subscribe_btn").hide();
          jQuery("#product-addtocart-button").show();
          jQuery("#qty").val(1);
          jQuery("#alert_msg").hide();
        }
    });

    /* Checking min qty for subscription */
    jQuery(document).on('click','#subscribe_btn',function(){
     var cont_url =  $("#cont_url").val();
     var cust_id =  $("#cust_id").val();
     var prod_id =   $("#prod_id").val();
     var cust_flag = $("#cust_flag").val();
     var min_qty =   $("#min_qty").val();
     var max_qty =   $("#max_qty").val();
     var discount =  $("#discount").val();
     var qty =       $("#qty").val();
     var sub_period = $("#period option:selected").val();

     if(parseInt(qty) < parseInt(min_qty) || parseInt(qty) > parseInt(max_qty)){
        
        jQuery("#alert_msg").show();
        jQuery("#alert_msg").html('Required quantity is '+ min_qty + '-'+ max_qty);

      } else {
        $.ajax({
        showLoader: true,
        url: cont_url,
        data: {"product_id":prod_id,"customer_id":cust_id,"sub_period":sub_period,"discount":discount,"qty":qty,"flag":'0'},
        type: "POST"
        }).done(function (data) {
          if(data == 'already'){
           jQuery("#alert_msg").show();
           jQuery("#alert_msg").html('<span class="success-message">Subscription has been updated</span>');
           setTimeout(function() {
              jQuery('#alert_msg').hide();
            }, 3000);
          } else if(data == 'alreadys'){
            jQuery("#alert_msg").show();
            jQuery("#alert_msg").html('<span class="error-message">You have already subscribed for this product.</span>');
            setTimeout(function() {
              jQuery('#alert_msg').hide();
            }, 3000);
          } else if(data == 'submitted'){
            jQuery("#alert_msg").show();
            jQuery("#alert_msg").html('<span class="success-message">You have subscribed for this product.</span>');
            setTimeout(function() {
              jQuery('#alert_msg').hide();
            }, 3000);
          } else {
            jQuery("#alert_msg").hide();
          }
        });
      }
    });
  });
});