<?php
namespace Retail\Subscribe\Model;

class Subscribe extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\Subscribe\Model\ResourceModel\Subscribe');
    }
}
