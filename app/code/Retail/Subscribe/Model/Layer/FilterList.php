<?php

namespace Retail\Subscribe\Model\Layer;

class FilterList 
    {


        public function __construct(
            \Magento\Catalog\Block\Product\Context $context,
            \Magento\Framework\ObjectManagerInterface $objectManager


        ) {
            $this->objectManager = $objectManager;                  

        }


        public function aroundGetFilters( \Magento\Catalog\Model\Layer\FilterList $subject, \Closure $proceed, \Magento\Catalog\Model\Layer $layer)
        {


            $result = $proceed($layer);
            $result[] = $this->objectManager->create('Retail\Subscribe\Model\Layer\Filter\Rating', ['layer' => $layer]);

            return $result;

        }


    }
