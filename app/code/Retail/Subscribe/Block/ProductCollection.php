<?php
namespace Retail\Subscribe\Block;

class ProductCollection extends \Magento\Framework\View\Element\Template
{    
    protected $_productCollectionFactory;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;    
        parent::__construct($context, $data);
    }
    
    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
     // $collection->addAttributeToSelect('*');
         $collection->addAttributeToSelect('id');
         $collection->addAttributeToSelect('price');
         $collection->addAttributeToSelect('special_price');
         $collection->addAttributeToSelect('custom_discount');
         $collection->addAttributeToSelect('sku');
        // $collection->setPageSize(10); // fetching only 3 products
        return $collection;
    }
}
?>