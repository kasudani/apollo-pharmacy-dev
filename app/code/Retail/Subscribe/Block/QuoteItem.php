<?php
namespace Retail\Subscribe\Block;

class QuoteItem extends \Magento\Framework\View\Element\Template
{    
    protected $_cart;    
    protected $_checkoutSession;    
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    )
    {
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
        
        parent::__construct($context, $data);
    }
    
    public function getCart()
    {        
        return $this->_cart;
    }
    
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}
 ?>