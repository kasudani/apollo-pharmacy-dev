<?php
namespace Retail\Subscribe\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;

use Magento\Framework\Setup\ModuleContextInterface;

use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface

{

public function upgrade(SchemaSetupInterface $setup,

ModuleContextInterface $context){

$setup->startSetup();

if (version_compare($context->getVersion(), '1.0.1') < 0) {

// Get module table

$tableName = $setup->getTable('quote');

// Check if the table already exists

if ($setup->getConnection()->isTableExists($tableName) == true) {

// Declare data

$columns = [

'guest_verification' => [

'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

'nullable' => true,

'comment' => 'Guest Verification',

],

];

$connection = $setup->getConnection();

foreach ($columns as $name => $definition) {

$connection->addColumn($tableName, $name, $definition);

}

 
}

}

 
$setup->endSetup();

}

}