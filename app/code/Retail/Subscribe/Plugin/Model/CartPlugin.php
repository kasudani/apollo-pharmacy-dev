<?php

namespace Retail\Subscribe\Plugin\Model;


use \Magento\Framework\Message\ManagerInterface ;

class CartPlugin
{
    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;

    /**
     * @var \Retail\Subscribe\Helper\CalculateDiscount
     */
    protected $_calculateDiscountHelper;

    /**
     * Plugin constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        ManagerInterface $messageManager,
        \Retail\Subscribe\Helper\CalculateDiscount $calculateDiscount
    ) {
        $this->quote = $checkoutSession->getQuote();
        $this->_messageManager = $messageManager;
        $this->_calculateDiscountHelper = $calculateDiscount;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param $data
     * @return array
     */
    public function beforeUpdateItem(
        \Magento\Checkout\Model\Cart $subject,
        $itemId, 
        $requestInfo = null, 
        $updatingParams = null
    )
    {
        $quoteItem = $subject->getQuote()->getItemById($itemId);        
        # tried to get the requested quantity but couldn't fetch.
        # need to check again on this later on how to get the qty from request object.

        return [$itemId,$requestInfo,$updatingParams];

    }
    
    
    // public function beforeupdateItems(\Magento\Checkout\Model\Cart $subject,$data)
    // {
    //     $quote = $subject->getQuote();
    //     $mappedProductSkus= $this->_helperData->getSku();
    //     foreach($data as $key=>$value){
    //         $item = $quote->getItemById($key);
    //          $productSku= $item->getSku();
    //           $itemQty= $value['qty'];
    //             if(Condition ))
    //             {
    //                 $data[$itemId]['qty'] = 1;
    //                 if($item->getQty()>1){
    //                     $this->_messageManager->addNoticeMessage('Only one item can be bought at a time');
    //                 }
    //             }

    //     }
    //     return [$data];

    // }

}