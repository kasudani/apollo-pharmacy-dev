<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Retail\Subscribe\Plugin\Cart;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Checkout\Model\Cart;
use Retail\Subscribe\Helper\CalculateDiscount;

class UpdatePost extends \Magento\Checkout\Controller\Cart\UpdatePost
{
    protected $_calculateDiscount;
    protected $_customerCart;

    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        cart $cart,
        CalculateDiscount $calculateDiscount
    ) {
        $this->_formKeyValidator = $formKeyValidator;
        $this->_scopeConfig = $scopeConfig;
        $this->_checkoutSession = $checkoutSession;
        $this->_storeManager = $storeManager;
        $this->_customerCart = $cart;
        $this->_calculateDiscount = $calculateDiscount;
        parent::__construct($context, $scopeConfig, $checkoutSession, $storeManager, $formKeyValidator, $cart);
    }
    /**
     * Empty customer's shopping cart
     *
     * @return void
     */
    protected function _emptyShoppingCart()
    {
        try {
            $this->cart->truncate()->save();
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->messageManager->addError($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the shopping cart.'));
        }
    }

    /**
     * Update customer's shopping cart
     *
     * @return void
     */
    protected function _updateShoppingCart()
    {
        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/cartUpdatePost.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        /* Code here */        
        $logger->info(__FUNCTION__);
        $logger->info("inside cart update post controller ... ");

        try {
            $cartData = $this->getRequest()->getParam('cart');

            # get the request 
            $logger->info($cartData);

            if (is_array($cartData)) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                );

                # get product id from item id...                                
                $logger->info("before loop");

                foreach ($cartData as $index => $data) {

                    $logger->info("quote item for index ==> " . $index);
                    $quoteItem = '';
                    $product_id = '';

                    if (isset($data['qty'])) {
                        $logger->info("cart qty ==> " . $data['qty']);
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));

                        # our code here...
                        $quoteItem = $this->_customerCart->getQuote()->getItemById($index);
                        $product_id = $quoteItem->getProduct()->getId();
                        $logger->info($product_id);

                        # call discount helper..
                        $discountedPrice = $this->_calculateDiscount->calculate($product_id, $data['qty']);
                        
                        # update the price
                        if($discountedPrice != '') {
                            $quoteItem->setCustomPrice($discountedPrice['final_price']);
                            $quoteItem->setOriginalCustomPrice($discountedPrice['final_price']);
                            $quoteItem->getProduct()->setIsSuperMode(true);
                        }
                    }
                }
                
                if (!$this->cart->getCustomerSession()->getCustomerId() && $this->cart->getQuote()->getCustomerId()) {
                    $this->cart->getQuote()->setCustomerId(null);
                }

                $cartData = $this->cart->suggestItemsQty($cartData);
                $this->cart->updateItems($cartData)->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError(
                $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
            );
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t update the shopping cart.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
    }

    /**
     * Update shopping cart data action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/cartUpdatePost.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        /* Code here */
        $logger->info(__CLASS__);
        $logger->info(__FUNCTION__);
        $logger->info("inside cart update post controller ... ");

        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $updateAction = (string)$this->getRequest()->getParam('update_cart_action');

        $logger->info("update action ==> " . $updateAction);

        switch ($updateAction) {
            case 'empty_cart':
                $this->_emptyShoppingCart();
                break;
            case 'update_qty':
                $this->_updateShoppingCart();
                break;
            default:
                $this->_updateShoppingCart();
        }

        return $this->_goBack();
    }
}
