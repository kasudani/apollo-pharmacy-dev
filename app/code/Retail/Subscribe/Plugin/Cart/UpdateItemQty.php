<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Retail\Subscribe\Plugin\Cart;

use Magento\Checkout\Model\Sidebar;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data;
use Psr\Log\LoggerInterface;
use Retail\Subscribe\Helper\CalculateDiscount;
use Magento\Checkout\Model\Cart;

class UpdateItemQty extends \Magento\Checkout\Controller\Sidebar\UpdateItemQty
{
    /**
     * @var Sidebar
     */
    protected $sidebar;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $jsonHelper;

    /**
     * @var \Retail\Subscribe\Helper\CalculateDiscount
     */
    protected $_calculateDiscountHelper;

    protected $_cart;

    /**
     * @param Context $context
     * @param Sidebar $sidebar
     * @param LoggerInterface $logger
     * @param Data $jsonHelper
     * @codeCoverageIgnore
     */
    public function __construct(
        Context $context,
        Sidebar $sidebar,
        LoggerInterface $logger,
        Data $jsonHelper,
        Cart $cart,
        CalculateDiscount $calculateDiscount
    ) {
        $this->sidebar = $sidebar;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->_calculateDiscountHelper = $calculateDiscount;
        $this->_cart = $cart;
        parent::__construct($context,$sidebar,$logger,$jsonHelper);
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/UpdateItemQty.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        /* Code here */
        $logger->info(__CLASS__);

        $itemId = (int)$this->getRequest()->getParam('item_id');
        $itemQty = (int)$this->getRequest()->getParam('item_qty');

        $logger->info("before quote");        

        # quote 
        $quoteItem = $this->_cart->getQuote()->getItemById($itemId); 
        $product_id = $quoteItem->getProduct()->getId();
        $logger->info($product_id);

        $logger->info("after params");        

        try {
            $logger->info("in try catch");
            $this->sidebar->checkQuoteItem($itemId);
            $logger->info("after checkQuoteItem");

            # call Discount helper
            $logger->info("calling discount helper...");
            $discountedPrice = $this->_calculateDiscountHelper->calculate($product_id, $itemQty);

            $logger->info("discountedPrice ==> ");
            $logger->info($discountedPrice);

            # can put a validation check if quote item exists
            if($discountedPrice != '') {
                $logger->info("now setting discountedPrice ... ");
                $quoteItem->setCustomPrice($discountedPrice['final_price']);
                $quoteItem->setOriginalCustomPrice($discountedPrice['final_price']);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }

            $logger->info("after setting discountedPrice");

            $this->sidebar->updateQuoteItem($itemId, $itemQty);
            $logger->info("after updateQuoteItem");
            return $this->jsonResponse();
        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Compile JSON response
     *
     * @param string $error
     * @return Http
     */
    protected function jsonResponse($error = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($this->sidebar->getResponseData($error))
        );
    }
}
