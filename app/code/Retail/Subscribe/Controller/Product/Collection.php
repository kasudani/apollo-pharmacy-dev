<?php 
namespace Retail\Subscribe\Controller\Product; 

class Collection extends \Magento\Framework\App\Action\Action
{

        protected $_objectManager;

        protected $eavConfig;

        public function __construct(
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory)
    {
        $this->eavConfig = $eavConfig;
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_storeManager = $storeManager;
        $this->_attributeFactory = $attributeFactory;
        parent::__construct($context);
    }
        public function execute()
        { 
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storeId = '0';
            $discount = "";
            $prod_collection = $this->_objectManager->get('Retail\Subscribe\Block\ProductCollection');
            $rtu = $prod_collection->getProductCollection();
            foreach ($rtu as $value) {
             if(!empty($value->getSpecialPrice())){
              $spec_price = $value->getSpecialPrice();
              $price = $value->getPrice();
              if($spec_price <= $price){
               $diff_price = $price - $spec_price;
                   $discount = ($diff_price/$price)*100;
                   $discount = round($discount);
                   echo "For sku ".$value->getSku()."discount ".$discount."%added <br>";
                   $productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');
                   $product = $productFactory->create()->setStoreId($storeId)->load($value->getId());
                   $product->setCustomDiscount($discount);
                   $product->save();
              }else{}
             }
            }
                
            }
}
?>