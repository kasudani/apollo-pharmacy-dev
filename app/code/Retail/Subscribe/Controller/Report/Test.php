<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Retail\Subscribe\Controller\Report;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
 
class Test extends \Magento\Framework\App\Action\Action
{
    
	protected $_orderCollectionFactory;
	protected $_customerFactory;
	protected $_resourceConnection;


    public function __construct(
        Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection

    ) 
    {
        parent::__construct($context);
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_customerFactory = $customerFactory;
        $this->_resourceConnection = $resourceConnection;
    }

    public function orderCollection()
   	{
       $collection = $this->_orderCollectionFactory->create()->addAttributeToSelect('*');

       return $collection;
       // // You Can filter collection as 
       // $this->orderCollectionFactory->addFieldToFilter($field, $condition);
    }

    public function getCustomerCollection()
    {   
        //Get customer collection
        return $this->_customerFactory->create();
    }

    public function customOrderCollection(){
    	return'';
    }

    public function execute()
    {
        $orderCollection = $this->orderCollection();

        $customerCollection = $this->getCustomerCollection();

        $connection = $this->_resourceConnection->getConnection();
		$tableName = $this->_resourceConnection->getTableName('sales_order');

		
		$sql2 = 'SELECT customer_id, max(created_at) as last_tranct_date FROM '.$tableName.' GROUP by customer_id';

		$sellers = $connection->fetchAll($sql2);

        /*foreach ($orderCollection as $order) {
        	# code...
        	print($order->getCustomerId()." => ".$order->getCreatedAt());
        	print('<br>');
        }*/


        //Get customer collection
		
		$customerIdsArray = [];

		foreach ($sellers as $value) {
		    array_push($customerIdsArray, $value['customer_id']);
		}
		

		$orderCollectionCustomerIds = [];

		foreach ($sellers as $value) {
			# code...
			$subArray = [];
			$subArray['customer_id'] = $value['customer_id'];
			$subArray['last_tranct_date'] = $value['last_tranct_date'];

		    $orderCollectionCustomerIds[$value['customer_id']] = $subArray;
		}


		
		//print_r($orderCollectionCustomerIds);
		

		/*print('<pre>');
		var_dump($sellers);
		print('</pre>');*/

		/*foreach ($sellers as $value) {
			# code...
			print($value['customer_id']);
			print('<br>');
		}*/

		$html = "";
		$html .= "<table>";
		
		$html .= "<thead><th>Customer ID</th><th>Date</th></thead>";

		foreach ($customerCollection as $customer) {
			# code...
			
			if (array_key_exists($customer->getId(), $orderCollectionCustomerIds)) {
				# code...
				$html .= "<tbody><td>".$customer->getId()."</td><td>".$orderCollectionCustomerIds[$customer->getId()]['last_tranct_date']."</td></tbody>";
			}
			else
			{
				$html .= "<tbody><td>".$customer->getId()."</td><td>NA</td></tbody>";
			}

			
			
		}

		$html .= "</table>";

		echo $html;

		// $html .= "<tbody><td>Customer ID</td><td>Date</td></tbody>";
		
		// foreach ($customerCollection as $customer) {
		// 	# code...

		// 	if (condition) {
		// 		# code...
		// 	}

		// 	$html .= "<tbody><td>".$customer->getId()."</td><td>ddd</td></tbody>";
		// 	// print($customer->getId());
		// 	// print('<br>');
		// }

		/*$html .= "</table>";

		echo $html;*/
		
    }

}