<?php
namespace Retail\Subscribe\Controller\Guest;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Delete extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    
    protected $_objectManager;
        
    public function __construct(\Magento\Framework\App\Action\Context $context, 
                                \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory      = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cartObject = $objectManager->create('Magento\Checkout\Model\Cart')->truncate();
        $cartObject->saveQuote();
        echo "delete";
    }
}
