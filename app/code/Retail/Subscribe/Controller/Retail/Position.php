<?php 
namespace Retail\Subscribe\Controller\Retail; 

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Position extends \Magento\Framework\App\Action\Action { 
  
protected $_objectManager;

protected $_pageFactory;

protected $_subscribeFactory;  

public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
	    \Retail\Subscribe\Model\SubscribeFactory $cu)
	{
		$this->_pageFactory = $pageFactory;
		$this->_subscribeFactory = $cu;
		parent::__construct($context);
	}

	public function execute() {    
	        $quote_ids = array();
	        $subs_ids = array();   
		    $cuy =     $this->_objectManager->get('Retail\Subscribe\Block\Rewrite\Customer\Account\Customer');
            $cust_id = $cuy->getLoggedinCustomerId();
			$luy = $this->_objectManager->get('Retail\Subscribe\Block\QuoteItem');
			$quote = $luy->getCheckoutSession()->getQuote();
			$items = $quote->getAllItems();
			$product= $this->_objectManager->get('Retail\Subscribe\Block\Product');
			foreach($items as $item) {
				$quote_prodid = $item->getProductId();
				array_push($quote_ids,$quote_prodid);
				$prod_obj =$product->getProduct($quote_prodid);
				$prod_ss = $prod_obj->getData('ss_plan');
				if($prod_ss == '488'){
                 array_push($subs_ids,$quote_prodid);
				}
			}
			if(!empty($subs_ids)){
			$getGrandTotal = $quote->getGrandTotal();
			if(($getGrandTotal !="") && ($getGrandTotal<600)){
			$save_ids = array();
            $model = $this->_subscribeFactory->create();
            $collection = $model->getCollection();
            $collection->addFieldToFilter('customer_id', $cust_id);
            $collection->addFieldToFilter('flag', 0);
            if(!empty($collection->getData())){
             foreach($collection as $rtu){
               $model->load($rtu['id'],'id');
               array_push($save_ids,$model->getProductId());
             }
            } 
            $result = array_diff($quote_ids,$save_ids);
            if(!empty($result)){
            echo '0';
            }else{
            echo '1';
            }
			}
        }else{}
	} 
}
