<?php
namespace Retail\Subscribe\Controller\Ajax;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Ajaxaddtocart extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function execute()
    {
        $product_id = "";
        $form_key = "";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $key_form = $objectManager->get('Magento\Framework\Data\Form\FormKey'); 
        $form_key = $key_form->getFormKey();

        if(isset($_REQUEST['product_id']))
        {
            $product_id = $_REQUEST['product_id'];
            $params = array(
                            'form_key' => $form_key,
                            'product' => $product_id,                
                            'qty'   => '1'
                        );
            $this->_redirect("checkout/cart/add/form_key/", $params);            
        }

        
	}
}
