<?php 

namespace Retail\Subscribe\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
 
class CalculateDiscount extends AbstractHelper
{
    protected $_product;
    protected $_productRepository;

    public function __construct(
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ){
        $this->_product = $product;
        $this->_productRepository = $productRepository;     
    }

    public function calculate($product_id, $product_qty)
    {
        $qty = $product_qty;
        $rangeIndex = '';

        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/calDiscount.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
       
        $logger->info('caculate discount...');
        $logger->info('product id ==> ' . $product_id);
        $logger->info('product qty ==> ' . $product_qty);

        # product object
        $productObject = $this->_productRepository->getById($product_id);

        $quantityPerDiscount = $productObject->getQuantityPerDiscount();
        $isQuantityPerDicountInUse = $productObject->getUseQuantityPerDiscount();
        $price = $productObject->getPrice();
        $specialPrice = $productObject->getSpecialPrice();

        $logger->info('b4 special_from_date ... ');
        $special_from_date  = $productObject->getSpecialFromDate();  
        $logger->info('after special_from_date ... ' . $special_from_date);    

        $logger->info('b4 special_to_date ... ');
        $special_to_date    = $productObject->getSpecialToDate();
        $logger->info('after special_to_date ... ' . $special_to_date);

        
        # validate the special price
        $logger->info('b4 isSpecialPriceValid ... ');
        $isSpecialPriceValid = $this->validateSpecialPrice($special_from_date, $special_to_date, $specialPrice);
        $logger->info('isSpecialPriceValid ==> ' . $isSpecialPriceValid);

        # get option text
        $optionText = '';
        $optionId = $productObject->getQuantityPerDiscountToApply();   
        $attr = $productObject->getResource()->getAttribute('quantity_per_discount_to_apply');
        if ($attr->usesSource()) {
            $optionText = $attr->getSource()->getOptionText($optionId);
        }

        
        $logger->info('quantityPerDiscount ==> ' . $quantityPerDiscount);
        $logger->info('isQuantityPerDicountInUse ==> ' . $isQuantityPerDicountInUse);
        $logger->info('optionText ==> ' . $optionText);
        $logger->info('price ==> ' . $price);
        $logger->info('specialPrice ==> ' . $specialPrice);

        # IF the product is not in use
        if(!$isQuantityPerDicountInUse) {
            $logger->info('quantity per discount is not in use.');
            return '';
        }

        # IF required attributes values are missing
        if($quantityPerDiscount=='' || $optionText=='' || $qty=='') {
            $logger->info('required attributes values are missing.');
            return '';
        }


        # IF the product is in use
        $discountArray = [];

        if($isQuantityPerDicountInUse) 
        {
            $quantityPerDiscountArray = explode(',', $quantityPerDiscount);

            foreach ($quantityPerDiscountArray as $key => $value) 
            {
                $temp = [];
                $temp = explode('-', $value);
                # temp[0] is quantity.
                # temp[1] is the discount to apply.
                $discountArray[$temp[0]] = $temp[1];    
            }
            
            $logger->info('discountArray ==> ');
            $logger->info($discountArray);

            # quantity keys
            $quantityKeys = [];
            foreach ($discountArray as $key => $value) {
                # code...
                array_push($quantityKeys,$key);
            }  

            $logger->info('quantityKeys ==> ');
            $logger->info($quantityKeys);

            #get rangeIndex to apply discount from discountArray
            for($i = 0; $i < count($quantityKeys); $i++) {
                
                # if provided qty is greater than or equals to the maximum allowed discount on a item qty
                if($qty >= $quantityKeys[count($quantityKeys)-1]) {
                    $rangeIndex = $quantityKeys[count($quantityKeys)-1];
                    break;
                }
                elseif($qty < $quantityKeys[$i]) {
                
                    # if provided qty is less than the qty from current index 
                    # if index is 0 than take the first index value
                    if($i == 0) {
                      $rangeIndex = $quantityKeys[0];        
                    }
                    else {
                      $rangeIndex = $quantityKeys[$i-1];        
                    }
                    break;
                }
            }
            
            $logger->info('rangeIndex ==> ' . $rangeIndex);

            # discount to apply
            $applyDiscount = $discountArray[$rangeIndex];

            $logger->info('applyDiscount ==> ' . $applyDiscount);

            # discount on actual price
            $final_discount_on_actual_price = (float)( $price * ($applyDiscount / 100));

            $logger->info('final_discount_on_actual_price ==> ' . $final_discount_on_actual_price);

            # discount on special price
            if($specialPrice != '') {
                $final_discount_on_special_price  = (float)($specialPrice * ($applyDiscount / 100));
                $logger->info('final_discount_on_special_price ==> ' . $final_discount_on_special_price);
            }
            else {
                $final_discount_on_special_price = $final_discount_on_actual_price;
            }

            # Final price after discount
            $final_price = '';

            # apply discount on actual price
            if($optionText == "On actual price") {
                $final_price = (float)($price - $final_discount_on_actual_price);
                $logger->info('discount applied on actual price ... ');
            }

            # apply discount on special price
            if($optionText == "On special price") {

                # if special price is not empty
                # if special price is valid 
                # need validation on special price expiry
                if($specialPrice != '' && $isSpecialPriceValid) {
                    $final_price = (float)($specialPrice - $final_discount_on_special_price);
                    $logger->info('discount applied on special price ... ');
                }
                else{
                    $final_price = (float)($price - $final_discount_on_actual_price);
                    $logger->info('discount applied on final price ... ');
                }                
            }
        }

        $logger->info('final_price ==> ' . $final_price);

        # return the final calulated discounted price to update and applied discount
        // return $final_price;
        return [
            "final_price" => $final_price,
            "applied_discount" => $applyDiscount
        ];
    }

    public function validateSpecialPrice($from_date, $to_date, $special_price){

        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/validateSP.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $flag = false;

        $logger->info('<br>');
        $logger->info("from_date ==> " . $from_date);
        $logger->info('<br>');
        $logger->info("to_date ==> " . $to_date);
        $logger->info('<br>');
        $logger->info("special price ==> " . $special_price);

        # if values are empty
        if(empty($from_date) || empty($to_date) || empty($special_price)) {
            $logger->info("special price required values are empty.");
            return $flag;
        }

        /* Current Date and Time */
        $current_date_time = date('Y-m-d H:i:s');
        $current_date_array = explode(' ', $current_date_time);

        $special_from_date  = $from_date;            
        $special_to_date    = $to_date;
        $special_to_date_array = explode(' ', $special_to_date);
        $current_day      = date_create($current_date_array[0]);
        $special_to       = date_create($special_to_date_array[0]);
        $date_difference  = date_diff($current_day, $special_to);
        $final_difference = $date_difference->format("%R%a");
        
        
        $logger->info('<br>');
        $logger->info(' diff => '.$final_difference);

        if (!empty($special_price)) {

            # if date difference id in -(ve) than assign empty value.
            # else if difference in +(ve) than assing the calculated discount.
            if ($final_difference >= 0) {
                # discount applicable
                $logger->info(' discount => YES');
                $flag = true;
            }
            elseif ($final_difference < 0) {
                # discount not applicable
                $logger->info(' discount => NO');
                $flag = false;
            }
            else{
                # if anything falls into than skip...
                $logger->info(' discount => ELSE CONDITION');
                // $flag = false;
            }
        }

        return $flag;
    }

}