<?php
namespace Retail\OutofstockNotify\Model;

class Notify extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\OutofstockNotify\Model\ResourceModel\Notify');
    }
}
