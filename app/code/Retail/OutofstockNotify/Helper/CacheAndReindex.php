<?php
/**
 * Re-index and Cache flush
 * 
 */
namespace Retail\OutofstockNotify\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class CacheAndReindex extends AbstractHelper
{
    protected $_indexerFactory;


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory
    ) 
    {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_indexerFactory = $indexerFactory;
    }

    public function reindexAll() {

        /* reindexs the following category */
        $indexer_ids = [
            //"design_config_grid",
            //"customer_grid",
            "catalog_category_product",
            "catalog_product_category",
            "catalog_product_price",
            "catalog_product_attribute",
            "catalogsearch_fulltext",
            "cataloginventory_stock",
            //"catalogrule_rule",
            //"catalogrule_product"
        ];
        
        foreach ($indexer_ids as $indexerId) {

            $indexerFactoryObject = $this->_indexerFactory->create();
            $indexerFactoryObject->load($indexerId);
            $indexerFactoryObject->reindexAll();

            // destroy object
            unset($indexerFactoryObject);
        }
        
        return $this;
    }

    public function cacheFlush() {
        
        /*$cacheTypes = [
            'config',
            'layout',
            'block_html',
            'collections',
            'reflection',
            'db_ddl',
            'eav',
            'config_integration',
            'config_integration_api',
            'full_page',
            'translate',
            'config_webservice'
        ];*/

        $typeCollection = $this->_cacheTypeList->getTypes();
        
        foreach ($typeCollection as $type) {
            $this->_cacheTypeList->cleanType($type->getId());
        }
        
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }

}