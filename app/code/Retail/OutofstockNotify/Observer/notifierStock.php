<?php
namespace Retail\OutofstockNotify\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Bootstrap;
use \Magento\Framework\App\Config\ScopeConfigInterface;
 
class notifierStock implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager; 

    protected $_notifyFactory; 

    protected $_productRepository; 

    protected $_transportBuilder;

    protected $_cacheAndReindexHelper;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Retail\OutofstockNotify\Model\NotifyFactory $nt,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        \Retail\OutofstockNotify\Helper\CacheAndReindex $cacheAndReindexHelper
    ) {
        $this->_notifyFactory = $nt;
        $this->_transportBuilder = $transportBuilder;
        $this->_objectManager = $objectManager;
        $this->_productRepository = $productRepository;
        $this->scopeConfig = $scopeConfig;
        $this->_cacheAndReindexHelper = $cacheAndReindexHelper;
    }
 
    /**
     * Send Email after CSV product update
     * 
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    { 
      /** Call Helper for Reindex */
      $this->_cacheAndReindexHelper->reindexAll();

      /** Call Helper for Cache Flush */
      $this->_cacheAndReindexHelper->cacheFlush();
      
      $product_ids = array();
      $stock_array = array();
      $data = $observer->getBunch();
      foreach($data as $val){
        if(array_key_exists('is_in_stock', $val)){
                if($val['sku']){
                 $productId = $this->_objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($val['sku']);
                /* main code starts */
                $model = $this->_notifyFactory->create();
                $collection = $model->getCollection();
                if(!empty($collection->getData())){
                  foreach($collection->getData() as $val){
                     array_push($stock_array, $val['product_id']);
                  }
                }
                foreach ($stock_array as $val){
                  $collection->addFieldToFilter('product_id', $val); 
                  if (!empty($collection->getData())) {
                    foreach($collection as $rtu){
                      $model->load($rtu['id'],'id');
                       $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($rtu['product_id']);
                         $cust_email = $rtu['customer_mail'];
                         $transport = $this->_transportBuilder->setTemplateIdentifier(14)
                            ->setTemplateOptions(['area' => 'frontend', 'store' =>1])
                            ->setTemplateVars(
                                [
                                     'product_title'=> $product->getName(),
                                     'product_url' => $product->getProductUrl()
                                ]
                            )
                            ->setFrom('general')
                            ->addTo($cust_email)
                            ->getTransport();
                            $transport->sendMessage();
                        echo "send the email";
                        $model->delete();
                    }
                  }
                }
        /* main code ends */

                }
      }
    }
    }
}
?>