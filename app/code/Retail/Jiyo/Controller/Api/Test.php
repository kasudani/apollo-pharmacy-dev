<?php
namespace Retail\Jiyo\Controller\Api;

ob_start();
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Test extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

    protected $_objectManager;  

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		parent::__construct($context);
	}
	public function execute()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();
     $api_secret = '14c958c91058b4c6632bc3364340a615';
	 $api_key = 'APOLLO_PHARMACY';
	 $category = $this->getRequest()->getPost('address');
	 $data = "category=".$category; 
	 $ch = curl_init('https://api.jiyo.com/v3/public/bit-list?'.$data);
	 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'API-KEY: ' . $api_key,
				'API-SECRET: ' . $api_secret
				));
	 curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$response = curl_exec($ch);
	$final_res = json_decode($response);
	$len = count($final_res->data);

   /* article details */
    $data1 = "category=".$category;
	$ch1 = curl_init('https://api.jiyo.com/v3/public/article-list/?'.$data1);
	 curl_setopt($ch1, CURLOPT_HTTPHEADER, array(
				'API-KEY: ' . $api_key,
				'API-SECRET: ' . $api_secret
				));
	 curl_setopt($ch1,CURLOPT_RETURNTRANSFER,1);
	$response1 = curl_exec($ch1);
	$final_res1 = json_decode($response1);
	$len1 = count($final_res1->data);
	$content = '<div class="tab-content">
				  <div class="tab-pane active" id="articles">';
				  for($j=0; $j<$len1; $j++){ 
	$content.='<div  class="row box-effect" style="margin-top: 6px;">
				  <div class="col-sm-4">
				  <a style="text-decoration:none;" href="'.$base_url.'"jiyo/description/description/?id='.$final_res1->data[$j]->id.'">
					<img src="'.$final_res1->data[$j]->teaser_image->preview.'"  class="article-img-size img-responsive">
					</a>
					</div>
				  <div class="col-sm-8">
					<span style="font-weight:bold;"><a href="'.$base_url.'"jiyo/description/description/?id='.$final_res1->data[$j]->id.'">'.$final_res1->data[$j]->title.'</a></span><br> 
					<p style="line-height: 1.5em;height: 4.3em;overflow: hidden;margin-top: 8px;">
					<a style="text-decoration:none;" href="'.$base_url.'"jiyo/description/description/?id='.$final_res1->data[$j]->id.'">'.$final_res1->data[$j]->description.'</a></p>
					</div>
				</div>';
				 } 
			$content.='</div>
			             <div class="tab-pane" id="videos">';
				  	for($i=0; $i<$len; $i++){ 
				  		$img_type = $final_res->data[$i]->media_type;
                        if($img_type == "video"){
			$content.='<div  class="row box-effect" style="margin-top: 6px;">
				  <div class="col-sm-8">
					<span style="font-weight:bold;"><a href="'.$base_url.'"jiyo/description/description/?id='.$final_res->data[$i]->article_id.'">'.$final_res->data[$i]->title.'</a></span><br>
                    <p style="line-height: 1.5em;height: 4.3em;overflow: hidden;margin-top: 8px;">
					<a style="text-decoration:none;" href="'.$base_url.'"jiyo/description/description/?id='.$final_res->data[$i]->article_id.'">'.$final_res->data[$i]->description.'</a></p>
					</div>
					<div class="col-sm-4">
					<a style="text-decoration:none;" href="'.$base_url.'"jiyo/description/description/?id='.$final_res->data[$i]->article_id.'">
					    <img src="'.$final_res->data[$i]->teaser_image->preview.'"  class="bits-img-size img-responsive">
					    </a>
					</div>
					
				</div>';
				 }
				 // else{
					// if($i == 0){
					// 	    echo "<span class='no_data'>No data available!</span>";
					// } 	} 
					} 		
			$content.='</div>
			</div>';
    echo $content;
    }	
}
