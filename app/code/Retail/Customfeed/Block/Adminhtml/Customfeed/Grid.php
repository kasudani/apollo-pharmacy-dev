<?php
namespace Retail\Customfeed\Block\Adminhtml\Customfeed;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Retail\Customfeed\Model\customfeedFactory
     */
    protected $_customfeedFactory;

    /**
     * @var \Retail\Customfeed\Model\Status
     */
    protected $_status;
    protected $scopeConfig;
    protected $_product;
    protected $_productCollectionFactory;
    protected $_categoryFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Retail\Customfeed\Model\customfeedFactory $customfeedFactory
     * @param \Retail\Customfeed\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Retail\Customfeed\Model\CustomfeedFactory $CustomfeedFactory,
        \Retail\Customfeed\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->_customfeedFactory = $CustomfeedFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_product = $product;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->setFilterVisibility(false);
        parent::__construct($context, $backendHelper, $data);
    }
    public function getStockItem($productId)
    {
        return $this->_stockItemRepository->get($productId);
    }
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $params=$this->getRequest()->getParams();
        
        # Custom Feed table collection
        $collection = $this->_customfeedFactory->create()->getCollection();

        
        /* Approved or Disapproved */
        if(array_key_exists("id",$params))
        {
          if( $params['id']==1){
                $collection->addFieldToFilter("status","approved");
          }
          else if( $params['id']==2){
                $collection->addFieldToFilter("status","disapproved");
          }
        }

        /* Brand Selection */
        if (array_key_exists("brand", $params)) {
           # code...
          $collection->addFieldToFilter("attribute_option_id",$params["brand"]);
        }

        /* Check Stock */
        if(array_key_exists("stock",$params))
        {
          if( $params['stock']=="in"){
                $collection->addFieldToFilter("inventory",array("gt" => 0));
          }
          else if( $params['stock']=="out"){
                $collection->addFieldToFilter("inventory",array("lteq" => 0));
          }
        }

        /* category selection */
        if (array_key_exists("category", $params)) {
           # code...
           $productArray = $this->categoryCollection($params['category']);
           # test add to filter
           $collection->addFieldToFilter('sku', array('in' => $productArray ));
        }

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
       		
		$this->addColumn(
			'sku',
			[
				'header' => __('Sku'),
				'index' => 'sku',
                'renderer'  => 'Retail\Customfeed\Block\Adminhtml\Customfeed\Renderer\SkuRenderer'
			]
		);
		
		$this->addColumn(
			'name',
			[
				'header' => __('Name'),
				'index' => 'name',
			]
		);
		
		$this->addColumn(
			'inventory',
			[
				'header' => __('Inventory'),
				'index' => 'inventory',
                'renderer'  => 'Retail\Customfeed\Block\Adminhtml\Customfeed\Renderer\InventoryRenderer'
			]
		);
		
		$this->addColumn(
			'price',
			[
				'header' => __('Price'),
				'index' => 'price',
                'renderer'  => 'Retail\Customfeed\Block\Adminhtml\Customfeed\Renderer\PriceRenderer'
			]
		);

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
            ]
        );

        $this->addColumn(
            'notes',
            [
                'header' => __('Notes'),
                'index' => 'notesnew',
                'filter' =>false,
                'renderer' => 'Retail\Customfeed\Block\Adminhtml\Customfeed\Edit\Tab\Renderer\Savenotes',
            ]
        );
				 
	
	   $this->addExportType($this->getUrl('customfeed/*/exportCsv', ['_current' => true]),__('CSV'));
	   $this->addExportType($this->getUrl('customfeed/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        
        $this->getMassactionBlock()->setFormFieldName('customfeed');

        $this->getMassactionBlock()->addItem(
            'approve',
            [
                'label' => __('Approved'),
                'url' => $this->getUrl('customfeed/*/massApprove'),
                'confirm' => __('Are you sure?')
            ]
        );


        $this->getMassactionBlock()->addItem(
            'disapprove',
            [
                'label' => __('Disapproved'),
                'url' => $this->getUrl('customfeed/*/massDisapprove', ['_current' => true]),
                'confirm' => __('Are you sure?')
                
            ]
        );
        $this->getMassactionBlock()->addItem(
            'savenote',
            [
                'label' => __('Save Notes'),
                'url' => $this->getUrl('customfeed/*/save', ['_current' => true]),
                'confirm' => __('Are you sure?')
                
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('customfeed/*/index', ['_current' => true]);
    }

    /**
     * @param \Retail\Customfeed\Model\customfeed|\Magento\Framework\Object $row
     * @return string
     */

    public function categoryCollection($category_id)
    {        
        # get the category id 
        # query the magento product collection with the category id
        # get the sku from the collection in a array
        # filter the custom feed collection by the sku array from magento product collection.
        $productCollection = $this->getCategoryProduct($category_id);
        # category sku array
        $categorySkuArray = [];
        # loop
        foreach ($productCollection as $product) {
            # code...
            array_push($categorySkuArray, $product->getSku());
        }

        return $categorySkuArray;

    }
	
    public function getCategoryProduct($categoryId)
    {
        $category = $this->_categoryFactory->create()->load($categoryId)
                                            ->getProductCollection()
                                            ->addAttributeToSelect('*');
        return $category;
    }

}