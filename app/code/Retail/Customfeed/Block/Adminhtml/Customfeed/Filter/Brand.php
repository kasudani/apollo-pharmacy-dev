<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Retail\Customfeed\Block\Adminhtml\Customfeed\Filter;

use Magento\Framework\DataObject;

class Brand extends \Magento\Backend\Block\Template
{
    protected $_product;
    protected $_productFactory;
    protected $_productCollectionFactory;
    protected $_storeManager;
    protected $_localeCurrency;
    protected $_eavConfig;
    protected $_urlBuilder;
    protected $_category;
    protected $_categoryRepository;


    public function __construct(
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\CurrencyInterface $currencyInterface,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Catalog\Helper\Category $category,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository
    )
    {
        $this->_product = $product;
        $this->_productFactory = $productFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_localeCurrency = $currencyInterface;
        $this->_eavConfig = $eavConfig;
        $this->_urlBuilder = $urlBuilder;
        $this->_category = $category;
        $this->_categoryRepository = $categoryRepository;
        
    }

    public function loadProduct($product_id)
    {
        $product = $this->_product->load($product_id);
        return $product;
    }

    public function getcurrencySymbol(){
        $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $currencySymbol = $this->_localeCurrency->getCurrency($currencyCode)->getSymbol();
        return $currencySymbol;
    }

    public function getBrands()
    {
        $attribute = $this->_eavConfig->getAttribute('catalog_product', 'brand');
        $options = $attribute->getSource()->getAllOptions();
        return $options;
    }

    public function filterClearUrl()
    {
        return $this->_urlBuilder->getUrl(
            'customfeed/*/index'
        );
    }

    public function urlParams()
    {
        $current_link = $this->current_url();
        $parts = parse_url($current_link);        
        return $parts;
    }

    public function current_url()
    {
        return $this->_urlBuilder->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
    }

    public function isRemoveFilterVisible()
    {
       
        $parts = $this->urlParams();

        if (array_key_exists('query', $parts)) {

            parse_str($parts['query'], $query);

            if (array_key_exists('brand', $query)) {    

                $price_query = $query['price'];
                $price_query_array = explode('-', $price_query);
                return true;

            }
            return false;
        }
        else
        {
            return false;
        }
    }

    public function storeCategory()
    {       
        return $this->_category->getStoreCategories();
    }

    public function categoryRepository($category_id)
    {
        return $this->_categoryRepository->get($category_id);        
    }
    
}