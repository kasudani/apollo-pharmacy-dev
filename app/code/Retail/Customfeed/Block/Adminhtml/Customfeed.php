<?php

namespace Retail\Customfeed\Block\Adminhtml;

class Customfeed extends \Magento\Backend\Block\Widget\Container
{
    /**
     * @var string
     */
    protected $_template = 'customfeed/customfeed.phtml';

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Widget\Context $context,array $data = [])
    {
        
        parent::__construct($context, $data);
    }

    /**
     * Prepare button and grid
     *
     * @return \Magento\Catalog\Block\Adminhtml\Product
     */
    protected function _prepareLayout()
    {		
        $addButtonProps = [
            'id' => 'customfeed-all',
            'label' => __('All'),
            'class' => 'action- scalable primary',
            'button_class' => '',
            'onclick' => "setLocation('" . $this->_getCreateUrl() . "')"
        ];
        $addButtonProps1 = [
            'id' => 'customfeed-refresh',
            'label' => __('Sync'),
            'class' => 'action- scalable primary',
            'button_class' => '',
            'onclick' => "setLocation('" . $this->_getRefreshUrl() . "')"
        ];
        $addButtonProps2= [
            'id' => 'customfeed-approve',
            'label' => __('Approved'),
            'class' => 'action- scalable primary',
            'button_class' => '',
            'onclick' => "setLocation('" . $this->_getApproveUrl() . "')"
        ];
        $addButtonProps3 = [
            'id' => 'customfeed-disapprove',
            'label' => __('Disapproved'),
            'class' => 'action- scalable primary',
            'button_class' => '',
            'onclick' => "setLocation('" . $this->_getDisapproveUrl() . "')"
        ];

        $this->buttonList->add('all', $addButtonProps);
        $this->buttonList->add('refresh', $addButtonProps1);        
        $this->buttonList->add('disapprove', $addButtonProps2);
        $this->buttonList->add('approve', $addButtonProps3);		
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Retail\Customfeed\Block\Adminhtml\Customfeed\Grid', 'retail.customfeed.grid')
        );
        return parent::_prepareLayout();
    }

    /**
     *
     *
     * @return array
     */
    protected function _getAddButtonOptions()
    {

        $splitButtonOptions[] = [
            'label' => __('All'),
            'onclick' => "setLocation('" . $this->_getCreateUrl() . "')"
        ];

        return $splitButtonOptions;
    }
    protected function _getRefreshButtonOptions()
    {

        $splitButtonOptions[] = [
            'label' => __('Refresh'),
            'onclick' => "setLocation('" . $this->_getRefreshUrl() . "')"
        ];

        return $splitButtonOptions;
    }
    protected function _getApproveButtonOptions()
    {

        $splitButtonOptions[] = [
            'label' => __('Approve'),
            'onclick' => "setLocation('" . $this->_getApproveUrl() . "')"
        ];

        return $splitButtonOptions;
    }
    protected function _getDisapproveButtonOptions()
    {

        $splitButtonOptions[] = [
            'label' => __('Disapprove'),
            'onclick' => "setLocation('" . $this->_getDisapproveUrl() . "')"
        ];

        return $splitButtonOptions;
    }

    /**
     *
     *
     * @param string $type
     * @return string
     */
    protected function _getCreateUrl()
    {
        return $this->getUrl(
            'customfeed/*/index'
        );
    }
    protected function _getRefreshUrl()
    {
        return $this->getUrl(
            'customfeed/*/newaction'
        );
    }
    protected function _getApproveUrl()
    {
        return $this->getUrl(
            'customfeed/*/index/id/1'
        );
    }
    protected function _getDisapproveUrl()
    {
        return $this->getUrl(
            'customfeed/*/index/id/2'
        );
    }

    /**
     * Render grid
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

}