<?php
namespace Retail\Customfeed\Model;

class Customfeed extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\Customfeed\Model\ResourceModel\Customfeed');
    }
}
?>