<?php
namespace Retail\RefillReminder\Block\Adminhtml\Reminder;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Retail\RefillReminder\Model\reminderFactory
     */
    protected $_reminderFactory;

    /**
     * @var \Retail\RefillReminder\Model\Status
     */
    protected $newstatus;
    protected $_product;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Retail\RefillReminder\Model\reminderFactory $reminderFactory
     * @param \Retail\RefillReminder\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Retail\RefillReminder\Model\ReminderFactory $ReminderFactory,
        \Retail\RefillReminder\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        \Magento\Catalog\Model\Product $_product,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $newstatus,
        array $data = []
    ) {
          $this->_product = $_product;
         $this->_eavAttribute = $eavAttribute;
        $this->_reminderFactory = $ReminderFactory;
        $this->newstatus = $newstatus;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        
         $attributeId = $this->_eavAttribute->getIdByCode('catalog_product', 'status');
        
        $collection = $this->_reminderFactory->create()->getCollection();
         $collection->addFieldToFilter('is_in_cart',1);
        
         $collection->addFieldToFilter('start_date', array('neq' => NULL));

         $collection->addFieldToFilter('notify_date', array('neq' => NULL));

         $collection->addFieldToFilter('flag', array('neq' => NULL));
        $collection->getSelect()->joinLeft( 
     ['product_entity' => $collection->getTable('catalog_product_entity')],

     'main_table.product_id = product_entity.entity_id',

     [ 'sku']

       );
       $collection->getSelect()->joinLeft( 
     ['customer_entity' => $collection->getTable('customer_entity')],
     'main_table.customer_id = customer_entity.entity_id',
     ['email','firstname','lastname','default_shipping']
      );
      $collection->getSelect()->joinLeft( 
     ['catalog_product_entity_int' => $collection->getTable('catalog_product_entity_int')],
     'main_table.product_id = catalog_product_entity_int.entity_id',
     ['value']
      )->where("catalog_product_entity_int.attribute_id='".$attributeId."'");

     $collection->getSelect()->joinLeft( 
     ['customer_address_entity' => $collection->getTable('customer_address_entity')],
     'customer_entity.default_shipping = customer_address_entity.entity_id',
     ['telephone']
      );
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn('value', 
         [
        'header' => __('Product Status'),
        'index' => 'value',
        'type' => 'options',
        'options' => $this->newstatus->getOptionArray(),
        'renderer'  =>'Retail\RefillReminder\Block\Adminhtml\Reminder\Edit\Tab\Renderer\Product',
        'filter_condition_callback' => [$this, '_filterCollection'],
       ]
    );

        
                $this->addColumn(
                    'sku',
                    [
                        'header' => __('Product Sku'),
                        'index' => 'sku',
                        'type'=>'text'
                    ]
                );
                 $this->addColumn(
                    'quantity',
                    [
                        'header' => __('Quantity'),
                        'index' => 'quantity',
                        'type'=>'text'
                    ]
                );
                $this->addColumn(
                    'email',
                    [
                        'header' => __('Email Id'),
                        'index' => 'email',
                        'type'=>'text'
                    ]
                );
                    $this->addColumn(
                    'firstname',
                    [
                        'header' => __('Customer First Name'),
                        'index' => 'firstname',
                        'type'=>'text',
                        'filter' => false
                    ]
                );
                    $this->addColumn(
                    'lastname',
                    [
                        'header' => __('Customer Last Name'),
                        'index' => 'lastname',
                        'type'=>'text',
                        'filter' => false
                    ]
                );
                $this->addColumn(
                    'telephone',
                    [
                        'header' => __('Telephone'),
                        'index' => 'telephone',
                        'type'=>'text'
                    ]
                );
                
                $this->addColumn(
                    'frequency',
                    [
                        'header' => __('Frequency(in days)'),
                        'index' => 'frequency',
                    ]
                ); 
                /*$this->addColumn(
                    'value',
                    [
                        'header' => __('Product Status'),
                        'index' => 'value',
                    ]
                );*/ 
                
                
                /*$this->addColumn(
                    'start_date',
                    [
                        'header' => __('Start Date'),
                        'index' => 'start_date',
                    ]
                );*/
                
                /*$this->addColumn(
                    'flag',
                    [
                        'header' => __('Flag'),
                        'index' => 'flag',
                    ]
                );*/
                
                /*$this->addColumn(
                    'notify_date',
                    [
                        'header' => __('Notify Date'),
                        'index' => 'notify_date',
                    ]
                );*/
                


        
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
        

        
           $this->addExportType($this->getUrl('refillreminder/*/exportCsv', ['_current' => true]),__('CSV'));
           $this->addExportType($this->getUrl('refillreminder/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

     protected function _filterCollection($collection, $column)
        {
            $value = trim($column->getFilter()->getValue());
           
            if($value==1){ 

            $collection->addFieldToFilter('value',1);
            }
            elseif($value==2){ 
            $collection->addFieldToFilter('value',0);
            }
            return $this;
        }

    
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('Retail_RefillReminder::reminder/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('reminder');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('refillreminder/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        //$statuses = $this->_status->getOptionArray();



        return $this;
    }
        

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('refillreminder/*/index', ['_current' => true]);
    }

    /**
     * @param \Retail\RefillReminder\Model\reminder|\Magento\Framework\Object $row
     * @return string
     */
    

    

}