<?php 
namespace Retail\RefillReminder\Block\Adminhtml\Reminder\Edit\Tab\Renderer; 
use Magento\Framework\DataObject;
 class Product extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    
    public function __construct(
	\Magento\Catalog\Model\ProductFactory $_productloader
    ) {
	   $this->_productloader = $_productloader;
     }
 
    
    public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($id);
    }
    public function render(DataObject $row)
    {
        $product_id = $row->getData('product_id');
          $product=$this->getLoadProduct($product_id);
		  if( $product->getStatus()==1)
          {
             echo "Enabled";
          }
          else
          {
            echo "Disabled";
          }
    }
}