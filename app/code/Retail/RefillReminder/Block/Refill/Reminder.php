<?php 
namespace Retail\RefillReminder\Block\Refill;
class Reminder extends \Magento\Framework\View\Element\Template
{
protected $_coreRegistry;
protected $_reminderFactory;
protected $_session;
protected $_urlInterface;

protected $_customerSession;

public function __construct(
\Magento\Framework\View\Element\Template\Context $context,
\Magento\Framework\Registry $registry,
\Magento\Customer\Model\Session $session,
\Retail\RefillReminder\Model\ReminderFactory $cu,
\Magento\Framework\UrlInterface $urlInterface,
\Magento\Customer\Model\SessionFactory $customerSession  
) {
	
		parent::__construct($context);
	
         $this->_customerSession = $customerSession->create();	 
    $this->_coreRegistry = $registry;
		$this->_session = $session;
		  $this->_urlInterface = $urlInterface;
	$this->_reminderFactory = $cu;
}
     public function getBaseUrl()
     {
     return  $this->_urlInterface->getBaseUrl();
 }
  
	public function getProductData()
	{
		return $this->_coreRegistry->registry('current_product');
	}
	 public function isCustLoggedin()
	 {
		 if($this->_customerSession->isLoggedIn()){
		 return $this->_customerSession->isLoggedIn();
		 }
		  else{
			   return 0;
		  }
	 }
	public function getCustomData()
	{
			  $product_id= $this->getProductData()->getId();
			   
			  
			  $model=$this->_reminderFactory->create();
				   $collection=$model->getCollection();
				   
				  $collection->addFieldToFilter('product_id',$product_id);
				  $collection->addFieldToFilter('customer_id', $this->_customerSession->getCustomer()->getId());
				  $collection->addFieldToFilter('is_in_cart',1); 
					if(!empty($collection->getData())){
						 return $collection->getData();
					}
	}
}