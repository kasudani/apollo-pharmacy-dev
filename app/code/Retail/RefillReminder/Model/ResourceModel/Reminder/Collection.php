<?php

namespace Retail\RefillReminder\Model\ResourceModel\Reminder;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\RefillReminder\Model\Reminder', 'Retail\RefillReminder\Model\ResourceModel\Reminder');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>