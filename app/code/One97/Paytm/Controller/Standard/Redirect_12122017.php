<?php

namespace One97\Paytm\Controller\Standard;

class Redirect extends \One97\Paytm\Controller\Paytm
{
    public function execute()
    {
        $order = $this->getOrder();
        $orderId = $order->getId();
        $this->updateOrderStatus($order,"pending_payment");
        $order->setStatus($order::STATUS_FRAUD);
        if ($order->getBillingAddress())
        {
            $this->getResponse()->setRedirect(
                $this->getPaytmModel()->buildPaytmRequest($order)
            );
        }
        else
        {
            $this->_cancelPayment();
            $this->_paytmSession->restoreQuote();
            $this->getResponse()->setRedirect(
                $this->getPaytmHelper()->getUrl('checkout')
            );
        }
    }
    public function updateOrderStatus($order,$orderStatus){
        $order->setStatus($orderStatus);
        $order->save();
        /*$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $salesTable = $resource->getTableName('sales_order');
        $sql = "Update  $salesTable Set state = '".$orderStatus."',status = '".$orderStatus."' where entity_id = $orderId";
        $connection->query($sql);*/
        return true;
    }
}