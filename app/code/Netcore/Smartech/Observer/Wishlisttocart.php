<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

class Wishlisttocart implements ObserverInterface
{    

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;
    protected $_queryFactory;
    protected $_catalogSession;
    protected $_request;

    private $_objectManager;

    /**
     * @var
     */
    protected $_cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Search\Model\QueryFactory $queryFactory,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->_queryFactory = $queryFactory;
        $this->_objectManager = $objectmanager;
        $this->_catalogSession = $catalogSession;
        $this->_cookieManager = $cookieManager;
        $this->_sessionManager = $sessionManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->config = $config;
    }

    public function create() {
        return $this->_objectManager->create(\Magento\Wishlist\Model\Item::class);
    }

    /* Event handled for move product from wishlist to cart, thus addition of product to cart and removal from wishlist */
    public function execute(EventObserver $observer)
    {
        $item = (int) $this->_request->getParam('item');
        $qty = $this->_request->getParam('qty');
        if ($item) {
            try {
                $itemData = $this->create()->load($item);
                if (!empty($itemData) && $itemData->getData() != '') {
                    $productId = $itemData->getProductId();
                    if ($productId) {
                        $this->_catalogSession->setSmWRemoveProd($productId);
                        $this->_catalogSession->setSmacProd($productId);
                        if ($qty > 1) {
                            $this->_catalogSession->setSmWRemoveQty($qty);
                            $this->_catalogSession->setSmacQty($qty);
                        }
                        // cookie set to dispatch the add to crat event
                        $publicCookieMetadata = $this->_cookieMetadataFactory->createPublicCookieMetadata()
                            ->setDuration(259200)
                            ->setPath($this->_sessionManager->getCookiePath())
                            ->setDomain($this->_sessionManager->getCookieDomain())
                            ->setHttpOnly(false);

                        $this->_cookieManager->setPublicCookie("Addcartflag", 'addcartsuccess', $publicCookieMetadata
                        );
                    }
                }
            } catch (Exception $e) {
                
            }
        }
    }   
}