<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerLogin implements ObserverInterface
{
	/* Netcore Smartech Api */
    CONST API_KEY = '43d4b598b63becd8952b2e2ce1978344';
    CONST TYPE = 'contact';
    CONST ACTIVITY = 'update'; // customer login..
    CONST LIST_ID = '3'; // customer login..

    protected $_curl;

    protected $_customerModel;

    protected $_session;

    
    public function __construct( 
    	\Magento\Customer\Model\Session $session,
    	\Magento\Customer\Model\Customer $customerModel,       
        \Magento\Framework\HTTP\Client\Curl $curl
    )
    {
        $this->_curl = $curl;
        $this->_customerModel = $customerModel;
        $this->_session = $session;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $customer = $observer->getEvent()->getCustomer();
        $customerName = $customer->getName(); //Get customer name
        $email = $customer->getEmail();
        $customerId = $customer->getId();
        

        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/login_observer.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $logger->info(" ");
        $logger->info("customer_login_observer_started");
        $logger->info("=================================");

        
        /* Data params */
        // CUST_ID, FIRSTNAME, LASTNAME, EMAIL, MOBILE

        $DATA = '{"CUST_ID":"'.$customerId.'","EMAIL": "'.$email.'"}';
        
        // /* API URL */
        $api = 'http://api.netcoresmartech.com/apiv2?apikey='.self::API_KEY.'&type='.self::TYPE.'&activity='.self::ACTIVITY.'&data='.$DATA.'&listid='.self::LIST_ID.' ';
        
        /* Form the CURL request */
        try 
        {
            $curl_url = curl_init($api);

            curl_setopt($curl_url, CURLOPT_POST, TRUE);
            curl_setopt($curl_url, CURLOPT_RETURNTRANSFER, true);

            $curl_response1 = curl_exec($curl_url);
            
            $logger->info($curl_response1); 
            $logger->info($DATA); 
            $logger->info($api);            
        } 
        catch (\Exception $e) 
        {
            $logger->info($e->getMessage());
        }

        $logger->info("=================================");
        $logger->info("customer_login_observer_ended");
        
       
    }
}