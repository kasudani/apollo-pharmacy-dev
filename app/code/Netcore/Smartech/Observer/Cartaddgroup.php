<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

class Cartaddgroup implements ObserverInterface
{    

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;
    protected $_queryFactory;
    protected $_request;
    protected $_orderItems;
    protected $_catalogSession;

    /**
     * @var
     */
    protected $_cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Search\Model\QueryFactory $queryFactory,
        \Netcore\Smartech\Model\OrderItems $orderItems,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->_queryFactory = $queryFactory;
        $this->config = $config;
        $this->_catalogSession = $catalogSession;
        $this->_orderItems = $orderItems;
        $this->_cookieManager = $cookieManager;
        $this->_sessionManager = $sessionManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
    }

    public function execute(EventObserver $observer)
    {
        $order_items = $this->_request->getParam('order_items');
        if (!empty($order_items)) {
            $itemId = $order_items[0];
            // Model cal - OrderItems
            $item = $this->_orderItems->getOrderItemData($itemId);
            if (!empty($item->getData())) {
                $productId = $item->getProductId();
                if ($productId) {
                    $this->_catalogSession->setSmacProd($productId);
                    // cookie set to dispatch the add to cart event
                    $publicCookieMetadata = $this->_cookieMetadataFactory->createPublicCookieMetadata()
                        ->setDuration(259200)
                        ->setPath($this->_sessionManager->getCookiePath())
                        ->setDomain($this->_sessionManager->getCookieDomain())
                        ->setHttpOnly(false);

                    $this->_cookieManager->setPublicCookie("Addcartflag", 'addcartsuccess', $publicCookieMetadata
                    );
                }
            }
        }
    }

}