<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

use \Magento\Checkout\Model\Session as CheckoutSession;

class Checkoutcartdelete implements ObserverInterface
{    

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;

    /**
     * @var
     */
    protected $_cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    protected $_sessionManager;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /** @var CheckoutSession */
    protected $_checkoutSession;

    protected $_request;
    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Netcore\Smartech\Helper\Data $helper,
        \Magento\Catalog\Model\Session $catalogSession,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->config = $config;
        $this->_catalogSession = $catalogSession;
        $this->helper = $helper;
    }

    public function execute(EventObserver $observer)
    {
        $itemId = $this->_request->getParam('id');
        if ($itemId) {
            // Handled for remove cart delete item thorugh redirection
            $this->_catalogSession->setCartRemove('remove');
            $qty = $this->helper->getQuoteItemQty($itemId);
            $this->_catalogSession->setCartRemoveQty($qty);
        }
    }

}