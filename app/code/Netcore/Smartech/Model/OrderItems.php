<?php

namespace Netcore\Smartech\Model;

class OrderItems
{

    protected $_orderRepository;
    protected $_itemRepository;

    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\OrderItemRepositoryInterface $itemRepository
    )
    {
        $this->_orderRepository = $orderRepository;
        $this->_itemRepository = $itemRepository;
    }

    /* Get Order data */

    public function getOrder($orderId)
    {
        try {
            $order = $this->_orderRepository->get($orderId);
            return $order;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return [];
        }
    }

    /* Get Order Items */

    public function getOrderItems($orderId)
    {
        try {
            $order = $this->_orderRepository->get($orderId);
            $items = $order->getAllItems();
            return $items;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return [];
        }
    }

    /* Get Item data through id */

    public function getOrderItemData($itemId)
    {
        try {
            $item = $this->_itemRepository->get($itemId);
            return $item;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return [];
        }
    }

}
