<?php

namespace Netcore\Smartech\Model;

class Customer
{
    protected $_customerFactory;

    public function __construct(\Magento\Customer\Model\CustomerFactory $customerFactory)
    {
        $this->_customerFactory = $customerFactory;
    }

    public function getCustomer($email)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $url = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');

        // If not area code set 

        $websiteId = $storeManager->getWebsite()->getWebsiteId();
        // Get Store ID
        $store = $storeManager->getStore();
        $storeId = $store->getStoreId();
        $customer = $this->_customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($email);
        return $customer;
    }

}
