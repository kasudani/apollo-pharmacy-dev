require(['jquery', 'jquery/jquery.cookie', 'domReady!'], function ($) {
    var config = {
        apiKey: $.cookie('BPN_API_KEY'),
        messagingSenderId: $.cookie('BPN_SENDER'),
        user_key: $.cookie('SM_WEB_ID'),
        siteid: $.cookie('SM_SITE_ID'),
    };
    importScripts('//tw.netcore.co.in/sw.js');
});
