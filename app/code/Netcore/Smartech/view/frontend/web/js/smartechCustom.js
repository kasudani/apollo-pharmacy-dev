require(['jquery', 'jquery/jquery.cookie', 'domReady!'], function ($) {
    $(document).ajaxSuccess(function (event, xhr, settings) {
        var str = settings.url;
        var cart = str.includes('/cart/add');
        var minicart = str.includes('customer/section/load');
        var removeItem = str.includes('/sidebar/removeItem');
        if (settings.type === 'GET' && minicart) {
            if ($.cookie('Addcartflag') !== null && $.cookie('Addcartflag') === 'addcartajax') {
                var myJsonString = $.cookie('SmCartJson') || '';
                if (typeof myJsonString === 'string') {
                    myJsonString = JSON.parse(myJsonString);
                }
                smartech('dispatch', 2, myJsonString);
                console.log('Cart add Ajax Success ');
                console.log(myJsonString);
                delete_cookie('Addcartflag');
                delete_cookie('SmCartJson');
            }
        }
        if (settings.type === 'POST' && settings.processData && removeItem) {
            if ($.cookie('Removecartflag') === 'removetItemSidebar') {
                var myJsonRemoveCart = $.cookie('SmRemoveCartJson');
                if (typeof myJsonRemoveCart === 'string') {
                    myJsonRemoveCart = JSON.parse(myJsonRemoveCart);
                }
                smartech('dispatch', 5, myJsonRemoveCart);
                console.log(myJsonRemoveCart);
                console.log('Remove Item Ajax Success');
                delete_cookie('SmRemoveCartJson');
                delete_cookie('Removecartflag');
            }
        }
    });

    function delete_cookie(name) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
});
