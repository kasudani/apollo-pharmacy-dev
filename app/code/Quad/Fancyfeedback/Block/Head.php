<?php
namespace Quad\Fancyfeedback\Block;
class Head extends \Magento\Framework\View\Element\Template
{
	public $assetRepository;
	public $_objectManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context, 
        array $data = [],    
        \Magento\Framework\View\Asset\Repository $assetRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->assetRepository = $assetRepository;
        $this->_objectManager=$objectManager;
        return parent::__construct($context, $data);
    }

    public function isEnabled()
    {
    	return $this->_objectManager->get('Quad\Fancyfeedback\Helper\Data')->getConfigValue('ws_fdb/feedback_group/ws_fdb_enabled');
    }
}
