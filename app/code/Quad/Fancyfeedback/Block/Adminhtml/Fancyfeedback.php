<?php
namespace Quad\Fancyfeedback\Block\Adminhtml;
class Fancyfeedback extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_fancyfeedback';/*block grid.php directory*/
        $this->_blockGroup = 'Quad_Fancyfeedback';
        $this->_headerText = __('Fancyfeedback');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();
		
    }
}