<?php
namespace Quad\Fancyfeedback\Controller\Adminhtml\Fancyfeedback;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
	public function execute()
    {		
        $data = $this->getRequest()->getParams();
        if ($data) {
            $model = $this->_objectManager->create('Quad\Fancyfeedback\Model\Fancyfeedback');
		
			$id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }
			
            $model->setData($data);
			
            try {
                $model->save();
                $this->messageManager->addSuccess(__('The feedback has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);

                try {
                    if (isset($data['send_email']) && $data['send_email'] == 1) {
                        $this->_objectManager->get('Quad\Fancyfeedback\Helper\Data')->sendMailToCustomer($model->load($id));
                    }
                } catch (\Exception $e) {
                }

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current' => true));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the feedback.'));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('banner_id' => $this->getRequest()->getParam('banner_id')));
            return;
        }
        $this->_redirect('*/*/');
    }
}
