<?php
namespace Quad\Fancyfeedback\Controller\Index;

class Submit extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
        $error_msg = 'Oops! There was a glitch.';

        
        if (!$this->getRequest()->isAjax() || !$this->getRequest()->getRequestUri() == '/fancyfeedback/index/submit/') {
            echo json_encode(array('status' => 'error', 'message' => $error_msg));
            die();
        }

        $post = $this->getRequest()->getPost();
        array_walk_recursive($post, array($this, 'sanitize'));

        try {
            $model = $this->_objectManager->create('Quad\Fancyfeedback\Model\Fancyfeedback');
            $fb = $model
                ->setData('name', $post->name)
                ->setData('email', $post->email)
                ->setData('phone', $post->phone)
                ->setData('message', $post->message)
                ->setData('created_at', date('Y-m-d H:i:s'))
                ->setData('updated_at', date('Y-m-d H:i:s'))
                ->save();
            if ($fb && $fb->getData('id')) {

                echo json_encode(array(
                    'status' => 'success',
                    'message' => 'Your feedback has been received.'
                ));

                try {
                    $this->_objectManager->get('Quad\Fancyfeedback\Helper\Data')->sendMailToAdmin($post);
                } catch (Exception $e) {
                    // var_dump($e->getMessage());
                }

            } else {
                echo json_encode(array('status' => 'error', 'message' => $error_msg));
            }
        } catch (\Magento\Framework\Model\Exception $e) {
            echo json_encode(array('status' => 'error', 'message' => $error_msg));
        } catch (\RuntimeException $e) {
            echo json_encode(array('status' => 'error', 'message' => $error_msg));
        } catch (\Exception $e) {
            echo json_encode(array('status' => 'error', 'message' => $error_msg));
        }
    }

    protected function sanitize(&$item, $key)
    {
        $item = strip_tags(urldecode(trim($item)));
    }
}
