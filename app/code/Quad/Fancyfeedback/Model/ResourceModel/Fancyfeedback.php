<?php
namespace Quad\Fancyfeedback\Model\ResourceModel;

/**
 * Fancyfeedback resource
 */
class Fancyfeedback extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('fancyfeedback', 'id');
    }

  
}
