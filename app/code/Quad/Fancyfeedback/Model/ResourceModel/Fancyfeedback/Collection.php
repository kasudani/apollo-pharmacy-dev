<?php
namespace Quad\Fancyfeedback\Model\ResourceModel\Fancyfeedback;

/**
 * Fancyfeedbacks Collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Quad\Fancyfeedback\Model\Fancyfeedback', 'Quad\Fancyfeedback\Model\ResourceModel\Fancyfeedback');
    }
}
