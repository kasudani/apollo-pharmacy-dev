define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'underscore',
		'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/step-navigator',
		'mage/url'
    ],
    function (
        $,
        ko,
        Component,
        _,
		customer,
        stepNavigator,
		url
    ) {
        'use strict';
        /**
        *
        * mystep - is the name of the component's .html template,
        * <Vendor>_<Module>  - is the name of the your module directory.
        *
        */  
			
        return Component.extend({
            defaults: {
                template: 'Retailinsights_GuestCheckoutStep/mystep'
            },
            //add here your logic to display step,
				    isVisible: ko.observable(false),
                    isPresent: !customer.isLoggedIn(),
					isEnterOtp: false,
            /**
			*
			* @returns {*}
			*/
            initialize: function () {	  
                this._super(); 				
				  if (performance.navigation.type == 1) 
				  {
					  var url=window.location.href;
				      var arr=url.split('#')[0];
				      window.location.replace(arr+"#shipping");
				  }	
                   if(!customer.isLoggedIn()){				  
                // register your step
                stepNavigator.registerStep(
                    //step code will be used as step content id in the component template
                    'guest_checkout',
                    //step alias
                    null,
                    //step title value
                    'Guest Checkout',
                    //observable property with logic when display step or hide step
                    this.isVisible,
                    _.bind(this.navigate, this),

                    /**
					* sort order value
					* 'sort order value' < 10: step displays before shipping step;
					* 10 < 'sort order value' < 20 : step displays between shipping and payment step
					* 'sort order value' > 20 : step displays after payment step
					*/
                    15
                );
                
                return this;
				   }
            },
			 generateotp: function(){
				 
				   if(!customer.isLoggedIn())
				 {
					     
				                var custom_url = url.build('ajaxotp/index/index');  
                                var email=$(".form-login input[name=username]").val();
                                var mobile= $("#guest-checkout-otp input[name=mobile_otp]").val();

					          if(mobile.length==10)
					          {
					          	 mobile='0'+mobile;
					          }
							  $.ajax({

								url : custom_url,
								type: 'POST',
								dataType: "json",
								data:{
									 email: email,

									 mobile:  mobile,
							  
									 },
								success : function(data) 
								{ 
                                    if(data==1)
									{ 	  
                                       
									  
			                         $("#guest-checkout-otp #myModal").css("display","block");							
                               
						
				                    }
			 
			                    }
							  });
		}
	},	
              closepopup: function(){ 	
	                $("#guest-checkout-otp #myModal").css("display","none");
					var email=$(".form-login input[name=username]").val();
					var delete_otp = url.build('ajaxotp/index/deleteotp');
					 $.ajax({

								url : delete_otp,
								type: 'POST',
								dataType: "json",
								
								data:{},
								success : function(data) 
								{
                                   $("#guest-checkout-otp #otp-validated-success").css("display", "none");
										 $("#guest-checkout-otp #otp-validated-error").css("display", "block");
                                }, 
		                        error : function(request,error)
								{
									alert("Failed");
								}
							});								
									 
					
			  },
			  
			 submitotp: function(){
			    $("#guest-checkout-otp #myModal").css("display","none");
				   var span = document.getElementsByClassName("close")[0];
				   var retVal=$("#guest-checkout-otp #otp_value").val();
                   var email=$(".form-login input[name=username]").val();
                   var mobile= $("#guest-checkout-otp input[name=mobile_otp]").val();
				   var validate_url = url.build('ajaxotp/index/validate');  
				   if(mobile.length==10)
					          {
					          	 mobile='0'+mobile;
					          }
				    $.ajax({

								url : validate_url,
								type: 'POST',
								dataType: "json",
								
								data:{
									 email: email,
									 mobile: mobile,
									 otp: retVal,
									 },
								success : function(data) 
								{              
									if(data==1){
										 $("#guest-checkout-otp #get_otp").css("display", "none");
										 $("#guest-checkout-otp #mobile_otp").prop("readonly", true);
										 $("#guest-checkout-otp #otp-validated-success").css("display", "block");
										 $("#guest-checkout-otp #otp-validated-error").css("display", "none");
                                          
										 $("#guest-checkout-otp #next-btn").css("display", "block");
										  localStorage.setItem("mobile_otp",mobile);
										  stepNavigator.next(); 
										
									}
									 else
									 {
										  
										 $("#guest-checkout-otp #get_otp").attr("disabled", false);
										 $("#guest-checkout-otp #mobile_otp").prop("readonly", false);
										 $("#guest-checkout-otp #otp-validated-success").css("display", "none");
										 
										 $("#guest-checkout-otp #otp-validated-error").css("display", "block");
                                           var key="mobile_otp";
                                            
                                           
										  
									 }
									   
								},
								error : function(request,error)
								{
									alert("Failed");
								}
							      });									  
				},
							
				
             
			 
            /**
			* The navigate() method is responsible for navigation between checkout step
			* during checkout. You can add custom logic, for example some conditions
			* for switching to your custom step
			*/
            navigate: function () {
            },
            /**
			* @returns void
			*/
            navigateToNextStep: function () {
				
						if(customer.isLoggedIn())
						{
							stepNavigator.next(); 
						}
				        if(!customer.isLoggedIn())
						{
				                var validate_url = url.build('ajaxotp/index/sessionset');
                                 var mobile= $("#guest-checkout-otp input[name=mobile_otp]").val();
                                 if(mobile.length==10)
					          {
					          	 mobile='0'+mobile;
					          }
                                 if(mobile=='')
								 {	
				                var validate_url = url.build('ajaxotp/index/unsetsession');  
                                     $.ajax
									 ({
								url : validate_url,
								type: 'POST',
								dataType: "json",
								success : function(data)
											{
													 $("#otp-validated-success").css("display", "none");
													 $("#otp-validated-error").css("display", "block");	
											},	
                               error : function(request,error)
											{
												 alert('Service Down');
											}									 
								    });	 
								 }
						else
						{
						          $.ajax({
								url : validate_url,
								type: 'POST',
								dataType: "json",
								data:{},
								success : function(data) 
								{              
									if(data==1)
									{
										
										 $("#otp-validated-success").css("display", "block");
										 $("#otp-validated-error").css("display", "none");
										 
										 $("#guest-checkout-otp #next-btn").css("display", "block");
										 
										 $("#guest-checkout-otp #get_otp").css("display", "none");
                                         stepNavigator.next();
								   
									 }
									 else
									 {  
										 $("#otp-validated-success").css("display", "none");
										 $("#otp-validated-error").css("display", "block");	 
									 }	   
								},
								error : function(request,error)
								{
									 alert("Service Down");
								}
							});
                     
				        }
				  }
            }
        });
    }
);