<?php
namespace Retailinsights\CustomProdList\Controller\Index;
use Retailinsights\CustomProdList\Block\Product\CustomList;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Retail\Subscribe\Block\Rewrite\Customer\Account\Customer;
class Index extends Action
{
    /** @var PageFactory */
    protected $pageFactory;
    /** @var  \Magento\Catalog\Model\ResourceModel\Product\Collection */
    protected $productCollection;
    protected $_productCollectionFactory;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule $resource
    )
    {

         $this->_connection = $resource->getConnection();
        $this->pageFactory = $pageFactory;
        $this->productCollection = $collectionFactory->create();
        parent::__construct($context);
     
    }
       public function isCustomerLoggedIn()
        {
            return $this->_session->isLoggedIn();
        }

         public function getCustomerId()
         {
           $om = \Magento\Framework\App\ObjectManager::getInstance();

           $customerSession = $om->get('Magento\Customer\Model\Session');
           if($customerSession->isLoggedIn()) 
           {
            return $customerSession->getCustomer()->getId();  // get Customer Id
           }   
         }

             public function getCouponUsageReportData($coupon_id)
        {
            $myTable = $this->_connection->getTableName('coupons_usage_report');
            $sql     = $this->_connection->select('coupon_usage')->from(["tn" => $myTable])
                  ->where('coupon_id = ?',$coupon_id);      
            $result  = $this->_connection->fetchRow($sql); 
            return $result['coupon_usage'];
        }
             public function getCustomerCouponUsageData($coupon_id)
        {
            $myTable = $this->_connection->getTableName('customer_coupon_usage');
            $sql     = $this->_connection->select('coupon_used')->from(["tn" => $myTable])
                  ->where('coupon_id= ?',$coupon_id)
                  ->where('customer_id= ?',$this->getCustomerId());          
            $result  = $this->_connection->fetchRow($sql); 
            return $result['coupon_used'];
        }

              public function getTableData()
        {
           date_default_timezone_set('Asia/Kolkata');
            $today=date("Y-m-d H:i:s");
                     $params= $this->getRequest()->getParams();

            $myTable = $this->_connection->getTableName('customer_custom_coupons');
                     $sql = "Select id,product_skus,uses_per_cust,uses_per_cpn,to_date FROM ". $myTable." where to_date >='".$today."' and from_date <= '".$today."'";	
                     
                           if(array_key_exists('product_list_order',$params)){
                           	$order_by=$params['product_list_order'];
            $myTable = $this->_connection->getTableName('customer_custom_coupons');
           /* $sql     = $this->_connection->select('id','product_skus','uses_per_cust','uses_per_cpn')->from(["tn" => $myTable])
                  ->where('to_date >= ?',$today)
                  ->where('from_date <= ?',$today); */
                  
                  if(strcmp($order_by, 'new_coupon')==0)
                  {
                    
                  $sql.=" order by from_date desc";
                  }
                  if(strcmp($order_by, 'expiring_coupon')==0)
                  {
                    
                  $sql.=" order by to_date asc";
                  }
                  if(strcmp($order_by, 'old_coupon')==0)
                  {
                    
                  $sql.=" order by from_date asc";
                  }
                   if(strcmp($order_by, 'most_used_coupon')==0)
                   {
                   	
                  $sql = "Select * FROM customer_custom_coupons,coupons_usage_report where customer_custom_coupons.id=coupon_id order by coupon_usage desc";
                
                   }
                   if(strcmp($order_by, 'discount_percent')==0)
                   { 	
                  $sql.=" order by discount_percent desc";   
                   }
            
            $result  = $this->_connection->fetchAll($sql); 
             

            return $result;
          }
          else
          {
          	$result  = $this->_connection->fetchAll($sql); 
            return $result;
          }
        }
    public function execute()
    {


       $params= $this->getRequest()->getParams();

        $result = $this->pageFactory->create();
           $data=$this->getTableData();
           
            $items=array();
           $newitems=array();
           
                 ob_start();
               foreach($data as $dat)
            {
                if($this->getCustomerCouponUsageData($dat['id'])<$dat['uses_per_cust'])
                {              
                 if($this->getCouponUsageReportData($dat['id'])<$dat['uses_per_cpn'])
                 {     
                 $skus=explode(",",$dat['product_skus']);
                 foreach($skus as $sk)
                   {  
                     array_push($items,$sk);
                   }      
                }
             }
          }
            $categories =$params['cat'];
        $this->productCollection->addFieldToSelect('*');
        $this->productCollection->addCategoriesFilter(['in' => $categories]);
        $this->productCollection->addAttributeToFilter('sku', array('in' => $items));
        $collection=$this->productCollection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
          $skus=implode(',', $items);
        $skus = "'" . str_replace(",", "','", $skus) . "'";

       $collection->getSelect()->order(new \Zend_Db_Expr("FIELD(sku,$skus)"));
        
        $list = $result->getLayout()->getBlock('custom.products.list');
        $list->setProductCollection($collection);
        return $result;

    }

}