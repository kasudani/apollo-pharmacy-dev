<?php 

namespace Retailinsights\CustomProdList\Helper;



class Data extends \Magento\Framework\App\Helper\AbstractHelper {



	protected $_resource;

	protected $_productloader;

  public function __construct(
    \Magento\Framework\App\ResourceConnection $resource,
    \Magento\Catalog\Model\ProductFactory $_productloader
  ) {
    $this->_resource = $resource;
    $this->_productloader = $_productloader;
  }



    public function getProductCouponPrice($id)
    {
        $product=$this->_productloader->create()->load($id);

        $sku=$product->getSku();

        $data=$this->getTableData();

        foreach($data as $dat)
        {

          $skus=explode(",",$dat['product_skus']);

          if(in_array($sku,$skus))
          {

            if($this->getCustomerCouponUsageData($dat['id'])<$dat['uses_per_cust'])
            {              

              if($this->getCouponUsageReportData($dat['id'])<$dat['uses_per_cpn'])
              {
                $discount_amount=($dat['discount_percent']*$product->getFinalPrice())/(100);
                echo "</br><span class='custom-discount-new'>Save ".$dat['discount_percent']."% at checkout.</span>";
                /* echo "<span class='custom-discount-amount'> Save &#8377;".number_format($discount_amount,2)."</span>";*/
                break;
              }
            }
          }
        }
    }

       public function getProductDiscount($id) {

        $product=$this->_productloader->create()->load($id);

           

             $sku=$product->getSku();

             $data=$this->getTableData();

             foreach($data as $dat)

            {



              $skus=explode(",",$dat['product_skus']);

              if(in_array($sku,$skus))

              {

                   if($this->getCustomerCouponUsageData($dat['id'])<$dat['uses_per_cust'])

                {              

                 if($this->getCouponUsageReportData($dat['id'])<$dat['uses_per_cpn'])

                 { 

             $discount_amount=($dat['discount_percent']*$product->getFinalPrice())/(100);

                   echo "<div class='display-offer-list'><span class='custom-discount-new-percent'> ".$dat['discount_percent']."% OFF.</span>";



                   echo "<span class='custom-discount-new-price'> Save Rs ".$discount_amount."</span></div>";

                   /*

                     echo "<span class='custom-discount-amount'> Save &#8377;".number_format($discount_amount,2)."</span>";*/

                   break;

              }

          }

        }



            }



       }

       public function getCouponFinalPrice($id) {

         $product=$this->_productloader->create()->load($id);

           

             $sku=$product->getSku();

             $data=$this->getTableData();

             foreach($data as $dat)

            {



              $skus=explode(",",$dat['product_skus']);

              if(in_array($sku,$skus))

              {

                   if($this->getCustomerCouponUsageData($dat['id'])<$dat['uses_per_cust'])

                {              

                 if($this->getCouponUsageReportData($dat['id'])<$dat['uses_per_cpn'])

                 { 

             $discount_amount=($dat['discount_percent']*$product->getFinalPrice())/(100);

/*

               echo "<span class='final_price_before'>".$product->getFinalPrice()."</span>";*/

                  $final=$product->getFinalPrice()-$discount_amount;

                   echo "<span class='final_price_after'>&#8377;".$final."</span>";



                   echo "<span class='discount-percent-off'> (".$dat['discount_percent']."% OFF.)</span>";

/*

                   echo "<span class='discount-percent-amount'> Save Rs".$discount_amount.".</span>";*/

                   break;

              }

          }

        }



            }

        }

         public function getCouponUsageReportData($coupon_id) {



        	  $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

            $myTable = $connection->getTableName('coupons_usage_report');

            $sql     = $connection->select('coupon_usage')->from(["tn" => $myTable])

                  ->where('coupon_id = ?',$coupon_id);      

            $result  = $connection->fetchRow($sql); 

            if($result['coupon_usage']=="")
             {
                return 0;
             }
             else
             {
            return $result['coupon_usage'];
             }

        }

        public function getCustomerCouponUsageData($coupon_id) {



        	  $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

            $myTable = $connection->getTableName('customer_coupon_usage');

            $sql     = $connection->select('coupon_used')->from(["tn" => $myTable])

                  ->where('coupon_id= ?',$coupon_id)

                  ->where('customer_id= ?',1);          

            $result  = $connection->fetchRow($sql); 

            if($result['coupon_used']=="")
             {
                return 0;
             }
             else
             {
            return $result['coupon_used'];
             }

        }

        public function getTableData() {

        	  $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

           date_default_timezone_set('Asia/Kolkata');

            $today=date("Y-m-d H:i:s");

            $myTable = $connection->getTableName('customer_custom_coupons');

            $sql     = $connection->select('id','product_skus','uses_per_cust','uses_per_cpn')->from(["tn" => $myTable])

                  ->where('to_date >= ?',$today)

                  ->where('from_date <= ?',$today);      

            $result  = $connection->fetchAll($sql); 

            return $result;

        }

}
