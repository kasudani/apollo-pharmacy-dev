<?php
namespace Retailinsights\CustomProdList\Block\Pricing\Render;

use Magento\Catalog\Pricing\Price;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Module\Manager;
use Magento\Framework\Pricing\Render;
use Magento\Framework\Pricing\Render\PriceBox as BasePriceBox;
use Magento\Msrp\Pricing\Price\MsrpPrice;
use Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\Render\RendererPool;

/**
 * Class for final_price rendering
 *
 * @method bool getUseLinkForAsLowAs()
 * @method bool getDisplayMinimalPrice()
 */
class FinalPriceBox extends BasePriceBox
{
    /**
     * @var SalableResolverInterface
     */
    private $salableResolver;

    /** @var  Manager */
    private $moduleManager;
    protected $_request;
    protected $_currency;
    /**
     * @param Context $context
     * @param SaleableInterface $saleableItem
     * @param PriceInterface $price
     * @param RendererPool $rendererPool
     * @param array $data
     * @param SalableResolverInterface $salableResolver
     */
    public function __construct(
        Context $context,
        SaleableInterface $saleableItem,
        PriceInterface $price,
        RendererPool $rendererPool,
        array $data = [],
        SalableResolverInterface $salableResolver = null,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule $resource,
        \Magento\Framework\App\Request\Http $Request,
        \Magento\Directory\Model\Currency $currency

    ) {
         $this->_currency = $currency; 
        $this->_request = $Request;
        $this->_productloader = $_productloader;
         $this->_connection = $resource->getConnection();
        parent::__construct($context, $saleableItem, $price, $rendererPool, $data);
        $this->salableResolver = $salableResolver ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(SalableResolverInterface::class);
    }
    public function getCurrentCurrencySymbol()
        {
            return $this->_currency->getCurrencySymbol();
        }
    /**
     * @return string
     */
             public function getTableData()
        {
           date_default_timezone_set('Asia/Kolkata');
            $today=date("Y-m-d H:i:s");
            $myTable = $this->_connection->getTableName('customer_custom_coupons');
            $sql     = $this->_connection->select('id','product_skus','uses_per_cust','uses_per_cpn')->from(["tn" => $myTable])
                  ->where('to_date >= ?',$today)
                  ->where('from_date <= ?',$today);      
            $result  = $this->_connection->fetchAll($sql); 
            return $result;
        }
    protected function _toHtml()
    {
        // Check catalog permissions
        if ($this->getSaleableItem()->getCanShowPrice() === false) {
            return '';
        }

        $result = parent::_toHtml();

        //Renders MSRP in case it is enabled
        if ($this->isMsrpPriceApplicable()) {
            /** @var BasePriceBox $msrpBlock */
            $msrpBlock = $this->rendererPool->createPriceRender(
                MsrpPrice::PRICE_CODE,
                $this->getSaleableItem(),
                [
                    'real_price_html' => $result,
                    'zone' => $this->getZone(),
                ]
            );
            $result = $msrpBlock->toHtml();
        }

        return $this->wrapResult($result);
    }

    /**
     * Check is MSRP applicable for the current product.
     *
     * @return bool
     */
    private function isMsrpPriceApplicable()
    {
        $moduleManager = $this->getModuleManager();

        if (!$moduleManager->isEnabled('Magento_Msrp') || !$moduleManager->isOutputEnabled('Magento_Msrp')) {
            return false;
        }

        try {
            /** @var MsrpPrice $msrpPriceType */
            $msrpPriceType = $this->getSaleableItem()->getPriceInfo()->getPrice('msrp_price');
        } catch (\InvalidArgumentException $e) {
            $this->_logger->critical($e);
            return false;
        }

        if ($msrpPriceType === null) {
            return false;
        }

        $product = $this->getSaleableItem();
        
        return $msrpPriceType->canApplyMsrp($product) && $msrpPriceType->isMinimalPriceLessMsrp($product);
    }

    /**
     * Wrap with standard required container
     *
     * @param string $html
     * @return string
     */
    protected function wrapResult($html)
    {
        return '<div class="price-box ' . $this->getData('css_classes') . '" ' .
            'data-role="priceBox" ' .
            'data-product-id="' . $this->getSaleableItem()->getId() . '"' .
            '>' . $html . '</div>';
    }

    /**
     * Render minimal amount
     *
     * @return string
     */
    public function renderAmountMinimal()
    {
        /** @var \Magento\Catalog\Pricing\Price\FinalPrice $price */
        $price = $this->getPriceType(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE);
        $id = $this->getPriceId() ? $this->getPriceId() : 'product-minimal-price-' . $this->getSaleableItem()->getId();
        return $this->renderAmount(
            $price->getMinimalPrice(),
            [
                'display_label'     => __('As low as'),
                'price_id'          => $id,
                'include_container' => false,
                'skip_adjustments' => true
            ]
        );
    }

    /**
     * Define if the special price should be shown
     *
     * @return bool
     */
    public function hasSpecialPrice()
    {
        $displayRegularPrice = $this->getPriceType(Price\RegularPrice::PRICE_CODE)->getAmount()->getValue();
        $displayFinalPrice = $this->getPriceType(Price\FinalPrice::PRICE_CODE)->getAmount()->getValue();
        return $displayFinalPrice < $displayRegularPrice;
    }

    /**
     * Define if the minimal price should be shown
     *
     * @return bool
     */
    public function showMinimalPrice()
    {
        /** @var Price\FinalPrice $finalPrice */
        $finalPrice = $this->getPriceType(Price\FinalPrice::PRICE_CODE);
        $finalPriceValue = $finalPrice->getAmount()->getValue();
        $minimalPriceAValue = $finalPrice->getMinimalPrice()->getValue();
        return $this->getDisplayMinimalPrice()
        && $minimalPriceAValue
        && $minimalPriceAValue < $finalPriceValue;
    }

    /**
     * Get Key for caching block content
     *
     * @return string
     */
    public function getCacheKey()
    {
         return parent::getCacheKey() . ($this->getData('list_category_page') ? '-list-category-page': '');
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $cacheKeys = parent::getCacheKeyInfo();
        $cacheKeys['display_minimal_price'] = $this->getDisplayMinimalPrice();
        return $cacheKeys;
    }

      public function getCustomData()
      {
          $product=$this->_productloader->create()->load($this->getSaleableItem()->getId());
           $sku=$product->getSku();
              $data=$this->getTableData();

            foreach($data as $dat)
            {

              $skus=explode(",",$dat['product_skus']);
              if(in_array($sku,$skus))
              {
                    if($this->getCustomerCouponUsageData($dat['id'])<$dat['uses_per_cust'])
                    {              
                 if($this->getCouponUsageReportData($dat['id'])<$dat['uses_per_cpn'])
                 { 
                     
             $discount_amount=($dat['discount_percent']*$product->getFinalPrice())/(100);
              $total=$product->getFinalPrice()-$discount_amount;
                   return "<div id='validty'><span class='custom-total'>&#8377;".number_format($total,2)."</span><div id='cust_val'><strong>Valid Till :</strong><span class='custom-valid'>".date('d M Y', strtotime($dat['to_date'])).'</span></div></div>';
                    
              } 
          }
      }


            }
    }

     public function getCouponDiscount()
      {
            if ($this->_request->getFullActionName() == 'customprodlist_index_index') 
            {
             $product=$this->_productloader->create()->load($this->getSaleableItem()->getId());
             $sku=$product->getSku();
             $data=$this->getTableData();
             foreach($data as $dat)
            {

              $skus=explode(",",$dat['product_skus']);
              if(in_array($sku,$skus))
              {
                   if($this->getCustomerCouponUsageData($dat['id'])<$dat['uses_per_cust'])
                {              
                 if($this->getCouponUsageReportData($dat['id'])<$dat['uses_per_cpn'])
                 { 
             $discount_amount=($dat['discount_percent']*$product->getFinalPrice())/(100);
                   echo "</br><span class='custom-discount'>".$dat['discount_percent']."%OFF</span>";
                 
                   echo "<span class='custom-discount-amount'> Save &#8377;".number_format($discount_amount,2)."</span>";
                   break;
              }
          }
        }

            }
        }
    }
               
           public function getCouponUsageReportData($coupon_id)
        {
            $myTable = $this->_connection->getTableName('coupons_usage_report');
            $sql     = $this->_connection->select('coupon_usage')->from(["tn" => $myTable])
                  ->where('coupon_id = ?',$coupon_id);      
            $result  = $this->_connection->fetchRow($sql); 
            if($result['coupon_usage']=="")
             {
                return 0;
             }
             else
             {
            return $result['coupon_usage'];
             }
        }
             public function getCustomerCouponUsageData($coupon_id)
        {
            $myTable = $this->_connection->getTableName('customer_coupon_usage');
            $sql     = $this->_connection->select('coupon_used')->from(["tn" => $myTable])
                  ->where('coupon_id= ?',$coupon_id)
                  ->where('customer_id= ?',$this->getCustomerId());          
            $result  = $this->_connection->fetchRow($sql); 
             if($result['coupon_used']=="")
             {
                return 0;
             }
             else
             {
            return $result['coupon_used'];
             }
        }

    /**
     * @deprecated
     * @return Manager
     */
    private function getModuleManager()
    {
        if ($this->moduleManager === null) {
            $this->moduleManager = ObjectManager::getInstance()->get(Manager::class);
        }
        return $this->moduleManager;
    }
}
