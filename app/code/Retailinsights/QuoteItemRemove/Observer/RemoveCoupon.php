<?php 
namespace Retailinsights\QuoteItemRemove\Observer;
use Magento\Framework\Event\ObserverInterface;
class RemoveCoupon implements ObserverInterface
{
    /**
     * Order Model
     *
     * @var \Magento\Sales\Model\Order $order
     */
    protected $_coreSession;
     protected $_registry;
    protected $order;
     public function __construct(
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    )
    {
        $this->order = $order;
        $this->_coreSession = $coreSession;
    }
        public function getCouponsVariable()
        {
              $this->_coreSession->start();
          return $this->_coreSession->getCouponsUsed();
        }
         public function setCouponsVariable($coupon_data)
         {
              $this->_coreSession->start();
          $this->_coreSession->setCouponsUsed($coupon_data);
         }
       public function unsetCouponsVariable()
        {
        $this->_coreSession->start();
        return $this->_coreSession->unsCouponsUsed();
        }
       public function execute(\Magento\Framework\Event\Observer $observer)
       {
              $quote = $observer->getEvent()->getQuoteItem();
              $i=0;
              $coupon_data=$this->getCouponsVariable();
                print_r($coupon_data);
              unset($coupon_data[$quote->getProduct()->getId()]);
               $this->setCouponsVariable($coupon_data);   
                print_r($coupon_data);
                   
       }
}