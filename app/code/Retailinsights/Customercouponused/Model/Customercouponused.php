<?php
namespace Retailinsights\Customercouponused\Model;

class Customercouponused extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\Customercouponused\Model\ResourceModel\Customercouponused');
    }
}
?>