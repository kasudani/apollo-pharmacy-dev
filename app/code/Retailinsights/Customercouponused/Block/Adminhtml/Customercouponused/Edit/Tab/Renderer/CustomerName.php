<?php
 
namespace Retailinsights\Customercouponused\Block\Adminhtml\Customercouponused\Edit\Tab\Renderer;
 
use Magento\Framework\DataObject;
 
class CustomerName extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $categoryFactory;
    protected $_customerRepositoryInterface;
    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,

        \CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule $resource,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
        $this->categoryFactory = $categoryFactory;

         $this->_connection = $resource->getConnection();
          $this->_customerRepositoryInterface = $customerRepositoryInterface;
    }   

    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
		   
        $customer_id= $row->getData('customer_id'); 
		   if($customer_id==0)
		   {
			    return "Guest Customer";
		   }
        $customer = $this->_customerRepositoryInterface->getById($customer_id);
         return $customer->getFirstName()."&nbsp;&nbsp;".$customer->getLastName();
    }
}