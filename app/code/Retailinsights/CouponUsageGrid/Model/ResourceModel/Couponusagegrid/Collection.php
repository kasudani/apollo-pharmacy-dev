<?php

namespace Retailinsights\CouponUsageGrid\Model\ResourceModel\Couponusagegrid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\CouponUsageGrid\Model\Couponusagegrid', 'Retailinsights\CouponUsageGrid\Model\ResourceModel\Couponusagegrid');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>