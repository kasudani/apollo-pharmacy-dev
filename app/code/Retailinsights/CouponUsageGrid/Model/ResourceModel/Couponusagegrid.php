<?php
namespace Retailinsights\CouponUsageGrid\Model\ResourceModel;

class Couponusagegrid extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('coupons_usage_report', 'id');
    }
}
?>