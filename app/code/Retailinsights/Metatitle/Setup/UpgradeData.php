<?php
 
namespace Retailinsights\Metatitle\Setup;
 
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
 
 
class UpgradeData implements UpgradeDataInterface {
 
    public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;
 
        if ( version_compare( $context->getVersion(), '1.0.5', '<' ) ) {
            $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
            $categoryCollection = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
            $categories = $categoryCollection->create();
            $categories->addAttributeToSelect('*');
            foreach ($categories as $category) {
              $category->setmeta_title($category->getName()." Products,Buy ".$category->getName()." - Apollo Pharmacy.");
              $category->setmeta_description('Find best deals & offers on '.$category->getName().' products online at Apollo Pharmacy. Avail COD, Home Delivery & Store Pickup available across India on wide range of '.$category->getName().' Products.');
              $category->save();
            }   
        }
        if ( version_compare( $context->getVersion(), '1.0.9', '<' ) ) {
         $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
         $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
         $collection = $productCollection->addAttributeToSelect('*')->load();
         foreach ($collection as $product){
           $product->setmeta_title($product->getName()." - Apollo Pharmacy.");
           $product->setmeta_description('Buy '.$product->getName().' for '.$product->getprice().' online at apollopharmacy.in, Check out '.$product->getName().' reviews, ratings, specifications and more at Apollopharmacy.in. Avail COD, Home Delivery & Store Pickup available across India.');
           $product->save();
         } 
       }
    }
}