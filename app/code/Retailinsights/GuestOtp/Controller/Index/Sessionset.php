<?php

namespace Retailinsights\GuestOtp\Controller\Index;

class Sessionset extends \Magento\Framework\App\Action\Action
{
	 public $GuestotpFactory;
	 protected $resultJsonFactory;
	  protected $request;
	 public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Retailinsights\GuestOtp\Model\GuestotpFactory $GuestotpFactory,
		\Magento\Framework\Session\SessionManagerInterface $coreSession
    ) 
	{
		$this->_coreSession = $coreSession; 
		$this->GuestotpFactory = $GuestotpFactory;
        parent::__construct($context);
    }
	public function setOtpValidated($value)
	{
    $this->_coreSession->start();
    $this->_coreSession->setOtpValidated($value);
     }
	 public function getOtpValidated()
	 {
    $this->_coreSession->start();
    return $this->_coreSession->getOtpValidated();
     }
    public function execute()
    {	    
	    if($this->getOtpValidated()){
		 echo 1;
		}
		else{
		echo 0; 
		}
			 
    }
}