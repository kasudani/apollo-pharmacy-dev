<?php	
namespace Retailinsights\GuestOtp\Controller\Index;

class Deleteotp extends \Magento\Framework\App\Action\Action
{
	 public $GuestotpFactory;
	 protected $resultJsonFactory;
	 public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Retailinsights\GuestOtp\Model\GuestotpFactory $GuestotpFactory
    ) 
	{
		$this->GuestotpFactory = $GuestotpFactory;
        parent::__construct($context);
    }
    public function execute()
    {	    	        
	              $model = $this->GuestotpFactory->create();
				    $collection=  $model->getCollection();
				    $collection->walk('delete');  
                   echo 1;
	}				   
}
