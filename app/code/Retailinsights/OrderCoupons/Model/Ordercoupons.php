<?php
namespace Retailinsights\OrderCoupons\Model;

class Ordercoupons extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\OrderCoupons\Model\ResourceModel\Ordercoupons');
    }
}
?>