<?php 

namespace Retailinsights\Ordersuccess\Observer;



use Magento\Framework\Event\ObserverInterface;

use Magento\Framework\App\ObjectManager;

use Magento\Framework\Controller\ResultFactory;



class AfterPlaceOrder implements ObserverInterface
{

    /**

     * Order Model

     *

     * @var \Magento\Sales\Model\Order $order

     */

        protected $order;

        protected $_moduleFactory;

        protected $_coreSession;

        protected $_exampleFactory; 

        protected $_couponsFactory;

        protected $_couponsusedFactory; 



     public function __construct(
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Retailinsights\CouponUsageReport\Model\CouponusagereportFactory $db,
        \Retailinsights\OrderCoupons\Model\OrdercouponsFactory $cf,
        \Retailinsights\Customercouponused\Model\CustomercouponusedFactory $cu
    )
    {

       $this->_exampleFactory = $db;

       $this->_couponsFactory = $cf;

       $this->_couponsusedFactory = $cu;

        $this->_coreSession = $coreSession;

        $this->order = $order;

    }



    public function execute(\Magento\Framework\Event\Observer $observer)
    {

       $orderId = $observer->getEvent()->getOrderIds();

        $order = $this->order->load($orderId);

        //get Order All Item

        $itemCollection = $order->getItemsCollection();

        $customer = $order->getCustomerId(); 

         $this->_coreSession->start(); 

         $coupons_used=array();

          if($this->_coreSession->getCouponsUsed()!=null)

          {

        $coupons_used=$this->_coreSession->getCouponsUsed();

          }

           $this->_coreSession->unsCouponsUsed();

           $data['customer_id']=$customer;



           $data['order_id']=$order->getId();



           $orderItems = $order->getAllItems();

                foreach ($itemCollection as $item) 

                {

                  $item_id=$item->getItemId();

                  $product_id=$item->getProductId();

                   $item_qty=$item->getQtyOrdered();



                  if (array_key_exists($item->getProductId(),$coupons_used))

                 {

                  $coupon_id=$coupons_used[$item->getProductId()];

                $data['item_id']=$item_id;

                $data['product_id']=$product_id;

                $data['coupon_id']=$coupon_id;

                $model=$this->_couponsFactory->create();

                $model->setData($data);

                $model->save();

                 $couponusage=$this->_exampleFactory->create();

                 $collection = $couponusage->getCollection();

                 $collection->addFieldToFilter('coupon_id',$coupon_id);

                   if(count($collection)==0)

                   {

                     

                    $model=$this->_exampleFactory->create();

                    $model->setData('coupon_id',$coupon_id);

                    $model->setData('coupon_usage',round($item_qty));

                    $model->save();

                    $model=$this->_couponsusedFactory->create();

                    $model->setData('customer_id',$customer);

                    $model->setData('coupon_id',round($coupon_id));



                    $model->setData('coupon_used',round($item_qty));

                    $model->save();



                   }

                   else

                   {

                      $couponusage=$this->_exampleFactory->create();

                      $couponusage->load($coupon_id,'coupon_id');

                      $current_qty=$couponusage->getData('coupon_usage');

                      $couponusage->setData('coupon_usage',$current_qty+round($item_qty));

                      $couponusage->save();



                      $model=$this->_couponsusedFactory->create();

                      $collection=$model->getCollection();

                      $collection->addFieldToFilter('coupon_id', $coupon_id);



                      $collection->addFieldToFilter('customer_id', $customer);

                        if(count($collection)>0){

                      foreach($collection as $col)

                      {

                        $model=$this->_couponsusedFactory->create();

                        $model->load($col->getData('id'),'id');

                           $model->setData('coupon_used',$col->getData('coupon_used')+round($item_qty));

                           $model->save();

                      }

                    }

                    else

                    {

                       $model=$this->_couponsusedFactory->create();

                      $model->setData('customer_id',$customer);

                      $model->setData('coupon_id',round($coupon_id));



                      $model->setData('coupon_used',round($item_qty));

                      $model->save();



                    }



                   }

                  }

                } 

        

   }       

 



}

