<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model\Productfeed\Source;

class FormatPriceDecimals implements \Magento\Framework\Option\ArrayInterface
{
    
    const ONE = 1;
    const TWO = 2;
    const THREE = 3;
    const FOUR = 4;


    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
           
            [
                'value' => self::ONE,
                'label' => __('One')
            ],
            [
                'value' => self::TWO,
                'label' => __('Two')
            ],
            [
                'value' => self::THREE,
                'label' => __('Three')
            ],
              [
                'value' => self::FOUR,
                'label' => __('Four')
              ],
        ];
        return $options;
    }
}
