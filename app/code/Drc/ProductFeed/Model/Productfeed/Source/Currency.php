<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model\Productfeed\Source;

class Currency implements \Magento\Framework\Option\ArrayInterface
{
    protected $storeManagerInterface;
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
    ) {
    
        $this->storeManagerInterface = $storeManagerInterface;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $currency = $this->storeManagerInterface->getStore()->getBaseCurrencyCode();
        
        $options = [
           
            [
                'value' => $currency,
                'label' => __($currency)
            ],
            
            
        ];
        return $options;
    }
}
