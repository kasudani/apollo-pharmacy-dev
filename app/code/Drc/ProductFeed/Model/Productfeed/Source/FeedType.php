<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model\Productfeed\Source;

class FeedType implements \Magento\Framework\Option\ArrayInterface
{
    const CSV = 1;
    const XML = 2;
    const TXT = 3;


    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::CSV,
                'label' => __('CSV')
            ],
            [
                'value' => self::XML,
                'label' => __('XML')
            ],
            [
                'value' => self::TXT,
                'label' => __('TXT')
            ],
        ];
        return $options;
    }
}
