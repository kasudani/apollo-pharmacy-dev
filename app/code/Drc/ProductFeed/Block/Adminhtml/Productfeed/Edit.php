<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Block\Adminhtml\Productfeed;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    protected $url;
    protected $request;
    /**
     * constructor
     *
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Backend\Model\UrlInterface $url,
        \Magento\Framework\App\Request\Http $request,
        array $data = []
    ) {
    
        $this->coreRegistry = $coreRegistry;
        $this->url = $url;
        $this->request = $request;
        parent::__construct($context, $data);
    }

    /**
     * Initialize ProductFeed edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'productfeed_id';
        $this->_blockGroup = 'Drc_ProductFeed';
        $this->_controller = 'adminhtml_productfeed';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save Product Feed'));
        $this->buttonList->add(
            'save-and-continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ]
                    ]
                ]
            ],
            -100
        );
        $this->addButton(
            'saveandgenerate',
            [
                'label' => __('Save and Generate'),
                'class' => 'save',
                'onclick' => 'document.getElementById("edit_form").action="'.$this->url->getUrl('drc_productfeed/*/saveandgenerate', ['productfeed_id' => $this->request->getParam('productfeed_id')]).'"; document.getElementById("edit_form").submit();'
            ]
        );
        $this->buttonList->update('delete', 'label', __('Delete Product Feed'));
    }
    /**
     * Retrieve text for header element depending on loaded ProductFeed
     *
     * @return string
     */
    public function getHeaderText()
    {
        /** @var \Drc\ProductFeed\Model\Productfeed $productfeed */
        $productfeed = $this->coreRegistry->registry('drc_productfeed_productfeed');
        if ($productfeed->getId()) {
            return __("Edit ProductFeed '%1'", $this->escapeHtml($productfeed->getName()));
        }
        return __('New ProductFeed');
    }
}
