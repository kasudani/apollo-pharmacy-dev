<?php
namespace Drc\Productfeed\Cron;

class CronTask
{
    protected $productfeedFactory;
    protected $logger;
    protected $filesystem;
    protected $directoryList;
    protected $storeManagerInterface;
    protected $ftp;
    protected $productCollectionFactory;
    protected $categoryCollectionFactory;
    protected $stockItemRepository;
    const ROOT = \Magento\Framework\App\Filesystem\DirectoryList::ROOT;
    protected $scopeConfig;

    public function __construct(
        \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Framework\Filesystem\Io\Ftp $ftp,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
    
        $this->productfeedFactory = $productfeedFactory;
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->ftp = $ftp;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->stockItemRepository = $stockItemRepository;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute()
    {
        $feeds = $this->productfeedFactory->create()->getCollection()->addFieldToSelect('*')->addFieldToFilter('is_active', ['eq' => 1])->addFieldToFilter('execute_mode', ['neq' => 1]);
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $excludeCategories = $this->scopeConfig->getValue('drc_productfeed/configurable_exclude_products/categories', $storeScope);
        $excludeCategories = explode(",",$excludeCategories);

        $excludeProducts = $this->scopeConfig->getValue('drc_productfeed/configurable_exclude_products/skus', $storeScope);
        $excludeProducts = explode(",",$excludeProducts);

        foreach ($feeds as $productfeed) {
            try {
                $content = json_decode($productfeed->getData('content'), true);
                $xml = $content['header'];
                $filename = $productfeed->getData('filename');
                $conditions = json_decode($productfeed->getData('conditions'), true);

                $product_types = !empty($conditions['product_types']) ? explode(",", $conditions['product_types']) : $conditions['product_types'];
                $attribute_set = !empty($conditions['attribute_set']) ? explode(",", $conditions['attribute_set']) : $conditions['attribute_set'];
                $visibility = !empty($conditions['product_visibility']) ? explode(",", $conditions['product_visibility']) : $conditions['product_visibility'];

                $collection = $this->productCollectionFactory->create();
                $collection->addStoreFilter($productfeed->getData('store_id'));
                $collection->addAttributeToSelect('*');

                if ($product_types) {
                    $collection->addAttributeToFilter('type_id', ['in' => $product_types]);
                }
                if ($attribute_set) {
                    $collection->addAttributeToFilter('attribute_set_id', ['in' => $attribute_set]);
                }
                if ($visibility) {
                    $collection->addAttributeToFilter('visibility', ['in' => $visibility]);
                }
                if($excludeCategories){
                    $collection->addCategoriesFilter(['nin' => $excludeCategories]);
                }
                if($excludeProducts){
                    $collection->addAttributeToFilter('sku', ['nin'=>$excludeProducts]);
                }

                foreach ($collection as $product) {
                    $stock = $this->stockItemRepository->get($product->getId());
                    if($stock->getIsInStock() != 1) { continue; }
                    $stock = $stock->getIsInStock() == 1 ? 'in stock' : 'out of stock';

                    $xml .= '<' . $content['item'] . '>';
                    $c = $content['content'];
                    $c = str_replace('{attr_sku}', $product->getSku(), $c);
                    $c = str_replace('{attr_name}', htmlspecialchars($product->getName()), $c);
                    $c = str_replace('{attr_desc}', htmlspecialchars($product->getDescription()), $c);

                    $catIds = $product->getCategoryIds();
                    $categories = $this->categoryCollectionFactory->create()
                        ->addAttributeToSelect('name')
                        ->addAttributeToFilter('entity_id', ['in' => $catIds]);
                    $catpath = '';
                    foreach ($categories as $category) {
                        $catpath .= $category->getName() . ' > ';
                    }
                    $catpath = rtrim($catpath, ' > ');
                    $c = str_replace('{attr_type}', htmlspecialchars($catpath), $c);
                    $catpath = '';

                    if ($product->getVisibility() == 1) {
                        $c = str_replace('{attr_link}', '', $c);
                    } else {
                        $c = str_replace('{attr_link}', $product->getProductUrl(), $c);
                    }

                    $productImg = $product->getImage();
                    if (trim($productImg) != '') {
                        $productImgUrl = $this->storeManagerInterface->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $productImg;
                        $c = str_replace('{attr_image_link}', $productImgUrl, $c);
                    }

                    $c = str_replace('{attr_availability}', $stock, $c);
                    $c = str_replace('{attr_condition}', 'new', $c);

                    $currencyCode = $productfeed->getData('currency');
                    $showCurrency = $productfeed->getData('format_price_currency_show');
                    $noOfDecimal = $productfeed->getData('format_price_decimals');
                    $decimalSeparator = $productfeed->getData('format_price_decimal_point');
                    $thousandSeparator = $productfeed->getData('format_price_thousands_separator');
                    if ($thousandSeparator == 'space') {
                        $thousandSeparator = ' ';
                    }
                    $price = number_format($product->getPriceInfo()->getPrice('final_price')->getValue(), $noOfDecimal, $decimalSeparator, $thousandSeparator);
                    if ($showCurrency == 1) {
                        $c = str_replace('{attr_price}', $price . ' ' . $currencyCode, $c);
                    } else {
                        $c = str_replace('{attr_price}', $price, $c);
                    }

                    if (trim($product->getAttributeText('manufacturer')) != '') {
                        $c = str_replace('{attr_manufacturer}', $product->getAttributeText('manufacturer'), $c);
                    } else {
                        $c = str_replace('{attr_manufacturer}', '', $c);
                    }
                    if (trim($product->getAttributeText('brand'))!='') {
                        $c = str_replace('{attr_brand}', htmlspecialchars($product->getAttributeText('brand')), $c);
                    } else {
                        $c = str_replace('{attr_brand}', '', $c);
                    }
                    $c = str_replace('{attr_gtin}', htmlspecialchars($product->getGtin()), $c);
                    $c = str_replace('{attr_google_category}', htmlspecialchars($product->getGoogleCategory()), $c);

                    $c = str_replace('{attr_identifier}', 'no', $c);

                    $xml .= $c;
                    $xml .= '</' . $content['item'] . '>';
                }
                $xml .= $content['footer'];

                $writer = $this->filesystem->getDirectoryWrite(self::ROOT);
                $feedExt = '';
                switch ($productfeed->getData('feed_type')) {
                    case 1:
                        $feedExt = '.csv';
                        break;
                    case 2:
                        $feedExt = '.xml';
                        break;
                    case 3:
                        $feedExt = '.txt';
                        break;
                }
                $xmlfile = $filename . $feedExt;
                $file = $writer->openFile('feed/' . $xmlfile, 'w');
                try {
                    $file->lock();
                    try {
                        $file->write($xml);
                    } finally {
                        $file->unlock();
                    }
                } finally {
                    $file->close();
                }

                $uploadFlag = false;
                $delivery = json_decode($productfeed->getData('delivery'), true);
                if ($delivery['ftp_enabled'] == 1) {
                    $host = $delivery['host'];
                    $user = $delivery['user'];
                    $pwd = $delivery['password'];
                    $protocol = $delivery['delivery_type'];
                    $remotepath = $delivery['path'];
                    $filepath = $this->directoryList->getRoot() . '/feed/' . $xmlfile;
                    $passive = $delivery['passivemode'];
                    if ($this->ftpUpload($host, $user, $pwd, $protocol, $xmlfile, $filepath, $remotepath, $passive)) {
                        $uploadFlag = true;
                    }
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->logger->debug($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->logger->debug($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());
            }
        }
        return $this;
    }

    protected function ftpUpload($host, $user, $pwd, $protocol, $filename, $filepath, $remotepath, $passive)
    {
        if ($protocol == 'ftp') {
            $passive = $passive == 1 ? true : false;
            $open = $this->ftp->open(['host' => $host, 'user' => $user, 'password' => $pwd, 'passive' => $passive]);
            if ($open) {
                $content = file_get_contents($filepath);
                $this->ftp->write($remotepath . $filename, $content);
                $this->ftp->close();
                return true;
            } else {
                return false;
            }
        } else {
            $this->logger->addError('sftp');
            if (strpos($host, ':') !== false) {
                list($host, $port) = explode(':', $host, 2);
            } else {
                $port = 22;
            }
            $sftp = new \phpseclib\Net\SFTP($host, $port);
            $open = $sftp->login($user, $pwd);
            if ($open) {
                $content = file_get_contents($filepath);
                $sftp->put($remotepath . $filename, $content);
                $sftp->disconnect();
                return true;
            } else {
                return false;
            }
        }
    }
}
