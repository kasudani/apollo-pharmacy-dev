<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Controller\Adminhtml;

abstract class Productfeed extends \Magento\Backend\App\Action
{
    /**
     * ProductFeed Factory
     *
     * @var \Drc\ProductFeed\Model\ProductfeedFactory
     */
    protected $productfeedFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * Result redirect factory
     *
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * constructor
     *
     * @param \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\App\Action\Context $context
    ) {
    
        $this->productfeedFactory    = $productfeedFactory;
        $this->coreRegistry          = $coreRegistry;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        parent::__construct($context);
    }

    /**
     * Init ProductFeed
     *
     * @return \Drc\ProductFeed\Model\Productfeed
     */
    protected function initProductfeed()
    {
        $productfeedId  = (int) $this->getRequest()->getParam('productfeed_id');
        /** @var \Drc\ProductFeed\Model\Productfeed $productfeed */
        $productfeed    = $this->productfeedFactory->create();
        if ($productfeedId) {
            $productfeed->load($productfeedId);
        }
        $this->coreRegistry->register('drc_productfeed_productfeed', $productfeed);
        return $productfeed;
    }
}
