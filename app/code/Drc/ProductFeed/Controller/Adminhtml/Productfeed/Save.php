<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Controller\Adminhtml\Productfeed;

class Save extends \Drc\ProductFeed\Controller\Adminhtml\Productfeed
{
    /**
     * Backend session
     *
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;
    protected $logger;

    /**
     * constructor
     *
     * @param \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory,
        \Magento\Framework\Registry $registry,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Backend\App\Action\Context $context
    ) {
    
        $this->backendSession = $context->getSession();
        $this->logger = $logger;
        parent::__construct($productfeedFactory, $registry, $context);
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPost('productfeed');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->format($data);
            
            //$this->logger->debug(json_encode($data));
            
            $productfeed = $this->initProductfeed();
            $productfeed->setData($data);
            $this->_eventManager->dispatch(
                'drc_productfeed_productfeed_prepare_save',
                [
                    'productfeed' => $productfeed,
                    'request' => $this->getRequest()
                ]
            );
            try {
                $productfeed->save();
                $this->messageManager->addSuccess(__('The product feed has been saved.'));
                $this->backendSession->setDrcProductFeedProductfeedData(false);
                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath(
                        'drc_productfeed/*/edit',
                        [
                            'productfeed_id' => $productfeed->getId(),
                            '_current' => true
                        ]
                    );
                    return $resultRedirect;
                }
                $resultRedirect->setPath('drc_productfeed/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the product feed.'));
            }
            $this->_getSession()->setDrcProductFeedProductfeedData($data);
            $resultRedirect->setPath(
                'drc_productfeed/*/edit',
                [
                    'productfeed_id' => $productfeed->getId(),
                    '_current' => true
                ]
            );
            return $resultRedirect;
        }
        $resultRedirect->setPath('drc_productfeed/*/');
        return $resultRedirect;
    }

    protected function format($data)
    {
        if (isset($data['currency']) && isset($data['format_price_currency_show'])
                && isset($data['format_price_decimals'])
                && isset($data['format_price_decimal_point'])
                && isset($data['format_price_thousands_separator'])
                ) {
            $variable = [ 'currency' => $data['currency'],
                              'format_price_currency_show' => $data['format_price_currency_show'],
                               'format_price_decimals'=>$data['format_price_decimals'],
                                'format_price_decimal_point'=>$data['format_price_decimal_point'],
                                'format_price_thousands_separator' => $data['format_price_thousands_separator']
                              
                           ];
                $data['format'] = json_encode($variable);
        }
        if (isset($data['ftp_enabled']) || isset($data['host'])
                || isset($data['delivery_type'])
                || isset($data['user'])
                || isset($data['password'])
                || isset($data['path'])
                || isset($data['passivemode'])
                ) {
                 $variable1 = [ 'ftp_enabled' => !empty($data['ftp_enabled']) ? $data['ftp_enabled'] :"",
                              'delivery_type'=> !empty($data['delivery_type']) ? $data['delivery_type'] :"",
                              'host' => !empty($data['host']) ? $data['host'] :"" ,
                               'user'=> !empty($data['user']) ? $data['user'] : "",
                               'password'=> !empty($data['password']) ? $data['password'] : "",
                               'path' => !empty($data['path']) ? $data['path'] :"",
                               'passivemode'=> !empty($data['passivemode']) ? $data['passivemode'] : "",
                              
                           ];
                 $data['delivery'] = json_encode($variable1);
        } else {
            $data['delivery'] =  "";
        }

        if (isset($data['product_types']) || isset($data['attribute_set'])
                || isset($data['product_visibility'])
         
              
                ) {
            $variable3 = [ 'product_types' => !empty($data['product_types']) ? implode(",", $data['product_types'])  :"",
                   'attribute_set' => !empty($data['attribute_set']) ? implode(",", $data['attribute_set'])  :"",
                   'product_visibility'=> !empty($data['product_visibility']) ? implode(",", $data['product_visibility'])  : "",
            ];
            $data['conditions'] = json_encode($variable3);
        } else {
            $data['conditions'] = "";
        }

                
        if (isset($data['header']) || isset($data['item'])
                || isset($data['content']) || isset($data['footer'])
         
              
                ) {
            $variable4 = [ 'header' => !empty($data['header']) ? $data['header']   :"",
                   'item' => !empty($data['item']) ? $data['item']  :"",
                   'content'=> !empty($data['content']) ?  $data['content']  : "",
                   'footer'=> !empty($data['footer']) ?  $data['footer']  : "",
                                  
                               
                              
            ];
            $data['content'] = json_encode($variable4);
        } else {
            $data['content'] = "";
        }
                
        return $data;
    }
}
