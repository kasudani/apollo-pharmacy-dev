<?php
namespace Drc\ProductFeed\Controller\Adminhtml\Productfeed;

use Magento\Framework\App\Filesystem\DirectoryList;

class SaveAndGenerate extends \Drc\ProductFeed\Controller\Adminhtml\Productfeed
{

     /**
     * Backend session
     *
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;
    protected $filesystem;
    protected $directoryList;
    protected $productCollectionFactory;
    protected $storeManagerInterface;
    protected $stockItemRepository;
    protected $categoryCollectionFactory;
    protected $ftp;
    protected $sftp;
    protected $stockStatusCriteriaFactory;
    protected $stockStatusRepository;
    const ROOT = DirectoryList::ROOT;

    protected $scopeConfig;
    /**
     * constructor
     *
     * @param \Magento\Backend\Model\Session $backendSession
     * @param \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Model\View\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Framework\Filesystem\Io\Ftp $ftp,
        \Magento\CatalogInventory\Api\StockStatusCriteriaInterfaceFactory $stockStatusCriteriaFactory,
        \Magento\CatalogInventory\Api\StockStatusRepositoryInterface $stockStatusRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
           
        $this->backendSession = $context->getSession();
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->stockItemRepository = $stockItemRepository;
        $this->ftp = $ftp;
        $this->stockStatusCriteriaFactory = $stockStatusCriteriaFactory;
        $this->stockStatusRepository = $stockStatusRepository;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($productfeedFactory, $registry, $context);
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $excludeCategories = $this->scopeConfig->getValue('drc_productfeed/configurable_exclude_products/categories', $storeScope);
        $excludeCategories = explode(",",$excludeCategories);

        $excludeProducts = $this->scopeConfig->getValue('drc_productfeed/configurable_exclude_products/skus', $storeScope);
        $excludeProducts = explode(",",$excludeProducts);
        //echo "<pre>";print_R($excludeCategories);echo "<pre>";print_R($excludeProducts);
        //die;

        $data = $this->getRequest()->getPost('productfeed');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->format($data);
            $productfeed = $this->initProductfeed();
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $productfeed->setId($id);
            }

            $productfeed->setData($data);
            
            try {
                $productfeed->save();

                $content = json_decode($productfeed->getData('content'), true);
                $xml = $content['header'];
                $filename = $productfeed->getData('filename');
                $conditions = json_decode($productfeed->getData('conditions'), true);
                
                $product_types = !empty($conditions['product_types']) ? explode(",", $conditions['product_types']) :$conditions['product_types'];
                $attribute_set = !empty($conditions['attribute_set']) ? explode(",", $conditions['attribute_set']) :$conditions['attribute_set'];
                $visibility = !empty($conditions['product_visibility']) ? explode(",", $conditions['product_visibility']) :$conditions['product_visibility'];                

                $collection = $this->productCollectionFactory->create();
                $collection->addStoreFilter($productfeed->getData('store_id'));
                $collection->addAttributeToSelect('*');

                if ($product_types) {
                    $collection->addAttributeToFilter('type_id', ['in'=>$product_types]);
                }
                if ($attribute_set) {
                    $collection->addAttributeToFilter('attribute_set_id', ['in'=>$attribute_set]);
                }
                if ($visibility) {
                    $collection->addAttributeToFilter('visibility', ['in'=>$visibility]);
                }
                if($excludeCategories){
                    $collection->addCategoriesFilter(['nin' => $excludeCategories]);
                }
                if($excludeProducts){
                    $collection->addAttributeToFilter('sku', ['nin'=>$excludeProducts]);
                }

                foreach ($collection as $product) {
                    $criteria = $this->stockStatusCriteriaFactory->create();
                    $criteria->setProductsFilter($product->getId());
                    $result = $this->stockStatusRepository->getList($criteria);
                    $stockStatus = current($result->getItems());
                    if($stockStatus->getStockStatus()!=1) {continue;}
                    $stock = $stockStatus->getStockStatus()==1?'in stock':'out of stock';

                    $xml .= '<' . $content['item'] . '>';
                    $c = $content['content'];
                    $c = str_replace('{attr_sku}', $product->getSku(), $c);
                    $c = str_replace('{attr_name}', htmlspecialchars($product->getName()), $c);
                    $c = str_replace('{attr_desc}', htmlspecialchars($product->getDescription()), $c);

                    $catIds = $product->getCategoryIds();
                    $categories = $this->categoryCollectionFactory->create()
                    ->addAttributeToSelect('name')
                    ->addAttributeToFilter('entity_id', ['in' => $catIds]);
                    $catpath = '';
                    foreach ($categories as $category) {
                        $catpath .= $category->getName() . ' > ';
                    }
                    $catpath = rtrim($catpath, ' > ');
                    $c = str_replace('{attr_type}', htmlspecialchars($catpath), $c);
                    $catpath = '';

                    if ($product->getVisibility() == 1) {
                        $c = str_replace('{attr_link}', '', $c);
                    } else {
                        $c = str_replace('{attr_link}', $product->getProductUrl(), $c);
                    }

                    $productImg = $product->getImage();
                    if (trim($productImg)!='') {
                        $productImgUrl = $this->storeManagerInterface->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'catalog/product'.$productImg;
                        $c = str_replace('{attr_image_link}', $productImgUrl, $c);
                    }
                    
                    /*$stock = $this->stockItemRepository->get($product->getId());
                    $stock = $stock->getIsInStock()==1?'in stock':'out of stock';*/



                    $c = str_replace('{attr_availability}', $stock, $c);
                    $c = str_replace('{attr_condition}', 'new', $c);
                    
                    $currencyCode = $productfeed->getData('currency');
                    $showCurrency = $productfeed->getData('format_price_currency_show');
                    $noOfDecimal = $productfeed->getData('format_price_decimals');
                    $decimalSeparator = $productfeed->getData('format_price_decimal_point');
                    $thousandSeparator = $productfeed->getData('format_price_thousands_separator');
                    if ($thousandSeparator == 'space') {
                        $thousandSeparator = ' ';
                    }
                    $price = number_format($product->getPriceInfo()->getPrice('final_price')->getValue(), $noOfDecimal, $decimalSeparator, $thousandSeparator);
                    if ($showCurrency==1) {
                        $c = str_replace('{attr_price}', $price . ' ' . $currencyCode, $c);
                    } else {
                        $c = str_replace('{attr_price}', $price, $c);
                    }
                    
                    if (trim($product->getAttributeText('manufacturer'))!='') {
                        $c = str_replace('{attr_manufacturer}', htmlspecialchars($product->getAttributeText('manufacturer')), $c);
                    } else {
                        $c = str_replace('{attr_manufacturer}', '', $c);
                    }
                    if (trim($product->getAttributeText('brand'))!='') {
                        $c = str_replace('{attr_brand}', htmlspecialchars($product->getAttributeText('brand')), $c);
                    } else {
                        $c = str_replace('{attr_brand}', '', $c);
                    }
                    $c = str_replace('{attr_gtin}', htmlspecialchars($product->getGtin()), $c);
                    $c = str_replace('{attr_google_category}', htmlspecialchars($product->getGoogleCategory()), $c);

                    $c = str_replace('{attr_identifier}', 'no', $c);

                    $xml .= $c;
                    $xml .= '</' . $content['item'] . '>';
                }
                $xml .= $content['footer'];

                $feedExt = '';
                switch ($productfeed->getData('feed_type')) {
                    case 1:
                        $feedExt = '.csv';
                        break;
                    case 2:
                        $feedExt = '.xml';
                        break;
                    case 3:
                        $feedExt = '.txt';
                        break;
                }
                
                if(trim($feedExt)=='')
                    $feedExt = '.xml';
                
                $xmlfile = $filename.$feedExt;
                $writer = $this->filesystem->getDirectoryWrite(self::ROOT);
                $file = $writer->openFile('feed/'.$xmlfile, 'w');
                try {
                    $file->lock();
                    try {
                        $file->write($xml);
                    }
                    finally {
                        $file->unlock();
                    }
                }
                finally {
                    $file->close();
                }
                
                $uploadFlag = false;
                if ($productfeed->getData('ftp_enabled')==1) {
                    $host = $productfeed->getData('host');
                    $user = $productfeed->getData('user');
                    $pwd = $productfeed->getData('password');
                    $protocol = $productfeed->getData('delivery_type');
                    $remotepath = $productfeed->getData('path');
                    $filepath = $this->directoryList->getRoot().'/feed/'.$xmlfile;
                    $passive = $productfeed->getData('passivemode');
                    if ($this->ftpUpload($host, $user, $pwd, $protocol, $xmlfile, $filepath, $remotepath, $passive)) {
                        $uploadFlag = true;
                    }
                }

                $this->messageManager->addSuccess(__('The product feed has been saved.').' '.__('Click <a target="_blank" href="'.$this->storeManagerInterface->getStore()->getBaseUrl().'feed/'.$filename.'.xml'.'">here</a> to view the feed.'));
                
                if ($productfeed->getData('ftp_enabled')==1) {
                    if ($uploadFlag) {
                        $this->messageManager->addSuccess(__('The product feed has been uploaded to FTP/SFTP account successfully.'));
                    } else {
                        $this->messageManager->addException($e, __('Something went wrong while uploading the product feed in FTP/SFTP account.'));
                    }
                }
                $this->backendSession->setDrcProductFeedProductfeedData(false);
                $resultRedirect->setPath(
                    'drc_productfeed/*/edit',
                    [
                        '_current' => true
                    ]
                );
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the product feed.'));
            }
            $this->_getSession()->setDrcProductFeedProductfeedData($data);
            $resultRedirect->setPath(
                'drc_productfeed/*/edit',
                [
                    'productfeed_id' => $productfeed->getId(),
                    '_current' => true
                ]
            );
            return $resultRedirect;
        }
    }

    protected function ftpUpload($host, $user, $pwd, $protocol, $filename, $filepath, $remotepath, $passive)
    {
        if ($protocol=='ftp') {
            $passive = $passive==1?true:false;
            $open = $this->ftp->open(['host' => $host,'user' => $user,'password' => $pwd, 'passive' => $passive]);
            if ($open) {
                $content = file_get_contents($filepath);
                $this->ftp->write($remotepath.$filename, $content);
                $this->ftp->close();
                return true;
            } else {
                return false;
            }
        } else {
            if (strpos($host, ':') !== false) {
                list($host, $port) = explode(':', $host, 2);
            } else {
                $port = 22;
            }
            $sftp = new \phpseclib\Net\SFTP($host, $port);
            $open = $sftp->login($user, $pwd);
            if ($open) {
                $content = file_get_contents($filepath);
                $sftp->put($remotepath.$filename, $content);
                $sftp->disconnect();
                return true;
            } else {
                return false;
            }
        }
    }
    
    protected function format($data)
    {
        if (isset($data['currency']) && isset($data['format_price_currency_show'])
                && isset($data['format_price_decimals'])
                && isset($data['format_price_decimal_point'])
                && isset($data['format_price_thousands_separator'])
                ) {
            $variable = [ 'currency' => $data['currency'],
                              'format_price_currency_show' => $data['format_price_currency_show'],
                               'format_price_decimals'=>$data['format_price_decimals'],
                                'format_price_decimal_point'=>$data['format_price_decimal_point'],
                                'format_price_thousands_separator' => $data['format_price_thousands_separator']
                              
                           ];
                $data['format'] = json_encode($variable);
        }
        if (isset($data['ftp_enabled']) || isset($data['host'])
                || isset($data['delivery_type'])
                || isset($data['user'])
                || isset($data['password'])
                || isset($data['path'])
                || isset($data['passivemode'])
                ) {
                 $variable1 = [ 'ftp_enabled' => !empty($data['ftp_enabled']) ? $data['ftp_enabled'] :"",
                              'delivery_type'=> !empty($data['delivery_type']) ? $data['delivery_type'] :"",
                              'host' => !empty($data['host']) ? $data['host'] :"" ,
                               'user'=> !empty($data['user']) ? $data['user'] : "",
                               'password'=> !empty($data['password']) ? $data['password'] : "",
                               'path' => !empty($data['path']) ? $data['path'] :"",
                               'passivemode'=> !empty($data['passivemode']) ? $data['passivemode'] : "",
                              
                           ];
                 $data['delivery'] = json_encode($variable1);
        } else {
            $data['delivery'] =  "";
        }
        
        if (isset($data['product_types']) || isset($data['attribute_set'])
                || isset($data['product_visibility'])
                ) {
            $variable3 = [ 'product_types' => !empty($data['product_types']) ? implode(",", $data['product_types'])  :"",
                   'attribute_set' => !empty($data['attribute_set']) ? implode(",", $data['attribute_set'])  :"",
                   'product_visibility'=> !empty($data['product_visibility']) ? implode(",", $data['product_visibility'])  : "",
            ];
            $data['conditions'] = json_encode($variable3);
        } else {
            $data['conditions'] = "";
        }

                
        if (isset($data['header']) || isset($data['item'])
                || isset($data['content']) || isset($data['footer'])
         
              
                ) {
            $variable4 = [ 'header' => !empty($data['header']) ? $data['header']   :"",
                 'item' => !empty($data['item']) ? $data['item']  :"",
                 'content'=> !empty($data['content']) ?  $data['content']  : "",
                 'footer'=> !empty($data['footer']) ?  $data['footer']  : "",
            ];
            $data['content'] = json_encode($variable4);
        } else {
            $data['content'] = "";
        }
                
        return $data;
    }
}
