<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Controller\Adminhtml\Productfeed;

abstract class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * JSON Factory
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * ProductFeed Factory
     *
     * @var \Drc\ProductFeed\Model\ProductfeedFactory
     */
    protected $productfeedFactory;

    /**
     * constructor
     *
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
    
        $this->jsonFactory        = $jsonFactory;
        $this->productfeedFactory = $productfeedFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }
        foreach (array_keys($postItems) as $productfeedId) {
            /** @var \Drc\ProductFeed\Model\Productfeed $productfeed */
            $productfeed = $this->productfeedFactory->create()->load($productfeedId);
            try {
                $productfeedData = $postItems[$productfeedId];//todo: handle dates
                $productfeed->addData($productfeedData);
                $productfeed->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithProductfeedId($productfeed, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithProductfeedId($productfeed, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithProductfeedId(
                    $productfeed,
                    __('Something went wrong while saving the product feed.')
                );
                $error = true;
            }
        }
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add ProductFeed id to error message
     *
     * @param \Drc\ProductFeed\Model\Productfeed $productfeed
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithProductfeedId(\Drc\ProductFeed\Model\Productfeed $productfeed, $errorText)
    {
        return '[Product Feed ID: ' . $productfeed->getId() . '] ' . $errorText;
    }
}
