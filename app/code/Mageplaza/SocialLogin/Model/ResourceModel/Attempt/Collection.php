<?php

namespace Mageplaza\SocialLogin\Model\ResourceModel\Attempt;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection

{

    protected function _construct()

    {

        $this->_init('Mageplaza\SocialLogin\Model\Attempt', 'Mageplaza\SocialLogin\Model\ResourceModel\Attempt');

    }

}

?>