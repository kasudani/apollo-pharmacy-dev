<?php

namespace Mageplaza\SocialLogin\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Entity extends AbstractDb

{

    protected function _construct()

    {

        $this->_init('customer_entity_text', 'value_id');

    }

}

?>