<?php

namespace Mageplaza\SocialLogin\Model\ResourceModel\Otp;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection

{

    protected function _construct()

    {

        $this->_init('Mageplaza\SocialLogin\Model\Otp', 'Mageplaza\SocialLogin\Model\ResourceModel\Otp');

    }

}

?>