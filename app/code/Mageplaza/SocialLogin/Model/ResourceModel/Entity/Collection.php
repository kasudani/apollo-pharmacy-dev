<?php

namespace Mageplaza\SocialLogin\Model\ResourceModel\Entity;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection

{

    protected function _construct()

    {

        $this->_init('Mageplaza\SocialLogin\Model\Entity', 'Mageplaza\SocialLogin\Model\ResourceModel\Entity');

    }

}

?>