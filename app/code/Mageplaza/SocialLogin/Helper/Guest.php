<?php

namespace Mageplaza\SocialLogin\Helper;

use Magento\Framework\App\RequestInterface;
use Mageplaza\SocialLogin\Helper\Data as HelperData;
use \Magento\Framework\App\Helper\AbstractHelper;

class Guest extends AbstractHelper
{

    protected $_directoryList;

    public function __construct(
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Mageplaza\SocialLogin\Model\Guest $guestFactory
    ) {
        $this->guestFactory = $guestFactory;
        $this->_directoryList = $directoryList;
    }
    
    public function getCustomer($cust_mobile)
    {
        $failures_num = "";
        $model1 = $this->guestFactory->getCollection();
        $model1->addFieldToFilter('customer_mobile',$cust_mobile);
        foreach($model1->getData() as $rtu1)
            {
                $failures_num = $rtu1['failures_num']; 
            }
        return $failures_num;
    }

    public function getExpire($cust_mobile)
    {
        $expire_time = "";
        $model1 = $this->guestFactory->getCollection();
        $model1->addFieldToFilter('customer_mobile',$cust_mobile);
        foreach($model1->getData() as $rtu1)
            {
                $expire_time = $rtu1['lock_expires']; 
            }
        return $expire_time;
    }

    public function getNum($cust_mobile)
    {
        $failures_num = "";
        $model1 = $this->guestFactory->getCollection();
        $model1->addFieldToFilter('customer_mobile',$cust_mobile);
        foreach($model1->getData() as $rtu1)
            {
                $failures_num = $rtu1['tmp_count']; 
            }
        return $failures_num;
    }

}
