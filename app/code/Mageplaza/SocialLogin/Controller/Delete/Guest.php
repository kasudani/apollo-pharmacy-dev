<?php 

namespace Mageplaza\SocialLogin\Controller\Delete;

class Guest extends \Magento\Framework\App\Action\Action {

    protected $_objectManager;

    protected $guestFactory;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\SocialLogin\Model\Guest $guestFactory
    )
    {
        $this->guestFactory     = $guestFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

	    $mobile = $this->getRequest()->getPost('otp_telephone');
	    $model = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Guest');
        $collection = $model->getCollection();
        $collection->addFieldToFilter('customer_mobile', $mobile);
		if(empty($collection->getData())){
		 echo "no";
		}else{
            foreach ($collection as $rtu) {
                    $model->load($rtu['id'], 'id');
                    $model->setCustomerOtp(0);
                    $model->save();
                }
		echo "yes";
		}
    }
}
?>
