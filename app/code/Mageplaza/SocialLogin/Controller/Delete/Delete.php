<?php 

namespace Mageplaza\SocialLogin\Controller\Delete;

class Otp extends \Magento\Framework\App\Action\Action {

    protected $entityFactory;

    protected $_objectManager;

    protected $otpFactory;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\SocialLogin\Model\Otp $otpFactory
    )
    {
        $this->otpFactory = $otpFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

	    $mobile = $this->getRequest()->getPost('valid_mobile');
	    $model = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Otp');
        $collection = $model->getCollection();
        $collection->addFieldToFilter('mobile', $mobile);
		if(empty($collection->getData())){
		 echo "no";
		}else{
            foreach ($collection as $rtu) {
                    $model->load($rtu['id'], 'id');
                    $model->setOtp(0);
                    $model->save();
                }
		echo "yes";
		}
    }
}
?>
