<?php 

namespace Mageplaza\SocialLogin\Controller\Mobile;
 
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
 
class Index extends \Magento\Framework\App\Action\Action {

    protected $entityFactory;

    protected $otpFactory;

    protected $_objectManager;

    protected $_helper;

    protected $attemptFactory;

        /**
     * @var Session
     */
    protected $session;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    	\Mageplaza\SocialLogin\Helper\Attempt $helper,
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\SocialLogin\Model\Attempt $attemptFactory,
        \Mageplaza\SocialLogin\Model\Entity $entityFactory,
        \Mageplaza\SocialLogin\Model\Otp $otpFactory,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        Session $customerSession,
        PageFactory $resultPageFactory
    )
    {

    	$this->session = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->entityFactory = $entityFactory;
        $this->dateTime = $dateTime;
        $this->otpFactory = $otpFactory;
        $this->helper = $helper;
        $this->attemptFactory = $attemptFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
	    if ($this->session->isLoggedIn()) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            //echo $_SESSION['refered_url']; die;
            if(isset($_SESSION['refered_url']) && $_SESSION['refered_url'] && strpos($_SESSION['refered_url'], 'logi' ) == false)
            $resultRedirect->setPath($_SESSION['refered_url']);
            else
            $resultRedirect->setPath($storeManager->getStore()->getBaseUrl());
            return $resultRedirect;
        } else {
        	/** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('sociallogin/mobile/login');
            return $resultRedirect;
        }
    }
}
?>