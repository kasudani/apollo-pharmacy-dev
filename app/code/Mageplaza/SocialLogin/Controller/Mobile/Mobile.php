<?php 

namespace Mageplaza\SocialLogin\Controller\Mobile;
 
 
class Mobile extends \Magento\Framework\App\Action\Action {

    protected $entityFactory;

    protected $otpFactory;

    protected $_objectManager;

    protected $_helper;

    protected $attemptFactory;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    	\Mageplaza\SocialLogin\Helper\Attempt $helper,
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\SocialLogin\Model\Attempt $attemptFactory,
        \Mageplaza\SocialLogin\Model\Entity $entityFactory,
        \Mageplaza\SocialLogin\Model\Otp $otpFactory,
        \Magento\Framework\Stdlib\DateTime $dateTime
    )
    {
        $this->entityFactory = $entityFactory;
        $this->dateTime = $dateTime;
        $this->otpFactory = $otpFactory;
        $this->helper = $helper;
        $this->attemptFactory = $attemptFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
	    $otp = rand(10000,99999);
	    $lockThreshold = 3;
	    $mobile = $this->getRequest()->getPost('mobile');
	    $model = $this->entityFactory->getCollection();
        $model->addFieldToFilter('value', $mobile);
        $model->addFieldToFilter('attribute_id', '155');
		if(empty($model->getData())){
		echo 'not';
		}
		else{
		$entity_id = $this->helper->getId($mobile);
		$failures_num = $this->helper->getCustomer($mobile);
		$lock_expires = $this->helper->getExpire($mobile);
		$question = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Attempt');
        $collection = $question->getCollection();
        $collection->addFieldToFilter('entity_id', $entity_id);
        $now = new \DateTime();
        $current_time = $this->dateTime->formatDate($now);
        $lockTime           = 1;
        $lockThreshInterval = new \DateInterval('PT' . $lockTime . 'M');
        $end_time = $this->dateTime->formatDate($now->add($lockThreshInterval));
        $B = strtotime($lock_expires);
        $C = strtotime($current_time);
        if($failures_num >= $lockThreshold){
          if ($B < $C){
           foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->setTmpCount(0);
                    $question->save();
                }
                $number  =  $mobile;
		$my_arr = array(
				"mobile" => $mobile,
				"otp" => $otp,
				"set_time" => $current_time,
				"end_time" => $end_time
				);	
        $xml = '<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<Sndotp xmlns="http://tempuri.org/">
					  <otp>'.$otp.'</otp>
					  <mobile>'.$number.'</mobile>
					</Sndotp>
				  </soap:Body>
				</soap:Envelope>';

				$url = "http://124.124.88.106:8083/smsservice.asmx?op=Sndotp";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

				$headers = array();
				array_push($headers, "Content-Type: text/xml; charset=utf-8");
				array_push($headers, "Accept: text/xml");
				array_push($headers, "Cache-Control: no-cache");
				array_push($headers, "Pragma: no-cache");
				if($xml != null) 
						{
							curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
							array_push($headers, "Content-Length: " . strlen($xml));
						}
				curl_setopt($ch, CURLOPT_USERPWD, "user_name:password"); /* If required */
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$response = curl_exec($ch);
				$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
                  if($code!= 'not'){
                  	$question = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Otp');
                    $model1 = $question->getCollection();
			        $model1->addFieldToFilter('mobile', $mobile);
                    if(empty($model1->getData())){
                    $question->setData($my_arr)->save();
					echo 'sent';
					}else{
						foreach($model1->getData() as $rtu){
							    $question->load($rtu['id'],'id');			
								$question->setOtp($otp);
								$question->setSetTime($current_time);
								$question->setEndTime($end_time);
								$question->save();
								echo "sent";						
						}			
					}
                  }
          }else{
          	echo "blocked";
          }
          
        }else{
        	$number  =  $mobile;
		$my_arr = array(
				"mobile" => $mobile,
				"otp" => $otp,
				"set_time" => $current_time,
				"end_time" => $end_time
				);	
        $xml = '<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<Sndotp xmlns="http://tempuri.org/">
					  <otp>'.$otp.'</otp>
					  <mobile>'.$number.'</mobile>
					</Sndotp>
				  </soap:Body>
				</soap:Envelope>';

				$url = "http://124.124.88.106:8083/smsservice.asmx?op=Sndotp";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

				$headers = array();
				array_push($headers, "Content-Type: text/xml; charset=utf-8");
				array_push($headers, "Accept: text/xml");
				array_push($headers, "Cache-Control: no-cache");
				array_push($headers, "Pragma: no-cache");
				if($xml != null) 
						{
							curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
							array_push($headers, "Content-Length: " . strlen($xml));
						}
				curl_setopt($ch, CURLOPT_USERPWD, "user_name:password"); /* If required */
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$response = curl_exec($ch);
				$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
                  if($code!= 'not'){
                  	$question = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Otp');
                    $model1 = $question->getCollection();
			        $model1->addFieldToFilter('mobile', $mobile);
                    if(empty($model1->getData())){
                    $question->setData($my_arr)->save();
					echo 'sent';
					}else{
						foreach($model1->getData() as $rtu){
							    $question->load($rtu['id'],'id');			
								$question->setOtp($otp);
								$question->setSetTime($current_time);
								$question->setEndTime($end_time);
								$question->save();
								echo "sent";						
						}			
					}
                  }
		}
        }
		
    }
}
?>
