<?php 

namespace Mageplaza\SocialLogin\Controller\Verify;
 
 
class Otp extends \Magento\Framework\App\Action\Action {

    protected $guestFactory;

    protected $_objectManager;
    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\SocialLogin\Model\Guest $guestFactory,        
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Mageplaza\SocialLogin\Helper\Guest $helper
    )
    {
        $this->guestFactory = $guestFactory;
        $this->helper           = $helper;
        $this->dateTime         = $dateTime;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
	    $otp = rand(10000,99999);
	    $mobile = $this->getRequest()->getPost('my_mobile');
	    $question = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Guest');
        $model1 = $question->getCollection();
		$number  =  $mobile;
		$now                = new \DateTime();
        $lockThreshold      = 3;
        $lockTime           = 15;
        $lockThreshInterval = new \DateInterval('PT' . $lockTime . 'M');
        $lock_time          = date('d-m-Y H:i');
        $failures_num = $this->helper->getCustomer($mobile);
        $failuresNum  = $this->helper->getNum($mobile);
        $lock_expires = $this->helper->getExpire($mobile);
        $current_time = $this->dateTime->formatDate($now);
        $B            = strtotime($lock_expires);
        $C            = strtotime($current_time);

        if ($failures_num >= $lockThreshold) {
        	if ($B < $C) {
              foreach ($model1->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->setTmpCount(0);
                    $question->save();
                }
            $my_arr = array(
				"customer_otp" => $otp,
				"customer_mobile" => $mobile
				);	
        $xml = '<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<Sndotp xmlns="http://tempuri.org/">
					  <otp>'.$otp.'</otp>
					  <mobile>'.$number.'</mobile>
					</Sndotp>
				  </soap:Body>
				</soap:Envelope>';

				$url = "http://124.124.88.106:8083/smsservice.asmx?op=Sndotp";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

				$headers = array();
				array_push($headers, "Content-Type: text/xml; charset=utf-8");
				array_push($headers, "Accept: text/xml");
				array_push($headers, "Cache-Control: no-cache");
				array_push($headers, "Pragma: no-cache");
				if($xml != null) 
						{
							curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
							array_push($headers, "Content-Length: " . strlen($xml));
						}
				curl_setopt($ch, CURLOPT_USERPWD, "user_name:password"); /* If required */
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$response = curl_exec($ch);
				$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
                  if($code!= 'not'){
			        $model1->addFieldToFilter('customer_mobile', $mobile);
                    if(empty($model1->getData())){
                    $question->setData($my_arr)->save();
					echo 'sent';
					}else{
						foreach($model1->getData() as $rtu){
							    $question->load($rtu['id'],'id');			
								$question->setCustomerOtp($otp);
								$question->save();
								echo "sent";						
						}			
					}
                  }
        	}else{
        	 		echo "blocked";
        	 	}
        }else{
        	$my_arr = array(
				"customer_otp" => $otp,
				"customer_mobile" => $mobile
				);	
        $xml = '<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<Sndotp xmlns="http://tempuri.org/">
					  <otp>'.$otp.'</otp>
					  <mobile>'.$number.'</mobile>
					</Sndotp>
				  </soap:Body>
				</soap:Envelope>';

				$url = "http://124.124.88.106:8083/smsservice.asmx?op=Sndotp";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

				$headers = array();
				array_push($headers, "Content-Type: text/xml; charset=utf-8");
				array_push($headers, "Accept: text/xml");
				array_push($headers, "Cache-Control: no-cache");
				array_push($headers, "Pragma: no-cache");
				if($xml != null) 
						{
							curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
							array_push($headers, "Content-Length: " . strlen($xml));
						}
				curl_setopt($ch, CURLOPT_USERPWD, "user_name:password"); /* If required */
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$response = curl_exec($ch);
				$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
                  if($code!= 'not'){
			        $model1->addFieldToFilter('customer_mobile', $mobile);
                    if(empty($model1->getData())){
                    $question->setData($my_arr)->save();
					echo 'sent';
					}else{
						foreach($model1->getData() as $rtu){
							    $question->load($rtu['id'],'id');			
								$question->setCustomerOtp($otp);
								$question->save();
								echo "sent";						
						}			
					}
                  }
        }

		
		//\Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug($response);
    }
}
?>