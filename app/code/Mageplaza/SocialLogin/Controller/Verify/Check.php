<?php 

namespace Mageplaza\SocialLogin\Controller\Verify;
 ob_start();
 
class Check extends \Magento\Framework\App\Action\Action {

    protected $guestFactory;

    protected $_objectManager;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\SocialLogin\Model\Guest $guestFactory,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Mageplaza\SocialLogin\Helper\Guest $helper
    )
    {
        $this->guestFactory     = $guestFactory;
        $this->helper           = $helper;
        $this->dateTime         = $dateTime;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $mobile = $this->getRequest()->getPost('otp_telephone');
        $otp = $this->getRequest()->getPost('my_check_otp');
        $now                = new \DateTime();
        $lockThreshold      = 3;
        $lockTime           = 15;
        $lockThreshInterval = new \DateInterval('PT' . $lockTime . 'M');
        $lock_time          = date('d-m-Y H:i');
        $failures_num = $this->helper->getCustomer($mobile);
        $failuresNum  = $this->helper->getNum($mobile);

        $question = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Guest');
        $model1 = $question->getCollection();
        $model2 = $question->getCollection();
        $model1->addFieldToFilter('customer_mobile', $mobile);
        $model2->addFieldToFilter('customer_mobile', $mobile);
        $model1->addFieldToFilter('customer_otp', $otp);

        if ($failures_num >= $lockThreshold) { //account is locked
            $lock_expires = $this->helper->getExpire($mobile);
            $current_time = $this->dateTime->formatDate($now);
            $B            = strtotime($lock_expires);
            $C            = strtotime($current_time);
            if ($B < $C) {
              foreach ($model1->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->setTmpCount(0);
                    $question->save();
                }
                if (empty($model1->getData())) {
                foreach ($model1->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $tmp_count = $der['tmp_count'];
                    $question->setTmpCount($tmp_count + 1);
                    $question->save();
                }
                echo "not matched";
            } else {
                foreach ($model1->getData() as $der) {
                    $question->load($der['id'], 'id');
                    if ($der['tmp_count'] > 0) {
                            $question->setTmpCount(0);
                            $question->save();
                        }
                }
                echo "";
            }
            $failuresNum = $this->helper->getNum($mobile);
            if ($failuresNum >= 3) {
                foreach ($model2->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $question->setFailuresNum(3);
                    $question->setFirstFailure($this->dateTime->formatDate($now));
                    $question->setLockExpires($this->dateTime->formatDate($now->add($lockThreshInterval)));
                    $question->setTmpCount(0);
                    $question->save();
                }
            } else {
                foreach ($model1->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->save();
                }
            }
            }else{
                echo "blocked";
            }
        }else{
            /* when its not locked */
            if (empty($model1->getData())) {
                foreach ($model2->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $tmp_count = $der['tmp_count'];
                    $question->setTmpCount($tmp_count + 1);
                    $question->save();
                }
                echo "not matched";
            } else {
                foreach ($model1->getData() as $der) {
                    $question->load($der['id'], 'id');
                    if ($der['tmp_count'] > 0) {
                            $question->setTmpCount(0);
                            $question->save();
                        }
                }
                echo "";
            }
            $failuresNum = $this->helper->getNum($mobile);
            if ($failuresNum >= 3) {
                foreach ($model2->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $question->setFailuresNum(3);
                    $question->setFirstFailure($this->dateTime->formatDate($now));
                    $question->setLockExpires($this->dateTime->formatDate($now->add($lockThreshInterval)));
                    $question->setTmpCount(0);
                    $question->save();
                }
            } else {
                foreach ($model1->getData() as $der) {
                    $question->load($der['id'], 'id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->save();
                }
            }
        }
    }
}
?>