<?php

namespace Mageplaza\SocialLogin\Controller\Verify;
ob_start();

class Verify extends \Magento\Framework\App\Action\Action
{
    
    protected $entityFactory;
    
    protected $attemptFactory;
    
    protected $otpFactory;
    
    protected $_objectManager;
    
    protected $_customerModel;
    
    protected $_customerSession;
    
    protected $_otpModel;
    
    protected $_helper;
    
    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(\Mageplaza\SocialLogin\Helper\Attempt $helper, \Magento\Framework\App\Action\Context $context, \Mageplaza\SocialLogin\Model\Entity $entityFactory, \Mageplaza\SocialLogin\Model\Attempt $attemptFactory, \Mageplaza\SocialLogin\Model\Otp $otpFactory, \Magento\Customer\Model\Customer $customerModel, \Magento\Customer\Model\Session $customerSession, \Mageplaza\SocialLogin\Model\Otp $otpModel, \Magento\Framework\Stdlib\DateTime $dateTime)
    {
        $this->entityFactory    = $entityFactory;
        $this->attemptFactory   = $attemptFactory;
        $this->otpFactory       = $otpFactory;
        $this->helper           = $helper;
        $this->_customerModel   = $customerModel;
        $this->_customerSession = $customerSession;
        $this->_otpModel        = $otpModel;
        $this->dateTime         = $dateTime;
        parent::__construct($context);
    }
    
    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $now                = new \DateTime();
        $lockThreshold      = 3;
        $lockTime           = 15;
        $lockThreshInterval = new \DateInterval('PT' . $lockTime . 'M');
        $lock_time          = date('d-m-Y H:i');

        $mobile       = $this->getRequest()->getPost('mobile');
        $otp          = $this->getRequest()->getPost('otp');
        $model1       = $this->_otpModel->getCollection()->addFieldToFilter('mobile', $mobile)->addFieldToFilter('otp', $otp);
        $failures_num = $this->helper->getCustomer($mobile);
        $entity_id    = $this->helper->getId($mobile);
        $failuresNum  = $this->helper->getNum($mobile);
        $question     = $this->_objectManager->create('Mageplaza\SocialLogin\Model\Attempt');
        $collection   = $question->getCollection();

        $collection->addFieldToFilter('entity_id', $entity_id);
        if ($failures_num >= $lockThreshold) {
            $lock_expires = $this->helper->getExpire($mobile);
            $current_time = $this->dateTime->formatDate($now);
            $B            = strtotime($lock_expires);
            $C            = strtotime($current_time);
            if ($B < $C) {
                foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->setTmpCount(0);
                    $question->save();
                }
                if (empty($model1->getData())) {
                foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $tmp_count = $der['tmp_count'];
                    $question->setTmpCount($tmp_count + 1);
                    $question->save();
                }
                echo "not matched";
            } else {
                $model = $this->entityFactory->getCollection();
                $model->addFieldToFilter('value', $mobile);
                if (empty($model->getData())) {
                } else {
                    foreach ($collection->getData() as $der) {
                        $question->load($der['entity_id'], 'entity_id');
                        if ($der['tmp_count'] > 0) {
                            $question->setTmpCount(0);
                            $question->save();
                        }
                    }
                    foreach ($model->getData() as $rtu) {
                        $ent_id       = $rtu['entity_id'];
                        $customerData = $this->_customerModel->setWebsiteId(1);
                        $customer     = $customerData->load($ent_id);
                        $this->_customerSession->loginById($ent_id);
                    }
                }
            }
            $failuresNum = $this->helper->getNum($mobile);
            if ($failuresNum >= 3) {
                foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $question->setFailuresNum(3);
                    $question->setFirstFailure($this->dateTime->formatDate($now));
                    $question->setLockExpires($this->dateTime->formatDate($now->add($lockThreshInterval)));
                    $question->setTmpCount(0);
                    $question->save();
                }
            } else {
                foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->save();
                }
            }
            } else {
                echo "blocked";
            }
        } else {
 
            if (empty($model1->getData())) {
                foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $tmp_count = $der['tmp_count'];
                    $question->setTmpCount($tmp_count + 1);
                    $question->save();
                }
                echo "not matched";
            } else {
                 $model = $this->entityFactory->getCollection();
                $model->addFieldToFilter('value', $mobile);
                if (empty($model->getData())) {
                } else {
                    foreach ($collection->getData() as $der) {
                        $question->load($der['entity_id'], 'entity_id');
                        if ($der['tmp_count'] > 0) {
                            $question->setTmpCount(0);
                            $question->save();
                        }
                    }
                    foreach ($model->getData() as $rtu) {
                        $ent_id       = $rtu['entity_id'];
                        $customerData = $this->_customerModel->setWebsiteId(1);
                        $customer     = $customerData->load($ent_id);
                        $this->_customerSession->loginById($ent_id);

                    }
                }
                
            }
            $failuresNum = $this->helper->getNum($mobile);
            if ($failuresNum >= 3) {
foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $question->setFailuresNum(3);
                    $question->setFirstFailure($this->dateTime->formatDate($now));
                    $question->setLockExpires($this->dateTime->formatDate($now->add($lockThreshInterval)));
                    $question->setTmpCount(0);
                    $question->save();
                }
            } else {
                foreach ($collection->getData() as $der) {
                    $question->load($der['entity_id'], 'entity_id');
                    $question->setFailuresNum(0);
                    $question->setFirstFailure(null);
                    $question->setLockExpires(null);
                    $question->save();
                }
            }
        }      
    }
}