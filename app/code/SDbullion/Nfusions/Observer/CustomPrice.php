<?php

    /**
     * Webkul Hello CustomPrice Observer
     *
     * @category    Webkul
     * @package     Webkul_Hello
     * @author      Webkul Software Private Limited
     *
     */

    namespace SDbullion\Nfusions\Observer;
 

    use Magento\Framework\Event\ObserverInterface;

    use Magento\Framework\App\RequestInterface;

 

    class CustomPrice implements ObserverInterface
    {

        protected $_coreSession;

        protected $_storeManager;     

        protected $customModelFactory;

        public function __construct(

            \Magento\Store\Model\StoreManagerInterface $storeManager,

             \Magento\Framework\Session\SessionManagerInterface $coreSession,

            \CustomerRicoupons\Couponmodule\Model\CouponmoduleFactory $CouponlistingFactory,

            \CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule $resource

        )
        {

            $this->_coreSession = $coreSession;

             $this->_connection = $resource->getConnection();

             $this->customModelFactory = $CouponlistingFactory;

            $this->_storeManager = $storeManager;

        }

       public function setCouponsVariable($itemid,$couponid)
        {

                $this->_coreSession->start(); 



                  $coupons_used=$this->_coreSession->getCouponsUsed();

              

               if(empty($coupons_used)){



                 $coupon[$itemid]=$couponid;

                

                  

              $this->_coreSession->setCouponsUsed($coupon);



            }

                 else

            {

                   $set=0;



                    foreach($coupons_used as $key=>$coup)

                    {

                         if($key==$itemid)

                         {

                              $set=1;

                         }

                    }

                        if($set==0)

                        {

                        $coupons_used[$itemid]=$couponid;

                       

                        $this->_coreSession->setCouponsUsed($coupons_used);

                       }



            }



        }

        public function getTableData()
        {

           date_default_timezone_set('Asia/Kolkata');

            $today=date("Y-m-d H:i:s");

            $myTable = $this->_connection->getTableName('customer_custom_coupons');

            $sql     = $this->_connection->select('id','product_skus','uses_per_cust','uses_per_cpn','discount_percent')->from(["tn" => $myTable])

                  ->where('to_date >= ?',$today)

                  ->where('from_date <= ?',$today);      

            $result  = $this->_connection->fetchAll($sql); 

            return $result;

        }

        public function execute(\Magento\Framework\Event\Observer $observer) 
        {

            $item = $observer->getEvent()->getData('quote_item');

                       

            $item = ( $item->getParentItem() ? $item->getParentItem() : $item );

    

              $data=$this->getTableData();

                $itemslist=array();

                $itemslistnew=array();

            foreach($data as $dat)
            {

              $skus=explode(",",$dat['product_skus']);

                   

                 foreach($skus as $sk)
                 {     

                     $itemslist[$sk]=$dat['discount_percent'];

                     $itemslistnew[$sk]=$dat['id'];

                 }      

            }

             if(array_key_exists($item->getProduct()->getSku(), $itemslist))
             {

                 $discount_percent= $itemslist[$item->getProduct()->getSku()];



                $final_price_before_cpn= $item->getProduct()->getFinalPrice();



                $discount_amount= ($final_price_before_cpn*$discount_percent)/100;

                $final_price_after_cpn=$final_price_before_cpn-$discount_amount;

                   

                $item->setCustomPrice($final_price_after_cpn);

                $item->setOriginalCustomPrice($final_price_after_cpn);

                $item->getProduct()->setIsSuperMode(true);

                 $this->setCouponsVariable($item->getProduct()->getId(),$itemslistnew[$item->getProduct()->getSku()]);
        

            }

        }

    }