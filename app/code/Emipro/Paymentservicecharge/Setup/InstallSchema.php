<?php
namespace Emipro\Paymentservicecharge\Setup;
 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
 
class InstallSchema implements InstallSchemaInterface {
 
    public function install( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;
        
         $installer->startSetup();
        
        $installer->    run("ALTER TABLE  `".$setup->getTable('quote_address')."` ADD  `paycharge_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('quote_address')."` ADD  `paycharge_base_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('quote_address')."` ADD  `paycharge_fee_name` varchar( 255)  NULL");     
        
        $installer->run("ALTER TABLE  `".$setup->getTable('sales_order')."` ADD  `paycharge_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('sales_order')."` ADD  `paycharge_base_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('sales_order')."` ADD  `paycharge_fee_name` varchar( 255)  NULL");       

        $installer->run("ALTER TABLE  `".$setup->getTable('sales_invoice')."` ADD  `paycharge_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('sales_invoice')."` ADD  `paycharge_base_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('sales_invoice')."` ADD  `paycharge_fee_name` varchar( 255)  NULL");     

        $installer->run("ALTER TABLE  `".$setup->getTable('sales_creditmemo')."` ADD  `paycharge_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('sales_creditmemo')."` ADD  `paycharge_base_fee` DECIMAL( 10, 2 )  NULL");
        $installer->run("ALTER TABLE  `".$setup->getTable('sales_creditmemo')."` ADD  `paycharge_fee_name` varchar(255)  NULL");      

         $installer->endSetup();
         
}
}
