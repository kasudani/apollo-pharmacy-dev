<?php
namespace Emipro\Paymentservicecharge\Observer;
use Magento\Framework\Event\ObserverInterface;

class Paymentfee implements ObserverInterface
{
    protected $scopeConfig;
   public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {  

    		$quote=$observer->getQuote();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger=$objectManager->create("\Psr\Log\LoggerInterface");
        $selectedpaymentmethod=$quote->getPayment()->getMethod();
        $customer_group_id=$objectManager->create('Magento\Customer\Model\Session')->getCustomerGroupId();
        $enable = $objectManager->get('Emipro\Paymentservicecharge\Helper\Data')->getConfig('checkout/general/active', true);
        
        $ua_regexp = $objectManager->get('Emipro\Paymentservicecharge\Helper\Data')->getConfig('checkout/general/active2', true);
        
        $table_data=unserialize($ua_regexp);

        $isCod = '';

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get('Apollo\Cartrule\Helper\Data');

        $$isCod = $helper->getIsCodCharge(); 

        $order=$observer->getOrder();
       
       foreach($table_data as $data)
        {
            if(($data['payment_method'] == $selectedpaymentmethod || $data["payment_method"]==32000) && ($customer_group_id==$data["customer_group"] || $data["customer_group"]==32000))
            {
              
    
                  $service_charge=$quote->getPaychargeFee();
                  $base_service_charge=$quote->getPaychargeBaseFee();
                  $name=$quote->getPaychargeFeeName();

                  if($$isCod){
                    $$service_charge = $service_charge;
                  }else{
                      $$service_charge = 0;
                  }
                  
      			      $order->setPaychargeFee($service_charge);
      			      $order->setPaychargeBaseFee($service_charge);
      			      $order->setPaychargeFeeName($name);
      			      $order->setGrandTotal($order->getGrandTotal());
      			      $order->setBaseGrandTotal($order->getBaseGrandTotal());
            
            }
        }
    }
    
}
