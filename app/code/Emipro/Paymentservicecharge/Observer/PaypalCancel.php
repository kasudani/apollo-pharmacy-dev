<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Emipro\Paymentservicecharge\Observer;

use Magento\Framework\Event\ObserverInterface;

class PaypalCancel implements ObserverInterface {

	protected $scopeConfig;
    protected $_collection;
	protected $request;
	protected $_messageManager;
	protected $pointsSession;
    public function __construct(\Psr\Log\LoggerInterface $logger, \Magento\Sales\Model\ResourceModel\Order\Collection $collection,
    	\Magento\Framework\App\Request\Http $request,
    	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    	\Magento\Framework\Message\ManagerInterface $messageManager) {
        $this->_collection = $collection;
        $this->_logger = $logger;
        $this->request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_messageManager = $messageManager;

    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $pointsSession=$objectManager->get('Magento\Catalog\Model\Session');
        $pointsSession->setCriditcustomitem(0);
        $pointsSession->setCriditcustomitempoint(0);
        $pointsSession->setCalcLable('');
    }

}
