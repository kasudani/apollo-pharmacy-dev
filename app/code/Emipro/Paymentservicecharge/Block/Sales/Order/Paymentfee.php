<?php
namespace Emipro\Paymentservicecharge\Block\Sales\Order;

class Paymentfee extends \Magento\Framework\View\Element\Template
{
    
    protected $_config;
    protected $_order;
    protected $_source;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Tax\Model\Config $taxConfig,
        array $data = []
    ) {
        $this->_config = $taxConfig;
        parent::__construct($context, $data);
    }

    
    public function displayFullSummary()
    {
        return true;
    }

    
    public function getSource()
    {
        return $this->_source;
    } 
    public function getStore()
    {
        return $this->_order->getStore();
    }

        public function getOrder()
    {
        return $this->_order;
    }

    
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    
    public function initTotals()
    {
       
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();

        

        if($this->_source->getPaychargeFee())
        {
            $store = $this->getStore();
            $fee = new \Magento\Framework\DataObject(
                [
                    'code' => 'paymentfee',
                    'strong' => false,
                    'value' => $this->_source->getPaychargeFee(),
                    'base_value' => $this->_source->getPaychargeBaseFee(),
                    'label' => __($this->_source->getPaychargeFeeName()),
                ]
            );

            $parent->addTotal($fee, 'paymentfee');
            $parent->addTotal($fee, 'paymentfee');
        return $this;
        }
    }

}
