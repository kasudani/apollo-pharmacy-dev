<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Emipro\Paymentservicecharge\Model\Total;
class Paymentfee extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
   /**
     * Collect grand total address amount
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     */
    protected $quoteValidator = null;
    protected $_storeManager;
    public function __construct(\Magento\Quote\Model\QuoteValidator $quoteValidator,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\Quote\Address\Total $total
        )
    {
        $this->_storeManager = $storeManager;
        $this->quoteValidator = $quoteValidator;
    }
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
      parent::collect($quote, $shippingAssignment, $total);
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $pointsSession=$objectManager->get('Magento\Catalog\Model\Session');
         $logger=$objectManager->create("\Psr\Log\LoggerInterface");
      $selectedpaymentmethod=$quote->getPayment()->getMethod();
      
      $customer_group_id=$objectManager->create('Magento\Customer\Model\Session')->getCustomerGroupId();
      
      
      $enable = $objectManager->get('Emipro\Paymentservicecharge\Helper\Data')->getConfig('checkout/general/active', true);
      
      $ua_regexp = $objectManager->get('Emipro\Paymentservicecharge\Helper\Data')->getConfig('checkout/general/active2', true);
        
      $table_data=unserialize($ua_regexp);

      $isCod = '';

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get('Apollo\Cartrule\Helper\Data');

        $$isCod = $helper->getIsCodCharge(); 


      $balance = 0; //Excellence_Fee_Model_Fee::getFee();
      $lable = '';
      if($enable == 1 && $$isCod)
      {
        foreach($table_data as $data)
        {

            if(($data['payment_method'] == $selectedpaymentmethod || $data["payment_method"]==32000) && ($customer_group_id==$data["customer_group"] || $data["customer_group"]==32000))
            {
                
                if($data['extra_charge_type'] == 'fixed')
                {
                    $balance=$data['extra_charge_value'];    
                }
                
                if($data['extra_charge_type'] == 'percentage')
                {
                    $totalamount=round($quote->getSubtotal());       
                    $balance=$totalamount*$data['extra_charge_value']/100;
                }

                $lable = $data['name'];
            }
            
        }

        $total->setTotalAmount('paymentfee', $balance);
        $total->setBaseTotalAmount('paymentfee', $balance);

        $total->setPaychargeFee($balance);
        $total->setPaychargeBaseFee($balance);
        $total->setPaychargeFeeName($lable);

        $quote->setPaychargeFee($balance);
        $quote->setPaychargeBaseFee($balance);
        $quote->setPaychargeFeeName($lable);

      
        $total->setGrandTotal($total->getGrandTotal() + $balance);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() + $balance);
        $pointsSession->setCalcpoints($balance);
        $pointsSession->setCalcLable($lable);
      }

        return $this;
    } 

    protected function clearValues(Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
        $pointsSession->setCalcpoints(0);
        $pointsSession->setCriditcustomitem(0);
        $pointsSession->setCriditcustomitempoint(0);
        $pointsSession->setCalcLable('');
    }
    
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger=$objectManager->create("\Psr\Log\LoggerInterface");
              
        $selectedpaymentmethod=$quote->getPayment()->getMethod();
        $customer_group_id=$objectManager->create('Magento\Customer\Model\Session')->getCustomerGroupId();
        $enable = $objectManager->get('Emipro\Paymentservicecharge\Helper\Data')->getConfig('checkout/general/active', true);
        
        $ua_regexp = $objectManager->get('Emipro\Paymentservicecharge\Helper\Data')->getConfig('checkout/general/active2', true);
        
        $table_data=unserialize($ua_regexp);
        
        $fee = 0; //Excellence_Fee_Model_Fee::getFee();
        
        $lable = '';
        $isCod = '';

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get('Apollo\Cartrule\Helper\Data');

        $$isCod = $helper->getIsCodCharge(); 

        
        if($enable == 1 && $$isCod)
        {
          foreach($table_data as $data)
          {
            if(($data['payment_method'] == $selectedpaymentmethod || $data["payment_method"]==32000) && ($customer_group_id==$data["customer_group"] || $data["customer_group"]==32000))
            {
                if($data['extra_charge_type'] == 'fixed')
                {
                  $fee=$data['extra_charge_value'];    
                }
                if($data['extra_charge_type'] == 'percentage')
                {
                  $totalamount=round($quote->getSubtotal());       
                  $fee=$totalamount*$data['extra_charge_value']/100;       
                }

                $lable = $data['name'];
            }
          }
        }


        return [
            'code' => 'paymentfee',
            'title' => __("$lable"),
            'value' => $fee
        ];
    }

    public function getLabel()
    {
        return __(" ");
    }
}