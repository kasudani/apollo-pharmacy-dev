<?php
namespace Emipro\Paymentservicecharge\Model\Total\Order\Creditmemo;

class Paymentfee extends \Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal
{
	protected $_code="paymentfee";
	protected $_service_charge;
	  public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }
    
	public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo) {

        parent::collect($creditmemo);

		$order=$creditmemo->getOrder();
		if($order->getPaychargeFee())
		{
			$creditmemo->setPaychargeFee($order->getPaychargeFee());
			$creditmemo->setPaychargeBaseFee($order->getPaychargeBaseFee());
			$creditmemo->setPaychargeFeeName($order->getPaychargeFeeName());

			$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $order->getPaychargeFee());
			$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $order->getPaychargeBaseFee());
		
		}
        return $this;
    }
}
