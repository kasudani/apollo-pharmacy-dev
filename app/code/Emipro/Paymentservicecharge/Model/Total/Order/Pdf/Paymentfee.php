<?php

namespace Emipro\Paymentservicecharge\Model\Total\Order\Pdf;

class Paymentfee extends \Magento\Sales\Model\Order\Pdf\Total\DefaultTotal
{

    public function getTotalsForDisplay()
    {
		parent::getTotalsForDisplay();
	
	    $amount = $this->getSource()->getPaychargeFee();
        $charge=$this->getOrder()->formatPriceTxt($amount);

        $label=__($this->getSource()->getPaychargeFeeName());
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        $total = ['amount' => $charge, 'label' => $label.":", 'font_size' => $fontSize];
        return [$total];
	
    }
}
