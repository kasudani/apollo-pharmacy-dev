<?php
namespace Tagalys\Search\Block;
 
class Trecommended extends \Magento\Framework\View\Element\Template
{
   public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Tagalys\Sync\Helper\Configuration $tagalysConfiguration
    )
    {
        $this->tagalysConfiguration = $tagalysConfiguration;
        $this->storeManager = $context->getStoreManager();
        parent::__construct($context);
    }

    public function isTagalysEnabled() {
        return $this->tagalysConfiguration->isTagalysEnabledForStore($this->getCurrentStoreId());
    }

    public function getCurrentStoreId() {
        return $this->storeManager->getStore()->getId();
    }
}