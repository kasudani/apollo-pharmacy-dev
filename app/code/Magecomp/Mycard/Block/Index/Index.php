<?php
namespace Magecomp\Mycard\Block\Index;

use Magento\Framework\View\Element\Template\Context;
use Magecomp\Mycard\Model\Data;
use Magento\Framework\View\Element\Template;


class Index extends Template
{
        
	public function __construct(Context $context, Data $model)
	{
        $this->model = $model;
		parent::__construct($context);
                
	}
        
	public function sayHello()
	{
		return __('Hello World');
	}
        public function getHelloCollection()
        {
            $helloCollection = $this->model->getCollection();
            return $helloCollection;
        }
}
?>