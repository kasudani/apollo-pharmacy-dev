<?php

namespace Magecomp\Mycard\Block\Rewrite\Customer\Account;

class Customer extends \Magento\Framework\View\Element\Template
{
protected $_customerSession;
 
public function __construct(
    \Magento\Customer\Model\SessionFactory $customerSession
){
    $this->_customerSession = $customerSession->create();
}
 
public function getLoggedinCustomerId() {
    if ($this->_customerSession->isLoggedIn()) {
        return $this->_customerSession->getId();
    }
}
}
?>