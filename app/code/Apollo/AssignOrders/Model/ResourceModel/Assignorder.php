<?php

namespace Apollo\AssignOrders\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Assignorder extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('admin_user', 'user_id'); //hello is table of module
    }
}