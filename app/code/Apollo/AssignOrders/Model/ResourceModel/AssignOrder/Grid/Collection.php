<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\AssignOrders\Model\ResourceModel\AssignOrder\Grid;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;

/**
 * Order grid collection
 */
class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    /**
     * Initialize dependencies.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'sales_order_grid',
        $resourceModel = '\Magento\Sales\Model\ResourceModel\Order'
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }
    public function _beforeLoad()
    {
        //echo "string12345";exit();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $adminSession = $objectManager->get('Magento\Backend\Model\Auth\Session');

        if($adminSession->isLoggedIn())
        {
            $admin = $adminSession->getUser()->getRole()->getData();
            $role_id = $admin['role_id'];    
        }    
        
        if($role_id == "1" || $role_id == "237" || $role_id == "13"){
            return parent::_beforeLoad();
        }
        else{
            $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
            $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();
                foreach($orderDatamodel as $orderDatamodel1){
                $adminSession = $objectManager->get('Magento\Backend\Model\Auth\Session');
                $admin = $adminSession->getUser();
                $admin_user = $admin->getUsername();
                //echo $admin_user;exit();
                $user = $orderDatamodel1->getUser();
                //echo $user;
                if($admin->getUsername() == $user){
                $this->addFieldToFilter('user',$user);
                return parent::_beforeLoad();
                }
            }
        }
        $this->addFieldToFilter('user','');
        return parent::_beforeLoad();
        
    }

}
