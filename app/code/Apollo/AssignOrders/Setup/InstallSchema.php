<?php

namespace Apollo\AssignOrders\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'user',
        [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'length' => 255,
            'nullable' => true,
            'comment' => 'User'
        ]
    );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'user', 
        [
           'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 
           'length' => 255, 
           'nullable' => true, 
           'comment' => 'User' 
       ]
   );
        $setup->endSetup();
    }
}