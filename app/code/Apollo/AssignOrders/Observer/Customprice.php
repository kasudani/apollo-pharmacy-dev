<?php
/**
 * Webkul Hello CustomPrice Observer
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 */
namespace Apollo\AssignOrders\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class Customprice implements ObserverInterface
{
    protected $_checkoutSession;
    protected $_productRepository;
    protected $_cart;
    const PAM0185_FREE = 127;//PAM0185
    const APD0056_FREE = 5575;//APM0031
    const HEA0265_FREE = 5507;//APT0020



    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Cart $cart
        //\Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
        //$this->_checkoutSession = $checkoutSession;

    }
    public function execute(\Magento\Framework\Event\Observer $observer) {
        date_default_timezone_set('Asia/Kolkata');
        $startDate = "2017-12-31 00:00:00";
        $endDate = "2018-01-31 23:59:59";
        $currentTime = time();
        if($currentTime >= strtotime($startDate) && $currentTime <= strtotime($endDate)){
            $item = $observer->getEvent()->getData('quote_item');
            //echo "<pre>";print_R($item->getData());die;
            $itemSku = $item->getSku();
            $itemQty = $item->getQty();
            //echo $itemSku."--".$itemQty;die;
            $freeProductId = "";
            if($itemSku == "PAM0185"){
                if($itemQty%2 == 1){
                    //$freeProductId = self::PAM0185_FREE;
                }
            }else if($itemSku == "APD0056"){
                $freeProductId = self::APD0056_FREE;
            }else if($itemSku == "HEA0265" && $itemQty >= 2001){
                $freeProductId = self::HEA0265_FREE;
            }
            if($freeProductId != ""){
                $params = array(
                    'product' => $freeProductId,
                    'qty' => 1,
                    //'price' => 1
                );
                $_product = $this->_productRepository->getById($freeProductId);
                $this->_cart->addProduct($_product,$params);
                $this->_cart->save();
            }
        }
        /*echo "Current ".date('Y-m-d H:i:s',$currentTime)."--".$currentTime."--<br>";
        echo "Start ".$startDate."--".strtotime($startDate)."--<br>";
        echo "End ".$endDate."--".strtotime($endDate)."--<br>";die;*/


        return true;

        //echo "<pre>";print_R($_REQUEST);die;
        /*$item = $observer->getEvent()->getData('quote_item');
        $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
        $price = 100; //set your price here
        $item->setCustomPrice($price);
        $item->setOriginalCustomPrice($price);
        $item->getProduct()->setIsSuperMode(true);
        $item->save();*/
    }
}