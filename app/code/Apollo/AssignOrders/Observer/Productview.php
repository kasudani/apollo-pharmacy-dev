<?php

namespace Apollo\AssignOrders\Observer;

class Productview implements \Magento\Framework\Event\ObserverInterface
{
    protected $_productRepository;
    protected $catalogSession;

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\Session $catalogSession
    ) {
        $this->_productRepository = $productRepository;
        $this->catalogSession = $catalogSession;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try{
            if(isset($_REQUEST['cod_pincode'])){
                $this->catalogSession->setCustomerPincode($_REQUEST['cod_pincode']);
            }
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

}