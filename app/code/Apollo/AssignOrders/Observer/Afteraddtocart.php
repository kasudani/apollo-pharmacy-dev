<?php

namespace Apollo\AssignOrders\Observer;

class Afteraddtocart implements \Magento\Framework\Event\ObserverInterface
{
    protected $_checkoutSession;
    protected $_productRepository;
    protected $_cart;
    const THERMAMETER_PRODUCT_ID = 5507;
    const PHARMA_PRODUCTS_CART_CHECK_LIMIT = 500;


    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try{
            $checkoutSession = $this->_checkoutSession->getData();
            $quoteId = isset($checkoutSession['quote_id_1']) ? $checkoutSession['quote_id_1'] : "";

            if(!empty($quoteId)){
                $this->_cart->save();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $magentoQuote = $objectManager->create('\Magento\Quote\Model\Quote')->load($quoteId);
                $quoteItems = $magentoQuote->getAllVisibleItems();
                $thermaMeterProdId = self::THERMAMETER_PRODUCT_ID; $thermaMeterExists = false; $thermaMeterCartItemId = 0;
                $pharmaItemsPrice = 0;
                foreach ($quoteItems as $quoteItem){
                    $quoteItemData = $quoteItem->getData();
                    $productId = $quoteItemData['product_id'];
                    if($thermaMeterProdId == $productId){
                        //if($quoteItemData['custom_price'] == "0.0000"){
                        $thermaMeterExists = true;
                        $thermaMeterCartItemId = $quoteItemData['item_id'];
                        //}
                    }
                    $productType = $objectManager->create('\Apollo\AssignOrders\Observer\Updateorderinfo')->getOrderTypes($productId,$objectManager);
                    if($productType != "Fmcg"){
                        $pharmaItemsPrice = $pharmaItemsPrice + $quoteItemData['base_row_total'];
                    }
                }
            }
            //$pharmaItemsPrice = 600;
            //echo $pharmaItemsPrice;die;
            if($pharmaItemsPrice >= self::PHARMA_PRODUCTS_CART_CHECK_LIMIT){
                if($thermaMeterExists == false){
                    $params = array(
                        'product' => $thermaMeterProdId,
                        'qty' => 1,
                        'price' => 1
                    );
                    $_product = $this->_productRepository->getById($thermaMeterProdId);
                    $this->_cart->addProduct($_product,$params);
                    $this->_cart->save();

                    $magentoQuote = $this->_cart->getQuote();//$objectManager->create('\Magento\Quote\Model\Quote')->load($quoteId);
                    $quoteItems = $magentoQuote->getAllVisibleItems();
                    foreach ($quoteItems as $quoteItem){
                        $quoteItemData = $quoteItem->getData();
                        $productId = $quoteItemData['product_id'];
                        if($thermaMeterProdId == $productId){
                            $quoteItem->setCustomPrice("0");
                            $quoteItem->setOriginalCustomPrice("0");
                            $quoteItem->save();
                        }
                    }
                }else{
                    $magentoQuote = $this->_cart->getQuote();
                    $quoteItems = $magentoQuote->getAllVisibleItems();
                    foreach ($quoteItems as $quoteItem){
                        $quoteItemData = $quoteItem->getData();
                        $productId = $quoteItemData['product_id'];
                        if($thermaMeterProdId == $productId){
                            $quoteItem->setQty("1");
                            $quoteItem->setCustomPrice("0");
                            $quoteItem->setOriginalCustomPrice("0");
                            $quoteItem->setRowTotal("0");
                            $quoteItem->setBaseRowTotal("0");
                            $quoteItem->save();
                        }
                    }
                }
            }else{
                //echo $thermaMeterExists.$thermaMeterCartItemId;
                if($thermaMeterCartItemId != 0){
                    //echo "deleting".$thermaMeterCartItemId;die;
                    $this->_cart->removeItem($thermaMeterCartItemId);
                    $this->_cart->save();
                }
            }
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

}