<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\AssignOrders\Controller\Adminhtml\Order;
//use Magento\Framework\Model\Resource\Db\Collection\AbstractCollection;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Config\ConfigOptionsListConstants;
class MassAssign extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
  public $_resource;
  private $deploymentConfig;
  public function __construct(Context $context,
  ResourceConnection $resource,
  Filter $filter, CollectionFactory $collectionFactory,DeploymentConfig $deploymentConfig)
    {
        
    $this->_resource = $resource;
    parent::__construct($context , $filter);
    $this->deploymentConfig = $deploymentConfig;
    $this->collectionFactory = $collectionFactory;
 
  }
    /**
     * Cancel selected orders
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
      //echo "<pre>"
      //print_r($collection->getData());exit();
      //$config = $this->getData('config');
      //echo "<pre>";
      //print_r($config);exit();
       //$status = $this->getRequest()->getParam('selected');
       //$filters = (array)$this->getRequest()->getParam('selected', []);
       //$text = $this->getValue($status);
       //echo "<pre>";
       //print_r($status);exit();
       //$optionText = $status->getSource()->getOptionText($optionId);
      $rateChangeStatus= 0;
       foreach ($collection as $rate) {
        // echo "<pre>";
        // print_r($rate->getData());
            $rate->setUser($this->getRequest()->getParam('status'))->save();
            $rateChangeStatus++;
       }
       //exit();
      //$status = (int) $this->getRequest()->getParam('status');

      //$data = $status->getValue($status);
      //$data = $this->getRequest()->getParam('status');
       //print_r($status);
       //exit;
        $this->messageManager->addSuccess(
                __('order(s) successfully Assigned')
            );
        $this->_redirect('sales/order/index');
    }
}
