<?php

namespace Apollo\Contactus\Model\ResourceModel;

class Contact extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    
    protected function _construct()
    {
        $this->_init('apollo_contactus', 'contact_id');
    }
}
