<?php
namespace Apollo\Contactus\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), "1.2.0", "<")) {
        //Your upgrade script
        
            $tableName = $installer->getTable('apollo_contactus');
            $installer->getConnection()->addColumn($tableName, 'email_cc', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_VARCHAR,
                'length'    => 200,
                'unsigned' => false,
                'nullable' => true,
                'comment' => 'Email Cc'
            ]);

            $tableName = $installer->getTable('apollo_contactus');
            $installer->getConnection()->addColumn($tableName, 'email_bcc', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_VARCHAR,
                'length'    => 200,
                'unsigned' => false,
                'nullable' => true,
                'comment' => 'Email BCc'
            ]);

        }
        }
        $installer->endSetup();
    }
}