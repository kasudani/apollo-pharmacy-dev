<?php
namespace Apollo\Healthareas\Api\Data;

interface HealthareaInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */

    const HEALTHAREA_ID = 'healtharea_id';
    const TITLE         = 'title';
    const URL           = 'url';
    const IMAGE         = 'image';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const DISPLAY_ORDER = 'display_order';
    const IS_ACTIVE     = 'is_active';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get url
     *
     * @return string|null
     */
    public function getUrl();

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Get display order
     *
     * @return bool|null
     */
    public function getDisplayOrder();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setId($id);

    /**
     * Set title
     *
     * @param string $title
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setTitle($title);

    /**
     * Set url
     *
     * @param string $url
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setUrl($url);

    /**
     * Set image
     *
     * @param string $image
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setImage($image);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setIsActive($isActive);

    /**
     * Set is display order
     *
     * @param int|bool $displayOrder
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setDisplayOrder($displayOrder);
}