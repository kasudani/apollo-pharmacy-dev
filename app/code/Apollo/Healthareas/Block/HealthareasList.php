<?php
namespace Apollo\Healthareas\Block;

use Apollo\Healthareas\Api\Data\HealthareaInterface;
use Apollo\Healthareas\Model\ResourceModel\Healtharea\Collection as HealthareaCollection;

class HealthareasList extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * @var \Apollo\Healthareas\Model\ResourceModel\Healtharea\CollectionFactory
     */
    protected $_healthareaCollectionFactory;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Apollo\Healthareas\Model\ResourceModel\Healtharea\CollectionFactory $healthareaCollectionFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Apollo\Healthareas\Model\ResourceModel\Healtharea\CollectionFactory $healthareaCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_healthareaCollectionFactory = $healthareaCollectionFactory;
    }

    /**
     * @return \Apollo\Healthareas\Model\ResourceModel\Healtharea\Collection
     */
    public function getHealthareas()
    {
        if (!$this->hasData('healthareas')) {
            $healthareas = $this->_healthareaCollectionFactory
                ->create()
                ->addFilter('is_active', 1)
                ->addOrder(
                    HealthareaInterface::DISPLAY_ORDER,
                    HealthareaCollection::SORT_ORDER_ASC
                );
            $this->setData('healthareas', $healthareas);
        }
        return $this->getData('healthareas');
    }
    
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        /*parent::_prepareLayout();
        if ($this->getHealthareas()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'healthareas.list.pager'
            )->setCollection(
                $this->getHealthareas()
            );
            $this->setChild('pager', $pager);
            $this->getHealthareas()->load();
        }
        return $this;*/
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    
    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Apollo\Healthareas\Model\Healtharea::CACHE_TAG . '_' . 'list'];
    }

    /**
     * get image url by image file name
     * @param $image
     * @return string
     */
    public function getImageUrl($image)
    {
        return $this->getMediaBaseUrl(). 'healtharea/images' . $image;
    }

    /**
     * get media url
     * @return mixed
     */
    public function getMediaBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

}