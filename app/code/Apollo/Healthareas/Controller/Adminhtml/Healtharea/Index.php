<?php

namespace Apollo\Healthareas\Controller\Adminhtml\Healtharea;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Apollo_Healthareas::healtharea');
        $resultPage->addBreadcrumb(__('Health Areas'), __('Health Areas'));
        $resultPage->addBreadcrumb(__('Manage Health Areas'), __('Manage Health Areas'));
        $resultPage->getConfig()->getTitle()->prepend(__('Health Areas'));

        return $resultPage;
    }

    /**
     * Is the user allowed to view the health area grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Apollo_Healthareas::healthareas');
    }


}