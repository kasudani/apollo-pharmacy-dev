<?php
namespace Apollo\Healthareas\Controller\Adminhtml\Healtharea;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Delete extends \Magento\Backend\App\Action
{

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Apollo_Healthareas::delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('healtharea_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Apollo\Healthareas\Model\Healtharea');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The health area has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['healtharea_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a health area to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}