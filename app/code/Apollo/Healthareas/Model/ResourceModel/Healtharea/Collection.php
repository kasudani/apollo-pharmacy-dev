<?php
namespace Apollo\Healthareas\Model\ResourceModel\Healtharea;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'healtharea_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Apollo\Healthareas\Model\Healtharea', 'Apollo\Healthareas\Model\ResourceModel\Healtharea');
    }
}