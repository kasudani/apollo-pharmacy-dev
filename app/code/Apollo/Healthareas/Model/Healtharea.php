<?php
namespace Apollo\Healthareas\Model;

use Apollo\Healthareas\Api\Data\HealthareaInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Healtharea  extends \Magento\Framework\Model\AbstractModel implements HealthareaInterface, IdentityInterface
{

    /**#@+
     * Healtharea's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'healtharea';

    /**
     * @var string
     */
    protected $_cacheTag = 'healtharea';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'healtharea';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Apollo\Healthareas\Model\ResourceModel\Healtharea');
    }

    /**
     * Prepare healtharea's statuses.
     * Available event healtharea_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::HEALTHAREA_ID);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get url
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->getData(self::URL);
    }

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Get Display order
     *
     * @return int|null
     */
    public function getDisplayOrder()
    {
        return $this->getData(self::DISPLAY_ORDER);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setId($id)
    {
        return $this->setData(self::HEALTHAREA_ID, $id);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set url
     *
     * @param string $url
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setUrl($url)
    {
        return $this->setData(self::URL, $url);
    }

    /**
     * Set image
     *
     * @param string $image
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Set creation time
     *
     * @param string $creation_time
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }

    /**
     * Set Display order
     *
     * @param int $displayOrder
     * @return \Apollo\Healthareas\Api\Data\HealthareaInterface
     */
    public function setDisplayOrder($displayOrder)
    {
        return $this->setData(self::HEALTHAREA_ID, $displayOrder);
    }
}
