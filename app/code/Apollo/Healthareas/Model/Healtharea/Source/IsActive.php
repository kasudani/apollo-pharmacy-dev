<?php
namespace Apollo\Healthareas\Model\Healtharea\Source;

class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Apollo\Healthareas\Model\Healtharea
     */
    protected $healtharea;

    /**
     * Constructor
     *
     * @param \Apollo\Healthareas\Model\Healtharea $healtharea
     */
    public function __construct(\Apollo\Healthareas\Model\Healtharea $healtharea)
    {
        $this->healtharea = $healtharea;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->healtharea->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}