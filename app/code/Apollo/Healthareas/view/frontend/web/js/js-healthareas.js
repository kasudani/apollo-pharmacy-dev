require([
  'jquery'
], function ($) {
  "use strict";

  $(document).ready(function () {
    $('.shop-health-slider').owlCarousel({
        loop:false,
        margin:10,
        responsiveClass:true,
        stagePadding: 20,
        dots:false,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:2
            },
            1000:{
                items:2
            }
        }
    });
  });
});
