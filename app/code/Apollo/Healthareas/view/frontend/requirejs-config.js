var config = {
	map: {
		'*': {
			'apollo/flexslider': 'Apollo_Healthareas/js/jquery-flexslider-min',
		},
	},
	paths: {
		'apollo/flexslider': 'Apollo_Healthareas/js/jquery-flexslider-min',
	},
	shim: {
		'apollo/flexslider': {
			deps: ['jquery']
		},
	}
};
