<?php
/**
 * Before placing order
 */
namespace Apollo\CrmOrders\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;
 
class BeforePlaceOrder implements ObserverInterface
{
    /** @var CheckoutSession */
    protected $checkoutSession;

    /** \Apollo\CrmOrders\Model\CrmOrdersLog */
	protected $_crmOrdersLogModel;

	/** \Apollo\CrmOrders\Model\ResourceModel\CrmOrdersTrack\Collection */
	protected $_crmOrdersLogCollection;

    protected $_orderInterface;

    public function __construct(
        CheckoutSession $checkoutSession,
        \Apollo\CrmOrders\Model\CrmOrdersLogFactory $crmOrdersLogFactory,
		\Apollo\CrmOrders\Model\ResourceModel\CrmOrdersLog\CollectionFactory $crmOrdersLogCollection,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface
    ) 
    {
        $this->checkoutSession = $checkoutSession;
        $this->_crmOrdersLogModel = $crmOrdersLogFactory;
		$this->_crmOrdersLogCollection = $crmOrdersLogCollection;
        $this->_orderInterface = $orderInterface;
    } 
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /**
         *   Get Order 
         *   ---------------     
         *   Methods to use
         *
         *   gettype($order)
         *   get_class($order)
         *   getRealOrderId()
         *   getQuoteId()
         *   loadByIncrementId()
         *   getData()
         *   getState()
         *   getStatus()
         */    
        $order = $observer->getEvent()->getOrder();
        /**
         *  Load CRM LOG Table data based on current quote
         */
        $crmLogRow  = $this->_crmOrdersLogModel->create()->load($order->getQuoteId(), 'quote_id');

        /**
         *  If Quote id is not present
         */
        if (!empty($crmLogRow->getData())) {
            return $this;
        }
                
        try{
            if (!empty($crmLogRow->getData())) {
                
                $crmLogRow->setOrderNumber($order->getRealOrderId())->save();
            }
        }
        catch(\Exception $e) { }
    }
} 