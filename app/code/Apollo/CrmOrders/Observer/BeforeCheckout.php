<?php
/**
 * Before going to checkout page
 */
namespace Apollo\CrmOrders\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;
 
class BeforeCheckout implements ObserverInterface
{
    /** @var CheckoutSession */
    protected $checkoutSession;

    /** \Apollo\CrmOrders\Model\CrmOrdersLog */
	protected $_crmOrdersLogModel;

	/** \Apollo\CrmOrders\Model\ResourceModel\CrmOrdersTrack\Collection */
	protected $_crmOrdersLogCollection;

    protected $_quote_id = '';

    public function __construct(
        CheckoutSession $checkoutSession,
        \Apollo\CrmOrders\Model\CrmOrdersLogFactory $crmOrdersLogFactory,
		\Apollo\CrmOrders\Model\ResourceModel\CrmOrdersLog\CollectionFactory $crmOrdersLogCollection
    ) 
    {
        $this->checkoutSession = $checkoutSession;
        $this->_crmOrdersLogModel = $crmOrdersLogFactory->create();
		$this->_crmOrdersLogCollection = $crmOrdersLogCollection;
    } 
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        /** @var \Magento\Quote\Model\Quote  */
        $quote = $this->checkoutSession->getQuote();
        $this->_quote_id = $quote->getId();

        /**
         *  If Quote id is null or empty
         */
        if (empty($this->_quote_id) || $this->_quote_id < 1) {
            # code...
            return $this;
        }

        /**
         *  Load CRM LOG Table data based on current quote
         */       

        $crmLogCollection  = $this->_crmOrdersLogModel->getCollection()
        ->addFieldToFilter('quote_id', $this->_quote_id)
        ->addFieldToFilter('order_id', '');

        if ($crmLogCollection->count() < 1) {
            # code...
            $this->createNewRecord();
        }
        
    }

    public function createNewRecord()
    {
        try{
            $this->_crmOrdersLogModel->setData(
                array(
                    'quote_id' => $this->_quote_id,
                    'order_id' => '',
                    'order_number' => '',
                    'payment_status' => '',
                    'crm_update_status' => '',
                    'push_count' => '0',
                    'email_sent' => ''
                )
            )->save();
        }
        catch(\Exception $e) {
            // print($e->getMessage());
        }
    }
} 