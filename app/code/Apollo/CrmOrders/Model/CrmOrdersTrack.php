<?php

namespace Apollo\CrmOrders\Model;

use \Magento\Framework\Model\AbstractModel;


class CrmOrdersTrack extends AbstractModel
{
    /**
     * Cache tag
     */
    const CACHE_TAG ='crmorders_track';
    
    protected function _construct()
    {
        $this->_init('Apollo\CrmOrders\Model\ResourceModel\CrmOrdersTrack');
    }
 
}