<?php
/**
 *  CRM Orders Log model
 */
namespace Apollo\CrmOrders\Model;

use \Magento\Framework\Model\AbstractModel;


class CrmOrdersLog extends AbstractModel
{
    /**
     * Cache tag
     */
    const CACHE_TAG ='crm_orders_log';
    
    protected function _construct()
    {
        $this->_init('Apollo\CrmOrders\Model\ResourceModel\CrmOrdersLog');
    }
 
}