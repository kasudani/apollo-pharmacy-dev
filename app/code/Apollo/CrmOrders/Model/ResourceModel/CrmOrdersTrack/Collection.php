<?php

namespace Apollo\CrmOrders\Model\ResourceModel\CrmOrdersTrack;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;


class Collection extends AbstractCollection {

    protected function _construct() {
    	
        $this->_init('Apollo\CrmOrders\Model\CrmOrdersTrack','Apollo\CrmOrders\Model\ResourceModel\CrmOrdersTrack');
    }
}