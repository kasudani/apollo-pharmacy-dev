<?php

namespace Apollo\CrmOrders\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CrmOrdersTrack extends AbstractDb {

    protected function _construct() {

        $this->_init('crmorders_track', 'slno');

    }
}