<?php

namespace Apollo\CrmOrders\Model\ResourceModel\CrmOrdersLog;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;


class Collection extends AbstractCollection {

    protected function _construct() {
    	
        $this->_init('Apollo\CrmOrders\Model\CrmOrdersLog','Apollo\CrmOrders\Model\ResourceModel\CrmOrdersLog');
    }
}