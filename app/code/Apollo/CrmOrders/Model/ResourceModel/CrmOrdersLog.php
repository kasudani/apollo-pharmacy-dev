<?php

namespace Apollo\CrmOrders\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CrmOrdersLog extends AbstractDb {

    protected function _construct() {

        $this->_init('crm_orders_log', 'id');

    }
}