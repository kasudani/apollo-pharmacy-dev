<?php

namespace Apollo\CrmOrders\Block;

use Magento\Sales\Model\Order;

class Failure  extends \Magento\Framework\View\Element\Template
{

    protected $orderRepository;
    protected $orderInterface;
    protected $_product;
    protected $_categoryFactory;
    protected $_checkoutSession;
    protected $x;

    protected $_crmOrdersLogModel;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,        
        \Magento\Catalog\Model\Product $product,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface,
        \Apollo\CrmOrders\Model\CrmOrdersLogFactory $crmOrdersLogFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->_checkoutSession = $checkoutSession;
        $this->_crmOrdersLogModel = $crmOrdersLogFactory->create();
        $this->orderRepository = $orderRepository;
        $this->orderInterface = $orderInterface;
        $this->_product = $product;
        $this->_categoryFactory = $categoryFactory;
    }

    public function getSomething()
    {

        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/failureOrder.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__CLASS__);
        $logger->info(__FUNCTION__);

        $order = $this->_checkoutSession->getLastOrderId();
            
        /* load order by order number */
        $orderObj = $this->orderRepository->get($order);

        $logger->info("Order Id => " . $orderObj->getId());
        $logger->info("Order number => " . $orderObj->getRealOrderId());

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();

        // echo "|".$order."|";

            
        $https = ((!empty($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] != 'off')) ? true : false;

        if($https) {
            $uurl = "https://";
        } else {
            $uurl = "http://";
        }
    
         $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
             $connection = $this->_resources->getConnection();
             $themeTable = $this->_resources->getTableName('sales_order_grid');

        $urlp=$_SERVER['HTTP_HOST'];

        $userData = array("username" => "apolloadm", "password" => "VarshiniG9");

        $ch = curl_init($base_url."index.php/rest/V1/integration/admin/token");

        //echo "|".$base_url."|".$uurl."|".$urlp."|";

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));
        $token = curl_exec($ch);   

        $logger->info("token => " . $token);

           $ch2 = curl_init($base_url."index.php/rest/V1/orders/".$order);  // to display particluar order
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $resultt = curl_exec($ch2);
        curl_close($ch2);
        $resultt = json_decode($resultt, 1);

        $logger->info("result ");
        $logger->info($resultt);

        

        $sqlord = "select order_types,delivery_info from sales_order_grid where entity_id='".$order."'";        
        $roword=$connection->fetchrow($sqlord);
        $rowcount=count($roword);

        $orderType = $roword['order_types'];
        $deliveryInfo = $roword['delivery_info'];
        /**
         *  For missing orders order type and delevery info might not be present 
         *  Check for the order type and delivery info
         */
        if ($rowcount <= 0) {

            /* test code here */
            $skus = array(); $orderTypes = array();
            $orderedItems = $orderObj->getAllVisibleItems();
            foreach ($orderedItems as $orderedItem){
                
                $skus[] = $orderedItem->getSku();
                $productId = $orderedItem->getProductId();
                // $orderTypes[] = $this->getOrderTypes($productId);

                $productData = $this->_product->load($productId);
                // $categoryFactory = $this->_categoryFactory->create();

                $category_id_ary = $productData->getCategoryIds();          
                $category_names_array = array();

                if(count($category_id_ary)){
                    foreach($category_id_ary as $category_id){
                        $_category = $this->_categoryFactory->create()->load($category_id);
                        $category_names_array[] = $_category->getName();
                    }
                }

                $pharma = false;
                $fmcg = false;
                $order_type = "";
                $category_names = array_unique($category_names_array);

                if(in_array("PHARMA",$category_names))    {
                    $pharma = true; $order_type = "Pharma";
                }
                if(in_array("FMCG",$category_names)) {
                    $fmcg = true; $order_type = "Fmcg";
                }

                if($pharma == true && $fmcg == true){
                    $order_type = "Both";
                }

                /* assing product type for each item */
                $orderTypes[] = (!empty($order_type) ? $order_type : "Fmcg");
            }

            $orderTypes = array_unique($orderTypes);
            
            if(count($orderTypes) > 1){
                $orderType = "Both";
            }else if(isset($orderTypes[0])){
                $orderType = $orderTypes[0];
            }else{
                $orderType = "Pharma";
            }

            /* final order type and delivery info */
            $orderType = $orderType;
            $deliveryInfo = '';
        }
        
        $presimg="";
        $x="";
        $y="";
        $z="";
        $i=0;
        $j=0;
        
        if($rowcount>=1 && $deliveryInfo != "")
        {
            
                $bb=explode('"previous_prescriptions";',$deliveryInfo);

             $regex = '/".*?"/';
             $match=array();
            
            
             preg_match_all($regex, $bb[1], $match);

            
            if(count($match[0])>=1)
            {
               for($i=0;$i<count($match[0]);$i++)
               {
                  $sqlimg = 'select uploaded_file from ordermedicine_data where id='.$match[0][$i];
                   $rowimg=$connection->fetchrow($sqlimg);

                $presimg .=$base_url."pub/media/medicine_prescription/".$rowimg['uploaded_file'].",";
               }
            }
          
        
        }
        else
        {
            $presimg="";
        }
        $i=0;

        foreach($resultt['items'] as $itms)
        {
            $dt=explode(" ",$itms['created_at']);
            $dtf=date_create($dt[0]);
            $date=date_format($dtf,"Y-M-d");

               $x[$i] =array("ItemID"=>$itms['sku'],"ItemName"=>$itms['name'],"Qty"=>$itms['qty_ordered'],"orddt"=>$date,"Price"=>$itms['base_original_price']);
            $i++;
        }  


        //$z=array("Totalamount"=>$resultt['payment']['amount_ordered'],"Paymentsource"=>$resultt['payment']['method']);
        
        if($resultt['payment']['method']=="ccavenue")
            $z =array(
                "Totalamount"=>'',
                "Paymentsource"=>$resultt['payment']['method'],
                "Paymentstatus"=>'',
                "Paymentorderid"=>''
            );
        else if($resultt['payment']['method']=="paytm")
            $z =array(
                "Totalamount"=>$resultt['payment']['amount_ordered'],
                "Paymentsource"=>$resultt['payment']['method'],
                "Paymentstatus"=>"",
                "Paymentorderid"=>""
            );
        else if($resultt['payment']['method']=="healingcard")
        {
            

            if(strpos($resultt['status_histories'][0]['comment'],'|')!==false)
            {
                $hcard=explode("|",$resultt['status_histories'][0]['comment']);

                            
                if($hcard[3]!="")
                    $hcardtid=$hcard[3];
                else
                    $hcardtid="";

                if($hcard[0]!="" && $hcard[3]!="")
                    $hcardsts=substr($hcard[0],strlen($hcard[0])-8);
                
            }
            else
            {

                $hcardsts=$resultt['status_histories'][0]['comment'];
                $hcardtid="";
            }
            
            
            $z =array("Totalamount"=>$resultt['payment']['amount_ordered'],"Paymentsource"=>$resultt['payment']['method'],"Paymentstatus"=>$hcardsts,"Paymentorderid"=>$hcardtid); 
        } 
        else
            $z =array("Totalamount"=>$resultt['payment']['amount_ordered'],"Paymentsource"=>$resultt['payment']['method'],"Paymentstatus"=>'',"Paymentorderid"=>'');

  
  
        foreach($resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['street'] as $addr)
        {
               $y.=$addr." ";
        }

        /**
         * if customer info is available
         */
        $state = '';
        if (isset($resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['region'])) {
            # code...
            $state = $resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['region'];
        }

        $cust=array(
        "Mobileno"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['telephone'],
        "Comm_addr"=>$y,
        "Del_addr"=>$y,
        "FirstName"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'],
        "LastName"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['lastname'],
        "City"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['city'],
        "State"=>$state,  
        "Postcode"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['postcode'],
        "Mailid"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['email'],
        "Age"=>"30",    
        "Cardno"=>"00",
        "PatientName"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'].$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['lastname']
        );

        $tpdetails=array(
        "Orderid"=>$resultt['increment_id'],
        "Shopid"=>"16001",
        "ShippingMethod"=>$resultt['shipping_description'],
        "PaymentMethod"=>$resultt['payment']['method'], 
        "Vendorname"=>"online",
        "DotorName"=>"Apollo",
        "url"=>$presimg,
        "Ordertype"=>$orderType,
        "Customerdetails"=>$cust,
        "Paymentdetails"=>$z,
        "itemdetails"=>$x
        );

        $arr=array("tpdetails"=>$tpdetails); 

        // $curl=curl_init("http://220.225.226.198:51/Onlineorder.svc/PLACE_ORDERS");
        $curl=curl_init("http://124.124.88.106:81/Onlineorder.svc/PLACE_ORDERS");
        // $curl=curl_init("http://online.apollopharmacy.org:51/Onlineorder.svc/PLACE_ORDERS");


        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($arr));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json", 
            "Content-Lenght: " . strlen(json_encode($arr))
        ));

        $tokenn = curl_exec($curl);

        $res=json_decode($tokenn, TRUE);

        $logger->info("CRM Order response => ");
        $logger->info($res);

        /**
         *  Set Flag if Crm response is success / failure
         */
        $crmResponseStatus = '';

        if($res['ordersResult']['Status']=="Success" || $res['ordersResult']['Status']=="success")
        {
            $ins="insert into crmorders_track (orderid,orderdt,crmstatus,entityid) values('".$resultt['increment_id']."','".date('Y-m-d')."','0','".$order."')";
            $connection->query($ins);

            $crmResponseStatus = 'success'; // response success

        }
        else
        {
            $ins="insert into crmorders_track (orderid,orderdt,crmstatus,entityid) values('".$resultt['increment_id']."','".date('Y-m-d')."','1','".$order."')";
            $connection->query($ins);

            $crmResponseStatus = 'failure'; // response failure
        }

        $logger->info("CRM Order response status=> " . $crmResponseStatus);
        

        /**
         *  Update CRM Log table
         */
        try {

            $logger->info("inside crm log update code ... ");
            /**
             *  Load CRM LOG Table data based on current quote
             */
            // $logTableRow  = $this->_crmOrdersLogModel->load($orderObj->getQuoteId(), 'quote_id');

            $logTableCollection  = $this->_crmOrdersLogModel->getCollection()
            ->addFieldToFilter("quote_id", $orderObj->getQuoteId())
            ->addFieldToFilter("order_id", "");

            $logTableData = $logTableCollection->getData();

            /** if data exists based on current quote id */
            if (isset($logTableData[0])) {
               
                if(empty($logTableData[0]['order_id'])){
                    /** load that row again with id */
                    $logTableRow  = $this->_crmOrdersLogModel->load($logTableData[0]['id']);

                    /** udpdate the current row in log table */ 
                    $logTableRow->setOrderId($order);
                    $logTableRow->setOrderNumber($orderObj->getRealOrderId());
                    $logTableRow->setPaymentStatus('failure');
                    $logTableRow->setCrmUpdateStatus($crmResponseStatus);
                    $logTableRow->setPushCount($logTableRow->getPushCount() + 1);
                    $logTableRow->save();

                    $logger->info("log table updated => ");
                    $logger->info($logTableRow->getData());
                }
            }

            $logger->info("log table row data => ");
            $logger->info($logTableRow->getData());


            // $logTableRow = $this->_crmOrdersLogModel->load($resultt['increment_id'],'order_number');

            // if row exists than update
            /*if (!empty($logTableRow->getData())) {

                $logTableRow->setOrderId($order);
                $logTableRow->setOrderNumber($orderObj->getRealOrderId());
                $logTableRow->setPaymentStatus('failure');
                $logTableRow->setCrmUpdateStatus($crmResponseStatus);
                $logTableRow->setPushCount($logTableRow->getPushCount() + 1);
                $logTableRow->save();

                $logger->info("log table updated => ");
                $logger->info($logTableRow->getData());
            }*/
                
        } catch (Exception $e) {
                $logger->info("log table updated failed => ");
                $logger->info($e->getMessage());
        }
            
    
        return '';
    }


}