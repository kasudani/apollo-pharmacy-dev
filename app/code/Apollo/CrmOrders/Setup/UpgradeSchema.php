<?php

namespace Apollo\CrmOrders\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * 
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{

		$installer = $setup;
		$installer->startSetup();

		/**
		 *  Create Crm Orders Log table for cron
		 */
		if (version_compare($context->getVersion(), '2.0.1') < 0) {

			// $tableName = $installer->getTable('crmorders_track');
			/*$installer->getConnection()->addColumn(
                $installer->getTable('crmorders_track'),
                'push_count',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => false,
                    'default' => 'initial',
                    'comment' => 'How many times order has been pushed to crm api'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('crmorders_track'),
                'push_status',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => false,
                    'default' => 'initial',
                    'comment' => 'Is the order was successfully pushed'
                ]
            );*/

            $crmOrdersLogTable = $installer->getTable('crm_orders_log');
            if ($installer->getConnection()->isTableExists($crmOrdersLogTable) != true) {
                
                $table = $installer->getConnection()->newTable($crmOrdersLogTable)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'quote_id',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false],
                        'Magento Quote id'
                    )
                    ->addColumn(
                        'order_id',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false],
                        'Magento order id'
                    )
                    ->addColumn(
                        'order_number',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false],
                        'Magento order number'
                    )
                    ->addColumn(
                        'payment_status',
                        Table::TYPE_TEXT,
                        null,
                        ['nullable' => false, 'default' => ''],
                        'Magento payment status'
                    )
                    ->addColumn(
                        'crm_update_status',
                        Table::TYPE_TEXT,
                        null,
                        ['nullable' => false, 'default' => ''],
                        'Order updated to crm or not'
                    )
                    ->addColumn(
                        'push_count',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false, 'default' => '0'],
                        'How many times order has been pushed to crm api'
                    )
                    ->addColumn(
                        'email_sent',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => true],
                        '1 => Sent, 0 => not sent'
                    )
                    ->addColumn(
                        'created_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                        'CREATED_AT'
                    )
                    ->addColumn(
                        'updated_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                        'UPDATED_AT'
                    )
                    ->setComment('CRM update log table')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
                
                // create index also

                $installer->getConnection()->createTable($table);



            }

		}
        if (version_compare($context->getVersion(), '2.0.2', '<')) {

           // Get module table
           $tableName = $setup->getTable('crm_orders_log');

           // Check if the table already exists
           if ($setup->getConnection()->isTableExists($tableName) == true) {
               // Declare data
               $columns = [
                   'message' => [
                       'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                       'nullable' => true,
                       'length' => 255,
                       'comment' => 'message',
                   ]
               ];

               $connection = $setup->getConnection();

               foreach ($columns as $name => $definition) {
                   $connection->addColumn($tableName, $name, $definition);
               }

           }
       }


        $setup->endSetup(); // end setup
	}
}