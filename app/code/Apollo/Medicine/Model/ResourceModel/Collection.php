<?php 
namespace Apollo\Medicine\Model\ResourceModel\OrderMedicineDataLog;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init('Apollo\Medicine\Model\OrderMedicineDataLog','Apollo\Medicine\Model\ResourceModel\OrderMedicineDataLog');
	}
}
 ?>