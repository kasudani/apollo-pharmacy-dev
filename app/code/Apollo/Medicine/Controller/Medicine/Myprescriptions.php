<?php
namespace Apollo\Medicine\Controller\Medicine;
/**
 * ExtendTree
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL)
 * This is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 *
 * DISCLAIMER**
 *
 * @category   Magento2 Simple Modules
 * @license    http://opensource.org/licenses/OSL-3.0  Open Software License (OSL)
 * @Website    http://www.extendtree.com/
 */
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class Myprescriptions extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if (!$customerSession->isLoggedIn()) {
        	$resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('sociallogin/mobile/login');
            return $resultRedirect;
        }

    $_SESSION['refered_url'] = $this->_redirect->getRefererUrl();

	$this->_view->loadLayout();
    $this->_view->getLayout()->getBlock('myprescriptions');
	$this->_view->renderLayout();	
    }

}