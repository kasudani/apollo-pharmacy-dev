<?php
namespace Apollo\Medicine\Controller\Medicine;
/**
 * ExtendTree
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL)
 * This is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 *
 * DISCLAIMER**
 *
 * @category   Magento2 Simple Modules
 * @license    http://opensource.org/licenses/OSL-3.0  Open Software License (OSL)
 * @Website    http://www.extendtree.com/
 */
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Storelocations extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        if(isset($_REQUEST['pincode'])){
            $pincode = $_REQUEST['pincode'];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');

            if(isset($_REQUEST['type']) && ($_REQUEST['type'] == "store_selection")){
                $checkoutSession->setSelectedStore($_REQUEST['store_id']);
                $deliveryInfo = array();
                $deliveryInfo['method'] = "store_pickup";
                $deliveryInfo['address'] = $_REQUEST['store_id'];
                $deliveryInfo = serialize($deliveryInfo);
                $checkoutSession->setDeliveryInfo($deliveryInfo);
                $objectManager->create('Apollo\Medicine\Helper\Data')->updatedDeliveryInfo($deliveryInfo,$objectManager);
                exit;
            }
            
            $checkoutSession->setSelectedPrescriptions(array());
            $checkoutSession->setSelectedPincode($_REQUEST['pincode']);
            $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');
            $storeUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
            $result = $objectManager->create('Apollo\Medicine\Helper\Data')->getStoreLocations($pincode);
            $htmlREsponse = '<p></p><div class="previous_prescriptions"> <ul>';
            $i = 0;
            if(sizeof($result) > 0){
                foreach ($result as $storeinfo){
                    $checked = ($i == 0) ? 'checked="checked"' : "";
                    $htmlREsponse .= '<li><input '.$checked.' type="radio" name="store_selection" id="store_'.$storeinfo['slno'].'" value="'.$storeinfo['slno'].'"  class="store_selection"> '.$storeinfo['baddress'].'</li>';
                    $i++;
                }
            }else{
                $htmlREsponse .= '<li>Stores are not available for this location</li>';
            }
            $htmlREsponse .= '</ul></div>';
            if($i>0){}
            echo $htmlREsponse;die;
        }else{
            echo "Invalid Request";die;
        }

    }
}
