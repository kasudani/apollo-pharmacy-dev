<?php
namespace Apollo\Medicine\Controller\Medicine;
/**
 * ExtendTree
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL)
 * This is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 *
 * DISCLAIMER**
 *
 * @category   Magento2 Simple Modules
 * @license    http://opensource.org/licenses/OSL-3.0  Open Software License (OSL)
 * @Website    http://www.extendtree.com/
 */
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Asset\Repository;

class Storepickupmobileconfirmation extends \Magento\Framework\App\Action\Action
{	

    protected $_storeManager;

    protected $_assetRepo;

    public function __construct(
    	\Magento\Framework\App\Action\Context $context,
    	\Magento\Framework\View\Asset\Repository $assetRepo,
    	StoreManagerInterface $storeManager
    	) {
        $this->_storeManager = $storeManager;
        $this->_assetRepo = $assetRepo;
        parent::__construct($context);
    }


    public function execute(){
		$this->_view->loadLayout();
	    $this->_view->getLayout()->getBlock('Storepickupmobileconfirmation');
		$this->_view->renderLayout();	
    }

}