<?php
namespace Apollo\Medicine\Controller\Medicine;
/**
 * ExtendTree
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL)
 * This is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 *
 * DISCLAIMER**
 *
 * @category   Magento2 Simple Modules
 * @license    http://opensource.org/licenses/OSL-3.0  Open Software License (OSL)
 * @Website    http://www.extendtree.com/
 */
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Productzipcodecheck extends \Magento\Framework\App\Action\Action
{
    const PRODUCT_CASH_ON_DELIVERY_AVAILABLE = '<li><i class="fa fa-check-square" aria-hidden="true"></i> Cash on Delivery available</li>';
    const PRODUCT_DELIVERY_AVAILABLE = '<li><i class="fa fa-check-square" aria-hidden="true"></i> This product is deliverable to this pincode</li>';
    const PRODUCT_CASH_ON_DELIVERY_NOT_AVAILABLE = '<li><i class="fa fa-check-square square2" aria-hidden="true"></i> Cash on Delivery not available';
    const PRODUCT_DELIVERY_NOT_AVAILABLE = '<li><i class="fa fa-check-square square2" aria-hidden="true"></i> This product is not deliverable to this pincode</li>';
    public function execute()
    {
        if(isset($_REQUEST['pincode'])){
            $pincode = $_REQUEST['pincode'];
            $productType = $_REQUEST['product_type'];
            $type = $_REQUEST['type'];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
            $checkoutSession->setSelectedPincode($pincode);
            $result = $objectManager->create('Apollo\Medicine\Helper\Data')->getCodShippingAvailbility($pincode);
            //echo "<pre>";print_R($result);die;
            if(isset($type) && ($type == "html")){
                echo $this->getMessage($productType,$result);
            }else{
                $resultJson = json_encode($result,JSON_UNESCAPED_SLASHES);
                echo $resultJson;die;
            }
        }else{
            echo "Invalid Request";die;
        }

    }

    public function getMessage($productType,$result){
        $htmlREsponse = '<ul>';
        $i = 0;
        $deliveryAvail = false;
        if(isset($result['pincode'])){
            if($productType == "Fmcg"){
                if($result['fmcg_cod'] == 1){ $i++;
                    $htmlREsponse .= self::PRODUCT_CASH_ON_DELIVERY_AVAILABLE;
                }else{
                    $htmlREsponse .= self::PRODUCT_CASH_ON_DELIVERY_NOT_AVAILABLE;
                }
                if($result['fmcg_shipping'] == 1){ $i++;
                    $deliveryAvail = true;
                    $htmlREsponse .= self::PRODUCT_DELIVERY_AVAILABLE;
                }else{
                    $htmlREsponse .= self::PRODUCT_DELIVERY_NOT_AVAILABLE;
                }
            }else{
                if($result['pharma_cod'] == 1){ $i++;
                    $htmlREsponse .= self::PRODUCT_CASH_ON_DELIVERY_AVAILABLE;
                }else{
                    $htmlREsponse .= self::PRODUCT_CASH_ON_DELIVERY_NOT_AVAILABLE;
                }
                if($result['pharma_shipping'] == 1){ $i++;
                    $deliveryAvail = true;
                    $htmlREsponse .= self::PRODUCT_DELIVERY_AVAILABLE;
                }else{
                    $htmlREsponse .= self::PRODUCT_DELIVERY_NOT_AVAILABLE;
                }
            }
            if($deliveryAvail == true){
                $htmlREsponse .= '<li><i class="fa fa-truck" aria-hidden="true"></i> Delivered within 4-5 days</li>';
            }
        }else{
            //$htmlREsponse = "<li>Data Not available</li>";
            $htmlREsponse .= self::PRODUCT_CASH_ON_DELIVERY_NOT_AVAILABLE;
            $htmlREsponse .= self::PRODUCT_DELIVERY_NOT_AVAILABLE;
        }

        $htmlREsponse .= '</ul>';
        return $htmlREsponse;
    }

}