<?php
namespace Apollo\Medicine\Controller\Medicine;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Pincodesession extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
		//print "Pincodesession";exit;
        if(isset($_REQUEST['skip_and_explore'])){
            $pcasession_val = $_REQUEST['skip_and_explore'];
            $_SESSION["skip_and_explore"] = $pcasession_val;
            exit;
        }
	
		if(isset($_REQUEST['pcasession_req_val']))
		{
			$pcasession_val = $_REQUEST['pcasession_req_val'];
			$_SESSION["pcasession_val"] = $pcasession_val;
		}
        if(isset($_REQUEST['popupcity_req_val']))
        {
            $popupcity_req_val = $_REQUEST['popupcity_req_val'];
            $_SESSION["city_val"] = $popupcity_req_val;
        }
        if(isset($_REQUEST['selected_storename']))
        {
            $selected_storename = $_REQUEST['selected_storename'];
            $_SESSION["selected_storename"] = $selected_storename;
        }

		if(isset($_REQUEST['pincodesession_req_val']))
		{
            $pincodesession = $_REQUEST['pincodesession_req_val'];
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');				
			$checkoutSession->setPincodesession($pincodesession);
            exit;
		}
		else
		{
            echo "Invalid Request";
			die;
        }
	}
}
