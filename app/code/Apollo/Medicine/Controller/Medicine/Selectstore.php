<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Selectstore extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,\Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $selectedPincode="123456";
        if(isset($_REQUEST['pincode'])){
            if(!isset($_REQUEST['store_id'])){
                $this->messageManager->addError(__('Please choose store'));
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                return $resultRedirect->setData(array("success"=>false,"mesage"=>"Please Select Store"));
            }
            $selectedPincode = $_REQUEST['pincode'];
            $checkoutSession->setSelectedStore($_REQUEST['store_id']);
        }
        if(!empty($selectedPincode)){
            $result = $objectManager->create('Apollo\Medicine\Helper\Data')->getCityPincodes2($selectedPincode);
            if(isset($result[0])){
                $result = $result[0];
                //echo "<pre>";print_R($result);die;
                $_SESSION["pcasession_val"] = $result['baddress'];
                $_SESSION["city_val"] = $result['pcity'];
                $_SESSION["selected_storename"] = $result['storename'];
                $checkoutSession->setPincodesession($selectedPincode);
            }
        }


        //$checkoutSession->setSelectedPincode($selectedPincode);
        $checkoutSession->setDeliveryMethod($_REQUEST['delivery_method']);

        $_REQUEST['previous_prescriptions'] = $checkoutSession->getSelectedPrescriptions();

        $deliveryInfo = array();
        $deliveryInfo['method'] = $_REQUEST['delivery_method'];
        if(isset($_REQUEST['previous_prescriptions'])){
            $deliveryInfo['previous_prescriptions'] = $_REQUEST['previous_prescriptions'];
        }
        if(isset($_REQUEST['store_id'])){
            $deliveryInfo['address'] = $_REQUEST['store_id'];
        }
        $deliveryInfo = serialize($deliveryInfo);
        $checkoutSession->setDeliveryInfo($deliveryInfo);
        //$objectManager->create('Apollo\Medicine\Helper\Data')->updatedDeliveryInfo($deliveryInfo,$objectManager);
        //echo $selectedPincode; die;



        if(isset($selectedPincode)){
             
            if(isset($_SESSION["pincheck"])){

                unset($_SESSION["pincheck"]);

            }
            $_SESSION["pincheck"]="325325";
            
             $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_JSON);
             $resultRedirect->setData(array("success"=>true,"mesage"=>"Store selected successfully"));
             return $resultRedirect;
        }else{
            echo "Invalid Request";die;
        }

    }
}