<?php
namespace Apollo\Medicine\Controller\Medicine;
/**
 * ExtendTree
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL)
 * This is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 *
 * DISCLAIMER**
 *
 * @category   Magento2 Simple Modules
 * @license    http://opensource.org/licenses/OSL-3.0  Open Software License (OSL)
 * @Website    http://www.extendtree.com/
 */
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class Ordermedicinemobile extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {

    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');

    	$selected_prescriptions = $checkoutSession->getSelectedPrescriptions();

    	if(!empty($selected_prescriptions)){
    		$resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('ordermedicine/medicine/uploadedprescriptionsmobile');
            return $resultRedirect;
    	}

	$this->_view->loadLayout();
    $this->_view->getLayout()->getBlock('ordermedicinemobile');
	$this->_view->renderLayout();	
    }

}