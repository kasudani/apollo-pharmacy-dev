<?php
namespace Apollo\Medicine\Controller\Medicine;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Getservicelocation extends \Magento\Framework\App\Action\Action
{
	
	public function execute()
    {
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resultJsonFactory = $objectManager->get('\Magento\Framework\Controller\Result\JsonFactory');

        $store = "Service Locator";
        if(isset($_SESSION["selected_storename"])){
            $store =  $_SESSION["selected_storename"];
        }

        if(isset($_SESSION["selected_storename"]) && isset($_SESSION["city_val"])){
            $store .= ", ";
        }
        
        if(isset($_SESSION["city_val"])){
            $store .= $_SESSION["city_val"];
        }
        return $resultJsonFactory->create()->setData(['result' => $store]);

    }

}
