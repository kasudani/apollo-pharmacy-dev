<?php
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Storesession extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
		if(isset($_REQUEST['storetype']))
		{
            $storetype = $_REQUEST['storetype'];
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');

            //$checkoutSession->setDeliveryMethod($storetype);
            $deliveryInfo = array();
            $deliveryInfo['method'] = "store_pickup";
            $deliveryInfo['address'] = (isset($_REQUEST['store_selection'])) ? $_REQUEST['store_selection'] : '';
            $deliveryInfo = serialize($deliveryInfo);
            $checkoutSession->setDeliveryInfo($deliveryInfo);
            $this->_objectManager->create('Apollo\Medicine\Helper\Data')->updatedDeliveryInfo($deliveryInfo,$this->_objectManager);
            $checkoutSession->setSelectedPincode($_REQUEST['store_pickup_pincode']);
            $checkoutSession->setSelectedPrescriptions(array());
           	exit;
        }else{
            echo "Invalid Request";die;
        }
	}
}
