<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Check extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    
    protected $_objectManager;
    
    public function __construct(\Magento\Framework\App\Action\Context $context, 
                                \Magento\Framework\ObjectManagerInterface $objectManager,
                                \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory      = $pageFactory;
        $this->_objectManager = $objectManager;
        parent::__construct($context);
    }
    public function execute()
    {
       $set = 'true';
       $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
       // print_r($checkoutSession->getData());
       // die();
       $cart = $this->_objectManager->get('\Magento\Checkout\Model\Cart'); 
       $items = $cart->getQuote()->getAllItems();
        foreach($items as $item) {
            $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($item->getProduct()->getId());
              if($product->getData('is_prescription_required') == 1){
                if(!$checkoutSession->getData('selected_prescriptions')){
                  $set = 'false';
                }
              }          
          }
        echo $set;
    }
}
