<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Saveguest extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $quoteRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    )
    {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->quoteRepository = $quoteRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $mobile = $_REQUEST['guest_mobile'];
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        echo $quoteId = $checkoutSession->getData('quote_id_1');
        $quote = $this->quoteRepository->get($quoteId); // Get quote by id
        $quote->setData('guest_verification', $mobile); // Fill data
        $this->quoteRepository->save($quote);
        echo "saved";
    }
}