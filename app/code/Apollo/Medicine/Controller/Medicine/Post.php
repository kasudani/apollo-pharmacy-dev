<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Apollo\Medicine\Controller\Medicine;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Apollo\Medicine\Model\OrderMedicineDataLogFactory;



class Post extends \Magento\Framework\App\Action\Action {

    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $_medicineOrdersLogModel;

    public function __construct(
      \Magento\Framework\App\Action\Context $context, 
      \Magento\Framework\ObjectManagerInterface $objectManager, 
      StoreManagerInterface $storeManager, 
      \Magento\Framework\Filesystem $filesystem, 
      \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
      \Apollo\Medicine\Model\OrderMedicineDataLogFactory $medicineOrdersLogFactory

    ) {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
         $this->_medicineOrdersLogModel = $medicineOrdersLogFactory;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute() {
        

        $post = $this->getRequest()->getPostValue();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager

        if (isset($post['mainid_'])) {
            $pin = $post['mainid_'];
            $_SESSION['logged_in'] = $pin;

            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('plocations'); //gives table name with prefix
            //Select Data from table
            $sql = "Select * FROM " . $tableName . " where pincode =" . $pin;
            $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.

            foreach ($result as $results) {
                $city = $results['pcity'];
                $state = $results['staten'];
                $address = $results['baddress'];
            }
            if (isset($city)) {
                echo $city . "&" . $state . "&" . $address;
            }
        } else if (isset($post['pincode'])) {
            //echo 'sdg';exit;

            $selectedPincode = $post['pincode'];
            if (!empty($selectedPincode)) {
                //$selectedStore = $checkoutSession->getSelectedStore();
                $selectedStore = " ";
                $pcasession = " ";

                $result = $objectManager->create('Apollo\Medicine\Helper\Data')->getStoreLocations($selectedPincode);
                $htmlREsponse = '<p></p><div class="previous_prescriptions"> <ul>';
                $i = 0;
                if (sizeof($result) > 0) {
                    foreach ($result as $storeinfo) {

                        if (empty($selectedStore) && !empty($pcasession)) {
                            $checkedstate = ($pcasession == $storeinfo['baddress']) ? "checked='checked'" : "";
                        } else {
                            $checkedstate = ($selectedStore == $storeinfo['slno']) ? "checked='checked'" : "";
                        }
                        if ($checkedstate == "") {
                            $checkedstate = ($i == 0) ? "checked='checked'" : "";
                        }

                         $htmlREsponse .= '<li><input ' . $checkedstate . ' type="radio" name="store_selection" id="store_' . $storeinfo['slno'] . '" value="' . $storeinfo['baddress'] . '" class="store_selection" onClick="categoryswitch(event)">' . $storeinfo['baddress'] . '</li>';
                        $i++;
                    }
                } else {
                    $htmlREsponse .= '<li><input name="store_select" type="text" id="store_select"></input>Stores are not available for this location</li>';
                }
                $htmlREsponse .= '</ul></div>';
                if ($i > 0) {
                    
                }
                if (isset($storeinfo['pcity'])) {
                    echo $htmlREsponse . "%" . $storeinfo['pcity'] . "%" . $storeinfo['staten'] . "%" . $storeinfo['baddress'];
                } else {
                    echo $htmlREsponse;
                }
            }
        } else {

           
            $fileruns= explode(",",$post['filerun']);
            $rand = rand(10000, 990000);
              $sDirPath = 'pub/media/medicine_prescription/'.$rand.'/'; //Specified Pathname
                    if (!file_exists ($sDirPath))
                    { mkdir($sDirPath,0777,true); }
             if(isset($_FILES['add_file2']['name'])){
                 
                for($i=0;$i<count($_FILES["add_file2"]["name"]);$i++)
                    {
                    
                     if (in_array($_FILES["add_file2"]["name"][$i], $fileruns))
                    {
                     $uploadfile=$_FILES["add_file2"]["tmp_name"][$i];
                     $folder="pub/media/medicine_prescription/";
                     move_uploaded_file($_FILES["add_file2"]["tmp_name"][$i], "$sDirPath".$_FILES["add_file2"]["name"][$i]);
                     $result_file[] = $_FILES["add_file2"]["name"][$i];
                     }
                    
                    }
            }
            
            
            if(isset($_FILES['add_file1']['name'])){
                for($i=0;$i<count($_FILES["add_file1"]["name"]);$i++)
                    {
                     if (in_array($_FILES["add_file1"]["name"][$i], $fileruns))
                    {
                        
                     $uploadfile=$_FILES["add_file1"]["tmp_name"][$i];
                     $folder="pub/media/medicine_prescription/";
                    chmod($sDirPath, 0777);
                     move_uploaded_file($_FILES["add_file1"]["tmp_name"][$i], "$sDirPath".$_FILES["add_file1"]["name"][$i]);
                     $result_file[] = $_FILES["add_file1"]["name"][$i];
                    }
                    
                    
                    }
            }
           
             if(isset($_FILES['add_file3']['name'])){
                for($i=0;$i<count($_FILES["add_file3"]["name"]);$i++)
                    {
                    
                     if (in_array($_FILES["add_file3"]["name"][$i], $fileruns))
                    {
                     $uploadfile=$_FILES["add_file3"]["tmp_name"][$i];
                     $folder="pub/media/medicine_prescription/";
                      chmod($sDirPath, 0777);
                     move_uploaded_file($_FILES["add_file3"]["tmp_name"][$i], "$sDirPath".$_FILES["add_file3"]["name"][$i]);
                    $result_file[] = $_FILES["add_file3"]["name"][$i];
                     }
                    
                    }
            }
            $result_file['rand'] = $rand;
             $save_files = implode(',', $result_file);
           
            
            $base_url = $this->_storeManager->getStore()->getBaseUrl();
            //print_r($post);echo 'dsg';exit;
            $rand = rand(10000, 990000);

            $pathurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'medicine_prescription/';
            $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
            $mediapath = $this->_mediaBaseDirectory = rtrim($mediaDir, '/');
            $currenttime = gmstrftime('%Y-%m-%d %H:%M:%S', time() + 19800);

            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
            $connection = $this->_resources->getConnection();

            $themeTable = $this->_resources->getTableName('ordermedicine_data');
            

            if (isset($post)) {
                if ($post['storpickup_pincode']!="Store") {
                    $odeliverytype = "Store";
                    $sql = "INSERT INTO " . $themeTable . "(oname,oemail,oaddress,ocontact,ostate,ocity,opincode,odeliverytype,created_at,uploaded_file,ototalqty,oamount,is_active) VALUES ('" . $post['oname'] . "','" . $post['oemail'] . "','" . $post['oaddress'] . "','" . $post['ocontact'] . "','" . $post['ostate'] . "','" . $post['ocity'] . "','" . $post['opincode'] . "','" . $odeliverytype . "','" . $currenttime . "','" . $save_files . "','0','0','1')";
                
                } else{
                    $sql = "INSERT INTO " . $themeTable . "(oname,oemail,oaddress,ocontact,ostate,ocity,opincode,odeliverytype,created_at,uploaded_file,ototalqty,oamount,is_active) VALUES ('" . $post['oname'] . "','" . $post['oemail'] . "','" . $post['oaddress'] . "','" . $post['ocontact'] . "','" . $post['ostate'] . "','" . $post['ocity'] . "','" . $post['opincode'] . "','" . $post['odeliverytype'] . "','" . $currenttime . "','" . $save_files . "','0','0','1')";
                                          
                }
                
               
            }

            	$connection->query($sql); 			
		
		$lid=$connection->lastInsertId();	
 	

		$resq=$connection->fetchAll("select * from ordermedicine_data where id='".$lid."'");

	
 		  $x=array("ItemID"=>'',"ItemName"=>'',"Qty"=>'',"orddt"=>'',"Price"=>'');
		  $z=array("Totalamount"=>'',"Paymentsource"=>'',"Paymentstatus"=>'',"Paymentorderid"=>'');
		 // $ordersResult=array("Status"=>'failure');
		    $presimg="";
		    $base_url="";
		
  		   $prescimg=explode(",",$resq[0]['uploaded_file']);	

			$cnt=count($prescimg)-1;

			   for($i=0;$i<$cnt;$i++)
		 	   {
		 	     	$presimg .=$pathurl.$prescimg[$cnt]."/".$prescimg[$i].",";
   		 	   }

			$presimgg = substr($presimg,0,strlen($presimg)-1);

			
		  $cust=array(
			"Mobileno"=>$resq[0]['ocontact'],
			"Comm_addr"=>$resq[0]['oaddress'],
			"Del_addr"=>$resq[0]['oaddress'],    
			"FirstName"=>$resq[0]['oname'],
			"LastName"=>"",
			"City"=>$resq[0]['ocity'],	
			"Postcode"=>$resq[0]['opincode'],
			"Mailid"=>$resq[0]['oemail'],
			"Age"=>"30",	
			"Cardno"=>"00",			
			"PatientName"=>""
			);

		  $tpdetails=array(   
			"Orderid"=>$lid,
			"Shopid"=>"16001",
			"ShippingMethod"=>$resq[0]['odeliverytype'],
			"PaymentMethod"=>'',	
			"Vendorname"=>"online",
			"DotorName"=>"Apollo",
			"url"=>$presimgg,
			"Ordertype"=>"Pharma",
			"Customerdetails"=>$cust,
			"Paymentdetails"=>$z,
			"itemdetails"=>$x
     
			);

      
      $arr=array("tpdetails"=>$tpdetails);

		  $curl=curl_init("http://220.225.226.198:51/Onlineorder.svc/PLACE_ORDERS");
		
		  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

		  curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($arr));
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($arr))));

		  $tokenn = curl_exec($curl);

		  $res=json_decode($tokenn, TRUE);
      /**
         *  Set Flag if Crm response is success / failure
         */
        $medicineResponseStatus = '';
       // $res = array();

        //$res['ordersResult']['Status']=="success";
       // $res['Status']="failure";

        if($res['ordersResult']['Status']=="Success" || $res['ordersResult']['Status']=="success")
        {
          $medicineResponseStatus = 'success'; // response success
        }
        else
        { 
          $medicineResponseStatus = 'failure'; // response failure
        }
        
         

		  $inlineTranslation = $objectManager->get('Magento\Framework\Translate\Inline\StateInterface');
                $escaper = $objectManager->get('Magento\Framework\Escaper');
                $transportBuilder = $objectManager->get('Magento\Framework\Mail\Template\TransportBuilder');


                $post = $this->getRequest()->getPostValue();
                if (!$post) {
                    $this->_redirect('*/*/');
                    return;
                }

                $inlineTranslation->suspend();
                try {
                      
                      
                    $postObject = new \Magento\Framework\DataObject();
                    $postObject->setData($post);

                    $error = false;

                    $sender = [
                        'name' => $escaper->escapeHtml('Apollo'),
                        'email' => $escaper->escapeHtml('apollo@gmail.com'),
                    ];
                    

                    $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                    $transport = $transportBuilder
                            ->setTemplateIdentifier('send_email_email_template') // this code we have mentioned in the email_templates.xml
                            ->setTemplateOptions(
                                    [
                                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                                    ]
                            )
                            ->setTemplateVars(['data' => $postObject])
                            ->setFrom($sender)
                            ->addTo($post['oemail'])
                            ->getTransport();

                    $transport->sendMessage();
                    ;
                    $inlineTranslation->resume();

                    $this->messageManager->addSuccess(
                            __('We\'ll respond to you very soon.')
                    );
                    $this->_redirect($base_url . 'ordersuccess');
                     
                      $model = $this->_medicineOrdersLogModel->create();
                      $model->addData([
                                        "order_id" =>$lid,
                                        "order_update_status" =>$medicineResponseStatus,
                                        "push_count" => '0'
                                       
                                     ]);
                      $saveData = $model->save();
                      if($saveData){

                        

                      }
                    return;
                } catch (\Exception $e) {
                                      
                   $inlineTranslation->resume();
                    $this->messageManager->addError(
                            __('We are not able to send email to you. Sorry, that\'s all we know.' . $e->getMessage())
                    );
                    $this->_redirect($base_url . 'ordersuccess');
                    return;
                }
            $this->_redirect($base_url . 'ordersuccess');
            
        }
    }
          
 public function getExtension($str)
{
 
         $i = strrpos($str,".");
         if (!$i) { return ""; }
 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
}

	

}
