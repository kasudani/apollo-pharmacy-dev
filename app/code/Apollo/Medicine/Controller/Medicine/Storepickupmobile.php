<?php
namespace Apollo\Medicine\Controller\Medicine;
/**
 * ExtendTree
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL)
 * This is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 *
 * DISCLAIMER**
 *
 * @category   Magento2 Simple Modules
 * @license    http://opensource.org/licenses/OSL-3.0  Open Software License (OSL)
 * @Website    http://www.extendtree.com/
 */
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Asset\Repository;

class Storepickupmobile extends \Magento\Framework\App\Action\Action
{	

    protected $_storeManager;

    protected $_assetRepo;

    public function __construct(
    	\Magento\Framework\App\Action\Context $context,
    	\Magento\Framework\View\Asset\Repository $assetRepo,
    	StoreManagerInterface $storeManager
    	) {
        $this->_storeManager = $storeManager;
        $this->_assetRepo = $assetRepo;
        parent::__construct($context);
    }


    public function execute(){
    	
    	$post = $this->getRequest()->getPostValue();
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$base_url = $this->_storeManager->getStore()->getBaseUrl();
    	$htmlREsponse = '';
    	if (isset($post['pincode'])) {
    		$selectedPincode = $post['pincode'];
    		$result = $objectManager->create('Apollo\Medicine\Helper\Data')->getStoreLocations($selectedPincode);
    		if (sizeof($result) > 0) {
    			foreach ($result as $storeinfo) {

					$latLonAccordingPincode = $this->getLatLonPincode($selectedPincode);
					$lat1  = !empty($latLonAccordingPincode['lat'])?$latLonAccordingPincode['lat']:'';
					$lat2  = !empty($storeinfo['plat'])?$storeinfo['plat']:'';
					$long1 = !empty($latLonAccordingPincode['lng'])?$latLonAccordingPincode['lng']:'';
					$long2 = !empty($storeinfo['plon'])?$storeinfo['plon']:'';
					
					$distance = $this->getDrivingDistance($lat1, $lat2, $long1, $long2);

    				$htmlREsponse .= '<div class="pos-rel store-result-box"><a href="javascript:void(0);" class="result-box"><div class="white-box f-store-box" storeid="'.$storeinfo['storeid'].'"><div class="box-table-cell"><div class="box-table"> <div class="f-store-name">'.$storeinfo['baddress'].' '.$storeinfo['storename'].'</div><div class="f-store-info"><div class="rating">4.5 <img src="'.$this->_assetRepo->getUrl('Apollo_Medicine::images/rating-star.png').'" /></div> <div class="distance">'.$distance.'</div></div></div></div></div></a><a href="'.$base_url.'ordermedicine/medicine/storepickupmobileconfirmation/store_id/'.$storeinfo['storeid'].'" class="pull-right green-button"><img src="'.$this->_assetRepo->getUrl('Apollo_Medicine::images/white-right-arrow.png').'" /></a></div>';
    			}

    		} else {
    			 $htmlREsponse .= '<div>Stores are not available for this location</div>';
    		}
            $final_result = [];
            array_push($final_result, $htmlREsponse);
            array_push($final_result, $result);
    		echo json_encode($final_result);
    		die();
    	}
		$this->_view->loadLayout();
	    $this->_view->getLayout()->getBlock('Storepickupmobile');
		$this->_view->renderLayout();	
    }

    function getLatLonPincode($pincode){

    	if(!empty($pincode)){

    		$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($pincode)."&key=AIzaSyC2ZN-ALGpefRZxV_3XjAiOT7rXdAEKa6o&sensor=false";
    		$response = $this->getCurl($url);
			$result = json_decode($response, true);
			return !empty($result['results'][0]['geometry']['location'])?$result['results'][0]['geometry']['location']:'';
    	}	
    }

    function getDrivingDistance($lat1, $lat2, $long1, $long2) {
	    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&key=AIzaSyC2ZN-ALGpefRZxV_3XjAiOT7rXdAEKa6o&mode=driving&language=pl-PL";
	    $response = $this->getCurl($url);
	    $response = json_decode($response, true);
	    $dist = !empty($response['rows'][0]['elements'][0]['distance']['text'])?$response['rows'][0]['elements'][0]['distance']['text']:'';

	    return $dist;
	}

	function getCurl($url){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    $response = curl_exec($ch);
	    curl_close($ch);

	    return $response;
	}

}