<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Controller\Adminhtml\Ordermedicine;

use Apollo\Medicine\Controller\Adminhtml\Ordermedicine as OrdermedicineController;
use Magento\Framework\Controller\ResultFactory;

class NewAction extends OrdermedicineController
{
    /**
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
        $result->forward('edit');
        return $result;
    }
}
