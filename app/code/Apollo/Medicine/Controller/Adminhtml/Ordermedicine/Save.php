<?php
namespace Apollo\Medicine\Controller\Adminhtml\Ordermedicine;
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */


use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Apollo\Medicine\Model\OrdermedicineFactory
     */
    protected $_ordermedicine;
    /**
     * @var Apollo\Medicine\Model\ResourceModel\Ordermedicine\CollectionFactory
     */
    protected $_ordermedicineCollection;
    /**
     * @param Action\Context                                                        $context
     * @param \Magento\Framework\Filesystem                                         $fileSystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory                      $fileUploader
     * @param \Apollo\Medicine\Model\ordermedicineFactory                         $zipcode
     * @param Apollo\Medicine\Model\ResourceModel\Ordermedicine\CollectionFactory $ordermedicineCollection
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploader,
        \Apollo\Medicine\Model\OrdermedicineFactory $ordermedicine,
        \Apollo\Medicine\Model\ResourceModel\Ordermedicine\CollectionFactory $ordermedicineCollection
    ) {
        $this->_fileSystem = $fileSystem;
        $this->_uploader = $fileUploader;
        $this->_ordermedicine = $ordermedicine;
        $this->_ordermedicineCollection = $ordermedicineCollection;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Apollo_Medicine::ordermedicine');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $error = false;
        $data = $this->getRequest()->getParams();
        if ($this->getRequest()->isPost()) {
            $id = (int) $this->getRequest()->getParam('id');
          //  $check = $this->checkOrder($data);
		  $check="true";
            if ($check) {
                $this->setOrderData($data);
				
	//			$user = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\User\Model\User');
			//	$user = $this->_auth->getUser();

				
			//	$currenttime = date('Y-m-d H:i:s');
			$currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);

				$this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
				$connection= $this->_resources->getConnection();

				$sql="update ordermedicine_data set oprescriptionid='PRE".$id."',created_at='".$currenttime."' where id='".$id."'";
				$connection->query($sql);


                $this->messageManager->addSuccess(__('Order saved succesfully'));
            } else {
                $this->messageManager->addError(__('Order already Exists'));
            }
        } else {
            $this->messageManager->addError(__("Something went wrong"));
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Save Order Data
     *
     * @param array $data
     */
    public function setOrderData($data)
    {
        $model = $this->_ordermedicine->create();
        if (array_key_exists("id", $data)) {
            $model->addData($data)->setId($data['id'])->save();
        } else {
            $model->setData($data)->save();
        }
    }
    /**
     * Check Order Data
     *
     * @param array $data
     */
    public function checkOrdercode($data)
    {
        $model = $this->_ordermedicineCollection->create()->addFieldToFilter('oname', $data['oname'])
        ->addFieldToFilter('oemail', $data['oemail']);
        foreach ($model as $value) {
            $result = $value->getData();
        }
        if (isset($result)) {
            return false;
        }
        return true;
    }
}
