<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Controller\Adminhtml;

use Magento\Backend\App\Action;

abstract class Ordermedicine extends \Magento\Backend\App\Action
{
    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Apollo_Medicine::ordermedicine');
    }
}
