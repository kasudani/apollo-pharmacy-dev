<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;

class Ordermedicine extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
