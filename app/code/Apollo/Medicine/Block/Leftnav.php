<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;

class Leftnav extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */

    protected $_customerSession;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
        array $data = []
    ) {
        $this->_customerSession = $customerSession->create();
        parent::__construct($context, $data);
    }

     public function getLoggedinCustomerId() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getId();
        }
        return false;
    }

    public function getCustomerData() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomerData();
        }
        return false;
    }
     public function getMediaUrl(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $media_dir = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
        ->getStore()
        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        return $media_dir;
    }

    public function isLoggedIn(){
       
    }

    public function getPrescriptions(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        $mediapath = $this->_mediaBaseDirectory = rtrim($mediaDir, '/');

        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('ordermedicine_data');
        $currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        
        $customerId = $customerSession->getCustomerId();

        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);

        $customerEmail = $customerSession->getCustomer()->getEmail();

        $sql = "SELECT * FROM ".$themeTable." where oemail='".$customerEmail."'";
        
        $selectedPrns =    $connection->query($sql);

        return $selectedPrns;
    }

    public function getSelectedStore(){

        $store = "Service Locator";
        if(isset($_SESSION["selected_storename"])){
            $store =  $_SESSION["selected_storename"];
        }

        if(isset($_SESSION["selected_storename"]) && isset($_SESSION["city_val"])){
            $store .= ", ";
        }
        
        if(isset($_SESSION["city_val"])){
            $store .= $_SESSION["city_val"];
        }
        return $store;
    }
}
