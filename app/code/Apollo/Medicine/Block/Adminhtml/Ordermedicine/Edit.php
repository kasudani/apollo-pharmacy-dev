<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block\Adminhtml\Ordermedicine;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Initialize Medicine ordermedicine Edit Block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Apollo_Medicine';
        $this->_controller = 'adminhtml_ordermedicine';
        parent::_construct();
        if ($this->_isAllowedAction('Apollo_Medicine::ordermedicine')) {
            $this->buttonList->update('save', 'label', __('Save Entry'));
            //$this->buttonList->remove('save');
            $this->buttonList->remove('delete');
        } else {
            $this->buttonList->remove('delete');
            $this->buttonList->remove('save');
        }
    }

    /**
     * Retrieve text for header element depending on loaded Group
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('medicine_ordermedicine')->getId()) {
            $title = $this->_coreRegistry->registry('medicine_ordermedicine')->getTitle();
            $title = $this->escapeHtml($title);
            return __("Edit Group '%'", $title);
        } else {
            return __('New Group');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
