<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;

class Myprescriptions extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
     public function getMediaUrl(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $media_dir = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            return $media_dir;
        }

    public function getPrescriptions(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        $mediapath = $this->_mediaBaseDirectory = rtrim($mediaDir, '/');


        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('ordermedicine_data');
        $currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
/*
           echo 'Customer Id: ' . $customerSession->getCustomer()->getId() . '<br/>';

    echo 'Customer Name: ' . $customerSession->getCustomer()->getName() . '<br/>';

    echo 'Customer Email: ' . $customerSession->getCustomer()->getEmail() . '<br/>';

    echo 'Customer Group Id: ' .  $customerSession->getCustomer()->getGroupId() . '<br/>';*/

        $customerId = $customerSession->getCustomerId();
        //print_r($customerSession->getData());
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $customerEmail = $customerSession->getCustomer()->getEmail();
       /* echo "<pre/>";
        $eatr = $customerObj->getAttribute('email');
        print_r($eatr->getData());
        print_r(get_class_methods(get_class($customerSession->getCustomer()))); 
        print_r(get_class_methods(get_class($eatr)));
        print_r(get_class_methods(get_class($customerObj))); die;
        $selectedPrescriptions = $checkoutSession->getSelectedPrescriptions();*/
        

        $sql = "SELECT * FROM ".$themeTable." where type='Prescription' and oemail='".$customerEmail."'";
        $selectedPrns =    $connection->query($sql);
        //print_r(get_class_methods(get_class($selectedPrns)));

        return $selectedPrns;
        
    }
}
