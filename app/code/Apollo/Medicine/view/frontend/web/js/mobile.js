define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'Magento_Ui/js/modal/modal'
], function ($, customerData, modal) {

    var options = {
                'type': 'popup',
                'title': 'Choose an option to upload your prescription',
                'modalClass': 'upload_prescription_popup', 
                'responsive': false,
                'innerScroll': false,
                'buttons': [/*{
                    text: $.mage.__('Back'),
                    class: 'back_button_class',
                    click: function () {
                        this.closeModal();
                        // any javascript coode
                    }
                }*/]
            };

	console.log(customerData);
  var upload_url  = $("#upload_url").val();
	var login_url  = $("#login_url").val();
  var next_url  = $("#next_url").val();
  var added_prescriptions_url  = $("#added_prescriptions_url").val();
  var select_prescription_url  = $("#select_prescription_url").val();
  var selectedPrescriptions  = [];
	var choose_prescriptions_url  = $("#choose_prescriptions_url").val();
  var customer = customerData.get('customer');
  var redirectIfNotLoggedIn = function(){
      console.log(customer().fullname);
            if(customer().fullname && customer().firstname)
            {
                return true;
            }
           //window.location = login_url;
  }

  var choosePrescriptions = function(){
    console.log(choose_prescriptions_url);
		window.location = choose_prescriptions_url;
	}

	var fileSelected = function() {
 
        var count = document.getElementById('fileToUpload').files.length;
 
              document.getElementById('details').innerHTML = "";
 
              for (var index = 0; index < count; index ++)
 
              {
 
                     var file = document.getElementById('fileToUpload').files[index];
 
                     var fileSize = 0;
 
                     if (file.size > 1024 * 1024)
 
                            fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
 
                     else
 
                            fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
 
                     document.getElementById('details').innerHTML += 'Name: ' + file.name + '<br>Size: ' + fileSize + '<br>Type: ' + file.type;
 
                     document.getElementById('details').innerHTML += '<p>';
 
              }
 
      }
 
      var uploadFile = function() {
 
 		   $(".loading").show();
        var fd = new FormData();
 
              var count = document.getElementById('fileToUpload').files.length;
 
              for (var index = 0; index < count; index ++)
 
              {
 
                     var file = document.getElementById('fileToUpload').files[index];
 
                     fd.append('myFile', file);
 
              }

              var count = document.getElementById('fileToUpload1').files.length;
 
              for (var index = 0; index < count; index ++)
 
              {
 
                     var file = document.getElementById('fileToUpload1').files[index];
 
                     fd.append('myFile', file);
 
              }
 
        var xhr = new XMLHttpRequest();
 
        xhr.upload.addEventListener("progress", uploadProgress, false);
 
        xhr.addEventListener("load", uploadComplete, false);
 
        xhr.addEventListener("error", uploadFailed, false);
 
        xhr.addEventListener("abort", uploadCanceled, false);
 
        xhr.open("POST", upload_url);
 
        xhr.send(fd);
 
      }
 
      var  uploadProgress = function(evt) {
 
        if (evt.lengthComputable) {
 
          var percentComplete = Math.round(evt.loaded * 100 / evt.total);
 
          //document.getElementById('progress').innerHTML = percentComplete.toString() + '%';
 
        }
 
        else {
 
          document.getElementById('progress').innerHTML = 'unable to compute';
          $(".loading").hide();
 
        }
 
      }
 
      var uploadComplete = function(evt) {
 
        /* This event is raised when the server send back a response */
 
        $(".loading").hide();
        localStorage.presentPrescriptions = evt.target.responseText;
        console.log(localStorage.presentPrescriptions);
        window.location = next_url;
        //alert(evt.target.responseText);
 
      }
 
      var uploadFailed = function(evt) {
 
 		     $(".loading").hide();
        alert("There was an error attempting to upload the file.");
 
      }
 
      var uploadCanceled = function(evt) {
 
         $(".loading").hide();
        alert("The upload has been canceled by the user or the browser dropped the connection.");
 
      } 

      var selectprescriptions = function(evt) {
 
 		     $(".loading").show();

         var data = {};
          data["selectedPrescriptions"] = selectedPrescriptions;
          $.ajax({
                url: select_prescription_url,
                type: 'POST',
                data: data
            }).done(function (response) {
              console.log(response);
              $(".loading").hide();
              if(response.status){
                window.location = added_prescriptions_url;
              }

            }).fail(function () {
               console.log("Error");
               $(".loading").hide();
            });
 
      }


	

    $(document).ready(function($) {

        
        $(".uploadYourPrescriptionLink").on("click", function(){
          var popup = modal(options, $('#uploadYourPrescription')); 
          $("#uploadYourPrescription").modal("openModal");
        });

        $(".upload-with-camera").on("click", function(){
          //redirectIfNotLoggedIn();
		    $("#fileToUpload").click();
		    

		});
    $(".upload-from-gallery").on("click", function(){
      // redirectIfNotLoggedIn();
        $("#fileToUpload1").click(); 
        

    });

		$(".choose-from-uploaded").on("click", function(){
      choosePrescriptions();

		});

		$("#fileToUpload").on("change", function(){
		   uploadFile();

		});
    $("#fileToUpload1").on("change", function(){
       uploadFile();

    });


    $(".my-prescription-box").on("click", function(){
      if($(this).hasClass("selected-border")){
        $(this).removeClass("selected-border");
        var pid = $(this).attr("data-id");
        for( var i = 0; i <= selectedPrescriptions.length-1; i++){ 
           if ( selectedPrescriptions[i] === pid) {
            selectedPrescriptions.splice(i, 1); 
           }
        }
      }else{
        $(this).addClass("selected-border");
        var pid = $(this).attr("data-id");
        selectedPrescriptions.push(pid);  
      }
      
    });

    $(".select-prescription").on("click", function(){
      if(selectedPrescriptions.length){
        console.log(selectedPrescriptions);
        selectprescriptions();  
      }else{
        $(".select_atleast_one").show();
        setTimeout(function(){ $(".select_atleast_one").hide(); }, 5000);
      }
      
    });


		$("#specify-detail-button").on("click", function(){

      var data = {};

      data['comment'] = $('#other-comments').val();
      var save_url = $('#save_url').val();
      var requestconfirmation_url = $('#requestconfirmation_url').val();

      var deliveryOption = $('input[name=selectDeliveryOption]:checked').val();

        console.log(deliveryOption);
       if(deliveryOption == 'homeDelivery'){
        var addressId = $('input[name=address]:checked').val();
        console.log(addressId);
        if(!addressId){
          $("#selectAddressError").show();
          setTimeout(function(){ $("#selectAddressError").hide(); }, 10000);
          return false;
        }
        data['deliveryType'] = 'Home';
        data['addressId'] = addressId;
       } else if(deliveryOption == 'storeDelivery'){
        var storeId = $('#store_available').val();
        if(!storeId){
          $("#selectStoreError").show();
          setTimeout(function(){ $("#selectStoreError").hide(); }, 10000);
          return false;
        }
        data['deliveryType'] = 'Store';
        data['storeId'] = storeId;
       }



      jQuery.ajax({
                url: save_url,
                data: data,
                type: "POST",
                showLoader: true,
                success: function (result) {
                  console.log(result);
                  window.location = requestconfirmation_url;    
                }
            });
		});

		
    });





});
