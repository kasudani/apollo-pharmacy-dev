/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2008-2017 Apollo Hospitals Limited
 */
var config = {
    map: {
        '*': {
            zipcodeadmin: 'Apollo_Medicine/js/ordermedicineadmin',
        }
    }
};

