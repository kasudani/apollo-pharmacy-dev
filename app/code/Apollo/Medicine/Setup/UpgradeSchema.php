<?php
namespace Apollo\Medicine\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
    {
        
        $setup->startSetup();
        
        if (version_compare($context->getVersion(), '2.0.7', '<')) {

            // Get module table
            $tableName = $setup->getTable('ordermedicine_data');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'type' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_VARBINARY,
                        'nullable' => false,
                        'length' => 55,
                        'default' => "Prescription",
                        'comment' => 'Prescription/Order',
                    ],
                    'prescriptions' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => 255,
                        'comment' => 'Prescriptions',
                    ],
                ];

                $connection = $setup->getConnection();

                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }

            }
        }
        if (version_compare($context->getVersion(), '2.0.8') < 0) {


            $medicineLogTable = $setup->getTable('ordermedicine_log');
            if ($setup->getConnection()->isTableExists($medicineLogTable) != true) {
                
                $table = $setup->getConnection()->newTable($medicineLogTable)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true
                        ],
                        'ID'
                    )
                     ->addColumn(
                        'order_id',
                        Table::TYPE_INTEGER,
                        null,
                        [ 
                            'nullable' => false
                            
                        ],
                        'ORDER ID'
                    )
                    ->addColumn(
                        'order_update_status',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false],
                        'Update order status'
                    )
                    ->addColumn(
                        'push_count',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false, 'default' => '0'],
                        'How many times order has been pushed to crm api'
                    )
                    ->addColumn(
                        'email_sent',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => true],
                        '1 => Sent, 0 => not sent'
                    )
                    ->addColumn(
                        'created_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                        'CREATED_AT'
                    )
                    ->addColumn(
                        'updated_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                        'UPDATED_AT'
                    )
                    ->setComment('Medicine update log table')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
                
                // create index also

                $setup->getConnection()->createTable($table);



            }

        }

        $setup->endSetup();
    }
}
