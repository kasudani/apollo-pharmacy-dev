<?php
namespace Apollo\ShopByBrand\Block;

use Magento\Store\Model\ScopeInterface;

/**
 * Main faq list block
 */
class Listbrand extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Group Collection
     */
    protected $_brandCollection;

    protected $_collection = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_brandHelper;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

	public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Framework\Registry $registry,
    \Magento\Framework\App\Http\Context $httpContext,
    \Ves\Brand\Helper\Data $brandHelper,
    \Ves\Brand\Model\Brand $brandCollection,
    array $data = []
	) { 
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_brandCollection = $brandCollection;
        $this->_brandHelper = $brandHelper;
        $this->_coreRegistry = $registry;
        $this->httpContext = $httpContext;

        parent::__construct($context);
	}

    public function getBrandCollection()
    {
        if(!$this->_collection) {
            $store = $this->_storeManager->getStore();
            $collection = $this->_brandCollection->getCollection()
            ->setOrder('name','ASC')
            ->addStoreFilter($store)
            ->addFieldToFilter('status',1);
            $this->_collection = $collection;
        }
        return $this->_collection;
    }

}
