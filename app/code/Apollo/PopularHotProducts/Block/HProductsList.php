<?php
namespace Apollo\PopularHotProducts\Block;

class HProductsList extends \Magento\Framework\View\Element\Template
{   

    protected $logger;

    protected $objectManager;

    protected $_curl;

    protected $_productCollectionFactory;

    /**
    * @param \Magento\Framework\View\Element\Template\Context $context
    * @param array $data
    */
    
    public function __construct(
        \Psr\Log\LoggerInterface $logger, 
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        
        array $data = []
    ) {
        $this->logger = $logger;
        $this->_curl = $curl;
        $this->objectManager = $objectManager;
        $this->_productCollectionFactory = $productCollectionFactory;  
        parent::__construct($context);
    }

    function getCatName() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->get('Magento\Framework\Registry')->registry('current_category');//get current category
        $catName = $category->getName();
        return strtolower($catName);        
    }
  
    function getProductBySku(){
        /*get cat name*/
        $categoryName = $this->getCatName();
        /*read product sku by json file*/
        $file_path = $this->getFileUrl('hot_sellers.json');
        $json = @file_get_contents($file_path);
        $jsonData = json_decode($json,true);
         $collection = '';
        if(!empty($jsonData)) {
            $skus = "";
            if(isset($jsonData[$categoryName])){
                $skus =  $jsonData[$categoryName];
                $collection = $this->_productCollectionFactory->create();
                $collection->addAttributeToSelect('*');
                $collection->addFieldToFilter('sku', array('in' => explode(",", $skus)));
                //$collection->setPageSize(52);
                return $collection;
            }
        }
        return $collection;
    }

    /**
     * get file url by file file name
     * @param $file
     * @return string
     */
    public function getFileUrl($file)
    {
        return $this->getFileBaseUrl(). $file;
    }

    /**
    * get file url
    * @return mixed
    */
    function getFileBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
}