<?php
namespace Apollo\Career\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use \Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;;

class Save extends \Magento\Backend\App\Action
{
    
    protected $uploaderFactory;
    
    protected $adapterFactory;
    
    protected $filesystem;

    public function __construct(
        Context $context,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem
    ) {
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if data sent
        $data = $this->getRequest()->getPostValue();
        
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            if(isset($_FILES['description']['name']) && $_FILES['description']['name'] != '') {
                try {

                    $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                    $target = $mediaDirectory->getAbsolutePath('career/documents');   
                    /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                    $uploader = $this->uploaderFactory->create(['fileId' => 'description']);
                    /** Allowed extension types */
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'docx', 'pdf']);
                    /** rename file name if already exists */
                    $uploader->setAllowRenameFiles(true);
                    /** upload file in folder "mycustomfolder" */
                    $result = $uploader->save($target);
                    if (!$result) {
                        throw new LocalizedException(
                            __('File cannot be saved to path: $1', $target)
                        );
                    }
                    $imagePath = $result['file'];
                    $data['description'] = $imagePath;
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                    $this->_getSession()->setFormData($data);
                    return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
                }
            }
            
            $model = $this->_objectManager->create('Apollo\Career\Model\Career')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This Career no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            // init model and set data

            $model->setData($data);

            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                $this->messageManager->addSuccess(__('You saved the Career.'));
                // clear previously saved data from session
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // save data in session
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                // redirect to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}