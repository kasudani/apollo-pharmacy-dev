<?php
namespace Apollo\Career\Controller\Index;

class Index extends \Apollo\Career\Controller\Index
{
    /**
     * Show Career List page
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
