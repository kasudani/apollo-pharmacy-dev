<?php
namespace Apollo\Career\Block;

use Magento\Store\Model\ScopeInterface;

/**
 * Main career list block
 */
class Listcareerregion extends \Magento\Framework\View\Element\Template
{
	protected $careerRegionFactory;

    const XML_PATH_ITEMS_PER_PAGE = 'apollo_career/apollo_career/items';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

	public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Apollo\Career\Model\Region $careerRegionFactory,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    array $data = []
	) { 
	    $this->careerRegionFactory = $careerRegionFactory;

        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

        parent::__construct($context);
	}

	public function getCareerRegions(){
		return $this->careerRegionFactory->getCollection();
	}


   /**
     * Set news collection
     */
    protected  function _construct()
    {
        parent::_construct();
        $collection = $this->careerRegionFactory->getCollection()
            ->addFilter('is_active', 1)
            ->setOrder('display_order', 'ASC');
        $this->setCollection($collection);
    }
 
   /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
           'Magento\Theme\Block\Html\Pager',
           'apollo.careerregion.list.pager'
        );
        $pager->setLimit($this->scopeConfig->getValue(self::XML_PATH_ITEMS_PER_PAGE, ScopeInterface::SCOPE_STORE))
            ->setShowAmounts(false)
            ->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
 
        return $this;
    }
 
   /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

}