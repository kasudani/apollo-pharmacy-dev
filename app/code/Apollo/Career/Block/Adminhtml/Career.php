<?php
namespace Apollo\Career\Block\Adminhtml;

class Career extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Apollo_Career';
        $this->_controller = 'adminhtml_career';
        $this->_headerText = __('Career List');
        $this->_addButtonLabel = __('Add New Career');
        parent::_construct();
    }
}
