<?php
namespace Apollo\Career\Model\ResourceModel\Region;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Apollo\Career\Model\Region','Apollo\Career\Model\ResourceModel\Region');
    }
}