<?php
namespace Apollo\Career\Model\ResourceModel\Career;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Apollo\Career\Model\Career','Apollo\Career\Model\ResourceModel\Career');
    }
}