<?php
namespace Apollo\Career\Model\ResourceModel;

class Region extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('apollo_career_regions','id');
    }
}