<?php
namespace Apollo\Career\Model;

class Career extends \Magento\Framework\Model\AbstractModel 
{
	/**#@+
     * Career's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
    /**#@-*/

	const CACHE_TAG = 'apollo_careers';
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Apollo\Career\Model\ResourceModel\Career');
    }

    /**
     * Prepare career's statuses.
     * Available event career_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }

}