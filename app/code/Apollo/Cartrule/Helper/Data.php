<?php

namespace Apollo\Cartrule\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_HOMEDELIVERY_CHARGE = 'cartrule/setting/home_charge';
    const XML_PATH_COD_CHARGE = 'cartrule/setting/cod_charge';
    const XML_PATH_COD_MIN_CHARGE = 'cartrule/setting/cod_min_order';
    const XML_PATH_HOMEDELIVERY_MINORDER = 'cartrule/setting/home_min_order';
    const XML_PATH_COD_MINORDER = 'cartrule/setting/cod_min_orde';
    const XML_PATH_FMCG_MINORDER = 'cartrule/setting/fmcg_min_order';

    protected $_objectManager;
    protected $_session;    

        
    public function __construct(
        \Magento\Checkout\Model\Session $session,
	\Magento\Framework\ObjectManagerInterface $objectmanager,
	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_session = $session;    
		$this->_objectManager = $objectmanager;    
		$this->scopeConfig = $scopeConfig;
		
     
    }
  
    function getOrderTypes($productId,$obj){
	    $productData = $obj->create('Magento\Catalog\Model\Product')->load($productId);
	    $categoryFactory = $obj->create('Magento\Catalog\Model\CategoryFactory');
	    $category_id_ary = $productData->getCategoryIds();
	    $category_names_array = array();
	    if(count($category_id_ary)){
		foreach($category_id_ary as $category_id){
		    $_category = $categoryFactory->create()->load($category_id);
		    $category_names_array[] = $_category->getName();
		}
	    }
	    $pharma = false;
	    $fmcg = false;
	    $order_type = "";
	    $category_names = array_unique($category_names_array);

	    if(in_array("PHARMA",$category_names))    {
		$pharma = true; $order_type = "Pharma";
	    }
	    if(in_array("FMCG",$category_names)) {
		$fmcg = true; $order_type = "Fmcg";
	    }

	    if($pharma == true && $fmcg == true){
		$order_type = "Both";
	    }
	    return (!empty($order_type) ? $order_type : "Fmcg");
	}
	
	 public function getShippingCharge()
	   {
	     $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
	     $home_charge = $this->scopeConfig->getValue(self::XML_PATH_HOMEDELIVERY_CHARGE, $storeScope);
	     $home_Min_order = $this->scopeConfig->getValue(self::XML_PATH_HOMEDELIVERY_MINORDER, $storeScope);

	     $order_type = array();
        $order_type[0]="";
	    $helper = $this->_objectManager->get('Apollo\Cartrule\Helper\Data');
            $objectManager = $this->_objectManager;
	    $total = $this->_session->getQuote()->getSubtotal();
	    if($total<$home_Min_order){		
	    $items = $this->_session->getQuote()->getAllVisibleItems();
	    foreach($items as $item){
			$pid = $item->getProductId();
			array_push($order_type,$helper->getOrderTypes($pid,$objectManager));
		}
		if(count(array_unique($order_type))>1){
			return $home_charge;
		}else{
			
			if($order_type[0]=='Pharma'){
				return 0;
			}else{
				return $home_charge;
			}
		}
	
		}else{
			return 0;
		}

	   }

	function getIsCheckoutPossible(){
	     $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
	     $fmcg_charge = $this->scopeConfig->getValue(self::XML_PATH_FMCG_MINORDER, $storeScope);
		 $cod_min_charge = $this->scopeConfig->getValue(self::XML_PATH_COD_MIN_CHARGE,$storeScope);

		$objectManager = $this->_objectManager;
		$order_type = array();
		$total = $this->_session->getQuote()->getSubtotal();		
        $order_type[0]="";

	        $items = $this->_session->getQuote()->getAllVisibleItems();
	    foreach($items as $item){
			$pid = $item->getProductId();
			array_push($order_type,$this->getOrderTypes($pid,$objectManager));
		}

		if(count(array_unique($order_type))>1){
			return true;
		}else{	
		
			if($order_type[0]=='Pharma'){
				if($total<$fmcg_charge){
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		
		}
		
	}

	function getIsCodCharge(){
	     /*$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
	     $fmcg_charge = $this->scopeConfig->getValue(self::XML_PATH_FMCG_MINORDER, $storeScope);
	     $cod_min_charge = $this->scopeConfig->getValue(self::XML_PATH_COD_MIN_CHARGE,$storeScope);

	     $methodTitle = $this->_session->getQuote()->getShippingAddress()->getShippingMethod(); //mphomedelivery_mphomedelivery;
	     if($methodTitle == 'mphomedelivery_mphomedelivery'){
			$objectManager = $this->_objectManager;
			$order_type = array();
			$total = $this->_session->getQuote()->getSubtotal();		
		        $items = $this->_session->getQuote()->getAllVisibleItems();
		    foreach($items as $item){
				$pid = $item->getProductId();
				array_push($order_type,$this->getOrderTypes($pid,$objectManager));
			}

			if(count(array_unique($order_type))>1){
				return false;
			}else{
				if(isset($order_type[0]) && $order_type[0]=='Pharma'){
						return false;
				}else if(isset($order_type[0]) && $order_type[0]=='Fmcg'){
                    return true;
                }else{
					if($total<=$cod_min_charge){
						return true;
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
			
		}*/

	} 

	



}
