<?php

namespace Apollo\Healingcard\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\UrlInterface as UrlInterface;

class HealingcardConfigProvider implements ConfigProviderInterface
{
    protected $methodCode = "healingcard";

    protected $method;
    
    protected $urlBuilder;

    public function __construct(PaymentHelper $paymentHelper, UrlInterface $urlBuilder) {
        $this->method = $paymentHelper->getMethodInstance($this->methodCode);
        $this->urlBuilder = $urlBuilder;
    }

    public function getConfig()
    {
        return $this->method->isAvailable() ? [
            'payment' => [
                'healingcard' => [
                    'redirectUrl' => $this->urlBuilder->getUrl('healingcard/Standard/Redirect', ['_secure' => true])
                ]
            ]
        ] : [];
    }

    protected function getRedirectUrl()
    {
        return $this->_urlBuilder->getUrl('paypal/ipn/');
    }
    
    protected function getFormData()
    {
        return $this->method->getRedirectUrl();
    }
}
