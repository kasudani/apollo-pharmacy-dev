define(
    [
    'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/payment/additional-validators'
    ],
    function ($, Component, placeOrderAction, selectPaymentMethodAction, customer, checkoutData, additionalValidators) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Apollo_Healingcard/payment/apollo'
            },
            placeOrder: function (data, event) {
                if (event) {
                    event.preventDefault();
                }
                var card_num = jQuery('#healingcard_mobile').val();
                var card_pin = jQuery('#healingcard_pinn').val();
                if(card_num == ""){
                    jQuery('#healingcard_mobile').after('<div for="card_num" generated="true" class="mage-error" id="card_num-error">This is a required field.</div>');
                    jQuery('.mage-error').delay(3000).fadeOut();
                    return false;
                }else if(card_num.length <16){
                    jQuery('#healingcard_mobile').after('<div for="card_num" generated="true" class="mage-error" id="card_num-error2">Pleae enter atleast 16 digit number.</div>');
                    jQuery('.mage-error').delay(3000).fadeOut();
                    return false;
                }else if(card_pin == ""){
                    jQuery('#healingcard_pinn').after('<div for="card_pin" generated="true" class="mage-error" id="card_pin-error">This is a required field.</div>');
                    jQuery('.mage-error').delay(3000).fadeOut();
                    return false;
                }else if(card_pin.length <6){
                    jQuery('#healingcard_pinn').after('<div for="card_pin" generated="true" class="mage-error" id="card_pin-error2">Pleae enter atleast 6 digit number.</div>');
                    jQuery('.mage-error').delay(3000).fadeOut();
                    return false;
                }
                var self = this,
                    placeOrder,
                    emailValidationResult = customer.isLoggedIn(),
                    loginFormSelector = 'form[data-role=email-with-possible-login]';
                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }
                if (emailValidationResult && this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                    placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                    $.when(placeOrder).fail(function () {
                        self.isPlaceOrderActionAllowed(true);
                    }).done(this.afterPlaceOrder.bind(this));
                    return true;
                }
                return false;
            },
            afterPlaceOrder: function () {
                //alert("hii");
                $.mage.redirect(window.checkoutConfig.payment.healingcard.redirectUrl);
            },
            checkd: function () {
                
                $.mage.cookies.set('test', $('#' +this.getCode() +'_test1').val(), {lifetime: -1});
                alert($.mage.cookies.get('status_id'));
                if ($.mage.cookies.get('status_id')==0) {
                    return 'none';
                } else {
                    alert('wrer');
                }
            },
            otpOrder: function () {
                //alert("reyerye");
                
                
                $.ajax({
                                showLoader: true,
                                url: window.checkoutConfig.payment.healingcard.oredirectUrl,
                                data: $.mage.cookies.set('mobile', $('#' +this.getCode() +'_mobile').val(), {lifetime: -1}),
                                type: "POST",
                                dataType: 'json',
                                 success: function(response){
                                     var statusd= response.status;
                                     var sessid= response.sess;
                                     //alert(response.status);
                                     //alert(statusd);
                                     //alert(sessid);
                                     if(statusd=='00'){
                                        $('#mobile_code').show();
                                        $('#mobile_pinn').show();
                                        $.mage.cookies.set('mobile', sessid, {lifetime: -1});
                                     }else{
                                         $('#message').text(response.message);
                                     }
                              }
                            });
            },
            getData: function() {
                //alert('werewt');
                //alert($('#' +this.getCode() +'_test1').val());
                $.mage.cookies.set('mobile', $('#' +this.getCode() +'_mobile').val(), {lifetime: -1});
                $.mage.cookies.set('pinn', $('#' +this.getCode() +'_pinn').val(), {lifetime: -1});
                
            return {
                'method': this.item.method,
                'additional_data': {
                    'mobile':  $('#' +this.getCode() +'_mobile').val(),
                    'pinn': $('#' +this.getCode() +'_pinn').val()
                }
            };
        }

        });
    }
    
);


