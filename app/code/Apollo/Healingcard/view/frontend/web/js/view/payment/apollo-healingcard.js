define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'healingcard',
                component: 'Apollo_Healingcard/js/view/payment/method-renderer/apollo-healingcard'
            }
        );
        return Component.extend({});
    }
 );