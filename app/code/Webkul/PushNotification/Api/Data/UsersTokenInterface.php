<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Api\Data;

interface UsersTokenInterface
{
    /**
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case.
     */
    const ID = 'entity_id';
    const TOKEN = 'token';
    const BROWSER = 'browser';
    const CREATED_AT = 'created_at';

    /**
     * get id
     * @return string
     */
    public function getId();

    /**
     * set id
     * @param int $id
     */
    public function setId($id);

    /**
     * get token
     * @return string
     */
    public function getToken();

    /**
     * set token
     * @param string $token
     */
    public function setToken($token);

    /**
     * get browser
     * @return string
     */
    public function getBrowser();

    /**
     * set browser
     * @param string $browser
     */
    public function setBrowser($browser);

    /**
     * get created time
     * @return timestamp
     */
    public function getcreatedAt();

    /**
     * set created time
     * @param timestamp $createdAt
     */
    public function setCreatedAt($createdAt);
}
