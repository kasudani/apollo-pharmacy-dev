<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Controller\Adminhtml\Templates;

use Webkul\PushNotification\Controller\Adminhtml\Templates;
use Webkul\PushNotification\Api\TemplatesRepositoryInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
 
class Edit extends Templates
{

    /** @var \Magento\Framework\View\Result\PageFactory  */
    protected $_resultPageFactory;

    /*
    TemplatesRepositoryInterface
     */
    protected $_templatesRepository;

    /**
     * [__construct description]
     * @param Filesystem                                 $filesystem
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param TemplatesRepositoryInterface               $templatesRepository
     * @param \Magento\Framework\Registry                $coreRegistry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Filesystem $filesystem,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        TemplatesRepositoryInterface $templatesRepository,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_templatesRepository = $templatesRepository;
        $this->_coreRegistry = $coreRegistry;
        $this->_filesystem = $filesystem;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    /**
     * Template edit action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $templateId = $this->initCurrentTemplate();
        $isExistingTemplate = (bool)$templateId;
        if ($isExistingTemplate) {
            try {
                $logoContainerDir = $this->_filesystem
                ->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('pushnotification/');
                $logoUrl = $this->_storeManager->getStore()
                ->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ).'pushnotification/';
                if (!is_dir($logoContainerDir)) {
                    mkdir($logoContainerDir, 0755, true);
                }
                $baseTmpPath = 'pushnotification';

                $templateData = [];
                $templateData['pushnotification_template'] = [];
                $templateObject = null;
                $templateObject = $this->_templatesRepository->getById($templateId);
                $result = $templateObject->getData();
                if (!empty($result)) {
                    $templateData['pushnotification_template'] = $result;
                    $templateData['pushnotification_template']['logo'] = [];
                    $templateData['pushnotification_template']['logo'][0] = [];
                    $templateData['pushnotification_template']['logo'][0]['name'] = $result['logo'];
                    $templateData['pushnotification_template']['logo'][0]['url'] =
                    $logoUrl.$result['logo'];
                    $filePath = $this->_mediaDirectory->getAbsolutePath(
                        $baseTmpPath
                    ).$result['logo'];
                    if (file_exists($filePath)) {
                        $templateData['pushnotification_template']['logo'][0]['size'] =
                        filesize($filePath);
                    } else {
                        $templateData['pushnotification_template']['logo'][0]['size'] = 0;
                    }
                    $templateData['pushnotification_template']['entity_id'] = $templateId;

                    $this->_getSession()->setTemplateFormData($templateData);
                } else {
                    $this->messageManager->addError(
                        __('Requested banner doesn\'t exist')
                    );
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath('pushnotification/templates/index');
                    return $resultRedirect;
                }
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while editing the banner.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('pushnotification/templates/index');
                return $resultRedirect;
            }
        }

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Webkul_PushNotification::templates');
        $this->prepareDefaultTemplateTitle($resultPage);
        $resultPage->setActiveMenu('Webkul_PushNotification::bannerimage');
        if ($isExistingTemplate) {
            $resultPage->getConfig()->getTitle()->prepend(__('Edit Template with id %1', $templateId));
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Template'));
        }
        return $resultPage;
    }

    /**
     * tempalate initialization
     *
     * @return string template id
     */
    protected function initCurrentTemplate()
    {
        $templateId = (int)$this->getRequest()->getParam('id');

        if ($templateId) {
            $this->_coreRegistry->register('template_id', $templateId);
        }

        return $templateId;
    }

    /**
     * Prepare banner default title
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return void
     */
    protected function prepareDefaultTemplateTitle(
        \Magento\Backend\Model\View\Result\Page $resultPage
    ) {
        $resultPage->getConfig()->getTitle()->prepend(__('Template Information'));
    }
}
