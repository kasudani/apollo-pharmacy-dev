<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Controller\Adminhtml\ManageUsers;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\PushNotification\Controller\Adminhtml\ManageUsers;
use Webkul\PushNotification\Model\ResourceModel\UsersToken\CollectionFactory;
use Webkul\PushNotification\Api\TemplatesRepositoryInterface;
use Webkul\PushNotification\Model\UsersToken;
use Magento\Store\Model\StoreManagerInterface;

class SendNotification extends ManageUsers
{
    const PATH = 'pushnotification/';

    const CHROME = 'Chrome';

    const FIREFOX = 'Firefox';

    /**
     * object of store manger class
     * @var storemanager
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /*
    CollectionFactory
     */
    protected $_collectionFactory;

    /*
    TemplatesRepositoryInterface
     */
    protected $_templatesRepository;

    /**
     * filter object of Filter
     * @var Filter
     */
    protected $_filter;

    /*
    UsersToken
     */
    protected $_usersToken;

    /*
    \Webkul\PushNotification\Helper\Data
     */
    protected $_helper;

    /**
     * [__construct description]
     * @param Context                              $context
     * @param Filter                               $filter
     * @param PageFactory                          $resultPageFactory
     * @param CollectionFactory                    $collectionFactory
     * @param TemplatesRepositoryInterface         $templatesRepository
     * @param StoreManagerInterface                $storemanager
     * @param \Webkul\PushNotification\Helper\Data $helper
     * @param UsersToken                           $usersToken
     */
    public function __construct(
        Context $context,
        Filter $filter,
        PageFactory $resultPageFactory,
        CollectionFactory $collectionFactory,
        TemplatesRepositoryInterface $templatesRepository,
        StoreManagerInterface $storemanager,
        \Webkul\PushNotification\Helper\Data $helper,
        UsersToken $usersToken
    ) {

        $this->_filter = $filter;
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
        $this->_collectionFactory = $collectionFactory;
        $this->_templatesRepository = $templatesRepository;
        $this->_storeManager = $storemanager;
        $this->_helper = $helper;
        $this->_usersToken = $usersToken;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

        $templateId = $this->getRequest()->getParam('entity_id');
        $model = $this->_filter;
        $collection = $model->getCollection($this->_collectionFactory->create());

        $response = $this->notificationData($templateId, $collection);

        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * manage notification data
     * @param  int $templateId
     * @param  object $collection
     * @return int|null
     */
    protected function notificationData($templateId, $collection)
    {
        $chrome = [];
        $mozila = [];
        foreach ($collection as $key => $user) {
            if ($user->getBrowser() == self::CHROME) {
                $chrome[]=$user->getToken();
            } elseif ($user->getBrowser() == self::FIREFOX) {
                $mozila[]=$user->getToken();
            }
            $user->setTemplateId($templateId)->save();
        }
        if (count($chrome)) {
            $response = $this->_helper->sendToChrome($chrome);
        }
        if (count($mozila)) {
            $response = $this->_helper->sendToFirefox($mozila);
        }
        $this->messageManager->addSuccess(
            __('Push notification(s) has been sent')
        );
    }
}
