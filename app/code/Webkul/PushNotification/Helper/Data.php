<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Helper;

/**
 * PushNotification data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const SEND_URL_CHROME = 'https://android.googleapis.com/gcm/send';

    const SEND_URL_FIREFOX = 'https://updates.push.services.mozilla.com/wpush/v1/';

    /*
    \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * [__construct description]
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Psr\Log\LoggerInterface              $logger
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
    
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_logger = $context->getLogger();
        parent::__construct($context);
    }

    /**
     * get api key
     * @return string
     */
    public function getApiKey()
    {
        return $this->_scopeConfig->getValue(
            'pushnotification/general/serverkey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get send id of api
     * @return string
     */
    public function getSenderId()
    {
        return $this->_scopeConfig->getValue(
            'pushnotification/general/senderid',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * send notification to chrome
     * @param  array $userIds
     * @return object
     */
    public function sendToChrome($userIds)
    {
            $apiKey = $this->getApiKey();
            $url = self::SEND_URL_CHROME;
            
            $headers = [
              'Authorization: key='.$apiKey,
              'Content-Type: application/json',
             /* 'TTL: 600000'*/
            ];

            if (count($userIds)==1) {
                $fields = [
                    'to'=>$userIds[0]
                ];
            } else {
                $fields = [
                  'registration_ids' => $userIds,
                ];
            }
            /**
             * push using curl request to FCM
             */
            $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === false) {
                $this->_logger->info(json_encode(curl_error($ch)));
            }
            $this->_logger->info(' chrome Result ');
            $this->_logger->info(json_encode($result));

            curl_close($ch);

            return $result;
    }

    /**
     * send to firefox
     * @param  array $userIds
     * @return object
     */
    public function sendToFirefox($userIds)
    {
        $url = self::SEND_URL_FIREFOX;
        $headers = [
          'Content-Type: application/json',
          'TTL: 600000'
        ];
        foreach ($userIds as $key => $value) {
            $finalUrl=$url.$value;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $finalUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            if ($result === false) {
                $this->_logger->info(json_encode(curl_error($ch)));
            }
            $this->_logger->info(' firefox Result ');
            $this->_logger->info(json_encode($result));

            curl_close($ch);
        }
        return $result;
    }
}
