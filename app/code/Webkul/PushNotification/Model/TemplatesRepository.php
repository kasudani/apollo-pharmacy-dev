<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Model;

use Webkul\PushNotification\Api\Data\TemplatesInterface;
use Webkul\PushNotification\Model\ResourceModel\Templates\Collection;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class TemplatesRepository implements \Webkul\PushNotification\Api\TemplatesRepositoryInterface
{
    /**
     * resource model
     * @var \Webkul\PushNotification\Model\ResourceModel\Ebayaccounts
     */
    protected $_resourceModel;

    public function __construct(
        TemplatesFactory $templatesFactory,
        \Webkul\PushNotification\Model\ResourceModel\Templates\CollectionFactory $collectionFactory,
        \Webkul\PushNotification\Model\ResourceModel\Templates $resourceModel
    ) {
        $this->_resourceModel = $resourceModel;
        $this->_templatesFactory = $templatesFactory;
        $this->_collectionFactory = $collectionFactory;
    }
    
    /**
     * get collection by template id
     * @param  int $templateId
     * @return object
     */
    public function getById($templateId)
    {
        $templateObject = $this->_templatesFactory->create()->load($templateId);
        return $templateObject;
    }
}
