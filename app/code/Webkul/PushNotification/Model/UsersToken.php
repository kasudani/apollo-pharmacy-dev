<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Model;

use Webkul\PushNotification\Api\Data\UsersTokenInterface;
use Magento\Framework\DataObject\IdentityInterface;

class UsersToken extends \Magento\Framework\Model\AbstractModel implements UsersTokenInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_pushnotification_users_token';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_pushnotification_users_token';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_pushnotification_users_token';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webkul\PushNotification\Model\ResourceModel\UsersToken');
    }

    /**
     * get id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }


    /**
     * set id
     * @param int $id
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }


    /**
     * get token
     * @return string
     */
    public function getToken()
    {
        return $this->getData(self::TOKEN);
    }


    /**
     * set token
     * @param string $token
     */
    public function setToken($token)
    {
        return $this->setData(self::TOKEN, $token);
    }

    /**
     * get browser
     * @return string
     */
    public function getBrowser()
    {
        return $this->getData(self::BROWSER);
    }

    /**
     * set browser
     * @param string $browser
     */
    public function setBrowser($browser)
    {
        return $this->setData(self::BROWSER, $browser);
    }

    /**
     * get created time
     * @return timestamp
     */
    public function getcreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * set created time
     * @param timestamp $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
