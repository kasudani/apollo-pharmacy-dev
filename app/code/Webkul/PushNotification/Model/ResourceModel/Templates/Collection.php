<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Model\ResourceModel\Templates;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(
            'Webkul\PushNotification\Model\Templates',
            'Webkul\PushNotification\Model\ResourceModel\Templates'
        );
    }
}
