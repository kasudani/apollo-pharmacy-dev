<?php

namespace Quadone\Prescription\Api\Data;
 
/**
 * Custom Data interface.
 * @api
 */
interface PrescriptionDataInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
	/**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
	const ID = 'id';
 
	const CUSTOMER_ID = 'customer_id';
	
	const PRODUCT_ID = 'product_id';
	
	const TEST = 'test';
	
	const NAME = 'name';
	
	const EMAIL = 'email';
	
	const PHONE = 'phone';
	
	const IMAGE1 = 'image1';
	
	const IMAGE2 = 'image2';
	
	const IMAGE3 = 'image3';
	
	
	/**#@-*/
 
	/**
	 * Get Id.
	 *
	 * @return int|null
	 */
	public function getId();
 
	/**
	 * Set Id.
	 *
	 * @param int $id
	 * @return $this
	 */
	public function setId($id = null);
 
	/**
	 * Get Customer Id.
	 *
	 * @return int|null
	 */
	public function getCustomerId();
 
	/**
	 * Set Customer Id.
	 *
	 * @param int $customerId
	 * @return $this
	 */
	public function setCustomerId($customerId = null);
	
	/**
	 * Get Product Id.
	 *
	 * @return int|null
	 */
	public function getProductId();
	
	/**
	 * Set Product Id.
	 *
	 * @param int $productId
	 * @return $this
	 */
	public function setProductId($productId = null);
	
	/**
	 * Get Product Id.
	 *
	 * @return string|null
	 */
	public function getTest();
	
	/**
	 * Set Product Id.
	 *
	 * @param string $test
	 * @return $this
	 */
	public function setTest($test= null);
	
	
	/**
	 * Get Name.
	 *
	 * @return string|null
	 */
	public function getName();
	
	/**
	 * Set Name.
	 *
	 * @param string $name
	 * @return $this
	 */
	public function setName($name= null);
	
	
	/**
	 * Get Email.
	 *
	 * @return string|null
	 */
	public function getEmail();
	
	/**
	 * Set Email.
	 *
	 * @param string $email
	 * @return $this
	 */
	public function setEmail($email= null);
	
	/**
	 * Get Phone.
	 *
	 * @return string|null
	 */
	public function getPhone();
	
	/**
	 * Set Phone.
	 *
	 * @param string $phone
	 * @return $this
	 */
	public function setPhone($phone= null);
	
	
	/**
	 * Get Image1.
	 *
	 * @return string|null
	 */
	public function getImage1();
	
	/**
	 * Set Image1.
	 *
	 * @param string $image1
	 * @return $this
	 */
	public function setImage1($image1= null);
	
	/**
	 * Get Image2.
	 *
	 * @return string|null
	 */
	public function getImage2();
	
	/**
	 * Set Image2.
	 *
	 * @param string $image2
	 * @return $this
	 */
	public function setImage2($image2= null);
	
	/**
	 * Get Image3.
	 *
	 * @return string|null
	 */
	public function getImage3();
	
	/**
	 * Set Image3.
	 *
	 * @param string $image3
	 * @return $this
	 */
	public function setImage3($image3= null);
	
	
	
}