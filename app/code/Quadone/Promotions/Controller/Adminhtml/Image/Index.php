<?php
/*
 * Quadone_Promotions

 * @category   Quadone
 * @package    Quadone_Promotions
 * @copyright  Copyright (c) 2017 Quadone
 * @license    https://github.com/quadone/magento2-sample-imageuploader/blob/master/LICENSE.md
 * @version    1.0.0
 */
namespace Quadone\Promotions\Controller\Adminhtml\Image;

use Quadone\Promotions\Controller\Adminhtml\Image;

class Index extends Image
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Quadone_Promotions::image');
        $resultPage->getConfig()->getTitle()->prepend(__('Promotions'));
        $resultPage->addBreadcrumb(__('Promotions'), __('Promotions'));
        return $resultPage;
    }
}
