<?php
/*
 * Quadone_Promotions

 * @category   Quadone
 * @package    Quadone_Promotions
 * @copyright  Copyright (c) 2017 Quadone
 * @license    https://github.com/quadone/magento2-sample-imageuploader/blob/master/LICENSE.md
 * @version    1.0.0
 */
namespace Quadone\Promotions\Controller\Adminhtml\Image;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Message\Manager;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Framework\View\Result\PageFactory;
use Quadone\Promotions\Api\ImageRepositoryInterface;
use Quadone\Promotions\Api\Data\ImageInterface;
use Quadone\Promotions\Api\Data\ImageInterfaceFactory;
use Quadone\Promotions\Controller\Adminhtml\Image;
use Quadone\Promotions\Model\Uploader;
use Quadone\Promotions\Model\UploaderPool;

class Save extends Image
{
    /**
     * @var Manager
     */
    protected $messageManager;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var ImageInterfaceFactory
     */
    protected $imageFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var UploaderPool
     */
    protected $uploaderPool;

    /**
     * Save constructor.
     *
     * @param Registry $registry
     * @param ImageRepositoryInterface $imageRepository
     * @param PageFactory $resultPageFactory
     * @param Date $dateFilter
     * @param Manager $messageManager
     * @param ImageInterfaceFactory $imageFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param UploaderPool $uploaderPool
     * @param Context $context
     */
    public function __construct(
        Registry $registry,
        ImageRepositoryInterface $imageRepository,
        PageFactory $resultPageFactory,
        Date $dateFilter,
        Manager $messageManager,
        ImageInterfaceFactory $imageFactory,
        DataObjectHelper $dataObjectHelper,
        UploaderPool $uploaderPool,
        Context $context
    ) {
        parent::__construct($registry, $imageRepository, $resultPageFactory, $dateFilter, $context);
        $this->messageManager   = $messageManager;
        $this->imageFactory      = $imageFactory;
        $this->imageRepository   = $imageRepository;
        $this->dataObjectHelper  = $dataObjectHelper;
        $this->uploaderPool = $uploaderPool;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
		
		foreach($data['image'] as $images){
			$imageurl = $images['url'];
		}
		if(isset($data['url'])){
            $name= $data['url'];
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam('image_id');
            if ($id) {
                $model = $this->imageRepository->getById($id);
				
            } else {
                unset($data['image_id']);
                $model = $this->imageFactory->create();
				$model->setUrl($imageurl);
                    $model->setLink($data['link']);
                    $model->setWidth($data['width']);
                    $model->setHeight($data['height']);
                    $model->setActived($data['actived']);
   			
            }

            try {
                $image = $this->getUploader('image')->uploadFileAndGetName('image', $data);
                $data['image'] = $image;

                $this->dataObjectHelper->populateWithArray($model, $data, ImageInterface::class);
                $this->imageRepository->save($model);

                /*code*/
                
                $datas = $this->getRequest()->getPostValue();
                $actived= $datas['actived'];
                $link= $datas['link'];
                
                foreach($datas['image'] as $imagedata){
                    $imagename=($imagedata['name']);
                    $imageurl=($imagedata['url']);
                    
                }
                
                $id = $this->getRequest()->getParam('image_id');
                if ($id) { 
                
                        $model = $this->imageRepository->getById($id);
                        $model->setActived($actived);
                        $model->setLink($link);
                        $model->setWidth($data['width']);
                        $model->setHeight($data['height']);
                        $model->save();
                        
                }
                    if ($id && $imagename == 'Image') { 
                        $model = $this->imageRepository->getById($id);
                        //$imageurl= $model->getImage();
                        $data = $imageurl;    
                        $linkurl = substr($data, strpos($data, "/images/image/") + 13);
                        if($imagename == 'Image'){
                            $model->setImage($linkurl);
                            $model->save();
                        }
                    } 
                    
                /*code end*/


                $this->messageManager->addSuccessMessage(__('You saved this promotion.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['image_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while saving the image:' . $e->getMessage())
                );
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['image_id' => $this->getRequest()->getParam('image_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param $type
     * @return Uploader
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }
}
