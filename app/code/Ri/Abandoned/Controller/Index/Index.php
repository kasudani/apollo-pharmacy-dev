<?php
namespace Ri\Abandoned\Controller\Index; 
use Magento\Framework\App\RequestInterface;
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_request;
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
    protected $reports;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $datetime;
    protected $_storeManager;
    protected $_productloader; 
     protected $_urlInterface; 
      protected $abandonedemailfactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context
        , \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
        , \Magento\Store\Model\StoreManagerInterface $storeManager
        , \Magento\Reports\Model\ResourceModel\Quote\CollectionFactory $reports
        , \Magento\Catalog\Model\ProductFactory $_productloader
        ,  \Magento\Framework\UrlInterface $urlInterface
        , \Magento\Quote\Model\QuoteFactory $quoteFactory
        , \Ri\Abandonedemail\Model\AbandonedemailFactory $abandonedemailfactory
                 )
		{
                 $this->abandonedemailfactory=$abandonedemailfactory;
		        $this->_transportBuilder = $transportBuilder;
		        $this->_storeManager = $storeManager;
		         $this->quoteFactory = $quoteFactory;
				$this->reports = $reports;
				$this->_productloader = $_productloader;
				 $this->_urlInterface = $urlInterface;
		        parent::__construct($context);
       }
 
    public function execute()
    {
        $store = $this->_storeManager->getStore()->getId();
         $collection = $this->reports->create();  
		         $collection->addFieldToFilter(
		            'customer_email',
		            ['neq' => 'NULL']
		       );    
		         $baseUrl= $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB); 

         	 foreach($collection as $row)
         	         
                     
                 {
                       $email=$row->getData("customer_email");

                       $model=$this->abandonedemailfactory->create();
                                   $col=$model->getCollection(); 
                                   $col->addFieldToFilter("email",$email);
                                   $col->addFieldToFilter("email_sent",1); 
                                  
                            if(count($col)==0){       
                     
         	 	     $updated_at=strtotime($row->getData('updated_at'));
                     $current=   strtotime(date("Y-m-d H:i:s"));
                    $diff=$current-$updated_at;
                       $diff= round($diff / (60 * 60 * 24));
         	 	     if($diff>3){
         	 	    $product=$this->_productloader->create()->load($row['entity_id']);
                     
		              $transport = $this->_transportBuilder->setTemplateIdentifier(8)
		            ->setTemplateOptions(['area' => 'frontend', 'store' =>1])
		            ->setTemplateVars(
		                [
		                    'store' => 1,
		                     'updated_at' =>$row->getData('updated_at'),
		                     'description'=>$product->getData("description"),
		                     'price'=>$product->getData('price'),
		                     'special_price'=>$product->getData('special_price'),
		                     'image'=>$baseUrl."pub/media/catalog/product".$product->getData('image'),
		                     'name'=>$product->getData('name'),
		                     'base_url'=>$baseUrl,
		                     'email'=>$row->getCustomerEmail(),
		                     'checkout_url'=>$baseUrl."checkout/cart",
                             'qty'=>$row->getData('items_qty'),
                             'grand_total'=>$row->getData('grand_total')
		                ]
		            )
		            ->setFrom('general')
		            // you can config general email address in Store -> Configuration -> General -> Store Email Addresses
		            ->addTo('varun@theretailinsights.com', 'Varun')
		            ->addTo('sivavarun1991@gmail.com', 'Siva Varun')
		            ->getTransport();
		             $transport->sendMessage();

                       $model=$this->abandonedemailfactory->create();
                       $model->setData('email',$row->getCustomerEmail());
                       $model->setData('email_sent',1);
                       $model->save();

		              die();
                     }
		            } 
           }
    }

}
