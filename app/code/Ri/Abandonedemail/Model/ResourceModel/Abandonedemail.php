<?php
namespace Ri\Abandonedemail\Model\ResourceModel;

class Abandonedemail extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('abandoned_cart_emails', 'id');
    }
}
