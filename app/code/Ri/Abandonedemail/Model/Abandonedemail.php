<?php
namespace Ri\Abandonedemail\Model;

class Abandonedemail extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ri\Abandonedemail\Model\ResourceModel\Abandonedemail');
    }
}
