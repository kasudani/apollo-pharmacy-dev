<?php

namespace Ri\Abandonedemail\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            $tableName = $installer->getTable('abandoned_cart_emails');
            // Check if the table already exists
            if ($installer->getConnection()->isTableExists($tableName) != true) {
                
                $table = $installer->getConnection()
                    ->newTable($tableName)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        11,
                        [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'email',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        55,
                        ['nullable' => false],
                        'Email Id'
                    )
                    ->addColumn(
                        'email_sent',
                        Table::TYPE_INTEGER,
                        50,
                        ['nullable' => false],
                        'Quantity'
                    )
                    ->setComment('Product Subscription created')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
                $installer->getConnection()->createTable($table);
            }
        }
        $setup->endSetup();
    }
}