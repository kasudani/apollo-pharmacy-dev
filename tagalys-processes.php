<?php

use \Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);

$obj = $bootstrap->getObjectManager();
$state = $obj->get('\Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$syncHelper = $obj->get('Tagalys\Sync\Helper\Sync');
$syncHelper->sync(500);
$syncHelper->cachePopularSearches();

