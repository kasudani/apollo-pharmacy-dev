<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

global $byblog_options;

// Page Variables
$byblog_layout = $byblog_options['byblog_archive_layout'];

get_header(); ?>

<div class="main-wrapper">
	<div class="cat-cover-box archive-cover-box">
		<div class="archive-cover-content">
            <?php
                the_archive_title( '<h1 class="category-title uppercase">', '</h1>' );
                the_archive_description( '<div class="taxonomy-description">', '</div>' );
            ?>
		</div>
	</div><!--."cat-cover-box-->
	<div id="page">
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
			<div class="archive-page">
				<div class="content-area archive-content-area">
                    <div class="content content-archive">
				        <div id="content" class="clearfix">
							<?php

                                // Post Position Count
                                $byblog_post_pos_count = 0;

								if (have_posts()) : while (have_posts()) : the_post();
								
								/*
								 * Include the post format-specific template for the content. If you want to
								 * use this in a child theme, then include a file called called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								
								get_template_part( 'template-parts/post-formats/content', get_post_format() );

                                $byblog_ad_count = ( $byblog_options['byblog_blog_ad_pos'] ? $byblog_options['byblog_blog_ad_pos'] : 1 );

                                $byblog_ad_code = $byblog_options['byblog_blog_archive_ad'];
                                if ( $byblog_ad_code ) {
                                    if( $byblog_post_pos_count == $byblog_ad_count && !is_paged() ) :
                                        echo '<div class="blog-ad post t-center">'. $byblog_ad_code .'</div>';
                                    endif;
                                }

                                $byblog_post_pos_count++;
							
								endwhile;
								
								else:
									// If no content, include the "No posts found" template.
									get_template_part( 'template-parts/post-formats/content', 'none' );

								endif;
							?>
						</div><!--.content-->
						<?php 
							// Previous/next page navigation.
							byblog_paging_nav();
						?>
					</div><!--.content-archive-->
				</div><!--#content-->
				<?php
					$byblog_layout_array = array(
                        'clayout',
                        'glayout',
                        'flayout'
                    );

                    if( !in_array( $byblog_layout, $byblog_layout_array ) ) {
                        get_sidebar();
                    }
				?>
			</div><!--.archive-page-->
<?php get_footer(); ?>