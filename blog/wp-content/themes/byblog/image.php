<?php
/**
 * The template for displaying image attachments
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

get_header();
global $byblog_options; ?>
<div class="main-wrapper">
	<div id="page">
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
			<div id="content" class="content-area single-content-area">
				<div class="content content-attachment">
					<?php
				        // Start the loop.
                        if (have_posts()) : while (have_posts()) : the_post();
                    ?>
                    <nav id="image-navigation" class="navigation clearfix image-navigation">
						<div class="nav-links">
							<div class="alignleft nav-previous"><?php previous_image_link( false, esc_html__( 'Previous Image', 'byblog' ) ); ?></div>
							<div class="alignright nav-next"><?php next_image_link( false, esc_html__( 'Next Image', 'byblog' ) ); ?></div>
						</div><!-- .nav-links -->
					</nav><!-- .image-navigation -->

                    <article <?php post_class(); ?>>
                        <div id="post-<?php the_ID(); ?>" class="post-box">
                            <?php get_template_part( 'template-parts/post-header' ); ?>
                            <?php echo wp_get_attachment_image( get_the_ID(), 'large' ); ?>
                            <?php echo byblog_excerpt( 'entry-caption' ); ?>
                            <div class="post-inner">
                                <div class="post-content entry-content single-post-content">
                                    <?php if( $byblog_options['byblog_below_title_ad'] != '' ) { ?>
                                        <div class="single-post-ad">
                                            <?php echo $byblog_options['byblog_below_title_ad']; ?>
                                        </div>
                                    <?php } ?>

                                    <?php the_content(); ?>

                                    <?php if( $byblog_options['byblog_below_content_ad'] != '' ) { ?>
                                        <div class="single-post-ad">
                                            <?php echo $byblog_options['byblog_below_content_ad']; ?>
                                        </div>
                                    <?php } ?>

                                    <?php byblog_wp_link_pages(); ?>

                                    <?php get_template_part( 'template-parts/post-footer-single' ); ?>
                                </div><!--.single-post-content-->
                            </div><!--.post-inner-->
                        </div><!--.post-box-->
                    </article>
                    
                    <?php endwhile;

                    else :
                        // If no content, include the "No posts found" template.
                        get_template_part( 'template-parts/post-formats/content', 'none' );

                    endif;
					
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					} ?>
				</div><!--.content-->
			</div><!--.content-area-->
			<?php
                if ( function_exists( 'rwmb_meta' ) ) {
                    $sidebar_position = rwmb_meta( 'byblog_layout', $args = array('type' => 'image_select'), get_the_ID() );
                } else {
                    $sidebar_position = '';
                }
				
				if ($byblog_options['byblog_single_layout'] != 'flayout') {
					if ($sidebar_position == 'left' || $sidebar_position == 'right' || $sidebar_position == 'default' || empty($sidebar_position)) {
						get_sidebar();
					}
				}
			?>
<?php get_footer();?>