<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

global $byblog_options;

// Page Variables
$byblog_layout = $byblog_options['byblog_layout'];

get_header(); ?>

<div class="main-wrapper">
	<div id="page">
		<div class="main-content <?php byblog_layout_class(); ?>">
			<div class="content-area">
				<div class="content content-page">
					<div class="content-detail">
						<div class="page-content">
							<div class="post-box error-page-content">
								<div class="error-head"><span><?php esc_html_e('Oops, This Page Could Not Be Found!','byblog'); ?></span></div>
								<div class="error-text"><?php esc_html_e('404','byblog'); ?></div>
								<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Back to Homepage','byblog'); ?></a></p>
								<p>
									<?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'byblog' ); ?>
								</p>
								<?php get_search_form(); ?>
							</div>
						</div><!--.page-content-->
					</div>
				</div><!--.content-->
			</div><!--.content-area-->
            <?php
                $byblog_layout_array = array(
                    'clayout',
                    'glayout',
                    'flayout'
                );

                if( !in_array( $byblog_layout, $byblog_layout_array ) ) {
                    get_sidebar();
                }
            ?>
		</div><!--.main-content-->
<?php get_footer();?>