<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

get_header();

global $byblog_options;

// Page Variables
$byblog_post_meta = $byblog_options['byblog_post_meta'];
$byblog_author = $byblog_options['byblog_post_meta_options']['1'];
$byblog_date = $byblog_options['byblog_post_meta_options']['2'];
$byblog_categories = $byblog_options['byblog_post_meta_options']['3'];
$byblog_post_icons = $byblog_options['byblog_post_meta_options']['6'];
$byblog_breadcrumbs = $byblog_options['byblog_breadcrumbs'];
$byblog_single_layout = $byblog_options['byblog_single_layout'];

if ( have_posts() ) : the_post();

    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_cover = rwmb_meta( 'byblog_post_cover_show', $args = array('type' => 'checkbox'), $post->ID );
        $sidebar_position = rwmb_meta( 'byblog_layout', $args = array('type' => 'image_select'), get_the_ID() );
    } else {
        $byblog_cover = '';
        $sidebar_position = '';
    }

if( $byblog_cover == '1' ) { ?>
	<div class="cover-box">
		<?php $byblog_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
		<div data-type="background" data-speed="3" class="cover-image" style="background-image: url( <?php echo esc_url( $byblog_url ); ?>);">
			<div class="cover-heading">
				<div class="slider-inner">
					 <?php
                        if( $byblog_post_meta == '1' ) { ?>
                            <div class="post-cats post-meta uppercase">
                                <?php
                                    if( $byblog_post_icons == '1' ) {
                                        get_template_part('template-parts/post-format-icons');
                                    }

                                    if( $byblog_categories == '1' ) {
                                        // Categories
                                        $categories = get_the_category();
                                        $separator = ' ';
                                        $output = '';
                                        if( $categories ){
                                            foreach( $categories as $category ) {
                                                $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s","byblog" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
                                            }
                                        echo trim( $output, $separator );
                                        }
                                    }
                                ?>
                            </div><?php
                        }
                    ?>
					<header>
						<h1 class="title entry-title">
							<?php the_title(); ?>
						</h1>
					</header><!--.header-->
					<?php if( $byblog_post_meta == '1' ) { ?>
                        <div class="post-meta-title post-meta">
                            <?php if( $byblog_author == '1' ) { ?>
                                <span class="post-author"><?php esc_html_e('By','byblog'); echo '&nbsp;'; the_author_posts_link(); ?></span>
                            <?php } ?>
                            <?php if( $byblog_date == '1' ) { ?>
                                <span class="post-date">
                                    <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php the_time(get_option( 'date_format' )); ?></time>
                                </span>
                            <?php }
                                edit_post_link( esc_html__( 'Edit', 'byblog' ), '<span class="edit-link">', '</span>' );
                            ?>
                        </div><!--.post-meta-title-->
                    <?php } ?>
				</div><!--.slider-inner-->
			</div><!--.cover-heading-->
		</div><!--.cover-image-->
	</div><!--.cover-box-->
<?php } ?>
<div class="main-wrapper">
	<div id="page">
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
			<div id="content" class="content-area single-content-area">
				<div class="content content-single">
					<?php if( $byblog_breadcrumbs == '1' ) { ?>
						<div class="breadcrumbs" itemtype="http://schema.org/BreadcrumbList" itemscope="">
							<?php byblog_breadcrumb(); ?>
						</div>
					<?php }

                    $byblog_single_enabled_elements = $byblog_options['byblog_single_post_layout']['enabled'];

                    if ( empty( $byblog_single_enabled_elements ) ) {
                        $byblog_single_enabled_elements = array (
                            'post-content' => 'Post Content',
                            'post-navigation' => 'Post Navigation',
                            'author-box' => 'Author Box',
                            'related-posts' => 'Related Posts'
                        );
                    }

                    rewind_posts(); while (have_posts()) : the_post();

                    foreach( $byblog_single_enabled_elements as $byblog_single_element_key => $byblog_single_element ) {
                        get_template_part( 'template-parts/'.$byblog_single_element_key );
                    }

                    endwhile;

                    else :
                        // If no content, include the "No posts found" template.
                        get_template_part( 'template-parts/post-formats/content', 'none' );

                    endif;
					
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					} ?>
				</div>
			</div>
			<?php 				
				if ( $byblog_single_layout != 'flayout' ) {
					if ( $sidebar_position == 'left' || $sidebar_position == 'right' || $sidebar_position == 'default' || empty($sidebar_position) ) {
						get_sidebar();
					}
				}
// Footer
get_footer(); ?>