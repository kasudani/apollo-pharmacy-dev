== Changelog ==

= 1.1 =
* Fixed: Compatibility issue with Jetpack Infinite Scroll module
* Added: Authors list template to show blog contributors
* Added: Demo importer

= 1.0 =
* Initial Release
