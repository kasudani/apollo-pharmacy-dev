<?php
/*
Template Name: Authors List
*/
?>
<?php global $byblog_options; ?>
<?php get_header(); ?>

<div class="main-wrapper">
	<div id="page">
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
			<div class="content-area">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>		
                    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                        <div class="post-box clearfix">
                            <header>
                                <h1 class="title page-title"><?php the_title(); ?></h1>
                            </header>
                            <?php
                                $byblog_all_users = get_users( 'orderby=post_count&order=DESC' );

                                $byblog_authors = array();

                                // Remove subscribers from the list as they won't write any articles
                                foreach( $byblog_all_users as $currentUser )
                                {
                                    if( !in_array( 'subscriber', $currentUser->roles ) )
                                    {
                                        $byblog_authors[] = $currentUser;
                                    }
                                }

                                foreach( $byblog_authors as $user ) { ?>
                                    <div class="author-box single-box">
                                        <div class="author-box-avtar">
                                            <?php echo get_avatar( $user->user_email, '128' ); ?>
                                        </div>
                                        <div class="author-info-container">
                                            <div class="author-info">
                                                <div class="author-head">
                                                    <h5 class="author-name">
                                                        <?php echo $user->display_name; ?>
                                                    </h5>
                                                </div>
                                                
                                                <p class="author-descrption">
                                                    <?php echo get_user_meta($user->ID, 'description', true); ?>
                                                </p>
                                                
                                                <p class="author-posts">
                                                    <a href="<?php echo get_author_posts_url( $user->ID ); ?>">
                                                        <?php esc_html_e('View Author Posts','byblog'); ?>
                                                    </a>
                                                </p>

                                                <div class="author-social">
                                                        <?php
                                                        $website = $user->user_url;
                                                        if( $user->user_url != '' ) { ?>
                                                            <span class="author-website">
                                                                <a class="fa fa-link" href="<?php echo $user->user_url; ?>">
                                                                    <span class="screen-reader-text"><?php esc_attr_e('Website','byblog'); ?></span>
                                                                </a>
                                                            </span>
                                                            <?php
                                                        }

                                                        $twitter = get_user_meta($user->ID, 'twitter', true);
                                                        if( $twitter != '' ) { ?>
                                                            <span class="author-twitter">
                                                                <a class="fa fa-twitter" href="<?php echo esc_url( $twitter ); ?>">
                                                                    <span class="screen-reader-text"><?php esc_attr_e('Twitter','byblog'); ?></span>
                                                                </a>
                                                            </span>
                                                            <?php
                                                        }

                                                        $facebook = get_user_meta($user->ID, 'facebook', true);
                                                        if( $facebook != '' ) { ?>
                                                            <span class="author-fb">
                                                                <a class="fa fa-facebook" href="<?php echo esc_url( $facebook ); ?>">
                                                                    <span class="screen-reader-text"><?php esc_attr_e('Facebook','byblog'); ?></span>
                                                                </a>
                                                            </span>
                                                            <?php
                                                        }

                                                        $google = get_user_meta($user->ID, 'googleplus', true);
                                                        if( $google != '' ) { ?>
                                                            <span class="author-gp">
                                                                <a class="fa fa-google-plus" href="<?php echo esc_url( $google ); ?>">
                                                                    <span class="screen-reader-text"><?php esc_attr_e('Google+','byblog'); ?></span>
                                                                </a>
                                                            </span>
                                                            <?php
                                                        }

                                                        $linkedin = get_user_meta($user->ID, 'linkedin', true);
                                                        if( $linkedin != '' ) { ?>
                                                            <span class="author-linkedin">
                                                                <a class="fa fa-linkedin" href="<?php echo esc_url( $linkedin ); ?>">
                                                                    <span class="screen-reader-text"><?php esc_attr_e('Linkedin','byblog'); ?></span>
                                                                </a>
                                                            </span>
                                                            <?php
                                                        }

                                                        $pinterest = get_user_meta($user->ID, 'pinterest', true);
                                                        if( $pinterest != '' ) { ?>
                                                            <span class="author-pinterest">
                                                                <a class="fa fa-pinterest" href="<?php echo esc_url( $pinterest ); ?>">
                                                                    <span class="screen-reader-text"><?php esc_attr_e('Pinterest','byblog'); ?></span>
                                                                </a>
                                                            </span>
                                                            <?php
                                                        }

                                                        $dribbble = get_user_meta($user->ID, 'dribbble', true);
                                                        if( $dribbble != '' ) { ?>
                                                            <span class="author-dribbble">
                                                                <a class="fa fa-dribbble" href="<?php echo esc_url( $dribbble ); ?>">
                                                                    <span class="screen-reader-text"><?php esc_attr_e('Dribbble','byblog'); ?></span>
                                                                </a>
                                                            </span>
                                                            <?php
                                                        }
                                                    ?>
                                                </div><!--.author-social-->
                                            </div><!--.author-info-->
                                        </div><!--.author-info-container-->
                                    </div><!--.author-box--->
                                <?php
                                }
                            ?>
                    </div>
                </article>
                <?php endwhile; ?>
                <?php else : ?>
                    <div class="post">
                        <div class="single-page-content error-page-content">
                            <p><strong><?php esc_html_e('Nothing Found', 'byblog'); ?></strong></p>
                            <?php get_search_form(); ?>
                        </div><!--noResults-->
                    </div>
                <?php endif; ?>
			</div><!--content-area-->
			<?php
                if ( function_exists( 'rwmb_meta' ) ) {
                    $sidebar_position = rwmb_meta( 'byblog_layout', $args = array('type' => 'image_select'), get_the_ID() );
                } else {
                    $sidebar_position = '';
                }
				
				if ($byblog_options['byblog_single_layout'] != 'flayout') {
					if ($sidebar_position == 'left' || $sidebar_position == 'right' || $sidebar_position == 'default' || empty($sidebar_position)) {
						get_sidebar();
					}
				}
			?>
		</div><!--.main-->
<?php get_footer(); ?>