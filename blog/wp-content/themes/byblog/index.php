<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

global $byblog_options;

// Page Variables
$byblog_slider = $byblog_options['byblog_slider'];
$byblog_featured_posts = $byblog_options['byblog_home_featured_posts'];
$byblog_latest_posts = $byblog_options['byblog_home_latest_posts'];
$byblog_layout = $byblog_options['byblog_layout'];

get_header(); ?>

<div class="main-wrapper clearfix">
    <?php
        // Include Featured Slider
        if( $byblog_slider == '1' ) {
            if( !is_paged() ) {
                get_template_part('template-parts/slider');
            }
        }
    ?>
	<div id="page">
        <?php
            if( $byblog_featured_posts == '1' ) {
                if( !is_paged() ) {
                    get_template_part('template-parts/featured-posts');
                }
            }
        ?>
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
            <div class="content-area home-content-area">
                <div class="content content-home">
                    <div id="content" class="clearfix">
                        <?php
                            if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                            elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                            else { $paged = 1; }

                            if( $byblog_latest_posts == '1' ) {
                                $byblog_recent_cats = $byblog_options['byblog_home_latest_cat'];
                                $byblog_recent_cat = implode(",", $byblog_recent_cats);
                                $args = array(
                                    'cat'   => $byblog_recent_cat,
                                    'paged' => $paged
                                );
                            } else {					
                                $args = array(
                                    'paged' => $paged
                                );
                            }

                            // The Query
                            query_posts( $args );

                            $byblog_post_pos_count = 0;

                            if (have_posts()) : while (have_posts()) : the_post();

                            /*
                             * Include the post format-specific template for the content. If you want to
                             * use this in a child theme, then include a file called called content-___.php
                             * (where ___ is the post format) and that will be used instead.
                             */

                            get_template_part( 'template-parts/post-formats/content', get_post_format() );

                            $byblog_ad_count = ( $byblog_options['byblog_blog_ad_pos'] ? $byblog_options['byblog_blog_ad_pos'] : 1 );
                    
                            $byblog_ad_code = $byblog_options['byblog_blog_archive_ad'];
                            if ( $byblog_ad_code ) {
                                if( $byblog_post_pos_count == $byblog_ad_count && !is_paged() ) :
                                    echo '<div class="blog-ad post t-center">'. $byblog_ad_code .'</div>';
                                endif;
                            }

                            $byblog_post_pos_count++;

                            endwhile;

                            else:
                                // If no content, include the "No posts found" template.
                                get_template_part( 'template-parts/post-formats/content', 'none' );

                            endif;
                        ?>
                    </div><!--content-->
                    <?php 
                        // Previous/next page navigation.
                        byblog_paging_nav();
                    ?>
                </div><!--content-page-->
            </div><!--content-area-->
            <?php
                $byblog_layout_array = array(
                    'clayout',
                    'glayout',
                    'flayout'
                );

                if( !in_array( $byblog_layout, $byblog_layout_array ) ) {
                    get_sidebar();
                }
            ?>
<?php get_footer(); ?>