<?php

/**
 * Deactivate Demo Mode of Redux
 * -------------------------------------------------
 * this will deactive demo mode of reduxframework plugin
 * and will not display and addvertisement
 */
if ( ! function_exists( 'redux_disable_dev_mode_plugin' ) ) {
    function redux_disable_dev_mode_plugin( $redux ) {
        if ( $redux->args['opt_name'] != 'byblog_options' ) {
            $redux->args['dev_mode'] = false;
        }
    }

    add_action( 'redux/construct', 'redux_disable_dev_mode_plugin' );
}

/**
 * Demo Importer
 * -------------------------------------------------
 * Include one click demo importer to Redux Framework
 */
require_once( trailingslashit( get_template_directory() ) . 'inc/theme-options/extensions-loader.php' );

/**
 * Redux Framework Settings
 * -------------------------------------------------
 * Check if $byblog_options is not already set and config file exists
 * then include the config file
 */
if ( !isset( $byblog_options ) && file_exists( get_template_directory() . '/inc/theme-options/theme-options.php' ) ) {
	require_once( get_template_directory() . '/inc/theme-options/theme-options.php' );
}

/**
 * Custom Template Functions
 * -------------------------------------------------
 * Inlcude some custom template functions for this theme
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Font Awesome Icons List
 * -------------------------------------------------
 * This file is needed to add font icons available for navigation menu and widgets
 */
require get_template_directory() . '/template-parts/font-icons.php';

/**
 * Sets up the content width value based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1108;
}

/*-----------------------------------------------------------------------------------*/
/* Sets up theme defaults and registers the various WordPress features that
/* UBlog supports.
/*-----------------------------------------------------------------------------------*/
function byblog_theme_setup() {
	global $byblog_options;
    
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	
    /*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
        'gallery',
        'link',
        'quote',
        'video',
        'image',
        'status',
        'audio',
        'aside'
    ) );
	
	// Register WordPress Custom Menus
	add_theme_support( 'menus' );
	register_nav_menu( 'main-menu', esc_html__( 'Main Menu', 'byblog' ) );
    
    /*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ByBlog, use a find and replace
	 * to change 'byblog' to the name of your theme in all the template files
	 */
    load_theme_textdomain( 'byblog', get_template_directory() . '/languages' );
	
    /*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
    // Entry Image Sizes
    if ( !empty( $byblog_options['byblog_entry_img_width'] ) ) {
        $byblog_entry_img_width = $byblog_options['byblog_entry_img_width'];
    } else {
        $byblog_entry_img_width = '835';
    }
    if ( !empty( $byblog_options['byblog_entry_img_height'] ) ) {
        $byblog_entry_img_height = $byblog_options['byblog_entry_img_height'];
    } else {
        $byblog_entry_img_height = '412';
    }
    
    // Widgets Thumbnail Sizes
    if ( !empty( $byblog_options['byblog_widgets_img_width'] ) ) {
        $byblog_widgets_img_width = $byblog_options['byblog_widgets_img_width'];
    } else {
        $byblog_widgets_img_width = '68';
    }
    if ( !empty( $byblog_options['byblog_widgets_img_height'] ) ) {
        $byblog_widgets_img_height = $byblog_options['byblog_widgets_img_height'];
    } else {
        $byblog_widgets_img_height = '68';
    }
    
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 200, 200, true );
	add_image_size( 'byblog-carousel', 390, 565, true );
	add_image_size( 'byblog-slider', 1170, 585, true );
	add_image_size( 'byblog-featured', $byblog_entry_img_width, $byblog_entry_img_height, true );
	add_image_size( 'byblog-featuredthumb', 295, 310, true );
	add_image_size( 'byblog-featuredgrid', 401, 316, true );
	add_image_size( 'byblog-widgetthumb', $byblog_widgets_img_width, $byblog_widgets_img_height, true );
	add_image_size( 'byblog-widththumb-full', 304, 150, true );
    
    /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
    
    /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
	) );

    if ( !empty( $byblog_options['byblog_emoji'] ) ) {
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
    }
}
add_action( 'after_setup_theme', 'byblog_theme_setup' );

/**
 * Post Meta Boxes
 * -------------------------------------------------
 * 'Meta Box' plugin is required to use these meta boxes
 */
include_once get_template_directory() . '/inc/meta-boxes.php';

require get_template_directory() .'/inc/category-meta/class-usage.php';

/**
 * Google Web fonts
 * -------------------------------------------------
 * Inlcude google fonts for this theme. If redux framework is not activated then include
 * Open Sans, Source Sans Pro, PT Serif and Droid Serif fonts else include only
 * Droid Serif font
 */
if ( ! class_exists( 'ReduxFramework' ) ) {
    function byblog_fonts_url() {
        $fonts_url = '';

        /* Translators: If there are characters in your language that are not
        * supported by Open Sans, translate this to 'off'. Do not translate
        * into your own language.
        */
        $open_sans = _x( 'on', 'Open Sans font: on or off', 'byblog' );
        
        /* Translators: If there are characters in your language that are not
        * supported by Source Sans Pro, translate this to 'off'. Do not translate
        * into your own language.
        */
        $source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'byblog' );
        
        /* Translators: If there are characters in your language that are not
        * supported by PT Serif, translate this to 'off'. Do not translate
        * into your own language.
        */
        $pt_serif = _x( 'on', 'PT Serif font: on or off', 'byblog' );
        
        /* Translators: If there are characters in your language that are not
        * supported by Droid Serif, translate this to 'off'. Do not translate
        * into your own language.
        */
        $droid_serif = _x( 'on', 'Droid Serif font: on or off', 'byblog' );

        if ( 'off' !== $source_sans_pro || 'off' !== $open_sans ) {
            $font_families = array();

            if ( 'off' !== $open_sans ) {
            $font_families[] = 'Open Sans:700italic,400,800,600';
            }

            if ( 'off' !== $source_sans_pro ) {
            $font_families[] = 'Source Sans Pro:400,400italic,600,600italic,700italic,800,600';
            }

            if ( 'off' !== $pt_serif ) {
            $font_families[] = 'PT Serif:400,400italic,700,700italic';
            }

            if ( 'off' !== $droid_serif ) {
            $font_families[] = 'Droid Serif:400,400italic,700,700italic';
            }

            $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
            );

            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }

        return esc_url_raw( $fonts_url );
    }
} else {
    function byblog_fonts_url() {
        $fonts_url = '';

        /* Translators: If there are characters in your language that are not
        * supported by Droid Serif, translate this to 'off'. Do not translate
        * into your own language.
        */
        $droid_serif = _x( 'on', 'Droid Serif font: on or off', 'byblog' );
        
        if ( 'off' !== $droid_serif ) {
            $font_families[] = 'Droid Serif:400,400italic,700,700italic';

            $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
            );

            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }

        return esc_url_raw( $fonts_url );
    }
}

function byblog_add_google_fonts() {
    wp_enqueue_style( 'byblog-google-fonts', byblog_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'byblog_add_google_fonts' );

/*-----------------------------------------------------------------------------------*/
/*	Add Stylesheets
/*-----------------------------------------------------------------------------------*/
function byblog_stylesheets() {
	global $byblog_options;
	wp_enqueue_style( 'byblog-style', get_stylesheet_uri() );
    
    // Default colors if redux framework is not active
    $byblog_theme_defaults = array(
		'color_scheme'            => '#f58d88',
		'body_background_color'   => '#fff',
    );
	
	// Color Scheme
	if (is_single() || is_page()) {
        if ( function_exists( 'rwmb_meta' ) ) {
            $byblog_single_bg_color = rwmb_meta( 'byblog_post_bg_color', $args = array('type' => 'color'), get_the_ID() );
            $byblog_single_bg_images = rwmb_meta( 'byblog_post_bg_img', $args = array('type' => 'image_advanced'), get_the_ID() );
                foreach ($byblog_single_bg_images as $byblog_single_bg_img_id) {
                    $byblog_single_bg_img_code = wp_get_attachment_image_src($byblog_single_bg_img_id['ID'],'full');
                    $byblog_single_bg_img = $byblog_single_bg_img_code[0];
                }
            $byblog_single_repeat = rwmb_meta( 'byblog_post_bg_repeat', $args = array('type' => 'select'), get_the_ID() );
            $byblog_single_position = rwmb_meta( 'byblog_post_bg_position', $args = array('type' => 'select'), get_the_ID() );
            $byblog_single_size = rwmb_meta( 'byblog_post_bg_size', $args = array('type' => 'select'), get_the_ID() );
            $byblog_single_attachment = rwmb_meta( 'byblog_post_bg_attachment', $args = array('type' => 'select'), get_the_ID() );
        } else {
            $byblog_single_bg_color = '';
            $byblog_single_bg_images = '';
            $byblog_single_repeat = '';
            $byblog_single_position = '';
            $byblog_single_size = '';
            $byblog_single_attachment = '';
        }
	}
	
	// Color Scheme
	$byblog_color_scheme = "";
	if ( $byblog_options['byblog_color_scheme'] != '' ) {
		$byblog_color_scheme = $byblog_options['byblog_color_scheme'];
	} else {
        $byblog_color_scheme = $byblog_theme_defaults['color_scheme'];
    }
	
	// Background Color
	if ( !empty( $byblog_options['byblog_body_bg']['background-color'] ) ) {
        $byblog_background_color = $byblog_options['byblog_body_bg']['background-color'];
    } else {
        $byblog_background_color = $byblog_theme_defaults['body_background_color'];
    }
	
	// Background Pattern
	$byblog_background_img = get_template_directory_uri(). '/images/bg.png';
	$byblog_bg_repeat = 'repeat';
	$byblog_bg_attachment = 'scroll';
	$byblog_bg_position = '0 0';
	$byblog_bg_size = 'inherit';
	if (is_single() || is_page()) {
		if (!empty($byblog_single_bg_img)) { // Single Background Image
			$byblog_background_img = $byblog_single_bg_img;
			$byblog_bg_repeat = $byblog_single_repeat;
			$byblog_bg_position = $byblog_single_position;
			$byblog_bg_size = $byblog_single_size;
			$byblog_bg_attachment = $byblog_single_attachment;
		} elseif (!empty($byblog_single_bg_color)) { // Single Background Color
            if ( function_exists( 'rwmb_meta' ) ) {
                $byblog_background_color = rwmb_meta( 'byblog_post_bg_color', $args = array('type' => 'color'), get_the_ID() );
            } else {
                $byblog_background_color = '';
            }
		} elseif (!empty($byblog_options['byblog_body_bg']['background-image'])) { // Body Custom Background Pattern
			$byblog_background_img = $byblog_options['byblog_body_bg']['background-image'];
			$byblog_bg_repeat = $byblog_options['byblog_body_bg']['background-repeat'];
			$byblog_bg_attachment = $byblog_options['byblog_body_bg']['background-attachment'];
			$byblog_bg_size = $byblog_options['byblog_body_bg']['background-size'];
			$byblog_bg_position = $byblog_options['byblog_body_bg']['background-position'];
		} elseif ($byblog_options['byblog_bg_pattern'] != 'nopattern') { // Body Default Background Pattern
			$byblog_background_img = get_template_directory_uri(). "/images/".$byblog_options['byblog_bg_pattern'].".png";
			$byblog_bg_repeat = 'repeat';
			$byblog_bg_attachment = 'scroll';
			$byblog_bg_position = '0 0';
		}
	} elseif (!empty($byblog_options['byblog_body_bg']['background-image'])) { // Body Custom Background Pattern
		$byblog_background_img = $byblog_options['byblog_body_bg']['background-image'];
		$byblog_bg_repeat = $byblog_options['byblog_body_bg']['background-repeat'];
		$byblog_bg_attachment = $byblog_options['byblog_body_bg']['background-attachment'];
		$byblog_bg_size = $byblog_options['byblog_body_bg']['background-size'];
		$byblog_bg_position = $byblog_options['byblog_body_bg']['background-position'];
	} elseif ($byblog_options['byblog_bg_pattern'] != 'nopattern') { // Body Default Background Pattern
		$byblog_background_img = get_template_directory_uri(). "/images/".$byblog_options['byblog_bg_pattern'].".png";
		$byblog_bg_repeat = 'repeat';
		$byblog_bg_attachment = 'scroll';
		$byblog_bg_position = '0 0';
		$byblog_bg_size = 'inherit';
	}
	
	// Layout Options
	$byblog_custom_css = '';

	// Navigation Menu
	if ( !empty( $byblog_options['byblog_nav_link_color']['regular'] ) ) {
		$byblog_nav_link_color = $byblog_options['byblog_nav_link_color']['regular'];
	} else {
        $byblog_nav_link_color = '#000';
    }
	if ( !empty( $byblog_options['byblog_nav_link_color']['hover'] ) ) {
		$byblog_nav_link_hover_color = $byblog_options['byblog_nav_link_color']['hover'];
	} else {
        $byblog_nav_link_hover_color = '#f58d88';
    }
	
	// Hex to RGB
	$byblog_hex = $byblog_color_scheme;
	list($byblog_r, $byblog_g, $byblog_b) = sscanf($byblog_hex, "#%02x%02x%02x");
    
    // Magnific Popup
	if ( $byblog_options['byblog_lightbox'] != '' ) {
		wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(), null );
	}
	
	// Font-Awesome CSS
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), null );
	
	// Custom CSS
	if ( $byblog_options['byblog_custom_css'] != '' ) {
		$byblog_custom_css = $byblog_options['byblog_custom_css'];
	}
	
	$byblog_css = "
	body { background-color:{$byblog_background_color}; background-image:url({$byblog_background_img}); background-repeat:{$byblog_bg_repeat}; background-attachment:{$byblog_bg_attachment}; background-position:{$byblog_bg_position}; background-size:{$byblog_bg_size} }
	.search-button, .tagcloud a:hover, .pagination .current, .pagination a:hover, .post-format-quote, .subscribe-widget input[type='submit'], #wp-calendar caption, #wp-calendar td#today, .comment-form .submit, .wpcf7-submit, .off-canvas-search, .widget li:hover:before, .product-categories li:hover:before, .post-tags a:hover, .recent-posts li .thumbnail-big:before, .owl-controls .owl-dot.active span, .owl-controls .owl-dot:hover span, .jetpack_subscription_widget input[type=submit], .post-content ol > li:before, .post-content ol > li ol li:before, .content-page ol > li:before, .content-page ol > li ol li:before, .post-type i, .read-more a:hover, .more-link:hover, .featured-section .post-cats, .popular-posts .thumbnail-big:before { background-color:{$byblog_color_scheme}; }
    .woocommerce ul.products li.product .button, .woocommerce div.product form.cart .button, .woocommerce .widget_price_filter .price_slider_amount .button, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current { background:{$byblog_color_scheme} !important; }
	a, a:hover, .title a:hover, .post-author a, .sidebar a:hover, .sidebar-small-widget a:hover, .category-title span, .meta a:hover, .post-meta a:hover, .post-cats, .reply:hover i, .reply:hover a, .edit-post a, .error-text, .primary-color, .post-comments .fa, .comment-reply-link, .post-content ul li:before, .content-page ul li:before, .featured-posts a:hover .title { color:{$byblog_color_scheme}; }
	.nav-menu a { color:{$byblog_nav_link_color}}
	.nav-menu .current-menu-parent > a, .nav-menu .current-page-parent > a, .nav-menu .current-menu-item > a, .nav-menu a:hover { color:{$byblog_nav_link_hover_color}; }
	.post-content blockquote, .tagcloud a:hover .post blockquote, .pagination .current, .pagination a:hover, .comment-reply-link:hover { border-color:{$byblog_color_scheme} !important; }
	#wp-calendar th, .carousel .owl-item:hover .slider-inner { background: rgba({$byblog_r},{$byblog_g},{$byblog_b}, 0.8) } {$byblog_custom_css}";
	wp_add_inline_style( 'byblog-style', $byblog_css );
	
    // RTL
	if ($byblog_options['byblog_rtl'] == '1') {
		wp_enqueue_style( 'byblog-rtl', get_template_directory_uri() . '/rtl.css' );
	}
	
    // Responsive
	if ($byblog_options['byblog_responsive_layout'] == '1') {
		wp_enqueue_style( 'byblog-responsive', get_template_directory_uri() . '/css/responsive.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'byblog_stylesheets' );

/*-----------------------------------------------------------------------------------*/
/*	Add JavaScripts
/*-----------------------------------------------------------------------------------*/
function byblog_scripts() {
    global $byblog_options;
    
	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
	
	// Sticky Menu
	if ( $byblog_options['byblog_sticky_menu'] == '1' ) {
		wp_enqueue_script( 'byblog-stickymenu', get_template_directory_uri() . '/js/stickymenu.js', array( 'jquery' ), '1.0', true );
	}
    
    // Instafeed
    if ( $byblog_options['byblog_show_instagram_photos'] == '1') {
        
        $byblog_instagram_user_id = $byblog_options['byblog_instagram_user_id'];
        $byblog_instagram_accesstoken = $byblog_options['byblog_instagram_accesstoken'];
        
        wp_register_script( 'byblog-instafeed', get_template_directory_uri() . '/js/instafeed.min.js', array( 'jquery' ), '1.0', true );
        
        wp_localize_script(
            'byblog-instafeed',
            'byblog_instafeed',
            array(
                'user_id' => ( empty( $byblog_instagram_user_id ) ) ? NULL : intval( $byblog_instagram_user_id ),
                'access_token' => ( empty( $byblog_instagram_accesstoken ) ) ? '' : $byblog_instagram_accesstoken,
            )
        );
        
        wp_enqueue_script( 'byblog-instafeed' );
    }
    
    // Sticky Sidebar
    if ( $byblog_options['byblog_sticky_sidebar'] == '1' ) {
        wp_enqueue_script( 'byblog-sticky-sidebar', get_template_directory_uri() . '/js/theia-sticky-sidebar.js', array( 'jquery' ), '1.4.0', true );
    }
	
	// Fitvids
    wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/js/fitvids.js', array( 'jquery' ), '1.1', true );
	
	// Magnific Popup
    if ( $byblog_options['byblog_lightbox'] == '1' ) {
        wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array( 'jquery' ), '1.1.0', true );
    }
	
	// Owl Carousel
    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '2.0', true );
    
    if ( $byblog_options['byblog_rtl'] == '1' ) {
        wp_enqueue_script( 'byblog-sliders-rtl', get_template_directory_uri() . '/js/sliders-rtl.js', array( 'jquery' ), '1.0', true );
    } else {
        wp_enqueue_script( 'byblog-sliders', get_template_directory_uri() . '/js/sliders.js', array( 'jquery' ), '1.0', true );
    }
    
    wp_enqueue_script( 'byblog-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '1.0', true );
	
	// Required jQuery Scripts
    wp_register_script( 'byblog-theme-scripts', get_template_directory_uri() . '/js/theme-scripts.js', array( 'jquery' ), null, true );
    wp_localize_script(
        'byblog-theme-scripts',
        'byblog_themescripts',
        array(
            'mailchimp_valid_email' => esc_html__( 'A valid email address must be provided.', 'byblog' ),
            'subscribing' => esc_html__( 'Subscribing...', 'byblog' ),
            'mailchimp_error_msg' => esc_html__( 'Sorry. Unable to subscribe. Please try again later.', 'byblog' ),
            'mailchimp_form_url_msg' => esc_html__( 'Please add mailchimp form URL for this widget', 'byblog' ),
            'mailchimp_already_subscribed_msg' => esc_html__( 'You\'re already subscribed. Thank you.', 'byblog' ),
            'mailchimp_confirm_msg' => esc_html__( 'Thank you!<br>You must confirm the subscription in your inbox.', 'byblog' ),
        )
    );
    wp_enqueue_script( 'byblog-theme-scripts' );
}
add_action( 'wp_enqueue_scripts', 'byblog_scripts' );

/*-----------------------------------------------------------------------------------*/
/*	Add Admin Scripts
/*-----------------------------------------------------------------------------------*/
function byblog_admin_scripts() {
    wp_enqueue_script( 'byblog-admin-scripts', get_template_directory_uri() . '/js/admin-scripts.js', array( 'jquery' ), '1.0', true );
	
    wp_enqueue_script( 'byblog-select2js', get_template_directory_uri() . '/js/select2js.js', array( 'jquery' ), '1.0', true );
	
	wp_enqueue_style( 'byblog-select2css', get_template_directory_uri() . '/css/select2css.css' );
	
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	
	wp_enqueue_style( 'byblog-adminstyle', get_template_directory_uri() . '/css/adminstyle.css' );
    
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker' );
}
add_action( 'admin_enqueue_scripts', 'byblog_admin_scripts' );

/*-----------------------------------------------------------------------------------*/
/*	Load Widgets
/*-----------------------------------------------------------------------------------*/
// Theme Functions
include get_template_directory() . "/inc/widgets/widget-ad125.php"; // 125x125 Ad Widget
include get_template_directory() . "/inc/widgets/widget-ad160.php"; // 160x600 Ad Widget
include get_template_directory() . "/inc/widgets/widget-ad300.php"; // 300x250 Ad Widget
include get_template_directory() . "/inc/widgets/widget-author-info.php"; // Author Info Widget
include get_template_directory() . "/inc/widgets/widget-fblikebox.php"; // Facebook Like Box
include get_template_directory() . "/inc/widgets/widget-flickr.php"; // Flickr Widget
include get_template_directory() . "/inc/widgets/widget-pinterest.php"; // Pinterest Widget
include get_template_directory() . "/inc/widgets/widget-popular-posts.php"; // Popular Posts
include get_template_directory() . "/inc/widgets/widget-cats.php"; // Categories Widget
include get_template_directory() . "/inc/widgets/widget-cat-posts.php"; // Category Posts
include get_template_directory() . "/inc/widgets/widget-random-posts.php"; // Random Posts
include get_template_directory() . "/inc/widgets/widget-recent-posts.php"; // Recent Posts
include get_template_directory() . "/inc/widgets/widget-social.php"; // Social Widget
include get_template_directory() . "/inc/widgets/widget-subscribe.php"; // Subscribe Widget
include get_template_directory() . "/inc/widgets/widget-tabs.php"; // Tabs Widget
include get_template_directory() . "/inc/widgets/widget-tweets.php"; // Tweets Widget
include get_template_directory() . "/inc/widgets/widget-video.php"; // Video Widget
include get_template_directory() . "/inc/nav-walker.php"; // Nav Walker Class
include get_template_directory() . "/inc/nav-walker-mobile.php"; // Nav Walker Class

/*-----------------------------------------------------------------------------------*/
/*	Register Theme Widgets
/*-----------------------------------------------------------------------------------*/
function byblog_widgets_init() {
	global $byblog_options;
	register_sidebar(array(
		'name'          => esc_html__( 'Primary Sidebar', 'byblog' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Main sidebar of the theme.', 'byblog' ),
		'before_widget' => '<div class="widget sidebar-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	$byblog_footer_columns = ( !empty( $byblog_options['byblog_footer_columns'] ) ? $byblog_options['byblog_footer_columns']: '' );
	if ( $byblog_footer_columns == 'footer_4' ) {
		$byblog_sidebars = array(1, 2, 3, 4);
		foreach( $byblog_sidebars as $byblog_number ) {
			register_sidebar(array(
                'name'          => sprintf( esc_html__( 'Footer %s','byblog' ), $byblog_number ),
				'id'            => 'footer-' . $byblog_number,
                'description'   => esc_html__( 'This widget area appears on footer of theme.', 'byblog' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title uppercase"><span>',
				'after_title'   => '</span></h3>'
			));
		}
	} elseif ( $byblog_footer_columns == 'footer_3' ) {
		$byblog_sidebars = array(1, 2, 3);
		foreach( $byblog_sidebars as $byblog_number ) {
			register_sidebar(array(
                'name'          => sprintf( esc_html__( 'Footer %s','byblog' ), $byblog_number ),
                'id'            => 'footer-' . $byblog_number,
                'description'   => esc_html__( 'This widget area appears on footer of theme.', 'byblog' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title uppercase">',
                'after_title'   => '</h3>'
			));
		}
	} elseif ( $byblog_footer_columns == 'footer_2' ) {
		$byblog_sidebars = array(1, 2);
		foreach( $byblog_sidebars as $byblog_number ) {
			register_sidebar(array(
                'name'          => sprintf( esc_html__( 'Footer %s','byblog' ), $byblog_number ),
                'id'            => 'footer-' . $byblog_number,
                'description'   => esc_html__( 'This widget area appears on footer of theme.', 'byblog' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title uppercase">',
                'after_title'   => '</h3>'
			));
		}
	} else {
		register_sidebar(array(
            'name'          => esc_html__( 'Footer', 'byblog' ),
            'id'            => 'footer',
            'description'   => esc_html__( 'This widget area appears on footer of theme.', 'byblog' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title uppercase">',
            'after_title'   => '</h3>'
		));
	}
    
    // Custom Sidebars
    $byblog_custom_sidebar_count = '';
    if ( !empty( $byblog_options['byblog_custom_sidebars'] ) ) {
        $byblog_custom_sidebar_count = $byblog_options['byblog_custom_sidebars'];
    }
    if ( $byblog_custom_sidebar_count > 0 ) {
        for ( $j = 1; $j <= $byblog_custom_sidebar_count; $j++ ) {
            register_sidebar(array(
                'name'          => esc_html__( 'Custom Sidebar', 'byblog' ) . ' ' . $j,
                'id'            => 'custom-sidebar-' . $j,
                'description'   => esc_html__( 'This widget area appears on footer of theme.', 'byblog' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title uppercase"><span>',
                'after_title'   => '</span></h3>'
            ));
        }
    }
}
add_action( 'widgets_init', 'byblog_widgets_init' );

function byblog_custom_sidebars() {
    global $byblog_options;
    
    $byblog_a=array( 'default' => esc_html__('Default Sidebar','byblog'), 'sidebar-1' => esc_html__('Primary Sidebar','byblog') );
    
    $byblog_footer_columns = (!empty($byblog_options['byblog_footer_columns']) ? $byblog_options['byblog_footer_columns']: '' );
    
    if ($byblog_footer_columns == 'footer_4') {
        
        $byblog_a=array( 'default' => esc_html__('Default Sidebar','byblog'), 'sidebar-1' => esc_html__('Primary Sidebar','byblog'), 'footer-1' => esc_html__('Footer 1','byblog'), 'footer-2' => esc_html__('Footer 2','byblog'), 'footer-3' => esc_html__('Footer 3','byblog'), 'footer-4' => esc_html__('Footer 4','byblog') );
        
    } elseif ($byblog_footer_columns == 'footer_3') {
        array_push( $byblog_a, esc_html__('Footer 1','byblog'), esc_html__('Footer 2','byblog'), esc_html__('Footer 3','byblog') );
    } elseif ($byblog_footer_columns == 'footer_2') {
        array_push( $byblog_a, esc_html__('Footer 1','byblog'), esc_html__('Footer 2','byblog') );
    } else {
        array_push( $byblog_a, esc_html__('Footer','byblog') );
    }   
    
    $byblog_custom_sidebar_count = '';
    if ( !empty( $byblog_options['byblog_custom_sidebars'] ) ) {
        $byblog_custom_sidebar_count = $byblog_options['byblog_custom_sidebars'];
    }
    
    for ( $j = 1; $j <= $byblog_custom_sidebar_count; $j++ ) {
    
        array_push($byblog_a, esc_html__('Custom Sidebar','byblog') . '&nbsp;' . $j);
        
    }
    
    return $byblog_a;
}

/*-----------------------------------------------------------------------------------*/
/*	Breadcrumb
/*-----------------------------------------------------------------------------------*/
function byblog_breadcrumb() {
	if (!is_home()) {
		echo '<span itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement" class="breadcrumb-item">';
		echo '<a itemprop="item" href="';
		echo home_url();
		echo '"> <i class="fa fa-home"></i>';
        echo '<span itemprop="name">';
		echo esc_html__( 'Home','byblog' );
		echo "</span>";
		echo "</a>";
		echo "</span>";
		if (is_category() || is_single()) {
			echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
            echo '<span itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement" class="breadcrumb-item">';
            echo '<span itemprop="item">';
			the_category(' &bull; ');
            echo "</span>";
            echo "</span>";
			if (is_single()) {
				echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
                echo '<span itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement" class="breadcrumb-item">';
                echo '<span itemprop="item">';
				the_title();
                echo "</span>";
                echo "</span>";
			}
		} elseif (is_page()) {
			echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
			echo the_title();
		}
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Schema for Single Post
/*-----------------------------------------------------------------------------------*/
function byblog_post_schema() {
    
    if ( is_singular( 'post' ) ) {
        global $post;
        global $byblog_options;
        
        $byblog_logo_url = ( ! empty( $byblog_options['byblog_logo']['url'] ) ? $byblog_options['byblog_logo']['url'] : '' );
        
        if ( has_post_thumbnail( $post->ID ) && !empty( $byblog_logo_url ) ) {

            $byblog_post_image  = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

            $data = array(
                "@context"          => "http://schema.org",
                "@type"             => "BlogPosting",
                "author"            => array(
                                        "@type" => "Person",
                                        "name"  => get_the_author_meta('display_name', $post->post_author),
                                    ),
                "datePublished"     => get_the_time( 'c', $post->ID ),
                "headline"          => $post->post_title,
                "image"             => array(
                                        "@type"  => "ImageObject",
                                        "url"    => $byblog_post_image[0],
                                        "width"  => $byblog_post_image[1],
                                        "height" => $byblog_post_image[2]
                                    ),
                "publisher"         => array(
                                        "@type" => "Organization",
                                        "name"  => get_bloginfo( 'name' ),
                                        "logo"  => array(
                                            "@type"  => "ImageObject",
                                            "url"    => esc_url( $byblog_logo_url ),
                                        )
                                    ),
                "dateModified"      => get_post_modified_time('c', '', $post->ID),
                "mainEntityOfPage"  => array(
                                        "@type" => "WebPage",
                                        "@id"   => get_permalink( $post->ID )
                                    ),
            );

            echo '<script type="application/ld+json">' . PHP_EOL;
            echo wp_json_encode( $data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT ) , PHP_EOL;
            echo '</script>' . PHP_EOL;
        }
    }
}
add_action( 'wp_head', 'byblog_post_schema' );

/*-----------------------------------------------------------------------------------*/
/*	Comments Callback
/*-----------------------------------------------------------------------------------*/
function byblog_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);
?>
	<li <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body" itemscope itemtype="http://schema.org/UserComments">
	<?php endif; ?>
	<div class="comment-author vcard">
		<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment->comment_author_email, 60 ); ?>
		<?php printf(__('<span class="fn" itemprop="creator" itemscope itemtype="http://schema.org/Person"><span itemprop="name">%s</span></span>','byblog'), get_comment_author_link()) ?>

		<span class="reply alignright uppercase">
		<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => esc_html__('Reply','byblog')))) ?>
		</span>
	</div>
	<?php if ($comment->comment_approved == '0') : ?>
		<em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.','byblog') ?></em>
		<br />
	<?php endif; ?>

	<div class="comment-meta commentmetadata">
        <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<time itemprop="commentTime" datetime="<?php echo esc_attr( get_comment_date( 'c' ) ); ?>">
            <?php
                printf( esc_html__('%1$s at %2$s','byblog'), get_comment_date(),  get_comment_time())
            ?>
        </time>
        </a>
        <?php edit_comment_link(esc_html__('(Edit)','byblog'),'  ','' ); ?>
	</div>

	<div class="comment-text" itemprop="commentText">
		<?php comment_text() ?>
	</div>
	</div>
<?php }


/*-----------------------------------------------------------------------------------*/
/*	Pagination
/*-----------------------------------------------------------------------------------*/
function byblog_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '') {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages) { $pages = 1; }
     }

     if(1 != $pages) {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}

/*-----------------------------------------------------------------------------------*/
/*	Custom wp_link_pages
/*-----------------------------------------------------------------------------------*/
function byblog_wp_link_pages( $args = '' ) {
	$defaults = array(
		'before'           => '<div class="pagination" id="post-pagination"><p class="page-links-title">' . esc_html__( 'Pages:', 'byblog' ) . '</p>',
		'after'            => '</div>',
		'text_before'      => '',
		'text_after'       => '',
		'next_or_number'   => 'number', 
		'nextpagelink'     => esc_html__( 'Next page', 'byblog' ),
		'previouspagelink' => esc_html__( 'Previous page', 'byblog' ),
		'pagelink'         => '%',
		'echo'             => 1
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				$output .= ' ';
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= _wp_link_page( $i );
				else
					$output .= '<span class="current-post-page">';

				$output .= $text_before . $j . $text_after;
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= '</a>';
				else
					$output .= '</span>';
			}
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $previouspagelink . $text_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $nextpagelink . $text_after . '</a>';
				}
				$output .= $after;
			}
		}
	}

	if ( $echo )
		echo $output;

	return $output;
}

/*-----------------------------------------------------------------------------------*/
/*	WooCommerce Support
/*-----------------------------------------------------------------------------------*/

// Check whether WooCommerce is activated
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
	}
}

//include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_woocommerce_activated() ) {
    global $byblog_options;

    //Declare WooCommerce support
	add_action( 'after_setup_theme', 'woocommerce_support' );
    function woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	// WooCommerce
	function byblog_woocommerce_style() {
		wp_enqueue_style('woocommerce', get_template_directory_uri() . '/css/woocommerce.css');
	}
	add_action('wp_enqueue_scripts', 'byblog_woocommerce_style', '999');

	add_action('woocommerce_before_main_content', 'byblog_wrapper_start', 10);
	add_action('woocommerce_after_main_content', 'byblog_wrapper_end', 10);

	function byblog_wrapper_start() {
        global $byblog_options;
        echo '<div class="main-wrapper clearfix">';
        echo '<div id="page">';
        echo '<div class="main-content clearfix ';
        echo $byblog_options['byblog_shop_layout'];
        echo '">';
        echo '<div class="content-area">';
        echo '<div class="content woo-page">';
	}

	function byblog_wrapper_end() {
		echo '</div>';
		echo '</div>';
	}

	// Register Shop Archive and Single Product Sidebar
	function byblog_woo_widgets_init() {
		register_sidebar(array(
			'name'          => esc_html__('Shop Primary Sidebar','byblog'),
			'description'   => esc_html__( 'Appears on shop archive and single pages.', 'byblog' ),
			'id'            => 'shop-sidebar',
			'before_widget' => '<div id="%1$s" class="widget sidebar-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		));
	}
	add_action( 'widgets_init', 'byblog_woo_widgets_init' );

    // Change number of products per row to 3
	add_filter('loop_shop_columns', 'byblog_loop_columns');
	if ( !function_exists('byblog_loop_columns') ) {
		function byblog_loop_columns() {
            return 3; // 3 products per row
		}
	}
    
    // Change number of products displayed per page
    if ( !function_exists( 'byblog_woo_products_count' ) ) {
        function byblog_woo_products_count() {

            global $byblog_options;

            $number_of_products = ( ! empty( $byblog_options['byblog_shop_products'] ) ? $byblog_options['byblog_shop_products'] : '9' );

            $products_return = 'return ' . $number_of_products . ';';

            add_filter( 'loop_shop_per_page', create_function( '$cols', $products_return ), 20 );
        }
        add_action( 'init', 'byblog_woo_products_count' );
    }
    
    /**
	 * Ajaxify cart viewer so it updates when an item is added
	 * -------------------------------------------------------
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX
	 *
	 */
    add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
    function woocommerce_header_add_to_cart_fragment( $fragments ) {
        ob_start();
        ?>
        <a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart','byblog' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'byblog' ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a> 
        <?php

        $fragments['a.cart-contents'] = ob_get_clean();

        return $fragments;
    }

	/**
	 * Hook in on activation
	 */
	global $pagenow;
	if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'byblog_woocommerce_image_dimensions', 1 );
	 
	/**
	 * Define image sizes
	 */
	function byblog_woocommerce_image_dimensions() {
		$catalog = array(
			'width' 	=> '300',	// px
			'height'	=> '310',	// px
			'crop'		=> 0 		// true
		);
	 
		$single = array(
			'width' 	=> '400',	// px
			'height'	=> '400',	// px
			'crop'		=> 0 		// true
		);
	 
		$thumbnail = array(
			'width' 	=> '120',	// px
			'height'	=> '120',	// px
			'crop'		=> 0 		// false
		);
	 
		// Image sizes
		update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
		update_option( 'shop_single_image_size', $single ); 		// Single product image
		update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
	}
	
	/**
	 * WooCommerce Extra Feature
	 * --------------------------
	 *
	 * Change number of related products on product page
	 * Set your own value for 'posts_per_page'
	 *
	 */ 
	function byblog_woo_related_products_limit() {
	  global $product;
		
		$args = array(
			'post_type'        	  => 'product',
			'no_found_rows'    	  => 1,
			'posts_per_page'   	  => 3,
			'ignore_sticky_posts' => 1,
			'post__not_in'        => array($product->id)
		);
		return $args;
	}
	add_filter( 'woocommerce_related_products_args', 'byblog_woo_related_products_limit' );
    
	// Redefine woocommerce_output_related_products()
	function woocommerce_output_related_products() {
        $args = array(
            'columns' => 3,
        );
		woocommerce_related_products( $args ); // Display 3 products in rows of 3
	}
}

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function byblog_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'byblog_widget_tag_cloud_args' );

/*-----------------------------------------------------------------------------------*/
/*	TGM Plugin Activation
/*-----------------------------------------------------------------------------------*/
require_once( get_template_directory(). '/inc/class-tgm-plugin-activation.php' );
require_once( get_template_directory(). '/inc/required-class.php' );

/*-----------------------------------------------------------------------------------*/
/*	Automatic Theme Updates
/*-----------------------------------------------------------------------------------*/
global $byblog_options;
$byblog_envato_username = (!empty($byblog_options['byblog_envato_user_name']) ? $byblog_options['byblog_envato_user_name']: '' );
$byblog_envato_api_key = (!empty($byblog_options['byblog_envato_api_key']) ? $byblog_options['byblog_envato_api_key']: '' );
$byblog_envato_author_name = 'Simrandeep Singh';

load_template( get_template_directory() . '/inc/wp-theme-upgrader/envato-wp-theme-updater.php' );
Envato_WP_Theme_Updater::init( $byblog_envato_username, $byblog_envato_api_key, $byblog_envato_author_name );
?>