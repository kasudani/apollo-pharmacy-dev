<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

global $byblog_options;

get_header(); ?>

<div class="main-wrapper" itemscope itemtype="http://schema.org/SearchResultsPage">
	<div class="cat-cover-box archive-cover-box">
		<div class="archive-cover-content">
            <h1 class="category-title uppercase">
                <?php esc_html_e('Search Results for', 'byblog'); echo '&nbsp;"<span itemprop="about">' . get_search_query() . '</span>"'; ?>
            </h1>
		</div>
	</div><!--."cat-cover-box-->
	<div id="page">
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
		<div class="content-area home-content-area">
            <div id="content" class="content content-search clearfix">
                <?php
                    // Start the Loop.
                    if (have_posts()) : while (have_posts()) : the_post();

                    /*
                     * Include the post format-specific template for the content. If you want to
                     * use this in a child theme, then include a file called called content-___.php
                     * (where ___ is the post format) and that will be used instead.
                     */
                    get_template_part( 'template-parts/post-formats/content', get_post_format() );

                    endwhile;

                    else:
                        // If no content, include the "No posts found" template.
                        get_template_part( 'template-parts/post-formats/content', 'none' );

                    endif;
                ?>
            </div><!--content-->
            <?php 
                // Previous/next page navigation.
                byblog_paging_nav();
            ?>
		</div><!--content-area-->
		<?php
			$byblog_layout_array = array(
				'clayout',
				'glayout',
				'flayout'
			);

			if(!in_array($byblog_options['byblog_search_layout'],$byblog_layout_array)) {
				get_sidebar();
			}
		?>
<?php get_footer(); ?>