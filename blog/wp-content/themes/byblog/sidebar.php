<?php global $byblog_options; ?>
<aside class="sidebar" id="sidebar" itemscope itemtype="http://schema.org/WPSideBar" role="complementary">
    <?php
        $byblog_sidebar_id = '';
        $byblog_sidebar_home = $byblog_options['byblog_sidebar_home'];
        $byblog_sidebar_post = $byblog_options['byblog_sidebar_post'];
        $byblog_sidebar_page = $byblog_options['byblog_sidebar_page'];
        $byblog_sidebar_search = $byblog_options['byblog_sidebar_search'];
        $byblog_sidebar_category = $byblog_options['byblog_sidebar_category'];
        $byblog_sidebar_author = $byblog_options['byblog_sidebar_author'];
        $byblog_sidebar_archive = $byblog_options['byblog_sidebar_archive'];

        if ( is_home() || is_front_page() || is_page_template('template-popular.php') ) {
            // If Homepage
            if ( !empty( $byblog_sidebar_home ) ) {
                $byblog_sidebar_id =  $byblog_sidebar_home;
            }
        } elseif ( is_single() ) {
            // If Single Post
            if ( function_exists( 'rwmb_meta' ) ) {
                $byblog_custom_sidebar_label = rwmb_meta( 'byblog_custom_sidebar', $args = array('type' => 'radio'), $post->ID );
            } else {
                $byblog_custom_sidebar_label = '';
            }

            $byblog_sidebar_array = array ( 'default', 'sidebar-1', 'footer-1', 'footer-2', 'footer-3', 'footer-4' );

            if ( $byblog_custom_sidebar_label == '0' || $byblog_custom_sidebar_label == '' || $byblog_custom_sidebar_label == 'default' ) {
                $byblog_sidebar_id = 'sidebar-1';
            } elseif ( in_array ( $byblog_custom_sidebar_label, $byblog_sidebar_array ) ) {
                $byblog_sidebar_id = $byblog_custom_sidebar_label;
            } else {
                if ( function_exists( 'rwmb_meta' ) ) {
                    $byblog_custom_sidebar_label = rwmb_meta( 'byblog_custom_sidebar', $args = array('type' => 'radio'), $post->ID );
                } else {
                    $byblog_custom_sidebar_label = '';
                }
                $byblog_custom_sidebar_num = '';
                $byblog_custom_sidebar_num = $byblog_custom_sidebar_label + 1;
                $byblog_sidebar_id = 'Custom Sidebar' . ' ' . $byblog_custom_sidebar_num;
            }

            if ( ( $byblog_sidebar_id == 'sidebar-1' && $byblog_custom_sidebar_label == 'default' ) || $byblog_sidebar_id == 'sidebar-1' && $byblog_custom_sidebar_label == '' ) {
                if ( !empty( $byblog_sidebar_post ) ) {
                    $byblog_sidebar_id =  $byblog_sidebar_post;
                }
            }
        } elseif ( is_page() ) {
            // If Single Page
            if ( function_exists( 'rwmb_meta' ) ) {
                $byblog_custom_sidebar_label = rwmb_meta( 'byblog_custom_sidebar', $args = array('type' => 'radio'), $post->ID );
            } else {
                $byblog_custom_sidebar_label = '';
            }

            $byblog_sidebar_array = array ( 'default', 'sidebar-1', 'footer-1', 'footer-2', 'footer-3', 'footer-4' );

            if ( $byblog_custom_sidebar_label == '' || $byblog_custom_sidebar_label == 'default' ) {
                $byblog_sidebar_id = 'sidebar-1';
            } elseif ( in_array ( $byblog_custom_sidebar_label, $byblog_sidebar_array ) ) {
                $byblog_sidebar_id = $byblog_custom_sidebar_label;
            } else {
                if ( function_exists( 'rwmb_meta' ) ) {
                    $byblog_custom_sidebar_label = rwmb_meta( 'byblog_custom_sidebar', $args = array('type' => 'radio'), $post->ID );
                } else {
                    $byblog_custom_sidebar_label = '';
                }
                $byblog_custom_sidebar_num = '';
                $byblog_custom_sidebar_num = $byblog_custom_sidebar_label + 1;
                $byblog_sidebar_id = 'Custom Sidebar' . ' ' . $byblog_custom_sidebar_num;
            }

            if ( ( $byblog_sidebar_id == 'sidebar-1' && $byblog_custom_sidebar_label == 'default' ) || $byblog_sidebar_id == 'sidebar-1' && $byblog_custom_sidebar_label == '' ) {
                if ( !empty( $byblog_sidebar_page ) ) {
                    $byblog_sidebar_id =  $byblog_sidebar_page;
                }
            }
        } elseif ( is_search() ) {
            // If Search Results
            if ( !empty( $byblog_sidebar_search ) ) {
                $byblog_sidebar_id =  $byblog_sidebar_search;
            }
        } elseif ( is_category() ) {
            // If Category Page
            if ( !empty( $byblog_sidebar_category ) ) {
                $byblog_sidebar_id =  $byblog_sidebar_category;
            }
        } elseif ( is_author() ) {
            // If Author Page
            if ( !empty( $byblog_sidebar_author ) ) {
                $byblog_sidebar_id =  $byblog_sidebar_author;
            }
        } elseif ( is_archive() ) {
            // If Archive Page
            if ( !empty( $byblog_sidebar_archive ) ) {
                $byblog_sidebar_id =  $byblog_sidebar_archive;
            }
        } else {
            $byblog_sidebar_id = 'sidebar-1';
        }

        if ( !$byblog_sidebar_id ) {
            $byblog_sidebar_id = 'sidebar-1';
        }

        if ( $byblog_sidebar_id ) :
            if ( is_active_sidebar( $byblog_sidebar_id ) ) {
                dynamic_sidebar( $byblog_sidebar_id );
            }
        endif;
    ?>
</aside>