<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('Redux_Framework_sample_config')) {

    class Redux_Framework_sample_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );
            
            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);
            
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            
            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field   set with compiler=>true is changed.

         * */
        function compiler_action($options, $css, $changed_values) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => esc_html__('Section via hook', 'byblog'),
                'desc' => esc_html__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'byblog'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(esc_html__('Customize &#8220;%s&#8221;', 'byblog'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'byblog'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'byblog'); ?>" />
                <?php endif; ?>

                <h4><?php echo $this->theme->display('Name'); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(esc_html__('By %s', 'byblog'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(esc_html__('Version %s', 'byblog'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . esc_html__('Tags', 'byblog') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                printf(' <p class="howto">' . esc_html__('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'byblog') . '</p>', esc_html__('http://codex.wordpress.org/Child_Themes', 'byblog'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'icon'          => 'el-icon-cogs',
                'icon_class'    => 'icon-large',
                'title'         => esc_html__('General Settings', 'byblog'),
                'fields'        => array(
                    array(
                        'id'        => 'byblog_logo',
                        'type'      => 'media', 
                        'url'       => true,
                        'title'     => esc_html__('Custom Logo', 'byblog'),
                        'subtitle'  => esc_html__('Upload a custom logo for your site.', 'byblog'),
                        ),  
                    array(
                        'id'        => 'byblog_retina_logo',
                        'type'      => 'media', 
                        'url'       => true,
                        'title'     => esc_html__('Retina Logo', 'byblog'),
                        'subtitle'  => esc_html__('Upload a retina logo for your site.', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_favicon',
                        'type'      => 'media', 
                        'url'       => true,
                        'title'     => esc_html__('Custom Favicon', 'byblog'),
                        'subtitle'  => esc_html__('Upload a custom favicon for your site.', 'byblog'),
                        ),  
                    array(
                        'id'        => 'byblog_pagination_type',
                        'type'      => 'button_set',
                        'title'     => esc_html__('Pagination Type', 'byblog'), 
                        'subtitle'  => esc_html__('Select the type of pagination for your site. Choose between Wide and Boxed.', 'byblog'),
                        'options'   => array(
                                            '1' => esc_html__('Numbered', 'byblog'),
                                            '2' => esc_html__('Next/Prev', 'byblog'),
                                       ),
                        'default'   => '1'
                        ),
                    array(
                        'id'        => 'byblog_scroll_btn',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Scroll to Top Button', 'byblog'),
                        'subtitle'  => esc_html__('Choose this option to show or hide scroll to top button.', 'byblog'),
                        'default'   => 1,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_lightbox',
                        'type'      => 'switch',
                        'title'     => esc_html__('Lightbox', 'byblog'), 
                        'subtitle'  => esc_html__('Choose this option to enable or disable lightbox.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Enable', 'byblog'),
                        'off'       => esc_html__('Disable', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_fb_og',
                        'type'      => 'switch',
                        'title'     => esc_html__('Facebook Open Graph Tags', 'byblog'), 
                        'subtitle'  => esc_html__('Choose this option to enable Facebook Open Graph tags.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Yes', 'byblog'),
                        'off'       => esc_html__('No', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_rtl',
                        'type'      => 'switch',
                        'title'     => esc_html__('RTL Language', 'byblog'), 
                        'subtitle'  => esc_html__('Choose this option to use right-to-left language on your blog.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Yes', 'byblog'),
                        'off'       => esc_html__('No', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_emoji',
                        'type'      => 'switch',
                        'title'     => esc_html__('Remove Emoji Support', 'byblog'), 
                        'subtitle'  => esc_html__('Choose this option to remove support for emoji on your blog.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Yes', 'byblog'),
                        'off'       => esc_html__('No', 'byblog'),
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon'          => 'el-icon-website',
                'icon_class'    => 'icon-large',
                'title'         => esc_html__('Layout Settings', 'byblog'),
                'fields'        => array(
                    array(
                        'id'        => 'byblog_layout_type',
                        'type'      => 'button_set',
                        'title'     => esc_html__('Layout Type', 'byblog'), 
                        'subtitle'  => esc_html__('Select the main layout for your site. Choose between Wide and Boxed.', 'byblog'),
                        'options'   => array(
                                            '1' => esc_html__('Full Width', 'byblog'), 
                                            '2' => esc_html__('Boxed', 'byblog'), 
                                       ),
                        'default'   => '1'
                        ),
                    array(
                        'id'        => 'byblog_layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => esc_html__('Homepage Layout', 'byblog'), 
                        'subtitle'  => wp_kses(__('Select main content and sidebar alignment. <br><br><strong>Note:</strong> These layouts are applied to homepage.', 'byblog'), array( 'strong' => array(), 'br' => array() ) ),
                        'options'   => array(
                                'cblayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout1.png'),
                                'bclayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout2.png'),
                                'lslayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout3.png'),
                                'sllayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout4.png'),
                                'gsblayout' => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout7.png'),
                                'sgblayout' => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout8.png'),
                                'sglayout'  => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout9.png'),
                                'gslayout'  => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout10.png'),
                                'clayout'   => array('alt' => '3 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout11.png'),
                                'glayout'   => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout12.png'),
                            ),
                        'default'   => 'cblayout'
                        ),
                    array(
                        'id'        => 'byblog_archive_layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => esc_html__('Archives Layout', 'byblog'), 
                        'subtitle'  => wp_kses(__('Select layout style for archives. <br><br><strong>Note:</strong> These layouts are applied to archives (Category, tags, author etc).', 'byblog'), array( 'strong' => array(), 'br' => array() ) ),
                        'options'   => array(
                                'cblayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout1.png'),
                                'bclayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout2.png'),
                                'lslayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout3.png'),
                                'sllayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout4.png'),
                                'gsblayout' => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout7.png'),
                                'sgblayout' => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout8.png'),
                                'sglayout'  => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout9.png'),
                                'gslayout'  => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout10.png'),
                                'clayout'   => array('alt' => '3 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout11.png'),
                                'glayout'   => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout12.png'),
                            ),
                        'default'   => 'cblayout'
                        ),
                    array(
                        'id'        => 'byblog_search_layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => esc_html__('Search Results Page Layout', 'byblog'), 
                        'subtitle'  => esc_html__('Select layout style for search results page.', 'byblog'),
                        'options'   => array(
                                'cblayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout1.png'),
                                'bclayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout2.png'),
                                'lslayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout3.png'),
                                'sllayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/layout4.png'),
                                'gsblayout' => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout7.png'),
                                'sgblayout' => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout8.png'),
                                'sglayout'  => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout9.png'),
                                'gslayout'  => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout10.png'),
                                'clayout'   => array('alt' => '3 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout11.png'),
                                'glayout'   => array('alt' => '2 Column Grid', 'img' => get_template_directory_uri() .'/images/layouts/layout12.png'),
                            ),
                        'default'   => 'cblayout'
                        ),
                    array(
                        'id'        => 'byblog_single_layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => esc_html__('Single Layout', 'byblog'), 
                        'subtitle'  => wp_kses(__('Select layout style for single pages. <br><br><strong>Note:</strong> These layouts are applied to single posts and pages.', 'byblog'), array( 'strong' => array(), 'br' => array() ) ),
                        'options'   => array(
                                'cblayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/cb.png'),
                                'bclayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/bc.png'),
                                'flayout'   => array('alt' => 'Full Width', 'img' => get_template_directory_uri() .'/images/layouts/f.png'),
                            ),
                        'default'   => 'cblayout'
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon' => 'el-icon-brush',
                'title' => esc_html__('Styling Options', 'byblog'),
                'fields' => array(
                    array(
                        'id'        => 'byblog_responsive_layout',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Enable Responsive Layout?', 'byblog'),
                        'subtitle'  => esc_html__('This theme can adopt to different screen resolutions automatically when rsponsive layout is enabled. You can enable or disable the responsive layout.', 'byblog'),
                        'default'   => 1,
                        'on'        => esc_html__('Enabled', 'byblog'),
                        'off'       => esc_html__('Disabled', 'byblog'),
                        ),
                    array(
                        'id'            => 'byblog_color_scheme',
                        'type'          => 'color',
                        'title'         => esc_html__('Primary Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick primary color scheme for the theme.', 'byblog'),
                        'default'       => '#9aac62',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'        => 'byblog_body_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Body', 'byblog')
                        ),
                    array(
                        'id'            => 'byblog_body_text_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Body Text Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for body text.', 'byblog'),
                        'output'        => array('body'),
                        'default'       => '#787878',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array( 
                        'id'            => 'byblog_body_bg',
                        'type'          => 'background',
                        'title'         => esc_html__('Body Background', 'byblog'),
                        'subtitle'      => esc_html__('Background options for body.', 'byblog'),
                        'preview_media' => true,
                        'preview'       => false,
                        'transparent'   => false,
                        'default'       => array(
                                'background-color'  => '#ffffff', 
                            ),
                        ),
                    array(
                        'id'            => 'byblog_bg_pattern',
                        'type'          => 'image_select',
                        'title'         => esc_html__('Background Pattern', 'byblog'), 
                        'subtitle'      => esc_html__('Choose a background pattern for the theme.', 'byblog'),
                        'options'       => array(
                                'nopattern' => array('alt' => 'nopattern', 'img' => get_template_directory_uri() .'/images/patterns/nopattern.png'),
                                'pattern0'  => array('alt' => 'pattern0', 'img' => get_template_directory_uri() .'/images/patterns/pattern0.png'),
                                'pattern1'  => array('alt' => 'pattern1', 'img' => get_template_directory_uri() .'/images/patterns/pattern1.png'),
                                'pattern2'  => array('alt' => 'pattern2', 'img' => get_template_directory_uri() .'/images/patterns/pattern2.png'),
                                'pattern3'  => array('alt' => 'pattern3', 'img' => get_template_directory_uri() .'/images/patterns/pattern3.png'),
                                'pattern4'  => array('alt' => 'pattern4', 'img' => get_template_directory_uri() .'/images/patterns/pattern4.png'),
                                'pattern5'  => array('alt' => 'pattern5', 'img' => get_template_directory_uri() .'/images/patterns/pattern5.png'),
                                'pattern6'  => array('alt' => 'pattern6', 'img' => get_template_directory_uri() .'/images/patterns/pattern6.png'),
                                'pattern7'  => array('alt' => 'pattern7', 'img' => get_template_directory_uri() .'/images/patterns/pattern7.png'),
                                'pattern8'  => array('alt' => 'pattern8', 'img' => get_template_directory_uri() .'/images/patterns/pattern8.png'),
                                'pattern9'  => array('alt' => 'pattern9', 'img' => get_template_directory_uri() .'/images/patterns/pattern9.png'),
                                'pattern10' => array('alt' => 'pattern10', 'img' => get_template_directory_uri() .'/images/patterns/pattern10.png'),
                                'pattern11' => array('alt' => 'pattern11', 'img' => get_template_directory_uri() .'/images/patterns/pattern11.png'),
                                'pattern12' => array('alt' => 'pattern12', 'img' => get_template_directory_uri() .'/images/patterns/pattern12.png'),
                                'pattern13' => array('alt' => 'pattern13', 'img' => get_template_directory_uri() .'/images/patterns/pattern13.png'),
                                'pattern14' => array('alt' => 'pattern14', 'img' => get_template_directory_uri() .'/images/patterns/pattern14.png'),
                                'pattern15' => array('alt' => 'pattern15', 'img' => get_template_directory_uri() .'/images/patterns/pattern15.png'),
                                'pattern16' => array('alt' => 'pattern16', 'img' => get_template_directory_uri() .'/images/patterns/pattern16.png'),
                                'pattern17' => array('alt' => 'pattern17', 'img' => get_template_directory_uri() .'/images/patterns/pattern17.png'),
                                'pattern18' => array('alt' => 'pattern18', 'img' => get_template_directory_uri() .'/images/patterns/pattern18.png'),
                                'pattern19' => array('alt' => 'pattern19', 'img' => get_template_directory_uri() .'/images/patterns/pattern19.png'),
                                'pattern20' => array('alt' => 'pattern20', 'img' => get_template_directory_uri() .'/images/patterns/pattern20.png'),
                                'pattern21' => array('alt' => 'pattern21', 'img' => get_template_directory_uri() .'/images/patterns/pattern21.png'),
                                'pattern22' => array('alt' => 'pattern22', 'img' => get_template_directory_uri() .'/images/patterns/pattern22.png'),
                                'pattern23' => array('alt' => 'pattern23', 'img' => get_template_directory_uri() .'/images/patterns/pattern23.png'),
                                'pattern24' => array('alt' => 'pattern24', 'img' => get_template_directory_uri() .'/images/patterns/pattern24.png'),
                                'pattern25' => array('alt' => 'pattern25', 'img' => get_template_directory_uri() .'/images/patterns/pattern25.png'),
                                'pattern26' => array('alt' => 'pattern26', 'img' => get_template_directory_uri() .'/images/patterns/pattern26.png'),
                                'pattern27' => array('alt' => 'pattern27', 'img' => get_template_directory_uri() .'/images/patterns/pattern27.png'),
                                'pattern28' => array('alt' => 'pattern28', 'img' => get_template_directory_uri() .'/images/patterns/pattern28.png'),
                            ),
                        'default'       => 'nopattern'
                        ),
                    array(
                        'id'            => 'byblog_nav_break',
                        'type'          => 'info',
                        'desc'          => esc_html__('Navigation Menu', 'byblog')
                        ),
                    array( 
                        'id'                    => 'byblog_top_nav_color',
                        'type'                  => 'background',
                        'title'                 => esc_html__('Navigation Background Color', 'byblog'),
                        'subtitle'              => esc_html__('Pick a background color for top navigation.', 'byblog'),
                        'output'                => array('.main-navigation'),
                        'preview_media'         => true,
                        'preview'               => false,
                        'background-image'      => false,
                        'background-position'   => false,
                        'background-repeat'     => false,
                        'background-attachment' => false,
                        'background-size'       => false,
                        'default'               => array(
                                                    'background-color'  => '#ffffff', 
                                                   ),
                        ),
                    array(
                        'id'            => 'byblog_nav_link_color',
                        'type'          => 'link_color',
                        'output'        => array('.nav-menu .current-menu-parent > a, .nav-menu .current-page-parent > a, .nav-menu .current-menu-item > a, .nav-menu a, .header-links a, header .search-button, .header-cart a'),
                        'title'         => esc_html__('Navigation Link Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a link color for the main navigation.', 'byblog'),
                        'default'       => array(
                            'regular'   => '#000000',
                            'hover'     => '#9aac62',
                        ),
                        'validate'      => 'color',
                        'transparent'   => false,
                        'active'        => false,
                        ),
                    array(
                        'id'            => 'byblog_nav_button_color',
                        'type'          => 'color',
                        'output'        => array('.menu-btn'),
                        'title'         => esc_html__('Mobile Menu Button Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for buttons that appears on mobile navigation.', 'byblog'),
                        'default'       => '#000000',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'             => 'byblog_menu_margin',
                        'type'           => 'spacing',
                        'output'         => array('.main-navigation'),
                        'mode'           => 'margin',
                        'units'          => array('px'),
                        'units_extended' => 'false',
                        'left'           => 'false',
                        'right'          => 'false',
                        'title'          => esc_html__('Navigation Menu Margin', 'byblog'),
                        'subtitle'       => esc_html__('Change distance from top and bottom of main navigation menu.', 'byblog'),
                        'default'        => array(
                            'margin-top'    => '0',
                            'margin-bottom' => '0',
                            'units'         => 'px',
                            )
                        ),
                    array(
                        'id'            => 'byblog_nav_break',
                        'type'          => 'info',
                        'desc'          => esc_html__('Header', 'byblog')
                        ),
                    array( 
                        'id'            => 'byblog_header_bg_color',
                        'type'          => 'background',
                        'output'        => array('.main-header'),
                        'title'         => esc_html__('Header Background', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a background color for header of the theme.', 'byblog'),
                        'preview_media' => true,
                        'preview'       => false,
                        ),
                    array(
                        'id'             => 'byblog_logo_margin',
                        'type'           => 'spacing',
                        'output'         => array('.logo-wrap'),
                        'mode'           => 'margin',
                        'units'          => array('px'),
                        'units_extended' => 'false',
                        'left'           => 'false',
                        'right'          => 'false',
                        'title'          => esc_html__('Logo Margin', 'byblog'),
                        'subtitle'       => esc_html__('Change distance from top and bottom of logo.', 'byblog'),
                        'default'        => array(
                            'margin-top'    => '40px',
                            'margin-bottom' => '40px',
                            'units'         => 'px',
                            )
                        ),
                    array(
                        'id'            => 'byblog_logo_color',
                        'type'          => 'color',
                        'output'        => array('.logo a'),
                        'title'         => esc_html__('Logo Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for logo that appears on header.', 'byblog'),
                        'default'       => '#000000',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_tagline_color',
                        'type'          => 'color',
                        'output'        => array('.tagline'),
                        'title'         => esc_html__('Header Tagline Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for tagline that appears on header under logo.', 'byblog'),
                        'default'       => '#333333',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_search_buttons_color',
                        'type'          => 'color',
                        'output'        => array('.header-search .searchform .search-submit'),
                        'title'         => esc_html__('Header Search Button Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for tagline that appears on header under logo.', 'byblog'),
                        'default'       => '#000000',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array( 
                        'id'                    => 'byblog_search_bg_color',
                        'type'                  => 'background',
                        'output'                => array('header .s'),
                        'title'                 => esc_html__('Header Search Background Color', 'byblog'), 
                        'subtitle'              => esc_html__('Pick a background color for search form that appears on header.', 'byblog'),
                        'preview_media'         => true,
                        'preview'               => false,
                        'background-image'      => false,
                        'background-position'   => false,
                        'background-repeat'     => false,
                        'background-attachment' => false,
                        'background-size'       => false,
                        'default'               => array(
                                                    'background-color'  => '#222222', 
                                                   ),
                        ),
                    array( 
                        'id'                    => 'byblog_newsletter_bg_color',
                        'type'                  => 'background',
                        'output'                => array('.top-bar'),
                        'title'                 => esc_html__('Header Newsletter Background Color', 'byblog'), 
                        'subtitle'              => esc_html__('Pick a background color for newsletter form that appears on header.', 'byblog'),
                        'preview_media'         => true,
                        'preview'               => false,
                        'background-image'      => false,
                        'background-position'   => false,
                        'background-repeat'     => false,
                        'background-attachment' => false,
                        'background-size'       => false,
                        'default'               => array(
                                                    'background-color'  => '#9aac62', 
                                                   ),
                        ),
                    array(
                        'id'            => 'byblog_post_box_break',
                        'type'          => 'info',
                        'desc'          => esc_html__('Content', 'byblog')
                        ),
                    array(
                        'id'            => 'byblog_post_box_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Posts Main Text Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for main text of post boxes.', 'byblog'),
                        'output'        => array('.post-box, .breadcrumbs, .author-info p, .comment-text'),
                        'default'       => '#555555',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_post_box_meta_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Posts Meta Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for meta of post boxes.', 'byblog'),
                        'output'        => array('.post-meta, .post-cats span, .r-meta, .r-meta a, .comment-meta a, #commentform p label, .single .pagination a, .read-more'),
                        'default'       => '#777777',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_post_box_title_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Posts Title Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for title of post boxes.', 'byblog'),
                        'output'        => array('color' => '.entry-title, .entry-title a, .section-heading, .author-box h5, .title a, .post-navigation span, .widgettitle, .post-navigation'),
                        'default'       => '#000000',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_page_title_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Page Title Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for title of page.', 'byblog'),
                        'output'        => array('.page-title'),
                        'default'       => '#000000',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_nav_break',
                        'type'          => 'info',
                        'desc'          => esc_html__('Sidebar', 'byblog')
                        ),
                    array(
                        'id'            => 'byblog_sidebar_widget_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Sidebar Widgets Text Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for text of sidebar widgets.', 'byblog'),
                        'output'        => array('.sidebar-widget'),
                        'default'       => '#555555',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_sidebar_widget_meta_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Sidebar Widgets Meta Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for meta of sidebar widgets.', 'byblog'),
                        'output'        => array('.meta, .meta a'),
                        'default'       => '#aaaaaa',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_sidebar_widget_title_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Sidebar Widget Title Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for title of sidebar widgets.', 'byblog'),
                        'output'        => array('.widget-title, #tabs li.active a'),
                        'default'       => '#000000',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array(
                        'id'            => 'byblog_nav_break',
                        'type'          => 'info',
                        'desc'          => esc_html__('Footer', 'byblog')
                        ),
                    array( 
                        'id'                    => 'byblog_footer_color',
                        'type'                  => 'background',
                        'title'                 => esc_html__('Footer Background Color', 'byblog'),
                        'subtitle'              => esc_html__('Pick a background color for the footer.', 'byblog'),
                        'output'                => array('.footer'),
                        'preview_media'         => true,
                        'preview'               => false,
                        'background-image'      => false,
                        'background-position'   => false,
                        'background-repeat'     => false,
                        'background-attachment' => false,
                        'background-size'       => false,
                        'default'               => array(
                                                        'background-color'  => '#ffffff', 
                                                    ),
                        ),
                    array(
                        'id'            => 'byblog_footer_link_color',
                        'type'          => 'link_color',
                        'title'         => esc_html__('Footer Link Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a link color for the footer.', 'byblog'),
                        'output'        => array('.footer a'),
                        'default'       => array(
                            'regular'   => '#000000',
                            'hover'     => '#ec5237',
                        ),
                        'validate'      => 'color',
                        'transparent'   => false,
                        'active'        => false,
                        ),
                    array(
                        'id'            => 'byblog_footer_widget_title_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Footer Widget Title Color', 'byblog'), 
                        'subtitle'      => esc_html__('Pick a color for title of footer widgets.', 'byblog'),
                        'output'        => array('.footer-widget .widget-title'),
                        'default'       => '#010101',
                        'validate'      => 'color',
                        'transparent'   => false,
                        ),
                    array( 
                        'id'                    => 'byblog_credit_bg_color',
                        'type'                  => 'background',
                        'title'                 => esc_html__('Credit/Copyright Background Color', 'byblog'),
                        'subtitle'              => esc_html__('Pick a background color for the credit or copyright area below footer.', 'byblog'),
                        'output'                => array('.copyright'),
                        'preview_media'         => true,
                        'preview'               => false,
                        'background-image'      => false,
                        'background-position'   => false,
                        'background-repeat'     => false,
                        'background-attachment' => false,
                        'background-size'       => false,
                        'default'               => array(
                                                        'background-color'  => '#ffffff', 
                                                    ),
                        ),
                    array( 
                        'id'            => 'byblog_credit_color',
                        'type'          => 'color',
                        'title'         => esc_html__('Credit/Copyright Text Color', 'byblog'),
                        'subtitle'      => esc_html__('Pick a text color for the credit or copyright area below footer.', 'byblog'),
                        'default'       => '#232323',
                        'validate'      => 'color',
                        'transparent'   => false,
                        'output'        => array( 'color' => '.copyright', ),
                        ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-css',
                'icon_class' => 'icon-large',
                'title' => esc_html__('Custom CSS', 'byblog'),
                'subsection' => true,
                'fields' => array(
                    array(
                        'id'=>'byblog_custom_css',
                        'type' => 'ace_editor',
                        'title' => esc_html__('Custom CSS', 'byblog'), 
                        'subtitle' => esc_html__('Quickly add some CSS to your theme by adding it to this block.', 'byblog'),
                        'mode' => 'css',
                        'theme' => 'monokai',
                        'default' => ""
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon' => 'el-icon-font',
                'icon_class' => 'icon-large',
                'title' => esc_html__('Typography', 'byblog'),
                'fields' => array(                                      
                    array(
                        'id'=>'byblog_body_font',
                        'type' => 'typography',
                        'output' => array('body'),
                        'title' => esc_html__('Body Font', 'byblog'),
                        'subtitle' => esc_html__('Select the main body font for your theme.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>false,
                        'line-height'=>false,
                        'default' => array(
                            'font-family'=>'Droid Serif',
                            'font-weight'=>'400',
                            ),
                        ),
                    array(
                        'id'=>'byblog_main_nav_font',
                        'type' => 'typography',
                        'output' => array('.main-nav'),
                        'title' => esc_html__('Main Navigation Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font for main navigation.', 'byblog'),
                        'google'=>true,
                        'color'=>false,
                        'text-align'=>false,
                        'line-height'=>false,
                        'text-transform'=>true,
                        'default' => array(
                            'font-family'=>'Poppins',
                            'font-size'=>'13px',
                            'font-weight'=>'600',
                            'text-transform'=>'uppercase',
                            ),
                        ),
                    array(
                        'id'=>'byblog_headings_font',
                        'type' => 'typography',
                        'output' => array('h1,h2,h3,h4,h5,h6, .header, .article-heading, .pagination, .ws-title, .carousel, .social-widget a, .post-navigation, #wp-calendar caption, .fn, #commentform input, #commentform textarea, input[type="submit"], .footer-subscribe, .heading'),
                        'title' => esc_html__('Headings Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font for headings for your theme.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>false,
                        'line-height'=>false,
                        'text-transform'=>true,
                        'default' => array(
                            'font-family'=>'Merriweather',
                            'font-weight'=>'700',
                            ),
                        ),
                    array(
                        'id'=>'byblog_title_font',
                        'type' => 'typography',
                        'output' => array('.entry-title'),
                        'title' => esc_html__('Post Title Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font for titles of posts for your theme.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'text-transform'=>true,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Playfair Display',
                            'font-size'=>'36px',
                            'font-weight'=>'400',
                            'line-height'=>'42px',
                            ),
                        ),
                    array(
                        'id'=>'byblog_post_content_font',
                        'type' => 'typography',
                        'output' => array('.post-content'),
                        'title' => esc_html__('Post Content Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font size and style for post content.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>true,
                        'line-height'=>true,
                        'default' => array(
                            'font-family'=>'Merriweather',
                            'font-size'=>'14px',
                            'font-weight'=>'400',
                            'line-height'=>'24px',
                            ),
                        ),
                    array(
                        'id'=>'byblog_meta_font',
                        'type' => 'typography',
                        'output' => array('.post-meta, .read-more, .r-meta, .woocommerce div.product form.cart .button, .woocommerce ul.products li.product .button, .woocommerce .widget_price_filter .price_slider_amount .button'),
                        'title' => esc_html__('Post Meta Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font size and style for meta section.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'text-transform'=>true,
                        'font-size'=>true,
                        'line-height'=>true,
                        'default' => array(
                            'font-family'=>'Poppins',
                            'font-size'=>'12px',
                            'font-style'=>'normal',
                            'font-weight'=>'500',
                            'line-height'=>'20px',
                            'text-transform'=>'upppercase',
                            ),
                        ),
                    array(
                        'id'=>'byblog_widget_title_font',
                        'type' => 'typography',
                        'output' => array('.widget-title, #tabs li, .section-heading'),
                        'title' => esc_html__('Widget Title Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font for titles of widgets for your theme.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'text-transform'=>true,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Poppins',
                            'font-size'=>'14px',
                            'font-weight'=>'600',
                            'line-height'=>'20px',
                            'text-transform'=>'uppercase',
                            ),
                        ),
                    array(
                        'id'=>'byblog_widget_post_title_font',
                        'type' => 'typography',
                        'output' => array('.widgettitle, .product-title'),
                        'title' => esc_html__('Widget Post Title Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font for titles posts in widgets for your theme.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'text-transform'=>true,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Playfair Display',
                            'font-size'=>'18px',
                            'font-weight'=>'400',
                            'line-height'=>'22px',
                            ),
                        ),
                    array(
                        'id'=>'byblog_widgets_meta_font',
                        'type' => 'typography',
                        'output' => array('.meta'),
                        'title' => esc_html__('Widgets Meta Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font size and style for meta section of widgets.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'text-transform'=>true,
                        'font-size'=>true,
                        'line-height'=>true,
                        'default' => array(
                            'font-family'=>'Poppins',
                            'font-size'=>'12px',
                            'font-weight'=>'400',
                            'line-height'=>'20px',
                            'text-transform'=>'uppercase',
                            ),
                        ),
                    array(
                        'id'=>'byblog_sidebar_widgets_font',
                        'type' => 'typography',
                        'output' => array('.sidebar-widget'),
                        'title' => esc_html__('Sidebar Widgets Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font for sidebar widgets.', 'byblog'),
                        'google'=>true,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Merriweather',
                            'font-size'=>'13px',
                            'font-weight'=>'400',
                            'line-height'=>'24px'
                            ),
                        ),
                    array(
                        'id'=>'byblog_logo_font',
                        'type' => 'typography',
                        'output' => array('.header #logo'),
                        'title' => esc_html__('Logo Font', 'byblog'),
                        'subtitle' => esc_html__('Select the font for logo for your theme.', 'byblog'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>true,
                        'line-height'=>true,
                        'default' => array(
                            'font-family'=>'Open Sans',
                            'font-size'=>'40px',
                            'font-weight'=>'400',
                            'line-height'=>'50px',
                            'text-transform'=>'uppercase',
                            ),
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon' => 'el-icon-home',
                'icon_class' => 'icon-large',
                'title' => esc_html__('Home', 'byblog'),
                'fields' => array(
                    array(
                        'id'=>'byblog_home_break',
                        'type' => 'info',
                        'desc' => esc_html__('Home Content', 'byblog')
                        ),
                    array(
                        'id'=>'byblog_home_latest_posts',
                        'type' => 'switch', 
                        'title' => esc_html__('Show Latest Posts by Category', 'byblog'),
                        'subtitle'=> esc_html__('Choose this option to show latest posts by category on homepage.', 'byblog'),
                        "default"       => 0,
                        'on'        => esc_html__('Yes', 'byblog'),
                        'off'       => esc_html__('No', 'byblog'),
                        ),  
                    array(
                        'id'        => 'byblog_home_latest_cat',
                        'type'     => 'select',
                        'multi'    => true,
                        'data' => 'categories',
                        'title'    => esc_html__('Latest Posts Category', 'byblog'), 
                        'required' => array('byblog_home_latest_posts','=','1'),
                        'subtitle' => esc_html__('Select category/categories for latest posts on homepage. Use Ctrl key to select more than one category.', 'byblog'),
                        ),
                    array(
                        'id'=>'byblog_home_content',
                        'type' => 'radio',
                        'title' => esc_html__('Home Content', 'byblog'), 
                        'subtitle' => esc_html__('Select content type for home.', 'byblog'),
                        'options' => array('1' => 'Excerpt', '2' => 'Full Content', '3' => 'None'),
                        'default' => '1'
                        ),  
                    array(
                        'id'=>'byblog_excerpt_length',
                        'type' => 'slider', 
                        'title' => esc_html__('Excerpt Length', 'byblog'), 
                        'required' => array('byblog_home_content','=','1'),
                        'subtitle' => esc_html__('Paste the length of post excerpt to be shown on blog posts.', 'byblog'),
                        "default"   => "16",
                        "min"       => "0",
                        "step"      => "1",
                        "max"       => "55",
                        ),
                    array(
                        'id'=>'byblog_read_more_btn',
                        'type' => 'switch', 
                        'title' => esc_html__('Show Read More Button', 'byblog'),
                        'subtitle'=> esc_html__('Choose this option to show read more button on blog posts.', 'byblog'),
                        "default"       => 1,
                        'on'        => esc_html__('Yes', 'byblog'),
                        'off'       => esc_html__('No', 'byblog'),
                        ),
                    array(
                        'id'=>'byblog_read_more_btn_text',
                        'type' => 'text',
                        'title' => esc_html__('Read More Button Text', 'byblog'),
                        'subtitle' => esc_html__('Enter text for read more button text.', 'byblog'),
                        'required' => array('byblog_read_more_btn','=','1'),
                        'validate' => 'no_special_chars',
                        'default' => 'Read More'
                        ),
                    array(
                        'id'=>'byblog_meta_break',
                        'type' => 'info',
                        'desc' => esc_html__('Post Meta', 'byblog')
                        ),
                    array(
                        'id'=>'byblog_post_meta',
                        'type' => 'switch', 
                        'title' => esc_html__('Post Meta', 'byblog'),
                        'subtitle'=> esc_html__('Choose the option to show or hide post meta (Categories, Tags, Author Name etc) on homepage and archives.', 'byblog'),
                        "default"       => 1,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),  
                    array(
                        'id'=>'byblog_post_meta_options',
                        'type' => 'checkbox',
                        'title' => esc_html__('Post Meta Info Options', 'byblog'), 
                        'required' => array('byblog_post_meta','=','1'),    
                        'subtitle' => esc_html__('Select which items you want to show for post meta on homepage and archives.', 'byblog'),
                        'options' => array('1' => 'Post Author','2' => 'Date','3' => 'Post Category','4' => 'Post Tags','5' => 'Post Comments','6' => 'Post Type Icon' ),//Must provide key => value pairs for multi checkbox options
                        'default' => array('1' => '1', '2' => '1', '3' => '1', '4' => '1', '5' => '1', '6' => '1')//See how std has changed? you also don't need to specify opts that are 0.
                        ),
                    array(
                        'id'=>'byblog_social_break',
                        'type' => 'info',
                        'desc' => esc_html__('Social Media Sharing', 'byblog')
                        ),
                    array(
                        'id'=>'byblog_show_home_share_buttons',
                        'type' => 'switch', 
                        'title' => esc_html__('Social Media Share Buttons', 'byblog'),
                        'subtitle'=> esc_html__('Choose the option to show or hide social media share buttons.', 'byblog'),
                        "default"       => 0,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),      
                    array(
                        'id'=>'byblog_share_buttons_home',
                        'type'     => 'sortable',
                        'title' => esc_html__('Select Share Buttons', 'byblog'), 
                        'required' => array('byblog_show_home_share_buttons','=','1'),    
                        'subtitle' => esc_html__('Select which buttons you want to show. You can drag and drop the buttons to change the position.', 'byblog'),
                        'mode'     => 'checkbox',
                        'options'  => array(
                            'fb'     => 'Facebook',
                            'twitter'    => 'Twitter',
                            'gplus'  => 'Google+',
                            'linkedin'  => 'LinkedIn',
                            'pinterest'  => 'Pinterest',
                            'stumbleupon'  => 'StumbleUpon',
                            'reddit'  => 'Reddit',
                            'tumblr'  => 'Tumblr',
                            'delicious'  => 'Delicious',
                        ),
                        // For checkbox mode
                        'default' => array(
                            'fb' => true,
                            'twitter' => true,
                            'gplus' => true,
                            'linkedin' => false,
                            'pinterest' => false,
                            'stumbleupon' => false,
                            'reddit' => false,
                            'tumblr' => false,
                            'delicious' => false,
                        ),
                    ),
                )
            );

            $this->sections[] = array(
                'icon'          => 'el-icon-adjust-alt',
                'icon_class'    => 'icon-large',
                'title'         => esc_html__('Featured Section', 'byblog'),
                'fields'        => array(  
                    array(
                        'id'        =>'byblog_featured_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Main Featured Section', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_slider',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Slider', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide slider.', 'byblog'),
                        "default"   => 0,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_slider_style',
                        'type'      => 'button_set',
                        'required'  => array( 'byblog_slider','=','1' ),
                        'title'     => esc_html__('Chose featured section style', 'byblog'),
                        'subtitle'  => esc_html__('Choose a style for main featured section shown below header.', 'byblog'),
                        'options'   => array(
                            'slider'    => esc_html__('Slider', 'byblog'),
                            'carousel'  => esc_html__('Carousel', 'byblog'),
                            'gallery'   => esc_html__('Gallery', 'byblog'),
                         ),
                        'default'   => 'slider'
                        ),
                    array(
                        'id'        =>'byblog_slider_type',
                        'type'      => 'button_set',
                        'required'  => array('byblog_slider','=','1'),
                        'title'     => esc_html__('What to show in slider', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide slider.', 'byblog'),
                        'options'   => array(
                            'custom_slides' => 'Custom Slides',
                            'posts_by_cat'  => 'Posts by Category/Categories',
                            'sel_posts'     => 'Selected Posts'
                         ),
                        'default'   => 'posts_by_cat'
                        ),
                    array(
                        'id'            =>'byblog_custom_slidess',
                        'type'          => 'slides',
                        'required'      => array('byblog_slider_type','=','custom_slides'),
                        'title'         => esc_html__('Slides Options', 'byblog'),
                        'subtitle'      => esc_html__('Select maximum of 4 slides. If you choose just 1 slide then it will become a static banner.', 'byblog'),
                        'placeholder'   => array(
                            'title'         => esc_html__('This is a title', 'byblog'),
                            'description'   => esc_html__('Description Here', 'byblog'),
                            'url'           => esc_html__('Give us a link!', 'byblog'),
                            ),                          
                        ),
                    array(
                        'id'        => 'byblog_slider_cat',
                        'type'      => 'select',
                        'multi'     => true,
                        'data'      => 'categories',
                        'title'     => esc_html__('Slider Category', 'byblog'),
                        'subtitle'  => esc_html__('Select category/categories for slider.', 'byblog'),
                        'required'  => array('byblog_slider_type','=','posts_by_cat'),
                        ),
                    array(
                        'id'        => 'byblog_slider_posts',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Posts', 'byblog'),
                        'subtitle'  => esc_html__('Select posts for slider', 'byblog'),
                        'required'  => array('byblog_slider_type','=','sel_posts'),
                        'multi'     => true,
                        'data'      => 'posts',
                        'args'      => array( 'posts_per_page' => -1 ),
                        ),
                    array(
                        'id'        => 'byblog_slider_posts_count',
                        'type'      => 'slider', 
                        'title'     => esc_html__('Number of Slider Posts', 'byblog'),
                        'subtitle'  => esc_html__('Choose the number of posts you want to show in slider.', 'byblog'),
                        'required'  => array( 'byblog_slider_type','=','posts_by_cat' ),
                        "default"   => "5",
                        "min"       => "1",
                        "step"      => "1",
                        "max"       => "20",
                        ),
                    array(
                        'id'        => 'byblog_featured_cat',
                        'type'      => 'select',
                        'multi'     => true,
                        'data'      => 'categories',
                        'title'     => esc_html__('Featured Category', 'byblog'),
                        'subtitle'  => esc_html__('Select category/categories for slider.', 'byblog'),
                        'required'  => array('byblog_slider_type','=','posts_by_cat'),
                        ),
                    array(
                        'id'        => 'byblog_featured_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Featured Posts', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_home_featured_posts',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Featured Posts on Homepage', 'byblog'),
                        'subtitle'  => esc_html__('Choose this option to show featured posts on homepage.', 'byblog'),
                        "default"   => 0,
                        'on'        => esc_html__('Yes', 'byblog'),
                        'off'       => esc_html__('No', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_featured_posts',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Posts', 'byblog'),
                        'subtitle'  => esc_html__('Select featured posts', 'byblog'),
                        'required'  => array('byblog_home_featured_posts','=','1'),
                        'multi'     => true,
                        'data'      => 'posts',
                        'args'      => array( 'posts_per_page' => -1 ),
                        ),
                )
            );

            $this->sections[] = array(
                'icon'          => 'el-icon-check-empty',
                'icon_class'    => 'icon-large',
                'title'         => esc_html__('Header', 'byblog'),
                'fields'        => array(
                    array(
                        'id'        => 'byblog_header_style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => esc_html__('Header Style', 'byblog'), 
                        'subtitle'  => esc_html__('Select style for header.', 'byblog'),
                        'options'   => array(
                            '1' => array('alt' => 'Header Style 1', 'img' => get_template_directory_uri() .'/images/layouts/header-1.png'),
                            '2' => array('alt' => 'Header Style 2', 'img' => get_template_directory_uri() .'/images/layouts/header-2.png'),
                            '3' => array('alt' => 'Header Style 3', 'img' => get_template_directory_uri() .'/images/layouts/header-3.png'),
                            '4' => array('alt' => 'Header Style 4', 'img' => get_template_directory_uri() .'/images/layouts/header-4.png'),
                            '5' => array('alt' => 'Header Style 5', 'img' => get_template_directory_uri() .'/images/layouts/header-5.png'),
                        ),
                        'default'   => '1'
                        ),
                    array(
                        'id'        => 'byblog_tagline',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Tagline', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide tagline below logo.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_header_search',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Search Box', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide search box in navigation menu', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_sticky_menu',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Sticky Menu', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to enable or disable the sticky/floating menu.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Enable', 'byblog'),
                        'off'       => esc_html__('Disable', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_newsletter_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Newsletter', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_header_newsletter',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Newsletter Form', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide mailchimp newsletter in header.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_mailchimp_newsletter_url',
                        'type'      => 'text',
                        'required'  => array('byblog_header_newsletter','=','1'),
                        'title'     => esc_html__('Mailchimp Form URL', 'byblog'),
                        'subtitle'  => esc_html__('Enter your mailchimp form URL here.', 'byblog'),
                        'validate'  => 'url',
                        'default'   => ''
                        ),
                    array(
                        'id'        => 'byblog_mailchimp_newsletter_text',
                        'type'      => 'text',
                        'required'  => array('byblog_header_newsletter','=','1'),
                        'title'     => esc_html__('Mailchimp Form Text', 'byblog'),
                        'subtitle'  => esc_html__('Enter description text for form here.', 'byblog'),
                        'default'   => 'Get the lastest stories straight into your inbox!'
                        ),
                    array(
                        'id'        => 'byblog_header_social_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Header Social Links', 'byblog')
                        ), 
                    array(
                        'id'        => 'byblog_social_links',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Social Links in Header', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide social links in top bar.', 'byblog'),
                        'default'   => 0,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_social_links_sort',
                        'type'      => 'sortable',
                        'required'  => array('byblog_social_links','=','1'),
                        'title'     => esc_html__('Arrange Social Links', 'byblog'),
                        'subtitle'  => wp_kses(__('Check the social links that you want to show in header.<br><br><strong>Note:</strong> You need to add links to your social profiles in Social Profiles tab of theme options.', 'byblog'), array( 'strong' => array(), 'br' => array() ) ),
                        'mode'      => 'checkbox',
                        'options'   => array(
                            'fb'        => 'Facebook',
                            'twitter'   => 'Twitter',
                            'gplus'     => 'Google+',
                            'pinterest' => 'Pinterest',
                            'linkedin'  => 'LinkedIn',
                            'youtube'   => 'YouTube',
                            'instagram' => 'Instagram',
                            'rss'       => 'RSS',
                            'reddit'    => 'Reddit',
                            'tumblr'    => 'Tumblr',
                            'flickr'    => 'Flickr',
                            'dribbble'  => 'Dribbble',
                            'git'       => 'GitHub',
                        ),
                        'default'   => array(
                            'fb'        => true,
                            'twitter'   => true,
                            'gplus'     => true,
                            'pinterest' => false,
                            'linkedin'  => false,
                            'youtube'   => false,
                            'instagram' => false,
                            'rss'       => false,
                            'reddit'    => false,
                            'tumblr'    => false,
                            'flickr'    => false,
                            'dribbble'  => false,
                            'git'       => false,
                        ),
                    ),
                )
            );

            $this->sections[] = array(
                'icon'          => 'el-icon-minus',
                'icon_class'    => 'icon-large',
                'title'         => esc_html__('Footer', 'byblog'),
                'fields'        => array(
                    array(
                        'id'        => 'byblog_footer_featured_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Footer Featured Posts', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_show_footer_posts',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Featured Posts on Footer', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide featured posts on footer.', 'byblog'),
                        'default'   => 2,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_footer_featured_posts',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Posts', 'byblog'),
                        'subtitle'  => esc_html__('Select featured posts', 'byblog'),
                        'required'  => array('byblog_show_footer_posts','=','1'),
                        'multi'     => true,
                        'data'      => 'posts',
                        'args'      => array('posts_per_page' => -1),
                        ),
                    array(
                        'id'        => 'byblog_footer_meta',
                        'type'      => 'switch', 
                        'required'  => array('byblog_show_footer_posts','=','1'),
                        'title'     => esc_html__('Footer Featured Posts Meta', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide post meta (Categories, Tags, Author etc) on single posts.', 'byblog'),
                        "default"   => 2,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),  
                    array(
                        'id'        => 'byblog_footer_post_meta_options',
                        'type'      => 'checkbox',
                        'required'  => array('byblog_footer_meta','=','1'),
                        'title'     => esc_html__('Footer Featured Posts Meta Info Options', 'byblog'),
                        'subtitle'  => esc_html__('Select which items you want to show for post meta on single pages.', 'byblog'),
                        'options'   => array(
                                        '1' => esc_html__('Post Author', 'byblog'),
                                        '2' => esc_html__('Date', 'byblog'),
                                        '3' => esc_html__('Post Category', 'byblog'),
                                       ),
                        'default'   => array(
                                        '1' => '1',
                                        '2' => '1',
                                        '3' => '1'
                                       )
                        ),
                    array(
                        'id'        => 'byblog_footer_widgets_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Footer Widgets', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_show_footer_widgets',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Footer Widgets', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide footer widgets.', 'byblog'),
                        'default'   => 2,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_footer_columns',
                        'type'      => 'image_select',
                        'required'  => array('byblog_show_footer_widgets','=','1'),
                        'compiler'  => true,
                        'title'     => esc_html__('Footer Columns', 'byblog'), 
                        'subtitle'  => esc_html__('Select number of columns for footer. Choose between 1, 2, 3 or 4 columns.', 'byblog'),
                        'options'   => array(
                                'footer_4' => array('alt' => '4 Columns', 'img' => get_template_directory_uri() .'/images/layouts/f4c.png'),
                                'footer_3' => array('alt' => '3 Columns', 'img' => get_template_directory_uri() .'/images/layouts/f3c.png'),
                                'footer_2' => array('alt' => '2 Columns', 'img' => get_template_directory_uri() .'/images/layouts/f2c.png'),
                                'footer_1' => array('alt' => '1 Column', 'img' => get_template_directory_uri() .'/images/layouts/f1c.png'),
                            ),
                        'default'   => 'footer_4'
                        ),
                    array(
                        'id'        => 'byblog_instagram_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Instagram Photos', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_show_instagram_photos',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Show Instagram Photos', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide instagram photos on footer.', 'byblog'),
                        'default'   => 2,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_instagram_profile_url',
                        'type'      => 'text',
                        'required'  => array('byblog_show_instagram_photos','=','1'),
                        'title'     => esc_html__('Instagram Profile URL', 'byblog'), 
                        'subtitle'  => esc_html__('Enter your instagram profile URL here.', 'byblog'),
                        'default'   => ''
                        ),
                    array(
                        'id'        => 'byblog_instagram_user_id',
                        'type'      => 'text',
                        'required'  => array('byblog_show_instagram_photos','=','1'),
                        'title'     => esc_html__('Instagram User ID', 'byblog'), 
                        'subtitle'  => esc_html__('Enter your instagram user ID here.', 'byblog'),
                        'default'   => ''
                        ),
                    array(
                        'id'        => 'byblog_instagram_accesstoken',
                        'type'      => 'text',
                        'required'  => array('byblog_show_instagram_photos','=','1'),
                        'title'     => esc_html__('Instagram Access Token', 'byblog'), 
                        'subtitle'  => esc_html__('Enter your instagram access token here.', 'byblog'),
                        'default'   => ''
                        ),
                    array(
                        'id'        => 'byblog_footer_text',
                        'type'      => 'editor',
                        'title'     => esc_html__('Copyright Text', 'byblog'), 
                        'subtitle'  => esc_html__('Enter copyright text to be shown on footer or you can keep it blank to show nothing.', 'byblog'),
                        'default'   => '&copy; Copyright 2016. Theme by <a href="http://themeforest.net/user/BloomPixel/portfolio?ref=bloompixel">BloomPixel</a>.',
                        'editor_options' => array(
                                'media_buttons' => false,
                                'textarea_rows' => 5
                            )
                        ),
                )
            );

            $this->sections[] = array(
                'icon'          => 'el-icon-folder-open',
                'icon_class'    => 'icon-large',
                'title'         => esc_html__('Single Post Options', 'byblog'),
                'fields'        => array(
                    array(
                        'id'        => 'byblog_single_post_layout',
                        'type'      => 'sorter',
                        'title'     => 'Single Posts Layout',
                        'subtitle'  => 'Organize the elements of single posts.',
                        'options'   => array(
                            'enabled'   => array(
                                'post-content'    => 'Post Content',
                                'post-navigation' => 'Post Navigation',
                                'author-box'      => 'Author Box',
                                'related-posts'   => 'Related Posts'
                            ),
                            'disabled' => array()
                        ),
                    ),
                    array(
                        'id'        => 'byblog_breadcrumbs',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Breadcrumbs', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide breadcrumbs on single posts.', 'byblog'),
                        'default'   => 1,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_single_featured',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Featured Content', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide featured thumbnails, gallery, audio or video on single posts.', 'byblog'),
                        'default'   => 1,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_single_meta_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post Meta', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_single_meta',
                        'type'      => 'switch',
                        'title'     => esc_html__('Single Post Meta', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide post meta (Categories, Tags, Author etc) on single posts.', 'byblog'),
                        'default'   => 1,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),  
                    array(
                        'id'        => 'byblog_single_post_meta_options',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Single Post Meta Info Options', 'byblog'),
                        'required'  => array('byblog_single_meta','=','1'),
                        'subtitle'  => esc_html__('Select which items you want to show for post meta on single pages.', 'byblog'),
                        'options'   => array(
                                            '1' => esc_html__('Post Author', 'byblog'),
                                            '2' => esc_html__('Date', 'byblog'),
                                            '3' => esc_html__('Post Category', 'byblog'),
                                            '4' => esc_html__('Post Tags', 'byblog'),
                                            '5' => esc_html__('Post Comments', 'byblog'),
                                            '6' => esc_html__('Post Type Icon', 'byblog'),
                                       ),
                        'default'   => array(
                                            '1' => '1',
                                            '2' => '1',
                                            '3' => '1',
                                            '4' => '1',
                                            '5' => '1',
                                            '6' => '1'
                                       )
                        ),
                    array(
                        'id'        => 'byblog_related_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Related Posts', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_related_posts_count',
                        'type'      => 'slider', 
                        'title'     => esc_html__('Number of Related Posts', 'byblog'),
                        'subtitle'  => esc_html__('Choose the number of related posts you want to show.', 'byblog'),
                        'default'   => '3',
                        'min'       => '3',
                        'step'      => '1',
                        'max'       => '20',
                        ),  
                    array(
                        'id'        => 'byblog_related_posts_by',
                        'type'      => 'radio',
                        'title'     => esc_html__('Filter Related Posts By', 'byblog'),
                        'subtitle'  => esc_html__('Choose whether to show related posts by categories or tags.', 'byblog'),
                        'options'   => array(
                                        '1' => esc_html__('Categories', 'byblog'),
                                        '2' => esc_html__('Tags', 'byblog'),
                                        '3' => esc_html__('Author', 'byblog'),
                                    ),
                        'default'   => '1',
                        ),
                    array(
                        'id'        => 'byblog_single_share_break',
                        'type'      => 'info',
                        'desc'      => esc_html__('Sharing Buttons', 'byblog')
                        ),
                    array(
                        'id'        => 'byblog_show_share_buttons',
                        'type'      => 'switch', 
                        'title'     => esc_html__('Social Media Share Buttons', 'byblog'),
                        'subtitle'  => esc_html__('Choose the option to show or hide social media share buttons.', 'byblog'),
                        "default"   => 0,
                        'on'        => esc_html__('Show', 'byblog'),
                        'off'       => esc_html__('Hide', 'byblog'),
                        ),
                    array(
                        'id'        => 'byblog_share_buttons',
                        'type'      => 'sortable',
                        'title'     => esc_html__('Select Share Buttons', 'byblog'), 
                        'required'  => array('byblog_show_share_buttons','=','1'), 
                        'subtitle'  => esc_html__('Select which buttons you want to show. You can drag and drop the buttons to change the position.', 'byblog'),
                        'mode'      => 'checkbox',
                        'options'   => array(
                            'fb'            => 'Facebook',
                            'twitter'       => 'Twitter',
                            'gplus'         => 'Google+',
                            'linkedin'      => 'LinkedIn',
                            'pinterest'     => 'Pinterest',
                            'stumbleupon'   => 'StumbleUpon',
                            'reddit'        => 'Reddit',
                            'tumblr'        => 'Tumblr',
                            'delicious'     => 'Delicious',
                        ),
                        'default'   => array(
                            'fb'            => true,
                            'twitter'       => true,
                            'gplus'         => true,
                            'linkedin'      => false,
                            'pinterest'     => false,
                            'stumbleupon'   => false,
                            'reddit'        => false,
                            'tumblr'        => false,
                            'delicious'     => false,
                        ),
                    ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-eur',
                'icon_class' => 'icon-large',
                'title' => esc_html__('Ad Management', 'byblog'),
                'fields' => array(
                    array(
                        'id'=>'byblog_below_content_sidebar_ad',
                        'type' => 'textarea',
                        'title' => esc_html__('Ad below Content and Sidebar', 'byblog'), 
                        'subtitle' => esc_html__('Paste your ad code here for showing ad below content and sidebar.', 'byblog'),
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_blog_ad_break',
                        'type' => 'info',
                        'desc' => esc_html__('Blog and Archives Ad', 'byblog')
                        ),
                    array(
                        'id'=>'byblog_blog_archive_ad',
                        'type' => 'textarea',
                        'title' => esc_html__('Blog and Archives Ad Code', 'byblog'), 
                        'subtitle' => esc_html__('Paste your ad code here.', 'byblog'),
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_blog_ad_pos',
                        'type' => 'text',
                        'title' => esc_html__('Show Ad after Nth Post', 'byblog'),
                        'default' => '1'
                        ),
                    array(
                        'id'=>'byblog_header_ad_break',
                        'type' => 'info',
                        'desc' => esc_html__('Header Ad', 'byblog')
                        ),
                    array(
                        'id'=>'byblog_header_ad',
                        'type' => 'textarea',
                        'title' => esc_html__('Header Ad Code', 'byblog'), 
                        'subtitle' => esc_html__('Paste your ad code here.', 'byblog'),
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_single_post_ad_break',
                        'type' => 'info',
                        'desc' => esc_html__('Single Post Ads', 'byblog')
                        ),
                    array(
                        'id'=>'byblog_below_title_ad',
                        'type' => 'textarea',
                        'title' => esc_html__('Below Post Title Ad', 'byblog'), 
                        'subtitle' => esc_html__('Paste your ad code here.', 'byblog'),
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_below_content_ad',
                        'type' => 'textarea',
                        'title' => esc_html__('Below Post Content Ad', 'byblog'), 
                        'subtitle' => esc_html__('Paste your ad code here.', 'byblog'),
                        'default' => ''
                        ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el el-lines',
                'icon_class' => 'icon-large',
                'title' => esc_html__('Sidebars', 'byblog'),
                'fields' => array(
                    array(
                        'id'       =>'byblog_sticky_sidebar',
                        'type'     => 'switch', 
                        'title'    => esc_html__('Sticky Sidebar', 'byblog'), 
                        'subtitle' => esc_html__('Choose this option to make your sidebar sticky while scrolling.', 'byblog'),
                        "default"  => 0,
                        'on'       => 'Yes',
                        'off'      => 'No',
                    ),
                    array(
                        'id'=>'byblog_create_sidebar_break',
                        'type' => 'info',
                        'desc' => esc_html__('Create Custom Sidebars', 'byblog')
                        ),
                    array(
                        'id'=>'byblog_custom_sidebars',
                        'type' => 'slider', 
                        'title' => esc_html__('Custom Sidebars', 'byblog'),
                        'subtitle'=> esc_html__('Choose the number of custom sidebars.', 'byblog'),
                        "default"   => "0",
                        "min"       => "0",
                        "step"      => "1",
                        "max"       => "50",
                        ),
                    array(
                        'id'=>'byblog_select_sidebar_break',
                        'type' => 'info',
                        'desc' => esc_html__('Select Sidebars', 'byblog')
                        ),
                    array(
                        'id'       => 'byblog_sidebar_home',
                        'type'     => 'select',
                        'data'     => 'sidebars',
                        'title'    => esc_html__( 'Homepage Sidebar', 'byblog' ),
                        'subtitle' => esc_html__( 'Select a sidebar for homepage', 'byblog' ),
                        'default'  => 'sidebar-1',
                        ),
                    array(
                        'id'       => 'byblog_sidebar_post',
                        'type'     => 'select',
                        'data'     => 'sidebars',
                        'title'    => esc_html__( 'Single Post Sidebar', 'byblog' ),
                        'subtitle' => esc_html__( 'Select a sidebar for single posts', 'byblog' ),
                        'default'  => 'sidebar-1',
                        ),
                    array(
                        'id'       => 'byblog_sidebar_page',
                        'type'     => 'select',
                        'data'     => 'sidebars',
                        'title'    => esc_html__( 'Page Sidebar', 'byblog' ),
                        'subtitle' => esc_html__( 'Select a sidebar for single page', 'byblog' ),
                        'default'  => 'sidebar-1',
                        ),
                    array(
                        'id'       => 'byblog_sidebar_search',
                        'type'     => 'select',
                        'data'     => 'sidebars',
                        'title'    => esc_html__( 'Search Sidebar', 'byblog' ),
                        'subtitle' => esc_html__( 'Select a sidebar for search pages', 'byblog' ),
                        'default'  => 'sidebar-1',
                        ),
                    array(
                        'id'       => 'byblog_sidebar_category',
                        'type'     => 'select',
                        'data'     => 'sidebars',
                        'title'    => esc_html__( 'Category Sidebar', 'byblog' ),
                        'subtitle' => esc_html__( 'Select a sidebar for category page', 'byblog' ),
                        'default'  => 'sidebar-1',
                        ),
                    array(
                        'id'       => 'byblog_sidebar_author',
                        'type'     => 'select',
                        'data'     => 'sidebars',
                        'title'    => esc_html__( 'Author Sidebar', 'byblog' ),
                        'subtitle' => esc_html__( 'Select a sidebar for author page', 'byblog' ),
                        'default'  => 'sidebar-1',
                        ),
                    array(
                        'id'       => 'byblog_sidebar_archive',
                        'type'     => 'select',
                        'data'     => 'sidebars',
                        'title'    => esc_html__( 'Archive Sidebar', 'byblog' ),
                        'subtitle' => esc_html__( 'Select a sidebar for archive page', 'byblog' ),
                        'default'  => 'sidebar-1',
                        ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-twitter',
                'icon_class' => 'icon-large',
                'title' => esc_html__('Social Profiles', 'byblog'),
                'fields' => array(
                    array(
                        'id'=>'byblog_facebook',
                        'type' => 'text',
                        'title' => esc_html__('Facebook', 'byblog'),
                        'subtitle' => esc_html__('Enter your Facebook URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => 'http://www.facebook.com'
                        ),
                    array(
                        'id'=>'byblog_twitter',
                        'type' => 'text',
                        'title' => esc_html__('Twitter', 'byblog'),
                        'subtitle' => esc_html__('Enter your Twitter URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => 'http://www.twitter.com'
                        ),
                    array(
                        'id'=>'byblog_googleplus',
                        'type' => 'text',
                        'title' => esc_html__('Google Plus', 'byblog'),
                        'subtitle' => esc_html__('Enter your Google Plus URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => 'http://plus.google.com'
                        ),
                    array(
                        'id'=>'byblog_instagram',
                        'type' => 'text',
                        'title' => esc_html__('Instagram', 'byblog'),
                        'subtitle' => esc_html__('Enter your Instagram URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_youtube',
                        'type' => 'text',
                        'title' => esc_html__('YouTube', 'byblog'),
                        'subtitle' => esc_html__('Enter your YouTube URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_pinterest',
                        'type' => 'text',
                        'title' => esc_html__('Pinterest', 'byblog'),
                        'subtitle' => esc_html__('Enter your Pinterest URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_flickr',
                        'type' => 'text',
                        'title' => esc_html__('Flickr', 'byblog'),
                        'subtitle' => esc_html__('Enter your Flickr URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_rss',
                        'type' => 'text',
                        'title' => esc_html__('RSS', 'byblog'),
                        'subtitle' => esc_html__('Enter your RSS URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_linked',
                        'type' => 'text',
                        'title' => esc_html__('LinkedIn', 'byblog'),
                        'subtitle' => esc_html__('Enter your LinkedIn URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_reddit',
                        'type' => 'text',
                        'title' => esc_html__('Reddit', 'byblog'),
                        'subtitle' => esc_html__('Enter your Reddit URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_tumblr',
                        'type' => 'text',
                        'title' => esc_html__('Tumblr', 'byblog'),
                        'subtitle' => esc_html__('Enter your Tumblr URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_git',
                        'type' => 'text',
                        'title' => esc_html__('GitHub', 'byblog'),
                        'subtitle' => esc_html__('Enter your GitHub URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_dribbble',
                        'type' => 'text',
                        'title' => esc_html__('Dribbble', 'byblog'),
                        'subtitle' => esc_html__('Enter your Dribbble URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                    array(
                        'id'=>'byblog_vine',
                        'type' => 'text',
                        'title' => esc_html__('Vine', 'byblog'),
                        'subtitle' => esc_html__('Enter your Vine URL here.', 'byblog'),
                        'validate' => 'url',
                        'default' => ''
                        ),
                )
            );

            $this->sections[] = array(
				'icon' => 'el-icon-picture',
				'icon_class' => 'icon-large',
				'title' => esc_html__('Image Sizes', 'byblog'),
				'fields' => array(
                    array(
                        'id'       =>'byblog_crop_images',
                        'type'     => 'switch', 
                        'title'    => esc_html__('Crop Featured Images', 'byblog'),
                        'subtitle' => esc_html__('Choose this option to crop featured images on posts.', 'byblog'),
                        "default"  => 1,
                        'on'       => 'Yes',
                        'off'      => 'No',
                        ),  
					array(
						'id'       => 'byblog_entry_img_width',
						'type'     => 'text',
						'title'    => esc_html__('Entry Image Width', 'byblog'),
						'subtitle' => esc_html__('Enter width for entry image.', 'byblog'),
                        'validate' => 'numeric',
						'default'  => '870'
					),
					array(
						'id'       => 'byblog_entry_img_height',
						'type'     => 'text',
						'title'    => esc_html__('Entry Image Height', 'byblog'),
						'subtitle' => esc_html__('Enter height for entry image.', 'byblog'),
                        'validate' => 'numeric',
						'default'  => '430'
					),
					array(
						'id'       => 'byblog_widgets_img_width',
						'type'     => 'text',
						'title'    => esc_html__('Widgets Thumbnail Width', 'byblog'),
						'subtitle' => esc_html__('Enter width for thumbnails shown in widgets.', 'byblog'),
                        'validate' => 'numeric',
						'default'  => '68'
					),
					array(
						'id'       => 'byblog_widgets_img_height',
						'type'     => 'text',
						'title'    => esc_html__('Widgets Thumbnail Height', 'byblog'),
						'subtitle' => esc_html__('Enter height for thumbnails shown in widgets.', 'byblog'),
                        'validate' => 'numeric',
						'default'  => '68'
					),
				)
			);

            if ( class_exists( 'woocommerce' ) ) {
                $this->sections[] = array(
                    'icon' => 'el-icon-shopping-cart',
                    'icon_class' => 'icon-large',
                    'title' => esc_html__('WooCommerce', 'byblog'),
                    'fields' => array(
                        array(
                            'id'        => 'byblog_shop_layout',
                            'type'      => 'image_select',
                            'compiler'  => true,
                            'title'     => esc_html__('Shop Layout', 'byblog'), 
                            'subtitle'  => esc_html__('Select layout style for shop pages.', 'byblog'),
                            'options'   => array(
                                    'cblayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/cb.png'),
                                    'bclayout'  => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/images/layouts/bc.png'),
                                    'flayout'   => array('alt' => 'Full Width', 'img' => get_template_directory_uri() .'/images/layouts/f.png'),
                                ),
                            'default'   => 'cblayout'
                            ),
                        array(
                            'id'        => 'byblog_shop_products',
                            'type'      => 'text',
                            'compiler'  => true,
                            'title'     => esc_html__('Number of Products', 'byblog'), 
                            'subtitle'  => esc_html__('Insert number of products to show on  shop page.', 'byblog'),
                            'default'   => '9'
                            ),
                        array(
                            'id'        => 'byblog_header_cart_icon',
                            'type'      => 'switch', 
                            'title'     => esc_html__('Cart Icon on Header', 'byblog'),
                            'subtitle'  => esc_html__('Choose this option to show or hide cart icon on header.', 'byblog'),
                            'default'   => 0,
                            'on'        => esc_html__('Show', 'byblog'),
                            'off'       => esc_html__('Hide', 'byblog'),
                            ),
                    )
                );
            }

            $this->sections[] = array(
                'icon' => 'el-icon-check',
                'icon_class' => 'icon-large',
                'title' => esc_html__('Updates', 'byblog'),
                'fields' => array(
                    array(
                        'id'=>'byblog_envato_user_name',
                        'type' => 'text',
                        'title' => esc_html__('Envato Username', 'byblog'), 
                        'subtitle' => esc_html__('Enter your Envato (ThemeForest) username here.', 'byblog'),
                        'default' => ""
                        ),
                    array(
                        'id'=>'byblog_envato_api_key',
                        'type' => 'text',
                        'title' => esc_html__('Envato API Key', 'byblog'), 
                        'subtitle' => esc_html__('Enter your Envato API key here.', 'byblog'),
                        'default' => ""
                        ),
                )
            );

            $this->sections[] = array(
                'type' => 'divide',
            );

            $theme_info  = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . wp_kses(__('<strong>Theme URL:</strong> ', 'byblog'), array( 'strong' => array() ) ) . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . wp_kses(__('<strong>Author:</strong> ', 'byblog'), array( 'strong' => array() ) ) . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . wp_kses(__('<strong>Version:</strong> ', 'byblog'), array( 'strong' => array() ) ) . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . wp_kses(__('<strong>Tags:</strong> ', 'byblog'), array( 'strong' => array() ) ) . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';

            $this->sections[] = array(
                'title'     => esc_html__('Import / Export', 'byblog'),
                'desc'      => esc_html__('Import and Export your Redux Framework settings from file, text or URL.', 'byblog'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            );
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => esc_html__('Theme Information 1', 'byblog'),
                'content'   => wp_kses(__('<p>This is the tab content, HTML is allowed.</p>', 'byblog'), array( 'p' => array() ) )
            );

            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => esc_html__('Theme Information 2', 'byblog'),
                'content'   => wp_kses(__('<p>This is the tab content, HTML is allowed.</p>', 'byblog'), array( 'p' => array() ) )
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = wp_kses(__('<p>This is the sidebar content, HTML is allowed.</p>', 'byblog'), array( 'p' => array() ) );
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'byblog_options',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'byblog'),
                'page_title'        => esc_html__('Theme Options', 'byblog'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => 'AIzaSyAX_2L_UzCDPEnAHTG7zhESRVpMPS4ssII', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.


                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                'ajax_save'          =>true,
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );

            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
                //$this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'byblog'), $v);
            } else {
                $this->args['intro_text'] = wp_kses(__('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'byblog'), array( 'p' => array() ) );
            }

            // Add content after the form.
            //$this->args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'byblog');
        }

    }
    
    global $reduxConfig;
    $reduxConfig = new Redux_Framework_sample_config();
}

/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;

/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';

        /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;
