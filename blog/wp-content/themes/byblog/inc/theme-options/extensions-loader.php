<?php
if( !function_exists('byblog_register_custom_extension_loader') ) :
    function byblog_register_custom_extension_loader($ReduxFramework) {
        $path = get_template_directory() . '/inc/theme-options/extensions/';
            $folders = scandir( $path, 1 );
            foreach ( $folders as $folder ) {
                if ( $folder === '.' or $folder === '..' or ! is_dir( $path . $folder ) ) {
                    continue;
                }
                $extension_class = 'ReduxFramework_Extension_' . $folder;
                if ( ! class_exists( $extension_class ) ) {
                    // In case you wanted override your override, hah.
                    $class_file = $path . $folder . '/extension_' . $folder . '.php';
                    $class_file = apply_filters( 'redux/extension/' . $ReduxFramework->args['opt_name'] . '/' . $folder, $class_file );
                    if ( $class_file ) {
                        require_once( $class_file );
                    }
                }
                if ( ! isset( $ReduxFramework->extensions[ $folder ] ) ) {
                    $ReduxFramework->extensions[ $folder ] = new $extension_class( $ReduxFramework );
                }
            }
    }
    // Modify {$redux_opt_name} to match your opt_name
    add_action("redux/extensions/byblog_options/before", 'byblog_register_custom_extension_loader', 0);
endif;

if ( !function_exists( 'byblog_filter_title' ) ) {
	/**
	 * Filter for changing demo title in options panel so it's not folder name.
	 *
	 * @param [string] $title name of demo data folder
	 *
	 * @return [string] return title for demo name.
	 */
	function byblog_filter_title( $title ) {
		return trim( ucfirst( str_replace( "-", " ", $title ) ) );
	}
	 add_filter( 'wbc_importer_directory_title', 'byblog_filter_title', 10 );
}

if ( !function_exists( 'byblog_set_demo_menu' ) ) {
	function byblog_set_demo_menu( $demo_active_import , $demo_directory_path ) {
		reset( $demo_active_import );
		$current_key = key( $demo_active_import );
		/************************************************************************
		* Setting Menus
		*************************************************************************/
		
		$wbc_menu_array = array( 'food-blog', 'fashion-blog', 'tech-blog', 'health-blog', 'blogging-tips-blog', 'photography-blog', 'travel-blog' );
        
		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {
			$top_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
			if ( isset( $top_menu->term_id ) ) {
				set_theme_mod( 'nav_menu_locations', array(
						'main-menu' => $top_menu->term_id,
					)
				);
			}
		}
        add_action( 'wbc_importer_after_content_import', 'byblog_set_demo_menu', 10, 2 );
    }
}

?>