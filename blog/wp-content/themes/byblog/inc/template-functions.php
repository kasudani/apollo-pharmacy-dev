<?php
/**
 * Custom template tags for Byblog
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

/*-----------------------------------------------------------------------------------*/
/*	Theme Layout Classes
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'byblog_layout_class' ) ) :
	function byblog_layout_class() {
		global $byblog_options;
		
		$byblog_class = '';
		
		if( is_home() || is_front_page() || is_page_template('template-popular.php') || is_404() ) {
			if( $byblog_options['byblog_layout'] == 'clayout' || $byblog_options['byblog_layout'] == 'gslayout' || $byblog_options['byblog_layout'] == 'sglayout' || $byblog_options['byblog_layout'] == 'glayout' ) {
				$byblog_class = 'masonry masonry-home ' . $byblog_options['byblog_layout'];
			} else {
				$byblog_class = $byblog_options['byblog_layout'];
			}
		}
        elseif( is_search() ) {
			if( $byblog_options['byblog_search_layout'] == 'clayout' || $byblog_options['byblog_search_layout'] == 'gslayout' || $byblog_options['byblog_search_layout'] == 'sglayout' || $byblog_options['byblog_search_layout'] == 'glayout' ) {
				$byblog_class = 'masonry masonry-home ' . $byblog_options['byblog_search_layout'];
			} else {
				$byblog_class = $byblog_options['byblog_search_layout'];
			}
		}
		elseif( is_archive() || is_author() ) {
            if ( is_category() ) {
                $category = get_the_category();
                $category_ID =  get_cat_ID( single_cat_title( '', false ) );
                $byblog_category_style = get_tax_meta($category_ID,'byblog_category_style');
                
                if ( !empty ( $byblog_category_style ) ) {
                    if( $byblog_category_style == 'clayout' || $byblog_category_style == 'gslayout' || $byblog_category_style == 'sglayout' || $byblog_category_style == 'glayout' ) {
                        $byblog_class = 'masonry masonry-archive ' . $byblog_category_style;
                    } else {
                        $byblog_class = $byblog_category_style;
                    }
                }
                else {
                    if( $byblog_options['byblog_archive_layout'] == 'clayout' || $byblog_options['byblog_archive_layout'] == 'gslayout' || $byblog_options['byblog_archive_layout'] == 'sglayout' || $byblog_options['byblog_archive_layout'] == 'glayout' ) {
                        $byblog_class = 'masonry masonry-archive ' . $byblog_options['byblog_archive_layout'];
                    } else {
                        $byblog_class = $byblog_options['byblog_archive_layout'];
                    }
                }
            } else {
                if( $byblog_options['byblog_archive_layout'] == 'clayout' || $byblog_options['byblog_archive_layout'] == 'gslayout' || $byblog_options['byblog_archive_layout'] == 'sglayout' || $byblog_options['byblog_archive_layout'] == 'glayout' ) {
                    $byblog_class = 'masonry masonry-archive ' . $byblog_options['byblog_archive_layout'];
                } else {
                    $byblog_class = $byblog_options['byblog_archive_layout'];
                }
			}
		}
		elseif( is_single() || is_page() || is_page_template('template-archive.php') ) {
			$byblog_class = $byblog_options['byblog_single_layout'];
		}
		
        if ( $byblog_class ) {
            echo $byblog_class;
        } else {
            echo 'cblayout';
        }
	}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Post Featured Image
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'byblog_featured_image' ) ) :
	function byblog_featured_image( $img_name = 'byblog-featured' ) {
        global $byblog_options;
        if ( has_post_thumbnail() ) {
            $byblog_image_size = ( $byblog_options['byblog_crop_images'] == 2 ? 'full' : $img_name ); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail featured-thumbnail-big" itemprop="image">
                <?php the_post_thumbnail( $byblog_image_size ); ?>
            </a><?php
        }
	}
endif;

if ( ! function_exists( 'byblog_featured_image_full' ) ) :
	function byblog_featured_image_full( $thumb = 'byblog-featured' ) {
        global $byblog_options;
        global $byblog_post_pos_count;
        
        $layouts = array( 'gslayout', 'sglayout', 'clayout' );
        
        if( is_home() || is_front_page() ) {
            if( $byblog_options['byblog_layout'] == 'cblayout' || $byblog_options['byblog_layout'] == 'bclayout' ) {
                $thumb = 'byblog-featured';
            }
            if( $byblog_options['byblog_layout'] == 'lslayout' || $byblog_options['byblog_layout'] == 'sllayout' ) {
                if ( $byblog_post_pos_count == 0 ) {
                    $thumb = 'byblog-featured';
                } else {
                    $thumb = 'byblog-featuredthumb';
                }
            }
            if( $byblog_options['byblog_layout'] == 'gsblayout' || $byblog_options['byblog_layout'] == 'sgblayout' ) {
                if ( $byblog_post_pos_count == 0 ) {
                    $thumb = 'byblog-featured';
                } else {
                    $thumb = 'byblog-featuredgrid';
                }
            }
            if ( in_array($byblog_options['byblog_layout'], $layouts) ) {
                $thumb = 'byblog-featuredgrid';
            }
        } elseif( is_search() ) {
            if( $byblog_options['byblog_search_layout'] == 'cblayout' || $byblog_options['byblog_search_layout'] == 'bclayout' ) {
                $thumb = 'byblog-featured';
            }
            if( $byblog_options['byblog_search_layout'] == 'lslayout' || $byblog_options['byblog_search_layout'] == 'sllayout' ) {
                if ( $byblog_post_pos_count == 0 ) {
                    $thumb = 'byblog-featured';
                } else {
                    $thumb = 'byblog-featuredthumb';
                }
            }
            if( $byblog_options['byblog_search_layout'] == 'gsblayout' || $byblog_options['byblog_search_layout'] == 'sgblayout' ) {
                if ( $byblog_post_pos_count == 0 ) {
                    $thumb = 'byblog-featured';
                } else {
                    $thumb = 'byblog-featuredgrid';
                }
            }
            if (in_array($byblog_options['byblog_search_layout'], $layouts)) {
                $thumb = 'byblog-featuredgrid';
            }
        } elseif ( is_archive() || is_author() ) {
            if( $byblog_options['byblog_archive_layout'] == 'cblayout' || $byblog_options['byblog_archive_layout'] == 'bclayout' ) {
                $thumb = 'byblog-featured';
            }
            if( $byblog_options['byblog_archive_layout'] == 'lslayout' || $byblog_options['byblog_archive_layout'] == 'sllayout' ) {
                if ( $byblog_post_pos_count == 0 ) {
                    $thumb = 'byblog-featured';
                } else {
                    $thumb = 'byblog-featuredthumb';
                }
            }
            if( $byblog_options['byblog_archive_layout'] == 'gsblayout' || $byblog_options['byblog_archive_layout'] == 'sgblayout' ) {
                if ( $byblog_post_pos_count == 0 ) {
                    $thumb = 'byblog-featured';
                } else {
                    $thumb = 'byblog-featuredgrid';
                }
            }
            if (in_array($byblog_options['byblog_archive_layout'], $layouts)) {
                $thumb = 'byblog-featuredgrid';
            }
        }
        
        byblog_featured_image( $thumb );
	}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Add Span tag Around Categories and Archives Post Count
/*-----------------------------------------------------------------------------------*/
if( !function_exists('byblog_cat_count') ){ 
	function byblog_cat_count($links) {
		return str_replace(array('</a> (',')'), array('<span class="cat-count">','</span></a>'), $links);
	}
}
add_filter('wp_list_categories', 'byblog_cat_count');

if( !function_exists('byblog_archive_count') ){
	function byblog_archive_count($links) {
	  	return str_replace(array('</a>&nbsp;(',')'), array('<span class="cat-count">','</span></a>'), $links);
	}
}
add_filter('get_archives_link', 'byblog_archive_count');

/*-----------------------------------------------------------------------------------*/
/*	Modify <!--more--> Tag in Posts
/*-----------------------------------------------------------------------------------*/
// Prevent Page Scroll When Clicking the More Link
function byblog_remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'byblog_remove_more_link_scroll' );

/*-----------------------------------------------------------------------------------*/
/*	Pagination
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'byblog_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Byblog 1.0
 */
function byblog_paging_nav() {
	global $byblog_options;
	
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

    // Set up paginated links.
    if ($byblog_options['byblog_rtl'] == '1') {
        $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => $GLOBALS['wp_query']->max_num_pages,
            'current'  => $paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => esc_html__( '&rarr; Previous', 'byblog' ),
            'next_text' => esc_html__( 'Next &larr;', 'byblog' ),
        ) );
    } else {
        $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => $GLOBALS['wp_query']->max_num_pages,
            'current'  => $paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => esc_html__( '&larr; Previous', 'byblog' ),
            'next_text' => esc_html__( 'Next &rarr;', 'byblog' ),
        ) );
    }
	if ($byblog_options['byblog_pagination_type'] == '1') :
		if ( $links ) :

		?>
		<nav class="navigation paging-navigation">
			<div class="pagination loop-pagination">
				<?php echo $links; ?>
			</div><!-- .pagination -->
		</nav><!-- .navigation -->
		<?php
		endif;
	else:
	?>
		<nav class="norm-pagination">
			<div class="nav-previous"><?php next_posts_link( esc_html__( '&larr; Older posts', 'byblog' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( esc_html__( 'Newer posts &rarr;', 'byblog' ) ); ?></div>
		</nav>
	<?php
	endif;
}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Facebook Open Graph Data
/*-----------------------------------------------------------------------------------*/
if ( isset( $byblog_options['byblog_fb_og'] ) ) {
    if ( $byblog_options['byblog_fb_og'] == '1' ) {
        //Adding the Open Graph in the Language Attributes
        function byblog_add_opengraph_doctype( $output ) {
            return $output . ' prefix="og: http://ogp.me/ns#"';
        }
        add_filter( 'language_attributes', 'byblog_add_opengraph_doctype' );

        // Add Facebook Open Graph Tags
        function byblog_fb_og_tags() {
            global $byblog_options;
            global $post;

            if ( have_posts() ):while(have_posts()):the_post(); endwhile; endif;

            if ( is_single() || is_page() ){

                if(get_the_post_thumbnail($post->ID, 'thumbnail')) {
                    $byblog_thumbnail_id = get_post_thumbnail_id($post->ID);
                    $byblog_thumbnail_object = get_post($byblog_thumbnail_id);
                    $byblog_image = $byblog_thumbnail_object->guid;
                } else {	
                    $byblog_image = ''; // Change this to the URL of the logo you want beside your links shown on Facebook
                }

                echo '<meta property="og:title" content="' . get_the_title() . '"/>';
                echo '<meta property="og:url" content="' . get_permalink() . '"/>';
                echo '<meta property="og:type" content="article" />';
                echo '<meta property="og:description" content="' . strip_tags(get_the_excerpt()) . '" />';
                if (!empty($byblog_image)) {
                    echo '<meta property="og:image" content="' . $byblog_image . '" />';
                }

            } elseif( is_home() ){
                if (!empty($byblog_options['byblog_logo']['url'])) {
                    $byblog_image = $byblog_options['byblog_logo']['url'];
                } else {	
                    $byblog_image = ''; // Change this to the URL of the logo you want beside your links shown on Facebook
                }
                echo '<meta property="og:title" content="' . get_bloginfo('name') . ' - ' . get_bloginfo('description') . '"/>';
                echo '<meta property="og:url" content="' . home_url() . '"/>';
                if (!empty($byblog_image)) {
                    echo '<meta property="og:image" content="' . $byblog_image . '" />';
                }
                echo '<meta property="og:type" content="website" />';
            }

            echo '<meta property="og:site_name" content="' . get_bloginfo('name') . '" />';
        }
        add_action( 'wp_head', 'byblog_fb_og_tags', 5 );
    }
}

/*-----------------------------------------------------------------------------------*/
/*	Post Excerpt
/*-----------------------------------------------------------------------------------*/
// Limit the Length of Excerpt from Theme Options
function byblog_excerpt_length( $length ) {
	global $byblog_options;
	if ( $byblog_options['byblog_excerpt_length'] != '') {
		$byblog_excerpt_length = $byblog_options['byblog_excerpt_length'];		
	} else {
		$byblog_excerpt_length = '40';
	}
	
	return $byblog_excerpt_length;
}
add_filter( 'excerpt_length', 'byblog_excerpt_length', 999 );

// Remove […] string
function byblog_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'byblog_excerpt_more');

// Add Shortcodes in Excerpt Field
add_filter( 'get_the_excerpt', 'do_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Exceprt Length
/*-----------------------------------------------------------------------------------*/
function byblog_excerpt_limit( $limit ) {
  $byblog_excerpt = explode(' ', get_the_excerpt(), $limit);
    
  if ( count( $byblog_excerpt )>=$limit ) {
    array_pop($byblog_excerpt);
    $byblog_excerpt = implode(" ",$byblog_excerpt).'...';
  } else {
    $byblog_excerpt = implode(" ",$byblog_excerpt);
  }
    
  $byblog_excerpt = preg_replace('`[[^]]*]`','',$byblog_excerpt);
    
  return $byblog_excerpt;
}
add_filter( 'get_the_excerpt', 'do_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Optional Excerpt
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'byblog_excerpt' ) ) :
	/**
	 * Displays the optional excerpt.
	 *
	 * Wraps the excerpt in a div element.
	 *
	 * Create your own byblog_excerpt() function to override in a child theme.
	 *
	 * @since Byblog 1.0
	 *
	 * @param string $class Optional. Class string of the div element. Defaults to 'entry-summary'.
	 */
	function byblog_excerpt( $class = 'entry-summary' ) {
		$class = esc_attr( $class );

		if ( has_excerpt() || is_search() ) : ?>
			<div class="<?php echo $class; ?>">
				<?php the_excerpt(); ?>
			</div><!-- .<?php echo $class; ?> -->
		<?php endif;
	}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Sanitize Hex Color
/*-----------------------------------------------------------------------------------*/
function byblog_sanitize_hex_color( $color ) {
    if ( '' === $color )
        return '';
 
    // 3 or 6 hex digits, or the empty string.
    if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) )
    return $color;
}

/*-----------------------------------------------------------------------------------*/
/*	Jetpack Infinite Scroll Support
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'infinite-scroll', array(
	'container'    => 'content',
	'render'       => 'byblog_infinite_scroll_render',
	'footer'       => 'footer'
) );

function byblog_infinite_scroll_render() {
    while ( have_posts() ) {
		the_post();
        get_template_part( 'template-parts/post-formats/content', get_post_format() );
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Social Links
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'byblog_social_links' ) ) :

function byblog_social_links( $social_links_pos ) {
	global $byblog_options;
	if ($byblog_options['byblog_social_links'] == '1' || $byblog_options['byblog_footer_social_links'] == '1') {
		
		if ($social_links_pos == 'header') { ?>
			<div class="social-links header-links"><?php
			$byblog_social_link_array = $byblog_options['byblog_social_links_sort'];
		} else { ?>
			<div class="social-links footer-links"><?php
			$byblog_social_link_array = $byblog_options['byblog_social_links_footer_sort'];
		}

		foreach ($byblog_social_link_array as $key=>$value) {
			//echo "<span>$value</span> <br>";	  
			if($key == "fb" && $value == "1") { ?>
				<!-- Facebook -->
				<?php if ($byblog_options['byblog_facebook'] != '') { ?><a class="facebook" href="<?php echo esc_url($byblog_options['byblog_facebook']); ?>" target="_blank"><span class="fa fa-facebook" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Facebook','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "twitter" && $value == "1") { ?>
				<!-- Twitter -->
				<?php if ($byblog_options['byblog_twitter'] != '') { ?><a class="twitter" href="<?php echo esc_url($byblog_options['byblog_twitter']); ?>" target="_blank"><span class="fa fa-twitter" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Twitter','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "gplus" && $value == "1") { ?>
				<!-- Google+ -->
				<?php if ($byblog_options['byblog_googleplus'] != '') { ?><a class="gplus" href="<?php echo esc_url($byblog_options['byblog_googleplus']); ?>" target="_blank"><span class="fa fa-google-plus" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Google+','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "pinterest" && $value == "1") { ?>
				<!-- Pinterest -->
				<?php if ($byblog_options['byblog_pinterest'] != '') { ?><a class="pinterest" href="<?php echo esc_url($byblog_options['byblog_pinterest']); ?>" target="_blank"><span class="fa fa-pinterest" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Pinterest','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "linkedin" && $value == "1") { ?>
				<!-- LinkedIn -->
				<?php if ($byblog_options['byblog_linked'] != '') { ?><a class="linkedin" href="<?php echo esc_url($byblog_options['byblog_linked']); ?>" target="_blank"><span class="fa fa-linkedin" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('LinkedIn','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "youtube" && $value == "1") { ?>
				<!-- YouTube -->
				<?php if ($byblog_options['byblog_youtube'] != '') { ?><a class="youtube" href="<?php echo esc_url($byblog_options['byblog_youtube']); ?>" target="_blank"><span class="fa fa-youtube-play" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('YouTube','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "instagram" && $value == "1") { ?>
				<!-- Instagram -->
				<?php if ($byblog_options['byblog_instagram'] != '') { ?><a class="instagram" href="<?php echo esc_url($byblog_options['byblog_instagram']); ?>" target="_blank"><span class="fa fa-instagram" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Instagram','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "rss" && $value == "1") { ?>
				<!-- RSS -->
				<?php if ($byblog_options['byblog_rss'] != '') { ?><a class="rss" href="<?php echo esc_url($byblog_options['byblog_rss']); ?>" target="_blank"><span class="fa fa-rss" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('RSS','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "reddit" && $value == "1") { ?>
				<!-- Reddit -->
				<?php if ($byblog_options['byblog_reddit'] != '') { ?><a class="reddit" href="<?php echo esc_url($byblog_options['byblog_reddit']); ?>" target="_blank"><span class="fa fa-reddit" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Reddit','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "tumblr" && $value == "1") { ?>
				<!-- Tumblr -->
				<?php if ($byblog_options['byblog_tumblr'] != '') { ?><a class="tumblr" href="<?php echo esc_url($byblog_options['byblog_tumblr']); ?>" target="_blank"><span class="fa fa-tumblr" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Tumblr','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "flickr" && $value == "1") { ?>
				<!-- Flickr -->
				<?php if ($byblog_options['byblog_flickr'] != '') { ?><a class="flickr" href="<?php echo esc_url($byblog_options['byblog_flickr']); ?>" target="_blank"><span class="fa fa-flickr" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Flickr','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "git" && $value == "1") { ?>
				<!-- GitHub -->
				<?php if ($byblog_options['byblog_git'] != '') { ?><a class="git" href="<?php echo esc_url($byblog_options['byblog_git']); ?>" target="_blank"><span class="fa fa-github" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('GitHub','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "dribbble" && $value == "1") { ?>
				<!-- Dribbble -->
				<?php if ($byblog_options['byblog_dribbble'] != '') { ?><a class="dribbble" href="<?php echo esc_url($byblog_options['byblog_dribbble']); ?>" target="_blank"><span class="fa fa-dribbble" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Dribbble','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "vine" && $value == "1") { ?>
				<!-- Vine -->
				<?php if ($byblog_options['byblog_vine'] != '') { ?><a class="vine" href="<?php echo esc_url($byblog_options['byblog_vine']); ?>" target="_blank"><span class="fa fa-vine" aria-hidden="true" ></span><span class="screen-reader-text"><?php _e('Vine','byblog'); ?></span></a><?php } ?>
			<?php }
			elseif($key == "") {
				echo "";
			}
		} ?>
		</div>
	<?php }
}

endif;

/*-----------------------------------------------------------------------------------*/
/*	Add Extra Fields to User Profiles
/*-----------------------------------------------------------------------------------*/
add_action( 'show_user_profile', 'byblog_user_profile_fields' );
add_action( 'edit_user_profile', 'byblog_user_profile_fields' );

function byblog_user_profile_fields( $user ) { ?>

<h3><?php esc_html_e("Author Page Settings", "byblog"); ?></h3>

<table class="form-table">
	<tr>
        <th><label for="user_meta_image"><?php esc_html_e( 'Cover Image', 'byblog' ); ?></label></th>
        <td>
        	<?php $byblog_author_attachment = get_the_author_meta( 'author-attachment-url', $user->ID );
        	if ( !empty( $byblog_author_attachment ) ) { ?>
        		<img class="author_bg_image" src="<?php echo esc_url( get_the_author_meta( 'author-attachment-url', $user->ID ) ); ?>" width="400" /><br />
        	<?php } else { ?>
        		<img class="hide author_bg_image" src="<?php echo esc_url( get_the_author_meta( 'author-attachment-url', $user->ID ) ); ?>" width="400" /><br />
        	<?php } ?>
            <input class="author-media-url regular-text" type="text" id="author-attachment-url" name="author-attachment-url" value="<?php echo esc_url( get_the_author_meta( 'author-attachment-url', $user->ID ) ); ?>">
            <input type='button' class="custom_media_upload button-primary" value="<?php esc_html_e( 'Upload Image', 'byblog' ); ?>" id="uploadimage"/>
            <input type='button' class="author-image-remove button-primary" value="<?php esc_html_e( 'Remove Image', 'byblog' ); ?>" id="removeimage"/><br />
            <span class="description"><?php esc_html_e( 'Upload cover image for your author page. This image will be displayed as background of author box on author page.', 'byblog' ); ?></span>
        </td>
    </tr>
</table>

<h3><?php esc_html_e("Social Profiles", "byblog"); ?></h3>

<table class="form-table">
	<tr>
		<th><label for="facebook"><?php esc_html_e("Facebook","byblog"); ?></label></th>
		<td>
		<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php esc_html_e("Enter your facebook profile URL.","byblog"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="twitter"><?php esc_html_e("Twitter","byblog"); ?></label></th>
		<td>
		<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php esc_html_e("Enter your twitter profile URL.","byblog"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="googleplus"><?php esc_html_e("Google+","byblog"); ?></label></th>
		<td>
		<input type="text" name="googleplus" id="googleplus" value="<?php echo esc_attr( get_the_author_meta( 'googleplus', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php esc_html_e("Enter your Google+ profile URL.","byblog"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="linkedin"><?php esc_html_e("LinkedIn","byblog"); ?></label></th>
		<td>
		<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php esc_html_e("Enter your LinkedIn profile URL.","byblog"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="pinterest"><?php esc_html_e("Pinterest","byblog"); ?></label></th>
		<td>
		<input type="text" name="pinterest" id="pinterest" value="<?php echo esc_attr( get_the_author_meta( 'pinterest', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php esc_html_e("Enter your Pinterest profile URL.","byblog"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="dribbble"><?php esc_html_e("Dribbble","byblog"); ?></label></th>
		<td>
		<input type="text" name="dribbble" id="dribbble" value="<?php echo esc_attr( get_the_author_meta( 'dribbble', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php esc_html_e("Enter your Dribbble profile URL.","byblog"); ?></span>
		</td>
	</tr>
</table>
<?php }
add_action( 'personal_options_update', 'byblog_save_byblog_user_profile_fields' );
add_action( 'edit_user_profile_update', 'byblog_save_byblog_user_profile_fields' );

function byblog_save_byblog_user_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

	update_user_meta( $user_id, 'author-attachment-url', $_POST['author-attachment-url'] );
	update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	update_user_meta( $user_id, 'googleplus', $_POST['googleplus'] );
	update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
	update_user_meta( $user_id, 'pinterest', $_POST['pinterest'] );
	update_user_meta( $user_id, 'dribbble', $_POST['dribbble'] );
}

function byblog_load_wp_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'byblog_load_wp_media_files' );
