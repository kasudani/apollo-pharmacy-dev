<?php
require_once get_template_directory() . "/inc/category-meta/Tax-meta-class/Tax-meta-class.php";
if (is_admin()){

  $prefix = 'byblog_';

  $config = array(
    'id' => 'byblog_cat_meta_box',          // meta box id, unique per meta box
    'title' => 'BloomPixel Meta Box',    // meta box title
    'pages' => array('category'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  /*
   * Initiate your meta box
   */
  $my_meta =  new Tax_Meta_Class($config);
  
  /*
   * Add fields to your meta box
   */
  
  // Category Cover Image
  $my_meta->addImage( $prefix.'cat_cover_img', array( 'name'=> esc_html__( 'Category Cover Image ','byblog') ) );
    
  // Category Style
  $my_meta->addSelect( $prefix.'category_style',
      array(
          'cblayout' => esc_html__( 'Classic Blog with Right Sidebar','byblog' ),
          'bclayout' => esc_html__( 'Classic Blog with Left Sidebar','byblog' ),
          'lslayout' => esc_html__( 'List Style with Right Sidebar','byblog' ),
          'sllayout' => esc_html__( 'List Style with Left Sidebar','byblog' ),
          'gsblayout' => esc_html__( '2 Column Grid with First Big Post and Right Sidebar','byblog' ),
          'sgblayout' => esc_html__( '2 Column Grid with First Big Post and Left Sidebar','byblog' ),
          'sglayout' => esc_html__( '2 Column Grid with Left Sidebar','byblog' ),
          'gslayout' => esc_html__( '2 Column Grid with Right Sidebar','byblog' ),
          'clayout' => esc_html__( '3 Column Grid','byblog' ),
          'glayout' => esc_html__( '2 Column Grid','byblog' ),
      ),
      array( 'name'=> esc_html__( 'Category Style','byblog' ), 'std'=> array('style_1') )
  );
  
  $my_meta->Finish();
}
