<?php

/*-----------------------------------------------------------------------------------

	Plugin Name: Popular Posts Widget
	Plugin URI: http://www.bloompixel.com
	Description: A widget that displays popular posts.
	Version: 1.0
	Author: BloomPixel
	Author URI: http://www.bloompixel.com

-----------------------------------------------------------------------------------*/

add_action( 'widgets_init', 'byblog_popular_posts_widget' );  

// Register Widget
function byblog_popular_posts_widget() {
    register_widget( 'byblog_popular_widget' );
}

// Widget Class
class byblog_popular_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'byblog_popular_widget', 

        // Widget name will appear in UI
        esc_html__('(Byblog) Popular Posts', 'byblog'), 

        // Widget description
        array( 'description' => esc_html__( 'A widget that displays the popular posts of your blog', 'byblog' ), ) 
        );
    }
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
        $title_icon = ( ! empty( $instance['title_icon'] ) ) ? $instance['title_icon'] : '';
		$posts = $instance['posts'];
		$show_thumb = (int) $instance['show_thumb'];
		$show_cat = (int) $instance['show_cat'];
		$show_author = (int) $instance['show_author'];
		$show_date = (int) $instance['show_date'];
		$show_comments = (int) $instance['show_comments'];
		$widget_style = $instance['widget_style'];
		
		// Before Widget
		echo $before_widget;
		
		// Display the widget title
		if ( $title ) {
            if ( $title_icon ) {
                echo $before_title . '<i class="fa fa-'.$title_icon.'"></i>' . $title . $after_title;
            } else {
                echo $before_title . $title . $after_title;
            }
        }
		?>
		<!-- START WIDGET -->
		<ul class="popular-posts popular-posts-widget">
			<?php
				$args = array(
                    'showposts' => $posts,
                    'orderby'	=> 'comment_count',
                    'order'		=> 'DESC',
                    'ignore_sticky_posts' => '1',
                );
				$popularposts = new WP_Query( $args );

                if($popularposts->have_posts()) : while ($popularposts->have_posts()) : $popularposts->the_post();

                if ( $widget_style == 'style-one' ) {
                    $thumbnail = 'byblog-widgetthumb';
                    $thumb_class = '';
                    $list_class = '';
                } else {
                    $thumbnail = 'byblog-widththumb-full';
                    $thumb_class = ' clearfix thumbnail-big heading';
                    $list_class = 'class="style-two"';
                }
            ?>
            <li <?php echo $list_class; ?>>
                <?php if ( $show_thumb == 1 ) { ?>
                    <?php if(has_post_thumbnail()): ?>
                        <div class="thumbnail<?php echo $thumb_class; ?>">
                            <a class="widgetthumb" href='<?php the_permalink(); ?>'>
                                <?php the_post_thumbnail( $thumbnail ); ?>
                                <div class="fhover"></div>
                            </a>
                        </div>
                    <?php endif; ?>
                <?php
                } ?>
                <div class="info">
                    <span class="widgettitle">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute( array( 'before' => esc_html__('Permalink to: ','byblog'), 'after' => '' ) ); ?>">
                            <?php the_title(); ?>
                        </a>
                    </span>
                    <span class="meta">
                        <?php if ( $show_author == 1 ) { ?>
                            <span class="post-author"><?php the_author_posts_link(); ?></span>
                        <?php } ?>
                        <?php if ( $show_date == 1 ) { ?>
                            <span class="widget-time"><time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php the_time(get_option( 'date_format' )); ?></time></span>
                        <?php } ?>
                        <?php if ( $show_cat == 1 ) { ?>
                            <span class="widget-cats"><?php the_category(', '); ?></span>
                        <?php } ?>
                        <?php if ( $show_comments == 1 ) { ?>
                            <span class="widget-comments"><?php comments_popup_link( '0 Comments', '1 Comment', '% Comments', 'comments-link', ''); ?></span>
                        <?php } ?>
                    </span>
                </div><!--.info-->
            </li>
			<?php endwhile; ?>
			<?php endif; ?>
		</ul>
		<!-- END WIDGET -->
		<?php
		
		// After Widget
		echo $after_widget;
	}
	
	// Update the widget
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['title_icon'] = strip_tags( $new_instance['title_icon'] );
		$instance['posts'] = $new_instance['posts'];
		$instance['show_thumb'] = intval( $new_instance['show_thumb'] );
		$instance['show_cat'] = intval( $new_instance['show_cat'] );
		$instance['show_author'] = intval( $new_instance['show_author'] );
		$instance['show_date'] = intval( $new_instance['show_date'] );
		$instance['show_comments'] = intval( $new_instance['show_comments'] );
		$instance['widget_style'] = strip_tags( $new_instance['widget_style'] );
		return $instance;
	}


	//Widget Settings
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array(
			'title'         => esc_html__('Popular Posts', 'byblog'),
            'title_icon'    => '',
			'posts'         => 4,
			'show_thumb'    => 1,
			'show_cat'      => 0,
			'show_author'   => 0,
			'show_date'     => 1,
			'show_comments' => 0,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$title_icon = isset( $instance['title_icon'] ) ? esc_attr( $instance['title_icon'] ) : '';
		$show_thumb = isset( $instance[ 'show_thumb' ] ) ? esc_attr( $instance[ 'show_thumb' ] ) : 1;
		$show_cat = isset( $instance[ 'show_cat' ] ) ? esc_attr( $instance[ 'show_cat' ] ) : 1;
		$show_author = isset( $instance[ 'show_author' ] ) ? esc_attr( $instance[ 'show_author' ] ) : 1;
		$show_date = isset( $instance[ 'show_date' ] ) ? esc_attr( $instance[ 'show_date' ] ) : 1;
		$show_comments = isset( $instance[ 'show_comments' ] ) ? esc_attr( $instance[ 'show_comments' ] ) : 1;
		$widget_style = isset( $instance['widget_style'] ) ? esc_attr( $instance['widget_style'] ) : '';

		// Widget Title: Text Input
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if(!empty($instance['title'])) { echo $instance['title']; } ?>" class="widefat" type="text" />
		</p>

        <!-- Widget Icon: Select -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title_icon' ); ?>"><?php esc_html_e('Title Icon', 'byblog') ?></label>
			<select id="<?php echo $this->get_field_id('title_icon'); ?>" class="title-icon" name="<?php echo $this->get_field_name('title_icon'); ?>" class="widefat" style="width:100%;">
                
                <option <?php if(empty($iconselect) || $iconselect == 'none') { echo 'selected="selected"'; } ?>><?php esc_html_e('No Icon','byblog'); ?></option>
                    <?php
                    global $byblog_icons_list;
                    $iconselect = $instance['title_icon'];
                    foreach ($byblog_icons_list as $icon_type => $icons_array ) { ?>
                        <optgroup label="<?php echo $icon_type; ?>">
                            <?php foreach ($icons_array as $icon ) { ?>
                                <option value="<?php echo $icon; ?>" <?php if($iconselect == $icon) { echo 'selected="selected"'; } ?>><?php echo $icon; ?></option>
                            <?php } ?>
                        </optgroup>
                    <?php } ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'posts' ); ?>"><?php esc_html_e('Number of posts to show:','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'posts' ); ?>" name="<?php echo $this->get_field_name( 'posts' ); ?>" value="<?php echo intval( $instance['posts'] ); ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'widget_style' ); ?>"><?php esc_html_e( 'Widget Style:','byblog' ); ?></label> 
			<select id="<?php echo $this->get_field_id( 'widget_style' ); ?>" name="<?php echo $this->get_field_name( 'widget_style' ); ?>" style="width:100%;" >
				<option value="style-one" <?php selected( $widget_style, 'style-one' ); ?>><?php esc_html_e( 'Small Thumbnail','byblog' ); ?></option>
				<option value="style-two" <?php selected( $widget_style, 'style-two' ); ?>><?php esc_html_e( 'Big Thumbnail','byblog' ); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_thumb"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_thumb"); ?>" name="<?php echo $this->get_field_name("show_thumb"); ?>" value="1" <?php if (isset($instance['show_thumb'])) { checked( 1, $instance['show_thumb'], true ); } ?> />
				<?php esc_html_e( 'Show Thumbnails', 'byblog'); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_cat"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_cat"); ?>" name="<?php echo $this->get_field_name("show_cat"); ?>" value="1" <?php if (isset($instance['show_cat'])) { checked( 1, $instance['show_cat'], true ); } ?> />
				<?php esc_html_e( 'Show Categories', 'byblog'); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_author"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_author"); ?>" name="<?php echo $this->get_field_name("show_author"); ?>" value="1" <?php if (isset($instance['show_author'])) { checked( 1, $instance['show_author'], true ); } ?> />
				<?php esc_html_e( 'Show Post Author', 'byblog'); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_date"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_date"); ?>" name="<?php echo $this->get_field_name("show_date"); ?>" value="1" <?php if (isset($instance['show_date'])) { checked( 1, $instance['show_date'], true ); } ?> />
				<?php esc_html_e( 'Show Post Date', 'byblog'); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_comments"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_comments"); ?>" name="<?php echo $this->get_field_name("show_comments"); ?>" value="1" <?php if (isset($instance['show_comments'])) { checked( 1, $instance['show_comments'], true ); } ?> />
				<?php esc_html_e( 'Show Post Comments', 'byblog'); ?>
			</label>
		</p>
		<?php
	}
}
?>