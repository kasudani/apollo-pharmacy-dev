<?php

/*-----------------------------------------------------------------------------------

	Plugin Name: Social Widget
	Plugin URI: http://www.bloompixel.com
	facebookription: A widget that displays links to your social profiles.
	Version: 1.0
	Author: BloomPixel
	Author URI: http://www.bloompixel.com

-----------------------------------------------------------------------------------*/

add_action( 'widgets_init', 'byblog_social_widget' );  

// Register Widget
function byblog_social_widget() {
    register_widget( 'byblog_social_widget' );
}

// Widget Class
class byblog_social_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'byblog_social_widget', 

        // Widget name will appear in UI
        esc_html__('(Byblog) Social Widget', 'byblog'), 

        // Widget description
        array( 'description' => esc_html__( 'A widget that displays links to your social profiles', 'byblog' ), ) 
        );
    }
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
        $title_icon = ( ! empty( $instance['title_icon'] ) ) ? $instance['title_icon'] : '';
		$buttons_style = $instance['buttons_style'];
		$social_target = (int) $instance['social_target'];
		$facebook = $instance['facebook'];
		$twitter = $instance['twitter'];
		$gplus = $instance['gplus'];
		$rss = $instance['rss'];
		$pinterest = $instance['pinterest'];
		$linkedin = $instance['linkedin'];
		$flickr = $instance['flickr'];
		$instagram = $instance['instagram'];
		$youtube = $instance['youtube'];
		$tumblr = $instance['tumblr'];
		$dribble = $instance['dribble'];
		$git = $instance['git'];
		$xing = $instance['xing'];
		
		// Before Widget
		echo $before_widget;
		
		?>
		<!-- START WIDGET -->
		<div class="social-widget <?php echo $buttons_style; ?>">
			<?php		
				// Display the widget title  
                if ( $title ) {
                    if ( $title_icon ) {
                        echo $before_title . '<i class="fa fa-'.$title_icon.'"></i>' . $title . $after_title;
                    } else {
                        echo $before_title . $title . $after_title;
                    }
                }
			?>
			<ul>
				<?php if ( $facebook ) { ?>
					<li class="facebook uppercase">
                        <a href="<?php echo esc_url( $facebook ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-facebook" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Facebook','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $twitter ) { ?>
					<li class="twitter uppercase">
                        <a href="<?php echo esc_url( $twitter ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-twitter" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Twitter','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $gplus ) { ?>
					<li class="gplus uppercase">
                        <a href="<?php echo esc_url( $gplus ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-google-plus" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Google+','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $rss ) { ?>
					<li class="rss uppercase">
                        <a href="<?php echo esc_url( $rss ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-rss" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('RSS','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $pinterest ) { ?>
					<li class="pinterest uppercase">
                        <a href="<?php echo esc_url( $pinterest ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-pinterest" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Pinterest','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $linkedin ) { ?>
					<li class="linkedin uppercase">
                        <a href="<?php echo esc_url( $linkedin ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-linkedin" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('LinkedIn','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $flickr ) { ?>
					<li class="flickr uppercase">
                        <a href="<?php echo esc_url( $flickr ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-flickr" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Flickr','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $instagram ) { ?>
					<li class="instagram uppercase">
                        <a href="<?php echo esc_url( $instagram ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-instagram" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Instagram','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $youtube ) { ?>
					<li class="youtube uppercase">
                        <a href="<?php echo esc_url( $youtube ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-youtube-play" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('YouTube','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $tumblr ) { ?>
					<li class="tumblr uppercase">
                        <a href="<?php echo esc_url( $tumblr ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-tumblr" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('tumblr','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $git ) { ?>
					<li class="git uppercase">
                        <a href="<?php echo esc_url( $git ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-github" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('GitHub','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $dribble ) { ?>
					<li class="dribble uppercase">
                        <a href="<?php echo esc_url( $dribble ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-dribbble" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Dribbble','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
				<?php if ( $xing ) { ?>
					<li class="xing uppercase">
                        <a href="<?php echo esc_url( $xing ); ?>" <?php if ( $social_target == 1 ) { echo 'target="_blank"'; } ?>>
                            <span class="fa fa-xing-square" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Xing','byblog'); ?></span>
                        </a>
                    </li>
				<?php } ?>
			</ul>
		</div>
		<!-- END WIDGET -->
		<?php
		
		// After Widget
		echo $after_widget;
	}
	
	// Update the widget
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['social_target'] = intval( $new_instance['social_target'] );
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['title_icon'] = strip_tags( $new_instance['title_icon'] );
		$instance['buttons_style'] = strip_tags( $new_instance['buttons_style'] );
		$instance['id'] = stripslashes( $new_instance['id']);
		$instance['facebook'] = $new_instance['facebook'];
		$instance['twitter'] = $new_instance['twitter'];
		$instance['gplus'] = $new_instance['gplus'];
		$instance['rss'] = $new_instance['rss'];
		$instance['pinterest'] = $new_instance['pinterest'];
		$instance['linkedin'] = $new_instance['linkedin'];
		$instance['flickr'] = $new_instance['flickr'];
		$instance['instagram'] = $new_instance['instagram'];
		$instance['youtube'] = $new_instance['youtube'];
		$instance['tumblr'] = $new_instance['tumblr'];
		$instance['dribble'] = $new_instance['dribble'];
		$instance['git'] = $new_instance['git'];
		$instance['xing'] = $new_instance['xing'];
		return $instance;
	}


	//Widget Settings
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array(
			'title'         => esc_html__('Social Media', 'byblog'),
            'title_icon'    => '',
            'buttons_style' => '',
			'social_target' => 1,
			'facebook'      => '',
			'twitter'       => '',
			'gplus'         => '',
			'rss'           => '',
			'pinterest'     => '',
			'linkedin'      => '',
			'flickr'        => '',
			'instagram'     => '',
			'youtube'       => '',
			'tumblr'        => '',
			'dribble'       => '',
			'git'           => '',
			'xing'          => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$title_icon = isset( $instance['title_icon'] ) ? esc_attr( $instance['title_icon'] ) : '';
		$buttons_style = isset( $instance['buttons_style'] ) ? esc_attr( $instance['buttons_style'] ) : '';
		$social_target = isset( $instance[ 'social_target' ] ) ? esc_attr( $instance[ 'social_target' ] ) : 1;

		// Widget Title: Text Input
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
		</p>

        <!-- Widget Icon: Select -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title_icon' ); ?>"><?php esc_html_e('Title Icon', 'byblog') ?></label>
			<select id="<?php echo $this->get_field_id('title_icon'); ?>" class="title-icon" name="<?php echo $this->get_field_name('title_icon'); ?>" class="widefat" style="width:100%;">
                
                <option <?php if(empty($iconselect) || $iconselect == 'none') { echo 'selected="selected"'; } ?>><?php esc_html_e('No Icon','byblog'); ?></option>
                    <?php
                    global $byblog_icons_list;
                    $iconselect = $instance['title_icon'];
                    foreach ($byblog_icons_list as $icon_type => $icons_array ) { ?>
                        <optgroup label="<?php echo $icon_type; ?>">
                            <?php foreach ($icons_array as $icon ) { ?>
                                <option value="<?php echo $icon; ?>" <?php if($iconselect == $icon) { echo 'selected="selected"'; } ?>><?php echo $icon; ?></option>
                            <?php } ?>
                        </optgroup>
                    <?php } ?>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'buttons_style' ); ?>"><?php esc_html_e( 'Buttons Style:','byblog' ); ?></label> 
			<select id="<?php echo $this->get_field_id( 'buttons_style' ); ?>" name="<?php echo $this->get_field_name( 'buttons_style' ); ?>" class="widefat">
				<option value="square" <?php selected( $buttons_style, 'square' ); ?>><?php esc_html_e( 'Square','byblog' ); ?></option>
				<option value="circular" <?php selected( $buttons_style, 'circular' ); ?>><?php esc_html_e( 'Circular','byblog' ); ?></option>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("social_target"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("social_target"); ?>" name="<?php echo $this->get_field_name("social_target"); ?>" value="1" <?php if (isset($instance['social_target'])) { checked( 1, $instance['social_target'], true ); } ?> />
				<?php esc_html_e( 'Open Links in New Window', 'byblog'); ?>
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php esc_html_e('Facebook URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="<?php echo esc_url( $instance['facebook'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php esc_html_e('Twitter URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo esc_url( $instance['twitter'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'gplus' ); ?>"><?php esc_html_e('Google+ URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'gplus' ); ?>" name="<?php echo $this->get_field_name( 'gplus' ); ?>" value="<?php echo esc_url( $instance['gplus'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'rss' ); ?>"><?php esc_html_e('RSS URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'rss' ); ?>" name="<?php echo $this->get_field_name( 'rss' ); ?>" value="<?php echo esc_url( $instance['rss'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'pinterest' ); ?>"><?php esc_html_e('Pinterest URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'pinterest' ); ?>" name="<?php echo $this->get_field_name( 'pinterest' ); ?>" value="<?php echo esc_url( $instance['pinterest'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'linkedin' ); ?>"><?php esc_html_e('LinkedIn URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" value="<?php echo esc_url( $instance['linkedin'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'flickr' ); ?>"><?php esc_html_e('Flickr URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'flickr' ); ?>" name="<?php echo $this->get_field_name( 'flickr' ); ?>" value="<?php echo esc_url( $instance['flickr'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'instagram' ); ?>"><?php esc_html_e('Instagram URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'instagram' ); ?>" name="<?php echo $this->get_field_name( 'instagram' ); ?>" value="<?php echo esc_url( $instance['instagram'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'youtube' ); ?>"><?php esc_html_e('YouTube URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" value="<?php echo esc_url( $instance['youtube'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'tumblr' ); ?>"><?php esc_html_e('tumblr URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'tumblr' ); ?>" name="<?php echo $this->get_field_name( 'tumblr' ); ?>" value="<?php echo esc_url( $instance['tumblr'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'dribble' ); ?>"><?php esc_html_e('Dribbble URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'dribble' ); ?>" name="<?php echo $this->get_field_name( 'dribble' ); ?>" value="<?php echo esc_url( $instance['dribble'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'git' ); ?>"><?php esc_html_e('GitHub URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'git' ); ?>" name="<?php echo $this->get_field_name( 'git' ); ?>" value="<?php echo esc_url( $instance['git'] ); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'xing' ); ?>"><?php esc_html_e('Xing URL','byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'xing' ); ?>" name="<?php echo $this->get_field_name( 'xing' ); ?>" value="<?php echo esc_url( $instance['xing'] ); ?>" class="widefat" type="text" />
		</p>
		<?php
	}
}
?>