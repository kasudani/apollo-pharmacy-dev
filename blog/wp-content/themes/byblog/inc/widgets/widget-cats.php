<?php

/*-----------------------------------------------------------------------------------

	Plugin Name: Categories Widget
	Plugin URI: http://www.bloompixel.com
	Description: A widget that displays categories with images.
	Version: 1.0
	Author: BloomPixel
	Author URI: http://www.bloompixel.com

-----------------------------------------------------------------------------------*/

add_action( 'widgets_init', 'byblog_categories_widget' );  

// Register Widget
function byblog_categories_widget() {
    register_widget( 'byblog_cats_widget' );
}

// Widget Class
class byblog_cats_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'byblog_cats_widget', 

        // Widget name will appear in UI
        esc_html__('(Byblog) Categories Widget', 'byblog'), 

        // Widget description
        array( 'description' => esc_html__( 'A widget that displays categories with images', 'byblog' ), ) 
        );
    }
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
		$exclude_cats = $instance['exclude_cats'];
        $title_icon = ( ! empty( $instance['title_icon'] ) ) ? $instance['title_icon'] : '';
        $sort_order = ( ! empty( $instance['sort_order'] ) ) ? $instance['sort_order'] : '';
        $limit_cats = ( ! empty( $instance['limit_cats'] ) ) ? $instance['limit_cats'] : 0;
        $hide_empty = ( ! empty( $instance['hide_empty'] ) ) ? $instance['hide_empty'] : 0;
		
		// Before Widget
		echo $before_widget;
		
		// Display the widget title  
		if ( $title ) {
            if ( $title_icon ) {
                echo $before_title . '<i class="fa fa-'.$title_icon.'"></i>' . $title . $after_title;
            } else {
                echo $before_title . $title . $after_title;
            }
        }
		
		?>
		<!-- START WIDGET -->
		<div class="categories-widget t-center uppercase">
			<?php
                if ( !$sort_order ) {
                    $sort_order = 'ASC';
                }
                if ( !$exclude_cats ) {
                    $exclude_cats = '';
                }
                if ( !$limit_cats ) {
                    $limit_cats = 0;
                }
                if ( !$hide_empty ) {
                    $hide_empty = 0;
                }
                $args = array(
                    'type'             => 'post',
                    'child_of'         => 0,
                    'parent'           => '',
                    'orderby'          => 'name',
                    'order'            => $sort_order,
                    'hide_empty'       => $hide_empty,
                    'hierarchical'     => 1,
                    'exclude'          => $exclude_cats,
                    'include'          => '',
                    'number'           => $limit_cats,
                    'taxonomy'         => 'category',
                    'pad_counts'       => false
                );

                $byblog_cats = get_categories( $args ); ?>
                <ul>
                <?php
                    foreach( $byblog_cats as $cat ) {
                        //print_r($cat);
                        $category_ID = $cat->cat_ID;
                        if( get_tax_meta($category_ID,'byblog_cat_cover_img') ) {
                            $byblog_cat_bg = get_tax_meta($category_ID,'byblog_cat_cover_img');
                            $byblog_cat_bg_src = $byblog_cat_bg['src'];
                            $byblog_cat_bg_src = 'style="background:url( '. esc_url( $byblog_cat_bg['src'] ) .' );"';
                        } else {
                            $byblog_cat_bg_src = '';
                        } ?>
                        <li <?php echo $byblog_cat_bg_src; ?>>
                            <a href="<?php echo esc_url( home_url() ); ?>/category/<?php echo trailingslashit( $cat->slug ); ?>">
                                <div class="cats-widget-text transition">
                                    <div class="cats-widget-content">
                                        <?php echo $cat->name; ?>
                                    </div>
                                </div>
                            </a>
                        </li><?php
                    }
                ?>
                </ul>
		</div>
		<!-- END WIDGET -->
		<?php
		
		// After Widget
		echo $after_widget;
	}
	
	// Update the widget
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['exclude_cats'] = $new_instance['exclude_cats'];
		$instance['title_icon'] = strip_tags( $new_instance['title_icon'] );
		$instance['sort_order'] = strip_tags( $new_instance['sort_order'] );
		$instance['limit_cats'] = strip_tags( $new_instance['limit_cats'] );
		$instance['hide_empty'] = strip_tags( $new_instance['hide_empty'] );
		return $instance;
	}


	//Widget Settings
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array(
            'title'        => esc_html__('Categories', 'byblog'),
            'title_icon'   => '',
            'sort_order'   => '',
            'exclude_cats' => '',
            'limit_cats'   => '',
            'hide_empty'   => ''
        );
		$title_icon = isset( $instance['title_icon'] ) ? esc_attr( $instance['title_icon'] ) : '';
		$sort_order = isset( $instance['sort_order'] ) ? esc_attr( $instance['sort_order'] ) : '';
		$exclude_cats = isset( $instance['exclude_cats'] ) ? esc_attr( $instance['exclude_cats'] ) : '';
		$hide_empty = isset( $instance[ 'hide_empty' ] ) ? esc_attr( $instance[ 'hide_empty' ] ) : 0;
		$limit_cats = isset( $instance[ 'limit_cats' ] ) ? esc_attr( $instance[ 'limit_cats' ] ) : 0;
		$instance = wp_parse_args( (array) $instance, $defaults );

		// Widget Title: Text Input
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if(!empty($instance['title'])) { echo $instance['title']; } ?>" class="widefat" type="text" />
		</p>

        <!-- Widget Icon: Select -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title_icon' ); ?>"><?php esc_html_e('Title Icon', 'byblog') ?></label>
			<select id="<?php echo $this->get_field_id('title_icon'); ?>" class="title-icon" name="<?php echo $this->get_field_name('title_icon'); ?>" class="widefat" style="width:100%;">
                
                <option <?php if(empty($iconselect) || $iconselect == 'none') { echo 'selected="selected"'; } ?>><?php esc_html_e('No Icon','byblog'); ?></option>
                    <?php
                    global $byblog_icons_list;
                    $iconselect = $instance['title_icon'];
                    foreach ($byblog_icons_list as $icon_type => $icons_array ) { ?>
                        <optgroup label="<?php echo $icon_type; ?>">
                            <?php foreach ($icons_array as $icon ) { ?>
                                <option value="<?php echo $icon; ?>" <?php if($iconselect == $icon) { echo 'selected="selected"'; } ?>><?php echo $icon; ?></option>
                            <?php } ?>
                        </optgroup>
                    <?php } ?>
			</select>
		</p>

        <!-- Sort Order: Select -->
		<p>
			<label for="<?php echo $this->get_field_id( 'sort_order' ); ?>"><?php esc_html_e( 'Sort Order:','byblog' ); ?></label> 
			<select id="<?php echo $this->get_field_id( 'sort_order' ); ?>" name="<?php echo $this->get_field_name( 'sort_order' ); ?>" style="width:100%;" >
				<option value="ASC" <?php selected( $sort_order, 'ASC' ); ?>><?php esc_html_e( 'Ascending','byblog' ); ?></option>
				<option value="DESC" <?php selected( $sort_order, 'DESC' ); ?>><?php esc_html_e( 'Descending','byblog' ); ?></option>
			</select>
		</p>
		
        <!-- Exclude Categories: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude_cats' ); ?>"><?php esc_html_e('Exclude Categories:', 'byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'exclude_cats' ); ?>" name="<?php echo $this->get_field_name( 'exclude_cats' ); ?>" value="<?php echo $instance['exclude_cats']; ?>" class="widefat" type="text" />
            <small><?php esc_html_e('Add comma-separated list of category IDs. For example: 2,3,4', 'byblog'); ?></small>
		</p>
		
        <!-- Limit Categories: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'limit_cats' ); ?>"><?php esc_html_e('Limit Categories:', 'byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'limit_cats' ); ?>" name="<?php echo $this->get_field_name( 'limit_cats' ); ?>" value="<?php echo $instance['limit_cats']; ?>" class="widefat" type="text" />
            <small><?php esc_html_e('Limit number of categories to show.', 'byblog'); ?></small>
		</p>

        <!-- Hide Empty: Checkbox -->
		<p>
			<label for="<?php echo $this->get_field_id("hide_empty"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("hide_empty"); ?>" name="<?php echo $this->get_field_name("hide_empty"); ?>" value="1" <?php if (isset($instance['hide_empty'])) { checked( 1, $instance['hide_empty'], true ); } ?> />
				<?php esc_html_e( 'Hide Empty Categories', 'byblog'); ?>
			</label>
		</p>
		<?php
	}
}
?>