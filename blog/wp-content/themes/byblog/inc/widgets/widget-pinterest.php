<?php

/*-----------------------------------------------------------------------------------

	Plugin Name: Pinterest Widget
	Plugin URI: http://www.bloompixel.com
	Description: A widget that displays your latest pins.
	Version: 1.0
	Author: BloomPixel
	Author URI: http://www.bloompixel.com

-----------------------------------------------------------------------------------*/

add_action( 'widgets_init', 'byblog_pinterest_pins_widget' );

// Register Widget
function byblog_pinterest_pins_widget() {
    register_widget( 'byblog_pinterest_widget' );
}

// Widget Class
class byblog_pinterest_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'byblog_pinterest_widget', 

        // Widget name will appear in UI
        esc_html__('(Byblog) Pinterest Widget', 'byblog'), 

        // Widget description
        array( 'description' => esc_html__( 'A widget that displays your latest pins', 'byblog' ), ) 
        );
    }
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
		$image_width = $instance['image_width'];
		$widget_width = $instance['widget_width'];
		$widget_height = $instance['widget_height'];
		$user_url = $instance['user_url'];
        $title_icon = ( ! empty( $instance['title_icon'] ) ) ? $instance['title_icon'] : '';
		
		// Before Widget
		echo $before_widget;
		
		// Display the widget title  
		if ( $title ) {
            if ( $title_icon ) {
                echo $before_title . '<i class="fa fa-'.$title_icon.'"></i>' . $title . $after_title;
            } else {
                echo $before_title . $title . $after_title;
            }
        }
		
		?>
		<!-- START WIDGET -->
		<div class="pinterest-widget">
			<?php
				if ( $user_url ) { ?>
					<a data-pin-do="embedUser" href="http://www.pinterest.com/pinterest/" data-pin-scale-width="<?php echo $image_width; ?>" data-pin-scale-height="<?php echo $widget_height; ?>" data-pin-board-width="<?php echo $widget_width; ?>"></a>
<!-- Please call pinit.js only once per page -->
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
				<?php }
			?>
		</div>
		<!-- END WIDGET -->
		<?php
		
		// After Widget
		echo $after_widget;
	}
	
	// Update the widget
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['user_url'] = $new_instance['user_url'];
		$instance['image_width'] = $new_instance['image_width'];
		$instance['widget_width'] = $new_instance['widget_width'];
		$instance['widget_height'] = $new_instance['widget_height'];
		$instance['title_icon'] = strip_tags( $new_instance['title_icon'] );
		return $instance;
	}


	//Widget Settings
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array(
            'title'         => '',
            'title_icon'    => 'pinterest',
            'user_url'      => '',
            'image_width'   => '140',
            'widget_width'  => '280',
            'widget_height' => '250'
        );
		$title_icon = isset( $instance['title_icon'] ) ? esc_attr( $instance['title_icon'] ) : '';
		$instance = wp_parse_args( (array) $instance, $defaults );

		// Widget Title: Text Input
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if(!empty($instance['title'])) { echo $instance['title']; } ?>" class="widefat" type="text" />
		</p>

        <!-- Widget Icon: Select -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title_icon' ); ?>"><?php esc_html_e('Title Icon', 'byblog') ?></label>
			<select id="<?php echo $this->get_field_id('title_icon'); ?>" class="title-icon" name="<?php echo $this->get_field_name('title_icon'); ?>" class="widefat" style="width:100%;">
                
                <option <?php if(empty($iconselect) || $iconselect == 'none') { echo 'selected="selected"'; } ?>><?php esc_html_e('No Icon','byblog'); ?></option>
                    <?php
                    global $byblog_icons_list;
                    $iconselect = $instance['title_icon'];
                    foreach ($byblog_icons_list as $icon_type => $icons_array ) { ?>
                        <optgroup label="<?php echo $icon_type; ?>">
                            <?php foreach ($icons_array as $icon ) { ?>
                                <option value="<?php echo $icon; ?>" <?php if($iconselect == $icon) { echo 'selected="selected"'; } ?>><?php echo $icon; ?></option>
                            <?php } ?>
                        </optgroup>
                    <?php } ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'user_url' ); ?>"><?php esc_html_e('Your Pinterest URL:', 'byblog') ?></label>
			<input id="<?php echo $this->get_field_id( 'user_url' ); ?>" name="<?php echo $this->get_field_name( 'user_url' ); ?>" value="<?php echo $instance['user_url']; ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'image_width' ); ?>"><?php esc_html_e('Image Width:', 'byblog') ?></label>
			<input id="<?php echo $this->get_field_id( 'image_width' ); ?>" name="<?php echo $this->get_field_name( 'image_width' ); ?>" value="<?php echo $instance['image_width']; ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'widget_width' ); ?>"><?php esc_html_e('Widget Width:', 'byblog') ?></label>
			<input id="<?php echo $this->get_field_id( 'widget_width' ); ?>" name="<?php echo $this->get_field_name( 'widget_width' ); ?>" value="<?php echo $instance['widget_width']; ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'widget_height' ); ?>"><?php esc_html_e('Widget Height:', 'byblog') ?></label>
			<input id="<?php echo $this->get_field_id( 'widget_height' ); ?>" name="<?php echo $this->get_field_name( 'widget_height' ); ?>" value="<?php echo $instance['widget_height']; ?>" class="widefat" type="text" />
		</p>
		<?php
	}
}
?>