<?php

/*-----------------------------------------------------------------------------------

	Plugin Name: 300 Ad Widget
	Plugin URI: http://www.bloompixel.com
	Description: A widget that displays 300x250px ad.
	Version: 1.0
	Author: BloomPixel
	Author URI: http://www.bloompixel.com

-----------------------------------------------------------------------------------*/

add_action( 'widgets_init', 'bp_300_ad_widget' );  

// Register Widget
function bp_300_ad_widget() {
    register_widget( 'bp_300_widget' );
}

// Widget Class
class bp_300_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'bp_300_widget', 

        // Widget name will appear in UI
        esc_html__('(Byblog) 300x250 Ad Widget', 'byblog'), 

        // Widget description
        array( 'description' => esc_html__( 'A widget that displays 300x250 ad', 'byblog' ), ) 
        );
    }
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		//Our variables from the widget settings.
		/* $title = apply_filters('widget_title', $instance['title'] ); */
		$byblog_banner = $instance['banner'];
		$byblog_link = $instance['link'];
		$byblog_ad_code = $instance['ad_code'];
		
		// Before Widget
		//echo $before_widget;
		
		// Display the widget title  
		/* if ( $title )
			echo $before_title . $title . $after_title; */
		
		?>
		<!-- START WIDGET -->
		<div class="ad-300-widget">
			<?php
				if ( $byblog_ad_code )
					echo '<div class="ad-block ad-block-300">' . $byblog_ad_code . '</div>';
					
				elseif ( $byblog_link )
					echo '<a href="' . esc_url( $byblog_link ) . '"><img src="' . esc_url( $byblog_banner ) . '" width="300" height="250" alt="'.esc_html_e("Advertisement","byblog").'" /></a>';
					
				elseif ( $byblog_banner )
					echo '<img src="' . esc_url( $byblog_banner ) . '" width="300" height="250" alt="'.esc_html_e("Advertisement","byblog").'" />';
			?>
		</div>
		<!-- END WIDGET -->
		<?php
		
		// After Widget
		//echo $after_widget;
	}
	
	// Update the widget
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		/* $instance['title'] = strip_tags( $new_instance['title'] ); */
		$instance['link'] = $new_instance['link'];
		$instance['banner'] = $new_instance['banner'];
		$instance['ad_code'] = $new_instance['ad_code'];
		return $instance;
	}


	//Widget Settings
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array(
			'link'    => 'http://bloompixel.com/',
			'banner'  => get_template_directory_uri()."/images/300x250.png",
			'ad_code' => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

		// Widget Title: Text Input
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php esc_html_e('Ad Link URL:', 'byblog') ?></label>
			<input id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo esc_url($instance['link']); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'banner' ); ?>"><?php esc_html_e('Ad Banner URL:', 'byblog') ?></label>
			<input id="<?php echo $this->get_field_id( 'banner' ); ?>" name="<?php echo $this->get_field_name( 'banner' ); ?>" value="<?php echo esc_url($instance['banner']); ?>" class="widefat" type="text" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'ad_code' ); ?>"><?php esc_html_e('Ad Code (Google Adsense):', 'byblog') ?></label>
			<textarea id="<?php echo $this->get_field_id( 'ad_code' ); ?>" name="<?php echo $this->get_field_name( 'ad_code' ); ?>" cols="20" rows="10" class="widefat"><?php echo esc_html($instance['ad_code']); ?></textarea>
		</p>
		<?php
	}
}
?>