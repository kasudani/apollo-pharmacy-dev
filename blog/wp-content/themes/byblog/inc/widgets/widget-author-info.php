<?php

/*-----------------------------------------------------------------------------------

	Plugin Name: Author Info Widget
	Plugin URI: http://www.bloompixel.com
	Description: A widget that displays Author Info.
	Version: 1.0

-----------------------------------------------------------------------------------*/

add_action( 'widgets_init', 'byblog_author_widget' );  

// Register Widget
function byblog_author_widget() {
    register_widget( 'byblog_author_info_widget' );
}

// Widget Class
class byblog_author_info_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'author_info_widget', 

        // Widget name will appear in UI
        esc_html__('(Byblog) Author Info Widget', 'byblog'), 

        // Widget description
        array( 'description' => esc_html__( 'A widget that displays the author information', 'byblog' ), ) 
        );
    }
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
		$show_avatar = (int) $instance['show_avatar'];
		$show_social_links = (int) $instance['show_social_links'];
		$show_author_description = (int) $instance['show_author_description'];
		$author_list = $instance['author_list'];
		
		// Before Widget
		echo $before_widget;
		
		// Display the widget title  
		if ( $title )
			echo $before_title . $title . $after_title;
		
		?>
		<!-- START WIDGET -->
		<div class="author-info-widget t-center">
            <?php $tpie_author_id = $author_list; ?>
                <h4>
                    <?php
                        // Author Name
                        echo $tpie_author_name = get_the_author_meta( 'display_name', $tpie_author_id );
                    ?>
                </h4>
                <?php if ( $show_avatar == 1 ) { ?>
                    <div class="author-avatar">
                        <?php
                            // Author Avatar
                            if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta( 'email', $tpie_author_id ), '150' );  }
                        ?>
                    </div>
                <?php } ?>
                <?php if ( $show_author_description == 1 ) { ?>
                    <div class="author-description">
                        <?php
                            // Author Description
                            $tpie_author_description = get_the_author_meta( 'description', $tpie_author_id );
                            echo $tpie_author_description;
                        ?>
                    </div>
                <?php } ?>
                <?php if ( $show_social_links == 1 ) { ?>
                    <div class="author-social">
                        <?php if(get_the_author_meta('facebook', $tpie_author_id)) { ?><span class="author-fb"><a class="fa fa-facebook" href="<?php echo esc_url( get_the_author_meta('facebook', $tpie_author_id) ); ?>"></a></span><?php } ?>
                        <?php if(get_the_author_meta('twitter', $tpie_author_id)) { ?><span class="author-twitter"><a class="fa fa-twitter" href="<?php echo esc_url( get_the_author_meta('twitter', $tpie_author_id) ); ?>"></a></span><?php } ?>
                        <?php if(get_the_author_meta('googleplus', $tpie_author_id)) { ?><span class="author-gp"><a class="fa fa-google-plus" href="<?php echo esc_url( get_the_author_meta('googleplus', $tpie_author_id) ); ?>"></a></span><?php } ?>
                        <?php if(get_the_author_meta('linkedin', $tpie_author_id)) { ?><span class="author-linkedin"><a class="fa fa-linkedin" href="<?php echo esc_url( get_the_author_meta('linkedin', $tpie_author_id) ); ?>"></a></span><?php } ?>
                        <?php if(get_the_author_meta('pinterest', $tpie_author_id)) { ?><span class="author-pinterest"><a class="fa fa-pinterest" href="<?php echo esc_url( get_the_author_meta('pinterest', $tpie_author_id) ); ?>"></a></span><?php } ?>
                        <?php if(get_the_author_meta('dribbble', $tpie_author_id)) { ?><span class="author-dribbble"><a class="fa fa-dribbble" href="<?php echo esc_url( get_the_author_meta('dribble', $tpie_author_id) ); ?>"></a></span><?php } ?>
                    </div>
                <?php } ?>
        </div>
		<!-- END WIDGET -->
		<?php
		
		// After Widget
		echo $after_widget;
	}
	
	// Update the widget
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['posts'] = $new_instance['posts'];
		$instance['heading_background'] = strip_tags( $new_instance['heading_background'] );
		$instance['show_avatar'] = intval( $new_instance['show_avatar'] );
		$instance['show_social_links'] = intval( $new_instance['show_social_links'] );
		$instance['show_author_description'] = intval( $new_instance['show_author_description'] );
		$instance['author_list'] = strip_tags( $new_instance['author_list'] );
		return $instance;
	}


	//Widget Settings
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array(
			'title'                   => '',
			'posts'                   => 4,
			'show_avatar'             => 1,
			'show_social_links'       => 0,
			'show_author_description' => 0,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$heading_background = isset( $instance['heading_background'] ) ? esc_attr( $instance['heading_background'] ) : '';
		$show_avatar = isset( $instance[ 'show_avatar' ] ) ? esc_attr( $instance[ 'show_avatar' ] ) : 1;
		$show_social_links = isset( $instance[ 'show_social_links' ] ) ? esc_attr( $instance[ 'show_social_links' ] ) : 1;
		$show_author_description = isset( $instance[ 'show_author_description' ] ) ? esc_attr( $instance[ 'show_author_description' ] ) : 1;
		$author_list = isset( $instance['author_list'] ) ? esc_attr( $instance['author_list'] ) : '';

		// Widget Title: Text Input
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'byblog'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if(!empty($instance['title'])) { echo $instance['title']; } ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'author_list' ); ?>"><?php esc_html_e( 'Category:','byblog' ); ?></label> 
			<?php wp_dropdown_users( Array(
                    'orderby'            => 'display_name', 
                    'order'              => 'ASC',
                    'echo'               => 1,
                    'selected'           => $author_list,
                    'show'               => 'display_name', 
                    'name'               => $this->get_field_name( 'author_list' ),
                    'id'                 => $this->get_field_id( 'author_list' ),
                ) ); ?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_avatar"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_avatar"); ?>" name="<?php echo $this->get_field_name("show_avatar"); ?>" value="1" <?php if (isset($instance['show_avatar'])) { checked( 1, $instance['show_avatar'], true ); } ?> />
				<?php esc_html_e( 'Show Author Avtar', 'byblog'); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_social_links"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_social_links"); ?>" name="<?php echo $this->get_field_name("show_social_links"); ?>" value="1" <?php if (isset($instance['show_social_links'])) { checked( 1, $instance['show_social_links'], true ); } ?> />
				<?php esc_html_e( 'Show Social Links', 'byblog'); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_author_description"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_author_description"); ?>" name="<?php echo $this->get_field_name("show_author_description"); ?>" value="1" <?php if (isset($instance['show_author_description'])) { checked( 1, $instance['show_author_description'], true ); } ?> />
				<?php esc_html_e( 'Show Author Description', 'byblog'); ?>
			</label>
		</p>
		<?php
	}
}
?>