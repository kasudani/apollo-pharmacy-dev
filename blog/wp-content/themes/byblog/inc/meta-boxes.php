<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */


add_filter( 'rwmb_meta_boxes', 'byblog_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function byblog_register_meta_boxes( $meta_boxes )
{
	/**
	 * Prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'byblog_';
	
	// Standard meta box
	$meta_boxes[] = array(
		'id'       => 'custom_sidebars_box',
		'title'    => esc_html__( 'Custom Sidebar', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'side',
		'priority' => 'core',
		'autosave' => true,

		// List of meta fields
		'fields'   => array(
			// Background Attachment
			array(
				'name'    => esc_html__( 'Choose the sidebar for this post', 'byblog' ),
				'desc'    => esc_html__( '', 'byblog' ),
				'id'      => "{$prefix}custom_sidebar",
				'type'    => 'radio',
				'options' => byblog_custom_sidebars(),
				'std'     => 'default',
			),
		),
	);
	
	// Standard meta box
	$meta_boxes[] = array(
		'id'       => 'standardbox',
		'title'    => esc_html__( 'Standard Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,

		// List of meta fields
		'fields'   => array(
			// Featured Image
			array(
				'name' => esc_html__( 'Hide Featured Image?', 'byblog' ),
				'id'   => "{$prefix}standard_single_hide",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to hide featured image on this post.', 'byblog' ),
				// Value can be 0 or 1
				'std'  => 0,
			),
		),
	);
	
	// Image meta box
	$meta_boxes[] = array(
		'id'       => 'imagebox',
		'title'    => esc_html__( 'Image Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,

		// List of meta fields
		'fields'   => array(
			// CHECKBOX
			array(
				'name' => esc_html__( 'Hide Featured Image?', 'byblog' ),
				'id'   => "{$prefix}image_single_hide",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to hide featured image on this post.', 'byblog' ),
				'std'  => 0,
			),
		),
	);
	
	// Audio meta box
	$meta_boxes[] = array(
		'id'       => 'audiobox',
		'title'    => esc_html__( 'Audio Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			// SELECT BOX
			array(
				'name'     => esc_html__( 'Audio File Host', 'byblog' ),
				'desc'     => wp_kses(__( 'Select host of your audio file. <br> <strong>Note:</strong> If you couldn\'t find host of your audio file in the dropdown list, you can paste the embed code of file in the "Audio Embed Code" box.', 'byblog' ), array( 'strong' => array(), 'br' => array() ) ),
				'id'       => "{$prefix}audiohost",
				'type'     => 'select',
				'options'  => array(
					'soundcloud' => esc_html__( 'SoundCloud', 'byblog' ),
					'mixcloud'   => esc_html__( 'MixCloud', 'byblog' ),
				),
				'multiple'    => false,
				'std'         => '',
				'placeholder' => esc_html__( 'Select an Item', 'byblog' ),
			),
			// TEXT
			array(
				'name'  => esc_html__( 'Audio File URL (Soundcloud or Mixcloud) ', 'byblog' ),
				'id'    => "{$prefix}audiourl",
				'desc'  => esc_html__( 'Enter URL of your audio file here.', 'byblog' ),
				'type'  => 'text',
				'std'   => '',
				'clone' => false,
			),
			// TEXTAREA
			array(
				'name' => esc_html__( 'Audio Embed Code', 'byblog' ),
				'desc' => esc_html__( 'Enter embed code of your audio file here.', 'byblog' ),
				'id'   => "{$prefix}audiocode",
				'type' => 'textarea',
				'cols' => 20,
				'rows' => 3,
			),
			// mp3 audio
			array(
				'name'  => esc_html__( 'Self Hosted Audio', 'byblog' ),
				'id'    => "{$prefix}mp3url",
				'desc'  => esc_html__( 'Upload your audio file here. Supported formats are: mp3, m4a, ogg, wav and wma.', 'byblog' ),
				'type'  => 'file_advanced',
				'std'   => '',
				'max_file_uploads'   => 1,
			),
			// CHECKBOX
			array(
				'name' => esc_html__( 'Hide Audio?', 'byblog' ),
				'id'   => "{$prefix}audio_single_hide",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to hide audio on this post.', 'byblog' ),
				// Value can be 0 or 1
				'std'  => 0,
			),
		),
	);
	
	// Video meta box
	$meta_boxes[] = array(
		'id'       => 'videobox',
		'title'    => esc_html__( 'Video Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,

		// List of meta fields
		'fields'   => array(
			// SELECT BOX
			array(
				'name'     => esc_html__( 'Video File Host', 'byblog' ),
				'id'       => "{$prefix}videohost",
				'desc'     => wp_kses(__( 'Select host of your video file. <br> <strong>Note:</strong> If you couldn\'t find host of your video file in the dropdown list, you can paste the embed code of file in the "Video Embed Code" box.', 'byblog' ), array( 'strong' => array(), 'br' => array() ) ),
				'type'     => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options'  => array(
					'youtube'     => esc_html__( 'YouTube', 'byblog' ),
					'vimeo'       => esc_html__( 'Vimeo', 'byblog' ),
					'dailymotion' => esc_html__( 'Dailymotion', 'byblog' ),
					'metacafe'    => esc_html__( 'Metacafe', 'byblog' ),
				),
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std'         => '',
				'placeholder' => esc_html__( 'Select an Item', 'byblog' ),
			),
			// Video ID
			array(
				'name'  => esc_html__( 'Video ID ', 'byblog' ),
				'id'    => "{$prefix}videourl",
				'desc'  => wp_kses(__( 'Paste ID of your YouTube or Vimeo video here (For example: http://www.youtube.com/watch?v=<strong>dQw4w9WgXcQ</strong>).', 'byblog' ), array( 'strong' => array() ) ),
				'type'  => 'text',
				'std'   => '',
				// CLONES: Add to make the field cloneable (i.e. have multiple value)
				'clone' => false,
			),
			// Video Embed Code
			array(
				'name' => esc_html__( 'Video Embed Code (YouTube, Vimeo etc) ', 'byblog' ),
				'desc' => esc_html__( 'Paste your YouTube or Vimeo embed code here.', 'byblog' ),
				'id'   => "{$prefix}videocode",
				'type' => 'textarea',
				'cols' => 20,
				'rows' => 3,
			),
			// Self Hosted Video
			array(
				'name'  => esc_html__( 'Self Hosted Video', 'byblog' ),
				'id'    => "{$prefix}hostedvideourl",
				'desc'  => esc_html__( 'Upload your video file here. Supported formats are: mp4, m4v, webm, ogv, wmv, and flv.', 'byblog' ),
				'type'  => 'file_advanced',
				'std'   => '',
				'max_file_uploads'   => 1,
				// CLONES: Add to make the field cloneable (i.e. have multiple value)
				'clone' => false,
			),
			// CHECKBOX
			array(
				'name' => esc_html__( 'Hide Video?', 'byblog' ),
				'id'   => "{$prefix}video_single_hide",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to hide featured video on this post.', 'byblog' ),
				// Value can be 0 or 1
				'std'  => 0,
			),
		),
	);
	
	// Link Post meta box
	$meta_boxes[] = array(
		'id'       => 'linkbox',
		'title'    => esc_html__( 'Link Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields'   => array(
			// TEXT
			array(
				'name'  => esc_html__( 'URL', 'byblog' ),
				'id'    => "{$prefix}linkurl",
				'desc'  => esc_html__( 'Paste URL here.', 'byblog' ),
				'type'  => 'text',
				'std'   => '',
				'clone' => false,
			),
			// COLOR
			array(
				'name' => esc_html__( 'Link Background Color', 'byblog' ),
				'id'   => "{$prefix}link_background",
				'type' => 'color',
			),
			// CHECKBOX
			array(
				'name' => esc_html__( 'Hide Link Box?', 'byblog' ),
				'id'   => "{$prefix}link_single_hide",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to hide link box on this post.', 'byblog' ),
				// Value can be 0 or 1
				'std'  => 0,
			),
		),
	);
	
	// Gallery meta box
	$meta_boxes[] = array(
		'id'       => 'gallerybox',
		'title'    => esc_html__( 'Gallery Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,

		// List of meta fields
		'fields'   => array(
			array(
				'name'             => esc_html__( 'Upload Images for Gallery', 'byblog' ),
				'id'               => "{$prefix}galleryimg",
				'type'             => 'image_advanced',
				'max_file_uploads' => 40,
			),
			// SELECT BOX
			array(
				'name'     => esc_html__( 'Gallery Type', 'byblog' ),
				'id'       => "{$prefix}gallerytype",
				'desc'     => esc_html__( 'Select the type of gallery you want to show.', 'byblog' ),
				'type'     => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options'  => array(
					'slider' => esc_html__( 'Slider', 'byblog' ),
					'tiled'  => esc_html__( 'Tiled', 'byblog' ),
				),
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std'         => '',
				'placeholder' => esc_html__( 'Select an Item', 'byblog' ),
			),
			// CHECKBOX
			array(
				'name' => esc_html__( 'Hide Gallery?', 'byblog' ),
				'id'   => "{$prefix}gallery_single_hide",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to hide gallery on this post.', 'byblog' ),
				// Value can be 0 or 1
				'std'  => 0,
			),
		),
	);
	
	// Status Post Meta Box
	$meta_boxes[] = array(
		'id'       => 'statusbox',
		'title'    => esc_html__( 'Status Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,

		// List of meta fields
		'fields'   => array(
			// SELECT BOX
			array(
				'name'     => esc_html__( 'Status Type', 'byblog' ),
				'id'       => "{$prefix}statustype",
				'desc'     => esc_html__( 'Select the type of status you want to publish.', 'byblog' ),
				'type'     => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options'  => array(
					'twitter'  => esc_html__( 'Twitter', 'byblog' ),
					'facebook' => esc_html__( 'Facebook', 'byblog' )
				),
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std'         => '',
				'placeholder' => esc_html__( 'Select an Item', 'byblog' ),
			),
			// TEXT
			array(
				'name'  => esc_html__( 'Status Link', 'byblog' ),
				'id'    => "{$prefix}statuslink",
				'desc'  => esc_html__( 'Paste source name of the quote here.', 'byblog' ),
				'type'  => 'text',
				'std'   => '',
				'clone' => false,
			),
			// CHECKBOX
			array(
				'name' => esc_html__( 'Hide Status?', 'byblog' ),
				'id'   => "{$prefix}status_single_hide",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to hide status on this post.', 'byblog' ),
				// Value can be 0 or 1
				'std'  => 0,
			),
		),
	);
	
	// Quote Post Meta Box
	$meta_boxes[] = array(
		'id'       => 'quotebox',
		'title'    => esc_html__( 'Quote Post Options', 'byblog' ),
		'pages'    => array( 'post', 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,

		// List of meta fields
		'fields'   => array(
			// TEXT
			array(
				'name'  => esc_html__( 'Source Name', 'byblog' ),
				'id'    => "{$prefix}sourcename",
				'desc'  => esc_html__( 'Paste source name of the quote here.', 'byblog' ),
				'type'  => 'text',
				'std'   => '',
				'clone' => false,
			),
			// TEXT
			array(
				'name'  => esc_html__( 'Source URL', 'byblog' ),
				'id'    => "{$prefix}sourceurl",
				'desc'  => esc_html__( 'Paste source URL of quote here.', 'byblog' ),
				'type'  => 'text',
				'std'   => '',
				'clone' => false,
			),
			// COLOR
			array(
				'name' => esc_html__( 'Quote Background Color', 'byblog' ),
				'id'   => "{$prefix}quote_background_color",
				'type' => 'color',
			),
		),
	);
	
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Layout Options', 'byblog' ),
		'pages'  => array( 'post', 'page' ),
		'fields' => array(
			// Cover Image
			array(
				'name' => esc_html__( 'Show Cover Image?', 'byblog' ),
				'id'   => "{$prefix}post_cover_show",
				'type' => 'checkbox',
				'desc' => esc_html__( 'Check this option to show cover image on this post.', 'byblog' ),
				'std'  => 0,
			),
			array(
				'id'       => "{$prefix}layout",
				'name'     => esc_html__( 'Layout', 'byblog' ),
				'type'     => 'image_select',

				// Array of 'value' => 'Image Source' pairs
				'options'  => array(
					'default'  => get_template_directory_uri() . '/images/default.png',
					'left'     => get_template_directory_uri() . '/images/left.png',
					'right'    => get_template_directory_uri() . '/images/right.png',
					'none'     => get_template_directory_uri() . '/images/none.png',
				),

				// Allow to select multiple values? Default is false
				// 'multiple' => true,
			),
		),
	);
	
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Styling Options', 'byblog' ),
		'pages'  => array( 'post', 'page' ),
		'fields' => array(
			// COLOR
			array(
				'name' => esc_html__( 'Background Color', 'byblog' ),
				'id'   => "{$prefix}post_bg_color",
				'type' => 'color',
			),
			array(
				'name'             => esc_html__( 'Background Image', 'byblog' ),
				'id'               => "{$prefix}post_bg_img",
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
			),
			// Background Repeat
			array(
				'name'     => esc_html__( 'Background Repeat', 'byblog' ),
				'desc'     => '',
				'id'       => "{$prefix}post_bg_repeat",
				'type'     => 'select',
				'options'  => array(
					'repeat'    => esc_html__( 'Repeat', 'byblog' ),
					'no-repeat' => esc_html__( 'No Repeat', 'byblog' ),
				),
				'multiple' => false,
				'std'      => '',
			),
			// Background Position
			array(
				'name'     => esc_html__( 'Background Position', 'byblog' ),
				'desc'     => '',
				'id'       => "{$prefix}post_bg_position",
				'type'     => 'select',
				'options'  => array(
					'left top'      => esc_html__( 'Left Top', 'byblog' ),
					'left center'   => esc_html__( 'Left Center', 'byblog' ),
					'left bottom'   => esc_html__( 'Left Bottom', 'byblog' ),
					'center top'    => esc_html__( 'Center Top', 'byblog' ),
					'center bottom' => esc_html__( 'Center Bottom', 'byblog' ),
					'center center' => esc_html__( 'Center Center', 'byblog' ),
					'right top'     => esc_html__( 'Right Top', 'byblog' ),
					'right center'  => esc_html__( 'Right Center', 'byblog' ),
					'right bottom'  => esc_html__( 'Right Bottom', 'byblog' ),
				),
				'multiple' => false,
				'std'      => '',
			),
			// Background Attachment
			array(
				'name'     => esc_html__( 'Background Attachment', 'byblog' ),
				'desc'     => '',
				'id'       => "{$prefix}post_bg_attachment",
				'type'     => 'select',
				'options'  => array(
					'scroll' => esc_html__( 'Scroll', 'byblog' ),
					'fixed'  => esc_html__( 'Fixed', 'byblog' )
				),
				'multiple' => false,
				'std'      => '',
			),
			// Background Size
			array(
				'name'     => esc_html__( 'Background Size', 'byblog' ),
				'desc'     => '',
				'id'       => "{$prefix}post_bg_size",
				'type'     => 'select',
				'options'  => array(
					'inherit' => esc_html__( 'Inherit', 'byblog' ),
					'cover'   => esc_html__( 'Cover', 'byblog' ),
					'contain' => esc_html__( 'Contain', 'byblog' )
				),
				'multiple'    => false,
				'std'         => '',
			),
		),
	);

	return $meta_boxes;
}


