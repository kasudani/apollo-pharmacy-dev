<form method="get" class="searchform search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<fieldset> 
        <label>
            <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'byblog' ); ?></span>
            <input type="text" name="s" class="s" value="" placeholder="<?php esc_html_e('Search Now','byblog'); ?>"> 
        </label>
		<button class="fa fa-search search-button" type="submit" value="<?php esc_html_e('Search','byblog'); ?>"><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'byblog' ); ?></button>
        <span class="search-submit t-center fa fa-search"></span>
	</fieldset>
</form>