<?php
/**
 * The template for displaying Author archive pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

global $byblog_options;

// Page Variables
$byblog_layout = $byblog_options['byblog_archive_layout'];

get_header(); ?>

<div class="main-wrapper" itemscope itemtype="http://schema.org/ProfilePage">
    <?php
        $byblog_author_bg = '';
        if( get_the_author_meta('author-attachment-url') ) {
           $byblog_author_bg = get_the_author_meta("author-attachment-url");
        }
        if ( $byblog_author_bg ) {
            $byblog_author_bg_style = 'style="background-image:url('.$byblog_author_bg.')";';
        } else {
            $byblog_author_bg_style = '';
        }
    ?>
    <div class="author-box author-desc-box"<?php echo $byblog_author_bg_style; ?> itemprop="author" itemscope itemtype="https://schema.org/Person">
        <div class="author-box-content">
            <div class="author-page-info archive-cover-content">
                <div class="author-head">
                    <h5 itemprop="name"><?php esc_attr( the_author_meta('display_name') ); ?></h5>
                </div><!--.author-head-->
                
                <span class="uppercase author-articles-count">
                    <?php the_author_posts(); ?>
                    <?php esc_html_e('Articles','byblog'); ?>
                </span>
                
                <div class="author-avtar">
                    <?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '100' );  } ?>
                </div><!--.author-avtar-->
                
                <?php if(get_the_author_meta('description')) { ?>
                    <div class="author-desc" itemprop="description"><?php esc_attr( the_author_meta('description') ); ?></div>
                <?php } ?>
                
                <div class="author-social">
                    <?php if(get_the_author_meta('facebook')) { ?><span class="author-fb"><a class="fa fa-facebook" href="<?php echo esc_url( get_the_author_meta('facebook') ); ?>"></a></span><?php } ?>
                    <?php if(get_the_author_meta('twitter')) { ?><span class="author-twitter"><a class="fa fa-twitter" href="<?php echo esc_url( get_the_author_meta('twitter') ); ?>"></a></span><?php } ?>
                    <?php if(get_the_author_meta('googleplus')) { ?><span class="author-gp"><a class="fa fa-google-plus" href="<?php echo esc_url( get_the_author_meta('googleplus') ); ?>"></a></span><?php } ?>
                    <?php if(get_the_author_meta('linkedin')) { ?><span class="author-linkedin"><a class="fa fa-linkedin" href="<?php echo esc_url( get_the_author_meta('linkedin') ); ?>"></a></span><?php } ?>
                    <?php if(get_the_author_meta('pinterest')) { ?><span class="author-pinterest"><a class="fa fa-pinterest" href="<?php echo esc_url( get_the_author_meta('pinterest') ); ?>"></a></span><?php } ?>
                    <?php if(get_the_author_meta('dribbble')) { ?><span class="author-dribbble"><a class="fa fa-dribbble" href="<?php echo esc_url( get_the_author_meta('dribbble') ); ?>"></a></span><?php } ?>
                </div><!--.author-social-->
                
            </div><!--.author-page-info-->
        </div>
    </div><!--.author-box-->
	<div id="page">
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
			<div class="archive-page">
				<div class="content-area archive-content-area">
                    <div class="content content-archive">
				        <div id="content" class="clearfix">
							<?php

                                $byblog_post_pos_count = 0;

								// Start the Loop.
								if (have_posts()) : while (have_posts()) : the_post();
								
									/*
									 * Include the post format-specific template for the content. If you want to
									 * use this in a child theme, then include a file called called content-___.php
									 * (where ___ is the post format) and that will be used instead.
									 */
									get_template_part( 'template-parts/post-formats/content', get_post_format() );

                                    $byblog_ad_count = ( $byblog_options['byblog_blog_ad_pos'] ? $byblog_options['byblog_blog_ad_pos'] : 1 );

                                    $byblog_ad_code = $byblog_options['byblog_blog_archive_ad'];
                                    if ( $byblog_ad_code ) {
                                        if( $byblog_post_pos_count == $byblog_ad_count && !is_paged() ) :
                                            echo '<div class="blog-ad post t-center">'. $byblog_ad_code .'</div>';
                                        endif;
                                    }

                                    $byblog_post_pos_count++;
								
								endwhile;
								
								else:
									// If no content, include the "No posts found" template.
									get_template_part( 'template-parts/post-formats/content', 'none' );

								endif;
							?>
						</div><!--.content-->
						<?php 
							// Previous/next page navigation.
							byblog_paging_nav();
						?>
					</div><!--.content-archive-->
				</div><!--#content-->
				<?php
					$byblog_layout_array = array(
                        'clayout',
                        'glayout',
                        'flayout'
                    );

                    if( !in_array( $byblog_layout, $byblog_layout_array ) ) {
                        get_sidebar();
                    }
				?>
			</div><!--.archive-page-->
<?php get_footer(); ?>