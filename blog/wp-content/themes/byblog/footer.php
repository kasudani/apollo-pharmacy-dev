<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the .main-wrapper and #page div elements.
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

global $byblog_options;

// Page Variables
$byblog_below_content_sidebar_ad = $byblog_options['byblog_below_content_sidebar_ad'];
$byblog_show_footer_posts = $byblog_options['byblog_show_footer_posts'];
$byblog_show_instagram_photos = $byblog_options['byblog_show_instagram_photos'];
$byblog_instagram_profile_url = $byblog_options['byblog_instagram_profile_url'];
$byblog_show_footer_widgets = $byblog_options['byblog_show_footer_widgets'];
$byblog_footer_columns = $byblog_options['byblog_footer_columns'];
$byblog_footer_text = $byblog_options['byblog_footer_text'];
$byblog_scroll_btn = $byblog_options['byblog_scroll_btn'];
?>

        </div><!--.main-content-->
            <?php if ( $byblog_below_content_sidebar_ad ) { ?>
                <div class="ad-below-cs t-center">
                    <div class="container">
                        <?php echo $byblog_below_content_sidebar_ad; ?>    
                    </div>
                </div><!--.ad-below-cs-->
            <?php } ?>
            <?php
                if ( $byblog_show_footer_posts == '1' ) {
                    // Footer Featured Posts
                    get_template_part( 'template-parts/footer-posts' );
                }
            ?>
		</div><!--#page-->
	</div><!--.main-wrapper-->
    
    <?php if ( $byblog_show_instagram_photos == '1' ) { ?>
        <div class="instagram-photos clearfix">
            <?php if ( $byblog_instagram_profile_url != '' ) { ?>
                <span class="instagram-title"><a href="<?php echo esc_url( $byblog_instagram_profile_url ); ?>"><?php esc_html_e('Follow us on Instagram','byblog'); ?></a></span>
            <?php } else { ?>
                <span class="instagram-title"><?php esc_html_e('Follow on Instagram','byblog'); ?></span>
            <?php } ?>
            <ul id="instafeed">
            </ul>
        </div>
    <?php } ?>

    <footer id="site-footer" class="footer site-footer clearfix" itemscope itemtype="http://schema.org/WPFooter" role="contentinfo">
        <div class="container">
            <?php if ( $byblog_show_footer_widgets == '1' ) { ?>
                <?php if ( $byblog_footer_columns == 'footer_4' ) { ?>
                    <div class="footer-widgets clearfix footer-columns-4">
                        <div class="footer-widget footer-widget-1">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer-widget footer-widget-2">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2') ) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer-widget footer-widget-3">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3') ) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer-widget footer-widget-4 last">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-4') ) : ?>
                            <?php endif; ?>
                        </div>
                    </div><!-- .footer-widgets -->
                <?php } elseif ( $byblog_footer_columns == 'footer_3' ) { ?>
                    <div class="footer-widgets clearfix footer-columns-3">
                        <div class="footer-widget footer-widget-1">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer-widget footer-widget-2">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2') ) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer-widget footer-widget-3 last">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3') ) : ?>
                            <?php endif; ?>
                        </div>
                    </div><!-- .footer-widgets -->
                <?php } elseif ( $byblog_footer_columns == 'footer_2' ) { ?>
                    <div class="footer-widgets clearfix footer-columns-2">
                        <div class="footer-widget footer-widget-1">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer-widget footer-widget-2 last">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2') ) : ?>
                            <?php endif; ?>
                        </div>
                    </div><!-- .footer-widgets -->
                <?php } else { ?>
                    <div class="footer-widgets clearfix footer-columns-1">
                        <div class="footer-widget footer-widget-1">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer') ) : ?>
                            <?php endif; ?>
                        </div>
                    </div><!-- .footer-widgets -->
                <?php } ?>
            <?php } ?>
            <div class="copyright">
                <div class="copyright-inner t-center">
                    <?php if( $byblog_footer_text != '' ) { ?>
                        <div class="copyright-text">
                            <?php echo $byblog_footer_text; ?>
                        </div>
                    <?php } ?>
                </div>
            </div><!-- .copyright -->
        </div><!-- .container -->
    </footer>
    <div class="site-overlay"></div>
	</div><!-- .st-pusher -->
</div><!-- .main-container -->
<?php if ( $byblog_scroll_btn == '1' ) { ?>
	<div class="back-to-top transition"><i class="fa fa-arrow-up"></i></div>
<?php } ?>
</div>
<?php wp_footer(); ?>
</body>
</html>