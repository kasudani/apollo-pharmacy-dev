<?php global $byblog_options; ?>
<?php if ( is_search() ) { ?>
    <div class="post-content entry-summary">
        <?php the_excerpt(); ?>
    </div><!-- .entry-summary -->
<?php } else { ?>
    <div class="post-content entry-content">
        <?php
            if( $byblog_options['byblog_home_content'] != '3' ) {
                if( $byblog_options['byblog_home_content'] == '2' ) {
                    the_content( esc_html__('Read More','byblog') );
                } else {
                    the_excerpt();
                }
            }

            get_template_part('template-parts/post-footer');
        ?>
    </div><!--post-content-->
<?php } ?>