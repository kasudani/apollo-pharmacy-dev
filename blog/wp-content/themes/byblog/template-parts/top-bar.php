<?php global $byblog_options; ?>
<?php if ( $byblog_options['byblog_header_newsletter'] ) { ?>
    <div class="top-bar clearfix">
        <div class="container">
            <?php $byblog_mailchimp_newsletter_text = $byblog_options['byblog_mailchimp_newsletter_text']; ?>
            <?php if ( $byblog_mailchimp_newsletter_text ) { ?>
                <span><?php echo esc_attr( $byblog_mailchimp_newsletter_text ); ?></span>
            <?php } ?>
            <div id="mc_embed_signup" class="header-subscribe-form">
                <?php $byblog_mailchimp_newsletter_url = $byblog_options['byblog_mailchimp_newsletter_url']; ?>
                <form action="<?php echo $byblog_mailchimp_newsletter_url; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" novalidate>
                    <div class="input-group">
                        <label for="mce-EMAIL" class="screen-reader-text"><?php esc_html_e('Enter your Email Address','byblog'); ?></label>
                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php esc_html_e('Email Address','byblog'); ?>">
                    </div>
                    <div class="input-group">
                        <button type="submit" value="<?php esc_html_e('Subscribe','byblog'); ?>" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary">
                            <span class="screen-reader-text"><?php esc_html_e('Subscribe','byblog'); ?></span>
                        </button>
                    </div>
                </form>
            </div>
            <div class="subscribe-result"></div>
        </div>
    </div><!-- .top-bar -->
<?php } ?>