<?php
global $byblog_options;

$byblog_read_more_btn = $byblog_options['byblog_read_more_btn'];
$byblog_read_more_text = $byblog_options['byblog_read_more_btn_text'];
$byblog_post_meta = $byblog_options['byblog_post_meta'];
$byblog_post_comments = $byblog_options['byblog_post_meta_options']['5'];
$byblog_show_share_buttons = $byblog_options['byblog_show_home_share_buttons'];

if ( $byblog_read_more_btn == '' ) {
    $byblog_read_more_btn = '1';
}
?>
<?php if( $byblog_read_more_btn == '1' ) { ?>
    <span class="read-more">
        <?php
            if( $byblog_read_more_text == '' ) {
                $byblog_read_more_text = esc_html__('Read More','byblog');
            }
            $byblog_read_more_text_reader = sprintf( wp_kses(__( '%1$s<span class="screen-reader-text"> about %2$s</span>', 'byblog' ), array( 'span' => array( 'class' => array() ) ) ), $byblog_read_more_text, get_the_title() );
        ?>
        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php echo $byblog_read_more_text_reader; ?></a>
    </span>
<?php } ?>
<div class="post-footer post-meta clearfix">
    <?php 
        if( $byblog_post_meta == '1' ) {
            if( $byblog_post_comments == '1' ) { ?>
                <span class="post-comments">
                    <?php
                        comments_popup_link( sprintf( wp_kses(__( 'Leave a comment<span class="screen-reader-text"> on %s</span>', 'byblog' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
                    ?>
                </span>
            <?php }
        }

        if( $byblog_show_share_buttons == '1' ) {
            get_template_part('template-parts/share-buttons');
        }
    ?>
</div><!--.post-footer-->