<?php global $byblog_options; ?>

<?php $byblog_featured_posts = ( isset ( $byblog_options['byblog_featured_posts'] ) ? $byblog_options['byblog_featured_posts'] : '' ); ?>

<?php if ( $byblog_featured_posts ) { ?>
<div class="featured-posts loading clearfix">
    <h4 class="featured-title section-heading">
        <span><?php esc_html_e('Featured Posts','byblog'); ?></span>
    </h4>
    <ul class="clearfix">
    <?php
        $featured_posts = new WP_Query( array(
            'post_type'           => 'post',
            'post__in'            => $byblog_featured_posts,
            'ignore_sticky_posts' => 1,
            'posts_per_page'      => 8
        ) );

        if( $featured_posts->have_posts() ) : while ($featured_posts->have_posts()) : $featured_posts->the_post(); ?>
        <li class="item">
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail f-thumb">
                <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail('byblog-featuredthumb');
                    }
                ?>
                <div class="post-inner clearfix">
                    <header>
                        <h2 class="title">
                            <?php the_title(); ?>
                        </h2>
                    </header><!--.header-->
                </div>
            </a>
        </li>
        <?php
        endwhile;

        wp_reset_postdata();

        else:
        endif;
    ?>
    </ul>
</div><!--.featured-posts-->
<?php } ?>