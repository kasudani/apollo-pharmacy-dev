<?php global $byblog_options; ?>
<?php
    the_tags( '<div class="post-tags clearfix" itemprop="keywords"><span>' . esc_html__("Tags:","byblog") . '</span> ', ' ', '</div>' );
?>
<div class="post-footer post-meta clearfix">
    <?php
        if( $byblog_options['byblog_show_share_buttons'] == '1' ) {
            get_template_part('template-parts/share-buttons');
        }
    ?>
</div><!--.post-footer-->