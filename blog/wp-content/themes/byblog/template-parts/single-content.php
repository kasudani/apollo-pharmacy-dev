<?php
global $byblog_options;

// Page Variables
$byblog_below_title_ad = $byblog_options['byblog_below_title_ad'];
$byblog_below_content_ad = $byblog_options['byblog_below_content_ad'];
?>
<div class="post-inner">
    <div class="post-content entry-content single-post-content">
        <?php if( $byblog_below_title_ad != '' ) { ?>
            <div class="single-post-ad">
                <?php echo $byblog_below_title_ad; ?>
            </div>
        <?php } ?>

        <?php the_content(); ?>

        <?php if( $byblog_below_content_ad != '' ) { ?>
            <div class="single-post-ad">
                <?php echo $byblog_below_content_ad; ?>
            </div>
        <?php } ?>

        <?php byblog_wp_link_pages() ?>

        <?php get_template_part('template-parts/post-footer-single'); ?>
    </div><!--.single-post-content-->
</div><!--.post-inner-->