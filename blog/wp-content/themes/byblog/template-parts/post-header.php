<?php global $byblog_options; ?>
<?php
    if ( is_single() ) { ?>
        <header>
            <?php if( $byblog_options['byblog_single_meta'] == '1' ) { ?>
                <div class="post-meta post-meta-top">
                    <?php
                        if( $byblog_options['byblog_single_post_meta_options']['6'] == '1' ) {
                            get_template_part('template-parts/post-format-icons');
                        }
                    ?>
                    <span class="post-cats">
                        <?php
                            if( $byblog_options['byblog_single_post_meta_options']['3'] == '1' ) {
                                // Categories
                                $categories = get_the_category();
                                $separator = ', ';
                                $output = '';
                                if( $categories ) {
                                    echo '<span><span class="screen-reader-text">'.esc_html__('Posted','byblog').'</span>' . esc_html__( 'in', 'byblog' ) . '</span>';
                                    foreach( $categories as $category ) {
                                        $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( 'View all posts in %s','byblog' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
                                    }
                                echo trim( $output, $separator );
                                }
                            }
                        ?>
                    </span><?php
                    if( $byblog_options['byblog_single_post_meta_options']['2'] == '1' ) {
                        $byblog_time_string = sprintf( '<time class="entry-date" datetime="%1$s">%2$s</time>',
                            esc_attr( get_the_date( 'c' ) ),
                            get_the_date()
                        );

                        printf( '<span class="post-date"><span class="screen-reader-text">%1$s </span>%2$s</span>',
                            _x( 'Posted on', 'Used before publish date.', 'byblog' ),
                            $byblog_time_string
                        );
                    }
                    ?>
                </div><!--.post-meta-->
            <?php } ?>
            
            <h1 class="title entry-title">
                <?php the_title(); ?>
            </h1>
            
            <?php if( $byblog_options['byblog_single_meta'] == '1' ) { ?>
                <div class="post-meta-title post-meta">
                    <?php if( $byblog_options['byblog_single_post_meta_options']['1'] == '1' ) { ?>
                        <span class="post-author"><span><span class="screen-reader-text"><?php esc_html_e('Posted','byblog'); ?></span> <?php esc_html_e('By','byblog'); ?></span><?php echo '&nbsp;'; the_author_posts_link(); ?></span>
                    <?php }
                        edit_post_link( esc_html__( 'Edit', 'byblog' ), '<span class="edit-link">', '</span>' );
                    ?>
                </div><!--.post-meta-title-->
            <?php } ?>
        </header><!--.header-->
    <?php } else { ?>
        <header>
            <?php if( $byblog_options['byblog_post_meta'] == '1' ) { ?>
                <div class="post-meta post-meta-top">
                    <?php
                        if( $byblog_options['byblog_post_meta_options']['6'] == '1' ) {
                            get_template_part('template-parts/post-format-icons');
                        }
                    ?>
                    <span class="post-cats">
                        <?php
                            if( $byblog_options['byblog_post_meta_options']['3'] == '1' ) {
                                // Categories
                                $categories = get_the_category();
                                $separator = ', ';
                                $output = '';
                                if( $categories ) {
                                    echo '<span><span class="screen-reader-text">'.esc_html__('Posted','byblog').'</span>' . esc_html__( 'in', 'byblog' ) . '</span>';
                                    foreach( $categories as $category ) {
                                        $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( 'View all posts in %s','byblog' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
                                    }
                                echo trim( $output, $separator );
                                }
                            }
                        ?>
                    </span><?php
                    if( $byblog_options['byblog_post_meta_options']['2'] == '1' ) {
                        $byblog_time_string = sprintf( '<time class="entry-date" datetime="%1$s">%2$s</time>',
                            esc_attr( get_the_date( 'c' ) ),
                            get_the_date()
                        );

                        printf( '<span class="post-date"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>',
                            _x( 'Posted on', 'Used before publish date.', 'byblog' ),
                            esc_url( get_permalink() ),
                            $byblog_time_string
                        );
                    }
                    ?>
                </div><!--.post-meta-->
            <?php } ?>
            
            <h2 class="title entry-title">
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h2>
            
            <?php if( $byblog_options['byblog_post_meta'] == '1' ) { ?>
                <div class="post-meta-title post-meta">
                    <?php if( $byblog_options['byblog_post_meta_options']['1'] == '1' ) { ?>
                        <span class="post-author"><span><span class="screen-reader-text"><?php esc_html_e('Posted','byblog'); ?></span> <?php esc_html_e('By','byblog'); ?></span><?php echo '&nbsp;'; the_author_posts_link(); ?></span>
                    <?php }
                        edit_post_link( esc_html__( 'Edit', 'byblog' ), '<span class="edit-link">', '</span>' );
                    ?>
                </div><!--.post-meta-title-->
            <?php } ?>
        </header><!--.header-->
    <?php }
?>