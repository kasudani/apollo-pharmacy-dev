<?php global $byblog_options; ?>
<header class="main-header header-2 clearfix" id="site-header" itemscope itemtype="http://schema.org/WPHeader" role="banner">
    <?php
        // Top Bar
        get_template_part( 'template-parts/top-bar' );
    ?>
	<div class="header clearfix">
		<div class="container">
			<div class="logo-wrap">
				<?php if (!empty($byblog_options['byblog_logo']['url'])) { ?>
					<div id="logo" class="logo uppercase" itemprop="headline">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo esc_url( $byblog_options['byblog_logo']['url'] ); ?>" width="<?php echo esc_attr( $byblog_options['byblog_logo']['width'] ); ?>" height="<?php echo esc_attr( $byblog_options['byblog_logo']['width'] ); ?>" <?php if (!empty($byblog_options['byblog_retina_logo']['url'])) { echo 'data-at2x="'.$byblog_options['byblog_retina_logo']['url'].'"';} ?> alt="<?php bloginfo( 'name' ); ?>">
                        </a>
                    </div>
				<?php } else { ?>
					<?php if( is_front_page() || is_home() || is_404() ) { ?>
						<h1 id="logo" class="logo uppercase" itemprop="headline">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
						</h1>
					<?php } else { ?>
						<h2 id="logo" class="logo uppercase" itemprop="headline">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
						</h2>
					<?php } ?>
				<?php } ?>
				<?php if ($byblog_options['byblog_tagline'] == '1') { ?>
					<span class="tagline" itemprop="description">
						<?php bloginfo( 'description' ); ?>
					</span>
				<?php } ?>
			</div><!--.logo-wrap-->
		</div><!-- .container -->
	</div><!-- .header -->
    <?php if ( $byblog_options['byblog_social_links'] == '1' || has_nav_menu( 'main-menu' ) || $byblog_options['byblog_header_search'] == '1' ) { ?>
	<div class="main-navigation clearfix">
		<div class="main-nav nav-down clearfix">
			<div class="center-width">
                <div class="menu-btn off-menu">
                    <i class="fa fa-align-justify"></i>
                    <?php esc_html_e('Menu','byblog'); ?>
                </div>
				<?php 
                    if ( $byblog_options['byblog_social_links'] == '1' ) {
                        byblog_social_links( 'header' );
                    }
                ?>
				<nav class="nav-menu" id="nav-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<?php
                        if ( has_nav_menu( 'main-menu' ) ) {
                            wp_nav_menu( array(
                                'theme_location' => 'main-menu',
                                'menu_class'     => 'menu',
                                'container'      => '',
                                'walker'         => new byblog_nav_walker
                            ) );
                        }
                    ?>
				</nav>
                <?php
                    if ( $byblog_options['byblog_header_search'] == '1' ) { ?>
					   <div class="header-search"><?php get_search_form(); ?></div>
				        <?php
                    }

                    if ( is_woocommerce_activated() ) {
                        if ( $byblog_options['byblog_header_cart_icon'] == '1' ) { ?>
                            <div class="header-cart">
                                <span class="fa fa-shopping-cart" aria-hidden="true"></span>
                                <a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart','byblog' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'byblog' ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
                            </div>
                        <?php
                        }
                    }
                ?>
			</div><!-- .center-width -->
		</div><!-- .main-nav -->
	</div><!-- .main-navigation -->
    <?php } ?>
</header>