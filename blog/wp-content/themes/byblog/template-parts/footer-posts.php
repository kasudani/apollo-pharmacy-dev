<?php global $byblog_options; ?>
<div class="footer-feature-posts clearfix">
    <div class="container">
        <?php
            $fcount = 1;
            $byblog_footer_featured_posts = ( isset ( $byblog_options['byblog_footer_featured_posts'] ) ? $byblog_options['byblog_footer_featured_posts'] : '' );

            if ( $byblog_footer_featured_posts ) {
            $footer_featured_posts = new WP_Query( array( 'post_type' => 'post', 'post__in' => $byblog_footer_featured_posts, 'ignore_sticky_posts' => 1, 'posts_per_page' => 8 ) );

            if($footer_featured_posts->have_posts()) : while ($footer_featured_posts->have_posts()) : $footer_featured_posts->the_post(); ?>
                <div class="footer-featured-post footer-featured-post-<?php echo $fcount; ?>">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail">
                            <?php the_post_thumbnail('thumbnail'); ?>
                        </a>
                    <?php } ?>
                    <header>
                        <?php if( $byblog_options['byblog_footer_meta'] == '1' ) { ?>
                                <div class="post-meta post-meta-top">
                                    <span class="post-cats">
                                        <?php
                                            if( $byblog_options['byblog_footer_post_meta_options']['3'] == '1' ) {
                                                // Categories
                                                $category = get_the_category();
                                                if ($category) {
                                                  echo '<span>' . esc_html__('in','byblog') . '</span><a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( esc_html__( "View all posts in %s", "byblog" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                                                }
                                            }
                                        ?>
                                    </span><?php
                                    if( $byblog_options['byblog_footer_post_meta_options']['2'] == '1' ) { ?>
                                        <span class="post-date">
                                            <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php the_time(get_option( 'date_format' )); ?></time>
                                        </span><?php
                                    } ?>
                                </div><!--.post-meta-->
                            <?php } ?>
                        <h2 class="title">
                            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
                        </h2>
                        <?php if($byblog_options['byblog_footer_post_meta_options']['1'] == '1') { ?>
                            <div class="post-meta footer-meta">
                                <span class="post-author"><span><?php esc_html_e('By','byblog'); ?></span><?php echo the_author_posts_link(); ?></span>
                            </div>
                        <?php } ?>
                    </header><!--.header-->
                </div><!--.post -->
            <?php $fcount++; endwhile; ?>
        <?php endif; } ?>
    </div>
</div><!--.footer-feature-posts-->