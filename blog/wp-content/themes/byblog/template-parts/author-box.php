<div class="author-box single-box">
    <h3 class="section-heading uppercase"><?php esc_html_e('About Author','byblog'); ?></h3>
    <div class="author-box-avtar">
        <?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '100' );  } ?>
    </div>
    <div class="author-info-container">
        <div class="author-info">
            <div class="author-head">
                <h5><?php esc_attr( the_author_meta('display_name') ); ?></h5>
            </div>
            <p><?php esc_attr( the_author_meta('description') ); ?></p>
            <div class="author-social">
                <?php if(get_the_author_meta('facebook')) { ?>
                    <span class="author-fb">
                        <a class="fa fa-facebook" href="<?php echo esc_url( get_the_author_meta('facebook') ); ?>">
                            <span class="screen-reader-text"><?php esc_attr_e('Facebook','byblog'); ?></span>
                        </a>
                    </span>
                <?php } ?>
                <?php if(get_the_author_meta('twitter')) { ?>
                    <span class="author-twitter">
                        <a class="fa fa-twitter" href="<?php echo esc_url( get_the_author_meta('twitter') ); ?>">
                            <span class="screen-reader-text"><?php esc_attr_e('Twitter','byblog'); ?></span>
                        </a>
                    </span>
                <?php } ?>
                <?php if(get_the_author_meta('googleplus')) { ?>
                    <span class="author-gp">
                        <a class="fa fa-google-plus" href="<?php echo esc_url( get_the_author_meta('googleplus') ); ?>">
                            <span class="screen-reader-text"><?php esc_attr_e('Google+','byblog'); ?></span>
                        </a>
                    </span>
                <?php } ?>
                <?php if(get_the_author_meta('linkedin')) { ?>
                    <span class="author-linkedin">
                        <a class="fa fa-linkedin" href="<?php echo esc_url( get_the_author_meta('linkedin') ); ?>">
                            <span class="screen-reader-text"><?php esc_attr_e('LinkedIn','byblog'); ?></span>
                        </a>
                    </span>
                <?php } ?>
                <?php if(get_the_author_meta('pinterest')) { ?>
                    <span class="author-pinterest">
                        <a class="fa fa-pinterest" href="<?php echo esc_url( get_the_author_meta('pinterest') ); ?>">
                            <span class="screen-reader-text"><?php esc_attr_e('Pinterest','byblog'); ?></span>
                        </a>
                    </span>
                <?php } ?>
                <?php if(get_the_author_meta('dribbble')) { ?>
                    <span class="author-dribbble">
                        <a class="fa fa-dribbble" href="<?php echo esc_url( get_the_author_meta('dribbble') ); ?>">
                            <span class="screen-reader-text"><?php esc_attr_e('Dribbble','byblog'); ?></span>
                        </a>
                    </span>
                <?php } ?>
            </div><!--.author-social-->
        </div><!--.author-info-->
    </div>
</div><!--.author-box-->