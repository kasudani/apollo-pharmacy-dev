<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
        <?php
            global $byblog_post_pos_count;

            // Featured Image
            byblog_featured_image_full();
        ?>
		<div class="post-inner">
            <?php
                // Post Header
                get_template_part('template-parts/post-header');
            
                // Post Content
                get_template_part('template-parts/post-content-front');
            ?>
		</div><!--.post-inner-->
	</div><!--.post-box-->
</article>