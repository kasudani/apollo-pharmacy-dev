<?php
    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_gallery_images = rwmb_meta( 'byblog_galleryimg', $args = array('type' => 'image'), $post->ID );
        $byblog_gallery_type = rwmb_meta( 'byblog_gallerytype', $args = array('type' => 'select'), $post->ID );
        $byblog_gallery_single = rwmb_meta( 'byblog_gallery_single_hide', $args = array('type' => 'checkbox'), $post->ID );
    } else {
        $byblog_gallery_images = '';
        $byblog_gallery_type = '';
        $byblog_gallery_single = '';
    }
?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<div class="post-inner">
            <?php
                if ( $byblog_gallery_images != NULL ) {
                    if ( $byblog_gallery_type == 'slider' ) { ?>
                        <div class="galleryslider loading">
                            <?php foreach ($byblog_gallery_images as $byblog_image_id) {
                                $byblog_image = wp_get_attachment_image_src($byblog_image_id['ID'],'byblog-featured');
                                $alt_text = get_post_meta($byblog_image_id['ID'], '_wp_attachment_image_alt', true);
                                echo "<div class='item'><img src='" . esc_url( $byblog_image[0] ) . "' alt='" . esc_attr( $alt_text ) . "' width='770' height='355' ></div>";
                            } ?>
                        </div>
                    <?php } else { ?>
                        <div class="gallerytiled">
                            <ul>
                                <?php foreach ($byblog_gallery_images as $byblog_image_id) {
                                    $byblog_image_thumb = wp_get_attachment_image_src($byblog_image_id['ID'],'thumbnail');
                                    $byblog_image_large = wp_get_attachment_image_src($byblog_image_id['ID'],'large');
                                    $alt_text = get_post_meta($byblog_image_id['ID'], '_wp_attachment_image_alt', true);
                                    echo "<li><a class='featured-thumbnail featured-thumb-gallery' href='" . esc_url( $byblog_image_large[0] ) . "'><img src='" . esc_url( $byblog_image_thumb[0] ) . "' alt='" . esc_attr( $alt_text ) . "' width='150' height='150' ><div class='fhover'></div></a></li>";
                                } ?>
                            </ul>
                        </div>
                    <?php }
                }

                // Post Header
                get_template_part('template-parts/post-header');
            
                // Post Content
                get_template_part('template-parts/post-content-front');
            ?>
		</div><!--.post-inner-->
	</div><!--.post-box-->
</article>