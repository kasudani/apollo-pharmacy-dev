<?php
    global $byblog_options;

    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_cover = rwmb_meta( 'byblog_post_cover_show', $args = array('type' => 'checkbox'), $post->ID );
        $byblog_status_type = rwmb_meta( 'byblog_statustype', $args = array('type' => 'text'), $post->ID );
        $byblog_status = rwmb_meta( 'byblog_statuslink', $args = array('type' => 'text'), $post->ID );
        $byblog_status_single = rwmb_meta( 'byblog_status_single_hide', $args = array('type' => 'checkbox'), $post->ID );
    } else {
        $byblog_cover = '';
        $byblog_status_type = '';
        $byblog_status = '';
        $byblog_status_single = '';
    }
?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<?php 
			if($byblog_cover == '0' || $byblog_cover == '') {
                // Post Header
                get_template_part('template-parts/post-header');
            }
			
			$thumb_id = get_post_thumbnail_id(get_the_ID());
			$thumb_url = wp_get_attachment_image_src($thumb_id,'featured');
			
			$status_bg = $thumb_url[0];
			if(!empty($status_bg)) {
				$status_bg_code = 'style=" background-image:url('.esc_url( $status_bg ).'); background-size: cover;"';
			} else {
				$status_bg_code = '';
			}
			if($byblog_options['byblog_single_featured'] == '1') {
				if(empty($byblog_status_single)) {
					if ($byblog_status_type == 'twitter') { ?>
						<div class="status-box home-status-box twitter-status" <?php echo $status_bg_code; ?>>
							<blockquote class="twitter-tweet" lang="en" width="670px"><p><a href="<?php if ($byblog_status != '') { echo esc_url( $byblog_status ); } ?>"></a></blockquote>
							<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					<?php } else if ($byblog_status_type == 'facebook') { ?>
						<div class="status-box home-status-box fb-status" <?php echo $status_bg_code; ?>>
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
							<div class="fb-post" data-href="<?php if ($byblog_status != '') { echo esc_url( $byblog_status ); } ?>" data-width="670px"></div>
						</div>
			<?php }
				}
			}

            get_template_part('template-parts/single-content');
        ?>
	</div><!--.post-box-->
</article>