<?php
    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_link_url = rwmb_meta( 'byblog_linkurl', $args = array('type' => 'text'), $post->ID );
        $byblog_link_bg = rwmb_meta( 'byblog_link_background', $args = array('type' => 'text'), $post->ID );
    } else {
        $byblog_link_url = '';
        $byblog_link_bg = '';
    }
?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<div class="post-inner">
            <?php
                $thumb_id = get_post_thumbnail_id();
                $thumb_url = wp_get_attachment_image_src($thumb_id,'featured');

                $status_bg = $thumb_url[0];
                if( !empty( $status_bg ) ) {
                    $status_bg_code = 'style="background-image: url('.esc_url( $status_bg ).'); background-size: cover;"';
                } else if( !empty( $byblog_link_bg ) ) {
                    $status_bg_code = 'style=" background:'.$byblog_link_bg.'"';
                } else {
                    $status_bg_code = '';
                }

                // Post Header
                get_template_part('template-parts/post-header');
            ?>
			<a href="<?php echo esc_url( $byblog_link_url ); ?>">
				<div class="post-content post-format-link" <?php echo $status_bg_code; ?>>
					<div class="post-content-format">
						<i class="fa fa-link post-format-icon"></i>
						<div class="post-format-link-content">
							<?php echo esc_url( $byblog_link_url ); ?>
						</div>
					</div>
				</div><!--.post-content-->
			</a>
            <?php
                // Post Content
                get_template_part('template-parts/post-content-front');
            ?>
		</div><!--.post-inner-->
	</div><!--.post-box-->
</article>