<?php global $byblog_options; ?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<div class="post-inner">
            <?php $byblog_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
            
            <a href="<?php echo esc_url( $byblog_url ); ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail featured-thumbnail-big clearfix">
                <?php
                    if ( has_post_thumbnail() ) {
                        $byblog_image_size = ( $byblog_options['byblog_crop_images'] == 1 ? 'featured' : 'full' );
                        the_post_thumbnail( $byblog_image_size );
                        echo '<div class="fhover"></div>';
                    }
                ?>
            </a>
            <?php
                // Post Header
                get_template_part('template-parts/post-header');
            
                // Post Content
                get_template_part('template-parts/post-content-front');
            ?>
		</div><!--.post-inner-->
	</div><!--.post-box-->
</article>