<?php
    global $byblog_options;

    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_cover = rwmb_meta( 'byblog_post_cover_show', $args = array('type' => 'checkbox'), $post->ID );
        $byblog_link_url = rwmb_meta( 'byblog_linkurl', $args = array('type' => 'text'), $post->ID );
        $byblog_link_bg = rwmb_meta( 'byblog_link_background', $args = array('type' => 'text'), $post->ID );
        $byblog_link_single = rwmb_meta( 'byblog_link_single_hide', $args = array('type' => 'checkbox'), $post->ID );
    } else {
        $byblog_cover = '';
        $byblog_link_url = '';
        $byblog_link_bg = '';
        $byblog_link_single = '';
    }
?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<?php			
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'featured');
			
			$status_bg = $thumb_url[0];
			if( !empty($status_bg) ) {
				$status_bg_code = 'style="background-image: url('.esc_url( $status_bg ).'); background-size: cover;"';
			} else if( !empty($byblog_link_bg) ) {
				$status_bg_code = 'style=" background:'.$byblog_link_bg.'"';
			} else {
				$status_bg_code = '';
			}

			if($byblog_cover == '0' || $byblog_cover == '') {
                // Post Header
                get_template_part('template-parts/post-header');
            }
        ?>
		<div class="post-inner">
			<?php
				if($byblog_options['byblog_single_featured'] == '1') {
					if(empty($byblog_link_single)) { ?>
                        <a href="<?php echo esc_url( $byblog_link_url ); ?>">
                            <div class="post-content post-format-link" <?php echo $status_bg_code; ?>>
					           <div class="post-content-format">
                                    <i class="fa fa-link post-format-icon"></i>
                                    <div class="post-format-link-content">
                                        <?php echo esc_url( $byblog_link_url ); ?>
                                    </div>
                                </div>
                            </div>
                        </a><?php
                    }
				}
			?>
			<div class="post-content entry-content single-post-content">
				<?php if($byblog_options['byblog_below_title_ad'] != '') { ?>
					<div class="single-post-ad">
						<?php echo $byblog_options['byblog_below_title_ad']; ?>
					</div>
				<?php } ?>
					
				<?php the_content(); ?>
				
				<?php if($byblog_options['byblog_below_content_ad'] != '') { ?>
					<div class="single-post-ad">
						<?php echo $byblog_options['byblog_below_content_ad']; ?>
					</div>
				<?php } ?>
					
				<?php byblog_wp_link_pages() ?>
                
                <?php get_template_part('template-parts/post-footer-single'); ?>
			</div><!--.single-post-content-->
		</div><!--.post-inner-->
	</div><!--.post-box-->
</article>