<?php
    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_videourl = rwmb_meta( 'byblog_videourl', $args = array('type' => 'text'), $post->ID );
        $byblog_videohost = rwmb_meta( 'byblog_videohost', $args = array('type' => 'text'), $post->ID );
        $byblog_hosted_video = rwmb_meta( 'byblog_hostedvideourl', $args = array('type' => 'file_advanced'), $post->ID );
        $byblog_videocode = rwmb_meta( 'byblog_videocode', $args = array('type' => 'textarea'), $post->ID );
    } else {
        $byblog_videourl = '';
        $byblog_videohost = '';
        $byblog_hosted_video = '';
        $byblog_videocode = '';
    }
?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<div class="post-inner">
            <?php
                if ( $byblog_videocode != '' ) {
                    echo $byblog_videocode;
                } elseif( $byblog_videohost != '' ) {
                    if ( $byblog_videohost == 'youtube' ) {
                        $src = 'http://www.youtube.com/embed/'.$byblog_videourl;
                    } elseif ( $byblog_videohost == 'vimeo' ) {
                        $src = 'http://player.vimeo.com/video/'.$byblog_videourl;
                    } elseif ( $byblog_videohost == 'dailymotion' ) {
                        $src = 'http://www.dailymotion.com/embed/video/'.$byblog_videourl;
                    } elseif ( $byblog_videohost == 'metacafe' ) {
                        $src = 'http://www.metacafe.com/embed/'.$byblog_videourl;
                    } ?>
                    <div class="post-format-content">
                        <iframe allowfullscreen src="<?php echo esc_url( $src ); ?>" class="vid iframe-<?php echo esc_attr( $byblog_videohost ); ?>"></iframe>
                    </div>
                    <?php
                } elseif ($byblog_hosted_video != NULL) { ?>
                    <div class="post-format-content">
                        <?php
                            foreach ( $byblog_hosted_video as $byblog_hosted_video_id ) {
                                echo do_shortcode( '[video src="'. esc_url( $byblog_hosted_video_id['url'] ) .'"][/video]' );
                            }
                        ?>
                    </div><?php
                }

                // Post Header
                get_template_part('template-parts/post-header');

                // Post Content
                get_template_part('template-parts/post-content-front');
            ?>
		</div><!--.post-inner-->
	</div><!--.post-box-->
</article>