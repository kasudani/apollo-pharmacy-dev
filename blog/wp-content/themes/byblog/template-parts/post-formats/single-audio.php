<?php
    global $byblog_options;

    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_cover = rwmb_meta( 'byblog_post_cover_show', $args = array('type' => 'checkbox'), $post->ID );
        $byblog_audio_url = rwmb_meta( 'byblog_audiourl', $args = array('type' => 'text'), $post->ID );
        $byblog_audio_host = rwmb_meta( 'byblog_audiohost', $args = array('type' => 'text'), $post->ID );
        $byblog_audio_mp3 = rwmb_meta( 'byblog_mp3url', $args = array('type' => 'file_advanced'), $post->ID );
        $byblog_audio_embed_code = rwmb_meta( 'byblog_audiocode', $args = array('type' => 'textarea'), $post->ID );
        $byblog_audio_single = rwmb_meta( 'byblog_audio_single_hide', $args = array('type' => 'checkbox'), $post->ID );
    } else {
        $byblog_cover = '';
        $byblog_audio_url = '';
        $byblog_audio_host = '';
        $byblog_audio_mp3 = '';
        $byblog_audio_embed_code = '';
        $byblog_audio_single = '';
    }
?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<?php 
			if( $byblog_cover == '0' || $byblog_cover == '' ) {
                // Post Header
                get_template_part('template-parts/post-header');
            }

			if( $byblog_options['byblog_single_featured'] == '1' ) {
                if( empty( $byblog_audio_single ) ) {
                // Single
                ?>
                <div class="audio-box clearfix">
                    <?php if ( $byblog_audio_embed_code != '' ) {
                        echo $byblog_audio_embed_code;
                    } else if( $byblog_audio_host == 'soundcloud' ) { ?>
                        <iframe border="no" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=<?php if($byblog_audio_url != '') { echo esc_url( $byblog_audio_url ); } ?>&auto_play=false&hide_related=false&visual=true"></iframe>
                    <?php } else if ( $byblog_audio_host == 'mixcloud' ) { ?>
                        <iframe src="//www.mixcloud.com/widget/iframe/?feed=<?php if( $byblog_audio_url != '' ) { echo esc_url( $byblog_audio_url ); } ?>&embed_type=widget_standard&embed_uuid=43f53ec5-65c0-4d1f-8b55-b26e0e7c2288&hide_tracklist=1&hide_cover=0" frameborder="0"></iframe>
                    <?php } else if ( $byblog_audio_mp3 != NULL ) {
                        foreach ( $byblog_audio_mp3 as $byblog_audio_mp3_id ) {
                            echo do_shortcode( '[audio src="'. esc_url( $byblog_audio_mp3_id['url'] ) .'"][/audio]' );
                        }
                    } ?>
                </div>
                <?php }
			}

            get_template_part('template-parts/single-content');
        ?>
	</div><!--.post-box-->
</article>