<?php
    global $byblog_options;

    if ( function_exists( 'rwmb_meta' ) ) {
        $byblog_cover = rwmb_meta( 'byblog_post_cover_show', $args = array('type' => 'checkbox'), $post->ID );
        $byblog_image_single = rwmb_meta( 'byblog_image_single_hide', $args = array('type' => 'checkbox'), $post->ID );
    } else {
        $byblog_cover = '';
        $byblog_image_single = '';
    }
?>
<article <?php post_class(); ?>>
	<div id="post-<?php the_ID(); ?>" class="post-box">
		<?php 
			if( $byblog_cover == '0' || $byblog_cover == '' ) {   
                // Post Header
                get_template_part('template-parts/post-header');
            }
			
            $byblog_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

            if( $byblog_options['byblog_single_featured'] == '1' ) {
                if( empty( $byblog_image_single ) ) {
                    if( $byblog_cover == '0' || $byblog_cover == '' ) {
                        if ( has_post_thumbnail() ) {
                            $byblog_image_size = ( $byblog_options['byblog_crop_images'] == 1 ? 'featured' : 'full' ); ?>
                            <a href="<?php echo esc_url( $byblog_url ); ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail featured-thumbnail-big">
                                <?php the_post_thumbnail( $byblog_image_size ); ?>
                            </a><?php
                        }
                    }
                }
            }

            get_template_part('template-parts/single-content');
        ?>
	</div><!--.post-box-->
</article>