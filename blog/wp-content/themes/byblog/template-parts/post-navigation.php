<?php
// Don't print empty markup if there's nowhere to navigate.
$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
$next     = get_adjacent_post( false, '', false );

if ( ! $next && ! $previous ) {
    return;
}

?>
<nav class="navigation post-navigation clearfix" role="navigation">
    <?php
    if ( is_attachment() ) :
        next_post_link('<div class="alignleft post-nav-links prev-link-wrapper"><div class="next-link"><span class="uppercase">'. esc_html__("Published In","byblog") .'</span> %link'."</div></div>");
    else :

        $prev_post_bg = '';
        $next_post_bg = '';

        $prev_post = get_previous_post();
        if (!empty( $prev_post )):
            $prev_post_bg = get_the_post_thumbnail( $prev_post->ID, 'byblog-widgetthumb' );
        endif;

        $next_post = get_next_post();
        if (!empty( $next_post )):
            $next_post_bg = get_the_post_thumbnail( $next_post->ID, 'byblog-widgetthumb' );
        endif;

        previous_post_link('<div class="alignleft post-nav-links prev-link-wrapper"><span class="uppercase">'. esc_html__("Previous Article","byblog").'</span><div class="prev-link"><div class="post-nav-link-thumb">'. $prev_post_bg .'</div>%link'."</div></div>");
        next_post_link('<div class="alignright post-nav-links next-link-wrapper"><span class="uppercase">'. esc_html__("Next Article","byblog") .'</span> <div class="post-nav-link-thumb">'. $next_post_bg .'</div><div class="next-link">%link'."</div></div>");
    endif;
    ?>
</nav><!-- .navigation -->
