<?php
    global $byblog_options;
    $orig_post = $post;
    global $post;

    $categories = '';
    $tags = '';
    $byblog_related_posts_count = $byblog_options['byblog_related_posts_count'];

    $byblog_related_posts_count = ( $byblog_related_posts_count ) ? $byblog_related_posts_count : '3';

    //Related Posts By Tags
    if ( $byblog_options['byblog_related_posts_by'] == '2' ) {
        $tags = wp_get_post_tags($post->ID);        
        if ( $tags ) {
            $tag_ids = array();  
            foreach( $tags as $individual_tag ) $tag_ids[] = $individual_tag->term_id;  
            $args = array(
                'tag__in' => $tag_ids,
                'post__not_in' => array( $post->ID ),
                'posts_per_page'=> $byblog_related_posts_count, // Number of related posts to display.  
                'ignore_sticky_posts'=> 1
            ); 
        }
    }
    // Related Posts by Author
    elseif ( $byblog_options['byblog_related_posts_by'] == '3' ) {
        $byblog_author_id = get_the_author_meta('ID') ;
        $byblog_user_post_count = count_user_posts( $byblog_author_id );
        
        $args = array(
            'author' => $byblog_author_id,
            'post__not_in' => array( $post->ID ),
            'posts_per_page'=> $byblog_related_posts_count, // Number of related posts that will be shown.
            'ignore_sticky_posts'=> 1
        );
    }
    //Related Posts By Categories
    else {
        $categories = get_the_category($post->ID);
        if ( $categories ) {
            $category_ids = array();
            foreach( $categories as $individual_category ) $category_ids[] = $individual_category->term_id;
            $args = array(
                'category__in' => $category_ids,
                'post__not_in' => array( $post->ID ),
                'posts_per_page'=> $byblog_related_posts_count, // Number of related posts that will be shown.
                'ignore_sticky_posts'=> 1
            );
        }
    }
    if ( $categories || $tags || $byblog_user_post_count ) {
        $my_query = new WP_Query( $args );
        if( $my_query->have_posts() ) {
            echo '<div class="related-posts single-box"><h3 class="section-heading uppercase">' . esc_html__('Related Posts','byblog') . '</h3><ul>';
            while( $my_query->have_posts() ) {
                $my_query->the_post();?>
                <li>
                    <?php if ( has_post_thumbnail() ) { ?> 
                        <a class="featured-thumbnail" href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow">
                            <div class="relatedthumb clearfix"><?php the_post_thumbnail('byblog-widththumb-full'); ?></div>
                        </a>
                    <?php } ?>
                    <?php
                        $post_title = the_title('','',false);
                        $short_title = substr($post_title,0,38);

                        if ( $short_title != $post_title ) {
                            $short_title .= "...";
                        } else {
                            $short_title = $post_title;
                        }
                    ?>
                    <div class="related-content">
                        <header>
                            <h2 class="widgettitle">
                                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h2>
                        </header><!--.header-->
                        <?php
                            if( $byblog_options['byblog_post_meta_options']['2'] == '1' ) { ?>
                                <div class="post-meta">
                                    <span class="post-date">
                                        <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php the_time(get_option( 'date_format' )); ?></time>
                                    </span>
                                </div><?php
                            }
                        ?>
                    </div><!--.related-content-->
                </li>
                <?php
            }
            echo '</ul></div>';
        }
    }
    $post = $orig_post;
    wp_reset_postdata();
?>