<?php global $byblog_options; ?>
<div class="fs-wrap clearfix">
    <div class="container">
    <?php
        $byblog_slider_style = $byblog_options['byblog_slider_style'];
        if ( $byblog_slider_style == 'carousel' || $byblog_slider_style == 'slider' ) {
            if ( $byblog_slider_style == 'carousel' ) {
                $byblog_slider_thumb = 'byblog-carousel'; ?>
                <div class="carousel-2 carousel loading clearfix"><?php
            } else {
                $byblog_slider_thumb = 'byblog-slider'; ?>
                <div class="featuredslider loading clearfix"><?php
            }
            if ( $byblog_options['byblog_slider_type'] == 'posts_by_cat' ) {
                if( empty($byblog_options['byblog_slider_cat']) ) {
                    esc_html_e( 'Please chose a category for slider','byblog' );
                } else {
                    $featured_slider_cats = $byblog_options['byblog_slider_cat'];
                    $featured_slider_posts_count = $byblog_options['byblog_slider_posts_count'];
                    $featured_slider_cat = implode(",", $featured_slider_cats);
                    
                    $featured_posts = new WP_Query( array(
                        'cat'       => $featured_slider_cat,
                        'orderby'   => 'date',
                        'order'     => 'DESC',
                        'showposts' => $featured_slider_posts_count,
                    ) );

                    if($featured_posts->have_posts()) : while ($featured_posts->have_posts()) : $featured_posts->the_post(); ?>
                    <div class="item">
                        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail f-thumb">
                            <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail( $byblog_slider_thumb );
                                }
                            ?>
                            <div class="post-inner">
                                <div class="slider-inner">
                                    <div class="post-meta">
                                        <?php 
                                            if($byblog_options['byblog_post_meta_options']['3'] == '1') { ?>
                                            <span class="post-cats">
                                                <?php
                                                    $category = get_the_category();
                                                    if ( $category ) {
                                                        echo '<span>' . esc_html__('in','byblog') . '</span>' . $category[0]->name;
                                                    }
                                                ?>
                                            </span>
                                        <?php }
                                            if( $byblog_options['byblog_post_meta_options']['2'] == '1' ) { ?>
                                            <span class="post-date">
                                                <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php the_time(get_option( 'date_format' )); ?></time>
                                            </span>
                                        <?php } ?>
                                    </div><!--.post-meta-->
                                    <header>
                                        <h2 class="title f-title">
                                            <?php the_title(); ?>
                                        </h2>
                                    </header><!--.header-->
                                    <?php if($byblog_options['byblog_post_meta_options']['1'] == '1') { ?>
                                        <span class="post-author post-meta">
                                            <span>
                                                <?php esc_html_e('By','byblog'); ?>
                                            </span>
                                            <?php the_author(); ?>
                                        </span>
                                    <?php } ?>
                                    <?php if ( $byblog_slider_style == 'carousel' ) { ?>
                                        <div class="slider-content">
                                            <p><?php echo byblog_excerpt_limit(18); ?></p>
                                        </div>
                                    <?php } ?>
                                    <?php if ( $byblog_slider_style == 'slider' ) { ?>
                                    <div class="read-more">
                                        <span><?php esc_html_e('Read More','byblog'); ?></span>
                                    </div>
                                    <?php } ?>
                                </div><!--.slider-inner-->
                            </div><!--.post-inner-->
                        </a>
                    </div>
                    <?php
                    endwhile;
                    endif;
                }
            } elseif ( $byblog_options['byblog_slider_type'] == 'sel_posts' ) {
                
                if( empty($byblog_options['byblog_slider_posts']) ) {
                    esc_html_e( 'Please chose posts for slider','byblog' );
                } else {
                    $byblog_slider_posts = ( isset ( $byblog_options['byblog_slider_posts'] ) ? $byblog_options['byblog_slider_posts'] : '' );

                    if ( $byblog_slider_posts ) {
                        $featured_posts = new WP_Query( array(
                            'post_type'           => 'post',
                            'post__in'            => $byblog_slider_posts,
                            'ignore_sticky_posts' => 1
                        ) );

                        if($featured_posts->have_posts()) : while ($featured_posts->have_posts()) : $featured_posts->the_post(); ?>
                        <div class="item">
                            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail f-thumb">
                                <?php
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail( $byblog_slider_thumb );
                                    }
                                ?>
                                <div class="post-inner">
                                    <div class="slider-inner">
                                        <div class="post-meta">
                                            <?php 
                                                if($byblog_options['byblog_post_meta_options']['3'] == '1') { ?>
                                                <span class="post-cats">
                                                    <?php
                                                        $category = get_the_category();
                                                        if ( $category ) {
                                                            echo '<span>' . esc_html__('in','byblog') . '</span>' . $category[0]->name;
                                                        }
                                                    ?>
                                                </span>
                                            <?php }
                                                if( $byblog_options['byblog_post_meta_options']['2'] == '1' ) { ?>
                                                <span class="post-date">
                                                    <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php the_time(get_option( 'date_format' )); ?></time>
                                                </span>
                                            <?php } ?>
                                        </div><!--.post-meta-->
                                        <header>
                                            <h2 class="title f-title">
                                                <?php the_title(); ?>
                                            </h2>
                                        </header><!--.header-->
                                        <?php if($byblog_options['byblog_post_meta_options']['1'] == '1') { ?>
                                            <span class="post-author post-meta">
                                                <span>
                                                    <?php esc_html_e('By','byblog'); ?>
                                                </span>
                                                <?php the_author(); ?>
                                            </span>
                                        <?php } ?>
                                        <?php if ( $byblog_slider_style == 'carousel' ) { ?>
                                            <div class="slider-content">
                                                <p><?php echo byblog_excerpt_limit(18); ?></p>
                                            </div>
                                        <?php } ?>
                                        <?php if ( $byblog_slider_style == 'slider' ) { ?>
                                        <div class="read-more">
                                            <span><?php esc_html_e('Read More','byblog'); ?></span>
                                        </div>
                                        <?php } ?>
                                    </div><!--.slider-inner-->
                                </div><!--.post-inner-->
                            </a>
                            </div>
                            <?php
                            endwhile;

                            wp_reset_postdata();

                            else:

                            endif;
                        }
                    }
                } elseif ( $byblog_options['byblog_slider_type'] == 'custom_slides' ) {
                    $byblog_slides_array = $byblog_options['byblog_custom_slidess'];
                    foreach ($byblog_slides_array as $value) { ?>
                        <div class="item">
                            <?php
                                if ( !empty($value['url'])) { ?>
                                    <a href="<?php echo esc_url( $value['url'] ); ?>" class="featured-thumbnail f-thumb">
                                        <img src="<?php echo esc_url( $value['image'] ); ?>">
                                            <div class="post-inner">
                                                <div class="slider-inner">
                                                    <header>
                                                    <?php if($value['url'] != '') {
                                                        echo '<h2 class="title f-title">'. $value['title'] . '</h2>';
                                                    } else {
                                                        if($value['title'] != '') { ?>
                                                        <h2 class="title f-title"><?php echo esc_attr( $value['title'] ); ?></h2>
                                                    <?php } } ?>
                                                    </header><!--.header-->
                                                    <?php if($value['description'] != '') { ?>
                                                        <div class="slider-desc"><?php echo do_shortcode($value['description']); ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                    </a>
                                <?php } else { ?>
                                    <img src="<?php echo esc_url( $value['image'] ); ?>">
                                        <div class="post-inner">
                                            <div class="slider-inner">
                                                <header>
                                                    <?php if($value['url'] != '') {
                                                        echo '<h2 class="title f-title">'. $value['title'] . '</h2>';
                                                    } else {
                                                    if($value['title'] != '') { ?>
                                                        <h2 class="title f-title"><?php echo esc_attr( $value['title'] ); ?></h2>
                                                    <?php } } ?>
                                                </header><!--.header-->
                                                <?php if($value['description'] != '') { ?>
                                                    <div class="slider-desc"><?php echo do_shortcode($value['description']); ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                <?php }
                            ?>
                    </div> 
                    <?php }
            }
        } else { ?>
            <div class="featured-section clearfix">
                <?php
                    $fcount = 1;
                    $featured_cats = $byblog_options['byblog_featured_cat'];
                    $featured_cat = implode(",", $featured_cats);

                    $featured_posts = new WP_Query( array(
                        'cat'       => $featured_cat,
                        'orderby'   => 'date',
                        'order'     => 'DESC',
                        'showposts' => 3,
                    ) );

                    if( $featured_posts->have_posts() ) : while ( $featured_posts->have_posts() ) : $featured_posts->the_post();
                        if ($fcount == 2) { ?><div class="featured-posts-right"><?php } ?>
                        <div class="clearfix featured-post featured-post-<?php echo $fcount; ?>">
                            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="featured-thumbnail f-thumb">
                                <?php
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('byblog-featured');
                                    }
                                ?>
                                <div class="post-inner">
                                    <div class="post-meta">
                                        <?php 
                                            if($byblog_options['byblog_post_meta_options']['3'] == '1') { ?>
                                            <span class="post-cats">
                                                <?php
                                                    $category = get_the_category();
                                                    if ( $category ) {
                                                        echo $category[0]->name;
                                                    }
                                                ?>
                                            </span>
                                        <?php }
                                            if( $byblog_options['byblog_post_meta_options']['2'] == '1' ) { ?>
                                            <span class="post-date">
                                                <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php the_time(get_option( 'date_format' )); ?></time>
                                            </span>
                                        <?php } ?>
                                    </div><!--.post-meta-->
                                    <header>
                                        <h2 class="title f-title">
                                            <?php the_title(); ?>
                                        </h2>
                                    </header><!--.header-->
                                    <?php if ( $fcount == 1 ) { ?>
                                        <?php if( $byblog_options['byblog_post_meta_options']['1'] == '1' ) { ?>
                                            <span class="post-author post-meta">
                                                <span>
                                                    <?php esc_html_e('By','byblog'); ?>
                                                </span>
                                                <?php the_author(); ?>
                                            </span>
                                        <?php } ?>
                                    <?php } ?>
                                </div><!--.post-inner-->
                            </a>
                        </div><!--.featured-post--> 
                        <?php if ( $fcount == 3 ) { ?></div><?php } ?>
                    <?php $fcount++; endwhile; endif; ?>
        <?php } ?>
        </div><!--.carousel-->
    </div><!--.container-->
</div><!--.featuredslider-->