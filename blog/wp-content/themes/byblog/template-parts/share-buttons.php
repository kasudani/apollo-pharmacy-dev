<?php global $byblog_options; ?>
<div class="share-buttons">
	<?php if(is_single()) {
            $byblog_social_array = $byblog_options['byblog_share_buttons'];

            foreach ($byblog_social_array as $key=>$value) {
                if($key == "twitter" && $value == "1") { ?>
                    <!-- Twitter -->
                    <span class="social-btn social-twitter">
                        <a rel="nofollow" href="http://twitter.com/home?status=<?php echo urlencode(get_the_title()); ?>+<?php the_permalink() ?>" title="<?php _e('Share on Twitter','byblog'); ?>" target="_blank">
                            <span class="fa fa-twitter" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Twitter','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "fb" && $value == "1") { ?>
                    <!-- Facebook -->
                    <span class="social-btn social-fb">
                        <a rel="nofollow" href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&amp;title=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on Facebook','byblog'); ?>" target="_blank">
                            <span class="fa fa-facebook" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Facebook','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "gplus" && $value == "1") { ?>
                    <!-- Google+ -->
                    <span class="social-btn social-gplus">
                        <a rel="nofollow" href="https://plus.google.com/share?url=<?php the_permalink() ?>" title="<?php _e('Share on Google+','byblog'); ?>" target="_blank">
                            <span class="fa fa-google-plus" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Google+','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "linkedin" && $value == "1") { ?>
                    <!-- LinkedIn -->
                    <span class="social-btn social-linkedin">
                        <a rel="nofollow" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink() ?>&amp;title=<?php echo urlencode(get_the_title()); ?>&amp;source=<?php echo esc_url( home_url() ); ?>" title="<?php _e('Share on LinkedIn','byblog'); ?>" target="_blank">
                            <span class="fa fa-linkedin" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on LinkedIn','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "pinterest" && $value == "1") { ?>
                    <!-- Pinterest -->
                    <?php $pin_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); $pin_url = $pin_thumb['0']; ?>
                    <span class="social-btn social-pinterest">
                        <a rel="nofollow" href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo esc_url( $pin_url ); ?>&amp;url=<?php the_permalink() ?>&amp;is_video=false&amp;description=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on Pinterest','byblog'); ?>" target="_blank">
                            <span class="fa fa-pinterest" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Pinterest','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "stumbleupon" && $value == "1") { ?>
                    <!-- StumbleUpon -->
                    <span class="social-btn social-stumbleupon">
                        <a rel="nofollow" href="http://www.stumbleupon.com/submit?url=<?php the_permalink() ?>&amp;title=<?php urlencode(the_title('','',false)); ?>" title="<?php _e('Share on StumbleUpon','byblog'); ?>" target="_blank">
                            <span class="fa fa-stumbleupon" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on StumbleUpon','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "reddit" && $value == "1") { ?>
                    <!-- Reddit -->
                    <span class="social-btn social-reddit">
                        <a rel="nofollow" href="http://www.reddit.com/submit?url=<?php the_permalink() ?>&amp;title=<?php urlencode(the_title('','',false)); ?>" title="<?php _e('Share on Reddit','byblog'); ?>" target="_blank">
                            <span class="fa fa-reddit" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Reddit','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "tumblr" && $value == "1") { ?>
                    <!-- Tumblr -->
                    <span class="social-btn social-tumblr">
                        <a rel="nofollow" href="http://www.tumblr.com/share?v=3&amp;u=<?php the_permalink() ?>&amp;t=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on tumblr','byblog'); ?>" target="_blank">
                            <span class="fa fa-tumblr" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on tumblr','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "delicious" && $value == "1") { ?>
                    <!-- Delicious -->
                    <span class="social-btn social-delicious">
                        <a rel="nofollow" href="http://del.icio.us/post?url=<?php the_permalink() ?>&amp;title=<?php echo urlencode(get_the_title()); ?>&amp;notes=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on Delicious','byblog'); ?>" target="_blank">
                            <span class="fa fa-delicious" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Delicious','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                else {
                    echo "";
                }
            }
        } else {
            $byblog_social_array_home = $byblog_options['byblog_share_buttons_home'];

            foreach ($byblog_social_array_home as $key=>$value) {
                if($key == "twitter" && $value == "1") { ?>
                    <!-- Twitter -->
                    <span class="social-btn social-twitter">
                        <a rel="nofollow" href="http://twitter.com/home?status=<?php echo urlencode(get_the_title()); ?>+<?php the_permalink() ?>" title="<?php _e('Share on Twitter','byblog'); ?>" target="_blank">
                            <span class="fa fa-twitter" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Twitter','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "fb" && $value == "1") { ?>
                    <!-- Facebook -->
                    <span class="social-btn social-fb">
                        <a rel="nofollow" href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&amp;title=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on Facebook','byblog'); ?>" target="_blank">
                            <span class="fa fa-facebook" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Facebook','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "gplus" && $value == "1") { ?>
                    <!-- Google+ -->
                    <span class="social-btn social-gplus">
                        <a rel="nofollow" href="https://plus.google.com/share?url=<?php the_permalink() ?>" title="<?php _e('Share on Google+','byblog'); ?>" target="_blank">
                            <span class="fa fa-google-plus" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Google+','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "linkedin" && $value == "1") { ?>
                    <!-- LinkedIn -->
                    <span class="social-btn social-linkedin">
                        <a rel="nofollow" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink() ?>&amp;title=<?php echo urlencode(get_the_title()); ?>&amp;source=<?php echo esc_url( home_url() ); ?>" title="<?php _e('Share on LinkedIn','byblog'); ?>" target="_blank">
                            <span class="fa fa-linkedin" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on LinkedIn','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "pinterest" && $value == "1") { ?>
                    <!-- Pinterest -->
                    <?php $pin_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); $pin_url = $pin_thumb['0']; ?>
                    <span class="social-btn social-pinterest">
                        <a rel="nofollow" href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo esc_url( $pin_url ); ?>&amp;url=<?php the_permalink() ?>&amp;is_video=false&amp;description=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on Pinterest','byblog'); ?>" target="_blank">
                            <span class="fa fa-pinterest" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Pinterest','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "stumbleupon" && $value == "1") { ?>
                    <!-- StumbleUpon -->
                    <span class="social-btn social-stumbleupon">
                        <a rel="nofollow" href="http://www.stumbleupon.com/submit?url=<?php the_permalink() ?>&amp;title=<?php urlencode(the_title('','',false)); ?>" title="<?php _e('Share on StumbleUpon','byblog'); ?>" target="_blank">
                            <span class="fa fa-stumbleupon" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on StumbleUpon','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "reddit" && $value == "1") { ?>
                    <!-- Reddit -->
                    <span class="social-btn social-reddit">
                        <a rel="nofollow" href="http://www.reddit.com/submit?url=<?php the_permalink() ?>&amp;title=<?php urlencode(the_title('','',false)); ?>" title="<?php _e('Share on Reddit','byblog'); ?>" target="_blank">
                            <span class="fa fa-reddit" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Reddit','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "tumblr" && $value == "1") { ?>
                    <!-- Tumblr -->
                    <span class="social-btn social-tumblr">
                        <a rel="nofollow" href="http://www.tumblr.com/share?v=3&amp;u=<?php the_permalink() ?>&amp;t=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on tumblr','byblog'); ?>" target="_blank">
                            <span class="fa fa-tumblr" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on tumblr','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                elseif($key == "delicious" && $value == "1") { ?>
                    <!-- Delicious -->
                    <span class="social-btn social-delicious">
                        <a rel="nofollow" href="http://del.icio.us/post?url=<?php the_permalink() ?>&amp;title=<?php echo urlencode(get_the_title()); ?>&amp;notes=<?php echo urlencode(get_the_title()); ?>" title="<?php _e('Share on Delicious','byblog'); ?>" target="_blank">
                            <span class="fa fa-delicious" aria-hidden="true"></span>
                            <span class="screen-reader-text"><?php _e('Share on Delicious','byblog'); ?></span>
                        </a>
                    </span>
                <?php }
                else {
                    echo "";
                }
            }
        } ?>
</div><!--.share-buttons-->