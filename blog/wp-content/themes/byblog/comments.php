<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */
 
	// Do not delete these lines
	if ( post_password_required() ) {
		return;
	}

	if ( have_comments() ) : ?>
		<div id="comments" class="comments-area single-box clearfix">
			<h3 class="comments-count section-heading uppercase"><span>
                <?php
                    printf( _n( '1 Comment', '%s Comments', get_comments_number(), 'byblog' ), number_format_i18n( get_comments_number() ) );
                ?>
            </span></h3>
			<?php	
				if (get_option('page_comments')) {
					$comment_pages = paginate_comments_links('echo=0');
					if($comment_pages){
					 echo '<div class="commentnavi pagination">'.$comment_pages.'</div>';
					}
				}
			?>
			<ol class="commentlist clearfix">
				<?php
					wp_list_comments( array(
						'callback'      => 'byblog_comment',
						'type'      => 'comment',
						'short_ping' => true,
						'avatar_size'=> 60,
					) );
				?>
				<?php
					wp_list_comments( array(
						'type'      => 'pingback',
						'short_ping' => true,
						'avatar_size'=> 60,
					) );
				?>
			</ol>
			<?php	
				if (get_option('page_comments')) {
					$comment_pages = paginate_comments_links('echo=0');
					if($comment_pages){
					 echo '<div class="commentnavi pagination">'.$comment_pages.'</div>';
					}
				}
			?>
		</div><!-- #comments -->

	<?php else : // this is displayed if there are no comments so far

	if ('open' == $post->comment_status) :
		// If comments are open, but there are no comments.

	else : // comments are closed
		// If comments are closed.
		
	endif;
	endif;
	global $aria_req; $comments_args = array(
		// remove "Text or HTML to be displayed after the set of comment fields"
		'title_reply'=>'<h4 class="section-heading uppercase"><span>' . esc_html__('Leave a Reply','byblog').'</span></h4>',
		'comment_notes_before' => '',
		'comment_notes_after' => '',
		'fields' => $fields =  array(
			  'author' =>
				'<p class="comment-form-author"><label for="author">' . esc_html__( 'Name ', 'byblog' ) .
				( $req ? '<span class="required">*</span>' : '' ) . '</label> ' .
				'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
				'" size="19"' . $aria_req . ' /></p>',

			  'email' =>
				'<p class="comment-form-email"><label for="email">' . esc_html__( 'Email ', 'byblog' ) . 
				( $req ? '<span class="required">*</span>' : '' ) . '</label> ' .
				'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
				'" size="19"' . $aria_req . ' /></p>',

			  'url' =>
				'<p class="comment-form-url"><label for="url">' . esc_html__( 'Website', 'byblog' ) . '</label>' .
				'<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
				'" size="19" /></p>',
			),
		'comment_field' => '<p class="comment-form-comment"><label for="comment">' . esc_html__( 'Comments ', 'byblog' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
		'label_submit' => esc_html__( 'Submit ', 'byblog' ),
	);
	
	comment_form($comments_args);
	?>