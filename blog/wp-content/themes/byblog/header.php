<!DOCTYPE html>
<?php
    global $byblog_options;

    // Page Variables
    $byblog_layout_type = $byblog_options['byblog_layout_type'];
    $byblog_header_style = $byblog_options['byblog_header_style'];
?>
<html <?php language_attributes(); ?>>
<head itemscope itemtype="http://schema.org/WebSite">
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta itemprop="url" content="<?php echo esc_url( home_url( '/' ) ); ?>"/>
    <meta itemprop="name" content="<?php bloginfo( 'name' ); ?>"/>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<meta name="viewport" content="width=device-width" />
    <?php
        if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
            if ( !empty( $byblog_options['byblog_favicon']['url'] ) ) { ?>
                <link rel="icon" href="<?php echo esc_url( $byblog_options['byblog_favicon']['url'] ); ?>" type="image/x-icon" />
                <?php
            }
        }
    ?>
	<?php wp_head(); ?>
</head>
<?php $byblog_layout_class = ( $byblog_layout_type == '2' ) ? ' boxed-layout' : ''; ?>
<body id="blog" <?php body_class('main'); ?> itemscope itemtype="http://schema.org/WebPage">
	<div id="st-container" class="st-container">
        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'byblog' ); ?></a>
		<nav class="st-menu">
			<div id="close-button"><i class="fa fa-times"></i></div>
			<div class="off-canvas-search">
				<div class="off-search"><?php get_search_form(); ?></div>
			</div>
			<?php
                if ( has_nav_menu( 'main-menu' ) ) {
				    wp_nav_menu( array(
                        'theme_location' => 'main-menu',
                        'menu_class'     => 'menu',
                        'container'      => '',
                        'walker'         => new byblog_nav_walker_mobile
                    ) );
                }
            ?>
		</nav>
		<div class="main-container<?php if( $byblog_layout_type != '1' ) { echo $byblog_layout_class; } ?>">
			<div class="menu-pusher">
				<!-- START HEADER -->
				<?php
					if ( empty( $byblog_header_style ) )  {
						$byblog_header_style = '2';
					}
					get_template_part('template-parts/header-'.$byblog_header_style );
				?>
				<!-- END HEADER -->