(function ($, window) {
    "use strict";

    // Responsive Navigation Menu
    //--------------------------------------------//
    function bpxl_responsive_menu() {
        $(".st-menu ul li").has('ul').prepend("<span class='side-sub-menu'><i class='fa fa-angle-down'></i></span>");

        $('.st-menu ul li .side-sub-menu').on("click", function(){
            $(this).next().next().slideToggle(300);
            if ($(this).children().hasClass('fa-angle-down')) {
                $(this).children().removeClass('fa-angle-down').addClass('fa-angle-up');
            } else {
                $(this).children().removeClass('fa-angle-up').addClass('fa-angle-down');
            }
        });

        $(".menu-btn").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            $("html").toggleClass("openNav");
        });
    
        $(document).on( 'click', function ( e ) {
            var mouseclick = $(e.target);
            //console.log(mouseclick);
            if ( mouseclick.hasClass('st-menu') || mouseclick.parent().hasClass('st-menu') || mouseclick.parent().hasClass('side-sub-menu') ) {
                return;
            }
            else {
                $('html').removeClass("openNav");
            }
        } );
    };

    // Tabs
    //--------------------------------------------//
    function bpxl_tabs() {
        "use strict";
        $("#tabs li").on("click", function(){
            $("#tabs li").removeClass('active');
            $(this).addClass("active");
            $(".tab-content").hide();
            var selected_tab = $(this).find("a").attr("href");
            $(selected_tab).fadeIn();
            return false;
        });
    };

    // Back to Top Button
    //--------------------------------------------//
    function bpxl_scroll_top() {
        var offset = 220;
        var duration = 500;
        $(window).scroll(function() {
            if ($(this).scrollTop() > offset) {
                $('.back-to-top').fadeIn(duration);
            } else {
                $('.back-to-top').fadeOut(duration);
            }
        });

        $('.back-to-top').on("click", function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop: 0}, duration);
            return false;
        })
    };

    // Cover Image
    //--------------------------------------------//
    function bpxl_cover_image() {
        $('.cover-image').each(function(){
            var $bgobj = $(this); // assigning the object

            $(window).scroll(function() {
                var yPos = -($(window).scrollTop() / $bgobj.data('speed')); 

                // Put together our final background position
                var coords = '50% '+ yPos + 'px';

                // Move the background
                $bgobj.css({ backgroundPosition: coords });
            }); 
        });    
    };

    // Fitvids
    //--------------------------------------------//
    function bpxl_fitvids() {
        $("body").fitVids( {
            customSelector: [
                "iframe[src*='youtu.be']",
                "iframe[src*='soundcloud.com']",
                "iframe[src*='mixcloud.com']",
                "iframe[src*='blip.tv']",
                "iframe[src*='dailymotion.com']",
                "iframe[src*='spotify.com']",
                "iframe[src*='slideshare.net']",
            ]
        } );
        $( ".wp-video-shortcode, .wp-audio-shortcode" ).css( 'max-width', '100%' );
    };

    // Mailchimp
    //--------------------------------------------//
    function bpxl_mailchimp() {
        ajaxMailChimpForm($("#mc-embedded-subscribe-form"), $(".subscribe-result"));
        // Turn the given MailChimp form into an ajax version of it.
        // If resultElement is given, the subscribe result is set as html to
        // that element.
        function ajaxMailChimpForm($form, $resultElement){
            // Hijack the submission. We'll submit the form manually.
            $form.attr("action");
            $form.submit(function(e) {
                e.preventDefault();
                if (!isValidEmail($form)) {
                    var error =  byblog_themescripts.mailchimp_valid_email;
                    $resultElement.html(error);
                    $resultElement.css("color", "red");
                } else {
                    $resultElement.html( byblog_themescripts.subscribing );
                    var url=$form.attr('action');
                    console.log(url);
                    if (url=='') {
                      $resultElement.css("color", "red");
                      $resultElement.html( byblog_themescripts.mailchimp_form_url_msg );
                      return false;
                    } else {
                      url=url.replace('subscribe/post', 'subscribe/post-json');
                    }
                    submitSubscribeForm($form, $resultElement, url);
                }
            });
        }
        // Validate the email address in the form
        function isValidEmail($form) {
            // If email is empty, show error message.
            // contains just one @
            var email = $form.find("input[type='email']").val();
            if (!email || !email.length) {
                return false;
            } else if (email.indexOf("@") == -1) {
                return false;
            }
            return true;
        }
        // Submit the form with an ajax/jsonp request.
        // Based on http://stackoverflow.com/a/15120409/215821
        function submitSubscribeForm($form, $resultElement, $url) {
            $.ajax({
                type: "GET",
                url: $url,
                data: $form.serialize(),
                cache: false,
                dataType: "jsonp",
                jsonp: "c", // trigger MailChimp to return a JSONP response
                contentType: "application/json; charset=utf-8",
                error: function(error){
                    // According to jquery docs, this is never called for cross-domain JSONP requests
                },
                success: function(data){
                    if (data.result != "success") {
                        var message = data.msg || byblog_themescripts.mailchimp_error_msg;
                        $resultElement.css("color", "green");
                        if (data.msg && data.msg.indexOf("already subscribed") >= 0) {
                            message = byblog_themescripts.mailchimp_already_subscribed_msg;
                        }
                        $resultElement.html(message);
                    } else {
                        $resultElement.html( byblog_themescripts.mailchimp_confirm_msg );
                    }
                }
            });
        }
    };

    // Header Search
    //--------------------------------------------//
    function bpxl_header_search() {
        var search_item = 'header .s';
        var search_button = '.header-search .search-submit';
        
        $( search_button ).show();

        $(search_button).toggle(function(){
            $(search_button).removeClass('fa-search');
            $(search_button).addClass('fa-close');
            $(".header-search").addClass('show-search');
        },function(){
            $(search_button).removeClass('fa-close');
            $(search_button).addClass('fa-search');
            $(".header-search").removeClass('show-search');
        });
    };

    // Open Social Share Buttons in Popup Window
    //--------------------------------------------//
    function bpxl_social_window() {
        $('.social-btn a').click(function(e) {
            e.preventDefault();
            window.open($(this).attr('href'), '', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=1');
            return false;
        });
    };
    
    function bpxl_scripts() {
        bpxl_responsive_menu();
        bpxl_tabs();
        bpxl_scroll_top();
        bpxl_cover_image();
        bpxl_fitvids();
        bpxl_mailchimp();
        bpxl_header_search();
        bpxl_social_window();
    }

    $(document).ready(bpxl_scripts);
})(jQuery, window);