<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage byblog
 * @since Byblog 1.0
 */

global $byblog_options;

// Page Variables
$byblog_layout = $byblog_options['byblog_archive_layout'];

get_header(); ?>

<div class="main-wrapper">
	<?php
		$category_ID = get_query_var('cat');
		if( get_tax_meta($category_ID,'byblog_cat_cover_img') ) {
			$byblog_cat_bg = get_tax_meta($category_ID,'byblog_cat_cover_img');
		}
	?>
	<div class="cat-cover-box archive-cover-box" <?php if (!empty($byblog_cat_bg)) { ?>style="background:url('<?php echo esc_url( $byblog_cat_bg['src'] ); ?>') no-repeat fixed 0 0 / cover" <?php } ?>>
        <h1 class="category-title">
            <?php printf( wp_kses(__( 'Category Archives: <span>%s</span>', 'byblog' ), array( 'span' => array() ) ), single_cat_title( '', false ) ); ?>
        </h1>
	</div>
	<div id="page">
		<div class="main-content clearfix <?php byblog_layout_class(); ?>">
			<div class="archive-page">
				<div class="content-area archive-content-area">
                    <div class="content content-archive">
				        <div id="content" class="clearfix">
							<?php

                                $byblog_post_pos_count = 0;

								// Start the Loop.
								if (have_posts()) : while (have_posts()) : the_post();
									
									/*
									 * Include the post format-specific template for the content. If you want to
									 * use this in a child theme, then include a file called called content-___.php
									 * (where ___ is the post format) and that will be used instead.
									 */
									get_template_part( 'template-parts/post-formats/content', get_post_format() );

                                    $byblog_ad_count = ( $byblog_options['byblog_blog_ad_pos'] ? $byblog_options['byblog_blog_ad_pos'] : 1 );

                                    $byblog_ad_code = $byblog_options['byblog_blog_archive_ad'];
                                    if ( $byblog_ad_code ) {
                                        if( $byblog_post_pos_count == $byblog_ad_count && !is_paged() ) :
                                            echo '<div class="blog-ad post t-center">'. $byblog_ad_code .'</div>';
                                        endif;
                                    }

                                    $byblog_post_pos_count++;
							
								endwhile;
								
								else:
									// If no content, include the "No posts found" template.
									get_template_part( 'template-parts/post-formats/content', 'none' );

								endif;
							?>
						</div><!--.content-->
						<?php 
							// Previous/next page navigation.
							byblog_paging_nav();
						?>
					</div><!--.content-archive-->
				</div><!--#content-->
				<?php
                    $byblog_category_ID = get_query_var('cat');
                    $byblog_category_style = get_tax_meta($byblog_category_ID,'byblog_category_style');

					$byblog_layout_array = array(
                        'clayout',
                        'glayout',
                        'flayout'
                    );

                    if ( !empty ( $byblog_category_style ) ) {
                        $byblog_layout = $byblog_category_style;
                    }

                    if ( !in_array( $byblog_layout, $byblog_layout_array ) ) {
                        get_sidebar();
                    }
				?>
			</div><!--.archive-page-->
<?php get_footer(); ?>