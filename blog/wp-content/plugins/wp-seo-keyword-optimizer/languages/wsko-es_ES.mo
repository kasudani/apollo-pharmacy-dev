��    H      \  a   �            !     .     ;     D     V  	   c     m     p     t  	   x     �     �     �     �     �     �     �     �     �     �  
   �     �               /     =     E     V  	   _  	   i     s     z     �     �     �     �  
   �     �     �     �     �     �     �     �     �                  
             &     7     G  
   Z     e     n     u     z     �     �     �  
   �  	   �     �     �     �  	   �     �     �     �  
   �  �  �     �     �     �               1     D     G     K  	   O     Y     _     r       	   �     �     �     �     �     �  
   �     �  "   �          7     K     S     j  	   v  	   �  	   �     �     �     �     �     �  
   �     �  
                       &     ,     4     C     F     N  
   W  	   b     l     �  $   �     �     �     �     �     �     �     �                    $     9  	   B  	   L     V     f     n  
   t     C   %      '           ?          F      1       "             <          ,       &   >   :      !   7            $             9   A   E   #   +                               .   6   
   H          B              4   0      =       *   )   3          	          5                       /   D   -       @             (          ;         G   2      8       404 Settings API Settings Advanced Advanced Settings All Keywords All Pages CO CPC CTR Canonical Clicks Clicks History Concurrency Configuration Content Content is empty Custom Fields Daily Descriptions Devices Duplicates Enter Keyword Error Reports Facebook Engagement Facebook Page General General Settings Headings Headings: Histories Hourly Impressions Impressions History Inactive Keyword Research Keywords Learn more Length Location Media: Metas Monthly Never None Not Set OK Overview Pages Permalinks Position Position History Post Attributes Post Type Settings Post Types Rankings Robots Save Search Settings Sitemap Social Taxonomies Technical Title Length Titles Too Long Too Short Total Favourites Weekly Yearly robots.txt Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: BAVOKO SEO Tools
POT-Creation-Date: 2018-03-06 14:37+0100
PO-Revision-Date: 2018-03-06 16:07+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: wsko.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: es_ES
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Configuración del Error 404! Configuración de la API Avanzado Ajustes avanzados Todas las Palabras claves Todas las páginas CO CPC CTR Canónico Clics Historial de clics concomitante Configuración Contenido No hay contenido&#x0D; Campos personalizados Diario Descripciones Dispositivos Duplicados Buscar Error Reports (informe de errores) Participación en Facebook Página de Facebook General Configuración general Encabezados Títulos: Historias Cada hora Impresiones Historial de impresiones Inactivo Investigación de palabra clave Palabras clave Saber más Longitud Ubicación Medios: Metas Mensual Nunca Ninguno No establecido OK Resumen Páginas Permalinks Posición Historial de posiciones Atributos de entrada Configuración del Tipo de Contenido Tipo de Publicación Ranking Robots Guardar Buscar Ajustes Mapa del sitio Social Taxonomías Técnico Longitud del título Títulos Muy largo Muy corta Total Favoritos Semanal Anual robots.txt 