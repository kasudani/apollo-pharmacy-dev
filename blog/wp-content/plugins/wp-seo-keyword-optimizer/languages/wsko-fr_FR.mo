��    L      |  e   �      p  	   q     {     �     �     �     �  	   �  
   �     �     �     �  	   �     �     �     �                    0     @     N     T     a  
   i     t     �     �     �     �     �     �     �     �  	   �     �     �               %  
   .     9     @     I     P     V     ^  	   d     n     s     {     ~     �  
   �     �     �     �  
   �     �     �     �     �     �     �     	     
	  
   	  	   	     &	     3	     :	  	   C	     M	     ^	     e	  
   l	  �  w	  	   <     F     V     d     l     �     �     �     �     �     �  	   �     �     �     �     �     �               2  	   H     R  	   _     i     r     �     �     �     �  	   �     �     �  	   �     �               )     1  
   I     T     c     l  	   x     �     �     �     �     �     �     �     �     �  
   �     �     �     �          .     :     A  
   M     X     l     x     �  
   �  	   �     �     �  	   �  	   �     �     �     �  
   �               )                      <   C           0       	   2      F   +      &   A   $   B         4                         J             D   
               L      ;          ?   .   >          E          =   7   :   @   -       ,                    5          /   (   "   K       H   3       !              8   #       9       %   6      I   1      *         G   '    .htaccess 404 Settings API Settings Advanced Advanced Settings All Keywords All Pages Bulk Tools CO CPC CTR Canonical Clicks Concurrency Configuration Content Content Length Content is empty Current Backups Custom Fields Daily Descriptions Devices Duplicates Enter Keyword Facebook Clicks Facebook Likes Facebook Page Facebook Reach General General Settings Google Clicks Headings Headings: Hourly Impressions Inactive Keyword Research Keywords Learn more Length Location Media: Metas Monthly Never New Pages None Not Set OK Overview Pages Permalinks Position Post Attributes Post Type Settings Post Types Rankings Robots Save Search Search Volume Settings Sitemap Social Taxonomies Technical Title Length Titles Too Long Too Short Total Favourites Weekly Yearly robots.txt Project-Id-Version: BAVOKO SEO Tools
POT-Creation-Date: 2018-03-06 15:37+0100
PO-Revision-Date: 2018-03-06 15:47+0100
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: wsko.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 .htaccess Paramètres 404 Réglages API Avancé Paramètres avancés Tous les mots-clés Toutes les pages Outils en vrac CO CPC CTR Canonique Clics Concurrence Configuration Contenu Longueur du contenu Le contenu est vide Sauvegardes disponibles Champs personnalisés Quotidien Descriptions Appareils Doublons Entrez un mot-clé Clics Facebook Likes sur Facebook Page Facebook Portée de Facebook Général Réglages généraux Clics Google En-têtes Titrages : Toutes les heures Impressions Inactif Recherche de mots-clés Mots clés En savoir plus Longueur Emplacement Média : Metas Mensuel Jamais Nouvelles Pages Aucun Non défini OK Vue d'ensemble Pages Permaliens Position Afficher les attributs Paramètres de type d’article Types d’article Classements Robots Enregistrer Rechercher Volume de recherche Paramètres Plan du site Social Taxonomies Technique Longueur du titre Titres Trop long Too $hort Total de j’aime Hebdomadaire Annuel robots.txt 