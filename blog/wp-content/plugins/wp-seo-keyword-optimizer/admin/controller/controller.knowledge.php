<?php
if (!defined('ABSPATH')) exit;

class WSKO_Controller_Knowledge extends WSKO_Controller
{
	//Options
	public $icon = "info";
	public $link = "knowledge";
	public $styles = array('admin/css/knowledge.css');
	public $scripts = array('admin/js/knowledge-base.js');
	
	public $ajax_actions = array('get_knowledge_base_article', 'search_knowledge_base', 'rate_knowledge_base_article');
	
	public $template_main = 'admin/templates/knowledge/frame-knowledge.php';
	
	public $article_list_step = 5;
	
	public function get_title()
	{
		return __('Knowledge Base', 'wsko');
	}
	
	public function load_lazy_page_data()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$notif = "";
		$data = array();
		
		$all_articles = WSKO_Class_Knowledge::search_knowledge_base('');
		$articles = array_slice($all_articles, 0, $this->article_list_step);
		$data['kb_articles'] = WSKO_Class_Template::render_template('admin/templates/knowledge/view-articles.php', array('articles' => $articles, 'count' => count($all_articles), 'page' => 1, 'step' => $this->article_list_step), true);
		
		return array(
				'success' => true,
				'data' => $data,
				'notif' => $notif
			);
	}
	
	public function action_get_knowledge_base_article()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$article_id = isset($_POST['article']) ? intval($_POST['article']) : false;
		if ($article_id)
		{
			$info = WSKO_Class_Knowledge::get_knowledge_base_article_info($article_id);
			if ($info)
			{
				$view = WSKO_Class_Template::render_template('admin/templates/knowledge/template-article-modal.php', array('article' => $info), true);
				return array(
						'success' => true,
						'view' => $view
					);
			}
		}
	}
	
	public function action_search_knowledge_base()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$search = isset($_POST['search']) ? sanitize_text_field($_POST['search']) : '';
		$cats = isset($_POST['cats']) ? $_POST['cats'] : array();
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$all_articles = WSKO_Class_Knowledge::search_knowledge_base($search, $cats);
		$count_articles = count($all_articles);
		if ((($page-1)*$this->article_list_step) > $count_articles)
			$page = 1;
		$articles = array_slice($all_articles, ($page-1)*$this->article_list_step, $this->article_list_step);
		$view = WSKO_Class_Template::render_template('admin/templates/knowledge/view-articles.php', array('articles' => $articles, 'count' => $count_articles, 'page' => $page, 'step' => $this->article_list_step, 'search' => $search, 'categories' => $cats), true);
		
		return array(
				'success' => true,
				'view' => $view
			);
	}
	public function action_rate_knowledge_base_article()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$type = isset($_POST['type']) ? sanitize_text_field($_POST['type']) : '';
		$post = isset($_POST['post']) ? intval($_POST['post']) : false;
		
		WSKO_Class_Knowledge::rate_article($post, $type);
		
		return array(
				'success' => true
			);
	}
	//Singleton
	static $instance;
}
?>