<?php
if (!defined('ABSPATH')) exit;

class WSKO_Controller_Dashboard extends WSKO_Controller
{
	//Options
	public $icon = "pie-chart";
	public $link = "dashboard";
	public $uses_timespan = true;
	
	public $template_main = 'admin/templates/dashboard/frame-dashboard.php';
	public $template_notifications = 'admin/templates/dashboard/notifications-dashboard.php';
	
	public function get_title()
	{
		return __('Dashboard', 'wsko');
	}
	
	public function load_lazy_page_data($lazy_data)
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$notif = "";
		$data = array();
		
		$is_fetching = false;
		$global_report = WSKO_Class_Onpage::get_onpage_analysis();
		if ((!$global_report || !isset($global_report['current_report'])) || (WSKO_Class_Search::get_se_token() && !WSKO_Class_Core::get_option('search_query_first_run')))
			$is_fetching = true;
		
		$dashboard_data = $is_fetching ? false : $this->get_cached_var('dashboard_data');
		$tmp_no_data = WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true);
		$tmp_api_fail = WSKO_Class_Template::render_template('admin/templates/template-no-api.php', array(), true);
			
		$se_invalid = WSKO_Class_Search::check_se_access();
		if (!$is_fetching && !$se_invalid && $dashboard_data)
		{
			$data['total_keywords'] = $dashboard_data['kw_count'].WSKO_Class_Template::render_progress_icon($dashboard_data['kw_count_ref'], $dashboard_data['kw_count_ref_perc'], array(), true);
			$data['total_clicks'] = $dashboard_data['kw_clicks'].WSKO_Class_Template::render_progress_icon($dashboard_data['kw_clicks_ref'], $dashboard_data['kw_clicks_ref_perc'], array(), true);
			
			if ($dashboard_data['search_clicks'])
				$data['search_clicks_history'] = WSKO_Class_Template::render_chart('area', array('Date', 'Clicks'), $dashboard_data['search_clicks'], array(), true);
			else
				$data['search_clicks_history'] = $tmp_no_data;
			
			if ($dashboard_data['search_impressions'])
				$data['search_impression_history'] = WSKO_Class_Template::render_chart('area', array('Date', 'Impressions'), $dashboard_data['search_impressions'], array(), true);
			else
				$data['search_impression_history'] = $tmp_no_data;
				
			if ($dashboard_data['search_positions'])
				$data['search_position_history'] = WSKO_Class_Template::render_chart('area', array('Date', 'Position'), $dashboard_data['search_positions'], array(), true);
			else
				$data['search_position_history'] = $tmp_no_data;
				
			if ($dashboard_data['search_ctrs'])
				$data['search_ctr_history'] = WSKO_Class_Template::render_chart('area', array('Date', 'CTR'), $dashboard_data['search_ctrs'], array(), true);
			else
				$data['search_ctr_history'] = $tmp_no_data;
			
			$data['table_keywords'] = WSKO_Class_Template::render_chart('area', array('Date', 'Keywords'), $dashboard_data['search_keywords'], array(), true).'<label style="padding-left:15px;">Top Keywords</label>'.
				WSKO_Class_Template::render_table(array('Keyword', 'Position', 'Clicks' /* , 'Impressions', 'CTR' */), $dashboard_data['table_keywords'], array('no_pages' => true, 'order' => array('col' => 2, 'dir' => 'desc')), true);
			$data['table_pages'] = WSKO_Class_Template::render_chart('area', array('Date', 'Pages'), $dashboard_data['search_pages'], array(), true).'<label style="padding-left:15px;">Top Pages</label>'.
				WSKO_Class_Template::render_table(array('URL', 'Position', 'Clicks' /* , 'Impressions', 'CTR' */), $dashboard_data['table_pages'], array('no_pages' => true, 'order' => array('col' => 2, 'dir' => 'desc')), true);
		}
		else
		{
			$data['total_keywords'] = "-";
			$data['total_clicks'] = "-";
			
			$data['search_clicks_history'] = $tmp_api_fail;
			$data['search_impression_history'] = $tmp_api_fail;
			$data['search_position_history'] = $tmp_api_fail;
			$data['search_ctr_history'] = $tmp_api_fail;
			$data['table_keywords'] = $tmp_api_fail;
			$data['table_pages'] = $tmp_api_fail;
		}
		
		$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
		if (!$is_fetching && $global_analysis && $dashboard_data)
		{
			$data['avg_onpage_score'] = round($dashboard_data['onpage_score'], 2).(isset($dashboard_data['last_onpage_score']) && $dashboard_data['last_onpage_score'] !== false ? WSKO_Class_Template::render_progress_icon($dashboard_data['last_onpage_score'], isset($dashboard_data['last_onpage_score_perc']) ? $dashboard_data['last_onpage_score_perc'] : 0, array(), true) : '');
			if ($dashboard_data['onpage_score_history'])
				$data['onpage_history'] = WSKO_Class_Template::render_chart('area', array('Date', 'Content Score'), $dashboard_data['onpage_score_history'], array(), true);
			else
				$data['onpage_history'] = $tmp_no_data;
			$data['onpage_worst_optimized'] = WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true),'URL',''), $dashboard_data['onpage_worst_optimized'], array('no_pages' => true, 'order' => array('col' => 0, 'dir' => 'asc')), true);
		}
		else
		{
			$data['avg_onpage_score'] = "-";
			
			$data['onpage_history'] = $tmp_no_data;
			$data['onpage_worst_optimized'] = $tmp_no_data;
		}
		
		if (!$is_fetching && ($dashboard_data['social_history_follower'] || $dashboard_data['total_social_follower']))
			$data['total_social_follower'] = intval($dashboard_data['total_social_follower']);
		else
			$data['total_social_follower'] = "-";
		
		if (!$is_fetching && $dashboard_data)
		{
			$header = array('Date');
			if ($dashboard_data['facebook_access'])
			{
				$header[]= 'Facebook';
				if ($dashboard_data['social_top_posts_fb'])
					$data['social_posts_fb'] = WSKO_Class_Template::render_table(array('Post', 'Likes', /* 'Engagement', 'Reach', */ 'Created'), $dashboard_data['social_top_posts_fb'], array('no_pages' => true, 'order' => array('col' => 2, 'dir' => 'desc')), true);
				else
					$data['social_posts_fb'] = $tmp_no_data;
			}
			else
			{
				$data['social_posts_fb'] = $tmp_api_fail;
			}
			if ($dashboard_data['twitter_access'])
			{
				$header[]= 'Twitter';
				if ($dashboard_data['social_top_posts_tw'])
					$data['social_posts_tw'] = WSKO_Class_Template::render_table(array('Post', 'Likes', /* 'Engagement', */ 'Created'), $dashboard_data['social_top_posts_tw'], array('no_pages' => true, 'order' => array('col' => 2, 'dir' => 'desc')), true);
				else
					$data['social_posts_fb'] = $tmp_no_data;
			}
			else
			{
				$data['social_posts_tw'] = $tmp_api_fail;
			}
			if ($dashboard_data['social_history_follower'])
			{
				$data['social_follower_history'] = WSKO_Class_Template::render_chart('area', $header, $dashboard_data['social_history_follower'], array(), true);
			}
			else
			{
				$data['social_follower_history'] = $tmp_no_data;
			}
		}
		else
		{
			$data['social_follower_history'] = $tmp_api_fail;
			$data['social_posts_fb'] = $tmp_api_fail;
			$data['social_posts_tw'] = $tmp_api_fail;
		}
		
		return array(
				'success' => true,
				'data' => $data,
				'notif' => $notif
			);
	}
	
	public function get_cached_dashboard_data($args)
	{
		$res = array('search_keywords' => array(), 'search_pages' => array(), 'search_clicks' => array(),
		'kw_count' => 0, 'kw_count_ref' => 0, 'kw_count_ref_perc' => 0,
		'kw_clicks' => 0, 'kw_clicks_ref' => 0, 'kw_clicks_ref_perc' => 0,
		'last_onpage_score' => false);
		
		$time3 = $this->timespan_start - ($this->timespan_end - $this->timespan_start);
		
		$page_rows = WSKO_Class_Search::get_se_data($this->timespan_start, $this->timespan_end, 'page');
		if ($page_rows)
		{
			$page_rows_ref = WSKO_Class_Search::get_se_data($time3, $this->timespan_start, 'page');
			$count = 0;
			foreach ($page_rows as $row)
			{
				$url = $row->keyval;
				$url_result = false;//WSKO_Class_Helper::url_get_title($url);
				
				$ref_row = false;
				if ($page_rows_ref)
				{
					if (isset($page_rows_ref[$row->keyval]))
					{
						$ref_row = $page_rows_ref[$row->keyval];
					}
				}
				$count++;
				if ($count <= 5)
				{
					if ($ref_row)
					{
						$ref_clicks = $ref_row->clicks != 0 && $ref_row->clicks != $row->clicks ? round(($row->clicks - $ref_row->clicks) / $ref_row->clicks * 100, 2) : 0;
						$ref_position = $ref_row->position != 0 && round($ref_row->position) != round($row->position) ? -round((round($row->position) - round($ref_row->position)) / $ref_row->position * 100, 2) : 0;
						$ref_impressions = $ref_row->impressions != 0 && $ref_row->impressions != $row->impressions ? round(($row->impressions - $ref_row->impressions) / $ref_row->impressions * 100, 2) : 0;
						$ref_ctr = $ref_row->ctr != 0 && $ref_row->ctr != $row->ctr ? round(($row->ctr - $ref_row->ctr) / $ref_row->ctr * 100, 2) : 0;
					}
					$res['table_pages'][] = array(
						array('class' => 'wsko_table_col1', 'value' => WSKO_Class_Template::render_url_post_field_s($url, array(), true)),
						array('order' => $row->position, 'value' => '<span style="float:left;">'.round($row->position, 0).'</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->position, 0), $ref_position, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_position != 0 ? ($ref_position < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_position > 0)) ? '+' : '').($ref_row ? $ref_position : '-').' %</span>'),
						array('order' => $row->clicks, 'value' => '<span style="float:left;">'.$row->clicks.'</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->clicks, 0), $ref_clicks, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_clicks != 0 ? ($ref_clicks < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_clicks > 0)) ? '+' : '').($ref_row ? $ref_clicks : '-').' %</span>'),
						//array('order' => $row->impressions, 'value' => '<span style="float:left;">'.$row->impressions.'</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->impressions, 0), $ref_impressions, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_impressions != 0 ? ($ref_impressions < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_impressions > 0)) ? '+' : '').($ref_row ? $ref_impressions : '-').' %</span>'),
						//array('order' => $row->ctr, 'value' => '<span style="float:left;">'.round($row->ctr, 2).' %</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->ctr, 0), $ref_ctr, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_ctr != 0 ? ($ref_ctr < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_ctr > 0)) ? '+' : '').($ref_row ? $ref_ctr : '-').' %</span>')
					);
				}
			}
		}
		$kw_rows = WSKO_Class_Search::get_se_data($this->timespan_start, $this->timespan_end, 'query');
		if ($kw_rows)
		{
			$page_rows_ref = WSKO_Class_Search::get_se_data($time3, $this->timespan_start, 'page');
			$kw_rows_ref = WSKO_Class_Search::get_se_data($time3, $this->timespan_start, 'query');
			
			$count = 0;
			foreach ($kw_rows as $key => $row)
			{
				$kw = $row->keyval;
				
				$ref_row = false;
				if ($kw_rows_ref)
				{
					if (isset($kw_rows_ref[$row->keyval]))
					{
						$ref_row = $kw_rows_ref[$row->keyval];
					}
				}
				$count++;
				if ($count <= 8)
				{
					if ($ref_row)
					{
						$ref_clicks = $ref_row->clicks != 0 && $ref_row->clicks != $row->clicks ? round(($row->clicks - $ref_row->clicks) / $ref_row->clicks * 100, 2) : 0;
						$ref_position = $ref_row->position != 0 && round($ref_row->position) != round($row->position) ? -round((round($row->position) - round($ref_row->position)) / $ref_row->position * 100, 2) : 0;
						$ref_impressions = $ref_row->impressions != 0 && $ref_row->impressions != $row->impressions ? round(($row->impressions - $ref_row->impressions) / $ref_row->impressions * 100, 2) : 0;
						$ref_ctr = $ref_row->ctr != 0 && $ref_row->ctr != $row->ctr ? round(($row->ctr - $ref_row->ctr) / $ref_row->ctr * 100, 2) : 0;
					}
					$res['table_keywords'][] = array(
						array('class' => 'wsko_table_col1', 'value' => WSKO_Class_Template::render_keyword_field($kw, true)),
						array('order' => $row->position, 'value' => '<span style="float:left;">'.round($row->position, 0).'</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->position, 0), $ref_position, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_position != 0 ? ($ref_position < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_position > 0)) ? '+' : '').($ref_row ? $ref_position : '-').' %</span>'),
						array('order' => $row->clicks, 'value' => '<span style="float:left;">'.$row->clicks.'</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->clicks, 0), $ref_clicks, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_clicks != 0 ? ($ref_clicks < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_clicks > 0)) ? '+' : '').($ref_row ? $ref_clicks : '-').' %</span>'),
						//array('order' => $row->impressions, 'value' => '<span style="float:left;">'.$row->impressions.'</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->impressions, 0), $ref_impressions, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_impressions != 0 ? ($ref_impressions < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_impressions > 0)) ? '+' : '').($ref_row ? $ref_impressions : '-').' %</span>'),
						//array('order' => $row->ctr, 'value' => '<span style="float:left;">'.round($row->ctr, 2).' %</span>'.($ref_row?WSKO_Class_Template::render_progress_icon(round($ref_row->ctr, 0), $ref_ctr, array(), true):'')),// <span class="wsko_single_progress '.($ref_row && $ref_ctr != 0 ? ($ref_ctr < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_ctr > 0)) ? '+' : '').($ref_row ? $ref_ctr : '-').' %</span>'),
					);
				}
			}
			$res['kw_count'] = count($kw_rows);
			$res['kw_count_ref'] = $kw_rows_ref ? count($kw_rows_ref) : 0;
			$res['kw_count_ref_perc'] = $res['kw_count_ref'] > 0 ? round(($res['kw_count'] - $res['kw_count_ref']) / $res['kw_count_ref'] * 100, 2) : 0;
		}
		$clicks = array();
		$position = array();
		$impressions = array();
		$ctr = array();
		$total_clicks = 0;
		$total_clicks_ref = 0;
		$date_rows = WSKO_Class_Search::get_se_data($this->timespan_start, $this->timespan_end, 'date');
		if ($date_rows)
		{
			$date_rows_ref = WSKO_Class_Search::get_se_data($time3, $this->timespan_start, 'date');
			foreach ($date_rows as $row)
			{
				$total_clicks += $row->clicks;
				$date = date("M. d, Y", strtotime($row->keyval));
				$clicks[] = array($date, $row->clicks);
				$impressions[] = array($date, $row->impressions);
				$ctr[] = array($date, $row->ctr);
				$position[] = array($date, $row->position);
			}
			if ($date_rows_ref)
			{
				foreach ($date_rows_ref as $row)
				{
					$total_clicks_ref += $row->clicks;
				}
			}
		}
		$res['search_clicks'] = $clicks;
		$res['search_impressions'] = $impressions;
		$res['search_positions'] = $position;
		$res['search_ctrs'] = $ctr;
		$res['kw_clicks'] = $total_clicks;
		$res['kw_clicks_ref'] = $total_clicks_ref;
		$res['kw_clicks_ref_perc'] = $res['kw_clicks_ref'] && $res['kw_clicks'] != $res['kw_clicks_ref'] ? round(($res['kw_clicks'] - $res['kw_clicks_ref']) / $res['kw_clicks_ref'] * 100, 2) : 0;
		$keywords = WSKO_Class_Search::get_se_cache_history($this->timespan_start, $this->timespan_end, 'query');
		if ($keywords)
		{
			foreach ($keywords as $k => $kw)
			{
				$keywords[$k]->time = date("M. d, Y", strtotime($keywords[$k]->time));
			}
		}
		$pages = WSKO_Class_Search::get_se_cache_history($this->timespan_start, $this->timespan_end, 'page');
		if ($pages)
		{
			foreach ($pages as $k => $kw)
			{
				$pages[$k]->time = date("M. d, Y", strtotime($pages[$k]->time));
			}
		}
		$res['search_keywords'] = $keywords ? $keywords : array();
		$res['search_pages'] = $pages ? $pages : array();
		
		$table_worst_optimized = array();
		$onpage_score_history = array();
		$res['onpage_score'] = 0;
		$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
		if ($global_analysis)
		{
			if (isset($global_analysis['current_report']))
			{
				$res['onpage_score'] = $global_analysis['current_report']['onpage_score_avg'];
				if (isset($global_analysis['last_onpage_score']))
				{
					$res['last_onpage_score'] = $global_analysis['last_onpage_score'];
					$res['last_onpage_score_perc'] = $res['last_onpage_score'] && $res['last_onpage_score'] != $res['onpage_score'] ? round(($res['onpage_score'] - $res['last_onpage_score']) / $res['last_onpage_score'] * 100, 2) : 0;
				}
				foreach ($global_analysis['onpage_history'] as $k => $re)
				{
					$onpage_score_history[] = array(date('d.m.Y', $k), $re);
				}
				
				$worst_optimized = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => 0, 'count' => 10), false /*array(array('key' => 'onpage_score', 'comp' => 'ra', 'val' => array(0,50)))*/, array('key' => '0', 'dir' => 1), array('specific_keys' => array('onpage_score', 'url', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_url_post_field($r[2], array(), true); },'2' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array(), true); })), false);
				if ($worst_optimized)
				{
					foreach ($worst_optimized['data'] as $row)
					{
						$table_worst_optimized[] = array(array('value' => $row[0]),array('value' => $row[1]), array('value' => $row[2]));
					}
				}
			}
		}
		$res['onpage_score_history'] = $onpage_score_history;
		$res['onpage_worst_optimized'] = $table_worst_optimized;
		
		$res['total_social_follower'] = 0;
		$res['social_history_follower'] = array();
		$res['social_top_posts_fb'] = array();
		$res['social_top_posts_tw'] = array();
		$res['facebook_access'] = false;
		$res['twitter_access'] = false;
		if (!WSKO_Class_Social::check_facebook_access())
		{
			$res['facebook_access'] = true;
		}
		if (!WSKO_Class_Social::check_twitter_access())
		{
			$res['twitter_access'] = true;
		}
		if ($res['facebook_access'])
		{
			$fb_data = WSKO_Class_Social::get_facebook_data();
			
			if ($fb_data)
				$res['total_social_follower'] += $fb_data->follower;
			
			$facebook_history = WSKO_Class_Social::get_facebook_history($this->timespan_start, $this->timespan_end);
			if ($facebook_history)
			{
				foreach ($facebook_history as $h)
				{
					if ($res['twitter_access'])
						$res['social_history_follower'][$h->time] = array($h->time, 0, $h->follower);
					else
						$res['social_history_follower'][$h->time] = array($h->time, $h->follower);
				}
			}
			
			$current_posts = WSKO_Class_Social::get_current_facebook_posts();
			if ($current_posts)
			{
				usort($current_posts, function($a, $b){
					if ($a->clicks == $b->clicks)
						return 0;
					return $a->clicks > $b->clicks ? 1 : -1;
				});
				$current_posts = array_slice($current_posts, 0, 10);
				$cp_res = array();
				foreach ($current_posts as $k => $p)
				{
					$data = json_decode($p->data, true);
					$cp_res[$k] = array(
						array('value' => $data ? (isset($data['img']) && $data['img'] ? '<img src="'.$data['img'].'"/>' : '').(isset($data['content'])?$data['content']:'') : ''),
						array('value' => $p->follower),
						//array('value' => $p->engagement),
						//array('value' => $p->reach),
						array('value' => date('d.m.Y H:i', $p->created)),
					);
				}
				$res['social_top_posts_fb'] = $cp_res;
			}
		}
		if ($res['twitter_access'])
		{
			$tw_data = WSKO_Class_Social::get_twitter_data();
			
			if ($tw_data)
				$res['total_social_follower'] += $tw_data->follower;
			
			$twitter_history = WSKO_Class_Social::get_twitter_history($this->timespan_start, $this->timespan_end);
			if ($twitter_history)
			{
				foreach ($twitter_history as $h)
				{
					if (isset($res['social_history_follower'][$h->time]))
						$res['social_history_follower'][$h->time][2] = $h->follower;
					else if ($res['facebook_access'])
						$res['social_history_follower'][$h->time] = array($h->time, 0, $h->follower);
					else
						$res['social_history_follower'][$h->time] = array($h->time, $h->follower);
				}
			}
			
			$current_posts = WSKO_Class_Social::get_current_twitter_posts();
			if ($current_posts)
			{
				usort($current_posts, function($a, $b){
					if ($a->follower == $b->follower)
						return 0;
					return $a->follower > $b->follower ? 1 : -1;
				});
				$current_posts = array_slice($current_posts, 0, 10);
				$cp_res = array();
				foreach ($current_posts as $k => $p)
				{
					$data = json_decode($p->data, true);
					$cp_res[$k] = array(
						array('value' => $data ? (isset($data['content'])?$data['content']:'').(isset($data['urls']) && $data['urls'] ? '(Urls: '.implode(',',$data['urls']).')' : '') : ''),
						array('value' => $p->follower),
						//array('value' => $p->engagement),
						array('value' => date('d.m.Y H:i', $p->created)),
					);
				}
				$res['social_top_posts_tw'] = $cp_res;
			}
		}
		return $res;
	}
	
	public function get_breadcrump_title()
	{
		$current_user = wp_get_current_user();
		return "Welcome back, ".($current_user?$current_user->user_login:'not logged in person...');
	}
	
	//Singleton
	static $instance;
}
?>