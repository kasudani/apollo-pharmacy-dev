<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class WSKO_Controller_Settings extends WSKO_Controller
{
    //Options
    public  $icon = "cog" ;
    public  $link = "settings" ;
    public  $styles = array( 'admin/css/settings.css' ) ;
    public  $scripts = array( 'admin/js/settings.js' ) ;
    public  $ajax_actions = array(
        'reset_keyword_update',
        'request_api_access',
        'revoke_api_access',
        'update_api_cache',
        'delete_api_cache',
        'set_ga_an_profile',
        'set_facebook_profile',
        'set_twitter_profile',
        'clear_session_cache',
        'reset_configuration',
        'reset_cronjobs',
        'clear_error_reports',
        'backup_configuration',
        'delete_conf_backups',
        'load_configuration_backup',
        'delete_configuration_backup',
        'export_configuration_backup',
        'import_configuration_backup'
    ) ;
    public  $template_main = 'admin/templates/settings/frame-settings.php' ;
    public  $template_notifications = 'admin/templates/settings/notifications-settings.php' ;
    public function get_title()
    {
        return __( 'Settings', 'wsko' );
    }
    
    public function redirect()
    {
        $action = ( isset( $_GET['action'] ) ? $_GET['action'] : '' );
        if ( $action == 'recieve_facebook_token' ) {
            
            if ( isset( $_GET['access_token'] ) && $_GET['access_token'] ) {
                $long_token = WSKO_Class_Social::get_facebook_long_token( $_GET['access_token'] );
                if ( $long_token ) {
                    WSKO_Class_Social::set_social_token( 'facebook', $long_token, true );
                }
                //$this::call_redirect('apis');
                $this::call_redirect_raw( WSKO_Controller_Social::get_link( 'facebook' ) );
            }
        
        }
    }
    
    public function load_lazy_page_data()
    {
        if ( !$this->can_execute_action( false ) ) {
            return false;
        }
        if ( !current_user_can( 'manage_options' ) ) {
            return true;
        }
        $notif = "";
        $data = array();
        //$data['tab_status'] = WSKO_Class_Template::render_template('admin/templates/settings/view-status.php', array(), true);
        $data['tab_reports'] = WSKO_Class_Template::render_template( 'admin/templates/settings/view-reports.php', array(), true );
        $data['api_search'] = WSKO_Class_Template::render_template( 'admin/templates/settings/view-api-search.php', array(), true );
        $data['api_facebook'] = WSKO_Class_Template::render_template( 'admin/templates/settings/view-api-facebook.php', array(), true );
        $data['api_twitter'] = WSKO_Class_Template::render_template( 'admin/templates/settings/view-api-twitter.php', array(), true );
        return array(
            'success' => true,
            'data'    => $data,
            'notif'   => $notif,
        );
    }
    
    function action_reset_keyword_update()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        WSKO_Class_Crons::bind_keyword_update( true );
        return true;
    }
    
    function action_clear_session_cache()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $this->delete_cache();
        return true;
    }
    
    function action_update_api_cache()
    {
        set_time_limit( WSKO_LRS_TIMEOUT );
        if ( !$this->can_execute_action() ) {
            return false;
        }
        
        if ( isset( $_POST['api'] ) ) {
            switch ( $_POST['api'] ) {
                case 'ga_search':
                    WSKO_Class_Search::update_se_cache();
                    break;
                case 'ga_analytics':
                    break;
                case 'social_facebook':
                case 'social_twitter':
                    WSKO_Class_Social::update_social_cache();
                    break;
            }
            $this->delete_cache();
            return array(
                'success' => true,
            );
        }
    
    }
    
    function action_request_api_access()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $this->delete_cache();
        if ( isset( $_POST['code'] ) && $_POST['code'] && isset( $_POST['type'] ) && $_POST['type'] ) {
            switch ( $_POST['type'] ) {
                case 'ga_search':
                    $show_default_error = true;
                    $err_view = "";
                    $client = WSKO_Class_Search::get_ga_client_se();
                    
                    if ( $client ) {
                        $client->authenticate( $_POST['code'] );
                        $token = $client->getAccessToken();
                        
                        if ( $token ) {
                            WSKO_Class_Search::set_se_token( $token );
                            
                            if ( !WSKO_Class_Search::check_se_access( true ) ) {
                                if ( WSKO_Class_Core::is_configured() ) {
                                    WSKO_Class_Crons::bind_keyword_update( true );
                                }
                                WSKO_Class_Core::save_option( 'search_query_first_run', false );
                                //reset first report flag
                                return array(
                                    'success' => true,
                                    'msg'     => 'Login successfull',
                                );
                            } else {
                                $is_owner_error = false;
                                $has_property = false;
                                $domain = WSKO_Class_Helper::get_host_base();
                                $properties = WSKO_Class_Search::get_se_properties();
                                if ( $properties ) {
                                    foreach ( $properties as $prop ) {
                                        
                                        if ( $prop['siteUrl'] === $domain ) {
                                            $has_property = true;
                                            if ( $prop['permissionLevel'] !== 'siteOwner' ) {
                                                $is_owner_error = true;
                                            }
                                        }
                                    
                                    }
                                }
                                WSKO_Class_Search::set_se_token( false );
                                
                                if ( $has_property ) {
                                    
                                    if ( $is_owner_error ) {
                                        $err_view = WSKO_Class_Template::render_notification( 'error', array(
                                            'msg' => 'Your account has access to the domain, but it is not set as the owner. Please use your owner account, or change the required permissions.',
                                        ), true );
                                        $show_default_error = false;
                                    }
                                
                                } else {
                                    $err_view = WSKO_Class_Template::render_notification( 'error', array(
                                        'msg' => "We can't find the property \"" . $domain . "\". Please choose another Google account!",
                                    ), true );
                                }
                            
                            }
                            
                            //WSKO_Class_Search::update_se_cache(true);
                        } else {
                            $err_view = WSKO_Class_Template::render_notification( 'error', array(
                                'msg' => 'Your Access Token is invalid or has expired (you can only submit it once).',
                            ), true );
                        }
                        
                        if ( $show_default_error ) {
                            $err_view .= WSKO_Class_Template::render_notification( 'warning', array(
                                'msg'  => 'Your credentials are invalid. Check the following steps and contact us if the error persists.',
                                'list' => array(
                                'Are you checking with the right account? ' . WSKO_Class_Template::render_infoTooltip( "Depending on how many accounts you have yourself, or how often another person is using this browser, you may have the wrong account set right now. Allways check, that the current logged in Google Account is also the property owner.", 'info', true ),
                                'Is the Google account the verified owner of your website? See Googles <a href="https://support.google.com/webmasters/answer/34592" target="_blank">How To Guide</a> and after that check your <a href="https://www.google.com/webmasters/tools/home">Search Console</a> if the property "' . home_url() . '" is listed and verified.',
                                'Has the verfication run out? ' . WSKO_Class_Template::render_infoTooltip( "Just having the page under Properties in your Search Console is not enough. If the page is marked with 'unverified', then you need to redo the verification process of this property. The message displayed should tell you if something is wrong with your property status.", 'info', true ),
                                'Did you set the property for both <b>http</b>://' . $_SERVER['HTTP_HOST'] . ' <b>and https</b>://' . $_SERVER['HTTP_HOST'] . '? ' . WSKO_Class_Template::render_infoTooltip( "There is a difference between http and https. Please check if you have both sites verified according to the steps above.", 'info', true ),
                                'Are you able to access the required data? Try to view the <a href="https://support.google.com/webmasters/answer/6155685">Search Analysis</a> ' . WSKO_Class_Template::render_infoTooltip( "Google features a permission system for external accounts managing your sites. Thus the account you are trying to login with may lack the required permissions to access the required data. Click the link below and click on the 'Get Report' button in the new page. You should be able to open this view for your site. ", 'info', true )
                            ),
                            ), true );
                        }
                        return array(
                            'success'  => false,
                            'err_view' => $err_view,
                            'msg'      => 'Your credentials are invalid!',
                        );
                    }
                    
                    break;
                case 'ga_analytics':
                    break;
            }
        }
        return array(
            'success' => false,
        );
    }
    
    function action_revoke_api_access()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $this->delete_cache();
        if ( isset( $_POST['api'] ) && $_POST['api'] ) {
            switch ( $_POST['api'] ) {
                case 'ga_search':
                    $token = WSKO_Class_Search::get_se_token();
                    
                    if ( $token ) {
                        $client = WSKO_Class_Search::get_ga_client_se();
                        if ( $client ) {
                            $client->revokeToken( $token );
                        }
                        $token = false;
                        WSKO_Class_Search::set_se_token( $token );
                        WSKO_Class_Crons::unbind_keyword_update();
                        return array(
                            'success' => true,
                            'msg'     => 'Logout successfull',
                        );
                    }
                    
                    break;
                case 'ga_analytics':
                    break;
                case 'social_facebook':
                    $token = WSKO_Class_Social::get_social_token( 'facebook', true );
                    
                    if ( $token ) {
                        WSKO_Class_Social::revoke_facebook_access();
                        return array(
                            'success' => true,
                            'msg'     => 'Logout successfull',
                        );
                    }
                    
                    break;
            }
        }
    }
    
    function action_delete_api_cache()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $this->delete_cache();
        if ( isset( $_POST['api'] ) && $_POST['api'] ) {
            switch ( $_POST['api'] ) {
                case 'ga_search':
                    WSKO_Class_Cache::delete_cache_rows( array( 'search' ) );
                    WSKO_Class_Core::save_option( 'search_query_first_run', false );
                    WSKO_Class_Crons::bind_keyword_update( true );
                    return array(
                        'success' => true,
                        'msg'     => 'Cache cleaned',
                    );
                    break;
                case 'ga_analytics':
                    break;
                case 'social_facebook':
                    WSKO_Class_Cache::delete_cache_rows( array( 'search' ) );
                    return array(
                        'success' => true,
                        'msg'     => 'Cache cleaned',
                    );
                    break;
            }
        }
    }
    
    function action_set_ga_an_profile()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $this->delete_cache();
        
        if ( isset( $_POST['profile'] ) ) {
            $prof = sanitize_text_field( $_POST['profile'] );
            
            if ( $prof ) {
                WSKO_Class_Search::set_an_profile( $prof );
                return array(
                    'success' => true,
                    'msg'     => 'Profile set',
                );
            }
        
        }
    
    }
    
    function action_set_facebook_profile()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $this->delete_cache();
        
        if ( isset( $_POST['profile'] ) && $_POST['profile'] ) {
            $prof = sanitize_text_field( $_POST['profile'] );
            
            if ( $prof ) {
                $profiles = WSKO_Class_Social::get_facebook_profiles();
                
                if ( isset( $profiles[$prof] ) ) {
                    $prof_r = $profiles[$prof];
                    WSKO_Class_Social::set_social_token( 'facebook', $prof_r['access_token'] );
                    WSKO_Class_Social::set_facebook_profile( $prof );
                    
                    if ( WSKO_Class_Core::is_configured() ) {
                        WSKO_Class_Cache::delete_cache_rows( array( 'social' ) );
                        WSKO_Class_Social::update_social_cache();
                        //WSKO_Class_Crons::bind_social_cache(true);
                    }
                    
                    return array(
                        'success' => true,
                        'msg'     => 'Profile set',
                    );
                }
            
            }
        
        }
    
    }
    
    function action_set_twitter_profile()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $this->delete_cache();
        
        if ( isset( $_POST['profile'] ) ) {
            $prof = sanitize_text_field( $_POST['profile'] );
            WSKO_Class_Social::set_twitter_profile( $prof );
            
            if ( WSKO_Class_Core::is_configured() ) {
                WSKO_Class_Cache::delete_cache_rows( array( 'social' ) );
                WSKO_Class_Social::update_social_cache();
                //WSKO_Class_Crons::bind_social_cache(true);
            }
            
            return array(
                'success' => true,
                'msg'     => 'Profile set',
            );
        }
    
    }
    
    function action_reset_configuration()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        
        if ( isset( $_POST['delete_backups'] ) && $_POST['delete_backups'] && $_POST['delete_backups'] != 'false' ) {
            delete_option( 'wsko_backups' );
        } else {
            WSKO_Class_Core::backup_configuration();
        }
        
        
        if ( isset( $_POST['delete_cache'] ) && $_POST['delete_cache'] && $_POST['delete_cache'] != 'false' ) {
            WSKO_Class_Cache::delete_cache();
            WSKO_Class_Core::save_option( 'search_query_first_run', false );
        }
        
        WSKO_Class_Helper::clear_logs( true );
        WSKO_Class_Crons::deregister_cronjobs();
        $token = WSKO_Class_Search::get_se_token();
        
        if ( $token ) {
            $client = WSKO_Class_Search::get_ga_client_se();
            if ( $client ) {
                $client->revokeToken( $token );
            }
        }
        
        $token = WSKO_Class_Social::get_social_token( 'facebook', true );
        if ( $token ) {
            WSKO_Class_Social::revoke_facebook_access();
        }
        delete_option( 'wsko_options' );
        delete_option( 'wsko_init' );
        $this->delete_cache();
        WSKO_Class_Helper::refresh_permalinks();
        return array(
            'success'  => true,
            'redirect' => WSKO_Controller_Setup::get_link(),
            'msg'      => 'Plugin reset',
        );
    }
    
    function action_reset_cronjobs()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        WSKO_Class_Crons::deregister_cronjobs();
        WSKO_Class_Crons::register_cronjobs();
        return true;
    }
    
    function action_clear_error_reports()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        WSKO_Class_Helper::clear_logs( true );
        return true;
    }
    
    function action_backup_configuration()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        WSKO_Class_Core::backup_configuration();
        return true;
    }
    
    function action_delete_conf_backups()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        delete_option( 'wsko_backups' );
        return true;
    }
    
    function action_load_configuration_backup()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $global_report = WSKO_Class_Onpage::get_onpage_analysis();
        if ( !$global_report || !isset( $global_report['current_report'] ) || WSKO_Class_Search::get_se_token() && !WSKO_Class_Core::get_option( 'search_query_first_run' ) ) {
            return false;
        }
        //if wsko is fetching, dont load
        $key = ( isset( $_POST['key'] ) ? sanitize_text_field( $_POST['key'] ) : false );
        WSKO_Class_Core::load_configuration_backup( $key );
        return true;
    }
    
    function action_delete_configuration_backup()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $key = ( isset( $_POST['key'] ) ? sanitize_text_field( $_POST['key'] ) : false );
        WSKO_Class_Core::delete_configuration_backup( $key );
        return true;
    }
    
    function action_export_configuration_backup()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $key = ( isset( $_POST['key'] ) ? sanitize_text_field( $_POST['key'] ) : false );
        $backup = WSKO_Class_Core::get_configuration_backup( $key );
        
        if ( $backup ) {
            $dir = WP_CONTENT_DIR . '/bst/';
            $name = $key . '-' . (( $backup['auto'] ? 'auto' : '' )) . '-' . date( 'd.m.Y-H:i', $backup['time'] ) . '.txt';
            $key_store = $dir . $name;
            $file_link = content_url( '/bst/' . $name );
            
            if ( is_writable( $dir ) && is_readable( $dir ) ) {
                $res = file_put_contents( $key_store, json_encode( $backup ) );
                if ( $res !== false ) {
                    return array(
                        'success'  => true,
                        'redirect' => $file_link,
                    );
                }
            }
        
        }
    
    }
    
    function action_import_configuration_backup()
    {
        if ( !$this->can_execute_action() ) {
            return false;
        }
        $fileName = $_FILES['file']['name'];
        $fileType = $_FILES['file']['type'];
        $fileError = $_FILES['file']['error'];
        
        if ( $fileError == UPLOAD_ERR_OK ) {
            $fileContent = file_get_contents( $_FILES['file']['tmp_name'] );
            
            if ( $fileContent ) {
                $res = WSKO_Class_Core::import_configuration_backup( $fileContent );
                if ( $res ) {
                    return true;
                }
            }
        
        }
    
    }
    
    //Singleton
    static  $instance ;
}