<?php
if (!defined('ABSPATH')) exit;

class WSKO_Controller
{
	/*Options*/
	//string
	public $icon;
	//string
	public $link;
	//array
	public $subpages;
	//array
	public $subpage;
	//array
	public $scripts;
	//array
	public $styles;
	//array
	public $ajax_actions;
	//string
	public $template_main;
	//string
	public $template_notifications;
	//string
	public $template_footer;
	/*/*/
	
	/*Controls*/
	public static function init_actions()
	{
		$inst = static::get_instance();
		$inst->add_actions();
	}
	
	public function add_actions()
	{
		if (!has_action('wp_ajax_wsko_load_lazy_data'))
			add_action('wp_ajax_wsko_load_lazy_data', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_load_lazy_page'))
			add_action('wp_ajax_wsko_load_lazy_page', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_set_timespan'))
			add_action('wp_ajax_wsko_set_timespan', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_save_ajax_input'))
			add_action('wp_ajax_wsko_save_ajax_input', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_resolve_url'))
			add_action('wp_ajax_wsko_resolve_url', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_get_content_optimizer'))
			add_action('wp_ajax_wsko_get_content_optimizer', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_import_plugin'))
			add_action('wp_ajax_wsko_import_plugin', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_send_feedback'))
			add_action('wp_ajax_wsko_send_feedback', array($this, 'handle_action'));
		if (!has_action('wp_ajax_wsko_discard_notice'))
			add_action('wp_ajax_wsko_discard_notice', array($this, 'handle_action'));
		if ($this->ajax_actions)
		{
			foreach ($this->ajax_actions as $action)
			{
				if (!has_action('wp_ajax_wsko_'.$action))
					add_action('wp_ajax_wsko_'.$action, array($this, 'handle_action'));
			}
		}
	}
	
	public function handle_action()
	{
		$action = sanitize_text_field($_POST['action']);
		$action = substr($action, strlen('wsko_'), strlen($action));
		$func = 'action_'.$action;
		if (method_exists($this, $func))
		{
			$res = $this->$func();
			if ($res === true)
			{
				wp_send_json(array('success' => true));
				wp_die();
			}
			else if (!$res && !is_array($res))
			{
				wp_send_json(array('success' => false));
				wp_die();
			}
			else
			{
				wp_send_json($res);
				wp_die();
			}
		}
		wp_send_json(array('success' => false));
		wp_die();
	}
	
	public function can_execute_action($high_cap = true, $no_ajax = false)
	{
		if ($no_ajax)
		{
			if (WSKO_Class_Helper::check_user_permissions($high_cap))
			{
				return true;
			}
		}
		else 
		{
			$real_action = sanitize_text_field($_POST['action']);
			if (defined('DOING_AJAX') && DOING_AJAX && $real_action && substr($real_action, 0, strlen('wsko_')) == 'wsko_')
			{
				$action = substr($real_action, strlen('wsko_'), strlen($real_action));
				if ($action && (($this->ajax_actions && in_array($action, $this->ajax_actions)) || in_array($action, array('load_lazy_data', 'load_lazy_page', 'set_timespan', 'save_ajax_input', 'resolve_url', 'get_content_optimizer', 'import_plugin', 'send_feedback', 'discard_notice'))))
				{
					if (WSKO_Class_Helper::check_user_permissions($high_cap))
					{
						if (wp_verify_nonce($_POST['nonce'], $real_action))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	
	public function set_subpage($page)
	{
		$subpages = $this->get_subpages();
		if (!$page && $subpages && is_array($subpages))
		{
			reset($subpages);
			$page = key($subpages);
		}
		
		if ($page && $subpages && is_array($subpages) && isset($subpages[$page]))
		{
			$this->subpage = array('key' => $page)+$subpages[$page];
			return true;
		}
		return false;
	}
	public function get_current_subpage($return_obj = false)
	{
		$subpages = $this->get_subpages();
		if ($subpages && is_array($subpages))
		{
			$subpage = $this->subpage;
			if (!$subpage)
			{
				reset($subpages);
				$key = key($subpages);
				$subpage = $subpages[$key];
			}
			return $return_obj ? $subpage : $subpage['key'];
		}
		return false;
	}
	/*/*/
	
	/*View*/	
	public function redirect()
	{
		
	}
	
	public function get_title()
	{
		return 'No Title';//$this->title;
	}
	
	public function get_subpage_title($subpage)
	{
		return 'No Title';
	}
	
	public function get_breadcrump()
	{
		$breadcrump = $this->get_breadcrump_title();
		if ($breadcrump)
		{
			return $breadcrump;
		}
		else if ($this->subpage && is_array($this->subpage))
		{
			return $this->get_title() . " > " . $this->get_subpage_title($this->subpage['key']);
		}
		return "";
	}
	
	public function get_breadcrump_title()
	{
		return false;
	}
	
	public function enqueue_scripts()
	{
		global $wsko_plugin_url;
		
		$times = $this->get_cached_var('timespan', true);
		
		$script_data = array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'mapsApiKey' => WSKO_GOOGLE_MAPS_API_KEY,
			'controller' => $this->get_link(false, true),
			'subpage' => $this->subpage ? $this->subpage['key'] : '',
			'timespan_start' => $times['start'],
			'timespan_end' => $times['end']
		);
		
		wp_enqueue_script('wsko_admin_js', $wsko_plugin_url . 'admin/js/admin.js', array(), WSKO_VERSION);
		wp_localize_script('wsko_admin_js', 'wsko_admin_data', $script_data);
		
		wp_enqueue_script('bootstrap-select', $wsko_plugin_url . 'includes/bootstrap-select/js/bootstrap-select.min.js', array(), WSKO_VERSION);
	}
	
	public function enqueue_styles()
	{
		global $wsko_plugin_url;
		
		wp_enqueue_style('bootstrap-select', $wsko_plugin_url . 'includes/bootstrap-select/css/bootstrap-select.min.css', array(), WSKO_VERSION);
		
		wp_enqueue_style('wsko_old_admin_css', $wsko_plugin_url . 'admin/css/old-admin.css', array(), WSKO_VERSION);//TODO: remove
		wp_enqueue_style('wsko_admin_css', $wsko_plugin_url . 'admin/css/admin.css', array(), WSKO_VERSION);
		wp_enqueue_style('wsko_responsive_css', $wsko_plugin_url . 'admin/css/responsive.css', array(), WSKO_VERSION);
		wp_enqueue_style('wsko_normalize_css', $wsko_plugin_url . 'admin/css/normalize.css', array(), WSKO_VERSION);	
	}
	
	public function notifications($return = false)
	{
		if ($return)
			ob_start();
		/*if (isset($wsko_data['timeout_check']) && $wsko_data['timeout_check'] > (time() + WSKO_LRS_TIMEOUT))
		{
			$wsko_data = WSKO_Class_Core::get_data();
			WSKO_Class_Template::render_notification('warning', array('msg' => "The Timeout Check has not been finished (Started at ".date('d.m.Y h:i', $wsko_data['timeout_check'])."). Cache updates can be very time consuming and exceeding the time limit will result in corrupted datasets and errors when updating manually. Please contact your Administrator to raise the script execution time limit to ".round(WSKO_LRS_TIMEOUT/60)." minutes (".WSKO_LRS_TIMEOUT." seconds).", 'subnote' => 'Next Check at '.date('d.m.Y h:i', wp_next_scheduled('wsko_check_timeout'))));
		}*/
		WSKO_Class_Template::render_template('admin/templates/template-global-notifications.php', array());
		global $wsko_plugin_path;
		if ($this->template_notifications)
			include($wsko_plugin_path.$this->template_notifications);
		if ($return)
			return ob_get_clean();
	}
	
	public function view($return = false)
	{
		if ($return)
			ob_start();		
		global $wsko_plugin_path;
		if ($this->template_main)
			include($wsko_plugin_path.$this->template_main);
		if ($return)
			return ob_get_clean();
	}
	
	public function subpage_view($return = false)
	{
		if ($return)
			ob_start();
		global $wsko_plugin_path;
		$subpages = $this->get_subpages();
		if ($subpages && is_array($subpages))
		{
			$subpage = $this->get_current_subpage(true);
			if ($subpage)
				include($wsko_plugin_path.$subpage['template']);
		}
		if ($return)
			return ob_get_clean();
	}
	
	public function content_footer($return = false)
	{
		if ($return)
			ob_start();
		global $wsko_plugin_path;
		if ($this->template_footer)
			include($wsko_plugin_path.$this->template_footer);
		if ($return)
			return ob_get_clean();
	}
	
	public function get_scripts($return = false)
	{
		if ($return)
			ob_start();
		?><script type="text/javascript" >
		jQuery(document).ready(function($){
			wsko_admin_data.timespan_nonce = '<?=wp_create_nonce('wsko_set_timespan')?>';
			wsko_admin_data.resolve_nonce = '<?=wp_create_nonce('wsko_resolve_url')?>';
			wsko_admin_data.lazy_page_nonce = '<?=wp_create_nonce('wsko_load_lazy_page')?>';
			wsko_admin_data.lazy_data_nonce = '<?=wp_create_nonce('wsko_load_lazy_data')?>';
			wsko_admin_data.save_ajax_input_nonce = '<?=wp_create_nonce('wsko_save_ajax_input')?>';
			wsko_admin_data.feedback_nonce = '<?=wp_create_nonce('wsko_send_feedback')?>';
		});
		</script>
		<?php
		global $wsko_plugin_url;
		if ($this->scripts && is_array($this->scripts))
		{
			foreach ($this->scripts as $script)
			{
				?><script type="text/javascript" src="<?=$wsko_plugin_url.$script.'?ver='.WSKO_VERSION?>"></script><?php
			}
		}
		if ($this->styles && is_array($this->styles))
		{
			foreach ($this->styles as $style)
			{
				?><link href="<?=$wsko_plugin_url.$style.'?ver='.WSKO_VERSION?>" rel="stylesheet" type="text/css"></link><?php
			}
		}
		?><script type="text/javascript" >
		jQuery(document).ready(function($){
			jQuery.wsko_load_lazy_data($('#wsko_admin_view_content_wrapper'), 'page_data', false);
			<?php if ($this->uses_timespan) { ?> 
			$(document).on("wsko_event_timespan_set", function(e){
				jQuery.wsko_load_lazy_data($('#wsko_admin_view_content_wrapper'), 'page_data', true);
			});
			<?php } ?>
			jQuery.wsko_init_admin();
		});
		</script>
	
<?php
		if ($return)
			return ob_get_clean();
	}
	
	public function action_load_lazy_data()
	{
		//$this->delete_cache();
		$controller = WSKO_Class_Core::get_current_controller();
		if ($controller)
		{
			$action = isset($_POST['wsko_action']) ? sanitize_text_field($_POST['wsko_action']) : false;
			$lazy_data = isset($_POST['lazy_data']) ? $_POST['lazy_data'] : false;
			if ($action && $action != 'page')
			{
				$func = 'load_lazy_'.$action;
				if (method_exists($controller, $func))
				{
					return $controller->$func($lazy_data);
				}
			}
		}
		return false;
	}
	
	public function action_load_lazy_page()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$controller = WSKO_Class_Core::get_current_controller();
		if ($controller)
		{
			global $wsko_plugin_path;
			$timespan_widget = "";
			if ($controller->uses_timespan)
			{
				$timespan_widget = WSKO_Class_Template::render_timespan_widget(true);
			}
			return array(
				'success' => true,
				'title' => $controller->get_title(),
				'breadcrump' => $controller->get_breadcrump(),
				'notif' => $controller->notifications(true),
				'view' => $controller->view(true),
				'scripts' => $controller->get_scripts(true),
				'footer' => $controller->content_footer(true),
				'timespan' => $timespan_widget
			);
		}
	}
	
	public function action_set_timespan()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$controller = WSKO_Class_Core::get_current_controller();
		if ($controller->uses_timespan && isset($_POST['start_time']) && $_POST['start_time'] && isset($_POST['end_time']) && $_POST['end_time'])
		{
			$controller->set_timespan(intval($_POST['start_time']), intval($_POST['end_time']));
			return true;
		}
	}
	
	public function action_save_ajax_input()
	{
		if (!$this->can_execute_action())
			return false;
		
		if (isset($_POST['target']) && $_POST['target'])
		{
			$target = sanitize_text_field($_POST['target']);
			$val = (isset($_POST['val']) && $_POST['val']) ? sanitize_text_field($_POST['val']) : false;
			$setting = (isset($_POST['setting']) && $_POST['setting']) ? sanitize_text_field($_POST['setting']) : false;
			switch ($target)
			{
				case 'settings':
				if ($setting)
				{
					$set = $val == 'false' ? false : $val;
					WSKO_Class_Core::save_setting($setting, $set);
					switch($setting)
					{
						case 'hide_category_slug': 
							if (isset($_POST['alert']))
							{
								if ($_POST['alert'] && $_POST['alert'] != 'false')
									WSKO_Class_Core::save_setting('hide_category_slug_redirect', true);
								else
									WSKO_Class_Core::save_setting('hide_category_slug_redirect', false);
							}
						break;
					}
				}
				break;
			}
			return true;
		}
	}
	
	public function action_resolve_url()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		if (isset($_POST['urls']) && $_POST['urls'] && is_array($_POST['urls']))
		{
			$urls = array_map('sanitize_text_field', $_POST['urls']);
			$url_res = array();
			$failed_urls = array();
			foreach ($urls as $url)
			{
				$res = WSKO_Class_Helper::url_get_title($url);
				if (!$res->resolved)
				{
					$failed_urls[]= $url;
				}
				$url_res[$url] = $res;
			}
			$eff_urls = WSKO_Class_Helper::get_effective_urls($failed_urls);
			foreach ($eff_urls as $o_url => $url)
			{
				$url_res[$o_url] = WSKO_Class_Helper::url_get_title($url);
			}
			return array('success' => true, 'urls' => $url_res);
		}
	}
	
	public function action_send_feedback()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$type = isset($_POST['type']) ? intval($_POST['type']) : false;
		$msg = isset($_POST['message']) ? sanitize_text_field($_POST['message']) : false;
		$title = isset($_POST['title']) ? sanitize_text_field($_POST['title']) : false;
		$email = isset($_POST['email']) ? sanitize_email($_POST['email']) : false;
		$name = isset($_POST['name']) ? sanitize_text_field($_POST['name']) : false;
		$append_reports = isset($_POST['append_reports']) && $_POST['append_reports'] && $_POST['append_reports'] != 'false' ? true : false;
		
		if ($msg && $title)
		{
			$title = 'BST: '.$title;
			$msg .= '<br/>Von: ' . $name . ' (' . $email .')';
			$headers = array('From: '.($name ? $name : $email).' <'.$email.'>', 'Content-Type: text/html; charset=UTF-8');
			if ($type == 1)
			{
				if ($append_reports)
				{
					$rep = WSKO_Class_Helper::format_reports();
					$msg .= "<br/><br/>---------------------------Reports---------------------------<br/><br/>".$rep;
				}
				wp_mail(WSKO_SUPPORT_MAIL, $title, $msg, $headers);
			}
			else 
			{
				wp_mail(WSKO_FEEDBACK_MAIL, $title, $msg, $headers);
			}
			return true;
		}
	}
	
	public function action_discard_notice()
	{
		if (!$this->can_execute_action(false))
			return false;
		$notice = isset($_POST['notice']) ? sanitize_text_field($_POST['notice']) : false;
		if ($notice)
		{
			$wsko_data = WSKO_Class_Core::get_data();
			if (!isset($wsko_data['discard_notice']) || !$wsko_data['discard_notice'])
			{
				$wsko_data['discard_notice'] = array();
			}
			$wsko_data['discard_notice'][] = $notice;
			WSKO_Class_Core::save_data($wsko_data);
		}
		return true;
	}
	
	public function action_get_content_optimizer()
	{
		if (!$this->can_execute_action(false))
			return false;
		$post_id = isset($_POST['post']) ? intval($_POST['post']) : false;
		$widget = isset($_POST['widget']) && $_POST['widget'] && $_POST['widget'] != 'false' ? true : false;
		$preview = isset($_POST['preview']) && $_POST['preview'] && $_POST['preview'] != 'false' ? true : false;
		$open_tab = isset($_POST['open_tab']) ? $_POST['open_tab'] : false;
		
		if ($post_id)
		{
			$preview_data = false;
			if ($preview)
			{
				$preview_data = array(
					'post_title' => isset($_POST['post_title']) ? sanitize_post_field('post_title', $_POST['post_title'], $post_id) : '',
					'post_content' => isset($_POST['post_content']) ? sanitize_post_field('post_content', $_POST['post_content'], $post_id) : '',
					'post_slug' => isset($_POST['post_slug']) ? sanitize_title_with_dashes($_POST['post_slug']) : ''
				);
			}
			$view = WSKO_Controller_Optimizer::render($post_id, $widget, $preview_data, $open_tab);
			if ($view)
				return array('success' => true, 'view' => $view);
		}
	}
	
	public function action_import_plugin()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$plugin = isset($_POST['plugin']) ? sanitize_text_field($_POST['plugin']) : false;
		$options = isset($_POST['options']) ? $_POST['options'] : false;
		if ($plugin && $options && is_array($options))
		{
			WSKO_Class_Import::import_plugin($plugin, $options);
			return true;
		}
	}
	
	/*Timespan*/
	public $uses_timespan = false;
	public $uses_fixed_timespan = false;
	public $timespan_start = 0;
	public $timespan_end = 0;
	
	public function set_timespan($start_time, $end_time)
	{
		if ($this->uses_fixed_timespan)
			return;
		$this->delete_cache();
		$this->timespan_start = intval($start_time);
		$this->timespan_end = intval($end_time);
		
		//Update Cache
		$this->get_cached_var('timespan', true, array(), true);
	}
	
	/*Cache*/
	private static $cache = array();
	
	public function build_cache()
	{
		WSKO_Class_Cache::prepare_session_cache();
		
		//Global Cache
		if ($this->uses_timespan)
		{
			if ($this->uses_fixed_timespan)
			{
				$today = WSKO_Class_Helper::get_midnight();
				$this->timespan_start = $today - (60 * 60 * 24 * 30);
				$this->timespan_end = time();//$today - (60 * 60 * 24 * 3);
			}
			else
			{
				$times = $this->get_cached_var('timespan', true);//, array(), true);
				$this->timespan_start = $times['start'];
				$this->timespan_end = $times['end'];
			}
		}
	}
	
	public function get_cached_var($param_key, $parent = false, $args = array(), $recache = false, $expire_time = false)
	{
		if (!$expire_time)
			$expire_time = WSKO_Class_Helper::get_midnight(time() + (60*60*24)) - time();
		if ($param_key && $param_key != 'var')
		{
			$int = static::get_instance();
			if ($int)
			{
				$arg_k = "";
				if ($args) { foreach ($args as $k => $v) { $arg_k .= $k.'_'.$v; } }
				$key = ($parent?'global':get_class($int)).'_'.$param_key.'_'.$arg_k;
				if (!$recache && isset(self::$cache[$key]))
					return self::$cache[$key];
				$val = WSKO_Class_Cache::get_session_cache($key);
				if ($recache || !$val)
				{
					$func = 'get_cached_' . $param_key;
					$val = false;
					if (method_exists($int, $func))
					{
						$val = $int->$func($args);
					}
					if ($val)
						WSKO_Class_Cache::set_session_cache($key, $val, $expire_time);
				}
				if ($val)
					self::$cache[$key] = $val;
				return $val;
			}
		}
		return false;
	}
	
	public function get_cached_var_for_time($param_key, $args = array(), $recache = false, $expire_time = false)
	{
		if (!$expire_time)
			$expire_time = WSKO_Class_Helper::get_midnight(time() + (60*60*24)) - time();
		if ($param_key && $param_key != 'var')
		{
			$int = static::get_instance();
			if ($int && $int->uses_timespan && $int->timespan_start && $int->timespan_end)
			{
				$arg_k = "";
				if ($args) { foreach ($args as $k => $v) { $arg_k .= $k.'_'.$v; } }
				$key = get_class($int).'_'.$param_key.'_'.$int->timespan_start.'_'.$int->timespan_end.'_'.$arg_k;
				if (!$recache && isset(self::$cache[$key]))
					return self::$cache[$key];
				$val = WSKO_Class_Cache::get_session_cache($key);
				if ($recache || !$val)
				{
					$func = 'get_cached_' . $param_key;
					$val = false;
					if (method_exists($int, $func))
					{
						$val = $int->$func($args);
					}
					if ($val)
						WSKO_Class_Cache::set_session_cache($key, $val, $expire_time);
				}
				if ($val)
					self::$cache[$key] = $val;
				return $val;
			}
		}
		return false;
	}
	
	public function delete_cache($key = false)
	{
		WSKO_Class_Cache::clear_session_cache($key);
	}
	
	//Global Cache	
	public function get_cached_timespan($args)
	{
		$res = array();
		$today = WSKO_Class_Helper::get_midnight();
		if ($this->timespan_start)
			$res['start'] = $this->timespan_start;
		else
			$res['start'] = $today - (60 * 60 * 24 * 30);
		if ($this->timespan_end)
			$res['end'] = $this->timespan_end;
		else
			$res['end'] = $today - (60 * 60 * 24 * 3);
		return $res;
	}
	/*/*/
	
	/*Static Helpers*/	
	public static function get_title_s()
	{
		$int = static::get_instance();
		if ($int)
		{
			return $int->get_title();
		}
		return "";
	}
	public static function get_link($subpage = false, $simple = false, $prefix = true)
	{
		$int = static::get_instance();
		if ($int && $int->link)
		{
			$subpages = $int->get_subpages();
			if ($subpage && $subpages && is_array($subpages) && isset($subpages[$subpage]))
			{
				return $simple ? ($prefix?'wsko_':'').$subpage : admin_url('admin.php?page=wsko_'.$int->link.'&subpage='.$subpage);
			}
			return $simple ? ($prefix?'wsko_':'').$int->link : admin_url('admin.php?page=wsko_'.$int->link);
		}
		return $simple ? "" : admin_url('admin.php');
	}
	
	public static function get_subpages()
	{
		$int = static::get_instance();
		if ($int->link)
		{
			if ($int->subpages && is_array($int->subpages))
			{
				$subpages = $int->subpages;
				if (!wsko_fs()->can_use_premium_code())
				{
					switch ($int->link)
					{
						//case 'onpage': unset($subpages['links']); break;
						case 'search': unset($subpages['competitors']); unset($subpages['keyword_research']); break;
					}
				}
				return $subpages;
			}
			return false;
		}
	}
	
	public static function call_redirect($subpage = false)
	{
		wp_redirect(static::get_link($subpage));
		exit;
	}
	public static function call_redirect_raw($url)
	{
		wp_redirect($url);
		exit;
	}
	/*/*/
	
	/*Singleton*/
	public static $instance;
	
	public static function get_instance()
	{
		if (!isset(static::$instance))
		{
			static::$instance = new static();
			static::$instance->build_cache();
		}
		return static::$instance;
	}
	/*/*/
}
/*Template
class WSKO_Controller_Temp extends WSKO_Controller
{
	static $instance;
	public function get_title()
	{
		return "WSKO - Temp";
	}
	
	public function view()
	{
		?>Content<?php
	}
}
*/
?>