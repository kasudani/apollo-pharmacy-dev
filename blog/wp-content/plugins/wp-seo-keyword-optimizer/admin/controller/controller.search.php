<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class WSKO_Controller_Search extends WSKO_Controller
{
    //Options
    public  $icon = "search" ;
    public  $link = "search" ;
    public  $uses_timespan = true ;
    public  $scripts = array( 'admin/js/search.js' ) ;
    public  $styles = array( 'admin/css/search.css' ) ;
    public  $ajax_actions = array(
        'do_keyword_research',
        'get_keyword_research',
        'table_search',
        'add_monitoring_keyword',
        'remove_monitoring_keyword'
    ) ;
    public  $template_main = 'admin/templates/search/frame-search.php' ;
    public  $template_notifications = 'admin/templates/search/notifications-search.php' ;
    public  $template_footer = 'admin/templates/search/footer-search.php' ;
    public  $subpages = array(
        'overview'         => array(
        'template' => 'admin/templates/search/page-overview.php',
    ),
        'keywords'         => array(
        'template' => 'admin/templates/search/page-keywords.php',
    ),
        'pages'            => array(
        'template' => 'admin/templates/search/page-pages.php',
    ),
        'competitors'      => array(
        'template' => 'admin/templates/search/page-competitors.php',
    ),
        'keyword_research' => array(
        'template' => 'admin/templates/search/page-research.php',
    ),
        'monitoring'       => array(
        'template' => 'admin/templates/search/page-monitoring.php',
    ),
    ) ;
    public function get_title()
    {
        return __( 'Search', 'wsko' );
    }
    
    public function get_subpage_title( $subpage )
    {
        switch ( $subpage ) {
            case 'overview':
                return __( 'Overview', 'wsko' );
            case 'keywords':
                return __( 'Keywords', 'wsko' );
            case 'pages':
                return __( 'Pages', 'wsko' );
            case 'competitors':
                return __( 'Competitors', 'wsko' );
            case 'keyword_research':
                return __( 'Keyword Research', 'wsko' );
            case 'monitoring':
                return __( 'Monitoring', 'wsko' );
        }
        return '';
    }
    
    public function load_lazy_page_data( $lazy_data )
    {
        if ( !$this->can_execute_action( false ) ) {
            return false;
        }
        $notif = "";
        $data = array();
        $page = $this->get_current_subpage();
        
        if ( $page ) {
            global  $wpdb ;
            $is_fetching = !WSKO_Class_Core::get_option( 'search_query_first_run' );
            $invalid = ( $is_fetching ? true : WSKO_Class_Search::check_se_access() );
            $client = WSKO_Class_Search::get_ga_client_se();
            $token = WSKO_Class_Search::get_se_token();
            if ( !$token ) {
                $is_fetching = false;
            }
            switch ( $page ) {
                case 'overview':
                    
                    if ( !$invalid ) {
                        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
                        $snippet_data = $this->get_cached_var_for_time( 'snippet_data' );
                        $graph_data = $this->get_cached_var_for_time( 'overview_graph_data' );
                        $table_data = $this->get_cached_var_for_time( 'table_data' );
                        $nextCron = wp_next_scheduled( 'wsko_cache_keywords' );
                        $is_admin = current_user_can( 'manage_options' );
                        //Snippets
                        $data['total_keywords'] = '<span class="wsko-label">' . (( (( $snippet_data['kw_count'] == WSKO_SEARCH_ROWS_LIMIT ? '<i data-toggle="tooltip" title="Google Search API only allows ' . WSKO_SEARCH_ROWS_LIMIT . ' rows per query. This limit will be removed in a further version.">></i>' : '' )) . $snippet_data['kw_count'] ? $snippet_data['kw_count'] : 0 )) . '</span>';
                        $data['total_keywords'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['kw_count_ref'],
                            $snippet_data['kw_count_ref_perc'],
                            array(),
                            true
                        );
                        $data['total_keyword_dist'] = '<span class="wsko-label">' . (( $snippet_data['kw_dist_count'] ? $snippet_data['kw_dist_count'] : 0 )) . '</span>';
                        $data['total_keyword_dist'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['kw_dist_count_ref'],
                            $snippet_data['kw_dist_count_ref_perc'],
                            array(),
                            true
                        );
                        $data['total_clicks'] = '<span class="wsko-label">' . (( $snippet_data['kw_clicks'] ? $snippet_data['kw_clicks'] : 0 )) . '</span>';
                        $data['total_clicks'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['kw_clicks_ref'],
                            $snippet_data['kw_clicks_ref_perc'],
                            array(),
                            true
                        );
                        $data['total_impressions'] = '<span class="wsko-label">' . (( $snippet_data['kw_imp'] ? $snippet_data['kw_imp'] : 0 )) . '</span>';
                        $data['total_impressions'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['kw_imp_ref'],
                            $snippet_data['kw_imp_ref_perc'],
                            array(),
                            true
                        );
                        //Charts
                        
                        if ( $timespan_data['date_rows'] && $snippet_data['kw_dist'] ) {
                            $data['chart_history_clicks'] = WSKO_Class_Template::render_chart(
                                'area',
                                array( 'Date', 'Clicks', 'Clicks Ref' ),
                                $graph_data['history_clicks'],
                                array(),
                                true
                            );
                            $data['chart_history_position'] = WSKO_Class_Template::render_chart(
                                'area',
                                array( 'Date', 'Position', 'Position Ref' ),
                                $graph_data['history_position'],
                                array(),
                                true
                            );
                            $data['chart_history_impressions'] = WSKO_Class_Template::render_chart(
                                'area',
                                array( 'Date', 'Impressions', 'Impressions Ref' ),
                                $graph_data['history_impressions'],
                                array(),
                                true
                            );
                            $data['chart_history_ctr'] = WSKO_Class_Template::render_chart(
                                'column',
                                array( 'Position', 'Keywords' ),
                                $graph_data['history_ctr'],
                                array(
                                'axisTitle' => 'SERPs',
                            ),
                                true
                            );
                            $data['chart_history_devices'] = WSKO_Class_Template::render_chart(
                                'pie',
                                array( 'Device', 'Clicks' ),
                                $graph_data['history_devices'],
                                array(
                                'legend_pos'   => 'left',
                                'chart_width'  => 55,
                                'legend_align' => 'start',
                            ),
                                true
                            );
                            $data['chart_history_countries'] = WSKO_Class_Template::render_chart(
                                'world',
                                array( 'Country', 'Clicks' ),
                                $graph_data['history_countries'],
                                array(),
                                true
                            );
                            $data['chart_history_keywords'] = WSKO_Class_Template::render_chart(
                                'area',
                                array( 'Date', 'Keywords', 'Keywords Ref' ),
                                $graph_data['history_keywords'],
                                array(),
                                true
                            );
                            $data['chart_history_pages'] = WSKO_Class_Template::render_chart(
                                'area',
                                array( 'Date', 'Pages', 'Pages Ref' ),
                                $graph_data['history_pages'],
                                array(),
                                true
                            );
                            $data['chart_history_rankings'] = WSKO_Class_Template::render_chart(
                                'line',
                                array(
                                'Date',
                                'SERP 1',
                                'SERP 2',
                                'SERP 3',
                                'SERP 4',
                                'SERP 5',
                                'SERP 6',
                                'SERP 7',
                                'SERP 8',
                                'SERP 9',
                                'SERP 10'
                            ),
                                $graph_data['history_rankings'],
                                array(
                                'legend_pos'   => 'top',
                                'pixel_height' => 400,
                                'suffix'       => 'Keywords',
                            ),
                                true
                            );
                        } else {
                            
                            if ( $client ) {
                                $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-cache.php', array(
                                    'api' => 'ga_search',
                                ), true );
                            } else {
                                $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-data.php', array(), true );
                            }
                            
                            $data['chart_history_clicks'] = $tmp_fail;
                            $data['chart_history_position'] = $tmp_fail;
                            $data['chart_history_impressions'] = $tmp_fail;
                            $data['chart_history_ctr'] = $tmp_fail;
                            $data['chart_history_devices'] = $tmp_fail;
                            $data['chart_history_countries'] = $tmp_fail;
                            $data['chart_history_keywords'] = $tmp_fail;
                            $data['chart_history_pages'] = $tmp_fail;
                            $data['chart_history_rankings'] = $tmp_fail;
                        }
                        
                        //Tables
                        foreach ( $table_data['top_keywords'] as $k => $kw ) {
                            $table_data['top_keywords'][$k][0]['value'] = WSKO_Class_Template::render_keyword_field( $kw[0]['value'], true );
                        }
                        foreach ( $table_data['top_pages'] as $k => $p ) {
                            $table_data['top_pages'][$k][0]['value'] = WSKO_Class_Template::render_url_post_field_s( $p[0]['value'], array(
                                'with_co'  => true,
                                'open_tab' => 'keywords',
                            ), true );
                        }
                        $data['table_top_keywords'] = WSKO_Class_Template::render_table(
                            array(
                            'Keyword',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            $table_data['top_keywords'],
                            array(
                            'no_pages' => true,
                            'order'    => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                        ),
                            true
                        );
                        $data['table_top_pages'] = WSKO_Class_Template::render_table(
                            array(
                            'URL',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            $table_data['top_pages'],
                            array(
                            'no_pages' => true,
                            'order'    => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                        ),
                            true
                        );
                        //Notifications
                        $today = WSKO_Class_Helper::get_midnight();
                        $start = $today - 60 * 60 * 24 * 3;
                        $end = $today - 60 * 60 * 24 * 90;
                        $num_cache_rows = WSKO_Class_Search::get_se_cache_history_count( $end, $start );
                        
                        if ( $num_cache_rows < 87 ) {
                            $missing = 87 - $num_cache_rows;
                            $missing_time = date( 'i:s', $missing * 2.35 );
                            //avg process time (1.35) + counter-query-limit-sleep between calls(1.0)
                            $notif_text = "There are " . $missing . " missing data sets in your last 90 days cache. You can wait for the next cronjob (" . (( $nextCron ? date( 'd.m.Y H:i', $nextCron ) : 'disabled' )) . ") or initiate it manually (this will take some time and may block your session for the time)<br/>" . WSKO_Class_Template::render_recache_api_button( 'ga_search', true );
                            $notif .= WSKO_Class_Template::render_notification( 'error', array(
                                'msg'     => $notif_text,
                                'subnote' => ( $is_admin ? "Please update your data by clicking on 'Update Manually'. Google only stores data from the last 90 days - a dataset older than that can't be updated anymore." : false ),
                            ), true );
                        }
                        
                        
                        if ( $snippet_data['no_data'] || $snippet_data['no_data_ref'] ) {
                            $notif_text = 'Your are missing some data sets for the selected timerange. If you have just launched the site, this is expected behaviour as you have no clicks to show yet. Have a look at your <a href="https://support.google.com/webmasters/answer/6155685">Search Console</a> and check if it has no datasets for that day too.';
                            $notif .= WSKO_Class_Template::render_notification( 'warning', array(
                                'msg'     => $notif_text,
                                'subnote' => (( $snippet_data['no_data'] ? $snippet_data['no_data'] . ' empty dataset(s) in the selected timerange' : '' )) . (( $snippet_data['no_data'] && $snippet_data['no_data_ref'] ? ' and ' : ' ' )) . (( $snippet_data['no_data_ref'] ? $snippet_data['no_data_ref'] . ' empty dataset(s) in the mirrored timerange (for the progress calculation)' : '' )),
                            ), true );
                        }
                    
                    } else {
                        $data['total_keywords'] = '<span class="wsko-label">-</span>';
                        $data['total_keyword_dist'] = '<span class="wsko-label">-</span>';
                        $data['total_clicks'] = '<span class="wsko-label">-</span>';
                        $data['total_impressions'] = '<span class="wsko-label">-</span>';
                        
                        if ( $is_fetching ) {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-cache-fetching.php', array(), true );
                        } else {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-api.php', array(), true );
                        }
                        
                        $data['chart_history_clicks'] = $tmp_fail;
                        $data['chart_history_position'] = $tmp_fail;
                        $data['chart_history_impressions'] = $tmp_fail;
                        $data['chart_history_ctr'] = $tmp_fail;
                        $data['chart_history_devices'] = $tmp_fail;
                        $data['chart_history_countries'] = $tmp_fail;
                        $data['chart_history_keywords'] = $tmp_fail;
                        $data['chart_history_pages'] = $tmp_fail;
                        $data['chart_history_rankings'] = $tmp_fail;
                        $data['table_top_keywords'] = $tmp_fail;
                        $data['table_top_pages'] = $tmp_fail;
                    }
                    
                    break;
                case 'keywords':
                    
                    if ( !$invalid ) {
                        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
                        $snippet_data = $this->get_cached_var_for_time( 'snippet_data' );
                        $keyword_overview_data = $this->get_cached_var_for_time( 'keyword_overview_data' );
                        $data['total_keywords'] = '<span class="wsko-label">' . (( (( $snippet_data['kw_count'] == WSKO_SEARCH_ROWS_LIMIT ? '<i data-toggle="tooltip" title="Google Search API only allows ' . WSKO_SEARCH_ROWS_LIMIT . ' rows per query. This limit will be removed in a further version.">></i>' : '' )) . $snippet_data['kw_count'] ? $snippet_data['kw_count'] : 0 )) . '</span>';
                        $data['total_keywords'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['kw_count_ref'],
                            $snippet_data['kw_count_ref_perc'],
                            array(),
                            true
                        );
                        $data['total_keyword_dist'] = '<span class="wsko-label">' . (( $snippet_data['kw_dist_count'] ? $snippet_data['kw_dist_count'] : 0 )) . '</span>';
                        $data['total_keyword_dist'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['kw_dist_count_ref'],
                            $snippet_data['kw_dist_count_ref_perc'],
                            array(),
                            true
                        );
                        $data['total_new_keywords'] = '<span class="wsko-label">' . count( $keyword_overview_data['new_keywords'] ) . '</span>';
                        $data['total_lost_keywords'] = '<span class="wsko-label">' . count( $keyword_overview_data['lost_keywords'] ) . '</span>';
                        $data['table_keywords'] = WSKO_Class_Template::render_table(
                            array(
                            'Keyword',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            array(),
                            array(
                            'order'  => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                            'ajax'   => array(
                            'action' => 'wsko_table_search',
                            'arg'    => '1',
                        ),
                            'filter' => array(
                            '0' => array(
                            'title' => 'Keywords',
                            'type'  => 'text',
                        ),
                            '1' => array(
                            'title' => 'Position',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_position'],
                        ),
                            '2' => array(
                            'title' => 'Clicks',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_clicks'],
                        ),
                            '3' => array(
                            'title' => 'Impressions',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_imp'],
                        ),
                        ),
                        ),
                            true
                        );
                        $data['table_new_keywords'] = WSKO_Class_Template::render_table(
                            array(
                            'Keyword',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            array(),
                            array(
                            'order'  => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                            'ajax'   => array(
                            'action' => 'wsko_table_search',
                            'arg'    => '2',
                        ),
                            'filter' => array(
                            '0' => array(
                            'title' => 'Keywords',
                            'type'  => 'text',
                        ),
                            '1' => array(
                            'title' => 'Position',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_position'],
                        ),
                            '2' => array(
                            'title' => 'Clicks',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_clicks'],
                        ),
                            '3' => array(
                            'title' => 'Impressions',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_imp'],
                        ),
                        ),
                        ),
                            true
                        );
                        $data['table_lost_keywords'] = WSKO_Class_Template::render_table(
                            array(
                            'Keyword',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            array(),
                            array(
                            'order'  => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                            'ajax'   => array(
                            'action' => 'wsko_table_search',
                            'arg'    => '3',
                        ),
                            'filter' => array(
                            '0' => array(
                            'title' => 'Keywords',
                            'type'  => 'text',
                        ),
                            '1' => array(
                            'title' => 'Position',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_position'],
                        ),
                            '2' => array(
                            'title' => 'Clicks',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_clicks'],
                        ),
                            '3' => array(
                            'title' => 'Impressions',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_kw_imp'],
                        ),
                        ),
                        ),
                            true
                        );
                    } else {
                        $data['total_keywords'] = '<span class="wsko-label">-</span>';
                        $data['total_keyword_dist'] = '<span class="wsko-label">-</span>';
                        $data['total_new_keywords'] = '<span class="wsko-label">-</span>';
                        $data['total_lost_keywords'] = '<span class="wsko-label">-</span>';
                        
                        if ( $is_fetching ) {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-cache-fetching.php', array(), true );
                        } else {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-api.php', array(), true );
                        }
                        
                        $data['table_keywords'] = $tmp_fail;
                        $data['table_new_keywords'] = $tmp_fail;
                        $data['table_lost_keywords'] = $tmp_fail;
                    }
                    
                    break;
                case 'pages':
                    
                    if ( !$invalid ) {
                        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
                        $snippet_data = $this->get_cached_var_for_time( 'snippet_data' );
                        $page_overview_data = $this->get_cached_var_for_time( 'page_overview_data' );
                        $data['total_pages'] = '<span class="wsko-label">' . (( (( $snippet_data['page_count'] == WSKO_SEARCH_ROW_LIMIT ? '<i data-toggle="tooltip" title="Google Search API only allows ' . WSKO_SEARCH_ROW_LIMIT . ' rows per query. This limit will be removed in a further version.">></i>' : '' )) . $snippet_data['page_count'] ? $snippet_data['page_count'] : 0 )) . '</span>';
                        $data['total_pages'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['page_count_ref'],
                            $snippet_data['page_count_ref_perc'],
                            array(),
                            true
                        );
                        $data['total_page_dist'] = '<span class="wsko-label">' . (( $snippet_data['page_dist_count'] ? $snippet_data['page_dist_count'] : 0 )) . '</span>';
                        $data['total_page_dist'] .= WSKO_Class_Template::render_progress_icon(
                            $snippet_data['page_dist_count_ref'],
                            $snippet_data['page_dist_count_ref_perc'],
                            array(),
                            true
                        );
                        $data['total_new_pages'] = '<span class="wsko-label">' . count( $page_overview_data['new_pages'] ) . '</span>';
                        $data['total_lost_pages'] = '<span class="wsko-label">' . count( $page_overview_data['lost_pages'] ) . '</span>';
                        $data['table_pages'] = WSKO_Class_Template::render_table(
                            array(
                            'URL',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            array(),
                            array(
                            'order'              => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                            'change_search_text' => 'Search (URL):',
                            'ajax'               => array(
                            'action' => 'wsko_table_search',
                            'arg'    => '4',
                        ),
                            'filter'             => array(
                            '0' => array(
                            'title' => 'URL',
                            'type'  => 'text',
                        ),
                            '1' => array(
                            'title' => 'Position',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_position'],
                        ),
                            '2' => array(
                            'title' => 'Clicks',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_clicks'],
                        ),
                            '3' => array(
                            'title' => 'Impressions',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_imp'],
                        ),
                        ),
                        ),
                            true
                        );
                        $data['table_new_pages'] = WSKO_Class_Template::render_table(
                            array(
                            'URL',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            array(),
                            array(
                            'order'              => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                            'change_search_text' => 'Search (URL):',
                            'ajax'               => array(
                            'action' => 'wsko_table_search',
                            'arg'    => '5',
                        ),
                            'filter'             => array(
                            '0' => array(
                            'title' => 'URL',
                            'type'  => 'text',
                        ),
                            '1' => array(
                            'title' => 'Position',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_position'],
                        ),
                            '2' => array(
                            'title' => 'Clicks',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_clicks'],
                        ),
                            '3' => array(
                            'title' => 'Impressions',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_imp'],
                        ),
                        ),
                        ),
                            true
                        );
                        $data['table_lost_pages'] = WSKO_Class_Template::render_table(
                            array(
                            'URL',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR'
                        ),
                            array(),
                            array(
                            'order'              => array(
                            'col' => 2,
                            'dir' => 'desc',
                        ),
                            'change_search_text' => 'Search (URL):',
                            'ajax'               => array(
                            'action' => 'wsko_table_search',
                            'arg'    => '6',
                        ),
                            'filter'             => array(
                            '0' => array(
                            'title' => 'URL',
                            'type'  => 'text',
                        ),
                            '1' => array(
                            'title' => 'Position',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_position'],
                        ),
                            '2' => array(
                            'title' => 'Clicks',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_clicks'],
                        ),
                            '3' => array(
                            'title' => 'Impressions',
                            'type'  => 'number_range',
                            'min'   => 0,
                            'max'   => $snippet_data['max_page_imp'],
                        ),
                        ),
                        ),
                            true
                        );
                    } else {
                        $data['total_pages'] = '<span class="wsko-label">-</span>';
                        $data['total_page_dist'] = '<span class="wsko-label">-</span>';
                        $data['total_new_pages'] = '<span class="wsko-label">-</span>';
                        $data['total_lost_pages'] = '<span class="wsko-label">-</span>';
                        
                        if ( $is_fetching ) {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-cache-fetching.php', array(), true );
                        } else {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-api.php', array(), true );
                        }
                        
                        $data['table_pages'] = $tmp_fail;
                        $data['table_new_pages'] = $tmp_fail;
                        $data['table_lost_pages'] = $tmp_fail;
                    }
                    
                    break;
                case 'competitors':
                    break;
                case 'monitoring':
                    $monitored_keywords = WSKO_Class_Search::get_monitored_keywords();
                    
                    if ( $monitored_keywords && !empty($monitored_keywords) ) {
                        $monitoring_data = $this->get_cached_var_for_time( 'monitoring_data' );
                        $data['total_keywords'] = $monitoring_data['total_keywords'];
                        $data['total_clicks'] = $monitoring_data['total_clicks'];
                        $data['total_impressions'] = $monitoring_data['total_impressions'];
                        $data['avg_position'] = $monitoring_data['avg_position'];
                        $data['chart_histories'] = WSKO_Class_Template::render_template( 'admin/templates/search/view-monitoring-histories.php', array(
                            'clicks'      => $monitoring_data['history_clicks'],
                            'impressions' => $monitoring_data['history_impressions'],
                            'position'    => $monitoring_data['history_position'],
                            'ctr'         => $monitoring_data['history_ctr'],
                        ), true );
                        $data['table_monitored_keywords'] = WSKO_Class_Template::render_table(
                            array(
                            'Keyword',
                            'Position',
                            'Clicks',
                            'Impressions',
                            'CTR',
                            ''
                        ),
                            $monitoring_data['table_monitored_keywords'],
                            array(),
                            true
                        );
                    } else {
                        $data['total_keywords'] = "-";
                        $data['total_clicks'] = "-";
                        $data['total_impressions'] = "-";
                        $data['avg_position'] = "-";
                        
                        if ( $is_fetching ) {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-cache-fetching.php', array(), true );
                        } else {
                            $tmp_fail = WSKO_Class_Template::render_template( 'admin/templates/template-no-data.php', array(), true );
                        }
                        
                        $data['chart_histories'] = $tmp_fail;
                        $data['table_monitored_keywords'] = $tmp_fail;
                    }
                    
                    break;
            }
            return array(
                'success' => true,
                'data'    => $data,
                'notif'   => $notif,
            );
        }
    
    }
    
    public function action_do_keyword_research()
    {
        if ( !$this->can_execute_action( false ) ) {
            return false;
        }
    }
    
    public function action_get_keyword_research()
    {
        if ( !$this->can_execute_action( false ) ) {
            return false;
        }
    }
    
    public function action_table_search()
    {
        if ( !$this->can_execute_action( false ) ) {
            return false;
        }
        $data = array();
        $arg = ( isset( $_POST['arg'] ) ? sanitize_text_field( $_POST['arg'] ) : false );
        $order = ( isset( $_POST['order'] ) ? $_POST['order'] : false );
        $offset = ( isset( $_POST['start'] ) ? intval( $_POST['start'] ) : 0 );
        $count = ( isset( $_POST['length'] ) ? intval( $_POST['length'] ) : 10 );
        $search = ( isset( $_POST['search']['value'] ) ? $_POST['search']['value'] : false );
        $custom_filter = ( isset( $_POST['custom_filter'] ) ? $_POST['custom_filter'] : false );
        if ( $offset < 0 ) {
            $offset = 0;
        }
        if ( $count < 1 ) {
            $count = 1;
        }
        
        if ( $order && $order[0] ) {
            
            if ( $order[0]['dir'] == "asc" ) {
                $orderdir = 1;
            } else {
                $orderdir = 0;
            }
            
            $order = $order[0]['column'];
        } else {
            $order = 0;
            $orderdir = 1;
        }
        
        switch ( $arg ) {
            case '1':
                $table_data = $this->get_cached_var_for_time( 'table_data' );
                $data = WSKO_Class_Helper::prepare_data_table(
                    $table_data['table_keywords'],
                    array(
                    'offset' => $offset,
                    'count'  => $count,
                ),
                    $custom_filter,
                    array(
                    'key' => $order,
                    'dir' => $orderdir,
                ),
                    array(
                    'search' => $search,
                    'format' => array(
                    '0' => 'keyword',
                ),
                ),
                    true
                );
                break;
            case '2':
                $keyword_overview_data = $this->get_cached_var_for_time( 'keyword_overview_data' );
                $data = WSKO_Class_Helper::prepare_data_table(
                    $keyword_overview_data['new_keywords'],
                    array(
                    'offset' => $offset,
                    'count'  => $count,
                ),
                    $custom_filter,
                    array(
                    'key' => $order,
                    'dir' => $orderdir,
                ),
                    array(
                    'search' => $search,
                    'format' => array(
                    '0' => 'keyword',
                ),
                ),
                    true
                );
                break;
            case '3':
                $keyword_overview_data = $this->get_cached_var_for_time( 'keyword_overview_data' );
                $data = WSKO_Class_Helper::prepare_data_table(
                    $keyword_overview_data['lost_keywords'],
                    array(
                    'offset' => $offset,
                    'count'  => $count,
                ),
                    $custom_filter,
                    array(
                    'key' => $order,
                    'dir' => $orderdir,
                ),
                    array(
                    'search' => $search,
                    'format' => array(
                    '0' => 'keyword',
                ),
                ),
                    true
                );
                break;
            case '4':
                $table_data = $this->get_cached_var_for_time( 'table_data' );
                $data = WSKO_Class_Helper::prepare_data_table(
                    $table_data['table_pages'],
                    array(
                    'offset' => $offset,
                    'count'  => $count,
                ),
                    $custom_filter,
                    array(
                    'key' => $order,
                    'dir' => $orderdir,
                ),
                    array(
                    'search' => $search,
                    'format' => array(
                    '0' => function ( $arg ) {
                    return WSKO_Class_Template::render_url_post_field_s( $arg, array(
                        'with_co'  => true,
                        'open_tab' => 'keywords',
                    ), true );
                },
                ),
                ),
                    true
                );
                break;
            case '5':
                $page_overview_data = $this->get_cached_var_for_time( 'page_overview_data' );
                $data = WSKO_Class_Helper::prepare_data_table(
                    $page_overview_data['new_pages'],
                    array(
                    'offset' => $offset,
                    'count'  => $count,
                ),
                    $custom_filter,
                    array(
                    'key' => $order,
                    'dir' => $orderdir,
                ),
                    array(
                    'search' => $search,
                    'format' => array(
                    '0' => function ( $arg ) {
                    return WSKO_Class_Template::render_url_post_field_s( $arg, array(
                        'with_co'  => true,
                        'open_tab' => 'keywords',
                    ), true );
                },
                ),
                ),
                    true
                );
                break;
            case '6':
                $page_overview_data = $this->get_cached_var_for_time( 'page_overview_data' );
                $data = WSKO_Class_Helper::prepare_data_table(
                    $page_overview_data['lost_pages'],
                    array(
                    'offset' => $offset,
                    'count'  => $count,
                ),
                    $custom_filter,
                    array(
                    'key' => $order,
                    'dir' => $orderdir,
                ),
                    array(
                    'search' => $search,
                    'format' => array(
                    '0' => function ( $arg ) {
                    return WSKO_Class_Template::render_url_post_field_s( $arg, array(
                        'with_co'  => true,
                        'open_tab' => 'keywords',
                    ), true );
                },
                ),
                ),
                    true
                );
                break;
        }
        return array(
            'success'         => true,
            'data'            => ( $data ? $data['data'] : array() ),
            'recordsFiltered' => ( $data ? $data['filtered'] : array() ),
            'recordsTotal'    => ( $data ? $data['total'] : array() ),
        );
    }
    
    public function action_add_monitoring_keyword()
    {
        if ( !$this->can_execute_action( false ) ) {
            return false;
        }
        $keyword = ( isset( $_POST['keyword'] ) ? sanitize_text_field( $_POST['keyword'] ) : false );
        $multi = ( isset( $_POST['multi'] ) && $_POST['multi'] && $_POST['multi'] != 'false' ? true : false );
        
        if ( $keyword ) {
            
            if ( $multi ) {
                $keywords = explode( ',', $keyword );
                foreach ( $keywords as $kw ) {
                    WSKO_Class_Search::add_monitored_keyword( trim( strtolower( $kw ) ) );
                }
            } else {
                WSKO_Class_Search::add_monitored_keyword( trim( strtolower( $keyword ) ) );
            }
            
            $this->delete_cache();
            return true;
        }
    
    }
    
    public function action_remove_monitoring_keyword()
    {
        if ( !$this->can_execute_action( false ) ) {
            return false;
        }
        $this->delete_cache();
        $keyword = ( isset( $_POST['keyword'] ) ? sanitize_text_field( $_POST['keyword'] ) : false );
        
        if ( $keyword ) {
            WSKO_Class_Search::remove_monitored_keyword( $keyword );
            return true;
        }
    
    }
    
    //Cache methods
    public function get_cached_timespan_data( $args )
    {
        global  $wpdb ;
        $res = array(
            'has_data' => false,
            'has_ref'  => false,
        );
        $client = WSKO_Class_Search::get_ga_client_se();
        $time_diff = abs( $this->timespan_end - $this->timespan_start );
        $day_diff = floor( $time_diff / (60 * 60 * 24) );
        $time_diff = $day_diff * (60 * 60 * 24);
        $res['date_rows'] = WSKO_Class_Search::get_se_data( $this->timespan_start, $this->timespan_end, 'date' );
        $res['page_rows'] = WSKO_Class_Search::get_se_data( $this->timespan_start, $this->timespan_end, 'page' );
        $res['device_rows'] = WSKO_Class_Search::get_se_data( $this->timespan_start, $this->timespan_end, 'device' );
        $res['country_rows'] = WSKO_Class_Search::get_se_data( $this->timespan_start, $this->timespan_end, 'country' );
        $res['kw_rows'] = WSKO_Class_Search::get_se_data( $this->timespan_start, $this->timespan_end, 'query' );
        if ( $res['date_rows'] !== -1 && $res['page_rows'] !== -1 && $res['kw_rows'] !== -1 && $res['date_rows'] !== false && $res['page_rows'] !== false && $res['kw_rows'] !== false ) {
            //uksort($res['date_rows'], function($a, $b){ $a = strtotime($a); $b = strtotime($b); return $a > $b ? 1 : -1; });
            $res['has_data'] = true;
        }
        
        if ( $res['has_data'] ) {
            $time3 = $this->timespan_start - ($this->timespan_end - $this->timespan_start);
            $res['kw_rows_ref'] = WSKO_Class_Search::get_se_data( $time3, $this->timespan_start, 'query' );
            $res['page_rows_ref'] = WSKO_Class_Search::get_se_data( $time3, $this->timespan_start, 'page' );
            $res['date_rows_ref'] = WSKO_Class_Search::get_se_data( $time3, $this->timespan_start, 'date' );
            if ( $res['date_rows_ref'] != -1 && $res['page_rows_ref'] != -1 && $res['kw_rows_ref'] != -1 && $res['date_rows_ref'] !== false && $res['page_rows_ref'] !== false && $res['kw_rows_ref'] !== false ) {
                $res['has_ref'] = true;
            }
        }
        
        return $res;
    }
    
    public function get_cached_snippet_data( $args )
    {
        $res = array();
        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
        $kw_dist = array();
        $kw_dist_ref = array();
        $kw_count = 0;
        $kw_count_ref = 0;
        $kw_clicks = 0;
        $kw_clicks_ref = 0;
        $kw_imp = 0;
        $kw_imp_ref = 0;
        $page_dist_count = 0;
        $page_dist = array();
        $page_dist_ref = array();
        $page_count = 0;
        $page_count_ref = 0;
        $page_dist_count = 0;
        $max_kw_clicks = 0;
        $max_kw_imp = 0;
        $max_kw_position = 0;
        $max_page_clicks = 0;
        $max_page_imp = 0;
        $max_page_position = 0;
        $has_ref = false;
        
        if ( !empty($timespan_data['kw_rows']) ) {
            foreach ( $timespan_data['kw_rows'] as $row ) {
                $kw_count++;
                
                if ( $row->position < 100 ) {
                    $pos = floor( $row->position / 10 ) + 1;
                    
                    if ( isset( $kw_dist[(string) $pos] ) ) {
                        $kw_dist[(string) $pos]++;
                    } else {
                        $kw_dist[(string) $pos] = 1;
                    }
                    
                    if ( $pos <= 1 ) {
                        $kw_dist_count++;
                    }
                }
                
                if ( $row->clicks > $max_kw_clicks ) {
                    $max_kw_clicks = $row->clicks;
                }
                if ( $row->position > $max_kw_position ) {
                    $max_kw_position = $row->position;
                }
                if ( $row->impressions > $max_kw_imp ) {
                    $max_kw_imp = $row->impressions;
                }
            }
            ksort( $kw_dist );
        }
        
        
        if ( !empty($timespan_data['page_rows']) ) {
            foreach ( $timespan_data['page_rows'] as $row ) {
                $page_count++;
                
                if ( $row->position < 100 ) {
                    $pos = floor( $row->position / 10 ) + 1;
                    
                    if ( isset( $page_dist[(string) $pos] ) ) {
                        $page_dist[(string) $pos]++;
                    } else {
                        $page_dist[(string) $pos] = 1;
                    }
                    
                    if ( $pos <= 1 ) {
                        $page_dist_count++;
                    }
                }
                
                if ( $row->clicks > $max_page_clicks ) {
                    $max_page_clicks = $row->clicks;
                }
                if ( $row->position > $max_page_position ) {
                    $max_page_position = $row->position;
                }
                if ( $row->impressions > $max_page_imp ) {
                    $max_page_imp = $row->impressions;
                }
            }
            ksort( $page_dist );
        }
        
        if ( !empty($timespan_data['date_rows']) ) {
            foreach ( $timespan_data['date_rows'] as $row ) {
                $kw_clicks += $row->clicks;
                $kw_imp += $row->impressions;
            }
        }
        $kw_dist_count_ref = 0;
        
        if ( !empty($timespan_data['kw_rows_ref']) ) {
            if ( !empty($timespan_data['date_rows_ref']) ) {
                $has_ref = true;
            }
            foreach ( $timespan_data['kw_rows_ref'] as $row ) {
                $kw_count_ref++;
                
                if ( $row->position < 100 ) {
                    $pos = floor( $row->position / 10 ) + 1;
                    
                    if ( isset( $kw_dist_ref[(string) $pos] ) ) {
                        $kw_dist_ref[(string) $pos]++;
                    } else {
                        $kw_dist_ref[(string) $pos] = 1;
                    }
                    
                    if ( $pos <= 1 ) {
                        $kw_dist_count_ref++;
                    }
                }
            
            }
        }
        
        $page_dist_count_ref = 0;
        
        if ( !empty($timespan_data['page_rows_ref']) ) {
            if ( !empty($timespan_data['date_rows_ref']) ) {
                $has_ref = true;
            }
            foreach ( $timespan_data['page_rows_ref'] as $row ) {
                $page_count_ref++;
                
                if ( $row->position < 100 ) {
                    $pos = floor( $row->position / 10 ) + 1;
                    
                    if ( isset( $page_dist_ref[(string) $pos] ) ) {
                        $page_dist_ref[(string) $pos]++;
                    } else {
                        $page_dist_ref[(string) $pos] = 1;
                    }
                    
                    if ( $pos <= 1 ) {
                        $page_dist_count_ref++;
                    }
                }
            
            }
            ksort( $page_dist_ref );
        }
        
        if ( !empty($timespan_data['date_rows_ref']) ) {
            foreach ( $timespan_data['date_rows_ref'] as $row ) {
                $kw_clicks_ref += $row->clicks;
                $kw_imp_ref += $row->impressions;
            }
        }
        $res['max_page_clicks'] = $max_page_clicks;
        $res['max_page_imp'] = $max_page_imp;
        $res['max_page_position'] = $max_page_position;
        $res['max_kw_clicks'] = $max_kw_clicks;
        $res['max_kw_imp'] = $max_kw_imp;
        $res['max_kw_position'] = $max_kw_position;
        $res['has_ref'] = $has_ref;
        $res['kw_dist'] = $kw_dist;
        $res['kw_dist_ref'] = $kw_dist_ref;
        $res['kw_dist_count'] = $kw_dist_count;
        $res['kw_dist_count_ref'] = $kw_dist_count_ref;
        $res['kw_count'] = $kw_count;
        $res['kw_count_ref'] = $kw_count_ref;
        $res['kw_clicks'] = $kw_clicks;
        $res['kw_clicks_ref'] = $kw_clicks_ref;
        $res['kw_imp'] = $kw_imp;
        $res['kw_imp_ref'] = $kw_imp_ref;
        $res['kw_count_ref_perc'] = ( $kw_count_ref && $kw_count != $kw_count_ref ? round( ($kw_count - $kw_count_ref) / $kw_count_ref * 100, 2 ) : 0 );
        $res['kw_dist_count_ref_perc'] = ( $kw_dist_count_ref && $kw_dist_count != $kw_dist_count_ref ? round( ($kw_dist_count - $kw_dist_count_ref) / $kw_dist_count_ref * 100, 2 ) : 0 );
        $res['kw_clicks_ref_perc'] = ( $kw_clicks_ref && $kw_clicks != $kw_clicks_ref ? round( ($kw_clicks - $kw_clicks_ref) / $kw_clicks_ref * 100, 2 ) : 0 );
        $res['kw_imp_ref_perc'] = ( $kw_imp_ref && $kw_imp != $kw_imp_ref ? round( ($kw_imp - $kw_imp_ref) / $kw_imp_ref * 100, 2 ) : 0 );
        $res['page_dist_count'] = $page_dist_count;
        $res['page_dist_count_ref'] = $page_dist_count_ref;
        $res['page_count'] = $page_count;
        $res['page_count_ref'] = $page_count_ref;
        $res['page_count_ref_perc'] = ( $page_count_ref && $page_count != $page_count_ref ? round( ($page_count - $page_count_ref) / $page_count_ref * 100, 2 ) : 0 );
        $res['page_dist_count_ref_perc'] = ( $page_dist_count_ref && $page_dist_count != $page_dist_count_ref ? round( ($page_dist_count - $page_dist_count_ref) / $page_dist_count_ref * 100, 2 ) : 0 );
        $res['no_data'] = 0;
        $res['no_data_ref'] = 0;
        $time3 = $this->timespan_start - ($this->timespan_end - $this->timespan_start);
        
        if ( $timespan_data['has_data'] ) {
            $times = array();
            for ( $i = $this->timespan_start ;  $i <= $this->timespan_end ;  $i += 60 * 60 * 24 ) {
                $times[date( 'Y-m-d', $i )] = true;
            }
            if ( !empty($timespan_data['date_rows']) ) {
                foreach ( $timespan_data['date_rows'] as $row ) {
                    unset( $times[$row->keyval] );
                }
            }
            $res['no_data'] = count( $times );
            $times_ref = array();
            for ( $i = $time3 ;  $i <= $this->timespan_start ;  $i += 60 * 60 * 24 ) {
                $times_ref[date( 'Y-m-d', $i )] = true;
            }
            if ( !empty($timespan_data['date_rows_ref']) ) {
                foreach ( $timespan_data['date_rows_ref'] as $row ) {
                    unset( $times_ref[$row->keyval] );
                }
            }
            $res['no_data_ref'] = count( $times_ref );
        }
        
        return $res;
    }
    
    public function get_cached_overview_graph_data( $args )
    {
        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
        $snippet_data = $this->get_cached_var_for_time( 'snippet_data' );
        $res = array();
        
        if ( isset( $timespan_data['date_rows'] ) ) {
            $diff = $this->timespan_end - $this->timespan_start;
            $time3 = $this->timespan_start - $diff;
            $keywords = array();
            $keywords_d = WSKO_Class_Search::get_se_cache_history( $this->timespan_start, $this->timespan_end, 'query' );
            $keywords_d_ref = WSKO_Class_Search::get_se_cache_history( $time3, $this->timespan_start, 'query' );
            foreach ( $keywords_d as $kw ) {
                $daydiff = round( ($this->timespan_end - strtotime( $kw->time )) / (60 * 60 * 24) );
                $kw->time = date( "M. d, Y", strtotime( $kw->time ) );
                $kw->count_ref = 0;
                $keywords[$daydiff] = $kw;
            }
            foreach ( $keywords_d_ref as $kw2 ) {
                $daydiff = round( ($this->timespan_start - strtotime( $kw2->time )) / (60 * 60 * 24) );
                if ( isset( $keywords[$daydiff] ) ) {
                    $keywords[$daydiff]->count_ref = $kw2->count;
                }
            }
            $pages = array();
            $pages_d = WSKO_Class_Search::get_se_cache_history( $this->timespan_start, $this->timespan_end, 'page' );
            $pages_d_ref = WSKO_Class_Search::get_se_cache_history( $time3, $this->timespan_start, 'page' );
            foreach ( $pages_d as $p ) {
                $daydiff = round( ($this->timespan_end - strtotime( $p->time )) / (60 * 60 * 24) );
                $p->time = date( "M. d, Y", strtotime( $p->time ) );
                $p->count_ref = 0;
                $pages[$daydiff] = $p;
            }
            foreach ( $pages_d_ref as $p2 ) {
                $daydiff = round( ($this->timespan_start - strtotime( $p2->time )) / (60 * 60 * 24) );
                if ( isset( $pages[$daydiff] ) ) {
                    $pages[$daydiff]->count_ref = $p2->count;
                }
            }
            $rankings = WSKO_Class_Search::get_se_cache_history_position( $this->timespan_start, $this->timespan_end );
            foreach ( $rankings as $k => $r ) {
                $rankings[$k]->time = date( "M. d, Y", strtotime( $rankings[$k]->time ) );
            }
            $clicks = array();
            $position = array();
            $impressions = array();
            $devices = array();
            $countries = array();
            foreach ( $timespan_data['date_rows'] as $row ) {
                $date = date( "M. d, Y", strtotime( $row->keyval ) );
                $daydiff = round( ($this->timespan_end - strtotime( $row->keyval )) / (60 * 60 * 24) );
                $clicks[$daydiff] = array( $date, $row->clicks, 0 );
                $position[$daydiff] = array( $date, $row->position, 0 );
                $impressions[$daydiff] = array( $date, $row->impressions, 0 );
            }
            foreach ( $timespan_data['date_rows_ref'] as $i => $row ) {
                $daydiff = round( ($this->timespan_start - strtotime( $row->keyval )) / (60 * 60 * 24) );
                if ( isset( $clicks[$daydiff] ) ) {
                    $clicks[$daydiff][2] = $row->clicks;
                }
                if ( isset( $position[$daydiff] ) ) {
                    $position[$daydiff][2] = $row->position;
                }
                if ( isset( $impressions[$daydiff] ) ) {
                    $impressions[$daydiff][2] = $row->impressions;
                }
            }
            foreach ( $timespan_data['device_rows'] as $row ) {
                $devices[] = array( $row->keyval, $row->clicks );
            }
            foreach ( $timespan_data['country_rows'] as $row ) {
                $country = WSKO_Class_Helper::get_country_name( strtoupper( $row->keyval ) );
                if ( $country ) {
                    $countries[] = array( $country, $row->clicks );
                }
            }
            $res['history_keywords'] = $keywords;
            $res['history_pages'] = $pages;
            $res['history_clicks'] = $clicks;
            $res['history_position'] = $position;
            $res['history_impressions'] = $impressions;
            $res['history_rankings'] = $rankings;
            $res['history_devices'] = $devices;
            $res['history_countries'] = $countries;
            $graph_data = array();
            if ( isset( $snippet_data['kw_dist'] ) ) {
                foreach ( $snippet_data['kw_dist'] as $pos => $count ) {
                    $graph_data[] = array( $pos, $count );
                }
            }
            $res['history_ctr'] = $graph_data;
        }
        
        return $res;
    }
    
    public function get_cached_table_data( $args )
    {
        $res = array();
        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
        $res['top_keywords'] = array();
        $res['top_pages'] = array();
        $res['table_keywords'] = array();
        $res['table_pages'] = array();
        $max = array(
            'kw_clicks'        => 0,
            'kw_impressions'   => 0,
            'kw_position'      => 0,
            'page_clicks'      => 0,
            'page_impressions' => 0,
            'page_position'    => 0,
        );
        if ( isset( $timespan_data['kw_rows'] ) ) {
            foreach ( $timespan_data['kw_rows'] as $key => $row ) {
                $kw = $row->keyval;
                if ( $row->clicks > $max['kw_clicks'] ) {
                    $max['kw_clicks'] = $row->clicks;
                }
                if ( $row->impressions > $max['kw_impressions'] ) {
                    $max['kw_impressions'] = $row->impressions;
                }
                if ( $row->position > $max['kw_position'] ) {
                    $max['kw_position'] = $row->position;
                }
                $ref_row = false;
                if ( isset( $timespan_data['kw_rows_ref'] ) ) {
                    if ( isset( $timespan_data['kw_rows_ref'][$row->keyval] ) ) {
                        $ref_row = $timespan_data['kw_rows_ref'][$row->keyval];
                    }
                }
                
                if ( $ref_row ) {
                    $ref_clicks = ( $ref_row->clicks != 0 && $ref_row->clicks != $row->clicks ? round( ($row->clicks - $ref_row->clicks) / $ref_row->clicks * 100, 2 ) : 0 );
                    $ref_position = ( $ref_row->position != 0 && round( $ref_row->position ) != round( $row->position ) ? -round( (round( $row->position ) - round( $ref_row->position )) / $ref_row->position * 100, 2 ) : 0 );
                    $ref_impressions = ( $ref_row->impressions != 0 && $ref_row->impressions != $row->impressions ? round( ($row->impressions - $ref_row->impressions) / $ref_row->impressions * 100, 2 ) : 0 );
                    $ref_ctr = ( $ref_row->ctr != 0 && $ref_row->ctr != $row->ctr ? round( ($row->ctr - $ref_row->ctr) / $ref_row->ctr * 100, 2 ) : 0 );
                }
                
                $res['table_keywords'][] = array(
                    array(
                        'class' => 'wsko_table_col1',
                        'value' => $kw,
                    ),
                    array(
                        'order' => $row->position,
                        'value' => '<span style="float:left;">' . round( $row->position, 0 ) . '</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        round( $ref_row->position, 0 ),
                        $ref_position,
                        array(),
                        true
                    ) : '' )),
                    ),
                    //<span class="wsko_single_progress '.($ref_row && $ref_position != 0 ? ($ref_position < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_position > 0)) ? '+' : '').($ref_row ? $ref_position : '-').' %</span>'),
                    array(
                        'order' => $row->clicks,
                        'value' => '<span style="float:left;">' . $row->clicks . '</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        $ref_row->clicks,
                        $ref_clicks,
                        array(),
                        true
                    ) : '' )),
                    ),
                    //<span class="wsko_single_progress '.($ref_row && $ref_clicks != 0 ? ($ref_clicks < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_clicks > 0)) ? '+' : '').($ref_row ? $ref_clicks : '-').' %</span>'),
                    array(
                        'order' => $row->impressions,
                        'value' => '<span style="float:left;">' . $row->impressions . '</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        $ref_row->impressions,
                        $ref_impressions,
                        array(),
                        true
                    ) : '' )),
                    ),
                    //<span class="wsko_single_progress '.($ref_row && $ref_impressions != 0 ? ($ref_impressions < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_impressions > 0)) ? '+' : '').($ref_row ? $ref_impressions : '-').' %</span>'),
                    array(
                        'order' => $row->ctr,
                        'value' => '<span style="float:left;">' . round( $row->ctr, 2 ) . ' %</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        round( $ref_row->ctr, 2 ),
                        $ref_ctr,
                        array(),
                        true
                    ) : '' )),
                    ),
                );
            }
        }
        if ( isset( $timespan_data['page_rows'] ) ) {
            foreach ( $timespan_data['page_rows'] as $row ) {
                $url = $row->keyval;
                if ( $row->clicks > $max['page_clicks'] ) {
                    $max['page_clicks'] = $row->clicks;
                }
                if ( $row->impressions > $max['page_impressions'] ) {
                    $max['page_impressions'] = $row->impressions;
                }
                if ( $row->position > $max['page_position'] ) {
                    $max['page_position'] = $row->position;
                }
                $ref_row = false;
                if ( isset( $timespan_data['page_rows_ref'] ) ) {
                    if ( isset( $timespan_data['page_rows_ref'][$row->keyval] ) ) {
                        $ref_row = $timespan_data['page_rows_ref'][$row->keyval];
                    }
                }
                
                if ( $ref_row ) {
                    $ref_clicks = ( $ref_row->clicks != 0 && $ref_row->clicks != $row->clicks ? round( ($row->clicks - $ref_row->clicks) / $ref_row->clicks * 100, 2 ) : 0 );
                    $ref_position = ( $ref_row->position != 0 && round( $ref_row->position ) != round( $row->position ) ? -round( (round( $row->position ) - round( $ref_row->position )) / $ref_row->position * 100, 2 ) : 0 );
                    $ref_impressions = ( $ref_row->impressions != 0 && $ref_row->impressions != $row->impressions ? round( ($row->impressions - $ref_row->impressions) / $ref_row->impressions * 100, 2 ) : 0 );
                    $ref_ctr = ( $ref_row->ctr != 0 && $ref_row->ctr != $row->ctr ? round( ($row->ctr - $ref_row->ctr) / $ref_row->ctr * 100, 2 ) : 0 );
                }
                
                $res['table_pages'][] = array(
                    array(
                        'class' => 'wsko_table_col1',
                        'value' => $url,
                    ),
                    array(
                        'order' => $row->position,
                        'value' => '<span style="float:left;">' . round( $row->position, 0 ) . '</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        round( $ref_row->position, 0 ),
                        $ref_position,
                        array(),
                        true
                    ) : '' )),
                    ),
                    //'<span class="wsko_single_progress '.($ref_row && $ref_position != 0 ? ($ref_position < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_position > 0)) ? '+' : '').($ref_row ? $ref_position : '-').' %</span>'),
                    array(
                        'order' => $row->clicks,
                        'value' => '<span style="float:left;">' . $row->clicks . '</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        $ref_row->clicks,
                        $ref_clicks,
                        array(),
                        true
                    ) : '' )),
                    ),
                    //<span class="wsko_single_progress '.($ref_row && $ref_clicks != 0 ? ($ref_clicks < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_clicks > 0)) ? '+' : '').($ref_row ? $ref_clicks : '-').' %</span>'),
                    array(
                        'order' => $row->impressions,
                        'value' => '<span style="float:left;">' . $row->impressions . '</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        $ref_row->impressions,
                        $ref_impressions,
                        array(),
                        true
                    ) : '' )),
                    ),
                    //<span class="wsko_single_progress '.($ref_row && $ref_impressions != 0 ? ($ref_impressions < 0 ? 'wsko_red_font' : 'wsko_green_font') : 'wsko_gray_font').'">'.(($ref_row && ($ref_impressions > 0)) ? '+' : '').($ref_row ? $ref_impressions : '-').' %</span>'),
                    array(
                        'order' => $row->ctr,
                        'value' => '<span style="float:left;">' . round( $row->ctr, 2 ) . ' %</span> ' . (( $ref_row ? WSKO_Class_Template::render_progress_icon(
                        round( $ref_row->ctr, 2 ),
                        $ref_ctr,
                        array(),
                        true
                    ) : '' )),
                    ),
                );
            }
        }
        $res['top_keywords'] = array_slice( $res['table_keywords'], 0, 5 );
        $res['top_pages'] = array_slice( $res['table_pages'], 0, 5 );
        $res['max'] = $max;
        return $res;
    }
    
    public function get_cached_keyword_overview_data( $args )
    {
        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
        $res = array(
            'top_new_keywords'  => array(),
            'top_lost_keywords' => array(),
            'new_keywords'      => array(),
            'lost_keywords'     => array(),
        );
        
        if ( isset( $timespan_data['kw_rows'] ) ) {
            $new = array();
            $lost = array();
            foreach ( $timespan_data['kw_rows'] as $row ) {
                $kw = $row->keyval;
                $res['new_keywords'][$kw] = array(
                    array(
                    'class' => 'wsko_table_col1',
                    'value' => $kw,
                ),
                    array(
                    'order' => $row->position,
                    'value' => '<span style="float:left;">' . round( $row->position, 0 ) . '</span>',
                ),
                    array(
                    'order' => $row->clicks,
                    'value' => '<span style="float:left;">' . $row->clicks . '</span> ',
                ),
                    array(
                    'order' => $row->impressions,
                    'value' => '<span style="float:left;">' . $row->impressions . '</span> ',
                ),
                    array(
                    'order' => $row->ctr,
                    'value' => '<span style="float:left;">' . round( $row->ctr, 2 ) . ' %</span>',
                )
                );
            }
            if ( isset( $timespan_data['kw_rows_ref'] ) ) {
                foreach ( $timespan_data['kw_rows_ref'] as $row ) {
                    $kw = $row->keyval;
                    
                    if ( !isset( $res['new_keywords'][$kw] ) ) {
                        $res['lost_keywords'][] = array(
                            array(
                            'class' => 'wsko_table_col1',
                            'value' => $kw,
                        ),
                            array(
                            'order' => $row->position,
                            'value' => '<span style="float:left;">' . round( $row->position, 0 ) . '</span>',
                        ),
                            array(
                            'order' => $row->clicks,
                            'value' => '<span style="float:left;">' . $row->clicks . '</span> ',
                        ),
                            array(
                            'order' => $row->impressions,
                            'value' => '<span style="float:left;">' . $row->impressions . '</span> ',
                        ),
                            array(
                            'order' => $row->ctr,
                            'value' => '<span style="float:left;">' . round( $row->ctr, 2 ) . ' %</span>',
                        )
                        );
                    } else {
                        unset( $res['new_keywords'][$kw] );
                    }
                
                }
            }
        }
        
        $res['top_new_keywords'] = array_slice( $res['new_keywords'], 0, 5 );
        $res['top_lost_keywords'] = array_slice( $res['lost_keywords'], 0, 5 );
        return $res;
    }
    
    public function get_cached_page_overview_data( $args )
    {
        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
        $res = array(
            'top_new_pages'  => array(),
            'top_lost_pages' => array(),
            'new_pages'      => array(),
            'lost_pages'     => array(),
        );
        
        if ( isset( $timespan_data['page_rows'] ) ) {
            $new = array();
            $lost = array();
            foreach ( $timespan_data['page_rows'] as $row ) {
                $url = $row->keyval;
                $res['new_pages'][$url] = array(
                    array(
                    'class' => 'wsko_table_col1',
                    'value' => $url,
                ),
                    array(
                    'order' => $row->position,
                    'value' => '<span style="float:left;">' . round( $row->position, 0 ) . '</span>',
                ),
                    array(
                    'order' => $row->clicks,
                    'value' => '<span style="float:left;">' . $row->clicks . '</span> ',
                ),
                    array(
                    'order' => $row->impressions,
                    'value' => '<span style="float:left;">' . $row->impressions . '</span> ',
                ),
                    array(
                    'order' => $row->ctr,
                    'value' => '<span style="float:left;">' . round( $row->ctr, 2 ) . ' %</span>',
                )
                );
            }
            if ( isset( $timespan_data['page_rows_ref'] ) ) {
                foreach ( $timespan_data['page_rows_ref'] as $row ) {
                    $url = $row->keyval;
                    
                    if ( !isset( $res['new_pages'][$url] ) ) {
                        $res['lost_pages'][] = array(
                            array(
                            'class' => 'wsko_table_col1',
                            'value' => $url,
                        ),
                            array(
                            'order' => $row->position,
                            'value' => '<span style="float:left;">' . round( $row->position, 0 ) . '</span>',
                        ),
                            array(
                            'order' => $row->clicks,
                            'value' => '<span style="float:left;">' . $row->clicks . '</span> ',
                        ),
                            array(
                            'order' => $row->impressions,
                            'value' => '<span style="float:left;">' . $row->impressions . '</span> ',
                        ),
                            array(
                            'order' => $row->ctr,
                            'value' => '<span style="float:left;">' . round( $row->ctr, 2 ) . ' %</span>',
                        )
                        );
                    } else {
                        unset( $res['new_pages'][$url] );
                    }
                
                }
            }
        }
        
        $res['top_new_pages'] = array_slice( $res['new_pages'], 0, 5 );
        $res['top_lost_pages'] = array_slice( $res['lost_pages'], 0, 5 );
        return $res;
    }
    
    public function get_cached_monitoring_data( $args )
    {
        $res = array();
        $timespan_data = $this->get_cached_var_for_time( 'timespan_data' );
        $total_keywords = 0;
        $total_clicks = 0;
        $total_impressions = 0;
        $avg_postion = 0;
        $monitored_keywords = WSKO_Class_Search::get_monitored_keywords();
        $table_kw = array();
        foreach ( $monitored_keywords as $mkw ) {
            
            if ( isset( $timespan_data['kw_rows'][$mkw] ) ) {
                $kw_obj = $timespan_data['kw_rows'][$mkw];
            } else {
                $kw_obj = WSKO_Class_Search::get_empty_se_row( $mkw );
            }
            
            $total_keywords++;
            $total_clicks += $kw_obj->clicks;
            $total_impressions += $kw_obj->impressions;
            $avg_postion += $kw_obj->position;
            $table_kw[] = array(
                array(
                'value' => $kw_obj->keyval,
            ),
                array(
                'value' => round( $kw_obj->position, 0 ),
            ),
                array(
                'value' => $kw_obj->clicks,
            ),
                array(
                'value' => $kw_obj->impressions,
            ),
                array(
                'order' => $kw_obj->ctr,
                'value' => round( $kw_obj->ctr, 2 ) . ' %',
            ),
                array(
                'value' => '<a class="wsko-remove-monitoring-keyword" data-keyword="' . $kw_obj->keyval . '" data-nonce="' . wp_create_nonce( 'wsko_remove_monitoring_keyword' ) . '" href=""><i class="fa fa-times"></i></a>',
            )
            );
        }
        $res['total_keywords'] = $total_keywords;
        $res['total_clicks'] = $total_clicks;
        $res['total_impressions'] = $total_impressions;
        
        if ( $total_keywords > 0 ) {
            $res['avg_position'] = round( $avg_postion / $total_keywords, 2 );
        } else {
            $res['avg_position'] = 0;
        }
        
        $history_clicks = array();
        $history_position = array();
        $history_impressions = array();
        $history_ctr = array();
        $histories = WSKO_Class_Search::get_se_cache_keword_history( $this->timespan_start, $this->timespan_end, $monitored_keywords );
        foreach ( $histories as $history ) {
            $history_clicks[] = array( $history->time, $history->clicks );
            $history_position[] = array( $history->time, round( $history->position, 2 ) );
            $history_impressions[] = array( $history->time, $history->impressions );
            $history_ctr[] = array( $history->time, round( $history->clicks / $history->impressions * 100, 2 ) );
        }
        $res['history_clicks'] = $history_clicks;
        $res['history_position'] = $history_position;
        $res['history_impressions'] = $history_impressions;
        $res['history_ctr'] = $history_ctr;
        $res['table_monitored_keywords'] = $table_kw;
        return $res;
    }
    
    /*/*/
    //Singleton
    static  $instance ;
}