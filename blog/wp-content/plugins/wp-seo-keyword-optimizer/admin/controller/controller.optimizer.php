<?php
if (!defined('ABSPATH')) exit;

class WSKO_Controller_Optimizer extends WSKO_Controller
{
	public $link = "";
	public $title = "";
	
	public $ajax_actions = array('co_save_content', 'co_add_priority_keyword', 'co_delete_priority_keyword', 'co_get_keyword_suggests', 'co_table_data', 'co_save_technicals', 'co_change_height');
	
	public function load_lazy_page_data()
	{
	}
	
	public static function render($post_id, $widget = false, $preview_args = false, $open_tab = false)
	{
		$int = static::get_instance();
		if (WSKO_Class_Core::get_option('search_query_first_run'))
			$keywords = $int->get_keywords($post_id);
		else
			$keywords = array();
		$view = WSKO_Class_Template::render_template('admin/templates/template-content-optimizer.php', array('post_id' => $post_id, 'widget' => $widget, 'keywords' => $keywords, 'preview' => $preview_args, 'open_tab' => $open_tab ), true);
		return $view;
	}
	
	public function action_co_save_content()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$post_id = isset($_POST['post_id']) ? intval($_POST['post_id']) : false;
		if ($post_id)
		{
			$title = isset($_POST['title']) ? sanitize_post_field('post_title', $_POST['title'], $post_id) : false;
			$slug = isset($_POST['slug']) ? sanitize_title_with_dashes(WSKO_Class_Helper::map_special_chars($_POST['slug'])) : false;
			$content = isset($_POST['content']) ? sanitize_post_field('post_content', $_POST['content'], $post_id) : false;
			
			if ($title && $content && $slug)
			{
				wp_update_post(array(
					'ID'           => $post_id,
					'post_title'   => $title,
					'post_content' => $content,
					'post_name'    => $slug,
				));
				return true;
			}
		}
	}
	
	public function action_co_add_priority_keyword()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$post_id = isset($_POST['post_id']) ? intval($_POST['post_id']) : false;
		$keywords = isset($_POST['keyword']) && $_POST['keyword'] ? explode(',', sanitize_text_field($_POST['keyword'])) : false;
		$prio = isset($_POST['prio']) ? intval($_POST['prio']) : false;
		if ($post_id && $keywords && $prio)
		{
			$duplicate = false;
			$hit_limit = false;
			$view = "";
			foreach ($keywords as $keyword)
			{
				$keyword = trim(strtolower($keyword));
				$kws = WSKO_Class_Onpage::get_priority_keywords($post_id);
				if ($keyword)
				{
					if (!$kws || count(array_keys($kws, 1)) < 2)
					{
						if ($kws && isset($kws[$keyword]) && $kws[$keyword] == $prio)
						{
							$duplicate = true;
						}
						else
						{
							WSKO_Class_Onpage::add_priority_keyword($post_id, $keyword, $prio);
							
							$keyword_data = WSKO_Controller_Optimizer::get_instance()->get_keywords($post_id);
							$kw_data = array();
							if ($keyword_data)
							{
								foreach ($keyword_data as $kw)
								{
									if ($kw->keyval == $keyword)
									{
										$kw_data['clicks'] = $kw->clicks;
										$kw_data['pos'] = $kw->position;
									}
								}
							}
							$op_report = WSKO_Class_Onpage::get_onpage_report($post_id);
							
							if ($op_report['issues']['keyword_density'])
							{
								foreach ($op_report['issues']['keyword_density'] as $k => $v)
								{
									if ($k == $keyword)
									{
										$kw_data['den'] = $v['density'];
										$kw_data['den_type']= $v['type'];
									}
								}
							}
							$view .= WSKO_Class_Template::render_priority_keyword_item($post_id, $keyword, $kw_data, true);
						}
					}
					else
					{
						$hit_limit = true;
					}
				}
			}
			if (!$duplicate && !$hit_limit)
			{
				return array('success' => true, 'view' => $view);
			}
			else if ($duplicate)
			{
				return array('success' => false, 'view' => $view, 'msg' => 'You have allready added this keyword.');
			}
			else if ($hit_limit)
			{
				return array('success' => false, 'view' => $view, 'msg' => 'You can only have 2 priority keywords per post in the free version.');
			}
		}
	}
	
	public function action_co_delete_priority_keyword()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$post_id = isset($_POST['post_id']) ? intval($_POST['post_id']) : false;
		$keyword = isset($_POST['keyword']) ? trim(strtolower(sanitize_text_field($_POST['keyword']))) : false;
		if ($post_id && $keyword)
		{
			WSKO_Class_Onpage::remove_priority_keyword($post_id, $keyword);
			return true;
		}
	}
	
	public function action_co_get_keyword_suggests()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$keyword = isset($_POST['keyword']) ? sanitize_text_field($_POST['keyword']) : false;
		if ($keyword)
		{
			$keyword = utf8_encode(urlencode(trim($keyword)));
			$loc = "";
			if (WSKO_Class_Core::get_setting('non_latin'))
			{
				$loc = get_locale();
				if (!$loc)
					$loc = 'de';
				else
					$loc = reset(explode('_', $loc));
				$loc = "&hl=" . $loc;
			}
			if (ini_get('allow_url_fopen'))
			{
				$file = utf8_encode(file_get_contents("http://google.de/complete/search?output=toolbar" . $loc . "&q=".$keyword));
				$xml = simplexml_load_string($file); 
				if ($xml)
				{
					ob_start();
					foreach($xml->CompleteSuggestion as $value)
					{
						echo '<li><a href="#" class="wsko-co-keyword-suggestion dark" data-val="'.$value->suggestion['data'].'">'.$value->suggestion['data'].'</a> <span class="wsko-badge badge-default"><a href="#" class="wsko-keyword-suggestion-add dark" data-keyword="'.$value->suggestion['data'].'" data-prio="1"><i class="fa fa-plus fa-fw"></i></a></span></li>';
					}
					$c = ob_get_clean();
					return array('success' => true, 'view' => $c);
				}
			}
		}
		else
		{
			return array('success' => true, 'view' => "");
		}
	}
	
	public function action_co_table_data()
	{
		if (!$this->can_execute_action(false))
			return false;
		
	}
	
	public function action_co_save_technicals()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$post = isset($_POST['post_id']) ? intval($_POST['post_id']) : false;
		$data_t = isset($_POST['data']) ? $_POST['data'] : false;
		if ($post)
		{
			$data = WSKO_Class_Onpage::get_technical_seo_data($post);
			if (isset($data_t['canon_type']) && in_array($data_t['canon_type'], array('1','2'/*,'3'*/)) && isset($data_t['canon_arg']))
			{
				$arg = sanitize_text_field($data_t['canon_arg']);
				if ($arg)
				{
					if ($data_t['canon_type'] === '2')
					{
						if (is_numeric($arg))
							$arg = intval($arg);
						else
							$data_t['canon_type'] = '3';
					}
				}
				if ($arg)
					$data['canonical'] = array('type' => $data_t['canon_type'], 'arg' => $arg);
				else
					unset($data['canonical']);
			}
			else
				unset($data['canonical']);
			if (isset($data_t['activate_redirect']) && $data_t['activate_redirect'] && $data_t['activate_redirect'] !== 'false' && isset($data_t['redirect_type']) && in_array($data_t['redirect_type'], array('1','2')) && isset($data_t['redirect_to']))
				$data['redirect'] = array('type' => $data_t['redirect_type'], 'to' => sanitize_text_field($data_t['redirect_to']));
			else
				unset($data['redirect']);
			
			if (isset($data_t['sitemap_exclude']) && $data_t['sitemap_exclude'] && $data_t['sitemap_exclude'] !== 'false')
			{
				$gen_params = WSKO_Class_Onpage::get_sitemap_params();
				if (!isset($gen_params['excluded_posts']) || !is_array($gen_params['excluded_posts']))
					$gen_params['excluded_posts'] = array($post);
				else if (!in_array($post, $gen_params['excluded_posts']))
					$gen_params['excluded_posts'][] = $post;
				WSKO_Class_Onpage::set_sitemap_params($gen_params);
			}
			else
			{
				$gen_params = WSKO_Class_Onpage::get_sitemap_params();
				if (isset($gen_params['excluded_posts']) && is_array($gen_params['excluded_posts']) && ($key=array_search($post, $gen_params['excluded_posts'])) !== false)
				{
					unset($gen_params['excluded_posts'][$key]);
					WSKO_Class_Onpage::set_sitemap_params($gen_params);
				}
			}
			WSKO_Class_Onpage::set_technical_seo_data($post, $data);
			return true;
		}
	}
	
	public function action_co_change_height()
	{
		if (!$this->can_execute_action(false))
			return false;
		$height = isset($_POST['height']) ? intval($_POST['height']) : false;
		if ($height)
		{
			WSKO_Class_Core::save_option('co_box_height_'.get_current_user_id(), $height);
			return true;
		}
	}
	
	public function get_keywords($post_id)
	{
		$end = time();
		$co_time = intval(WSKO_Class_Core::get_setting('content_optimizer_time'));
		$start = $end - (60*60*24*($co_time?$co_time:28));
		return $this->get_cached_var('keywords', false, array('post_id' => $post_id, 'start' => $start, 'end' => $end));
	}
	
	public function get_cached_keywords($args)
	{
		$post_id = isset($args['post_id']) ? $args['post_id'] : false;
		$start = isset($args['start']) ? $args['start'] : false;
		$end = isset($args['end']) ? $args['end'] : false;
		if ($post_id && $start && $end)
		{
			if (WSKO_Class_Core::get_option('search_query_first_run'))
			{
				$search_connected = !WSKO_Class_Search::check_se_access();
				if ($search_connected)
				{
					$keywords_data = WSKO_Class_Search::get_se_data($start, $end, 'query', defined('WSKO_HOST_BASE') && WSKO_HOST_BASE ? WSKO_HOST_BASE : get_permalink($post_id));
					if ($keywords_data && $keywords_data !== -1)
						return $keywords_data;
				}
			}
		}
		return false;
	}
}
?>