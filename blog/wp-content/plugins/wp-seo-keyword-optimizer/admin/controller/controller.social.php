<?php
if (!defined('ABSPATH')) exit;

class WSKO_Controller_Social extends WSKO_Controller
{
	//Options
	public $uses_timespan = true;
	public $uses_fixed_timespan = true;
	public $icon = "share-alt";
	public $link = "social";
	
	public $ajax_actions = array('table_social');
	
	public $template_main = "admin/templates/social/frame-social.php";
	
	public $subpages = array(
		//'facebook' => array('title' => 'Facebook', 'template' => 'admin/templates/social/page-facebook.php'),
		'twitter' => array('title' => 'Twitter', 'template' => 'admin/templates/social/page-twitter.php'),
		'snippets' => array('title' => 'Social Snippets', 'template' => 'admin/templates/social/page-snippets.php'),
		'analysis' => array('title' => 'Snippet Analysis', 'template' => 'admin/templates/social/page-snippet-analysis.php'),
	);
	
	public function get_title()
	{
		return __('Social', 'wsko');
	}
	
	public function get_subpage_title($subpage)
	{
		switch($subpage)
		{
			case 'facebook': return __('Facebook', 'wsko');
			case 'twitter': return __('Twitter', 'wsko');
			case 'snippets': return __('Social Snippets', 'wsko');
			case 'analysis': return __('Snippet Analysis', 'wsko');
		}
		return '';
	}
	
	public function load_lazy_page_data($lazy_data)
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$notif = "";
		$data = array();
		
		$page = $this->get_current_subpage();
		if ($page)
		{
			global $wpdb;	
			$tmp_fail = WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true);		
			switch ($page)
			{
				case 'facebook':
				if (WSKO_Class_Social::get_social_logged_in('facebook'))
				{
					$facebook_data = $this->get_cached_var('facebook_data');
					if ($facebook_data)
					{
						$data['total_likes'] = $facebook_data['yesterday']['follower'];
						$data['total_reach'] = $facebook_data['yesterday']['reach'];
						$data['total_views'] = $facebook_data['yesterday']['views'];
						$data['total_engagement'] = $facebook_data['yesterday']['engagement'];
						
						if ($facebook_data['history_follower'])
							$data['chart_history_follower'] = WSKO_Class_Template::render_chart('area', array('Date', 'Follower'), $facebook_data['history_follower'], array(), true);
						else
							$data['chart_history_follower'] = $tmp_fail;
						if ($facebook_data['history_follower'])
							$data['chart_history_reach'] = WSKO_Class_Template::render_chart('area', array('Date', 'Reach'), $facebook_data['history_reach'], array(), true);
						else
							$data['chart_history_reach'] = $tmp_fail;
						if ($facebook_data['history_follower'])
							$data['chart_history_views'] = WSKO_Class_Template::render_chart('area', array('Date', 'Views'), $facebook_data['history_views'], array(), true);
						else
							$data['chart_history_views'] = $tmp_fail;
						if ($facebook_data['history_follower'])
							$data['chart_history_engagement'] = WSKO_Class_Template::render_chart('area', array('Date', 'Engagement'), $facebook_data['history_engagement'], array(), true);
						else
							$data['chart_history_engagement'] = $tmp_fail;
						
						$current_posts = WSKO_Class_Social::get_current_facebook_posts();
						$total_posts = 0;
						$avg_post_likes = 0;
						$avg_post_engagement = 0;
						$avg_post_reach = 0;
						if ($current_posts)
						{
							foreach($current_posts as $p)
							{
								$total_posts++;
								$avg_post_likes += $p->follower;
								$avg_post_engagement += $p->engagement;
								$avg_post_reach += $p->reach;
							}
						}
						$data['total_posts'] = $total_posts;
						if ($total_posts)
						{
							$data['avg_post_likes'] = $avg_post_likes ? round($avg_post_likes/$total_posts, 2) : 0;
							$data['avg_post_engagement'] = $avg_post_engagement ? round($avg_post_engagement/$total_posts, 2) : 0;
							$data['avg_post_reach'] = $avg_post_reach ? round($avg_post_reach/$total_posts, 2) : 0;
						}
						else
						{
							$data['avg_post_likes'] =  "-";
							$data['avg_post_engagement'] = "-";
							$data['avg_post_reach'] = "-";
						}
					}
					else
					{
						$data['total_likes'] = "-";
						$data['total_reach'] = "-";
						$data['total_views'] = "-";
						$data['total_engagement'] = "-";
						
						$data['chart_history_follower'] = $tmp_fail;
						$data['chart_history_reach'] = $tmp_fail;
						$data['chart_history_views'] = $tmp_fail;
						$data['chart_history_engagement'] = $tmp_fail;
						
						$data['total_posts'] = "-";
						$data['avg_post_likes'] =  "-";
						$data['avg_post_engagement'] = "-";
						$data['avg_post_reach'] = "-";
					}
				}
				break;
				case 'twitter':
				if (!WSKO_Class_Social::check_twitter_access())
				{
					$twitter_data = $this->get_cached_var('twitter_data');
					if ($twitter_data)
					{
						$data['total_follower'] = $twitter_data['yesterday']['follower'];
						
						if ($twitter_data['history_follower'])
							$data['chart_history_follower'] = WSKO_Class_Template::render_chart('area', array('Date', 'Follower'), $twitter_data['history_follower'], array(), true);
						else
							$data['chart_history_follower'] = $tmp_fail;
						
						$current_posts = WSKO_Class_Social::get_current_twitter_posts();
						$total_posts = 0;
						$total_post_likes = 0;
						$avg_post_engagement = 0;
						foreach($current_posts as $p)
						{
							$total_posts++;
							$total_post_likes += $p->follower;
							$avg_post_engagement += $p->engagement;
							
						}
						$data['total_post_likes'] = $total_posts;
						if ($total_posts)
						{
							$data['total_post_engagement'] = $avg_post_engagement;
							$data['total_post_likes'] = $total_post_likes;
							$data['avg_post_likes'] = $total_post_likes ? round($total_post_likes/$total_posts, 2) : 0;
							$data['avg_post_engagement'] = $avg_post_engagement ? round($avg_post_engagement/$total_posts, 2) : 0;
						}
						else
						{
							$data['total_post_engagement'] = '-';
							$data['total_post_likes'] = '-';
							$data['avg_post_likes'] = '-';
							$data['avg_post_engagement'] = '-';
						}
					}
					else
					{
						$data['total_follower'] = "-";
						$data['total_posts'] = "-";
						
						$tmp_fail = WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true);
						$data['chart_history_follower'] = $tmp_fail;
						
						$data['total_post_likes'] =  "-";
						$data['total_post_engagement'] = "-";
						$data['avg_post_likes'] =  "-";
						$data['avg_post_engagement'] = "-";
					}
				}
				break;
				case 'snippets':
				if (WSKO_Class_Onpage::seo_plugins_disabled())
				{
					$post_types = WSKO_Class_Helper::get_public_post_types('objects');
					$taxonomies = get_taxonomies(array('public' => true), 'objects');
					foreach ($post_types as $type)
					{
						ob_start();
						/* ?><ul class="nav nav-tabs nav-justified"><li class="active"><a href="#wsko_metas_post_type_<?=$type->name?>_single" data-toggle="tab">Single Page</a></li><li><a href="#wsko_metas_post_type_<?=$type->name?>_archive" data-toggle="tab">Archive</a></li></ul> 
						<div class="tab-content"><div id="wsko_metas_post_type_<?=$type->name?>_single" class="tab-pane fade in active">*/ ?><?php 
							WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_type' => $type->name, 'type' => 'post_type', 'meta_view' => 'social'));
						/* ?></div><div id="wsko_metas_post_type_<?=$type->name?>_archive" class="tab-pane fade"><?php
							WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_type' => $type->name, 'type' => 'post_archive', 'meta_view' => 'social'));
						?></div></div><?php */
						$data['post_type_'.$type->name] = ob_get_clean();
					}
					foreach ($taxonomies as $tax)
					{
						$data['post_tax_'.$tax->name] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_tax' => $tax->name, 'type' => 'post_tax', 'meta_view' => 'social'), true);
					}
					
					//$data['other_home'] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('arg' => 'home', 'type' => 'other', 'meta_view' => 'social'), true);
					//$data['other_blog'] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('arg' => 'blog', 'type' => 'other', 'meta_view' => 'social'), true);
				}
				break;
			}
			return array(
					'success' => true,
					'data' => $data,
					'notif' => $notif
				);
		}
	}
	
	public function action_table_social()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$data = array();
		$arg = isset($_POST['arg']) ? sanitize_text_field($_POST['arg']) : false;
		$order = isset($_POST['order']) ? $_POST['order'] : false;
		$offset = isset($_POST['start']) ? intval($_POST['start']) : 0;
		$count = isset($_POST['length']) ? intval($_POST['length']) : 10;
		$search = isset($_POST['search']['value']) ? $_POST['search']['value'] : false;
		$custom_filter = isset($_POST['custom_filter']) ? $_POST['custom_filter'] : false;
		if ($offset < 0)
			$offset = 0;
		if ($count < 1)
			$count = 1;
		if ($order && $order[0])
		{
			if ($order[0]['dir'] == "asc")
				$orderdir = 1;
			else
				$orderdir = 0;
			$order = $order[0]['column'];
		}
		else
		{
			$order = 0;
			$orderdir = 1;
		}
		switch ($arg)
		{
			case '1':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'og_title_length', 'og_desc_length', 'og_img_provided', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => 'url', '4' => function($arg){return $arg == 1 ? 'set' : 'not set';}, '5' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'social'), true); })), false);
			break;
			case '2':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'tw_title_length', 'tw_desc_length', 'tw_img_provided', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => 'url', '4' => function($arg){return $arg == 1 ? 'set' : 'not set';}, '5' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'social'), true); })), false);
			break;
			case '3':
				$current_posts = WSKO_Class_Social::get_current_facebook_posts();
				$data = WSKO_Class_Helper::prepare_data_table($current_posts, array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('data', 'follower', 'engagement', 'reach', 'created', 'data'), 'format' => array('4' => 'datetime', '0' => function($v){ $v = json_decode($v, true); return $v ? ($v['img'] ? '<img src="'.$v['img'].'"/>' : '').(isset($v['content'])?$v['content']:'') : ''; }, '5' => function($v){ $v = json_decode($v, true); $link = WSKO_Class_Social::get_fb_post_link($v['id']); if (!$link) return ""; return '<a href="'.$link.'" target="_blank"><i class="fa fa-eye"></i></a>'; })), false);
			break;
			case '4':
				$current_posts = WSKO_Class_Social::get_current_twitter_posts();
				$data = WSKO_Class_Helper::prepare_data_table($current_posts, array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('data', 'follower', 'engagement', 'created', 'data'), 'format' => array('3' => 'datetime', '0' => function($v){ $v = json_decode($v, true); return $v ? (isset($v['content'])?$v['content']:'').($v['urls'] ? '(Urls: '.implode(',',$v['urls']).')' : '') : ''; }, '4' => function($v){ $v = json_decode($v, true); $link = WSKO_Class_Social::get_tw_post_link($v['id']); if (!$link) return ""; return '<a href="'.$link.'" target="_blank"><i class="fa fa-eye"></i></a>'; })), false);
			break;
			case '5':
			$post_type = 'any';
			if ($custom_filter)
			{
				foreach ($custom_filter as $cf)
				{
					if ($cf['key'] == 'post_type')
						$post_type = $cf['val'];
				}
			}
			$query = new WP_Query(array('s' => $search, 'post_type' => $post_type, 'posts_per_page' => $count, 'offset' => $offset, 'fields' => 'ids'));
			$res = array();
			foreach ($query->posts as $p)
			{
				$url = get_permalink($p);
				$t = get_the_title($p);
				$res[] = array(
					WSKO_Class_Template::render_url_post_field($p, array(), true),
					//'<div class="wsko_nowrap" title="'.$url.'"><p style="float:right">'.WSKO_Class_Template::render_content_optimizer_link($p, array('open_tab' => 'social'), true).'</p><span><p>'.($t?$t:WSKO_Class_Helper::get_empty_page_title()).'</p><a class="font-unimportant" href="'.$url.'">'.$url.'</a></span></div>',
					WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_id' => $p, 'type' => 'post_id', 'meta_view' => 'social', 'collapse' => true), true)
				);
			}
			$data = array('data' => $res, 'filtered' => $query->found_posts, 'total' => $query->found_posts);
			break;
		}
		return array(
				'success' => true,
				'data' => $data ? $data['data'] : array(),
				'recordsFiltered' => $data ? $data['filtered'] : array(),
				'recordsTotal' => $data ? $data['total'] : array(),
			);
	}
	
	public function get_cached_facebook_data($args)
	{
		$res = array();
		$fb_data = WSKO_Class_Social::get_facebook_data();
		
		if ($fb_data)
			$res['yesterday'] = array('follower' => $fb_data->follower, 'reach' => $fb_data->reach, 'views' => $fb_data->views, 'engagement' => $fb_data->engagement);
		$facebook_history = WSKO_Class_Social::get_facebook_history($this->timespan_start, $this->timespan_end);
		if ($facebook_history)
		{
			$res['history_follower'] = array();
			$res['history_reach'] = array();
			$res['history_views'] = array();
			$res['history_engagement'] = array();
			foreach ($facebook_history as $h)
			{
				$res['history_follower'][] = array($h->time, $h->follower);
				$res['history_reach'][] = array($h->time, $h->reach);
				$res['history_views'][] = array($h->time, $h->views);
				$res['history_engagement'][] = array($h->time, $h->engagement);
			}
		}		
		return $res;
	}
	
	public function get_cached_twitter_data($args)
	{
		$res = array();
		$tw_data = WSKO_Class_Social::get_twitter_data();
		if ($tw_data)
			$res['yesterday'] = array('follower' => $tw_data->follower, 'reach' => $tw_data->reach, 'views' => $tw_data->views, 'engagement' => $tw_data->engagement);
		$twitter_history = WSKO_Class_Social::get_twitter_history($this->timespan_start, $this->timespan_end);
		if ($twitter_history)
		{
			$res['history_follower'] = array();
			$res['history_reach'] = array();
			$res['history_views'] = array();
			$res['history_engagement'] = array();
			foreach ($twitter_history as $h)
			{
				$res['history_follower'][] = array($h->time, $h->follower);
				$res['history_reach'][] = array($h->time, $h->reach);
				$res['history_views'][] = array($h->time, $h->views);
				$res['history_engagement'][] = array($h->time, $h->engagement);
			}
		}
		return $res;
	}
	
	public function get_cached_graph_data($args)
	{
		$res = array('facebook_history' => array(), 'twitter_history' => array());
		$facebook_history = WSKO_Class_Social::get_facebook_history($this->timespan_start, $this->timespan_end);
		foreach ($facebook_history as $h)
		{
			$res['facebook_history'][] = array($h->time, $h->follower);
		}
		
		$twitter_history = WSKO_Class_Social::get_twitter_history($this->timespan_start, $this->timespan_end);
		foreach ($twitter_history as $h)
		{
			$res['twitter_history'][] = array($h->time, $h->follower);
		}
		return $res;
	}
	
	//Singleton
	static $instance;
}
?>