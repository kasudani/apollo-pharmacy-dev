<?php
if (!defined('ABSPATH')) exit;

class WSKO_Controller_Setup extends WSKO_Controller
{
	public $link = "setup";
	public $styles = array('admin/css/setup.css');
	
	public $template_main = 'admin/templates/setup/frame-setup.php';
	
	public $ajax_actions = array('finish_setup');

	public function get_title()
	{
		return __('Setup', 'wsko');
	}
	
	public function redirect()
	{
		$action = isset($_GET['action']) ? $_GET['action'] : '';
		if ($action == 'recieve_facebook_token')
		{
			if (isset($_GET['access_token']) && $_GET['access_token'])
			{
				$long_token = WSKO_Class_Social::get_facebook_long_token($_GET['access_token']);
				if ($long_token)
					WSKO_Class_Social::set_social_token('facebook', $long_token, true);
				$this::call_redirect();
			}
		}
	}
	
	public function load_lazy_page_data()
	{
		return true;
	}
	
	public function action_finish_setup()
	{
		if (!$this->can_execute_action())
			return false;
		if (WSKO_Class_Core::set_configured())
		{
			WSKO_Class_Core::setup();
			return array(
					'success' => true,
					'redirect' => WSKO_Controller_Dashboard::get_link()
				);
		}
		return array(
				'success' => false,
				'msg' => 'Setup could not be finished'
			);
	}
	
	//Singleton
	static $instance;
}
?>