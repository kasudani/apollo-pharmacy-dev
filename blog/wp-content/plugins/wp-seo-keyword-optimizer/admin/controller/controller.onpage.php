<?php
if (!defined('ABSPATH')) exit;

class WSKO_Controller_Onpage extends WSKO_Controller
{
	//Options
	public $icon = "code";
	public $link = "onpage";
	public $scripts = array('admin/js/onpage.js');
	
	public $template_main = "admin/templates/onpage/frame-onpage.php";
	public $template_notifications = 'admin/templates/onpage/notifications-onpage.php';
	
	public $ajax_actions = array('table_onpage', 'refresh_analysis', 'set_metas', 'remove_auto_redirect', 'save_robots', 'save_htaccess', 'add_redirect', 'check_redirect', 'update_automatic_redirect', 'remove_redirect', 'remove_page_redirect', 'update_sitemap', 'upload_sitemap');
	
	public $subpages = array(
		'analysis' => array('template' => 'admin/templates/onpage/page-analysis.php'),
		'metas' => array('template' => 'admin/templates/onpage/page-metas.php'),
		'links' => array('template' => 'admin/templates/onpage/page-links.php'),
		'redirects' => array('template' => 'admin/templates/onpage/page-redirects.php'),
		'sitemap' => array('template' => 'admin/templates/onpage/page-sitemap.php'),
		'robots' => array('template' => 'admin/templates/onpage/page-robots.php'),
	);
	
	public function get_title()
	{
		return __('Onpage', 'wsko');
	}
	
	public function get_subpage_title($subpage)
	{
		switch($subpage)
		{
			case 'analysis': return __('Onpage Analysis', 'wsko');
			case 'metas': return __('Metas', 'wsko');
			case 'links': return __('Permalinks', 'wsko');
			case 'redirects': return __('Redirect Manager', 'wsko');
			case 'sitemap': return __('Sitemap', 'wsko');
			case 'robots': return __('.htaccess & robots.txt', 'wsko');
		}
		return '';
	}
	
	public function load_lazy_page_data($lazy_data)
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$notif = "";
		$data = array();
		
		$page = $this->get_current_subpage();
		if ($page)
		{
			global $wpdb;
			$invalid = !WSKO_Class_Onpage::seo_plugins_disabled();
			
			switch ($page)
			{
				case 'metas':
				if (!$invalid)
				{
					$post_types = WSKO_Class_Helper::get_public_post_types('objects');
					$taxonomies = get_taxonomies(array('public' => true), 'objects');
					foreach ($post_types as $type)
					{
						$data['post_type_'.$type->name] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_type' => $type->name, 'type' => 'post_type', 'meta_view' => 'metas'), true);
					}
					foreach ($taxonomies as $tax)
					{
						$data['post_tax_'.$tax->name] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_tax' => $tax->name, 'type' => 'post_tax', 'meta_view' => 'metas'), true);
					}
					//$data['other_home'] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('arg' => 'home', 'type' => 'other', 'meta_view' => 'metas'), true);
					//$data['other_blog'] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('arg' => 'blog', 'type' => 'other', 'meta_view' => 'metas'), true);
				}
				break;
				case 'links':
				if (!$invalid)
				{
					$post_types = WSKO_Class_Helper::get_public_post_types('objects');
					unset($post_types['post']);
					unset($post_types['page']);
					$taxonomies = get_taxonomies(array('public' => true), 'objects');
					foreach ($post_types as $type)
					{
						$data['post_type_'.$type->name] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_type' => $type->name, 'type' => 'post_type', 'meta_view' => 'links'), true);
					}
					foreach ($taxonomies as $tax)
					{
						$data['post_tax_'.$tax->name] = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_tax' => $tax->name, 'type' => 'post_tax', 'meta_view' => 'links'), true);
					}
				}
				break;
				case 'redirects':
				$redirects = WSKO_Class_Onpage::get_redirects();
				$page_redirects = WSKO_Class_Onpage::get_page_redirects();
				$auto_post_redirects = WSKO_Class_Onpage::get_auto_redirects('post_id');
				$auto_post_type_redirects = WSKO_Class_Onpage::get_auto_redirects('post_type');
				
				$table_redirects = array();
				$table_page_redirects = array();
				$table_auto_page_redirects = array();
				$table_auto_pt_redirects = array();
				$is_admin = current_user_can('manage_options');
				if ($redirects)
				{
					foreach ($redirects as $k => $r)
					{
						$table_redirects[] = array(
							array('value' => $r['comp']),
							array('value' => $r['page']),
							array('value' => $r['type'] == 2 ? 302 : 301),
							array('value' => $r['comp_to']),
							array('value' => $r['target'].'<br/><p class="font-unimportant">'.WSKO_Class_Helper::format_url($r['target']).'</p>'),
							array('value' => $is_admin ? WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_redirect', array('redirects' => $k), array(), true) : ''),
						);
					}
				}

				if ($page_redirects)
				{
					foreach ($page_redirects as $k => $r)
					{
						$table_page_redirects[] = array(
							array('value' => WSKO_Class_Template::render_url_post_field($r['post'], array(), true)),
							array('value' => $r['type'] == 2 ? 302 : 301),
							array('value' => $r['to'].'<br/><p class="font-unimportant">'.home_url($r['to']).'</p>'),
							array('value' => $is_admin ? WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_page_redirect', array('post' => $r['post']), array(), true) : ''),
						);
					}
				}

				if ($auto_post_redirects)
				{
					foreach ($auto_post_redirects as $p => $links)
					{
						$table_auto_page_redirects[] = array(
							array('value' => WSKO_Class_Template::render_url_post_field($p, array(), true)),
							array('order' => count($links), 'value' => count($links).' redirect(s) from<br/><ul><li><i>'.implode('</i></li><li><i>',$links).'</li></ul>'),
							array('value' => $is_admin ? WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_auto_redirect', array('type' => 'post_id', 'arg' => $p), array(), true) : ''),
						);
					}
				}
				
				if ($auto_post_type_redirects)
				{
					foreach ($auto_post_type_redirects as $pt => $redirects)
					{
						$red_temp = "";
						$sources = $redirects['source'];
						if ($sources)
						{
							foreach ($sources as $slug => $link_snapshot)
							{
								$red_temp .= 'From: '.$slug.' ('.count($link_snapshot).')'.($is_admin ? WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_auto_redirect', array('type' => 'post_type', 'arg' => $pt, 'key' => $slug), array('no_button' => true), true) : '').'<br/>';
							}
						}
						if ($red_temp)
						{
							$table_auto_pt_redirects[] = array(
								array('value' => $pt),
								array('value' => $red_temp),
								array('value' => $is_admin ? WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_auto_redirect', array('type' => 'post_type', 'arg' => $pt), array(), true) : '')
							);
						}
					}
				}
				
				$data['redirects'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-redirects.php', array('redirects' => $table_redirects, 'page_redirects' => $table_page_redirects, 'auto_post_type_redirects' => $table_auto_pt_redirects, 'atuo_post_redirects' => $table_auto_page_redirects), true);
				break;
				case 'analysis':
				if (!$invalid)
				{
					$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
					if ($global_analysis)
					{
						$data['analysis_overview'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-overview.php', array('analysis' => $global_analysis), true);
						$data['analysis_titles'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-titles.php', array('analysis' => $global_analysis), true);
						$data['analysis_descriptions'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-descriptions.php', array('analysis' => $global_analysis), true);
						$data['analysis_headings'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-headings.php', array('analysis' => $global_analysis), true);
						$data['analysis_content'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-content.php', array('analysis' => $global_analysis), true);
						$data['analysis_keywords'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-keywords.php', array('analysis' => $global_analysis), true);
						$data['analysis_links'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-links.php', array('analysis' => $global_analysis), true);
						//$data['analysis_index'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-index.php', array('analysis' => $global_analysis), true);
						//$data['analysis_resources'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-resources.php', array('analysis' => $global_analysis), true);
						//$data['analysis_canon'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-canon.php', array('analysis' => $global_analysis), true);
						//$data['analysis_robots'] = WSKO_Class_Template::render_template('admin/templates/onpage/view-analysis-robots.php', array('analysis' => $global_analysis), true);
					}
				}
				break;
			}
			return array(
					'success' => true,
					'data' => $data,
					'notif' => $notif
				);
		}
	}
	
	public function action_table_onpage()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$data = array();
		$arg = isset($_POST['arg']) ? sanitize_text_field($_POST['arg']) : false;
		$order = isset($_POST['order']) ? $_POST['order'] : false;
		$offset = isset($_POST['start']) ? intval($_POST['start']) : 0;
		$count = isset($_POST['length']) ? intval($_POST['length']) : 10;
		$search = isset($_POST['search']['value']) ? $_POST['search']['value'] : false;
		$custom_filter = isset($_POST['custom_filter']) ? $_POST['custom_filter'] : false;
		if ($offset < 0)
			$offset = 0;
		if ($count < 1)
			$count = 1;
		if ($order && $order[0])
		{
			if ($order[0]['dir'] == "asc")
				$orderdir = 1;
			else
				$orderdir = 0;
			$order = $order[0]['column'];
		}
		else
		{
			$order = 0;
			$orderdir = 1;
		}
		switch ($arg)
		{
			case '1':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'title_length', 'title_duplicate_posts', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ $op_report = WSKO_Class_Onpage::get_onpage_analysis(); return WSKO_Class_Template::render_post_dirty_icon($r[4], array(), true).WSKO_Class_Template::render_url_post_field($r[4], array(), true).'<br/><p class="wsko-onpage-meta-title"><b>Title:</b> '.esc_html($op_report['current_report']['pages'][$arg]['title']).'</p>'; }, '3' => function($arg) { $count = count($arg); return $count ? WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($arg), $count.' <i class="fa fa-eye fa-fw text-off"></i>', array('msg' => 'Title Duplicates'), true) : '0'; }, '4' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'metas'), true); })), false);
			break;
			case '2':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'desc_length', 'desc_duplicate_posts', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_post_dirty_icon($r[4], array(), true).WSKO_Class_Template::render_url_post_field($r[4], array(), true); }, '3' => function($arg) { $count = count($arg); return $count ? WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($arg), $count.' <i class="fa fa-eye fa-fw text-off"></i>', array('msg' => 'Description Duplicates'), true) : '0'; }, '4' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'metas'), true); })), false);
			break;
			case '3':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'count_h1', 'count_h2', 'count_h3', 'count_h4', 'count_h5', 'count_h6', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_post_dirty_icon($r[8], array(), true).WSKO_Class_Template::render_url_post_field($r[8], array(), true); }, '8' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'content'), true); })), false);
			break;
			case '4':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', /*'content_length',*/ 'word_count', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_post_dirty_icon($r[3], array(), true).WSKO_Class_Template::render_url_post_field($r[3], array(), true); }, '3' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'content'), true); })), false);
			break;
			case'5':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'prio1_kw_den', 'post_id'/*, 'prio2_kw_den', 'prio3_kw_den'*/), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_post_dirty_icon($r[3], array(), true).WSKO_Class_Template::render_url_post_field($r[3], array(), true); }, '3' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'keywords'), true); })), false);
			break;
			case'6':
				$sitemap = $this->get_cached_var('sitemap');
				$data = WSKO_Class_Helper::prepare_data_table($sitemap, array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'format' => array('0' => 'url')), false);
			break;
			case '7':
			$post_type = 'any';
			if ($custom_filter)
			{
				foreach ($custom_filter as $cf)
				{
					if ($cf['key'] == 'post_type')
						$post_type = $cf['val'];
				}
			}
			$query = new WP_Query(array('s' => $search, 'post_type' => $post_type, 'posts_per_page' => $count, 'offset' => $offset, 'fields' => 'ids'));
			$res = array();
			foreach ($query->posts as $p)
			{
				$url = get_permalink($p);
				$t = get_the_title($p);
				$res[] = array(
					WSKO_Class_Template::render_url_post_field($p, array(), true),
					//'<div class="wsko_nowrap" title="'.$url.'"><p style="float:right">'.WSKO_Class_Template::render_content_optimizer_link($p, array(), true).'</p><span><p>'.($t?$t:WSKO_Class_Helper::get_empty_page_title()).'</p><a class="font-unimportant" href="'.$url.'">'.$url.'</a></span></div>',
					WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_id' => $p, 'type' => 'post_id', 'meta_view' => 'metas', 'collapse' => true), true),
				);
			}
			$data = array('data' => $res, 'filtered' => $query->found_posts, 'total' => $query->found_posts);
			break;
			case '8':
				if (!$custom_filter)
					$custom_filter = array();
				$custom_filter[] = array('key' => 'title_duplicates', 'val' => 0, 'comp' => 'gt');
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'title_length', 'title_duplicate_posts', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ $op_report = WSKO_Class_Onpage::get_onpage_analysis(); return WSKO_Class_Template::render_post_dirty_icon($r[4], array(), true).WSKO_Class_Template::render_url_post_field($r[4], array(), true).'<br/><p class="wsko-onpage-meta-title"><b>Title:</b> '.esc_html($op_report['current_report']['pages'][$arg]['title']).'</p>'; }, '3' => function($arg) { $count = count($arg); return $count ? WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($arg), $count.' <i class="fa fa-eye fa-fw text-off"></i>', array('msg' => 'Title Duplicates'), true) : '0'; }, '4' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'metas'), true); })), false);
			break;
			case '9':
				if (!$custom_filter)
					$custom_filter = array();
				$custom_filter[] = array('key' => 'desc_duplicates', 'val' => 0, 'comp' => 'gt');
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'desc_length', 'desc_duplicate_posts', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_post_dirty_icon($r[4], array(), true).WSKO_Class_Template::render_url_post_field($r[4], array(), true); }, '3' => function($arg) { $count = count($arg); return $count ? WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($arg), $count.' <i class="fa fa-eye fa-fw text-off"></i>', array('msg' => 'Description Duplicates'), true) : '0'; }, '4' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array('open_tab' => 'metas'), true); })), false);
			break;
			case '10':
				$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
				$data = WSKO_Class_Helper::prepare_data_table($global_analysis['current_report']['pages'], array('offset' => $offset, 'count' => $count), $custom_filter, array('key' => $order, 'dir' => $orderdir), array('search' => $search, 'specific_keys' => array('onpage_score', 'url', 'url_length', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_post_dirty_icon($r[3], array(), true).WSKO_Class_Template::render_url_post_field($r[3], array(), true); }, '3' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array(), true); })), false);
			break;
			case '11':
			$post_type = 'any';
			if ($custom_filter)
			{
				foreach ($custom_filter as $cf)
				{
					if ($cf['key'] == 'post_type')
						$post_type = $cf['val'];
				}
			}
			$query = new WP_Query(array('s' => $search, 'post_type' => $post_type, 'posts_per_page' => $count, 'offset' => $offset, 'fields' => 'ids'));
			$res = array();
			foreach ($query->posts as $p)
			{
				$url = get_permalink($p);
				$t = get_the_title($p);
				$res[] = array(
					WSKO_Class_Template::render_url_post_field($p, array(), true),
					//'<div class="wsko_nowrap" title="'.$url.'"><p style="float:right">'.WSKO_Class_Template::render_content_optimizer_link($p, array(), true).'</p><span><p>'.($t?$t:WSKO_Class_Helper::get_empty_page_title()).'</p><a class="font-unimportant" href="'.$url.'">'.$url.'</a></span></div>',
					WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('post_id' => $p, 'type' => 'post_id', 'meta_view' => 'links', 'collapse' => true), true),
				);
			}
			$data = array('data' => $res, 'filtered' => $query->found_posts, 'total' => $query->found_posts);
			break;
		}
		return array(
				'success' => true,
				'data' => $data ? $data['data'] : array(),
				'recordsFiltered' => $data ? $data['filtered'] : array(),
				'recordsTotal' => $data ? $data['total'] : array(),
			);
	}
	
	public function action_refresh_analysis()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		WSKO_Class_Crons::bind_onpage_analysis();
		
		return true;
	}
	
	public function action_set_metas()
	{
		if (!$this->can_execute_action(false))
			return false;
		
		$data = false;
		$data_str = isset($_POST['data']) ? $_POST['data'] : false;
		if ($data_str)
			parse_str($data_str, $data);
		if (isset($data['robots_ni']) || isset($data['robots_nf']))
		{
			$robots_ni = isset($data['robots_ni']) && $data['robots_ni'] && $data['robots_ni'] != 'false' ? true : false;
			$robots_nf = isset($data['robots_nf']) && $data['robots_nf'] && $data['robots_nf'] != 'false' ? true : false;
			unset($data['robots_ni']);
			unset($data['robots_nf']);
			$data['robots'] = $robots_ni ? ($robots_nf ? 3 : 2) : ($robots_nf ? 1 : 0);
		}
		unset($data['hide_slug']); //temporary disabled
		if (isset($data['hide_slug']))
		{
			$data['hide_slug'] = $data['hide_slug'] && $data['hide_slug'] != 'false' ? true : false;
		}
		$meta_view = isset($data['meta_view']) ? sanitize_text_field($data['meta_view']) : false;
		$collapse = isset($data['collapse']) && $data['collapse'] && $data['collapse'] != 'false' ? true : false;
		$type = isset($_POST['type']) ? sanitize_text_field($_POST['type']) : false;
		$arg = isset($_POST['arg']) ? sanitize_text_field($_POST['arg']) : false;
		if ($type && $arg)
		{
			$data = array_map('stripslashes', array_map('sanitize_text_field', $data));
			$data_pre = WSKO_Class_Onpage::get_post_meta($arg, $type);
			
			if (isset($data['url']))
			{
				if ($type === "post_id")
				{
					$post = intval($arg);
					$url = sanitize_title_with_dashes(WSKO_Class_Helper::map_special_chars($data['url']));
					if ($post && $url)
						wp_update_post(array('ID' => $post, 'post_name' => $url));
				}
				else if ($type === "post_type" || $type === "post_tax")
				{
					$data['slug'] = trim($data['url'], ' ');
				}
				unset($data['url']);
			}
			if ($type !== "post_type")
			{
				unset($data['hide_slug']);
			}
			WSKO_Class_Onpage::update_automatic_redirects($type, $arg, isset($data['hide_slug']) && $data['hide_slug'] ? '/' : (isset($data['slug']) && $data['slug'] ? $data['slug'] : false), isset($data['create_redirects']) && $data['create_redirects'] && $data['create_redirects'] !== 'false' ? true : false);
			
			if (isset($data['create_redirects']))
			{
				unset($data['create_redirects']);
			}
			if ($data_pre)
				$data = $data + $data_pre;//array_merge($data_pre, $data);
			if (isset($data['meta_view']))
				unset($data['meta_view']);
			if (isset($data['collapse']))
				unset($data['collapse']);
			if (isset($_POST['reset']) && $_POST['reset'])
				WSKO_Class_Onpage::unset_post_meta($arg, $type);
			else
				WSKO_Class_Onpage::set_post_meta($arg, $data, $type);
			$type_s = $type === 'post_archive' ? 'post_type' : ($type === 'other' ? 'arg' : $type);
			
			WSKO_Class_Helper::refresh_permalinks();
			
			$view = WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array($type_s => $arg, 'type' => $type, 'meta_view' => $meta_view, 'collapse' => $collapse, 'is_collapse_open' => true), true);
		}
		if ($view)
		{
			return array(
					'success' => true,
					'new_view' => $view
				);
		}
	}
	
	public function action_remove_auto_redirect()
	{
		if (!$this->can_execute_action())
			return false;
		
		$type = isset($_POST['type']) ? sanitize_text_field($_POST['type']) : false;
		$arg = isset($_POST['arg']) ? sanitize_text_field($_POST['arg']) : false;
		$key = isset($_POST['key']) ? sanitize_text_field($_POST['key']) : false;
		if ($type && $arg)
			WSKO_Class_Onpage::remove_auto_redirect($type, $arg, $key);
		
		return true;
	}
	
	public function action_save_robots()
	{
		if (!$this->can_execute_action())
			return false;
		if (WSKO_Class_Core::get_setting('activate_editor_robots'))
		{
			$robots_path = get_home_path().'robots.txt';
			$robots_exists = file_exists($robots_path);
			if (($robots_exists && is_writable($robots_path)) || (!$robots_exists && is_writable(get_home_path())))
			{
				global $wsko_plugin_path;
				$robots = isset($_POST['robots']) ? "".$_POST['robots'] : "";
				if ($robots_exists)
					copy($robots_path, $wsko_plugin_path.'backups/robots.txt-'.time().'-'.date('d.m.Y-H:i').'.bak');
				file_put_contents($robots_path, $robots);
				return true;
			}
		}
	}
	
	public function action_save_htaccess()
	{
		if (!$this->can_execute_action())
			return false;
		
		if (WSKO_Class_Core::get_setting('activate_editor_htaccess'))
		{
			$htaccess_path = get_home_path().'.htaccess';
			$htaccess_exists = file_exists($htaccess_path);
			if (($htaccess_exists && is_writable($htaccess_path)) || (!$htaccess_exists && is_writable(get_home_path())))
			{
				global $wsko_plugin_path;
				$htaccess = isset($_POST['htaccess']) ? "".$_POST['htaccess'] : "";
				if ($htaccess_exists)
					copy($htaccess_path, $wsko_plugin_path.'backups/.htaccess-'.time().'-'.date('d.m.Y-H:i').'.bak');
				file_put_contents($htaccess_path, $htaccess);
				return true;
			}
		}
	}
	
	public function action_add_redirect()
	{
		if (!$this->can_execute_action())
			return false;
		
		$comp = isset($_POST['comp']) ? sanitize_text_field($_POST['comp']) : false;
		$comp_to = isset($_POST['comp_to']) ? sanitize_text_field($_POST['comp_to']) : false;
		$type = isset($_POST['type']) ? intval($_POST['type']) : false;
		$redirect_to = isset($_POST['redirect_to']) ? sanitize_text_field($_POST['redirect_to']) : false;
		$page = isset($_POST['page']) ? sanitize_text_field($_POST['page']) : false;
		if ($page && $redirect_to && $comp && $comp_to && $type)
		{
			WSKO_Class_Onpage::add_redirect($page, $comp, $redirect_to, $comp_to, $type);
			return true;
		}
	}
	public function action_check_redirect()
	{
		if (!$this->can_execute_action())
			return false;
		$url = isset($_POST['url']) ? sanitize_text_field($_POST['url']) : false;
		$status_check = isset($_POST['status_check']) && $_POST['status_check'] && $_POST['status_check'] !== 'false' ? true : false;
		$view = false;
		if ($url)
		{
			$view = WSKO_Class_Template::render_template('admin/templates/onpage/view-url-check.php', array('url' => $url, 'status_check' => $status_check), true);
		}
		if ($view)
		{
			return array('success' => true, 'view' => $view);
		}
	}
	public function action_remove_redirect()
	{
		if (!$this->can_execute_action())
			return false;
		
		$redirects = isset($_POST['redirects']) ? $_POST['redirects'] : false;
		if (($redirects || $redirects == '0') && !is_array($redirects))
			$redirects = array($redirects);
		if ($redirects)
		{
			WSKO_Class_Onpage::remove_redirects($redirects);
			return array(
					'success' => true,
					'redirects' => $redirects
				);
		}
	}
	
	public function action_remove_page_redirect()
	{
		if (!$this->can_execute_action())
			return false;
		$post = isset($_POST['post']) ? intval($_POST['post']) : false;
		if ($post)
		{
			$data = WSKO_Class_Onpage::get_technical_seo_data($post);
			unset($data['redirect']);
			WSKO_Class_Onpage::set_technical_seo_data($post, $data);
			return true;
		}
	}
	
	public function action_update_automatic_redirect()
	{
		if (!$this->can_execute_action())
			return false;
		
		$activate = isset($_POST['activate']) && $_POST['activate'] && $_POST['activate'] != 'false' ? true : false;
		$type = isset($_POST['type']) ? intval($_POST['type']) : "";
		$custom = isset($_POST['custom']) ? sanitize_text_field($_POST['custom']) : "";
		if ($type)
		{
			WSKO_Class_Onpage::set_redirect_404($activate, $type, $custom);
			return true;
		}
	}
	
	public function action_change_sitemap()
	{
		if (!$this->can_execute_action())
			return false;
		
		$res = WSKO_Class_Onpage::generate_sitemap();
		if ($res)
		{
			return true;
		}
	}
	
	public function action_update_sitemap()
	{
		if (!$this->can_execute_action())
			return false;
		
		$onpage_data = WSKO_Class_Onpage::get_onpage_data();
		$types = array();
		if (isset($_POST['types']) && is_array($_POST['types']) && $_POST['types'])
		{
			foreach($_POST['types'] as $type)
			{
				$types[sanitize_text_field($type['name'])] = array('freq' => sanitize_text_field($type['freq']), 'prio' =>  floatval(str_replace(',', '.', $type['prio'])));
			}
		}
		$stati = array();
		if (isset($_POST['stati']) && is_array($_POST['stati']) && $_POST['stati'])
		{
			$stati = array_map('sanitize_text_field', $_POST['stati']);
		}
		$gen_params = WSKO_Class_Onpage::get_sitemap_params();
		$gen_params['types'] = $types;
		$gen_params['stati'] = $stati;
		WSKO_Class_Onpage::set_sitemap_params($gen_params);
		if (isset($_POST['ping']) && $_POST['ping'] && $_POST['ping'] != 'false')
		{
			WSKO_Class_Core::save_setting('automatic_sitemap_ping', true);
		}
		else
		{
			WSKO_Class_Core::save_setting('automatic_sitemap_ping', false);
		}
		if (isset($_POST['auto_generation']) && $_POST['auto_generation'] && $_POST['auto_generation'] != 'false')
		{
			WSKO_Class_Core::save_setting('automatic_sitemap', true);
			WSKO_Class_Crons::bind_sitemap_generation();
		}
		else
		{
			WSKO_Class_Core::save_setting('automatic_sitemap', false);
			WSKO_Class_Crons::unbind_sitemap_generation();
		}
		$res = WSKO_Class_Onpage::generate_sitemap();
		if ($res)
		{
			return true;
		}
	}
	
	public function action_upload_sitemap()
	{
		if (!$this->can_execute_action())
			return false;
		
		$res = WSKO_Class_Onpage::upload_sitemap();
		if ($res)
		{
			return true;
		}
	}
	
	public function get_cached_sitemap($args)
	{
		$res = array();
		$sitemap_real_path = get_home_path().'sitemap_bst.xml';
		if (file_exists($sitemap_real_path))
		{
			$xml = simplexml_load_file($sitemap_real_path);
			if ($xml)
			{
				$childs = $xml->children();
				foreach($childs as $child)
				{
					$res[] = array('page' => $child->loc->__toString(), 'last_modified' => strtotime($child->lastmod->__toString()), 'freq' => $child->changefreq ? $child->changefreq->__toString() : '', 'prio' => $child->priority ? $child->priority->__toString() : '0.5 (default)');
				}
			}
		}
		return $res;
	}
	
	//Singleton
	static $instance;
}
?>