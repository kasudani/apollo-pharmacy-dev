<?php
if (!defined('ABSPATH')) exit;


$current_user = wp_get_current_user();
$current_user_mail = $current_user->user_email;
$current_user_name = $current_user->user_firstname . ' ' . $current_user->user_lastname;
?>
<div class="modal fade wsko_modal" id="wsko_modal_rating">
  <div class="modal-dialog">
    <div class="modal-content">
	  <button type="button" class="close m5" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	  <form id="wsko_feedback_form" class="wsko-form-feedback" data-nonce="<?=wp_create_nonce('wsko-feedback')?>">
	    <div class="modal-body">
		    <h4 class="modal-title">Rate BAVOKO SEO Tools</h4>
			<p>How likely are you to share BAVOKO SEO Tools with your friends and colleagues?</p>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-3">
						<label>Rate</label>
					</div>
					<div class="col-sm-9">
						<input placeholder="" class="form-control" type="text" value="" name="name">
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label>Your Feedback</label>
					</div>
					<div class="col-sm-9">
						<textarea placeholder="" class="form-control" type="text" value="" name="name"></textarea>
					</div>
					<div class="clearfix"></div>
				</div>				
			</div>
		</div>
	  </form>
	  
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 