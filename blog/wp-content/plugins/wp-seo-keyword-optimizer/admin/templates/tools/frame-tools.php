<?php
if (!defined('ABSPATH')) exit;

?>
<div class="wsko-col-sm-3 wsko-col-xs-12">
	<p class="panel-heading m0"><?=__('Test Redirect Rules & Path', 'wsko');?><?php WSKO_Class_Template::render_infoTooltip("Check your page's URLs for matching redirect rules and real redirects from your system.", 'info') ?></a></p>
	<small class="wsko-text-off">Get HTTP status code redirects and current redirect rules for this post/page</small>
</div>
<div class="wsko-col-sm-9 wsko-col-xs-12">
	<div id="test_redirects" class="wsko-co-redirect-test">
		<form id="wsko_check_redirect_form" data-nonce="<?=wp_create_nonce('wsko_check_redirect')?>">
			<div class="form-group">
				<input type="text" class="form-control wsko-field-url" placeholder="/link/" value="<?=home_url('/')?>" required>
				<label><input type="checkbox" class="form-control wsko-field-status-check"> Get real HTTP status code redirects (can take a while)</label>
				<input type="submit" class="btn btn-primary">
			</div>
			<div id="wsko_check_redirect_results">
			</div>
		</form>
	</div>	
</div>