<?php
if (!defined('ABSPATH')) exit;
?>
<div class="row">
<?php

if (WSKO_Class_Social::get_social_logged_in('facebook'))
{
	?>
	<div class="wsko-section clearfix">
	<p class="panel-heading pl30"><?=__('Facebook Page', 'wsko');?> <a href="<?=WSKO_Class_Social::get_fb_page_link()?>" target="_blank"><i class="fa fa-external-link fa-fw"></i></a></p>
	<?php
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Facebook Likes', 'wsko' ), 'col' => 'col-sm-3 col-xs-12', 'fa' => 'facebook', 'lazyVar' => 'total_likes'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Facebook Reach', 'wsko' ), 'col' => 'col-sm-3 col-xs-12', 'fa' => '', 'lazyVar' => 'total_reach'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Facebook Clicks', 'wsko' ), 'col' => 'col-sm-3 col-xs-12', 'fa' => '', 'lazyVar' => 'total_views'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Facebook Engagement', 'wsko' ), 'col' => 'col-sm-3 col-xs-12', 'fa' => '', 'lazyVar' => 'total_engagement'));
	
	?>
	</div>
	<div class="wsko-section clearfix">
	<?php
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Likes History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_follower'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Reach History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_reach'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Clicks History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_views'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Engagement History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_engagement'));
	
	?>
	</div>
	
	<div class="wsko-section clearfix">
		<p class="panel-heading pl30"><?=__('Facebook Posts', 'wsko');?></p>
	<?php
	
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Facebook Posts', 'wsko' ) . '<br/><small class="text-off">within last 30 days</small>', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'facebook', 'lazyVar' => 'total_posts'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Avg. Post Likes', 'wsko' ), 'col' => 'col-sm-3 col-xs-12', 'fa' => '', 'lazyVar' => 'avg_post_likes'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Avg. Post Engagement', 'wsko' ), 'col' => 'col-sm-3 col-xs-12', 'fa' => '', 'lazyVar' => 'avg_post_engagement'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Avg. Post Reach', 'wsko' ), 'col' => 'col-sm-3 col-xs-12', 'fa' => '', 'lazyVar' => 'avg_post_reach'));
	
	WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Posts within the last 30 days', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
	'custom' => WSKO_Class_Template::render_table(array('Post', 'Likes', 'Engagement', 'Reach', 'Created', 'FB URL'), array(), array('ajax' => array('action' => 'wsko_table_social', 'arg' => '3'), 'filter' => array('0' => array('title' => 'Post', 'type' => 'text'), '1' => array('title' => 'Likes', 'type' => 'number_range', 'min' => 0, 'max' => 100), '2' => array('title' => 'Engagement', 'type' => 'number_range', 'min' => 0, 'max' => 100),'3' => array('title' => 'Reach', 'type' => 'number_range', 'min' => 0, 'max' => 100))), true)));
	?>
	</div>
	<?php
}
else
{
	?>
	<div class="col-sm-12">
	<?php
	//WSKO_Class_Template::render_template('admin/templates/social/view-connect.php', array());
	WSKO_Class_Template::render_notification('warning', array('msg' => 'You have not connected Facebook yet. Set your credentials in the box below.'));
	WSKO_Class_Template::render_template('admin/templates/settings/view-api-facebook.php', array());
	?>
	</div>
	<?php
} ?>
</div>