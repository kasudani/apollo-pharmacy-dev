<?php
if (!defined('ABSPATH')) exit;

	WSKO_Class_Template::render_notification('warning', array('msg' => 'You have not connected this API yet. '));
	WSKO_Class_Template::render_page_link(WSKO_Controller_Settings::get_instance(), 'apis', 'Connect Social APIs', true);
?>