<?php
if (!defined('ABSPATH')) exit;

if (!wp_next_scheduled('wsko_cache_social'))
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'The Cronjob for your Social Data is not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
} ?>