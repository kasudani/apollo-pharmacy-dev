<?php
if (!defined('ABSPATH')) exit;

$fb_logged_in = WSKO_Class_Social::get_social_logged_in('facebook');
$tw_logged_in = !WSKO_Class_Social::check_twitter_access();
$li_logged_in = WSKO_Class_Social::get_social_logged_in('linkedin');
$gp_logged_in = WSKO_Class_Social::get_social_logged_in('googleplus');
?>
<div class="row">
	<?php
	if ($fb_logged_in)
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'Facebook Likes', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'facebook', 'lazyVar' => 'total_likes_fb'));
	}
	else
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'title' => 'Facebook Likes', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'facebook',
												'custom' => WSKO_Class_Template::render_page_link(WSKO_Controller_Settings::get_instance(), 'apis', 'Connect', true, true)));
	}	
	if ($tw_logged_in)
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'Twitter Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'twitter', 'lazyVar' => 'total_follower_tw'));
	}
	else
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'title' => 'Twitter Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'twitter',
												'custom' => WSKO_Class_Template::render_page_link(WSKO_Controller_Settings::get_instance(), 'apis', 'Connect', true, true)));
	}	
	/*if ($li_logged_in)
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'LinkedIn Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'linkedin', 'lazyVar' => 'total_follower_li'));
	}
	else
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'title' => 'LinkedIn Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'linkedin',
												'custom' => WSKO_Class_Template::render_page_link(WSKO_Controller_Settings::get_instance(), 'apis', 'Connect', true, true)));
	}	
	if ($gp_logged_in)
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'Google+ Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'google-plus', 'lazyVar' => 'total_follower_gp'));
	}
	else
	{
		WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'title' => 'Google+ Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'google-plus',
												'custom' => WSKO_Class_Template::render_page_link(WSKO_Controller_Settings::get_instance(), 'apis', 'Connect', true, true)));
	}*/
		WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'title' => 'LinkedIn Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'google-plus', 'custom' => 'Comming soon'));
		WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'title' => 'Google+ Follower', 'col' => 'col-sm-3 col-xs-12', 'fa' => 'linkedin', 'custom' => 'Comming soon'));
	?>
</div>
<div class="row">
	<?php
	if ($fb_logged_in)
		WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => 'Facebook Likes', 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'chart_history_likes_fb'));
	
	if ($tw_logged_in)
		WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => 'Twitter Follower', 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'chart_history_follower_tw'));
	
	if ($li_logged_in)
		WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => 'LinkedIn Follower', 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'chart_history_follower_li'));
	
	if ($gp_logged_in)
		WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => 'Google+ Follower', 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'chart_history_follower_gp'));
	?>
</div>
<div class="row">
	<?php
		WSKO_Class_Template::render_panel(array('type' => 'table', 'title' => 'Top 10 Social Pages', 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_top_pages', 'tableLink' => array('link' => WSKO_Controller_Social::get_link('pages'), 'title' => 'All Pages')));
	?>
</div>