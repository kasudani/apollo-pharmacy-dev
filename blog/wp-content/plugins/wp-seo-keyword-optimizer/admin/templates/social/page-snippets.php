<?php
if (!defined('ABSPATH')) exit;

if (WSKO_Class_Onpage::seo_plugins_disabled())
{
	$post_types = get_post_types(array('public' => true), 'objects');
	$post_types_san = array();
	foreach ($post_types as $pt)
	{
		$post_types_san[$pt->name] = $pt->label;
	}
	$taxonomies = get_taxonomies(array('public' => true), 'objects');
	?>
	<div class="row">
		<div class="col-sm-12">
			<ul class="nav nav-tabs bsu-tabs border-dark">
				<li class="active"><a data-toggle="tab" href="#wsko_snippet_post_type"><?=__( 'Post Types', 'wsko' )?></a></li>
				<li><a data-toggle="tab" href="#wsko_snippet_tax"><?=__( 'Taxonomies', 'wsko' )?></a></li>
				<?php /*<li><a data-toggle="tab" href="#wsko_snippet_other">Other</a></li>*/?>
				<li><a data-toggle="tab" href="#wsko_snippet_bulk"><?=__( 'Bulk Tools', 'wsko' )?></a></li>
			</ul>

			<div class="tab-content">
				<div id="wsko_snippet_post_type" class="tab-pane fade in active">
					<div class="panel-group" id="wsko_snippet_post_type_wrapper">
						<?php
						$f = true;
						foreach ($post_types as $type)
						{
							WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => $type->label." <span class='panel-info'>Post Type '".$type->name."'<span>", 'parent' => 'wsko_snippet_post_type_wrapper', 'active' => $f, 'lazyVar' => 'post_type_'.$type->name, 'class' => 'wsko-collapse-page'));
							$f = false;
						} ?>
					</div>
				</div>
				<div id="wsko_snippet_tax" class="tab-pane fade">
					<div class="panel-group" id="wsko_snippet_tax_wrapper">
						<?php
						$f = true;
						foreach ($taxonomies as $type)
						{
							WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => $type->label." <span class='panel-info'>Taxonomy '".$type->name."'<span>", 'parent' => 'wsko_snippet_tax_wrapper', 'active' => $f, 'lazyVar' => 'post_tax_'.$type->name, 'class' => 'wsko-collapse-page'));
							$f = false;
						} ?>
					</div>
				</div>
				<?php /*<div id="wsko_snippet_other" class="tab-pane fade">
					<div class="panel-group" id="wsko_snippet_other_wrapper">
						<?php
							WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => "Home <span class='panel-info'>".home_url()."<span>", 'parent' => 'wsko_snippet_other_wrapper', 'active' => true, 'lazyVar' => 'other_home'));
							WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => "Blog Page <span class='panel-info'>".home_url()."<span>", 'parent' => 'wsko_snippet_other_wrapper', 'active' => true, 'lazyVar' => 'other_blog'));
							?>
					</div>
				</div>*/?>
				<div id="wsko_snippet_bulk" class="tab-pane fade">
					<div class="bsu-panel bsu-panel-chart">
						<div class="panel panel-default wsko-panel-bulk-tools">
							<p class="panel-heading m0"><?=__('Bulk Tools', 'wsko')?></p>
							<?php
								WSKO_Class_Template::render_table(array('Post', 'Metas'), array(), array('ajax' => array('action' => 'wsko_table_social', 'arg' => '5'), 'filter' => array('post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => $post_types_san)))); 
								?>
						</div>
					</div>	
				</div>
			</div>
			<?php	
			//WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'Referring Backlinks', 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'search', 'lazyVar' => 'total_backlinks'));
			?>
		</div>	
	</div>
<?php }
else
{
	WSKO_Class_Template::render_template('admin/templates/template-seo-plugins-disable.php', array());
} ?>