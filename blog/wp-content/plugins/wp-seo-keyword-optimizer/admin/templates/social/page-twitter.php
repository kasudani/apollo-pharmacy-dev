<?php
if (!defined('ABSPATH')) exit;

//var_dump(WSKO_Class_Social::get_current_twitter_posts_query());
?>
<div class="row">
<?php 
if (!WSKO_Class_Social::check_twitter_access())
{
	
	?>
	<div class="wsko-section">
		<p class="panel-heading pl30"><?=__('Twitter', 'wsko');?> <a href="<?=WSKO_Class_Social::get_tw_page_link()?>" target="_blank"><i class="fa fa-external-link fa-fw"></i></a></p>
		<?php
		WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Twitter Follower', 'wsko' ), 'col' => 'col-sm-4 col-xs-12', 'fa' => 'twitter', 'lazyVar' => 'total_follower'));
		WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Total Favourites', 'wsko' ), 'col' => 'col-sm-4 col-xs-12', 'fa' => 'twitter', 'lazyVar' => 'total_post_likes'));
		WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Total Retweets', 'wsko' ), 'col' => 'col-sm-4 col-xs-12', 'fa' => 'twitter', 'lazyVar' => 'total_post_engagement'));	
		//WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'Tweets in the last 7 days', 'col' => 'col-sm-6 col-xs-12', 'fa' => '', 'lazyVar' => 'total_posts'));
		
		WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'History Follower', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'chart_history_follower'));
		
		//WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'Avg Favourites', 'col' => 'col-sm-6 col-xs-12', 'fa' => '', 'lazyVar' => 'avg_post_likes'));
		//WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => 'Avg Retweets', 'col' => 'col-sm-6 col-xs-12', 'fa' => '', 'lazyVar' => 'avg_post_engagement'));
		
		
		WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Your last 200 Tweets', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
		'custom' => WSKO_Class_Template::render_table(array('Post', 'Likes', 'Retweets', 'Created', 'TW URL'), array(), array('ajax' => array('action' => 'wsko_table_social', 'arg' => '4'), 'filter' => array('0' => array('title' => 'Post', 'type' => 'text'), '1' => array('title' => 'Likes', 'type' => 'number_range', 'min' => 0, 'max' => 100), '2' => array('title' => 'Engagement', 'type' => 'number_range', 'min' => 0, 'max' => 100))), true)));
		?>
	</div>
	<?php
}
else
{
	?>
	<div class="col-sm-12">
	<?php
	//WSKO_Class_Template::render_template('admin/templates/social/view-connect.php', array());
	WSKO_Class_Template::render_notification('warning', array('msg' => 'You have not connected Twitter yet. Set your credentials in the box bellow, submit and reload the page.'));
	WSKO_Class_Template::render_template('admin/templates/settings/view-api-twitter.php', array());
	?>
	</div>
	<?php
} ?>
</div>