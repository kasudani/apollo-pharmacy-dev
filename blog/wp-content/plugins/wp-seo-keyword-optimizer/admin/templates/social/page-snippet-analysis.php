<?php
if (!defined('ABSPATH')) exit;

if (WSKO_Class_Onpage::seo_plugins_disabled())
{
	$global_analysis_data = WSKO_Class_Onpage::get_onpage_analysis();
	?>
	<div class="row">
		<div class="col-sm-12 col-xs-12">
		<?php
		if (isset($global_analysis_data['new_report']['query_running']))
		{
			WSKO_Class_Template::render_notification('info', array('msg' => 'A crawl is currently running, your data will soon be updated.'));
		}
		
		if ($global_analysis_data && isset($global_analysis_data['current_report']))
		{
			$analysis = $global_analysis_data['current_report'];
			$chart_dist_fb = array();
			$chart_dist_tw = array();
			foreach($analysis['facebook_meta_dist'] as $k => $dupl)
			{
				switch ($k) {
					case 'title_length': $k = 'Title Length'; break;
					case 'desc_length': $k = 'Description Length'; break;
					case 'image': $k = 'Image'; break;
				}
				$chart_dist_fb[]= array($k, $dupl[0], $dupl[1], isset($dupl[2]) ? $dupl[2] : 0, isset($dupl[3]) ? $dupl[3] : 0);
			}
			foreach($analysis['twitter_meta_dist'] as $k => $dupl)
			{
				switch ($k) {
					case 'title_length': $k = 'Title Length'; break;
					case 'desc_length': $k = 'Description Length'; break;
					case 'image': $k = 'Image'; break;
				}
				$chart_dist_tw[]= array($k, $dupl[0], $dupl[1], isset($dupl[2]) ? $dupl[2] : 0, isset($dupl[3]) ? $dupl[3] : 0);
			}
			$matrix_fb = array(
				array('og_title_length:0:0', 'og_desc_length:0:0', 'og_img_provided:0:0'),
				array('og_title_length:'.WSKO_ONPAGE_FB_TITLE_MIN.':'.WSKO_ONPAGE_FB_TITLE_MAX, 'og_desc_length:'.WSKO_ONPAGE_FB_DESC_MIN.':'.WSKO_ONPAGE_FB_DESC_MAX, 'og_img_provided:1:1'),
				array('og_title_length:0:'.(WSKO_ONPAGE_FB_TITLE_MIN-1), 'og_desc_length:0:'.(WSKO_ONPAGE_FB_DESC_MIN-1), ''),
				array('og_title_length:'.(WSKO_ONPAGE_FB_TITLE_MAX-1).':500', 'og_desc_length:'.(WSKO_ONPAGE_FB_DESC_MAX-1).':500', '')
			);
			$matrix_tw = array(
				array('tw_title_length:0:0', 'tw_desc_length:0:0', 'tw_img_provided:0:0'),
				array('tw_title_length:'.WSKO_ONPAGE_TW_TITLE_MIN.':'.WSKO_ONPAGE_TW_TITLE_MAX, 'tw_desc_length:'.WSKO_ONPAGE_TW_DESC_MIN.':'.WSKO_ONPAGE_TW_DESC_MAX, 'tw_img_provided:1:1'),
				array('tw_title_length:0:'.(WSKO_ONPAGE_TW_TITLE_MIN-1), 'tw_desc_length:0:'.(WSKO_ONPAGE_TW_DESC_MIN-1), ''),
				array('tw_title_length:'.(WSKO_ONPAGE_TW_TITLE_MAX-1).':500', 'tw_desc_length:'.(WSKO_ONPAGE_TW_DESC_MAX-1).':500', '')
			);
			?>
			<ul class="nav nav-tabs bsu-tabs border-dark">
				<li class="active"><a href="#wsko_social_snippet_analysis_facebook" data-toggle="tab">Facebook</a></li>
				<li><a href="#wsko_social_snippet_analysis_twitter" data-toggle="tab">Twitter</a></li>
			</ul>
			<div class="tab-content">
				<div id="wsko_social_snippet_analysis_facebook" class="tab-pane fade in active">
					<div class="row">
						<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => 'Facebook Snippet Analysis', 'col' => 'col-sm-12 col-xs-12', 
							'custom' => WSKO_Class_Template::render_chart('column', array('Meta', 'Not Set', 'Set', ' Too Short', 'Too Long'), $chart_dist_fb, array('isStacked' => true, 'chart_id' => 'fb_snippet', 'table_filter' => array('table' => '#wsko_social_facebook_analysis_table', 'value_matrix' => $matrix_fb)), true))); ?>
								
						<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => 'Pages', 'col' => 'col-sm-12 col-xs-12', 
							'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'Title length', 'Description length', 'Image set', ''), array(), array('id' => 'wsko_social_facebook_analysis_table', 'ajax' => array('action' => 'wsko_table_social', 'arg' => '1'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'og_title_length' => array('title' => 'Title Length', 'type' => 'number_range', 'max' => 500), 'og_desc_length' => array('title' => 'Description Length', 'type' => 'number_range', 'max' => 500), 'og_img_provided' => array('title' => 'Image', 'type' => 'number_range', 'max' => 1))), true))); ?>
					</div>
				</div>
				<div id="wsko_social_snippet_analysis_twitter" class="tab-pane fade">
					<div class="row">
						<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => 'Twitter Snippet Analysis', 'col' => 'col-sm-12 col-xs-12', 
							'custom' => WSKO_Class_Template::render_chart('column', array('Meta', 'Not Set', 'Set', ' Too Short', 'Too Long'), $chart_dist_tw, array('isStacked' => true, 'chart_id' => 'tw_snippet', 'table_filter' => array('table' => '#wsko_social_twitter_analysis_table', 'value_matrix' => $matrix_tw)), true))); ?>
								
						<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => 'Pages', 'col' => 'col-sm-12 col-xs-12', 
							'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'Title length', 'Description length', 'Image set', ''), array(), array('id' => 'wsko_social_twitter_analysis_table', 'ajax' => array('action' => 'wsko_table_social', 'arg' => '2'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'tw_title_length' => array('title' => 'Title Length', 'type' => 'number_range', 'max' => 500), 'tw_desc_length' => array('title' => 'Description Length', 'type' => 'number_range', 'max' => 500), 'tw_img_provided' => array('title' => 'Image', 'type' => 'number_range', 'max' => 1))), true))); ?>
					</div>
				</div>
			</div><?php
		}
		else
		{
			WSKO_Class_Template::render_notification('warning', array('msg' => 'The current Onpage Report has not been generated yet. See '.WSKO_Class_Template::render_page_link(WSKO_Controller_Onpage::get_instance(), 'analysis', 'Onpage', false, true).' for more information.'));
		}
		?>
		</div>
	</div>
<?php }
else
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'You need to disable your SEO plugins to gain access to this area. Please deactivate the following', 'list' => WSKO_Class_Import::get_active_seo_plugins(), 'subnote' => 'We are sorry for the inconvenience, but otherwise your system could become instabile.'));
}
?>