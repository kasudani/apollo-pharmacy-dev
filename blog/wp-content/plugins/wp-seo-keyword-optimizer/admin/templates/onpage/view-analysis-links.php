<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;
?>
<div class="row">
	<?php WSKO_Class_Template::render_panel(array('type' => 'progress', 'title' => 'URL Length', 'col' => 'col-sm-12 col-xs-12',
		'items' => array(
			array('title' => __( 'OK', 'wsko' ), 'value' => $analysis['url_length_dist']['ok'], 'max' => $analysis['total_pages'], 'progress_class' => 'success', 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_permalink_table', 'val' => 'url_length:0:'.(WSKO_ONPAGE_URL_MAX-1))),
			array('title' => __( 'Too Long', 'wsko' ), 'value' => $analysis['url_length_dist']['too_long'], 'max' => $analysis['total_pages'], 'progress_class' => 'warning', 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_permalink_table', 'val' => 'url_length:'.(WSKO_ONPAGE_URL_MAX+1).':'.$analysis['max']['url_length'])),
		))); ?>
		
	<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
		'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'URL Length', ''), array(), array('id' => 'wsko_onpage_analysis_permalink_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '10'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), 'url_length' => array('title' => 'URL Length', 'type' => 'number_range', 'max' => $analysis['max']['url_length']))), true))); ?>
</div>