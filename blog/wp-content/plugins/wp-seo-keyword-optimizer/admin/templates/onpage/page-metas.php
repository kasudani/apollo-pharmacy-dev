<?php
if (!defined('ABSPATH')) exit;

$post_types = WSKO_Class_Helper::get_public_post_types('objects');
$post_types_san = array();
foreach ($post_types as $pt)
{
	$post_types_san[$pt->name] = $pt->label;
}
$taxonomies = get_taxonomies(array('public' => true), 'objects');
?>
<div class="row">
	<div class="col-sm-12">
		<p class="hidden-xs pull-right" style="margin:10px 5px;"><a class="dark text-off wsko-small" target="_blank" href="https://www.bavoko.tools/en/knowledge_base/generate-dynamic-meta-descriptions-titles-in-wordpress/"><i class="fa fa-info fa-fw"></i> Knowledge Base: Generate Dynamic Meta Titles & Descriptions</a></p>
		<ul class="nav nav-tabs bsu-tabs border-dark">
			<li class="active"><a data-toggle="tab" href="#wsko_meta_post_type"><?=__( 'Post Types', 'wsko' )?></a></li>
			<li><a data-toggle="tab" href="#wsko_meta_tax"><?=__( 'Taxonomies', 'wsko' )?></a></li>
			<?php /*<li><a data-toggle="tab" href="#wsko_meta_other">Other</a></li>*/?>
			<li><a data-toggle="tab" href="#wsko_meta_bulk"><?=__( 'Bulk Tools', 'wsko' )?></a></li>
		</ul>

		<div class="tab-content">
			<div id="wsko_meta_post_type" class="tab-pane fade in active">
				<div class="panel-group" id="wsko_meta_post_type_wrapper">
					<?php
					$f = true;
					foreach ($post_types as $type)
					{
						WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => $type->label." <span class='panel-info'>Post Type '".$type->name."'<span>", 'parent' => 'wsko_meta_post_type_wrapper', 'active' => $f, 'lazyVar' => 'post_type_'.$type->name, 'class' => 'wsko-collapse-page'));
						$f = false;
					} ?>
				</div>
			</div>
			<div id="wsko_meta_tax" class="tab-pane fade">
				<div class="panel-group" id="wsko_meta_tax_wrapper">
					<?php
					$f = true;
					foreach ($taxonomies as $type)
					{
						WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => $type->label." <span class='panel-info'>Taxonomy '".$type->name."'<span>", 'parent' => 'wsko_meta_tax_wrapper', 'active' => $f, 'lazyVar' => 'post_tax_'.$type->name, 'class' => 'wsko-collapse-page'));
						$f = false;
					} ?>
				</div>
			</div>
			<?php /*<div id="wsko_meta_other" class="tab-pane fade">
				<div class="panel-group" id="wsko_meta_other_wrapper">
					<?php
						WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => "Home <span class='panel-info'>".home_url()."<span>", 'parent' => 'wsko_meta_other_wrapper', 'active' => true, 'lazyVar' => 'other_home', 'class' => 'wsko-collapse-page'));
						WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => "Blog Page <span class='panel-info'>".home_url()."<span>", 'parent' => 'wsko_meta_other_wrapper', 'lazyVar' => 'other_blog', 'class' => 'wsko-collapse-page'));
						?>
				</div>
			</div>*/ ?>
			<div id="wsko_meta_bulk" class="tab-pane fade">
				<div class="bsu-panel bsu-panel-chart">
					<div class="panel panel-default wsko-panel-bulk-tools">
						<p class="panel-heading m0"><?=__('Bulk Tools', 'wsko')?></p>
						<?php WSKO_Class_Template::render_table(array('Post', 'Metas'), array(), array('ajax' => array('action' => 'wsko_table_onpage', 'arg' => '7'), 'filter' => array('post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => $post_types_san)))); ?>
					</div>
				</div>	
			</div>
		</div>
	</div>	
</div>