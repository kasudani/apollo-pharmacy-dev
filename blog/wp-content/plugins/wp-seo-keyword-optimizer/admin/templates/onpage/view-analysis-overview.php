<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;
$onpage_score_history = array();
if (isset($template_args['analysis']['onpage_history']))
{
	foreach ($template_args['analysis']['onpage_history'] as $k => $re)
	{
		$onpage_score_history[] = array(date('d.m.Y', $k), $re);
	}
}
$table_worst_optimized = array();//$worst_optimized['data'];
if ($analysis)
{
	$worst_optimized = WSKO_Class_Helper::prepare_data_table($analysis['pages'], array('offset' => 0, 'count' => 10), false /*array(array('key' => 'onpage_score', 'comp' => 'ra', 'val' => array(0,50)))*/, array('key' => '0', 'dir' => 1), array('specific_keys' => array('onpage_score', 'url', 'post_id'), 'format' => array('0' => 'prog_rad', '1' => function($arg,$r){ return WSKO_Class_Template::render_post_dirty_icon($r[2], array(), true).WSKO_Class_Template::render_url_post_field($r[2], array(), true); }, '2' => function($arg){ return WSKO_Class_Template::render_content_optimizer_link($arg, array(), true); })), false);

	if ($worst_optimized)
	{
		foreach ($worst_optimized['data'] as $row)
		{
			$table_worst_optimized[] = array(array('value' => $row[0]),array('value' => $row[1]), array('value' => $row[2]));
		}
	}
}
?>
<div class="row">
	<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Avg. Content Score', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
		'custom' => $onpage_score_history ? WSKO_Class_Template::render_chart('area', array('Date', 'Content Score'), $onpage_score_history, array(), true) : WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true))); 
	?>
		
	<div class="wsko-section clearfix col-sm-12 col-xs-12">	
		<p class="panel-heading m0"><?=__( 'Onpage Issues', 'wsko' ) ?></p>	
		<div class="panel-group wsko-onpage-analysis-issues clearfix">
			<div id="wsko_onpage_issues" class="">
				<?php WSKO_Class_Template::render_template('admin/templates/onpage/view-onpage-issues.php', array('analysis' => WSKO_Class_Onpage::get_onpage_analysis())); ?>
			</div>
		</div>	
	</div>	
</div>
<div class="row">
	<div class="col-md-12">
		<?php
		/*foreach ($analysis['issues'] as $k => $iss)
		{
			  $tran = WSKO_Class_Onpage::translate_onpage_issue(array('code' => $k));
			  $tran['msg'] = $iss . ' with "'.$tran['msg'].'"';
			  unset($tran['subnote']);
			  WSKO_Class_Template::render_notification($tran['severity'], $tran);
		}*/ ?>
	</div>
</div>
<div class="row">
	<?php
		WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __('Pages with the lowest Content Score', 'wsko'), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'fa' => 'ellipsis-v', 
			'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true),'URL',''), $table_worst_optimized, array('no_pages' => true, 'order' => array('col' => 0, 'dir' => 'asc')), true)));
	?>
</div>