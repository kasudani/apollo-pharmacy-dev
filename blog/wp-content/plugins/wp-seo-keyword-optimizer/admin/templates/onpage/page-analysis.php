<?php
if (!defined('ABSPATH')) exit;

$global_analysis_data = WSKO_Class_Onpage::get_onpage_analysis();
//var_dump(date('d.m.Y H:i', wp_next_scheduled('wsko_onpage_analysis')));
?>
<div class="row">
	<?php
	if ($global_analysis_data && isset($global_analysis_data['current_report']))
	{
		$global_analysis = $global_analysis_data['current_report'];
		?>
		<div class="col-md-3 onpage-analysis-nav">
			<div class="wsko-onpage-main-nav">
				<label><?=__( 'Onpage Analysis', 'wsko' ) ?></label>
				<ul class="nav nav-tabs bsu-tabs nav-stacked">
					<li class="active"><a data-toggle="tab" href="#wsko_onpage_analysis_overview"><?=__( 'Overview', 'wsko' ) ?></a></li>
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_titles"><?=__( 'Titles', 'wsko' ) ?></a></li>
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_descriptions"><?=__( 'Descriptions', 'wsko' ) ?></a></li>
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_links"><?=__( 'Permalinks', 'wsko' ) ?></a></li>					
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_headings"><?=__( 'Headings', 'wsko' ) ?></a></li>
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_content"><?=__( 'Content Length', 'wsko' ) ?></a></li>
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_keywords"><?=__( 'Priority Keywords', 'wsko' ) ?></a></li>
					<?php /* <li><a data-toggle="tab" href="#wsko_onpage_analysis_index">Indexability</a></li> 
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_resources">Resources</a></li>
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_canon">Canonicals</a></li>
					<li><a data-toggle="tab" href="#wsko_onpage_analysis_robots">robots.txt</a></li> */ ?>
				</ul>
				
				<label><?=__( 'Crawling Info', 'wsko' ) ?></label>
				<div class="crawling-info-inner">
					<small class="font-unimportant"><b><?=__( 'Next Crawl:', 'wsko' ) ?></b> <?=date('d.m.Y', $global_analysis['started'] + (60*60*24*7))?></small><br/>		
					<small class="font-unimportant"><b><?=__( 'Last Crawl:', 'wsko' ) ?></b> <?=date('d.m.Y', $global_analysis['started'])?></small><br/>					
					<small class="font-unimportant"><b><?=__( 'Crawled Pages:', 'wsko' ) ?></b> <?=$global_analysis['total_pages']?></small><br/>
					
					<p style="line-height: 15px; margin-top: 5px;"><small class="text-off">Please note: Changes in your content will not be visible in the current report until the next crawl on <?=date('d.m.Y', $global_analysis['started'] + (60*60*24*7))?></small></p>
					<!--a class="btn btn-flat btn-sm" href="#">Regenerate Report</a-->  
				</div>
			</div>
		</div>
		<div class="col-md-9 onpage-analysis-content">
			<?php 
				if (isset($global_analysis_data['new_report']['query_running']))
					{
						WSKO_Class_Template::render_notification('info', array('msg' => 'A crawl is currently running, your data will soon be updated.'));
					}
			?>		
			<div class="tab-content">
				<div id="wsko_onpage_analysis_overview" class="tab-pane fade in active">
					<?php WSKO_Class_Template::render_lazy_field('analysis_overview', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_titles" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_titles', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_descriptions" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_descriptions', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_headings" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_headings', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_content" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_content', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_keywords" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_keywords', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_index" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_index', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_links" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_links', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_resources" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_resources', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_canon" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_canon', 'small'); ?>
				</div>
				<div id="wsko_onpage_analysis_robots" class="tab-pane fade">
					<?php WSKO_Class_Template::render_lazy_field('analysis_robots', 'small'); ?>
				</div>
			</div>
		</div>
		<?php
	}
	else
	{
		$post_types = WSKO_Class_Helper::get_public_post_types('objects');
		if (!wp_next_scheduled('wsko_onpage_analysis'))
		{
			?> 
			<div class="col-sm-12 col-xs-12">
				<div class="bsu-panel">
					<div class="panel panel-default">
						<p class="panel-heading m0">Onpage Analysis Settings</p>
						<div class="panel-body">
							<div class="row wsko-onpage-include-post-types form-group" style="overflow-y:auto;max-height:130px;">
								<div class="col-sm-3 col-xs-12">
									<p>Include Post Types in Onpage Analysis</p>
								</div>
								<div class="col-sm-9 col-xs-12">
									<div class="row">
										<?php 
										$included = explode(',', WSKO_Class_Core::get_setting('onpage_include_post_types'));
										foreach ($post_types as $pt)
										{
											?><div class="col-md-3">
												<label><input class="form-control wsko-ajax-input" type="checkbox" <?=in_array($pt->name, $included) ? 'checked="checked"' : ''?> data-wsko-target="settings" data-wsko-setting="onpage_include_post_types" data-multi-parent=".wsko-onpage-include-post-types" value="<?=$pt->name?>"><?=$pt->label?> <span class="settings-sub font-unimportant"><?=$pt->name?></span></label>
											</div><?php
										}
										?>
									</div>	
								</div>	
							</div>
							<div class="row wsko-onpage-post-types-h1-title form-group" style="overflow-y:auto;max-height:130px;">
								<div class="col-sm-3 col-xs-12">
									<p>Post Title is used as H1</p>
									<p class="small text-off">Select post types, where the post title is used as the H1-Heading of your content.</p>
								</div>
								<div class="col-sm-9 col-xs-12">
									<div class="row">
										<?php 
										$included = explode(',', WSKO_Class_Core::get_setting('onpage_title_h1'));
										foreach ($post_types as $pt)
										{
											?><div class="col-md-3">
												<label><input class="form-control wsko-ajax-input" type="checkbox" <?=in_array($pt->name, $included) ? 'checked="checked"' : ''?> data-wsko-target="settings" data-wsko-setting="onpage_title_h1" data-multi-parent=".wsko-onpage-post-types-h1-title" value="<?=$pt->name?>"><?=$pt->label?> <span class="settings-sub font-unimportant"><?=$pt->name?></span></label>
											</div><?php
										}
										?>
									</div>	
								</div>	
							</div>
							
							<?php WSKO_Class_Template::render_ajax_button('Start Onpage Crawl', 'refresh_analysis', array(), array()); ?>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		else
		{
			?>
			<div class="col-sm-12 col-xs-12">
				<?php WSKO_Class_Template::render_notification('success', array('msg' => 'Your onpage analysis data will be available soon - we are working on your next website report.')) ?>
			</div>
			<?php 
		}
	}
	?>
</div>