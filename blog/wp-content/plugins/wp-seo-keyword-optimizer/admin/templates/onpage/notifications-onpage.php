<?php
if (!defined('ABSPATH')) exit;

if (WSKO_Class_Onpage::seo_plugins_disabled())
{
	if (!wp_next_scheduled('wsko_onpage_analysis'))
	{
		if (isset(WSKO_Class_Onpage::get_onpage_analysis()['current_report']))
			WSKO_Class_Template::render_notification('error', array('msg' => 'The Cronjob for your Page Analysis is not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
		else if (WSKO_Class_Core::get_option('onpage_calc_changed'))
			WSKO_Class_Template::render_notification('warning', array('msg' => 'Please Note: We have improved our onpage analysis tool with the last update, which requires a new crawl. Please generate a new onpage report.'));
	}
	if (!wp_next_scheduled('wsko_sitemap_generation') && WSKO_Class_Core::get_setting('automatic_sitemap'))
	{
		WSKO_Class_Template::render_notification('error', array('msg' => 'The Cronjob for your Sitemap Generation is not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
	}
}
?>