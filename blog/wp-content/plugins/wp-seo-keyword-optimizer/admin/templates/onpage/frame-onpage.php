<?php
if (!defined('ABSPATH')) exit;

if (WSKO_Class_Onpage::seo_plugins_disabled())
{ 
	$controller = WSKO_Class_Core::get_current_controller();
	$controller->subpage_view();
}
else
{
	WSKO_Class_Template::render_template('admin/templates/template-seo-plugins-disable.php', array());
} ?>