<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;

$chart_dupl = array();
if ($analysis)
{
	foreach($analysis['desc_duplicate_dist'] as $k => $dupl)
	{
		if ($k != 0)
			$chart_dupl[]= array($k.' Duplicates', $dupl);
	}
}
$matrix_desc = array(array(/*'desc_duplicates:0:0',*/'desc_duplicates:1:1','desc_duplicates:2:2','desc_duplicates:3:3','desc_duplicates:4:4','desc_duplicates:5:5','desc_duplicates:6:6','desc_duplicates:7:7','desc_duplicates:8:8','desc_duplicates:9:'.$analysis['max']['desc_dupl']));
?>
<div class="row">
	<div class="col-sm-12">
		<ul class="nav nav-tabs bsu-tabs border-dark">
			<li class="waves-effect active"><a data-toggle="tab" href="#wsko_onpage_analysis_descriptions_length">Length</a></li>
			<li class="waves-effect"><a data-toggle="tab" href="#wsko_onpage_analysis_descriptions_duplicates">Duplicates</a></li>
		</ul>

		<div class="tab-content">
			<div id="wsko_onpage_analysis_descriptions_length" class="tab-pane fade in active">
				<div class="row">
					<?=WSKO_Class_Template::render_panel(array('type' => 'progress', 'title' => 'Length Distribution', 'col' => 'col-sm-12 col-xs-12', 
						'items' => array(
							array('title' => __( 'OK', 'wsko' ), 'value' => $analysis['desc_length_dist']['ok'], 'max' => $analysis['total_pages'], 'progress_class' => 'success', 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_descriptions_table', 'val' => 'desc_length:'.WSKO_ONPAGE_DESC_MIN.':'.WSKO_ONPAGE_DESC_MAX)),
							array('title' => __( 'Too Short', 'wsko' ), 'value' => $analysis['desc_length_dist']['too_short'],  'progress_class' => 'warning', 'max' => $analysis['total_pages'], 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_descriptions_table', 'val' => 'desc_length:0:'.(WSKO_ONPAGE_DESC_MIN-1))),
							array('title' => __( 'Too Long', 'wsko' ), 'value' => $analysis['desc_length_dist']['too_long'], 'progress_class' => 'warning', 'max' => $analysis['total_pages'], 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_descriptions_table', 'val' => 'desc_length:'.(WSKO_ONPAGE_DESC_MAX+1).':'.$analysis['max']['desc_length'])),
							array('title' => __( 'Not Set', 'wsko' ), 'value' => $analysis['desc_length_dist']['not_set'], 'progress_class' => 'danger', 'max' => $analysis['total_pages'], 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_descriptions_table', 'val' => 'desc_length:0:0'))
						)), true); ?>
						
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
						'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'Desc Length', 'Desc Duplicates', ''), array(), array('id' => 'wsko_onpage_analysis_descriptions_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '2'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), 'desc_length' => array('title' => 'Description Length', 'type' => 'number_range', 'max' => $analysis['max']['desc_length']), 'desc_duplicates' => array('title' => 'Description Duplicates', 'type' => 'number_range', 'max' => $analysis['max']['desc_dupl']))), true))); ?>
				</div>		
			</div>
			<div id="wsko_onpage_analysis_descriptions_duplicates" class="tab-pane fade">
				<div class="row">
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Description Duplicates', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
					'custom' => $chart_dupl ? WSKO_Class_Template::render_chart('column', array('Duplicates', 'Page Count'), $chart_dupl, array('table_filter' => array('table' => '#wsko_onpage_analysis_descriptions_dupl_table', 'value_matrix' => $matrix_desc), 'chart_id' => 'desc_dupl'), true) : WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true))); ?>
					
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
						'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'Desc Length', 'Desc Duplicates', ''), array(), array('id' => 'wsko_onpage_analysis_descriptions_dupl_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '9'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), 'desc_length' => array('title' => 'Description Length', 'type' => 'number_range', 'max' => $analysis['max']['desc_length']), 'desc_duplicates' => array('title' => 'Description Duplicates', 'type' => 'number_range', 'max' => $analysis['max']['desc_dupl']))), true))); ?>
				</div>
			</div>
		</div>
	</div>
	
</div>