<?php
if (!defined('ABSPATH')) exit;

$url = isset($template_args['url']) ? $template_args['url'] : false;
$suppress_success = isset($template_args['suppress_success']) && $template_args['suppress_success'] ? true : false;
$status_check = isset($template_args['status_check']) && $template_args['status_check'] ? true : false;
if ($url)
{
	$redirects = WSKO_Class_Onpage::get_redirect_for_url($url, true);
	if ($redirects)
	{
		$is_admin = current_user_can('manage_options');
		$list = array();
		$count = 0;
		foreach($redirects as $re)
		{
			$count++;
			$str = "";
			switch ($re['type'])
			{
				//case '404': $str .= '404 to 301 - '; break;
				case 'custom': 
				$str .= 'Custom Rule ('.$re['data']['re']['comp'].') <span>'.$re['data']['re']['page'].' to <i>'.$re['to'].'</i></span>';
				if ($is_admin)
					$str .= WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_redirect', array('redirects' => $re['data']['key']), array(), true);
				break;
				case 'post': 
				$str .= 'Post Redirect <span>To <i>'.$re['to'].'</i></span>';
				if ($is_admin)
					$str .= WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_page_redirect', array('post' => $re['data']['post']), array(), true);
				break;
				case 'auto_post': 
				$str .= 'Auto Post Redirect <span><i>'.$re['from'].'</i> to <i>'.$re['to'].'</i></span>';
				if ($is_admin)
					$str .= WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_auto_redirect', array('type' => $re['data']['type'], 'arg' => $re['data']['arg'], 'key' => $re['data']['key']), array(), true);
				break;
				case 'auto_post_type': 
				$str .= 'Auto Post Type ('.$re['data']['arg'].') <span>'.$re['data']['slug'].' to <i>'.$re['to'].'</i></span>';
				if ($is_admin)
					$str .= WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_auto_redirect', array('type' => $re['data']['type'], 'arg' => $re['data']['arg'], 'key' => $re['data']['slug']), array(), true);
				break;
			}
			$list[] = $str;
		}
		WSKO_Class_Template::render_notification('warning', array('msg' => '<b>'.$count.'</b> redirect rule(s) are matching the URL <i>'.$url.'</i>:', 'list' => $list, 'subnote' => 'Rules are sorted descending by priority', 'notif-class' => 'wsko-post-redirects'));
	}
	else if (!$suppress_success)
	{
		WSKO_Class_Template::render_notification('success', array('msg' => 'No redirect rules found matching "'.$url.'".'));
	}
	if ($status_check)
	{
		?><label>HTTP Code Trace</label><?php
		$codes = WSKO_Class_Helper::get_http_status_codes($url);
		if ($codes)
		{
			$last_url = false;
			?>
			<div class="wsko-http-trace">
				Call <i class="fa fa-angle-right fa-fw"></i> <?php
				$f = true;
				foreach ($codes as $code)
				{
					if ($code['code'] == 200)
						$color = 'green';
					else if ($code['code'] < 400)
						$color = 'yellow';
					else if ($code['code'] == 'loop')
						$color = 'blue';
					else
						$color = 'red';
					if (!$f)
						echo ' <i class="fa fa-angle-right fa-fw"></i> ';
					?>
					
					<p class=""><span class="badge wsko-badge wsko-<?=$color?>"><?=$code['code']?></span> <span><?=$code['url']?></span></p><?php
					$f = false;
					$last_url = $code;
				}
				if ($last_url)
				{
					?><br/>Effective URL: <?=$last_url['url']?> (<?=$last_url['code']?>)<?php
				}
				?>
			</div>
			<?php
		}
		else
		{
			WSKO_Class_Template::render_notification('error', array('msg' => 'Status codes could not be fetched.'));
		}
	}
}
?>