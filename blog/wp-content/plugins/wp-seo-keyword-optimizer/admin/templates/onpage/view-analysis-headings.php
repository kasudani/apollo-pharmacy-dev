<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;
$chart_dist = array();

?>

<?php
if ($analysis)
{
	foreach($analysis['heading_dist'] as $k => $dupl)
	{
		if ($k == 'h1') {
			$chart_dist[]= array('stacked' => true, 'title' => $k,
			'value' => array(
				array('class' => 'danger wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][0], 'title' => __( 'None', 'wsko' ), 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':0:0')),
				array('class' => 'success wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][1], 'title' => '1', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':1:1')),
				array('class' => 'danger wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][2], 'title' => '2', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':2:2')),
				array('class' => 'danger wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][3], 'title' => '3', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':3:3')),
				array('class' => 'danger wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][4], 'title' => '4', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':4:4')),
				array('class' => 'danger wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][5], 'title' => '5+', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':5:'.$analysis['max']['heading_count'])),
			), 'max' => $analysis['total_pages']);		
		} else {
			//$chart_dist[]= array_merge(array($k), $dupl);
			$chart_dist[]= array('stacked' => true, 'title' => $k,
			'value' => array(
				array('class' => 'danger wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][0], 'title' => __( 'None', 'wsko' ), 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':0:0')),
				array('class' => 'success wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][1], 'title' => '1', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':1:1')),
				array('class' => 'success wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][2], 'title' => '2', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':2:2')),
				array('class' => 'success wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][3], 'title' => '3', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':3:3')),
				array('class' => 'success wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][4], 'title' => '4', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':4:4')),
				array('class' => 'success wsko-external-table-filter', 'value' => $analysis['heading_dist'][$k][5], 'title' => '5+', 'data' => array('table' => '#wsko_onpage_analysis_headings_table', 'val' => 'count_'.$k.':5:'.$analysis['max']['heading_count'])),
			), 'max' => $analysis['total_pages']);
		}	
	}
}
/*$matrix_headings = array(array('count_h1:0:0','count_h2:0:0','count_h3:0:0','count_h4:0:0','count_h5:0:0','count_h6:0:0'),
array('count_h1:1:1','count_h2:1:1','count_h3:1:1','count_h4:1:1','count_h5:1:1','count_h6:1:1'),
array('count_h1:2:2','count_h2:2:2','count_h3:2:2','count_h4:2:2','count_h5:2:2','count_h6:2:2'),
array('count_h1:3:3','count_h2:3:3','count_h3:3:3','count_h4:3:3','count_h5:3:3','count_h6:3:3'),
array('count_h1:4:4','count_h2:4:4','count_h3:4:4','count_h4:3:3','count_h5:4:4','count_h6:4:4'),
array('count_h1:5:'.$analysis['max']['heading_count'],'count_h2:5:'.$analysis['max']['heading_count'], 'count_h3:5:'.$analysis['max']['heading_count'],'count_h4:5:'.$analysis['max']['heading_count'],'count_h5:5:'.$analysis['max']['heading_count'],'count_h6:5:'.$analysis['max']['heading_count']));*/
?>
<div class="row">
	<?php WSKO_Class_Template::render_panel(array('type' => 'progress', 'title' => __( 'Heading Count Distribution', 'wsko' ), 'col' => 'col-sm-12 col-xs-12',
		'items' => $chart_dist)); ?>
		
	<?php /*WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => 'Heading Count Distribution', 'col' => 'col-sm-12 col-xs-12', 
		'custom' => $chart_dist ? WSKO_Class_Template::render_chart('column', array('Content Length', 'Pages with 0', 'Pages with 1', 'Pages with 2', 'Pages with 3', 'Pages with 4', 'Pages with 5+'), $chart_dist, array('isStacked' => true, 'table_filter' => array('table' => '#wsko_onpage_analysis_headings_table', 'value_matrix' => $matrix_headings), 'chart_id' => 'headings'), true) : WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true)));*/ ?>
			
	<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
		'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', ''), array(), array('id' => 'wsko_onpage_analysis_headings_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '3'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), 'count_h1' => array('title' => 'Count H1', 'type' => 'number_range', 'max' => $analysis['max']['heading_count']), 'count_h2' => array('title' => 'Count H2', 'type' => 'number_range', 'max' => $analysis['max']['heading_count']), 'count_h3' => array('title' => 'Count H3', 'type' => 'number_range', 'max' => $analysis['max']['heading_count']), 'count_h4' => array('title' => 'Count H4', 'type' => 'number_range', 'max' => $analysis['max']['heading_count']), 'count_h5' => array('title' => 'Count H5', 'type' => 'number_range', 'max' => $analysis['max']['heading_count']), 'count_h6' => array('title' => 'Count H6', 'type' => 'number_range', 'max' => $analysis['max']['heading_count']))), true))); ?>
</div>