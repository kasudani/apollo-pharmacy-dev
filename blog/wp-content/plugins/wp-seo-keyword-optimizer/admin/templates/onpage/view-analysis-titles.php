<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;

$chart_dupl = array();
if ($analysis)
{
	foreach($analysis['title_duplicate_dist'] as $k => $dupl)
	{
		if ($k != 0)
			$chart_dupl[]= array($k.' Duplicates', $dupl);
	}
}
$matrix_title = array(array(/*'title_duplicates:0:0',*/'title_duplicates:1:1','title_duplicates:2:2','title_duplicates:3:3','title_duplicates:4:4','title_duplicates:5:5','title_duplicates:6:6','title_duplicates:7:7','title_duplicates:8:8','title_duplicates:9:'.$analysis['max']['desc_dupl']));
?>
<div class="row">
	<div class="col-sm-12">
		<ul class="nav nav-tabs bsu-tabs border-dark">
			<li class="waves-effect active"><a data-toggle="tab" href="#wsko_onpage_analysis_titles_length"><?=__( 'Length', 'wsko' ) ?></a></li>
			<li class="waves-effect"><a data-toggle="tab" href="#wsko_onpage_analysis_titles_duplicates"><?=__( 'Duplicates', 'wsko' ) ?></a></li>
		</ul>

		<div class="tab-content">
			<div id="wsko_onpage_analysis_titles_length" class="tab-pane fade in active">
				<div class="row">
					<?php WSKO_Class_Template::render_panel(array('type' => 'progress', 'title' => __( 'Title Length', 'wsko' ), 'col' => 'col-sm-12 col-xs-12',
						'items' => array(
							array('title' => __( 'OK', 'wsko' ), 'value' => $analysis['title_length_dist']['ok'], 'max' => $analysis['total_pages'], 'progress_class' => 'success', 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_titles_table', 'val' => 'title_length:'.WSKO_ONPAGE_TITLE_MIN.':'.WSKO_ONPAGE_TITLE_MAX)),
							array('title' => __( 'Too Short', 'wsko' ), 'value' => $analysis['title_length_dist']['too_short'], 'max' => $analysis['total_pages'], 'progress_class' => 'warning', 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_titles_table', 'val' => 'title_length:1:'.(WSKO_ONPAGE_TITLE_MIN-1))),
							array('title' => __( 'Too Long', 'wsko' ), 'value' => $analysis['title_length_dist']['too_long'], 'max' => $analysis['total_pages'], 'progress_class' => 'warning', 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_titles_table', 'val' => 'title_length:'.(WSKO_ONPAGE_TITLE_MAX+1).':'.$analysis['max']['title_length'])),
							array('title' => __( 'Not Set', 'wsko' ), 'value' => $analysis['title_length_dist']['not_set'], 'max' => $analysis['total_pages'], 'progress_class' => 'danger', 'class' => 'wsko-external-table-filter', 'data' => array('table' => '#wsko_onpage_analysis_titles_table', 'val' => 'title_length:0:0'))
						))); ?>
						
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
						'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'Title Length', 'Title Duplicates', ''), array(), array('id' => 'wsko_onpage_analysis_titles_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '1'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), 'title_length' => array('title' => 'Title Length', 'type' => 'number_range', 'max' => $analysis['max']['title_length']), 'title_duplicates' => array('title' => 'Title Duplicates', 'type' => 'number_range', 'max' => $analysis['max']['title_dupl']))), true))); ?>
				</div>	
			</div>
			<div id="wsko_onpage_analysis_titles_duplicates" class="tab-pane fade">
				<div class="row">
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Title Duplicates', 'wsko' ), 'col' => 'col-sm-12 col-xs-12',  
						'custom' => $chart_dupl ? WSKO_Class_Template::render_chart('column', array('Duplicates', 'Page Count'), $chart_dupl, array('table_filter' => array('table' => '#wsko_onpage_analysis_titles_dupl_table', 'value_matrix' => $matrix_title), 'chart_id' => 'title_dupl'), true) : WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true))); ?>
						
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
						'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'Title Length', 'Title Duplicates', ''), array(), array('id' => 'wsko_onpage_analysis_titles_dupl_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '8'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), 'title_length' => array('title' => 'Title Length', 'type' => 'number_range', 'max' => $analysis['max']['title_length']), 'title_duplicates' => array('title' => 'Title Duplicates', 'type' => 'number_range', 'max' => $analysis['max']['title_dupl']))), true))); ?>
				</div>		
			</div>
		</div>
	</div>
	
</div>