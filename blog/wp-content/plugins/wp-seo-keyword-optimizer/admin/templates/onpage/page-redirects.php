<?php
if (!defined('ABSPATH')) exit;

$onpage_data = WSKO_Class_Onpage::get_onpage_data();
?>
<div class="row">
<?php if (current_user_can('manage_options')) { ?>
		<div class="bsu-panel bsu-panel-custom col-md-12 col-xs-12">
			<div class="panel panel-default">
				<p class="panel-heading m0">Add custom redirect</p>
				<div class="panel-body">
					<form id="wsko_add_redirect_form" data-nonce="<?=wp_create_nonce('wsko_add_redirect')?>">
						<?php /*
						<div class="row">
							<div class="wsko-redirect-type-infos wsko-infos-exact" style="display:none">Help Text "Exact"</div>
							<?php /*<div class="wsko-redirect-type-infos wsko-infos-starts_with" style="display:none">Help Text "Starts with"</div>
							 <div class="wsko-redirect-type-infos wsko-infos-contains" style="display:none">Help Text "Contains"</div>* /?>
							<div class="wsko-redirect-type-infos wsko-infos-replace" style="display:none">Help Text "Contains"</div>
						</div>
						*/ ?>
						<div class="row form-group">
							<div class="col-md-3">
								<p>Redirect from</p>
							</div>
							<div class="col-md-3">
								<select class="form-control wsko-field-comp" data-ph-exact="https://www.domain.com/path" data-ph-contains="/relative/link/">
									<option value="exact" selected>Exact match</option>
									<option value="contains">Contains</option>
								</select>
							</div>
							<div class="col-md-6">
								<input class="form-control wsko-field-page" name="page" type="text" placeholder="" required>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
								Redirect to
							</div>
							<div class="col-md-3">
								<select class="form-control wsko-field-comp-to" data-ph-exact="https://www.domain.com/path" data-ph-replace="/relative/link/">
									<option value="exact" selected>Exact match</option>
									<option value="replace">Replace</option>
								</select>
							</div>
							<div class="col-md-6">
								<input class="form-control wsko-field-redirect" name="redirect_to" type="text" placeholder="" required>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
								Redirect type
							</div>
							<div class="col-md-9">
								<select class="form-control wsko-field-type">
									<option value="1">301 (Permanent Redirect, SEO friendly)</option>
									<option value="2">302 (Temporary Redirect)</option>
								</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
							</div>
							<div class="col-md-9">
								<button class="button wsko-save-btn" type="submit">Add Redirect</button>
							</div>
						</div>
					</form>
				</div>	
			</div>
		</div>
	<?php } ?>
	<div class="bsu-panel bsu-panel-custom col-md-12 col-xs-12">
		<div class="panel panel-default">
			<p class="panel-heading m0">Current Redirects</p>
			<?php WSKO_Class_Template::render_lazy_field('redirects', 'small'); ?>
		</div>
	</div>
</div>