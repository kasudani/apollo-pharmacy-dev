<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;
if ($analysis)
{
	$kw_issues = array();
	$kw_issues_count = $analysis['issues']['keyword_density'][2]['sum'] + $analysis['issues']['keyword_density'][3]['sum'] + $analysis['issues']['keyword_density'][0]['sum'];
	if ($analysis['issues']['keyword_density'][0]['sum'])
		$kw_issues[] = array('class' => 'error', 'title' => __( 'Keyword is not set', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['keyword_density'][0]['posts']), $analysis['issues']['keyword_density'][0]['sum'].' issues', array('msg' => 'Keyword is not set'), true), 'count' => $analysis['issues']['keyword_density'][0]['sum']);
	//if ($analysis['issues']['keyword_density'][1]['sum'])
		//$kw_issues[] = array('class' => 'success', 'title' => 'Keyword Density is good!', 'issue' => $analysis['issues']['keyword_density'][1].' issues');
	if ($analysis['issues']['keyword_density'][2]['sum'])
		$kw_issues[] = array('class' => 'warning', 'title' => __( 'Keyword Density is too low', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['keyword_density'][2]['posts']), $analysis['issues']['keyword_density'][2]['sum'].' issues', array('msg' => 'Keyword Density is too low'), true), 'count' => $analysis['issues']['keyword_density'][2]['sum']);
	if ($analysis['issues']['keyword_density'][3]['sum'])
		$kw_issues[] = array('class' => 'warning', 'title' => __( 'Keyword Density is too high', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['keyword_density'][3]['posts']), $analysis['issues']['keyword_density'][3]['sum'].' issues', array('msg' => 'Keyword Density is too high'), true), 'count' => $analysis['issues']['keyword_density'][3]['sum']);

	$title_issues = array();
	$title_issues_count = $analysis['issues']['title_length'][2]['sum'] + $analysis['issues']['title_length'][3]['sum'] + $analysis['issues']['title_length'][0]['sum'] + $analysis['issues']['title_prio1'][0]['sum'];
	//if ($analysis['issues']['title_length'][1]['sum'])
		//$title_issues[] = array('class' => 'success', 'title' => 'Title Length is good!', 'issue' => $analysis['issues']['title_length'][1].' issues');
	if ($analysis['issues']['title_length'][2]['sum'])
		$title_issues[] = array('class' => 'warning', 'title' => __( 'Title Length is too short', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['title_length'][2]['posts']), $analysis['issues']['title_length'][2]['sum'].' issues', array('msg' => 'Title Length is too short'), true), 'count' => $analysis['issues']['title_length'][2]['sum']);
	if ($analysis['issues']['title_length'][3]['sum'])
		$title_issues[] = array('class' => 'warning', 'title' => __( 'Title Length is too long', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['title_length'][3]['posts']), $analysis['issues']['title_length'][3]['sum'].' issues', array('msg' => 'Title Length is too long'), true), 'count' => $analysis['issues']['title_length'][3]['sum']);
	if ($analysis['issues']['title_length'][0]['sum'])
		$title_issues[] = array('class' => 'error', 'title' => __( 'Title is not set', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['title_length'][0]['posts']), $analysis['issues']['title_length'][0]['sum'].' issues', array('msg' => 'Title is not set'), true), 'count' => $analysis['issues']['title_length'][0]['sum']);
	//if ($analysis['issues']['title_prio1'][1]['sum'])
		//$title_issues[] = array('class' => 'success', 'title' => 'Title includes Prio 1 Keyword', 'issue' => $analysis['issues']['title_prio1'][1].' issues');
	if ($analysis['issues']['title_prio1'][0]['sum'])
		$title_issues[] = array('class' => 'error', 'title' => __( 'Title has no Prio 1 Keyword', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['title_prio1'][0]['posts']), $analysis['issues']['title_prio1'][0]['sum'].' issues', array('msg' => 'Title has no Prio 1 Keyword'), true), 'count' => $analysis['issues']['title_prio1'][0]['sum']);

	$desc_issues = array();
	$desc_issues_count = $analysis['issues']['desc_length'][2]['sum'] + $analysis['issues']['desc_length'][3]['sum'] + $analysis['issues']['desc_length'][0]['sum'] + $analysis['issues']['desc_prio1'][0]['sum'];
	//if ($analysis['issues']['desc_length'][1]['sum'])
		//$desc_issues[] = array('class' => 'success', 'title' => 'Description Length is good!', 'issue' => $analysis['issues']['desc_length'][1].' issues');
	if ($analysis['issues']['desc_length'][2]['sum'])
		$desc_issues[] = array('class' => 'warning', 'title' => __( 'Description Length is too short', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['desc_length'][2]['posts']), $analysis['issues']['desc_length'][2]['sum'].' issues', array('msg' => 'Description Length is too short'), true), 'count' => $analysis['issues']['desc_length'][2]['sum']);
	if ($analysis['issues']['desc_length'][3]['sum'])
		$desc_issues[] = array('class' => 'warning', 'title' => __( 'Description Length is too long', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['desc_length'][3]['posts']), $analysis['issues']['desc_length'][3]['sum'].' issues', array('msg' => 'Description Length is too long'), true), 'count' => $analysis['issues']['desc_length'][3]['sum']);
	if ($analysis['issues']['desc_length'][0]['sum'])
		$desc_issues[] = array('class' => 'error', 'title' => __( 'Description is not set', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['desc_length'][0]['posts']), $analysis['issues']['desc_length'][0]['sum'].' issues', array('msg' => 'Description is not set'), true), 'count' => $analysis['issues']['desc_length'][0]['sum']);
	//if ($analysis['issues']['desc_prio1'][1]['sum'])
		//$desc_issues[] = array('class' => 'success', 'title' => 'Description includes Prio 1 Keyword', 'issue' => $analysis['issues']['desc_prio1'][1].' issues');
	if ($analysis['issues']['desc_prio1'][0]['sum'])
		$desc_issues[] = array('class' => 'error', 'title' => __( 'Description has no Prio 1 Keyword', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['desc_prio1'][0]['posts']), $analysis['issues']['desc_prio1'][0]['sum'].' issues', array('msg' => 'Description has no Prio 1 Keyword'), true), 'count' => $analysis['issues']['desc_prio1'][0]['sum']);

	$content_issues = array();
	$content_issues_count = $analysis['issues']['word_count'][2]['sum'] + $analysis['issues']['word_count'][3]['sum'] + $analysis['issues']['word_count'][4]['sum'] + $analysis['issues']['word_count'][0]['sum'];
	//if ($analysis['issues']['word_count'][1]['sum'])
		//$content_issues[] = array('class' => 'success', 'title' => 'Content Length is good', 'issue' => $analysis['issues']['word_count'][1].' issues');
	if ($analysis['issues']['word_count'][2]['sum'])
		$content_issues[] = array('class' => 'warning', 'title' => __( 'Content Length is less than 50 Words', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['word_count'][2]['posts']), $analysis['issues']['word_count'][2]['sum'].' issues', array('msg' => 'Content Length is less than 50 Words'), true), 'count' => $analysis['issues']['word_count'][2]['sum']);
	if ($analysis['issues']['word_count'][3]['sum'])
		$content_issues[] = array('class' => 'warning', 'title' => __( 'Content Length is less than 100 Words', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['word_count'][3]['posts']), $analysis['issues']['word_count'][3]['sum'].' issues', array('msg' => 'Content Length is less than 100 Words'), true), 'count' => $analysis['issues']['word_count'][3]['sum']);
	if ($analysis['issues']['word_count'][4]['sum'])
		$content_issues[] = array('class' => 'warning', 'title' => __( 'Content Length is less than 200 Words', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['word_count'][4]['posts']), $analysis['issues']['word_count'][4]['sum'].' issues', array('msg' => 'Content Length is less than 200 Words'), true), 'count' => $analysis['issues']['word_count'][4]['sum']);
	//if ($analysis['issues']['word_count'][5]['sum'])
		//$content_issues[] = array('class' => 'success', 'title' => 'Content Length is less than 400 Words', 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['word_count'][5]['posts']), $analysis['issues']['word_count'][5]['sum'].' issues', array(), true));
	if ($analysis['issues']['word_count'][0]['sum'])
		$content_issues[] = array('class' => 'error', 'title' => __( 'There is no Content!', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['word_count'][0]['posts']), $analysis['issues']['word_count'][0]['sum'].' issues', array('msg' => 'There is no Content!'), true), 'count' => $analysis['issues']['word_count'][0]['sum']);

	$heading_issues = array();
	$heading_issues_count = $analysis['issues']['heading_h1_count'][0]['sum'] + $analysis['issues']['heading_h1_count'][2]['sum'] + $analysis['issues']['heading_h1_prio1'][0]['sum'] + $analysis['issues']['heading_h1_prio1_count'][0]['sum'] +
							//$analysis['issues']['heading_h1_prio1_den'][2]['sum'] + $analysis['issues']['heading_h1_prio1_den'][3]['sum'] + $analysis['issues']['heading_h1_prio1_den'][4]['sum'] + $analysis['issues']['heading_h1_prio1_den'][0]['sum'] +
							$analysis['issues']['heading_h2_250'][0]['sum'] + $analysis['issues']['heading_h2_500'][0]['sum'] + $analysis['issues']['heading_h2h3_count'][0]['sum'];
	//if ($analysis['issues']['heading_h1_count'][1]['sum'])
		//$heading_issues[] = array('class' => 'success', 'title' => 'Has H1', 'issue' => $analysis['issues']['heading_h1_count'][1].' issues');
	if ($analysis['issues']['heading_h1_count'][0]['sum'])
		$heading_issues[] = array('class' => 'error', 'title' => __( 'H1 not set', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_count'][0]['posts']), $analysis['issues']['heading_h1_count'][0]['sum'].' issues', array('msg' => 'Has no H1'), true), 'count' => $analysis['issues']['heading_h1_count'][0]['sum']);
	if ($analysis['issues']['heading_h1_count'][2]['sum'])
		$heading_issues[] = array('class' => 'error', 'title' => __( 'There is more than one H1 used!', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_count'][2]['posts']), $analysis['issues']['heading_h1_count'][2]['sum'].' issues', array('msg' => 'Has too many H1'), true), 'count' => $analysis['issues']['heading_h1_count'][2]['sum']);
	//if ($analysis['issues']['heading_h1_prio1'][1]['sum'])
		//$heading_issues[] = array('class' => 'success', 'title' => 'H1 has Prio1', 'issue' => $analysis['issues']['heading_h1_prio1'][1].' issues');
	if ($analysis['issues']['heading_h1_prio1'][0]['sum'])
		$heading_issues[] = array('class' => 'error', 'title' => __( 'H1 has no Prio 1 Keyword', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_prio1'][0]['posts']), $analysis['issues']['heading_h1_prio1'][0]['sum'].' issues', array('msg' => 'H1 has no Prio1'), true), 'count' => $analysis['issues']['heading_h1_prio1'][0]['sum']);
	//if ($analysis['issues']['heading_h1_prio1_count'][1]['sum'])
		//$heading_issues[] = array('class' => 'success', 'title' => 'H1 has all Prio1', 'issue' => $analysis['issues']['heading_h1_prio1_count'][1].' issues');
	if ($analysis['issues']['heading_h1_prio1_count'][0]['sum'])
		$heading_issues[] = array('class' => 'warning', 'title' => __( 'H1 has not all Prio 1 Keywords', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_prio1_count'][0]['posts']), $analysis['issues']['heading_h1_prio1_count'][0]['sum'].' issues', array('msg' => 'H1 has not all Prio1'), true), 'count' => $analysis['issues']['heading_h1_prio1_count'][0]['sum']);
	//if ($analysis['issues']['heading_h1_prio1_den'][1]['sum'])
		//$heading_issues[] = array('class' => 'success', 'title' => 'H1 Prio Keyword density is okay', 'issue' => $analysis['issues']['heading_h1_prio1_den'][1].' issues');
	//if ($analysis['issues']['heading_h1_prio1_den'][2]['sum'])
		//$heading_issues[] = array('class' => 'warning', 'title' => __( 'H1 Prio Keyword density is < 0.33', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_prio1_den'][2]['posts']), $analysis['issues']['heading_h1_prio1_den'][2]['sum'].' issues', array('msg' => 'H1 Prio Keyword density is < 0.33'), true));
	//if ($analysis['issues']['heading_h1_prio1_den'][3]['sum'])
		//$heading_issues[] = array('class' => 'warning', 'title' => __( 'H1 Prio Keyword density is < 0.25', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_prio1_den'][3]['posts']), $analysis['issues']['heading_h1_prio1_den'][3]['sum'].' issues', array('msg' => 'H1 Prio Keyword density is < 0.25'), true));
	//if ($analysis['issues']['heading_h1_prio1_den'][4]['sum'])
		//$heading_issues[] = array('class' => 'error', 'title' => __( 'H1 Prio Keyword density is < 0.18', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_prio1_den'][4]['posts']), $analysis['issues']['heading_h1_prio1_den'][4]['sum'].' issues', array('msg' => 'H1 Prio Keyword density is < 0.18'), true));
	//if ($analysis['issues']['heading_h1_prio1_den'][0]['sum'])
		//$heading_issues[] = array('class' => 'error', 'title' => __( 'H1 Prio Keyword density is zero!', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h1_prio1_den'][0]['posts']), $analysis['issues']['heading_h1_prio1_den'][0]['sum'].' issues', array('msg' => 'H1 Prio Keyword density is zero!'), true));
	//if ($analysis['issues']['heading_h2_250'][1]['sum'])
		//$heading_issues[] = array('class' => 'success', 'title' => 'H2 provided by over 250 words', 'issue' => $analysis['issues']['heading_h2_250'][1].' issues');
	if ($analysis['issues']['heading_h2_250'][0]['sum'] || $analysis['issues']['heading_h2_500'][0]['sum'])
		$heading_issues[] = array('class' => 'warning', 'title' => __( 'H2 Count too low', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles(!$analysis['issues']['heading_h2_250'][0]['posts'] ? $analysis['issues']['heading_h2_500'][0]['posts'] : (!$analysis['issues']['heading_h2_500'][0]['posts'] ? $analysis['issues']['heading_h2_250'][0]['posts'] : array_merge($analysis['issues']['heading_h2_250'][0]['posts'],$analysis['issues']['heading_h2_500'][0]['posts']))), ($analysis['issues']['heading_h2_250'][0]['sum']+$analysis['issues']['heading_h2_500'][0]['sum']).' issues', array('msg' => 'H2 Count too low'), true), 'count' => $analysis['issues']['heading_h2_250'][0]['sum']+$analysis['issues']['heading_h2_500'][0]['sum']);
	//if ($analysis['issues']['heading_h2_500'][1]['sum'])
		//$heading_issues[] = array('class' => 'error', 'title' => 'H2 provided by over 500 words', 'issue' => $analysis['issues']['heading_h2_500'][1]['sum'].' issues');
	//if ($analysis['issues']['heading_h2_500'][0]['sum'])
		//$heading_issues[] = array('class' => 'error', 'title' => __( 'H2 Count too low', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h2_500'][0]['posts']), $analysis['issues']['heading_h2_500'][0]['sum'].' issues', array('msg' => 'H2 Count too low'), true), 'count' => $analysis['issues']['heading_h2_500'][0]['sum']);
	//if ($analysis['issues']['heading_h2h3_count'][1]['sum'])
		//$heading_issues[] = array('class' => 'error', 'title' => 'H2/H3 found', 'issue' => $analysis['issues']['heading_h2h3_count'][1]['sum'].' issues');
	if ($analysis['issues']['heading_h2h3_count'][0]['sum'])
		$heading_issues[] = array('class' => 'error', 'title' => __( 'No H2/H3 found', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['heading_h2h3_count'][0]['posts']), $analysis['issues']['heading_h2h3_count'][0]['sum'].' issues', array('msg' => 'No H2/H3 found'), true), 'count' => $analysis['issues']['heading_h2h3_count'][0]['sum']);

	$url_issues = array();
	$url_issues_count = $analysis['issues']['url_length'][0]['sum'] + $analysis['issues']['url_prio1'][0]['sum'];
	//if ($analysis['issues']['url_length'][1]['sum'])
		//$url_issues[] = array('class' => 'success', 'title' => 'URL length OK', 'issue' => $analysis['issues']['url_length'][1].' issues');
	if ($analysis['issues']['url_length'][0]['sum'])
		$url_issues[] = array('class' => 'warning', 'title' => __( 'URL length too long', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['url_length'][0]['posts']), $analysis['issues']['url_length'][0]['sum'].' issues', array('msg' => 'URL length too long'), true), 'count' => $analysis['issues']['url_length'][0]['sum']);
	//if ($analysis['issues']['url_prio1'][1]['sum'])
		//$url_issues[] = array('class' => 'success', 'title' => 'URL has Prio1', 'issue' => $analysis['issues']['url_prio1'][1].' issues');
	if ($analysis['issues']['url_prio1'][0]['sum'])
		$url_issues[] = array('class' => 'warning', 'title' => __( 'URL has no Prio 1 Keyword', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['url_prio1'][0]['posts']), $analysis['issues']['url_prio1'][0]['sum'].' issues', array('msg' => 'URL has no Prio1'), true), 'count' => $analysis['issues']['url_prio1'][0]['sum']);

	$media_issues = array();
	$media_issues_count = $analysis['issues']['media'][0]['sum'] + $analysis['issues']['media_missing_alt'][0]['sum'];
	//if ($analysis['issues']['media'][1]['sum'])
		//$media_issues[] = array('class' => 'success', 'title' => 'Media found', 'issue' => $analysis['issues']['media'][1].' issues');
	if ($analysis['issues']['media'][0]['sum'])
		$media_issues[] = array('class' => 'warning', 'title' => __( 'No Media found!', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['media'][0]['posts']), $analysis['issues']['media'][0]['sum'].' issues', array('msg' => 'No Media found!'), true), 'count' => $analysis['issues']['media'][0]['sum']);
	//if ($analysis['issues']['media_missing_alt'][1]['sum'])
		//$media_issues[] = array('class' => 'success', 'title' => 'No missing alt on Media elements', 'issue' => $analysis['issues']['media_missing_alt'][1].' issues');
	if ($analysis['issues']['media_missing_alt'][0]['sum'])
		$media_issues[] = array('class' => 'error', 'title' => __( 'Missing alt on media elements', 'wsko' ), 'issue' => WSKO_Class_Template::render_content_optimizer_multi_link(WSKO_Class_Helper::get_post_titles($analysis['issues']['media_missing_alt'][0]['posts']), $analysis['issues']['media_missing_alt'][0]['sum'].' issues', array('msg' => 'Missing alt on media elements'), true), 'count' => $analysis['issues']['media_missing_alt'][0]['sum']);

	if ($kw_issues)
		WSKO_Class_Template::render_issueGroup(array('title' => __('Keyword Density:', 'wsko') . '<span class="text-off count-issues">' . ($kw_issues_count?' '.$kw_issues_count.' issues</span>':''), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'parent' => 'wsko_onpage_issues', 'active' => true, 'items' => $kw_issues));
	if ($title_issues)
		WSKO_Class_Template::render_issueGroup(array('title' => __('Meta Titles:', 'wsko') . '<span class="text-off count-issues">' . ($title_issues_count?' '.$title_issues_count.' issues</span>':''), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'parent' => 'wsko_onpage_issues', 'items' => $title_issues));
	if ($desc_issues)
		WSKO_Class_Template::render_issueGroup(array('title' => __('Meta Descriptions:', 'wsko') . '<span class="text-off count-issues">' .  ($desc_issues_count?' '.$desc_issues_count.' issues</span>':''), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'parent' => 'wsko_onpage_issues', 'items' => $desc_issues));
	if ($content_issues)
		WSKO_Class_Template::render_issueGroup(array('title' => __('Content Length:', 'wsko') . '<span class="text-off count-issues">' .  ($content_issues_count?' '.$content_issues_count.' issues</span>':''), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'parent' => 'wsko_onpage_issues', 'items' => $content_issues));
	if ($heading_issues)
		WSKO_Class_Template::render_issueGroup(array('title' => __('Headings:', 'wsko') . '<span class="text-off count-issues">' .  ($heading_issues_count?' '.$heading_issues_count.' issues</span>':''), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'parent' => 'wsko_onpage_issues', 'items' => $heading_issues));						
	if ($url_issues)
		WSKO_Class_Template::render_issueGroup(array('title' => __('Permalinks:', 'wsko') . '<span class="text-off count-issues">' .  ($url_issues_count?' '.$url_issues_count.' issues</span>':''), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'parent' => 'wsko_onpage_issues', 'items' => $url_issues));
	if ($media_issues)
		WSKO_Class_Template::render_issueGroup(array('title' => __('Media:', 'wsko') . '<span class="text-off count-issues">' .  ($media_issues_count?' '.$media_issues_count.' issues</span>':''), 'col' => 'col-md-12 col-sm-12 col-xs-12', 'parent' => 'wsko_onpage_issues', 'items' => $media_issues));
}
?>