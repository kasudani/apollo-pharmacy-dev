<?php
if (!defined('ABSPATH')) exit;

$post_types = WSKO_Class_Helper::get_public_post_types('objects');
$post_types_san = array();
foreach ($post_types as $pt)
{
	$post_types_san[$pt->name] = $pt->label;
}
unset($post_types['post']);
unset($post_types['page']);
$taxonomies = get_taxonomies(array('public' => true), 'objects');
?>
<div class="row">
	<div class="bsu-panel bsu-panel-custom panel-group col-md-12 col-xs-12">
		<div class="panel panel-default">
			<p class="panel-heading m0"><a class="dark" data-toggle="collapse" href="#settings"><span class="pull-right"><i class="fa fa-angle-down font-unimportant"></i></span><?=__('404 to 301 Settings', 'wsko');?></a></p>
			<div id="settings" class="panel-collapse collapse">
				<div class="panel-body">
					<form id="wsko_automatic_redirect_form" data-nonce="<?=wp_create_nonce('wsko_update_automatic_redirect')?>">
						<div class="row form-group">
							<div class="col-md-3">
								Activate 404 to 301
							</div>
							<div class="col-md-9">
								<input class="form-control wsko-field-activate" type="checkbox" <?=isset($onpage_data['redirect_404']) && $onpage_data['redirect_404']['activate'] ? 'checked' : ''?>>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
								Redirect to
							</div>
							<div class="col-md-9">
								<select class="form-control wsko-field-type">
									<option value="1" <?=isset($onpage_data['redirect_404']) && $onpage_data['redirect_404']['type'] == '1' ? 'selected' : ''?>>Home URL (<?=home_url()?>)</option>
									<?php if (WSKO_Class_Helper::get_host_base() != home_url()) {?><option value="2" <?=isset($onpage_data['redirect_404']) && $onpage_data['redirect_404']['type'] == '2' ? 'selected' : ''?>>Domain Home URL (<?=WSKO_Class_Helper::get_host_base()?>)</option><?php } ?>
									<option value="3" <?=isset($onpage_data['redirect_404']) && $onpage_data['redirect_404']['type'] == '3' ? 'selected' : ''?>>Custom URL</option>
									<option value="4" <?=isset($onpage_data['redirect_404']) && $onpage_data['redirect_404']['type'] == '4' ? 'selected' : ''?>>Parent Directory</option>
								</select>
							</div>
						</div>
						<div class="row form-group wsko-field-custom-wrapper">
							<div class="col-md-3">
								Custom URL
							</div>
							<div class="col-md-9">
								<input class="form-control wsko-field-custom" type="text" placeholder="/relative/link/ (optional)" value="<?=isset($onpage_data['redirect_404']) ? $onpage_data['redirect_404']['custom'] : ''?>">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
							</div>
							<div class="col-md-9">
								<button class="button wsko-save-btn" type="submit">Save</button>
							</div>
						</div>
					</form>
				</div>	
			</div>	
		</div>
	</div>
	<div class="col-md-12 col-sm-12">
		<div class="bsu-panel bsu-panel-custom">
			<div class="panel panel-default">
				<p class="panel-heading m0">Permalink settings</p>
				<div class="panel-body">
					<!--div class="row form-group">
						<div class="col-md-3">
							<p>Hide post type slug</p>
							<small class="text-off">Every post is rewritten to cut of the slug added by the corresponding post type. Beware of possible duplicates!</small>
						</div>
						<div class="col-md-9">
							<label>
								<input class="form-control wsko-ajax-input" type="checkbox" <?=WSKO_Class_Core::get_setting('hide_post_types') ? 'checked="checked"' : ''?> data-wsko-target="settings" data-wsko-setting="hide_post_types">
								Hide slugs
							</label>
						</div>
					</div-->
					<div class="row form-group border-bottom" style="padding-bottom:15px;">
						<div class="col-sm-6 col-xs-12">
							<div class="row">
								<div class="col-md-6">
									<p>Hide category base</p>
									<small class="text-off">Category slug is hidden from the URL, so <i><?=home_url('/category/term-name/')?></i> becomes <i><?=home_url('/term-name/')?></i></small>
								</div>
								<div class="col-md-6">
									<label>
										<input class="form-control wsko-ajax-input" type="checkbox" <?=WSKO_Class_Core::get_setting('hide_category_slug') ? 'checked="checked"' : ''?> data-wsko-target="settings" data-wsko-setting="hide_category_slug" data-reload="true" data-alert="Do you want to activate redirects from your old category structure?" data-alert-send="true">
										Hide category 
									</label>
								</div>
							</div>	
						</div>
						<div class="col-sm-6 col-xs-12">
							<div class="row">
								<div class="col-md-6">
									<p>Redirect old category links</p>
									<small class="text-off">If checked, BST will redirect the old category URLs (with/without "category") to the new structure. E.G: If you have hidden the category base, activating this option will redirect "/category/general" to "/general"</i></small>
								</div>
								<div class="col-md-6">
									<label>
										<input class="form-control wsko-ajax-input" type="checkbox" <?=WSKO_Class_Core::get_setting('hide_category_slug_redirect') ? 'checked="checked"' : ''?> data-wsko-target="settings" data-wsko-setting="hide_category_slug_redirect">
										Activate Category Redirect
									</label>
								</div>
							</div>	
						</div>	
					</div>
					<div class="row form-group">
						<div class="col-md-3">
							<p>Auto Post Redirect</p>
							<small class="text-off">Auto-add 301 redirects, when one of your posts URL is changed</small>
						</div>
						<div class="col-md-9">
							<label>
								<input class="form-control wsko-ajax-input" type="checkbox" <?=WSKO_Class_Core::get_setting('auto_post_slug_redirects') ? 'checked="checked"' : ''?> data-wsko-target="settings" data-wsko-setting="auto_post_slug_redirects">
								Activate Auto Redirect
							</label>
						</div>
					</div>
				</div>	
			</div>
		</div>	
	</div>
	<div class="col-sm-12">
		<!--p class="hidden-xs pull-right" style="margin:10px 5px;"><a class="dark text-off wsko-small" target="_blank" href="https://www.bavoko.tools/en/knowledge_base/generate-dynamic-meta-descriptions-titles-in-wordpress/"><i class="fa fa-info fa-fw"></i> Knowledge Base: Generate Dynamic Meta Titles & Descriptions</a></p-->
		<ul class="nav nav-tabs bsu-tabs border-dark" style="margin-bottom: 10px;">
			<li class="active"><a data-toggle="tab" href="#wsko_links_post_type"><?=__( 'Post Types', 'wsko' )?></a></li>
			<li><a data-toggle="tab" href="#wsko_links_tax"><?=__( 'Taxonomies', 'wsko' )?></a></li>
			<li><a data-toggle="tab" href="#wsko_links_bulk"><?=__( 'Bulk Tools', 'wsko' )?></a></li>
		</ul>

		<div class="tab-content">
			<div id="wsko_links_post_type" class="tab-pane fade in active">
				<p class="wsko-text-off"><i class="fa fa-info fa-fw"></i> Change post type slugs</p>
				<div class="panel-group" id="wsko_meta_post_type_wrapper">
					<?php
					$f = true;
					if ($post_types) {
						foreach ($post_types as $type)
						{
							$data = WSKO_Class_Helper::get_obj_rewrite_base('post_type', $type->name);
							if ($data)
							{
								$first_slug = $data['original'];
								$curr_slug = $data['current'];
							}
							else
							{
								$first_slug = "<i>not provided</i>";
								$curr_slug =  "<i>not provided</i>";
							}
							$url = get_post_type_archive_link($type->name);
							if (strpos($url, '?') !== false)
							{
								$url .= '&'.$type->name.'=post-name';
							}
							else
							{
								$url .= (WSKO_Class_Helper::ends_with($url, '/')?'':'/').'post-name/';
								$url = str_replace('/'.$curr_slug.'/', '<b>/'.$curr_slug.'/</b>', $url);
							}
							WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => $type->label." <span class='panel-info'>Post Type '".$type->name."' • ".$url."<span>", 'parent' => 'wsko_meta_post_type_wrapper', 'active' => $f, 'lazyVar' => 'post_type_'.$type->name, 'class' => 'wsko-collapse-page'));
							$f = false;
						} 
					} else {
						echo "<p class='wsko-text-off'>No post types available</p>";
					}	?>
				</div>
			</div>
			<div id="wsko_links_tax" class="tab-pane fade">
				<p class="wsko-text-off"><i class="fa fa-info fa-fw"></i> Change taxonomy slugs</p>
				<div class="panel-group" id="wsko_meta_tax_wrapper">
					<?php
					$f = true;
					foreach ($taxonomies as $type)
					{
						$data = WSKO_Class_Helper::get_obj_rewrite_base('post_tax', $type->name);
						if ($data)
						{
							$first_slug = $data['original'];
							$curr_slug = $data['current'];
						}
						else
						{
							$first_slug = "<i>not provided</i>";
							$curr_slug =  "<i>not provided</i>";
						}
						$url = home_url($curr_slug.'/a-term/');
						$url = str_replace('/'.$curr_slug.'/', '<b>/'.$curr_slug.'/</b>', $url);
						WSKO_Class_Template::render_panel(array('type' => 'collapse', 'title' => $type->label." <span class='panel-info'>Taxonomy '".$type->name."' • ".$url."<span>", 'parent' => 'wsko_meta_tax_wrapper', 'active' => $f, 'lazyVar' => 'post_tax_'.$type->name, 'class' => 'wsko-collapse-page'));
						$f = false;
					} ?>
				</div>
			</div>
			<div id="wsko_links_bulk" class="tab-pane fade">
				<p class="wsko-text-off"><i class="fa fa-info fa-fw"></i> Bulk tools for post slugs</p>
				<div class="bsu-panel bsu-panel-chart">
					<div class="panel panel-default wsko-panel-bulk-tools">
						<p class="panel-heading m0"><?=__('Bulk Tools', 'wsko')?></p>
						<?php WSKO_Class_Template::render_table(array('Post', 'Links'), array(), array('ajax' => array('action' => 'wsko_table_onpage', 'arg' => '11'), 'filter' => array('post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => $post_types_san)))); ?>
					</div>
				</div>	
			</div>
		</div>
	</div>	
</div>