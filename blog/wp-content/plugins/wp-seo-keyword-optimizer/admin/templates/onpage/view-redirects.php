<?php
if (!defined('ABSPATH')) exit;

$table_redirects = isset($template_args['redirects']) && $template_args['redirects'] ? $template_args['redirects'] : array();
$table_page_redirects = isset($template_args['page_redirects']) && $template_args['page_redirects'] ? $template_args['page_redirects'] : array();
$table_auto_page_redirects = isset($template_args['atuo_post_redirects']) && $template_args['atuo_post_redirects'] ? $template_args['atuo_post_redirects'] : array();
$table_auto_pt_redirects = isset($template_args['auto_post_type_redirects']) && $template_args['auto_post_type_redirects'] ? $template_args['auto_post_type_redirects'] : array();
?>

<div class="p15" style="padding-bottom:0px;">
	<ul class="bsu-tabs nav nav-tabs">
		<li class="active"><a href="#wsko_redirects_general" data-toggle="tab">Custom Redirects (<?=count($table_redirects)?>)</a></li>
		<?php /*	
			<li><a href="#wsko_redirects_auto_types" data-toggle="tab">Auto Post Type Redirects (<?=count($table_auto_pt_redirects)?>)</a></li>
			<li><a href="#wsko_redirects_auto_posts" data-toggle="tab">Auto Page Redirects (<?=count($table_auto_page_redirects)?>)</a></li>
		*/ ?>
		<li><a href="#wsko_redirects_pages" data-toggle="tab">Content Optimizer Redirects (<?=count($table_page_redirects)?>)</a></li>
	</ul>
</div>	
<div class="tab-content">
	<div id="wsko_redirects_general" class="tab-pane fade in active">
		<?php WSKO_Class_Template::render_table(array('Comp', 'Page', 'Type', 'Comp to', 'Redirect to', 'Options'), $table_redirects, array());?>
	</div>
	<div id="wsko_redirects_pages" class="tab-pane fade">
		<?php WSKO_Class_Template::render_table(array('Page', 'Type', 'Redirect to', 'Options'), $table_page_redirects, array());?>
	</div>
	<?php /*
	<div id="wsko_redirects_auto_types" class="tab-pane fade">
		<?php WSKO_Class_Template::render_table(array('Post Type', 'Redirects', 'Options'), $table_auto_pt_redirects, array());?>
	</div>
	<div id="wsko_redirects_auto_posts" class="tab-pane fade">
		<?php WSKO_Class_Template::render_table(array('Page', 'Redirects', 'Options'), $table_auto_page_redirects, array());?>
	</div>
	*/ ?>
</div>