<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;
$chart_dist = array();
if ($analysis)
{
	foreach($analysis['content_length_dist'] as $k => $dupl)
	{
		$chart_dist[]= array($k, $dupl);
	}
}
$matrix_content = array(array('word_count:0:100','word_count:100:200','word_count:200:300','word_count:300:500','word_count:500:700','word_count:700:1000','word_count:1000:1500','word_count:1500:2000','word_count:2000:3000','word_count:3000:'.$analysis['max']['word_count']));
?>
<div class="row">
	<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Content Length Distribution', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
		'custom' => $chart_dist ? WSKO_Class_Template::render_chart('column', array('Content Length', 'Page Count'), $chart_dist, array('axisTitle' => 'Word Count', 'table_filter' => array('table' => '#wsko_onpage_analysis_content_table', 'value_matrix' => $matrix_content), 'chart_id' => 'content_length'), true) : WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true))); ?>
			
	<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
		'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', /*'Content Length',*/ 'Word Count', ''), array(), array('id' => 'wsko_onpage_analysis_content_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '4'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), /*'content_length' => array('title' => 'Content Length', 'type' => 'number_range', 'max' => $analysis['max']['content_length']),*/ 'word_count' => array('title' => 'Word Count', 'type' => 'number_range', 'max' => $analysis['max']['word_count']))), true))); ?>
</div>