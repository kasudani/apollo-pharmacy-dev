<?php
if (!defined('ABSPATH')) exit;

$analysis = isset($template_args['analysis']['current_report']) ? $template_args['analysis']['current_report'] : false;
$chart_dist = array();
$chart_den_dist = array();
/*foreach($analysis['priority_kw_den_dist'] as $k => $dupl)
{
	if ($k == 'prio1')
		$chart_den_dist[]= array($k, $dupl[0], $dupl[2], $dupl[1], $dupl[3]);
}*/
if ($analysis)
{
	foreach($analysis['priority_kw_dist'] as $k => $dupl)
	{
		if ($k == 'prio1')
			$chart_dist[]= array($k, $dupl[0], $dupl[1]);
	}
}
$matrix_keywords = array(
	array('prio1_kw_den:off'), array('prio1_kw_den:on'),
);
?>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		<ul class="nav nav-tabs bsu-tabs border-dark">
			<li class="waves-effect active"><a data-toggle="tab" href="#wsko_onpage_analysis_keywords_pages"><?=__( 'Pages', 'wsko' )?></a></li>
			<li class="waves-effect"><a data-toggle="tab" href="#wsko_onpage_analysis_keywords_keywords"><?=__( 'Keywords', 'wsko' )?></a></li>
		</ul>
		<div class="row">
			<div class="tab-content">
				<div id="wsko_onpage_analysis_keywords_pages" class="tab-pane fade in active">
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Priority Keywords', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
					'custom' => $chart_dist ? WSKO_Class_Template::render_chart('column', array('Priority', 'Not Set', 'Set'), $chart_dist, array('isStacked' => true, 'chart_id' => 'prio_keywords', 'table_filter' => array('table' => '#wsko_onpage_analysis_keywords_table', 'value_matrix' => $matrix_keywords)), true) : WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true))); ?>
					
				
					<?php WSKO_Class_Template::render_panel(array('type' => 'custom', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 
						'custom' => WSKO_Class_Template::render_table(array('CS' . WSKO_Class_Template::render_infoTooltip('Content Score', 'info', true), 'Page', 'Prio 1 Keywords', ''/*, 'Prio 2 Keywords', 'Prio 3 Keywords'*/), array(), array('id' => 'wsko_onpage_analysis_keywords_table', 'ajax' => array('action' => 'wsko_table_onpage', 'arg' => '5'), 'filter' => array('onpage_score' => array('title' => 'Onpage Score', 'type' => 'number_range', 'max' => $analysis['max']['onpage_score']), 'url' => array('title' => 'URL', 'type' => 'text'), 'post_type' => array('title' => 'Post Type', 'type' => 'select', 'values' => isset($analysis['post_types']) ? WSKO_Class_Helper::get_post_type_labels($analysis['post_types']) : array()), 'prio1_kw_den' => array('title' => 'Prio Keywords', 'type' => 'set'))), true))); ?>
				</div>
				<div id="wsko_onpage_analysis_keywords_keywords" class="tab-pane fade">
					<div class="align-center">
						<p class="wsko-text-off">COMING SOON</p>
					</div>	
				</div>
			</div>
		</div>	
</div>