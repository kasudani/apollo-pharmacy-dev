<?php
if (!defined('ABSPATH')) exit;

$onpage_data = WSKO_Class_Onpage::get_onpage_data();
$sitemap_params = WSKO_Class_Onpage::get_sitemap_params();

$post_types = WSKO_Class_Helper::get_public_post_types('objects');
$post_stati = get_post_stati(array('public' => true), 'objects');

$sitemap_path = get_home_path().'sitemap.xml';
$sitemap_real_path = get_home_path().'sitemap_bst.xml';
$sitemap_exists = file_exists($sitemap_path);
$sitemap_real_exists = file_exists($sitemap_real_path);

?>
<div class="row">
	<?php
	if (current_user_can('manage_options')) {
		if ((($sitemap_exists && is_writable($sitemap_path)) || (!$sitemap_exists && is_writable(get_home_path()))) && (($sitemap_real_exists && is_writable($sitemap_real_path)) || (!$sitemap_real_exists && is_writable(get_home_path()))))
		{ 		
			if (WSKO_Class_Core::get_setting('automatic_sitemap')) {	?>
				<div class="col-sm-12 col-xs-12">
					<div class="bs-callout wsko-notice wsko-notice-success" style="margin-bottom: 15px;">
						<p>Please follow <a href="https://support.google.com/webmasters/answer/183668" target="_blank">this instruction</a> to submit your sitemap to the Google Search Console manually.</p>
					</div>
				</div>	
			<?php } ?>	
			<div class="bsu-panel bsu-panel-custom col-md-12">
				<div class="panel panel-default">
					<p class="panel-heading m0">Current Sitemap</p>
					<div class="panel-body">
						<div class="wsko-row row form-group">
							<div class="wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12">
								<p>Download current XML Sitemap</p>
								<small class="text-off"><?=home_url('sitemap_bst.xml')?></small>
							</div>
							<div class="wsko-col-sm-9 wsko-col-xs-12 col-sm-9 col-xs-12">
								<?php /* <a class="btn btn-flat" href="<?=home_url('sitemap.xml')?>" target="_blank" download>Download Sitemap Register</a> */ ?>
								<?php 
								if ($sitemap_real_exists) { ?>
									<a class="btn btn-flat" href="<?=home_url('sitemap_bst.xml')?>" target="_blank" download>Download Sitemap</a>
								<?php } else { ?>
									<span class="text-off">No Sitemap available.</span>
								<?php } ?>
							</div>
						</div>	
					</div>	
				</div>
			</div>
			<div class="bsu-panel bsu-panel-custom col-sm-12">
				<div class="panel panel-default">
					<p class="panel-heading m0"><?=__('Sitemap','wsko')?> <span class="wsko-badge badge badge-<?=WSKO_Class_Core::get_setting('automatic_sitemap') ? 'success' : 'error'?> pull-right" style="float: none !important; margin: 0px; margin-left: 10px;"><?=WSKO_Class_Core::get_setting('automatic_sitemap') ? __('Active - Last update', 'wsko') . ' '. (isset($onpage_data['last_sitemap_upload']) ? date('d M, Y H:i', $onpage_data['last_sitemap_upload']) : __('Never', 'wsko')) : __('Inactive', 'wsko') ;?></span></p>
					<div class="panel-body">
						<div class="wsko-row row form-group">
							<div class="wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12">
								<p><?=__('Activate Sitemap', 'wsko');?></p>
								<p class="font-unimportant"><?=__('Sitemap will be automatically generated every hour.', 'wsko');?></p>
							</div>
							<div class="wsko-col-sm-9 wsko-col-xs-12 col-sm-9 col-xs-12">
								<label><input class="wsko-sitemap-param-activate form-control" type="checkbox" <?=WSKO_Class_Core::get_setting('automatic_sitemap') ? 'checked="checked"' : ''?>><?=__('Automatic Sitemap Generation', 'wsko')?></label>
							</div>
						</div>
						<div class="wsko-row row form-group">
							<div class="wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12">
								<p><?=__('Activate Ping', 'wsko');?></p>
								<p class="font-unimportant"><?=__('Sitemap will be sent to Google and Bing in hourly intervals after you changed something in your content.', 'wsko');?></p>
							</div>
							<div class="wsko-col-sm-9 wsko-col-xs-12 col-sm-9 col-xs-12">
								<label><input class="wsko-sitemap-param-ping form-control" type="checkbox" <?=WSKO_Class_Core::get_setting('automatic_sitemap_ping') ? 'checked="checked"' : ''?>><?=__('Automatic Sitemap Ping', 'wsko')?></label>
							</div>
						</div>
						<div class="wsko-row row form-group">
							<div class="wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12">
								<p><?=__('Post Type Settings', 'wsko');?></p>
								<p class="font-unimportant"><?=__('Select and configure post types, that should be included in the sitemap', 'wsko');?></p>
							</div>
							<div class="wsko-col-sm-9 wsko-col-xs-12 col-sm-9 col-xs-12">
									<div class="row">
										<div class="col-sm-3 col-xs-3 col-sm-offset-6 align-right">
											<span>Frequency <?=WSKO_Class_Template::render_infoTooltip(__('The estimate rate you are going to update your website with.', 'wsko'), 'info');?></span>
										</div>
										<div class="col-sm-3 col-xs-3 align-right">
											<span>Priority <?=WSKO_Class_Template::render_infoTooltip(__('Set a value between 0 and 1 for low or respective high priority.' ,'wsko'), 'info');?></span>
										</div>
									</div>
									<ul style="list-style-type:none;max-height:300px;overflow-y:auto;overflow-x:hidden;">
										<?php 
										foreach($post_types as $pt)
										{
											$data = false;
											if (isset($sitemap_params['types'][$pt->name]))
												$data = $sitemap_params['types'][$pt->name];
												
											?><li class="wsko-sitemap-param-type">
												<div class="row">
													<div class="col-sm-6 col-xs-12">
														<label>
															<input class="form-control wsko-sitemap-type-activate" type="checkbox" name="post_types[]" value="<?=$pt->name?>" <?=(!$data && ($pt->name == 'page' || $pt->name == 'post')) ? 'checked' : ''?><?=$data ? 'checked' : ''?>> <?=$pt->label?> (<?=$pt->name?>)
														</label>
													</div>
													<div class="col-sm-6 col-xs-12 align-right">
														<div class="wsko-sitemap-sub-params">
															<div class="row">
																<div class="col-sm-6 col-xs-6">
																	<select class="wsko-sitemap-subparam-freq form-control" <?=!$data ? 'disabled' : ''?>>
																		<option value="always" <?=$data && $data['freq'] == 'always' ? 'selected' : ''?>><?=__('Always (generated new every time)', 'wsko')?></option>
																		<option value="hourly" <?=$data && $data['freq'] == 'hourly' ? 'selected' : ''?>><?=__('Hourly', 'wsko')?></option>
																		<option value="daily" <?=$data && $data['freq'] == 'daily' ? 'selected' : ''?>><?=__('Daily', 'wsko')?></option>
																		<option value="weekly" <?=$data && $data['freq'] == 'weekly' ? 'selected' : ''?>><?=__('Weekly', 'wsko')?></option>
																		<option value="monthly" <?=!$data || $data && $data['freq'] == 'monthly' ? 'selected' : ''?>><?=__('Monthly', 'wsko')?></option>
																		<option value="yearly" <?=$data && $data['freq'] == 'yearly' ? 'selected' : ''?>><?=__('Yearly', 'wsko')?></option>
																		<option value="never" <?=$data['freq'] == 'never' ? 'selected' : ''?>><?=__('Never (archived)', 'wsko')?></option>
																	</select>
																</div>
																<div class="col-sm-6 col-xs-6">
																	<input class="wsko-sitemap-subparam-prio form-control" placeholder="0.0 - 1.0 | Default: 0.5" <?=!$data ? 'disabled' : ''?> value="<?=$data && isset($data['prio']) && ($data['prio'] || $data['prio'] === 0) ? $data['prio'] : ''?>">
																</div>
															</div>	
														</div>
													</div>
												</div>	
											</li><?php
										}
										?>
									</ul>
							</div>
						</div>
						
						<?php /*
						<div class="wsko-row row form-group">
							<div class="wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12">
								<p>Exclude posts</p>
							</div>
							<div class="wsko-col-sm-9 wsko-col-xs-12 col-sm-9 col-xs-12">
								<input class="wsko-sitemap-param-excposts form-control" placeholder="e. g. 99, 219, 55" value="<?=isset($sitemap_params['excluded_posts']) ? implode(", ", $sitemap_params['excluded_posts']) : ''?>">
							</div>
						</div>
						*/ ?>

						<div style="display:block;">
							<div class="row form-group">
								<div class="col-md-9 col-sm-offset-3">	
									<a class="mb10 btn btn-flat" data-toggle="collapse" data-target="#advance_sitemap_settings"><?=__('Advanced', 'wsko');?></a>
								</div>
							</div>	
							
							<div id="advance_sitemap_settings" class="collapse">
								 <div class="wsko-row row form-group">
									<div class="wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12">
										<p>Public Post Status</p>
										<small class="text-off">Change this option, if one of your posts changes its post status to anything else but “published” because of a plugin or theme.</small>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12 col-sm-9 col-xs-12">
										<ul style="list-style-type:none;">
											<?php 
											foreach($post_stati as $ps)
											{
												?><li class="wsko-sitemap-param-status inline-block mr5"><label><input class="form-control wsko-sitemap-status-activate" type="checkbox" name="post_stati[]" value="<?=$ps->name?>" <?=in_array($ps->name, $sitemap_params['stati']) ? 'checked' : ''?>> <?=$ps->label?> (<?=$ps->name?>)</label></li><?php
											}
											?>
										</ul>
									</div>
								</div>	
							</div>	
						</div>	

						<div class="row form-group">
							<div class="col-md-9 col-sm-offset-3">
								<a href="#" class="button wsko-update-sitemap" data-nonce="<?=wp_create_nonce('wsko_update_sitemap')?>">Update Sitemap Settings</a>
							</div>
						</div>	
					</div>	
				</div>
			</div>
			
			<?php /*
			
			<div class="bsu-panel bsu-panel-custom col-md-12">
				<div class="panel panel-default">
					<p class="panel-heading m0">Current Sitemap</p>
					<?php
					if (!$sitemap_real_exists)
					{
						WSKO_Class_Template::render_notification('info', array('msg' => 'Sitemap has not been generated yet.'));
					}

					WSKO_Class_Template::render_table(array('Page', 'Last Modified', 'Update Frequenzy', 'Priority'), array(), array('ajax' => array('action' => 'wsko_table_onpage', 'arg' => '6')));?>
				</div>
			</div>
			*/ ?>
		<?php }
		else
		{
			WSKO_Class_Template::render_notification('error', array('msg' => 'You are missing the required permissions to create/edit the files "sitemap.xml" and "sitemap_bst.xml" in your home path.'));
		}
	}
	else
	{
		WSKO_Class_Template::render_notification('error', array('msg' => 'Only administrators can edit the sitemap.'));
	} ?>
</div>