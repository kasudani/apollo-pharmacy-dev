<?php
if (!defined('ABSPATH'))
	exit;
$api = isset($template_args['api']) ? $template_args['api'] : false;
?>
<div class="wsko_no_cache_wrapper">
<?php if (current_user_can('manage_options'))
{
	?><i class="fa fa-retweet fa-no-data-icon" aria-hidden="true"></i></br>
	<span>Dataset empty. <?=$api ? '<br/>'.WSKO_Class_Template::render_recache_api_button($api, true) : ('Try updating your cache under the '.WSKO_Class_Template::render_page_link(WSKO_Controller_Settings::get_instance(), '#apis', 'Settings', false).'</span>')?><?php
}
else
{
	?>
	<i class="fa fa-times fa-no-data-icon" aria-hidden="true"></i></br>
	<span>No Data available</span>
	<?php
} ?>
</div>