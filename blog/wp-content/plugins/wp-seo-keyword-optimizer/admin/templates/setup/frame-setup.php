<?php
if (!defined('ABSPATH')) exit;

global $wsko_plugin_url;

$wsko_data = WSKO_Class_Core::get_data();
$is_update = (isset($wsko_data['version_pre']) && $wsko_data['version_pre'] && $wsko_data['version_pre'] != WSKO_VERSION);
?>

<div class="wsko-setup-wrapper wrap">
	<div class="align-center wsko-notif-fix">
		<img class="wsko_logo" src="<?=$wsko_plugin_url.'admin/img/logo-bl.png'?>" />
		<h2>BAVOKO SEO Tools</h2>
	</div>
	<div style="margin-top:20px;" class="align-center">
		<img class="wsko_logo" src="<?=$wsko_plugin_url.'admin/img/logo-bl.png'?>" />
		<h2>BAVOKO SEO Tools</h2>
	</div>
	<div class="wsko-setup-notifications-wrapper">
	</div>
	<div class="wsko-section section">
		<h3 class="panel-heading">1. Step: API Connection</h3>
		<?php
		WSKO_Class_Template::render_template('admin/templates/settings/view-api-search.php', array());
		//WSKO_Class_Template::render_template('admin/templates/settings/view-api-facebook.php', array());
		//WSKO_Class_Template::render_template('admin/templates/settings/view-api-twitter.php', array());
		?>
	</div>
	
	<div style="margin-bottom:20px;" class="wsko-section section">
		<h3 class="panel-heading">2. Step: Import Data</h3>
		<?php WSKO_Class_Template::render_template('admin/templates/settings/view-import.php', array()); ?>
	</div>
	
	<div style="display:block; margin-top: 5px; float:right;">
		<a class="wsko-give-feedback wsko-inline-block text-off dark" style="cursor:pointer;">Contact Support</a>
	</div>
	
	<?php WSKO_Class_Template::render_ajax_button('Finish Setup', 'finish_setup'); ?>	
</div>