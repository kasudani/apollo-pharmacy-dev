<?php
if (!defined('ABSPATH'))
	exit;


global $wsko_plugin_url;
 
$isConfig = WSKO_Class_Core::is_configured();

?>
<style>
* {
    box-sizing: inherit;
}
h2 {
    font-size: 22px;
}
.wsko-uppercase {
    text-transform: uppercase;
}
body {background-color:white;}

.wsko-updated-wrapper .feature img {
    max-width: 100%;
}

.wsko-updated-wrapper h1 {
    font-size:28px;
}

h3 {font-weight:600; font-size:20px;}

/*
.wsko-updated-wrapper .wsko-row {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display:         flex;
}
*/
.wsko-updated-wrapper .wsko-col-sm-5 {
	margin: auto 0;
}

.wsko-updated-wrapper .wsko_logo {
    width: 70px;
    height: 70px;
    border-radius: 0px;
    box-shadow: none;
}
.wsko-updated-wrapper iframe {
    width: 100%;
	height: 100%;
    box-shadow: 0 0 30px 3px rgba(0,0,0,.1);	
}
.wsko-updated-wrapper ul li {
    list-style-type: disc;
}
.wsko-updated-wrapper ul {
	padding-left:40px;
}

.wsko-updated-wrapper p {
	margin-top:0px;
	margin-bottom:5px;
}
.wsko-updated-wrapper .text-off {
	opacity: .7;
}

.schatten {-webkit-box-shadow: 3px 0px 20px 0px rgba(0,0,0,0.18);
-moz-box-shadow: 3px 0px 20px 0px rgba(0,0,0,0.18);
box-shadow: 3px 0px 20px 0px rgba(0,0,0,0.18);
}

.abtrennung {border-bottom:1px solid #f7f7f7; padding-bottom:20px; padding-top:20px;}


</style>

<div class="wsko-container wsko_wrapper wsko-updated-wrapper" style="max-width:1000px; margin:15px auto;">
	<div class="wsko-row" style="margin-top:50px">
		<div class="wsko-col-sm-12" style="text-align:center; margin-bottom:10px;">
			<div style=" margin:20px 0px;"><img class="wsko_logo" src="<?=$wsko_plugin_url.'admin/img/logo-bl.png'?>" /></div>
			<h2>WP SEO Keyword Optimizer is now BAVOKO SEO Tools</h2>
			<p class="text-off wsko-uppercase">The Most Comprehensive All-in-One WordPress SEO Plugin</p>			
		</div>
	</div>
	<div class="wsko-row" style="margin:30px 0px">
		<div class="wsko-col-sm-6" style="padding: 20px 40px;">
			<h3 style="font-weight:normal;">Extensive data and optimized workflows</h3>
			<p>BAVOKO SEO Tools is the first WordPress SEO plugin that combines both SEO analysis and optimization in just one application.
			With the help of integrated search, onpage, performance, backlink and social media tools, all aspects of your search engine optimization can be easily accessed via your WordPress backend. Thanks to the intelligent architecture of BAVOKO SEO Tools, you can now make changes and optimize your pages in just seconds.</p>
		</div>
		<div class="wsko-col-sm-6" style="height:270px;">
			<iframe src="https://www.youtube.com/embed/_s-HcePDwww" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
	
	<p style="text-align:center; margin-top:50px;">	
		<?php 
		if ($isConfig) {
			WSKO_Class_Template::render_page_link(WSKO_Controller_Dashboard::get_instance(), false, 'Continue to Dashboard', array('button' => true));
		} else {
			WSKO_Class_Template::render_page_link(WSKO_Controller_Setup::get_instance(), false, 'Continue to Setup', array('button' => true));
		}	
		?>
	</p>
</div>