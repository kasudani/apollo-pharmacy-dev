<?php
if (!defined('ABSPATH')) exit;

$post_id = isset($template_args['post_id']) ? $template_args['post_id'] : false;
$priority_keywords = isset($template_args['priority_keywords']) ? $template_args['priority_keywords'] : false;

?>

<div class="wsko-widget-sidebar-keywords">
	<div class="wsko-co-priority-keywords-container" data-post="<?=$post_id?>" data-nonce="<?=wp_create_nonce('wsko_co_add_priority_keyword')?>">
		<div class="wsko-co-keyword-suggest-wrapper">
			<input class="wsko-co-keyword-input wsko-form-control wsko-mb10" placeholder="Keyword Suggestions" data-nonce="<?=wp_create_nonce('wsko_co_get_keyword_suggests')?>">
			<a href="#" style="line-height: 14px;" class="wsko-co-add-priority-keyword btn btn-flat">Add</a>
			<div class="keyword-suggest-inner-wrapper">
				<ul class="wsko-co-keyword-suggests">
				</ul>
			</div>
			<select style="display:none;" class="wsko-co-keyword-prio">
				<option value="1" selected>Prio 1</option>
				<option value="2">Prio 2</option>
				<option value="3">Prio 3</option>
			</select>
		</div>
		
		<div class="wsko-priority-keyword prio-1">
			<p class="wsko-priority-keyword-label">Priority Keywords</p>
			<ul class="wsko-co-priority-keyword-group" data-prio="1" style="list-style-type:none;min-height:50px;">
				<?php
				if ($priority_keywords)
				{
					foreach($priority_keywords as $pk => $data)
					{
						if ($data['prio'] == 1)
							WSKO_Class_Template::render_priority_keyword_item($post_id, $pk, $data);
					}
				}
				else {
					echo '<span class="text-off wsko-co-keyword-group-no-items">No Keywords added</span>';
				}
				?>
			</ul>
		</div>	
		
		<?php /*
		<div class="wsko-priority-keyword prio-2">
			<p class="badge badge-success">Prio 2</p>
			<ul class="wsko-co-priority-keyword-group" data-prio="2" style="list-style-type:none;min-height:50px;">
				<?php
				foreach($priority_keywords as $pk => $data)
				{
					if ($data['prio'] == 2)
						WSKO_Class_Template::render_priority_keyword_item($post_id, $pk, $data);
				}
				?>
			</ul>
		</div>
		<div class="wsko-priority-keyword prio-3">						
			<p class="badge badge-success">Prio 3</p>
			<ul class="wsko-co-priority-keyword-group" data-prio="3" style="list-style-type:none;min-height:50px;">
				<?php
				foreach($priority_keywords as $pk => $prio)
				{
					if ($data['prio'] == 3)
						WSKO_Class_Template::render_priority_keyword_item($post_id, $pk, $data);
				}
				?>
			</ul>
		</div>	
		*/ ?>
	</div>
</div>