<?php
if (!defined('ABSPATH')) exit;

$is_fetching = false;
global $wsko_plugin_url;
$global_report = WSKO_Class_Onpage::get_onpage_analysis();
if ((!$global_report /*|| !isset($global_report['current_report'])*/) || (WSKO_Class_Search::get_se_token() && !WSKO_Class_Core::get_option('search_query_first_run')))
	$is_fetching = true;
if (!$is_fetching)
{
	$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
	?>
	<div class="wsko-dashboard">
		<div class="row">
			<?php
			 //$current_user = wp_get_current_user();
			?>	
			<!--div class="col-sm-12 col-xs-12">
				<h4 class="m0 mb10 font-unimportant" style="padding:5px 15px;"><?php //=__('Welcome back, ', 'wsko') . $current_user->user_login . '!';?></h4>
			</div-->	
			<div class="col-sm-12 col-xs-12">
				<div class="wsko-section row">
					<?=WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Keywords', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'key', 'lazyVar' => 'total_keywords', 'tableLink' => array('link' => WSKO_Controller_Search::get_link('keywords'), 'title' => __( 'Learn more', 'wsko' ))));?>
					<?=WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Google Clicks', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'mouse-pointer', 'lazyVar' => 'total_clicks', 'tableLink' => array('link' => WSKO_Controller_Search::get_link('overview'), 'title' => __( 'Learn more', 'wsko' ))));?>
					<?=WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Content Score', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'trophy', 'lazyVar' => 'avg_onpage_score', 'tableLink' => array('link' => WSKO_Controller_Onpage::get_link('analysis'), 'title' => __( 'Learn more', 'wsko' ))));?>
					<?=WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Social Follower', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'share-alt', 'lazyVar' => 'total_social_follower', 'tableLink' => array('link' => WSKO_Controller_Social::get_link('facebook'), 'title' => __( 'Learn more', 'wsko' ))));?>
				</div>
			</div>	

			<div class="col-sm-12">
				<ul class="nav nav-tabs bsu-tabs wsko-big-nav">
				  <li class="active"><a data-toggle="tab" href="#wsko-dashboard-search"><i class="fa fa-search fa-fw mr5"></i><?=__( 'Search', 'wsko' ) ?></a></li>
				  <li class=""><a data-toggle="tab" href="#wsko-dashboard-onpage"><i class="fa fa-code fa-fw mr5"></i><?=__( 'Onpage', 'wsko' ) ?></a></li>
				  <li class=""><a data-toggle="tab" href="#wsko-dashboard-social"><i class="fa fa-share-alt fa-fw mr5"></i><?=__( 'Social', 'wsko' ) ?></a></li>
				</ul>
			</div>	
			
			<div class="tab-content">
				<div id="wsko-dashboard-search" class="tab-pane fade in active">			
			
					<div class="col-sm-12 col-xs-12">
						<div class="wsko-section wsko-dashboard-search-wrapper">
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<div class="bsu-panel panel panel-default">
										<p class="panel-heading m0"><?=__( 'Clicks & Impressions', 'wsko' ) ?></p>
										<div class="p15" style="padding-bottom:0px;">
											<ul class="nav nav-tabs bsu-tabs">
											  <li class="active"><a data-toggle="tab" href="#history_se_clicks"><?=__( 'Clicks', 'wsko' ) ?></a></li>
											  <li class=""><a data-toggle="tab" href="#history_se_impressions"><?=__( 'Impressions', 'wsko' ) ?></a></li>
											</ul>
										</div>	

										<div class="tab-content">
											<div id="history_se_clicks" class="tab-pane fade in active">
												<?php WSKO_Class_Template::render_lazy_field('search_clicks_history', 'small'); ?>
											</div>
											<div id="history_se_impressions" class="tab-pane fade">
												<?php WSKO_Class_Template::render_lazy_field('search_impression_history', 'small'); ?>
											</div>
										</div>	
									</div>
								</div>
								<div class="col-sm-6 col-xs-12">
									<div class="bsu-panel panel panel-default">
										<p class="panel-heading m0"><?=__( 'Position & CTR', 'wsko' ) ?></p>
										<div class="p15" style="padding-bottom:0px;">
											<ul class="nav nav-tabs bsu-tabs">
											  <li class="waves-effect active"><a data-toggle="tab" href="#history_se_pos"><?=__( 'Position', 'wsko' ) ?></a></li>
											  <li class="waves-effect"><a data-toggle="tab" href="#history_se_ctr"><?=__( 'CTR', 'wsko' ) ?></a></li>
											</ul>
										</div>	

										<div class="tab-content">
											<div id="history_se_pos" class="tab-pane fade in active">
												<?php WSKO_Class_Template::render_lazy_field('search_position_history', 'small'); ?>
											</div>
											<div id="history_se_ctr" class="tab-pane fade">
												<?php WSKO_Class_Template::render_lazy_field('search_ctr_history', 'small'); ?>
											</div>
										</div>	
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 col-xs-12">	
									<div class="bsu-panel panel panel-default">
										<p class="panel-heading m0"><?=__( 'Keywords', 'wsko' ) ?></p>	

										<div id="all_keywords">
											<?php WSKO_Class_Template::render_lazy_field('table_keywords', 'small'); ?>
										</div>
									</div>	
								</div>
								<div class="col-sm-6 col-xs-12">
									<div class="bsu-panel panel panel-default">
										<p class="panel-heading m0"><?=__( 'Pages', 'wsko' ) ?></p>	
								
										<div id="all_pages">
											<?php WSKO_Class_Template::render_lazy_field('table_pages', 'small'); ?>
										</div>
									</div>	
								</div>
							</div>	
						</div>	
					</div>	
					
				</div>	
				<div id="wsko-dashboard-onpage" class="tab-pane fade">	
					
					<div class="col-sm-12 col-xs-12">
						<div class="wsko-section wsko-dashboard-onpage-wrapper">
							<div class="crawling-info-inner pull-right" style="margin-top: -40px; margin-bottom: 0px;">
								<?php
								$global_analysis = WSKO_Class_Onpage::get_onpage_analysis();
								if ($global_analysis && isset($global_analysis['current_report']))
								{ ?>
									<small class="font-unimportant">Crawled Pages: <?=$global_analysis['current_report']['total_pages']?>, </small>
									<small class="font-unimportant">Last Crawl: <?=date('d.m.Y', $global_analysis['current_report']['started'])?>, </small>
									<small class="font-unimportant">Next Crawl: <?=date('d.m.Y', $global_analysis['current_report']['started'] + (60*60*24*7))?></small>
								<?php } ?>
							</div>
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<?php
										WSKO_Class_Template::render_panel(array('type' => 'table', 'title' => __('Content Score History', 'wsko'), 'col' => '', 'fa' => 'ellipsis-v', 'lazyVar' => 'onpage_history'));
									?>	
										<div id="wsko_onpage_issues" class="panel-group">
									<?php	
											WSKO_Class_Template::render_template('admin/templates/onpage/view-onpage-issues.php', array('analysis' => $global_analysis));
									?>
										</div>
								</div>
								<div class="col-sm-6 col-xs-12">
									<?php	
										WSKO_Class_Template::render_panel(array('type' => 'table', 'title' => __('Pages with the lowest Content Score', 'wsko'), 'col' => '', 'fa' => 'ellipsis-v', 'lazyVar' => 'onpage_worst_optimized'));
									?>
								</div>
							</div>	
						</div>	
					</div>	
					
				</div>
				<div id="wsko-dashboard-social" class="tab-pane fade">	
					<div class="col-sm-12 col-xs-12">		
						<div class="wsko-section wsko-dashboard-social-wrapper">
							<div class="bsu-panel">
								<div class="row">
										<?php
										WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Social Follower', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'social_follower_history')); 
										//WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Top 10 Facebook Posts', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'social_posts_fb'));
										WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Top 10 Tweets', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'social_posts_tw'));
										?> 
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } else {
	?>
	<div class="align-center">
		<i class="mb15 fa fa-database fa-no-data-icon"></i>
		<p class="mb15"><?=__( 'Your Dashboard will be ready in a few minutes. Please follow the steps shown above.', 'wsko' )?></p>
		<div class="clearfix"></div>
	</div>
	<?php
} ?>
