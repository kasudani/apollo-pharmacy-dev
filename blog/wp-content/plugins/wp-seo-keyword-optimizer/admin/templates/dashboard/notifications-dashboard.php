<?php
if (!defined('ABSPATH')) exit;

if (WSKO_Class_Onpage::seo_plugins_disabled())
{
	$global_report = WSKO_Class_Onpage::get_onpage_analysis();
	if (!$global_report || !isset($global_report['current_report']))
	{
		if (!wp_next_scheduled('wsko_onpage_analysis'))
		{
			if (WSKO_Class_Core::get_option('onpage_calc_changed'))
				WSKO_Class_Template::render_notification('warning', array('msg' => 'Please Note: We improved our onpage analysis tool with the last update, which requires a new crawl. Please generate a new onpage report.<br/>'.WSKO_Class_Template::render_page_link(WSKO_Controller_Onpage::get_instance(), 'analysis', 'Configure Onpage Analysis', true, true)));
			else
				WSKO_Class_Template::render_notification('warning', array('msg' => 'Your Onpage Analysis will be generated, after you have set the initial parameters. Hit the button bellow to configure your first crawl.<br/>'.WSKO_Class_Template::render_page_link(WSKO_Controller_Onpage::get_instance(), 'analysis', 'Configure Onpage Analysis', true, true), 'subnote' => 'The Analysis will refresh itself once every week, after the first run.'));
		}
		else
			WSKO_Class_Template::render_notification('success', array('msg' => 'Your Onpage Analysis is being generated'));
	}
}
else
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'You need to disable your SEO plugins to gain access to the Onpage section. The following plugins have been detected:', 'list' => WSKO_Class_Import::get_active_seo_plugins(), 'subnote' => 'We are sorry for the inconvenience, but this prevents unexpected changes in your SEO rankings.'));
}
if (WSKO_Class_Search::get_se_token())
{
	if (!wp_next_scheduled('wsko_cache_keywords'))
	{
		WSKO_Class_Template::render_notification('error', array('msg' => 'The Cronjob for your Search Data is not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
	}
	else if (!WSKO_Class_Core::get_option('search_query_first_run') && wp_next_scheduled('wsko_cache_keywords'))
	{
		if (wp_next_scheduled('wsko_cache_keywords') > time() && WSKO_Class_Core::get_option('search_query_last_start') && (WSKO_Class_Core::get_option('search_query_last_start') < (time()-(60*5)) || !WSKO_Class_Core::get_option('search_query_running')))
			WSKO_Class_Template::render_notification('warning', array('msg' => 'Your Search Data Request has timed out. '.WSKO_Class_Template::render_ajax_button('Reset CronJob', 'reset_keyword_update', array(), array(), true)));
		else
			WSKO_Class_Template::render_notification('info', array('msg' => 'Your Search Data is being fetched. Please wait a few minutes. Working on package '.intval(WSKO_Class_Core::get_option('search_query_first_run_step')).' out of 9 packages.', 'subnote' => 'You may see some errors while this message is being displayed.'));
	}
}

if (/*!wp_next_scheduled('wsko_cache_social') || !wp_next_scheduled('wsko_check_timeout') ||*/ !wp_next_scheduled('wsko_daily_maintenance'))
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'One or more system CronJobs are not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
}
?>
