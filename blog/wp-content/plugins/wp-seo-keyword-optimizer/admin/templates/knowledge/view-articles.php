<?php
if (!defined('ABSPATH')) exit;
global $wsko_plugin_url;

$articles = isset($template_args['articles']) ? $template_args['articles'] : false;
$count = isset($template_args['count']) ? $template_args['count'] : false;
$step = isset($template_args['step']) ? $template_args['step'] : false;
$page = isset($template_args['page']) ? $template_args['page'] : 1;
$search = isset($template_args['search']) ? $template_args['search'] : false;
$cats = isset($template_args['categories']) ? $template_args['categories'] : array();

$categories = WSKO_Class_Knowledge::get_knowledge_base_categories();
if ($articles !== false)
{
	$last_fetch = WSKO_Class_Core::get_option('last_knowledge_base_update');
	?><div class="wsko-search-knowledge-base-wrapper kb-main" style="position:relative;">
		<img class="wsko-kb-books" src="<?=$wsko_plugin_url.'admin/img/books.png'?>" />
		<div class="inline-block wsko-kb-search-wrapper" style="margin-bottom: 10px; width: 70%;">
			<h2>BAVOKO Knowledge Base</h2>
			<input class="form-control wsko-search-knowledge-base mb10" data-nonce="<?=wp_create_nonce('wsko_search_knowledge_base')?>" placeholder="Search Articles" value="<?=$search?>">
			<small class="text-off">Last fetch: <?=$last_fetch ? date('d.m.Y H:i', $last_fetch) : 'never' ?></small>
		</div>
		<div class="kb-category-wrapper">	
			<p class="small text-off wsko-uppercase">Categories:</p>
			<?php foreach ($categories as $cat => $title)
			{
				?><label <?=in_array($cat, $cats) ? 'class="wsko-kb-cat-selected"' : ''?>><input type="checkbox" class="form-control wsko-search-knowledge-base-cat" value="<?=$cat?>" <?=in_array($cat, $cats) ? 'checked="checked"' : ''?>> <?=$title?></label><?php
			} ?>
		</div>	
		<ul class="wsko-search-knowledge-base-list kb_posts_wrapper"><?php
		if ($articles)
			WSKO_Class_Template::render_template('admin/templates/knowledge/template-article-list.php', array('articles' => $articles));
		else
			WSKO_Class_Template::render_notification('info', array('msg' => 'No articles found.'));
		?></ul>
		<input type="hidden" class="wsko-search-knowledge-base-page" value="<?=$page?>">
		<ul class="pagination">
			  <?php for ($i = 0; $i < $count; $i+=$step)
			  { 
				$i_v = intval(1+floor($i/$step));
				?><li <?=($i_v === $page ? 'class="active"' : '')?>><a class="wsko-search-knowledge-base-page-set" data-page="<?=$i_v?>" href="#"><?=$i_v?></a></li><?php
			  } ?>
		</ul>
	</div><?php
}
else
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'Knowledge Base Articles could not be loaded. Please try again in a few minutes.'));
}
?>