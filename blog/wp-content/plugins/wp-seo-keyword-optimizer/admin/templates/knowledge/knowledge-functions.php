<?php
function kb_helpful($atts)
{
	ob_start();
	?>
		<div class="kb-helpful">
			<p>Was this article helpful?</p>
			<a class="btn btn-primary btn-sm" style="color:#fff;">Yes</a>
			<a class="btn btn-secondary btn-sm" style="color:#fff;">No</a>
		</div>	
	<?php
	return ob_get_clean();
}
add_shortcode('kb_helpful', 'kb_helpful');
?>