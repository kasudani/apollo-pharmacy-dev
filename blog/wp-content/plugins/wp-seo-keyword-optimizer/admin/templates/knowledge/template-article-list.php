<?php
if (!defined('ABSPATH')) exit;

$articles = isset($template_args['articles']) ? $template_args['articles'] : false;
if ($articles)
{
	foreach ($articles as $a)
	{
		?><li class="kb_posts_item">
			<a href="#" class="wsko-open-knowledge-base-article dark" data-article="<?=$a->id?>">
				<?php foreach ($a->categories as $cat) { ?><p class="small text-off wsko-uppercase" style="margin-bottom:10px;"><?=$cat?></p><?php }?>			
				<h4><?=$a->title?></h4> <?php /* <a href="<?=$a->link?>" target="_blank">Open Page</a><br/> */ ?>			
				<?=$a->preview?>
			</a>	
		</li><?php
	}
}
else
{
	?><li>No Articles found.</li><?php
}
?>