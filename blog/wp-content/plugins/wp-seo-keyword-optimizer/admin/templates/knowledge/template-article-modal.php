<?php
if (!defined('ABSPATH')) exit;

$article = isset($template_args['article']) ? $template_args['article'] : false;

if ($article)
{
	?>
	<div class="col-sm-12 col-xs-12">
		<?php /* <a href="<?=$article->link?>" target="_blank"><i class="fa fa-share"></i></a> */ ?>
		<h2><?=$article->title?></h2>
		<?=$article->content?>
		<div class="kb-helpful">
			<p>Was this article helpful?</p>
			<a class="btn btn-primary btn-sm wsko-kb-rate-article" data-good="true" data-post="<?=$article->id?>" style="color:#fff; line-height: 1.5;" data-nonce="<?=wp_create_nonce('wsko_rate_knowledge_base_article')?>"><i class="fa fa-thumbs-up fa-fw"></i> Yes</a>
			<a class="btn btn-secondary btn-sm wsko-kb-rate-article" data-good="false" data-post="<?=$article->id?>" style="color:#fff; line-height: 1.5;" data-nonce="<?=wp_create_nonce('wsko_rate_knowledge_base_article')?>"><i class="fa fa-thumbs-down fa-fw"></i> No</a>
		</div>
	</div>	
	<?php
}
else
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'The Article could not be loaded. Please try again in a few minutes.'));
}
?>