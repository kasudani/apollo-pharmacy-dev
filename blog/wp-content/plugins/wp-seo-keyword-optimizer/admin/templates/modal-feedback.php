<?php
if (!defined('ABSPATH')) exit;


$current_user = wp_get_current_user();
$current_user_mail = $current_user->user_email;
$current_user_name = $current_user->user_firstname . ' ' . $current_user->user_lastname;
?>
<div class="modal fade wsko_modal" id="wsko_modal_feedback">
  <div class="modal-dialog">
    <div class="modal-content">
	    <button type="button" class="close m5" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>


      <div class="modal-header">
        <h4 class="modal-title">Contact us!</h4>
      </div>
	  <form id="wsko_feedback_form" class="wsko-form-feedback" data-nonce="<?=wp_create_nonce('wsko-feedback')?>">
	    <div class="modal-body">
			<div class="row">
				<div class="form-group clearfix mb5">
					<div class="col-sm-9 col-sm-offset-3">	
						<fieldset class="wsko-feedback-type">
							<div class="btn-group btn-group-justified" data-toggle="buttons">
								<label class="btn btn-warning active wsko-feedback-type-btn">
									<input type="radio" name="type" value="1" autocomplete="off" checked> Support
								</label>
								<label class="btn btn-success wsko-feedback-type-btn">
									<input type="radio" name="type" value="2" autocomplete="off"> Feedback
								</label>
								<label class="btn btn-info wsko-feedback-type-btn">
									<input type="radio" name="type" value="3" autocomplete="off"> Questions
								</label>
							</div>
						</fieldset>
					</div>
				</div>	
				<div class="form-group">
					<div class="col-sm-3">
						<label>Full Name</label><br/>
						<small class="text-off">Your name (optional)</small>
					</div>
					<div class="col-sm-9">
						<input placeholder="Enter your name (optional)" class="form-control wsko-feedback-name" type="text" value="<?=$current_user_name ? $current_user_name : ''?>" name="name">
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label>Email</label><br/>
						<small class="text-off">Your mail adress, so we can answer your question</small>
					</div>
					<div class="col-sm-9">
						<input placeholder="Your contact email adress" class="form-control wsko-feedback-email" type="email" name="email" value="<?=$current_user_mail ? $current_user_mail : ''?>" required>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label>Subject</label><br/>
						<small class="text-off">A brief summary of your request</small>
					</div>
					<div class="col-sm-9">
						<input placeholder="A brief title" class="form-control wsko-feedback-title" type="text" name="title" required>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-3">	
						<label>Message</label><br/>
						<small class="text-off">Tell us what's on your mind</small>
					</div>
					<div class="col-sm-9">
						<textarea rows="10" placeholder="Your Message" class="form-control wsko-feedback-msg" name="msg" required></textarea>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="form-group wsko-support-options">
					<div class="col-sm-9 col-sm-offset-3">
						<label><input type="checkbox" class="form-control wsko-feedback-reports" name="reports"> Append Error Reports</label><br/>
						<small class="text-off">Add your last 20 error reports to your ticket, to make it easier for us to solve your problem. You can view every report in your settings, if the reporting view has been activated in the advanced panel.</small>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-primary wsko-feedback-submit btn-flat" type="submit"><i class="fa fa-spin fa-spinner" style="display:none;"></i> Send</button>
						<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
					</div>
				</div>				
			</div>	
	    </div>
	  </form>
	  
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 