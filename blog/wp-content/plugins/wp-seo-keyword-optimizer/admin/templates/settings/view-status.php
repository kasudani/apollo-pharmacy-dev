<?php
if (!defined('ABSPATH')) exit;

$cache_stats = WSKO_Class_Cache::get_cache_stats();
$db_size_data = array();
//$db_row_data = array();
foreach ($cache_stats['data_tables'] as $key => $data)
{
	$db_size_data[] = array($key, $data['size']);
	//$db_row_data[] = array($key, $data['rows']);
}
$cron_search = wp_next_scheduled('wsko_cache_keywords');
$cron_analytics = wp_next_scheduled('wsko_cache_backlinks');
$cron_social = wp_next_scheduled('wsko_cache_social');
$cron_onpage = wp_next_scheduled('wsko_onpage_analysis');
$cron_sitemap = wp_next_scheduled('wsko_sitemap_generation');
$cron_expire = wp_next_scheduled('wsko_cache_expire');
?>
<div class="row">
	<div class="col-md-12" style="border-bottom:solid 1px #ddd;">
		<h2>API Caching</h2>
		<?php WSKO_Class_Template::render_api_statusbar()?>
		<div class="row">
			<div class="col-md-6">
				<h3>Size Distribution</h3>
				<?php WSKO_Class_Template::render_chart('pie', array('DB', 'Size'), $db_size_data); ?>
			</div>
			<div class="col-md-2">
				Saved Days: <b style="float:right"><?=$cache_stats['general']['days']?></b><br/>
				Data Rows: <b style="float:right"><?=number_format($cache_stats['general']['rows'], 0, ',', '.')?></b><br/>
				Total Size: <b style="float:right"><?=WSKO_Class_Helper::format_byte($cache_stats['general']['size'])?></b><br/>
			</div>
			<div class="col-md-4">
				<h3>Caching CronJobs</h3>
				
				<b><i class="fa fa-search"></i> Search</b>
				<?=$cron_search ? '<p style="color:green">active</p> next run on '.date('d.m.Y H:i', $cron_search) : '<p style="color:red">inactive '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true).'</p>'?> | <?=$cache_stats['data_tables']['search']['days']?> days cached<br/>
				
				<b><i class="fa fa-bar-chart"></i> Backlinks</b>
				<?=$cron_analytics ? '<p style="color:green">active</p> next run on '.date('d.m.Y H:i', $cron_analytics) : '<p style="color:red">inactive '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true).'</p>'?> | <?=$cache_stats['data_tables']['analytics']['rows']?> backlinks cached<br/>
				
				<b><i class="fa fa-bar-chart"></i> Social</b>
				<?=$cron_social ? '<p style="color:green">active</p> next run on '.date('d.m.Y H:i', $cron_social) : '<p style="color:red">inactive '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true).'</p>'?> | <?=$cache_stats['data_tables']['social']['days']?> days cached<br/>
			</div>
		</div>
	</div>
	<div class="col-md-12" style="border-bottom:solid 1px #ddd;">
		<h2>Other CronJobs</h2>
		<b><i class="fa fa-search"></i> Onpage Analysis</b>
		<?=$cron_onpage ? '<p style="color:green">active</p> next run on '.date('d.m.Y H:i', $cron_onpage) : '<p style="color:red">inactive '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true).'</p>'?><br/>
		
		<b><i class="fa fa-search"></i> Sitemap</b>
		<?=$cron_sitemap ? '<p style="color:green">active</p> next run on '.date('d.m.Y H:i', $cron_sitemap) : '<p style="color:red">inactive '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true).'</p>'?><br/>
		
		<b><i class="fa fa-search"></i> Cache Expire</b>
		<?=$cron_expire ? '<p style="color:green">active</p> next run on '.date('d.m.Y H:i', $cron_expire) : '<p style="color:red">inactive '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true).'</p>'?><br/>
		
		<?php WSKO_Class_Template::render_ajax_button('Reset All CronJobs', 'reset_cronjobs', array(), array()); ?>
	</div>
</div>