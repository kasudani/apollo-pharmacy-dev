<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( current_user_can( 'manage_options' ) ) {
    global  $wp_roles, $wsko_plugin_url ;
    $roles = $wp_roles->get_names();
    $post_types = WSKO_Class_Helper::get_public_post_types( 'objects' );
    $lang = WSKO_Class_Core::get_setting( 'plugin_lang' );
    $backups = get_option( 'wsko_backups' );
    ?>
	<div class="row wsko-settings-wrapper">
		<div class="col-md-12">
		
		<ul class="nav nav-tabs bsu-tabs border-dark">
			<li class="active"><a id="tab_settings_link" href="#tab_settings" data-toggle="tab"><?php 
    echo  __( 'General', 'wsko' ) ;
    ?></a><li>
			<li><a id="tab_advanced_link" href="#tab_advanced" data-toggle="tab"><?php 
    echo  __( 'Advanced', 'wsko' ) ;
    ?></a><li>
			<li><a id="tab_apis_link" href="#tab_apis" data-toggle="tab"><?php 
    echo  __( 'API Settings', 'wsko' ) ;
    ?></a><li>
			<li><a id="tab_configuration_link" href="#tab_configuration" data-toggle="tab"><?php 
    echo  __( 'Configuration', 'wsko' ) ;
    ?></a><li>
			<?php 
    
    if ( WSKO_Class_Core::get_setting( 'activate_log' ) ) {
        ?><li><a id="tab_reports_link" href="#tab_reports" data-toggle="tab"><?php 
        echo  __( 'Error Reports', 'wsko' ) ;
        ?></a><li><?php 
    }
    
    ?>
			<?php 
    /* <li><a href="#tab_import" data-toggle="tab"><?=__('Import', 'wsko');?></a><li> */
    ?>
			<?php 
    /* <li><a href="#tab_status" data-toggle="tab"><?=__('Status', 'wsko');?></a><li> */
    ?>
			<?php 
    /* <li><a href="#tab_reset" data-toggle="tab"><i style="padding-right:5px;" class="fa fa-times fa-blue" aria-hidden="true"></i> Reset</a><li> */
    ?>
		</ul>
		
		<div class="wsko_box_wrapper">
				<div class="tab-content">
					<div id="tab_settings" class="tab-pane fade in active">				
						<div class="row">
							<div class="bsu-panel col-sm-12 col-xs-12">
								<div class="panel panel-default">
									<p class="panel-heading m0"><?php 
    echo  __( 'General Settings', 'wsko' ) ;
    ?></p>
									
									<div class="panel-body">
										<h3 class="small text-off">General</h3>
											<div class="row form-group">
												<div class="col-sm-3">
													<p class="m0">Plugin Language</p>
													<small class="text-off">Force a specific lanuage or let WordPress decide.</small>
												</div>
												<div class="col-sm-9">
													<select class="selectpicker wsko-ajax-input form-control" data-wsko-target="settings" data-wsko-setting="plugin_lang" data-reload-real="true">
														<option value="auto" <?php 
    echo  ( !$lang || $lang == 'auto' ? 'selected' : '' ) ;
    ?>> <?php 
    echo  __( 'Auto', 'wsko' ) ;
    ?></option>
														<option value="en_EN" <?php 
    echo  ( $lang == 'en_EN' ? 'selected' : '' ) ;
    ?> data-content="<img class='mr5' src='<?php 
    echo  $wsko_plugin_url ;
    ?>includes/famfamfam_flags/us.png'> English"></option>
														<option value="de_DE" <?php 
    echo  ( $lang == 'de_DE' ? 'selected' : '' ) ;
    ?> data-content="<img class='mr5' src='<?php 
    echo  $wsko_plugin_url ;
    ?>includes/famfamfam_flags/de.png'> German"></option>
													</select>
												</div>
											</div>
										<h3 class="small text-off">Content Optimizer</h3>
											<div class="row form-group no-border">
												<div class="col-sm-3">
													<p class="m0">Activate Content Optimizer</p>
													<small class="text-off">Show the Content Optimizer Widget on every post edit page.</small>
												</div>
												<div class="col-sm-9">
													<label>
														<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'activate_content_optimizer' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="activate_content_optimizer">
														Activate Content Optimizer
													</label>
												</div>
											</div>
											<div class="row form-group">
												<div class="col-sm-3">
													<p class="m0">Include Content Optimizer in Post Types</p>
													<small class="text-off">Show Content Optimizer link on the post type's overview and the corresponding edit pages.</small>
												</div>
												<div class="col-sm-9">
													<div class="row wsko-settings-co-include-post-types" style="overflow-y:auto;max-height:150px;">
													<?php 
    $included = explode( ',', WSKO_Class_Core::get_setting( 'content_optimizer_post_types' ) );
    foreach ( $post_types as $pt ) {
        ?><div class="col-md-3">
															<label><input class="form-control wsko-ajax-input" type="checkbox" <?php 
        echo  ( in_array( $pt->name, $included ) ? 'checked="checked"' : '' ) ;
        ?> data-wsko-target="settings" data-wsko-setting="content_optimizer_post_types" data-multi-parent=".wsko-settings-co-include-post-types" value="<?php 
        echo  $pt->name ;
        ?>"><?php 
        echo  $pt->label ;
        ?> <span class="settings-sub font-unimportant"><?php 
        echo  $pt->name ;
        ?></span></label>
														</div><?php 
    }
    ?>
													</div>
												</div>
											</div>	
											
										<h3 class="small text-off">Search</h3>
											<div class="row form-group">
												<div class="col-sm-3">
													<p class="m0">Search Settings</p>
													<small class="text-off">Select time range to cache in the 'Search'-section</small>
												</div>
												<div class="col-sm-9">
													<span>
														<small class="text-off">Cache Limit (Days to keep data rows in cache)* </small>
														<input class="form-control wsko-ajax-input" type="number" name="cache_time_limit" value="<?php 
    echo  WSKO_Class_Core::get_setting( 'cache_time_limit' ) ;
    ?>" data-wsko-target="settings" data-wsko-setting="cache_time_limit" min="90" placeholder="Default: Infinite (Min.: 90)">
													</span>
												</div>
											</div>										
											
										<h3 class="small text-off">Onpage</h3>
											<div class="row form-group no-border">
												<div class="col-sm-3">
													<p class="m0">Onpage Analysis</p>
													<small class="text-off">Select Post Types to include in 'Onpage Analysis'</small>
												</div>
												<div class="col-sm-9">
													<div class="row wsko-settings-include-post-types" style="overflow-y:auto;max-height:150px;">
													<?php 
    $included = explode( ',', WSKO_Class_Core::get_setting( 'onpage_include_post_types' ) );
    foreach ( $post_types as $pt ) {
        ?><div class="col-md-3">
															<label><input class="form-control wsko-ajax-input" type="checkbox" <?php 
        echo  ( in_array( $pt->name, $included ) ? 'checked="checked"' : '' ) ;
        ?> data-wsko-target="settings" data-wsko-setting="onpage_include_post_types" data-multi-parent=".wsko-settings-include-post-types" value="<?php 
        echo  $pt->name ;
        ?>"><?php 
        echo  $pt->label ;
        ?> <span class="settings-sub font-unimportant"><?php 
        echo  $pt->name ;
        ?></span></label>
														</div><?php 
    }
    ?>
													</div>
												</div>
											</div>	
											
											<?php 
    /*
    <div class="row form-group no-border">
    	<div class="col-sm-4">
    		<p class="m0">Content Optimizer Timespan</p>
    		<small class="text-off">Select time range to consider, when displaying content optimizer values.</small>
    	</div>
    	<div class="col-sm-8">
    		<span>
    			<input class="form-control wsko-ajax-input" type="number" name="content_optimizer_time" value="<?=WSKO_Class_Core::get_setting('content_optimizer_time')?>" data-wsko-target="settings" data-wsko-setting="content_optimizer_time" placeholder="Default: 27 (days)">
    		</span>
    	</div>
    </div>			
    */
    ?>
											
											<div class="row form-group no-border">
												<div class="col-sm-3">
													<p class="m0">Onpage Analysis - Post Title is used as H1</p>
													<small class="text-off">The H1 of your pages is outside of the post content.</small>
												</div>
												<div class="col-sm-9 wsko-settings-post-types-h1-title" style="overflow-y:auto;max-height:150px;">
													<div class="row">
														<?php 
    $included = explode( ',', WSKO_Class_Core::get_setting( 'onpage_title_h1' ) );
    foreach ( $post_types as $pt ) {
        ?><div class="col-md-3">
																<label><input class="form-control wsko-ajax-input" type="checkbox" <?php 
        echo  ( in_array( $pt->name, $included ) ? 'checked="checked"' : '' ) ;
        ?> data-wsko-target="settings" data-wsko-setting="onpage_title_h1" data-multi-parent=".wsko-settings-post-types-h1-title" value="<?php 
        echo  $pt->name ;
        ?>"><?php 
        echo  $pt->label ;
        ?> <span class="settings-sub font-unimportant"><?php 
        echo  $pt->name ;
        ?></span></label>
															</div><?php 
    }
    ?>
													</div>	
												</div>
											</div>		
											
											<div class="row form-group">
												<div class="col-sm-3">
													<p class="m0">Automatic Canonicals</p>
													<small class="text-off">If a page has no specific canonical tag set, BST will add a tag linking to the same resource.</small>
												</div>
												<div class="col-sm-9">
													<label>
														<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'auto_canonical' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="auto_canonical">
														Activate automatic Canonicals
													</label>
												</div>
											</div>									
										<h3 class="small text-off">Social</h3>
											<div class="row form-group no-border">
												<div class="col-sm-3">
													<p class="m0">Automatic Social Snippet</p>
													<small class="text-off">If activated, BST will automatically fill every empty social meta with your corresponding Google Meta, or (in lower priority) data from your post.</small>
												</div>
												<div class="col-sm-9">
													<label>
														<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'auto_social_snippet' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="auto_social_snippet">
														Activate automatic Snippets
													</label>
												</div>
											</div>
											<div class="row form-group">
												<div class="col-sm-3">
													<p class="m0">Set Post Thumbnail as preview image</p>
													<small class="text-off">Set your Post Thumbnail as the preview image for your social metas. If you have disabled Auto-Snippets this will only affect posts with a Facebook or Twitter meta set.</small>
												</div>
												<div class="col-sm-9">
													<label>
														<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'auto_social_thumbnail' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="auto_social_thumbnail">
														Set Thumbnail as Preview
													</label>
												</div>
											</div>
									</div>	
								</div>
							</div>
						</div>	
					</div>
					
					<div id="tab_advanced" class="tab-pane fade">
						<div class="row">
							<div class="bsu-panel col-sm-12 col-xs-12">
								<div class="panel panel-default">
									<p class="panel-heading m0"><?php 
    echo  __( 'Advanced Settings', 'wsko' ) ;
    ?></p>
									
									<div class="panel-body">
										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Unlock .htaccess Editor</p>
												<small class="text-off">Activate saving for the Onpage .htaccess editor</small>
											</div>
											<div class="col-sm-9">
												<label>
												<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'activate_editor_htaccess' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="activate_editor_htaccess">
												Unlock Editor
												</label>
												<br/>
												<br/>
												<small class="wsko-red">Attention: Please unlock only, if you are experienced with .htaccess files and know what you are doing. Even a single error in this file can make your entire website go down and can only be fixed with a SSH or FTP connection.</small>
											</div>
										</div>

										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Unlock robots.txt Editor</p>
												<small class="text-off">Activate saving for the Onpage robots.txt editor</small>
											</div>
											<div class="col-sm-9">
												<label>
												<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'activate_editor_robots' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="activate_editor_robots">
												Unlock Editor
												</label>
												<br/>
												<br/>
												<small class="wsko-red">Attention: Please unlock only, if you are experienced with robots.txt files and know what you are doing. Errors in this file can impact your sites rankings and can lead to exclusion of your entire site from the google index.</small>
											</div>
										</div>

										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Additional Permissions</p>
												<small class="text-off">Add user roles to gain access to the plugin functionality. Only Admins are able to edit the settings and interact with the cache.</small>
											</div>
											<div class="col-sm-9">
												<p>
													<div class="row wsko-settings-additional-permissions" style="overflow-y:auto;max-height:150px;">
													<?php 
    $additional = explode( ',', WSKO_Class_Core::get_setting( 'additional_permissions' ) );
    foreach ( $roles as $role_key => $role ) {
        
        if ( $role_key != 'administrator' ) {
            ?><div class="col-md-3">
																<label><input class="form-control wsko-ajax-input" type="checkbox" <?php 
            echo  ( in_array( $role_key, $additional ) ? 'checked="checked"' : '' ) ;
            ?> data-wsko-target="settings" data-wsko-setting="additional_permissions" data-multi-parent=".wsko-settings-additional-permissions" value="<?php 
            echo  $role_key ;
            ?>"><?php 
            echo  $role ;
            ?> <span class="settings-sub font-unimportant"><?php 
            echo  $role_key ;
            ?></span></label>
															</div><?php 
        }
    
    }
    ?>
													</div>
													<p style="margin-top:5px;"><small class="font-unimportant">Administrators have full access, additional roles can view statistics and make use of some tools. Settings are restricted to admins only.</small></p>
												</p>
											</div>
										</div>
					
										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Error-Reporting</p>
												<small class="text-off">Activate the logging view with your current error reports.</small>
											</div>
											<div class="col-sm-9">
												<label>
													<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'activate_log' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="activate_log" data-reload="true">
													Show Error Reporting View
												</label>
											</div>
										</div>

										<?php 
    /*<div class="row form-group">
    			<div class="col-sm-4">
    				<p class="m0">Non-Latin Content</p>
    				<small class="text-off">If your content is written in non-latin characters the "Page Length" Attribute in your Content Optimizer may show a wrong number. Warning: This mode may show incorrect data with latin characters.</small>
    			</div>
    			<div class="col-sm-8">
    				<label>
    					<input class="form-control wsko-ajax-input" type="checkbox" <?=WSKO_Class_Core::get_setting('non_latin') ? 'checked="checked"' : ''?> data-wsko-target="settings" data-wsko-setting="non_latin">
    					Use Non-Latin Mode
    				</label>
    			</div>
    		</div> */
    ?>
										
										
										<div class="row form-group no-border">
											<div class="col-sm-3">
												<p class="m0">Lightweight Session Cache</p>
												<small class="text-off">To increase performance, your analysis data is generated only once and saved into your Session. Deactivating this option will advice BST to use a file-based cache in your "wp-uploads" folder.</small>
											</div>
											<div class="col-sm-9">
												<label>
													<input class="form-control wsko-ajax-input" type="checkbox" <?php 
    echo  ( WSKO_Class_Core::get_setting( 'use_leightweight_cache' ) ? 'checked="checked"' : '' ) ;
    ?> data-wsko-target="settings" data-wsko-setting="use_leightweight_cache">
													Use lightweight Cache
												</label>
												<br/>
												<br/>
												<small class="wsko-red">Attention: Using the lightweight cache will solve problems with the PHP Session, but will also reduce the plugins performance.</small>
											</div>
										</div>
										
										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Clear Session Cache</p>
												<small class="text-off">If you have seemingly false data, try deleting the Cache and revisit the corresponding page.</small>
											</div>
											<div class="col-sm-9">
												<?php 
    WSKO_Class_Template::render_ajax_button(
        'Clear Session Cache',
        'clear_session_cache',
        array(),
        array()
    );
    ?>
											</div>
										</div>
									</div>
								</div>
							</div>		
						</div>
					</div>
					
					<div id="tab_apis" class="tab-pane fade">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6 col-sm-6  col-xs-12">
										<?php 
    WSKO_Class_Template::render_lazy_field( 'api_search', 'big', 'center' );
    ?>
									</div>
									<?php 
    ?>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<?php 
    WSKO_Class_Template::render_lazy_field( 'api_twitter', 'big', 'center' );
    ?>
										<?php 
    //WSKO_Class_Template::render_lazy_field('api_facebook', 'big', 'center');
    ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="tab_configuration" class="tab-pane fade">				
						<div class="row">
							<div class="bsu-panel col-sm-12 col-xs-12">
								<div class="panel panel-default">
									<p class="panel-heading m0"><?php 
    echo  __( 'Import from other Plugins', 'wsko' ) ;
    ?></p>
									
									<div class="panel-body">
										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Import from other SEO Plugins</p>
											</div>
											<div class="col-sm-9">
												<?php 
    WSKO_Class_Template::render_template( 'admin/templates/settings/view-import.php', array() );
    ?>														
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="bsu-panel col-sm-12 col-xs-12">
								<div class="panel panel-default">
									<p class="panel-heading m0"><?php 
    echo  __( 'Automatic Backup Generation', 'wsko' ) ;
    ?></p>
									
									<div class="panel-body">
										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Interval</p>
												<small class="text-off">How often should a backup be created? 1 - every day, 2 - every 2 days, 3 - every 3 days,...</small>
											</div>
											<div class="col-sm-9">
												<input class="form-control wsko-ajax-input" type="number" placeholder="Default: 1" value="<?php 
    echo  WSKO_Class_Core::get_setting( 'conf_backup_interval' ) ;
    ?>" data-wsko-target="settings" data-wsko-setting="conf_backup_interval">
											</div>
										</div>
										
										<div class="row form-group">
											<div class="col-sm-3">
												<p class="m0">Limit</p>
												<small class="text-off">How many automatic backups should be stored</small>
											</div>
											<div class="col-sm-9">
												<input class="form-control wsko-ajax-input" type="number" placeholder="Default: 7" value="<?php 
    echo  WSKO_Class_Core::get_setting( 'conf_backup_limit' ) ;
    ?>" data-wsko-target="settings" data-wsko-setting="conf_backup_limit">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="bsu-panel col-sm-12 col-xs-12">
								<div class="panel panel-default">
									<p class="panel-heading m0"><?php 
    echo  __( 'Current Backups', 'wsko' ) ;
    ?></p>
									
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-3">
												<p class="m0">Save and restore Backup</p>
												<small class="text-off">Backup your plugin configuration, metas, social snippets and redirects. Please note: Search and Onpage analysis data will not be stored in the backup.</small>
											</div>
											<div class="col-sm-9">
											<?php 
    $global_report = WSKO_Class_Onpage::get_onpage_analysis();
    
    if ( !$global_report || !isset( $global_report['current_report'] ) || WSKO_Class_Search::get_se_token() && !WSKO_Class_Core::get_option( 'search_query_first_run' ) ) {
        WSKO_Class_Template::render_notification( 'warning', array(
            'msg' => 'WSKO is loading your initial data. Backups are disabled until the fetch is finished.',
        ) );
    } else {
        WSKO_Class_Template::render_ajax_button(
            'Backup Current Configuration',
            'backup_configuration',
            array(),
            array()
        );
        ?>
													<a class="button" data-toggle="collapse" href="#importBackup" role="button" aria-expanded="false">Import Backup</a>
													<?php 
        if ( $backups ) {
            WSKO_Class_Template::render_ajax_button(
                'Delete Backups',
                'delete_conf_backups',
                array(),
                array(
                'alert' => 'Do you really want to delete all of your backup files?',
            )
            );
        }
        ?>
													
													<div class="collapse wsko-import-collapse-wrapper" id="importBackup">
														<div class="row wsko-import-backup-wrapper">
															<div class="col-sm-12">
																<p>Import configuration backup file:</p>
															</div>	
															<div class="col-md-12">
																<input type="file" placeholder="Select .bst_backup file" accept=".bst_backup" class="wsko-import-backup-file form-control">
															</div>
															<div class="col-md-12 wsko-mt10">
																<a class="button wsko-import-backup button-primary" href="#" data-nonce="<?php 
        echo  wp_create_nonce( 'wsko_import_configuration_backup' ) ;
        ?>">Import</a>
																<a class="button" data-toggle="collapse" href="#importBackup" role="button" aria-expanded="false">Cancel</a>
															</div>
														</div>
													</div>		
												
													<ul class="wsko-backup-list">
														<?php 
        
        if ( $backups ) {
            $backups = array_reverse( $backups, true );
            ?>
															<p class="small text-off">CURRENT BACKUPS</p>
															<?php 
            foreach ( $backups as $k => $b ) {
                
                if ( $b['auto'] ) {
                    ?><li>Auto-Backup for '<?php 
                    echo  date( 'd.m.Y', $b['time'] ) ;
                    ?>' (created at '<?php 
                    echo  date( 'H:i', $b['time'] ) ;
                    ?>') <span class="pull-right"><?php 
                    WSKO_Class_Template::render_ajax_button(
                        'Restore',
                        'load_configuration_backup',
                        array(
                        'key' => $k,
                    ),
                        array(
                        'alert' => 'Are you sure? This will reset all your settings, including your metas.',
                    )
                    );
                    ?> <a class="button" href="<?php 
                    echo  WSKO_Controller_Download::get_download_link( 'backup', $k ) ;
                    ?>" target="_blank"><i class="fa fa-download"></i></a> <?php 
                    WSKO_Class_Template::render_ajax_button(
                        '<i class="fa fa-times"></i>',
                        'delete_configuration_backup',
                        array(
                        'key' => $k,
                    ),
                        array()
                    );
                    ?></span></li><?php 
                } else {
                    ?><li>Backup from '<?php 
                    echo  date( 'd.m.Y H:i', $b['time'] ) ;
                    ?>' <span class="pull-right"><?php 
                    WSKO_Class_Template::render_ajax_button(
                        'Restore',
                        'load_configuration_backup',
                        array(
                        'key' => $k,
                    ),
                        array(
                        'alert' => 'Are you sure? This will reset all your settings, including your metas.',
                    )
                    );
                    ?> <a class="button" href="<?php 
                    echo  WSKO_Controller_Download::get_download_link( 'backup', $k ) ;
                    ?>" target="_blank"><i class="fa fa-download"></i></a> <?php 
                    WSKO_Class_Template::render_ajax_button(
                        '<i class="fa fa-times"></i>',
                        'delete_configuration_backup',
                        array(
                        'key' => $k,
                    ),
                        array()
                    );
                    ?></span></li><?php 
                }
            
            }
        } else {
            ?><li>No Backup found.</li><?php 
        }
        
        ?>
													</ul>
												<?php 
    }
    
    ?>
											</div>	
										</div>
									</div>
								</div>
							</div>
							
							<div class="bsu-panel col-sm-12 col-xs-12">
								<div class="panel panel-default">
									<p class="panel-heading m0"><?php 
    echo  __( 'Reset BAVOKO SEO Tools', 'wsko' ) ;
    ?></p>
									<div class="panel-body">
										<div class="row">	
											<div class="col-sm-3">
												<p class="m0">Reset BST to factory default</p>
											</div>
											<div class="col-sm-9">
												<!-- Reset -->				
												<p class="mb10">Do you really want to reset BAVOKO SEO Tools? This will delete your entire configuration, including the options below. Note: An automatic backup of your config will be created unless you tick "Delete Configuration Backups".</p>
												<br/>
												<ul>
													<li><input class="form-control" type="checkbox" id="wsko_reset_opt_cache">Delete Database Cache</li>
													<li><input class="form-control" type="checkbox" id="wsko_reset_opt_backups">Delete Configuration Backups</li>
												</ul>
												<br/>
												<div class="row">
													<div class="col-md-6">
														<a id="wsko_reset_configuration" class="button" style="margin-top:10px;" data-nonce="<?php 
    echo  wp_create_nonce( 'wsko_reset_configuration' ) ;
    ?>"> Reset Configuration</a>
													</div>
												</div>
											</div>	
										</div>	
									</div>	
								</div>	
							</div>
						</div>
					</div>	

					<?php 
    
    if ( WSKO_Class_Core::get_setting( 'activate_log' ) ) {
        ?>
					<div id="tab_reports" class="tab-pane fade">				
						<div class="row">
							<div class="col-sm-12">
								<?php 
        WSKO_Class_Template::render_lazy_field( 'tab_reports', 'big', 'center' );
        ?>
							</div>	
						</div>
					</div>	
					<?php 
    }
    
    /*<div id="tab_import" class="tab-pane fade">
    			<h2>Import Plugin Data</h2>
    			<?php WSKO_Class_Template::render_template('admin/templates/settings/view-import.php', array()); ?>
    		</div>
    		
    		<div id="tab_status" class="tab-pane fade">				
    			<div class="row">
    				<div class="col-sm-12">
    					<?php WSKO_Class_Template::render_lazy_field('tab_status', 'big', 'center'); ?>
    				</div>	
    			</div>
    		</div>*/
    ?>
				</div>
		</div>
		</div>
	</div>
<?php 
    
    if ( isset( $_REQUEST['showtab'] ) && $_REQUEST['showtab'] ) {
        $target = false;
        switch ( $_REQUEST['showtab'] ) {
            case 'apis':
                $target = "#tab_apis_link";
                break;
            case 'advanced':
                $target = "#tab_advanced_link";
                break;
        }
        
        if ( $target ) {
            ?><script type="text/javascript">
		jQuery(document).ready(function($){
			$('<?php 
            echo  $target ;
            ?>').click();
		});
		</script><?php 
        }
    
    }

} else {
    WSKO_Class_Template::render_notification( 'error', array(
        'msg' => 'You are not able to edit the settings. Please contact your site administrator.',
    ) );
}
