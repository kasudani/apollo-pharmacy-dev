<?php
if (!defined('ABSPATH')) exit;

$social_client_fb = WSKO_Class_Social::get_facebook_client();
$social_token_fb = WSKO_Class_Social::get_social_token('facebook', true);
$social_valid_fb = !WSKO_Class_Social::check_facebook_access(true);
$selected_profile = WSKO_Class_Social::get_facebook_profile();
$social_token2_fb = WSKO_Class_Social::get_social_token('facebook');
$social_valid2_fb = !WSKO_Class_Social::check_facebook_access(false);
?>
<div class="wsko-settings-api-wrapper">
	<div class="bsu-panel">
		<div class="panel panel-default">
			<div class="pull-right m10">
				<i class="fa fa-circle" style="<?=$social_client_fb ? 'color:#5cb85c;' : 'color:#d9534f'?>" data-toggle="tooltip" data-title="<?=$social_client_fb ? 'API loaded' : 'API could not be loaded!'?>"></i>
				<i class="fa fa-circle" style="<?=$social_token_fb ? 'color:#5cb85c;' : 'color:#d9534f'?>" data-toggle="tooltip" data-title="<?=$social_token_fb ? 'Credentials provided' : 'No Credentials provided'?>"></i>
				<i class="fa fa-circle" style="<?=$social_valid_fb ? 'color:#5cb85c;' : ($social_valid2_fb ? 'color:#f0ad4e' : 'color:#d9534f;')?>" data-toggle="tooltip" data-title="<?=$social_valid_fb ? 'Permission granted' : ($social_valid2_fb ? 'User Token expired, but Page allready selected.' : 'Credentials invalid or insufficient permissions')?>"></i>
				<i class="fa fa-circle" style="<?=$selected_profile ? 'color:#5cb85c;' : 'color:#f0ad4e'?>" data-toggle="tooltip" data-title="<?=$selected_profile ? 'Page selected' : 'No Page selected'?>"></i>
				<i class="fa fa-circle" style="<?=$social_valid2_fb ? 'color:#5cb85c;' : 'color:#d9534f;'?>" data-toggle="tooltip" data-title="<?=$social_valid2_fb ? 'Page Access Token valid' : 'Page Access Token invalid'?>"></i>
			</div>
			<p class="panel-heading m0"><i class="fa fa-facebook-official fa-fw"></i> Facebook API
			</p>
			<div class="panel-body">
				<div class="wsko-settings-api-box"><?php
					if ($social_client_fb)
					{
						if ($social_token_fb)
						{
							if ($social_valid_fb)
							{
								$profiles = WSKO_Class_Social::get_facebook_profiles();
								if ($profiles && !empty($profiles))
								{
									?>
									Selected page: <?=$selected_profile?>
									<ul class="fb-subpages-wrapper" style="list-style-type:none;"><?php
									foreach ($profiles as $profile)
									{
										?><li class="wsko-fb-sub-item <?=$profile['id'] == $selected_profile ? 'bg-selected' : ''?>"><?=$profile['name']?> <span class="pull-right wsko-fb-connect-link"><?php WSKO_Class_Template::render_ajax_button('Select', 'set_facebook_profile', array('profile' => $profile['id']));?></span></li><?php
									}
									?></ul><?php
								}
								else
								{
									?>Your account doesn't have access to any page or the user token has expired. Please login again.<?php
								}
								if (WSKO_Class_Core::is_configured())
									WSKO_Class_Template::render_recache_api_button('social_facebook');
								WSKO_Class_Template::render_revoke_api_button('social_facebook');
							}
							else
							{
								WSKO_Class_Template::render_revoke_api_button('social_facebook');
							}
						}
						else
						{
							$loginUrl = WSKO_Class_Social::get_facebook_login_url();
							?><a class="button" href="<?=$loginUrl?>" target="_blank">Log in with Facebook</a><?php
						}
					}
					else
					{
						WSKO_Class_Template::render_notification('error', array('msg' => 'API could not be loaded. Another plugin may have loaded an earlier version of the Facebook API.'));
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</div>