<?php
if (!defined('ABSPATH')) exit;

$ga_client_se = WSKO_Class_Search::get_ga_client_se();
$ga_token_se = WSKO_Class_Search::get_se_token();
$ga_valid_se = false;
if (WSKO_Class_Core::is_configured())
{
	if (WSKO_Class_Core::get_option('search_query_first_run'))
		$ga_valid_se = !WSKO_Class_Search::check_se_access(true);
	else if ($ga_client_se && $ga_token_se)
		$ga_valid_se = true;
}
else
	$ga_valid_se = !WSKO_Class_Search::check_se_access(true);
?>
<div class="wsko-settings-api-wrapper">
	<div class="bsu-panel">
		<div class="panel panel-default">
			<div class="pull-right m10">
				<i class="fa fa-circle" style="<?=$ga_client_se ? 'color:#5cb85c;' : 'color:#d9534f;'?>" data-toggle="tooltip" data-title="<?=$ga_client_se ? 'API loaded' : 'API could not be loaded!'?>"></i>
				<i class="fa fa-circle" style="<?=$ga_token_se ? 'color:#5cb85c;' : 'color:#d9534f;'?>" data-toggle="tooltip" data-title="<?=$ga_token_se ? 'Credentials provided' : 'No Credentials provided'?>"></i>
				<i class="fa fa-circle" style="<?=$ga_valid_se ? 'color:#5cb85c;' : 'color:#d9534f;'?>" data-toggle="tooltip" data-title="<?=$ga_valid_se ? 'Permission granted' : 'Credentials invalid or insufficient permissions'?>"></i>
			</div>
			<p class="panel-heading m0"><i class="fa fa-search fa-fw"></i> Search Console API
			</p>
			<div class="panel-body">
				<div class="wsko-settings-api-box">
					<div class="wsko-api-login-help-box" style="display:none;margin-bottom:10px">
					</div>
					<div class="wsko-api-login-help-box-custom" style="display:none;margin-bottom:10px">
					</div>
				<?php
					if ($ga_client_se)
					{
						if ($ga_token_se)
						{
							if ($ga_valid_se)
							{
								if (WSKO_Class_Core::is_configured())
								{
									if (WSKO_Class_Core::get_option('search_query_first_run'))
									{
										WSKO_Class_Template::render_recache_api_button('ga_search');
										WSKO_Class_Template::render_delete_api_cache_button('ga_search');
									}
									else
									{
										WSKO_Class_Template::render_notification('info', array('msg' => 'Your first report is being fetched, please be patient.', 'subnote' => 'Working on package '.intval(WSKO_Class_Core::get_option('search_query_first_run_step')).' out of 9 packages.'));
									}
								}
								WSKO_Class_Template::render_revoke_api_button('ga_search');
							}
							else
							{
								WSKO_Class_Template::render_notification('error', array('msg' => 'Your credentials are invalid or you have insufficient permissions.'));
								WSKO_Class_Template::render_revoke_api_button('ga_search');
							}
						}
						else
						{
							$auth_url = WSKO_Class_Search::get_se_auth_url();
							if (!$auth_url)
							{
								WSKO_Class_Template::render_notification('error', array('msg' => 'Authentication URL could not be generated.'));
							}
							else
							{
								?>
								<p><a class="wp-core-ui button" style="margin-bottom: 5px;" href="<?=$auth_url?>" target="_blank">Get Access Token</a></p>
								<form class="wsko-admin-request-api-access" method="POST" data-nonce="<?=wp_create_nonce('wsko_request_api_access')?>" data-api="ga_search">
									<input placeholder="Insert Access Token" class="form-control wsko-token-field mb10" type="text" name="code" autocomplete="off" required>
									<input class="wsko-request-btn wp-core-ui button button-primary" type="submit" value="Submit">
								</form>
								<?php
							}
						}
					}
					else
					{
						WSKO_Class_Template::render_notification('error', array('msg' => 'API could not be loaded. Another plugin may have loaded an earlier version of the Google Client API.'));
					}
					?>
				</div>
			</div>
		</div>	
	</div>	
</div>