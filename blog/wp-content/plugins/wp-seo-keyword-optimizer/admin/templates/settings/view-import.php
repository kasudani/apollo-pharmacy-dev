<?php
if (!defined('ABSPATH')) exit;

$importable_plugins = WSKO_Class_Import::get_importable_plugins();
if ($importable_plugins)
{
	?>
	<div class="wsko-import-plugins-wrapper panel-group" id="wsko_import_plugins_wrapper">
	<?php
		$pluginRef = 0;
		foreach ($importable_plugins as $k => $pl)
		{
			?>
			<div class="bsu-panel panel panel-default wsko-import-plugin-wrapper z-depth-1" data-nonce="<?=wp_create_nonce('wsko_import_plugin')?>" data-plugin="<?=$k?>">
				<div class="panel-heading">
					<h4 class="panel-title">
						<i class="fa fa-plus fa-fw text-off"></i><a data-toggle="collapse" data-parent="#wsko_import_plugins_wrapper" href="#wsko_import_plugin<?=$pluginRef?>"><span class="pull-right"><i class="fa fa-angle-down font-unimportant"></i></span> <?=$pl['title']?> (<?=$pl['active'] ? '' : 'in'?>active)</a>
					</h4>
				</div>
				<div id="wsko_import_plugin<?=$pluginRef?>" class="panel-collapse collapse <?=($pluginRef == 0) ? 'in' : '';?>">
					<div class="panel-body">
						<?php 
						foreach ($pl['options'] as $opt_k => $opt)
						{
							?><p class="mb5"><input class="wsko-import-plugin-option form-control" type="checkbox" value="<?=$opt_k?>"> <?=$opt?></p><?php
						}
						?>
						<a style="margin-top: 5px; " class="wsko-import-plugin button button button-primary" href="">Import</a>
						<p class="text-off" style="margin-top: 5px; margin-bottom: 0px;">This may take a while</p>
					</div>
				</div>
			</div>	
			
			<?php
			$pluginRef++;	
		}
		?>
	</div>
	<?php
}
else
{
	?>No importable Plugins found.<?php
}
?>