<?php
if (!defined('ABSPATH'))
	exit;

$args = array(
	'posts_per_page'   => -1,
	'post_type'        => WSKO_POST_TYPE_ERROR,
	'post_status'      => 'any',
	'suppress_filters' => true 
);

$msgs = get_posts($args);
if (!empty($msgs))
{ 
	WSKO_Class_Template::render_ajax_button('Clear Logs', 'clear_error_reports', array(), array());
	?>
	<div class="panel-group" id="tab_errors_group" style="margin-top: 40px;">
		<?php
		$first = true;
		foreach ($msgs as $msg)
		{
			$type = '';
			switch ($msg->post_status)
			{
				case 'error':
					$type = '<i class="fa fa-times-circle" style="color:red"></i>';
					break;
				
				case 'warning':
					$type = '<i class="fa fa-exclamation-triangle" style="color:Gold"></i>';
					break;
				
				case 'info':
					$type = '<i class="fa fa-info-circle" style="color:LightSkyBlue"></i>';
					break;
			}
			?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#tab_errors_group" href="#log_report_<?=$msg->ID?>"><?=$type?> <b><?=$msg->post_title?></b> <p style="float:right"><?=get_the_date('d.m.Y H:i:s', $msg->ID)?></p></a>
					</h4>
				</div>
				<div id="log_report_<?=$msg->ID?>" class="panel-collapse collapse <?=$first ? 'in' : ''?>">
					<div class="panel-body">
						<?=$msg->post_content?>
					</div>
				</div>
			</div>
			<?php
			$first = false;
		} ?>
	</div><?php
}
else
{
	WSKO_Class_Template::render_notification('success', array('msg' => 'No Error Report found!'));
} ?>