<?php
if (!defined('ABSPATH')) exit;

/*if (!wp_next_scheduled('wsko_check_timeout'))
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'The Cronjob for your expire time check is not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
}*/
if (!wp_next_scheduled('wsko_daily_maintenance'))
{
	WSKO_Class_Template::render_notification('error', array('msg' => 'The Cronjob for your daily maintenance is not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
}
?>