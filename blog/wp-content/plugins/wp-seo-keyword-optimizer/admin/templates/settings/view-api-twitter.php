<?php
if (!defined('ABSPATH')) exit;

$social_client_tw = WSKO_Class_Social::get_twitter_client();
$social_profile_tw = WSKO_Class_Social::get_twitter_profile();
$social_valid_tw = !WSKO_Class_Social::check_twitter_access(true);
?>
<div class="wsko-settings-api-wrapper">
	<div class="bsu-panel">
		<div class="panel panel-default">
			<div class="pull-right m10">
				<i class="fa fa-circle" style="<?=$social_client_tw ? 'color:#5cb85c;' : 'color:#d9534f'?>" data-toggle="tooltip" data-title="<?=$social_client_tw ? 'API loaded' : 'API could not be loaded!'?>"></i>
				<i class="fa fa-circle" style="<?=$social_profile_tw ? 'color:#5cb85c;' : 'color:#d9534f'?>" data-toggle="tooltip" data-title="<?=$social_profile_tw ? 'Profile set' : 'No Profile set'?>"></i>
				<i class="fa fa-circle" style="<?=$social_valid_tw ? 'color:#5cb85c;' : 'color:#d9534f'?>" data-toggle="tooltip" data-title="<?=$social_valid_tw ? 'Permission granted' : 'Credentials invalid or insufficient permissions'?>"></i>
			</div>
			<p class="panel-heading m0"><i class="fa fa-twitter fa-fw"></i> Twitter API
			</p>
			<div class="panel-body">
				<div class="wsko-settings-api-box"><?php
					if ($social_client_tw)
					{
						if ($social_profile_tw)
						{
							if (!$social_valid_tw)
							{
								WSKO_Class_Template::render_notification('error', array('msg' => 'Your credentials are invalid or you have insufficient permissions.'));
							}
						}
						?><div class="wsko-api-twitter-set-profile">
							<input class="wsko-api-twitter-set-profile-value form-control mb10" value="<?=$social_profile_tw?>" placeholder="Your page's twitter username">
							<a href="#" class="button wsko-api-twitter-set-profile-btn" data-nonce="<?=wp_create_nonce('wsko_set_twitter_profile')?>">Set as Profile</a>
							<?php
							if ($social_profile_tw)
								{
									WSKO_Class_Template::render_recache_api_button('social_twitter');
								}
							?>	
						</div><?php
					}
					else
					{
						WSKO_Class_Template::render_notification('error', array('msg' => 'API could not be loaded. Another plugin may have loaded an earlier version of the Twitter API.'));
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</div>