<?php
WSKO_Class_Template::render_notification('warning', array('msg' => 'Google recently shortened the meta description length to approximately 160 characters. We recommend you to update your meta descriptions to get the best results.', 'discardable' => 'onpage_meta_desc_length_16_05_2018'));

if (!WSKO_Class_Helper::is_php_func_enabled('set_time_limit'))
{
    WSKO_Class_Template::render_notification('info', array('msg' => 'The critical function "set_time_limit" has been disabled on your server. This can lead to not finished scripts, corrupt data and thus endless loading.', 'subnote' => "This usually happens on shared hosting providers due to their systems structure. Please contact your website administrator."));
}
?>