<?php
if (!defined('ABSPATH')) exit;
$client = WSKO_Class_Search::get_ga_client_se();
if (defined('WSKO_GOOGLE_INCLUDE_FAILED') && WSKO_GOOGLE_INCLUDE_FAILED)
{
	$old_ver = Google_Client::LIBVER;
	
	if (!$client)
	{
		WSKO_Class_Template::render_notification('error', array('msg' => "Google API 2.1.0 couldn't be loaded, because you are already loading another version (".$old_ver.") of it. This has caused a failure with the client.", 'subnote' => 'This error is usually caused by a plugin or theme loading an old version of the Google Client API. Updating your plugins and themes may solve this error.'));
	}
	else
	{
		WSKO_Class_Template::render_notification('warning', array('msg' => "Google API 2.1.0 couldn't be loaded, because you are already loading another version (.".$old_ver.") of it. This may cause errors with deprecated methods, if your version is outdated.", 'subnote' => 'This error is usually caused by a plugin or theme loading an old version of the Google Client API. Updating your plugins and themes may solve this error.'));
	}
}
else
{
	if (WSKO_Class_Search::get_se_token())
	{
		if (!wp_next_scheduled('wsko_cache_keywords'))
		{
			WSKO_Class_Template::render_notification('error', array('msg' => 'The Cronjob for your Search Data is not running. '.WSKO_Class_Template::render_ajax_button('Reset CronJobs', 'reset_cronjobs', array(), array(), true)));
		}
		else if (!WSKO_Class_Core::get_option('search_query_first_run') && wp_next_scheduled('wsko_cache_keywords'))
		{
			if (wp_next_scheduled('wsko_cache_keywords') > time() && WSKO_Class_Core::get_option('search_query_last_start') && (WSKO_Class_Core::get_option('search_query_last_start') < (time()-(60*5)) || !WSKO_Class_Core::get_option('search_query_running')))
				WSKO_Class_Template::render_notification('warning', array('msg' => 'Your Search Data Request has timed out. '.WSKO_Class_Template::render_ajax_button('Reset CronJob', 'reset_keyword_update', array(), array(), true)));
			else
				WSKO_Class_Template::render_notification('info', array('msg' => 'Your Search Data is being fetched. Please wait a few minutes. Working on package '.intval(WSKO_Class_Core::get_option('search_query_first_run_step')).' out of 9 packages.', 'subnote' => 'You may see some errors while this message is being displayed.'));
		}
		else
		{
			if (WSKO_Class_Search::check_se_access())
			{
				WSKO_Class_Template::render_notification('error', array('msg' => 'Search API Error. See '.WSKO_Class_Template::render_page_link(WSKO_Controller_Settings::get_instance(), '#apis', 'Settings', false, true).' for more information.'));
			}
		}
	}
}
?>