<?php
if (!defined('ABSPATH')) exit;
?>
<div class="row">
	<?php
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'All Keywords', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'key', 'lazyVar' => 'total_keywords'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Keywords in Top 10', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'search', 'lazyVar' => 'total_keyword_dist'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Clicks', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'mouse-pointer', 'lazyVar' => 'total_clicks'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Impressions', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'eye', 'lazyVar' => 'total_impressions'));
	?>
</div>	
	
<div class="row">	
	<?php 
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Ranking Keywords History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_keywords'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Ranking Pages History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_pages'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Click History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_clicks'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Impressions History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_impressions'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Position History', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_position'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Keyword Ranking Distribution', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'lazyVar' => 'chart_history_ctr'));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Devices', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'info' => 'Clicks by device', 'lazyVar' => 'chart_history_devices', 'panelBody' => true));
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Location', 'wsko' ), 'col' => 'col-sm-6 col-xs-12', 'info' => 'Clicks by location', 'lazyVar' => 'chart_history_countries', 'panelBody' => true));	
	WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Keyword Ranking Distribution History', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'chart_history_rankings'));
	?>
</div>

<div class="row">		
	<?php
	WSKO_Class_Template::render_panel(array('type' => 'table-simple', 'title' =>  __( 'Top 5 Keywords', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_top_keywords', 'tableLink' => array('link' => WSKO_Controller_Search::get_link('keywords'), 'title' => __( 'All Keywords', 'wsko' ))));
	WSKO_Class_Template::render_panel(array('type' => 'table-simple', 'title' =>  __( 'Top 5 Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_top_pages', 'tableLink' => array('link' => WSKO_Controller_Search::get_link('pages'), 'title' => __( 'All Pages', 'wsko' ))));
	?>
</div>
