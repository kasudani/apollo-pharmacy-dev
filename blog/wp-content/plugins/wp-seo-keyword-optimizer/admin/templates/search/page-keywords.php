<?php
if (!defined('ABSPATH')) exit;

$invalid = WSKO_Class_Search::check_se_access();

if ($invalid)
	$tmp_fail = WSKO_Class_Template::render_template('admin/templates/template-no-api.php', array(), true);
?>

<div class="row">
	<?php 
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Keywords', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'key', 'lazyVar' => 'total_keywords'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Keywords in Top 10', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'search', 'lazyVar' => 'total_keyword_dist'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'New Keywords', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'plus', 'lazyVar' => 'total_new_keywords'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Lost Keywords', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'minus', 'lazyVar' => 'total_lost_keywords'));
	?>
</div>	

<div class="row">
	<div class="col-sm-12">
		<ul class="nav nav-tabs bsu-tabs border-dark">
		  <li class="waves-effect active"><a data-toggle="tab" href="#all_keywords"><?=__( 'All Keywords', 'wsko' ) ?></a></li>
		  <li class="waves-effect"><a data-toggle="tab" href="#new_keywords"><?=__( 'New Keywords', 'wsko' ) ?></a></li>
		  <li class="waves-effect"><a data-toggle="tab" href="#lost_keywords"><?=__( 'Lost Keywords', 'wsko' ) ?></a></li>
		</ul>

		<div class="tab-content row">
		  <div id="all_keywords" class="tab-pane fade in active">
			<?php 
			if (!$invalid)
			{
				WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'All Keywords', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_keywords'));
			}
			else
			{
				echo $tmp_fail;
			} ?>
		  </div>
		  <div id="new_keywords" class="tab-pane fade">
			<?php 
			if (!$invalid)
			{
				WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'New Keywords', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_new_keywords'));
			}
			else
			{
				echo $tmp_fail;
			} ?>
		  </div>
		  <div id="lost_keywords" class="tab-pane fade">
			<?php 
			if (!$invalid)
			{
				WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Lost Keywords', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_lost_keywords'));
			}
			else
			{
				echo $tmp_fail;
			} ?>
		  </div>
		</div>
	</div>
</div>