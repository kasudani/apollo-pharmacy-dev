<?php
if (!defined('ABSPATH')) exit;

$invalid = WSKO_Class_Search::check_se_access();

if ($invalid)
	$tmp_fail = WSKO_Class_Template::render_template('admin/templates/template-no-api.php', array(), true);
?>

<div class="row">
	<?php
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Pages', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'key', 'lazyVar' => 'total_pages'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Pages in Top 10', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'search', 'lazyVar' => 'total_page_dist'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'New Pages', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'plus', 'lazyVar' => 'total_new_pages'));
	WSKO_Class_Template::render_panel(array('type' => 'hero', 'title' => __( 'Lost Pages', 'wsko' ), 'col' => 'col-md-3 col-sm-6 col-xs-12', 'fa' => 'minus', 'lazyVar' => 'total_lost_pages'));
	?>
</div>	

<div class="row">
	<div class="col-sm-12">
		<ul class="nav nav-tabs bsu-tabs border-dark">
		  <li class="waves-effect active"><a data-toggle="tab" href="#all_pages"><?=__( 'All Pages', 'wsko' )?></a></li>
		  <li class="waves-effect"><a data-toggle="tab" href="#new_pages"><?=__( 'New Pages', 'wsko' )?></a></li>
		  <li class="waves-effect"><a data-toggle="tab" href="#lost_pages"><?=__( 'Lost Pages', 'wsko' )?></a></li>
		</ul>

		<div class="tab-content row">
		  <div id="all_pages" class="tab-pane fade in active">
		  <?php
			if (!$invalid)
			{
				WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'All Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_pages'));
			}
			else
			{
				echo $tmp_fail;
			} ?>
		  </div>
		  <div id="new_pages" class="tab-pane fade">
		  <?php
			if (!$invalid)
			{
				WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'New Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_new_pages'));
			}
			else
			{
				echo $tmp_fail;
			} ?>
		  </div>
		  <div id="lost_pages" class="tab-pane fade">
		  <?php
			if (!$invalid)
			{
				WSKO_Class_Template::render_panel(array('type' => 'lazy', 'title' => __( 'Lost Pages', 'wsko' ), 'col' => 'col-sm-12 col-xs-12', 'lazyVar' => 'table_lost_pages'));
			}
			else
			{
				echo $tmp_fail;
			} ?>
		  </div>
		</div>
	</div>
</div>

