<?php
if (!defined('ABSPATH')) exit;

$uniqid = WSKO_Class_Helper::get_unique_id();

$clicks = isset($template_args['clicks']) ? $template_args['clicks'] : array();
$impressions = isset($template_args['impressions']) ? $template_args['impressions'] : array();
$position = isset($template_args['position']) ? $template_args['position'] : array();
$ctr = isset($template_args['ctr']) ? $template_args['ctr'] : array();

$tmp_fail = WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array(), true);
?>
<ul class="nav nav-tabs bsu-tabs">
	<li class="active"><a href="#wsko_kw_monitoring_history_clicks_<?=$uniqid?>" data-toggle="tab">Clicks</a></li>
	<li><a href="#wsko_kw_monitoring_history_impressions_<?=$uniqid?>" data-toggle="tab">Impressions</a></li>
	<li><a href="#wsko_kw_monitoring_history_position_<?=$uniqid?>" data-toggle="tab">Position</a></li>
	<li><a href="#wsko_kw_monitoring_history_ctr_<?=$uniqid?>" data-toggle="tab">CTR</a></li>
</ul>
<div class="tab-content">
	<div id="wsko_kw_monitoring_history_clicks_<?=$uniqid?>" class="tab-pane fade in active">
		<?php if ($clicks)  
			WSKO_Class_Template::render_chart('area', array('Date', 'Sum. Clicks'), $clicks, array());
		else
			echo $tmp_fail; ?>
	</div>
	<div id="wsko_kw_monitoring_history_impressions_<?=$uniqid?>" class="tab-pane fade">
		<?php if ($impressions)  
			WSKO_Class_Template::render_chart('area', array('Date', 'Sum. Impressions'), $impressions, array());
		else
			echo $tmp_fail; ?>
	</div>
	<div id="wsko_kw_monitoring_history_position_<?=$uniqid?>" class="tab-pane fade">
		<?php if ($position)  
			WSKO_Class_Template::render_chart('area', array('Date', 'Avg. Position'), $position, array());
		else
			echo $tmp_fail; ?>
	</div>	
	<div id="wsko_kw_monitoring_history_ctr_<?=$uniqid?>" class="tab-pane fade">
		<?php if ($ctr)  
			WSKO_Class_Template::render_chart('area', array('Date', 'Avg. Click Through Rate'), $ctr, array());
		else
			echo $tmp_fail; ?>
	</div>
</div>