<?php
if (!defined('ABSPATH')) exit;

$post_id = isset($template_args['post_id']) ? $template_args['post_id'] : false;
$post_type = isset($template_args['post_type']) ? $template_args['post_type'] : false;
$post_tax = isset($template_args['post_tax']) ? $template_args['post_tax'] : false;
$post_term = isset($template_args['post_term']) ? $template_args['post_term'] : false;

$meta_view = isset($template_args['meta_view']) ? $template_args['meta_view'] : 'metas';

$type = isset($template_args['type']) ? $template_args['type'] : false;
$collapse = isset($template_args['collapse']) && $template_args['collapse'] ? true : false;
$is_collapse = isset($template_args['is_collapse_open']) && $template_args['is_collapse_open'] ? true : false;
$dataParent = isset($template_args['data-parent']) ? $template_args['data-parent'] : false;

$term_id = false;

$arg = false;
$metas = array();
$tax = array();
$overrides = array();
switch ($type)
{
	case 'post_id':
		$pms = get_post_meta($post_id);
		foreach ($pms as $pm => $val)
		{
			if (!isset($metas[$pm]))
				$metas[$pm] = 1;
			else
				$metas[$pm]++;
		}
		$pts = get_post_taxonomies($post_id);
		foreach ($pts as $pt)
		{
			if (!isset($tax[$pt]))
				$tax[$pt] = 1;
			else
				$tax[$pt]++;
		}
		$arg = $post_id;
		$overrides = array('tax' => false, 'term' => false, 'post' => $post_id);
		break;
	case 'post_archive':
		$url = get_post_type_archive_link($post_type);
		$arg = $post_type;
		break;
	case 'post_type':
		$url = get_post_type_archive_link($arg);
		if (strpos($url, '?') !== false)
			$url .= '&'.$arg.'='.'a-post';
		else
			$url .= '/a-post/';
		$rand_posts = WSKO_Class_Helper::get_random_post($post_type, 5);
		if ($rand_posts)
		{
			$post_id = $rand_posts[0]->ID;
			
			foreach ($rand_posts as $r)
			{
				$pms = get_post_meta($r->ID);
				foreach ($pms as $pm => $val)
				{
					if (!isset($metas[$pm]))
						$metas[$pm] = 1;
					else
						$metas[$pm]++;
				}
				$pts = get_post_taxonomies($r->ID);
				foreach ($pts as $pt)
				{
					if (!isset($tax[$pt]))
						$tax[$pt] = 1;
					else
						$tax[$pt]++;
				}
			}
			
			$first_post = WSKO_Class_Helper::get_first_post($post_type);
			if ($first_post)
				$post_id = $first_post->ID;
		}
		$arg = $post_type;
		$overrides = array('tax' => false, 'term' => false, 'post' => $post_id);
		break;
	case 'post_tax':
		$arg = $post_tax;
		$tax = $post_tax;
		$term_f = false;
		$terms = get_terms(array('taxonomy' => $post_tax, 'hide_empty' => false));
		foreach ($terms as $term)
		{
			if (!$term_f)
				$term_f = $term;
			$pms = get_term_meta($term->term_id);
			foreach ($pms as $pm => $val)
			{
				if (!isset($metas[$pm]))
					$metas[$pm] = 1;
				else
					$metas[$pm]++;
			}
		}
		$overrides = array('tax' => $tax, 'term' => $term_f ? $term_f->term_id : false, 'post' => false);
		$term_id = $term_f ? $term_f->term_id : false;
		$term = $term_f;
		break;
	case 'post_term':
		$arg = $post_term;
		$args = explode(':', $arg, 2);
		$term_id = false;
		$tax = false;
		if (count($args) == 2)
		{
			$tax = $args[0];
			$term_id = $args[1];
			update_term_meta($term_id, 'test_term_meta', 'test_val');
			$pms = get_term_meta($term_id);
			foreach ($pms as $pm => $val)
			{
				if (!isset($metas[$pm]))
					$metas[$pm] = 1;
				else
					$metas[$pm]++;
			}
			$overrides = array('tax' => $tax, 'term' => $term_id, 'post' => false);
		}
		break;
	case 'other':
		$arg = isset($template_args['arg']) ? $template_args['arg'] : false;
		$overrides = array('tax' => false, 'term' => false, 'post' => $post_id);
		break;
}

$meta_obj = false;
$meta_obj_r = false;

$title = "";
$url = "";
$desc = "";

$og_title = "";
$og_desc = "";
$og_img = "";
$tw_title = "";
$tw_desc = "";
$tw_img = "";
$title_r = "";
$desc_r = "";
$og_title_r = "";
$og_desc_r = "";
$og_img_r = "";
$tw_title_r = "";
$tw_desc_r = "";
$tw_img_r = "";

$meta_obj = WSKO_Class_Onpage::get_post_meta($arg, $type);
if ($post_id)
{
	//global $post;
	$post = get_post($post_id);
	//setup_postdata($post);
	$title_o = WSKO_Class_Helper::sanitize_meta(get_the_title($post->ID));
	$title = $title_d = WSKO_Class_Helper::get_default_page_title($title_o);
	$desc = WSKO_Class_Helper::sanitize_meta($post->post_content, 'the_content');
	$url = get_permalink($post->ID);
	
	$meta_obj_r = WSKO_Class_Onpage::get_post_meta($post->post_type, 'post_type');
}
if ($term_id)
{
	if ($type === 'post_term')
		$meta_obj_r = WSKO_Class_Onpage::get_post_meta($tax, 'post_tax');
	$term = get_term_by('id', $term_id, $tax);
	$title = $title_d = WSKO_Class_Helper::get_default_page_title(WSKO_Class_Helper::sanitize_meta($term->name));
}
if ($meta_obj_r)
{
	if (isset($meta_obj_r['title']) && $meta_obj_r['title'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['title'], $overrides);
		if ($meta)
			$title_r = $meta;
	}
	if (isset($meta_obj_r['desc']) && $meta_obj_r['desc'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['desc'], $overrides);
		if ($meta)
			$desc_r = $meta;
	}
	if (isset($meta_obj_r['og_title']) && $meta_obj_r['og_title'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['og_title'], $overrides);
		if ($meta)
			$og_title_r = $meta;
	}
	else
		$og_title_r = $title_r;
	
	if (isset($meta_obj_r['og_desc']) && $meta_obj_r['og_desc'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['og_desc'], $overrides);
		if ($meta)
			$og_desc_r = $meta;
	}
	else
		$og_desc_r = $desc_r;
	
	if (isset($meta_obj_r['og_img']) && $meta_obj_r['og_img'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['og_img'], $overrides);
		if ($meta)
			$og_img_r = $meta;
	}
	if (isset($meta_obj_r['tw_title']) && $meta_obj_r['tw_title'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['tw_title'], $overrides);
		if ($meta)
			$tw_title_r = $meta;
	}
	else
		$tw_title_r = $title_r;
	
	if (isset($meta_obj_r['tw_desc']) && $meta_obj_r['tw_desc'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['tw_desc'], $overrides);
		if ($meta)
			$tw_desc_r = $meta;
	}
	else
		$tw_desc_r = $desc_r;
	
	if (isset($meta_obj_r['tw_img']) && $meta_obj_r['tw_img'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj_r['tw_img'], $overrides);
		if ($meta)
			$tw_img_r = $meta;
	}
	
	if (isset($meta_obj_r['slug']) && $meta_obj_r['slug'])
	{
		$url = home_url($meta_obj_r['slug']);
	}
}
if ($meta_obj)
{
	if (isset($meta_obj['title']) && $meta_obj['title'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj['title'], $overrides);
		if ($meta)
			$title = $meta;
	}
	if (isset($meta_obj['desc']) && $meta_obj['desc'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj['desc'], $overrides);
		if ($meta)
			$desc = $meta;
	}
	if (isset($meta_obj['og_title']) && $meta_obj['og_title'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj['og_title'], $overrides);
		if ($meta)
			$og_title = $meta;
	}
	if (isset($meta_obj['og_desc']) && $meta_obj['og_desc'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj['og_desc'], $overrides);
		if ($meta)
			$og_desc = $meta;
	}
	if (isset($meta_obj['og_img']) && $meta_obj['og_img'])
	{
		$meta = $meta_obj['og_img'];
		if ($meta)
			$og_img = $meta;
	}
	if (isset($meta_obj['tw_title']) && $meta_obj['tw_title'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj['tw_title'], $overrides);
		if ($meta)
			$tw_title = $meta;
	}
	if (isset($meta_obj['tw_desc']) && $meta_obj['tw_desc'])
	{
		$meta = WSKO_Class_Onpage::calculate_meta($meta_obj['tw_desc'], $overrides);
		if ($meta)
			$tw_desc = $meta;
	}
	if (isset($meta_obj['tw_img']) && $meta_obj['tw_img'])
	{
		$meta = $meta_obj['tw_img'];
		if ($meta)
			$tw_img = $meta;
	}
	
	if ($type === "post_type")
	{
		if (isset($meta_obj['slug']) && $meta_obj['slug'])
		{
			$url = home_url($meta_obj['slug']);
		}
	}
}
else if ($type !== 'post_id' && !$post_id)
{
	if (!$title)
		$title = "Some Title";
	if (!$desc)
		$desc = "Description (no post selected)";
}
//if ($post_id)
	//wp_reset_postdata();

if ($post_id && has_post_thumbnail($post_id) && (!$tw_img || !$og_img))
{
	$thumbnail_url = get_the_post_thumbnail_url($post_id);
	if (!$tw_img)
		$tw_img = $thumbnail_url;
	if (!$og_img)
		$og_img = $thumbnail_url;
}
if (!$url)
	$url = home_url('/some-link/');
$unique_id = WSKO_Class_Helper::get_unique_id();
$max_len = 350;
if (mb_strlen($desc) > $max_len)
{
	$desc = substr($desc, 0, $max_len-3);
	$desc .= '...';
}

if (mb_strlen($og_desc) > $max_len)
{
	$og_desc = substr($og_desc, 0, $max_len-3);
	$og_desc .= '...';
}

if (mb_strlen($tw_desc) > $max_len)
{
	$tw_desc = substr($tw_desc, 0, $max_len-3);
	$tw_desc .= '...';
}
?><div class="wsko-set-metas-wrapper <?=$collapse ? 'wsko-collapsed' : ''?>" data-type="<?=$type?>" data-arg="<?=$arg?>" data-nonce="<?=wp_create_nonce('wsko_set_metas')?>" <?=$meta_view === 'noindex' || $meta_view === 'metas' ? 'data-robots="true"' : ''?> style="position:relative;">
	<div class="<?=$type === 'post_id' || $type === 'post_term' ? 'wsko-row' : 'wsko-row'?>">
		<div class="<?=$type === 'post_id' || $type === 'post_term' || $meta_view === 'links' ? 'wsko-col-sm-12' :'wsko-col-sm-8'?> wsko-col-xs-12">
			<?php  if ($post_id && $type !== 'post_id' && $meta_view !== 'noindex' && $meta_view !== 'links') { ?>
				<p><small class="font-unimportant">Example snippet was generated from "<a href="<?=get_edit_post_link($post_id)?>" target="_blank"><?=substr($title_o, 0, 25)?></a>"</small></p>
			<?php } else if ($term_id && $type !== 'post_term' && $meta_view !== 'noindex' && $meta_view !== 'links') { ?>
				<p><small class="font-unimportant">Example snippet was generated from term "<a href="<?=admin_url('term.php?taxonomy='.$tax.'&tag_ID='.$term->term_taxonomy_id)?>" target="_blank"><?=substr($term->name, 0, 25)?></a>"</small></p>
			<?php } ?>
			<?php if ($meta_view === 'social') { ?>
			<ul class="wsko-nav bsu-tabs bsu-tabs-sm wsko-tabs-social-snippets">
				<li><a class="wsko-nav-link wsko-nav-link-active" href="#wsko_metas_<?=$unique_id?>_facebook"><i class="fa fa-facebook fa-fw"></i> Facebook</a></li>
				<li><a class="wsko-nav-link" href="#wsko_metas_<?=$unique_id?>_twitter"><i class="fa fa-twitter fa-fw"></i> Twitter</a></li>
				<?php /* <li><a href="#wsko_metas_<?=$unique_id?>_tab" data-toggle="tab"><i class="fa fa-internet-explorer fa-fw"></i> Tab</a></li> */ ?>		
			</ul>
			<?php } ?>
			<div class="wsko-tab-content">
			<?php if ($type == "post_id" && $meta_obj_r) { 
				//WSKO_Class_Template::render_notification('warning', array('msg' => 'You have set '.WSKO_Class_Template::render_page_link(WSKO_Controller_Onpage::get_instance(), 'dynamic metas', 'metas', false, true).' for this post\'s post type. Fill the fields below to overwrite post type values.', 'show_support' => false));
			} ?>
			<?php if ($type == "post_term" && $meta_obj_r) { 
				//WSKO_Class_Template::render_notification('warning', array('msg' => 'You have set '.WSKO_Class_Template::render_page_link(WSKO_Controller_Onpage::get_instance(), 'dynamic metas', 'metas', false, true).' for this term\'s taxonomy. Fill the fields below to overwrite taxnomy values.', 'show_support' => false));
			} ?>
				<?php if ($meta_view === 'metas') { ?>
					<?php if ($collapse) { ?>
						<a class="wsko-metas-bulk-collapse btn btn-flat btn-sm pull-right m10" data-toggle="collapse" data-toggle-heading="Collapse" href="#snippet_input_wrapper_<?=$post_id?>_<?=$unique_id?>" aria-expanded="false">
							Edit
						</a>
					<?php } ?>
					<?php
					WSKO_Class_Template::render_meta_snippet('google', $title && (!$title_r || $title != $title_d) ? $title : $title_r, $url, $desc ? $desc : $desc_r, array('no_title' => !isset($meta_obj['title']) || !$meta_obj['title'], 'no_desc' => !isset($meta_obj['desc']) || !$meta_obj['desc']));
					?>
					<div class="<?=$collapse ? 'collapse'.($is_collapse?' in':'') : '' ?>" <?=$collapse ? 'id="snippet_input_wrapper_' . $post_id . '_'.$unique_id.'"' : ''?>>	
						<input name="meta_view" type="hidden" value="<?=$meta_view?>">
						<input name="collapse" type="hidden" value="<?=$collapse?'true':'false'?>">
						<?php  
							echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Title', 'value' => htmlentities($meta_obj && isset($meta_obj['title']) ? $meta_obj['title'] : ''), 'class' => 'wsko-metas-field-title', 'name' => 'title', 'placeholder' => 'Title - '.($title_r?"Default by ".($type=="post_term"?'taxonomy':'post type').": '".$title_r."'":"Default title"), 'id' => 'wsko_meta_title_input', 'progressBar' => ($type=='post_id' && $post_id) ? true : false, 'progressID' => 'meta_title-' . $post_id, 'progressType' => 'google_title'), true); 
							echo WSKO_Class_Template::render_form(array('type' => 'textarea', 'title' => 'Description', 'value' => htmlentities($meta_obj && isset($meta_obj['desc']) ? $meta_obj['desc'] : ''), 'class' => 'wsko-metas-field-desc', 'name' => 'desc', 'placeholder' => 'Description - '.($desc_r?"Default by ".($type=="post_term"?'taxonomy':'post type').": '".$desc_r."'":'not set'), 'id' => 'wsko_meta_description_input', 'progressBar' => ($type=='post_id' && $post_id) ? true : false, 'progressID' => 'meta_description-' . $post_id, 'progressType' => 'google_desc'), true); 
							?>								
							<div class="wsko-row form-group">
								<div class="wsko-col-sm-3 wsko-col-xs-12">
									<?=__('Robots', 'wsko')?>
								</div>
								<div class="wsko-col-sm-9 wsko-col-xs-12">
									<label><input class="form-control wsko-metas-field-robotsnf" name="robots_nf" type="checkbox" <?=($meta_obj && isset($meta_obj['robots']) && ($meta_obj['robots'] == 1 || $meta_obj['robots'] == 3)) ? 'checked' : ''?>> NoFollow</label>
									<label><input class="form-control wsko-metas-field-robotsni" name="robots_ni" type="checkbox" <?=($meta_obj && isset($meta_obj['robots']) && ($meta_obj['robots'] == 2 || $meta_obj['robots'] == 3)) ? 'checked' : ''?>> NoIndex</label>
								</div>
							</div>	
							<?php
							//echo WSKO_Class_Template::render_form(array('type' => 'submit', 'class' => 'btn btn primary'), true); 
						?> 

						<?php /*
						<input class="form-control wsko-metas-field-title" name="title" placeholder="Title" value="<?=$meta_obj ? $meta_obj['title'] : ''?>">
						<input class="form-control wsko-metas-field-desc" name="desc" placeholder="Description" value="<?=$meta_obj ? $meta_obj['desc'] : ''?>">
						<div style="float:right">
							<button class="button" style="background-color: lightgreen;">Set</button>
						</div>
						*/ ?>
					</div>	
				<?php } else if ($meta_view === 'social') { ?>
				<div id="wsko_metas_<?=$unique_id?>_facebook" class="wsko-tab wsko-tab-active">
					<?php
					$auto_snippet = WSKO_Class_Core::get_setting('auto_social_snippet');
					WSKO_Class_Template::render_meta_snippet('facebook', $og_title ? $og_title : ($og_title_r ? $og_title_r : ($auto_snippet ? $title : '')), $url, $og_desc ? $og_desc : ($og_desc_r ? $og_desc_r : ($auto_snippet ? $desc : '')), array('img' =>  $og_img ? $og_img : $og_img_r));
					?>

						<input name="meta_view" type="hidden" value="<?=$meta_view?>">
						<input name="collapse" type="hidden" value="<?=$collapse?'true':'false'?>">
						<?php /*
						<input class="form-control wsko-metas-field-title" name="og_title" placeholder="FB Title" value="<?=$meta_obj ? $meta_obj['og_title'] : ''?>">
						<input class="form-control wsko-metas-field-desc" name="og_desc" placeholder="FB Description" value="<?=$meta_obj ? $meta_obj['og_desc'] : ''?>">
						<div style="float:right">
							<button class="button" style="background-color: lightgreen;">Set</button>
						</div>
						*/ ?>
						<?php  
							echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Facebook Title', 'value' => htmlentities($meta_obj && isset($meta_obj['og_title']) ? $meta_obj['og_title'] : ''), 'class' => 'wsko-metas-field-title', 'name' => 'og_title', 'placeholder' => 'Facebook Title'), true); ?><?php
							echo WSKO_Class_Template::render_form(array('type' => 'textarea', 'title' => 'Facebook Description', 'value' => htmlentities($meta_obj && isset($meta_obj['og_desc']) ? $meta_obj['og_desc'] : ''), 'class' => 'wsko-metas-field-desc', 'name' => 'og_desc', 'placeholder' => 'Facebook Description'), true); 
							echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Facebook Preview Image', 'value' => htmlentities($meta_obj && isset($meta_obj['og_img']) ? $meta_obj['og_img'] : ''), 'class' => 'wsko-metas-field-img', 'name' => 'og_img', 'placeholder' => 'Image URL'), true); 
							//echo WSKO_Class_Template::render_form(array('type' => 'submit', 'class' => 'btn btn primary'), true); 
						?>	
		
				</div>
				<div id="wsko_metas_<?=$unique_id?>_twitter" class="wsko-tab">
					<?php
					WSKO_Class_Template::render_meta_snippet('twitter', $tw_title ? $tw_title : ($tw_title_r ? $tw_title_r : ($auto_snippet ? $title : '')), $url, $tw_desc ? $tw_desc : ($tw_desc_r ? $tw_desc_r : ($auto_snippet ? $desc : '')), array('img' =>  $tw_img ? $tw_img : $tw_img_r));
					?>
						<input name="meta_view" type="hidden" value="<?=$meta_view?>">
						<input name="collapse" type="hidden" value="<?=$collapse?'true':'false'?>">
						<?php /*
						<input class="form-control wsko-metas-field-title" name="tw_title" placeholder="Twitter Title" value="<?=$meta_obj ? $meta_obj['tw_title'] : ''?>">
						<input class="form-control wsko-metas-field-desc" name="tw_desc" placeholder="Twitter Description" value="<?=$meta_obj ? $meta_obj['tw_desc'] : ''?>">
						<div style="float:right">
							<button class="button" style="background-color: lightgreen;">Set</button>
						</div>
						*/ ?>
						
						<?php  
							echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Twitter Title', 'value' => htmlentities($meta_obj && isset($meta_obj['tw_title']) ? $meta_obj['tw_title'] : ''), 'class' => 'wsko-metas-field-title', 'name' => 'tw_title', 'placeholder' => 'Twitter Title'), true); 
							echo WSKO_Class_Template::render_form(array('type' => 'textarea', 'title' => 'Twitter Description', 'value' => htmlentities($meta_obj && isset($meta_obj['tw_desc']) ? $meta_obj['tw_desc'] : ''), 'class' => 'wsko-metas-field-desc', 'name' => 'tw_desc', 'placeholder' => 'Twitter Description'), true); 
							echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Twitter Preview Image', 'value' => htmlentities($meta_obj && isset($meta_obj['tw_img']) ? $meta_obj['tw_img'] : ''), 'class' => 'wsko-metas-field-img', 'name' => 'tw_img', 'placeholder' => 'Image URL'), true); 
							//echo WSKO_Class_Template::render_form(array('type' => 'submit', 'class' => 'btn btn primary'), true); 
						?>	
				</div>
				<?php } else if ($meta_view === 'noindex') { ?>
					<?php
					//WSKO_Class_Template::render_meta_snippet('google', $title, $url, $desc);
					?>
						<input name="meta_view" type="hidden" value="<?=$meta_view?>">					
						<div class="wsko-row form-group">
							<div class="wsko-col-sm-3 wsko-col-xs-12">
								<?=__('Robots', 'wsko')?>
							</div>
							<div class="wsko-col-sm-9 wsko-col-xs-12">
								<label><input class="form-control wsko-metas-field-robotsnf" name="robots_nf" type="checkbox" <?=($meta_obj && isset($meta_obj['robots']) && ($meta_obj['robots'] == 1 || $meta_obj['robots'] == 3)) ? 'checked' : ''?>> NoFollow</label>
								<label><input class="form-control wsko-metas-field-robotsni" name="robots_ni" type="checkbox" <?=($meta_obj && isset($meta_obj['robots']) && ($meta_obj['robots'] == 2 || $meta_obj['robots'] == 3)) ? 'checked' : ''?>> NoIndex</label>
							</div>
						</div>	
						<?php
						echo WSKO_Class_Template::render_form(array('type' => 'submit', 'class' => 'btn btn primary'), true); 
						?> 

						<?php /*
						<input class="form-control wsko-metas-field-title" name="title" placeholder="Title" value="<?=$meta_obj ? $meta_obj['title'] : ''?>">
						<input class="form-control wsko-metas-field-desc" name="desc" placeholder="Description" value="<?=$meta_obj ? $meta_obj['desc'] : ''?>">
						<div style="float:right">
							<button class="button" style="background-color: lightgreen;">Set</button>
						</div>
						*/ ?>
				<?php } else if ($meta_view === 'links') { 
							?><input name="meta_view" type="hidden" value="<?=$meta_view?>"><?php
							$auto_redirects = WSKO_Class_Onpage::get_automatic_redirects();
							if ($type === "post_id" && $post)
							{
								$slug = $post->post_name;
								echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'URL Slug', 'value' => htmlentities($slug), 'class' => 'wsko-metas-field-url', 'name' => 'url', 'placeholder' => 'some-slug', 'id' => 'wsko_meta_url_input'), true); 
							}
							else if ($type === "post_type")
							{
								$data = WSKO_Class_Helper::get_obj_rewrite_base($type, $arg);
								if ($data)
								{
									$first_slug = $data['original'];
									$curr_slug = $data['current'];
								}
								else
								{
									$first_slug = "<i>not provided</i>";
									$curr_slug =  "<i>not provided</i>";
								}
								/*?><div class="wsko-row form-group">
									<div class="wsko-col-sm-3 wsko-col-xs-12">
										<?=__('Hide Slug', 'wsko')?>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">
										<label><input class="form-control wsko-metas-field-hide-slug" name="hide_slug" type="checkbox" <?=($meta_obj && isset($meta_obj['hide_slug']) && $meta_obj['hide_slug']) ? 'checked' : ''?>> Hide Slug</label>
									</div>
								</div><?php*/
								echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Post Type Slug<br/><small class="text-off">Original: '.$first_slug.' • Current: '.$curr_slug.'</small>', 'value' => htmlentities($meta_obj && isset($meta_obj['slug']) ? $meta_obj['slug'] : ''), 'class' => 'wsko-metas-field-url', 'name' => 'url', 'placeholder' => 'The URL part between your domain and the post name', 'id' => 'wsko_meta_url_input'), true); 
							}
							else if ($type === "post_tax")
							{
								$data = WSKO_Class_Helper::get_obj_rewrite_base($type, $arg);
								if ($data)
								{
									$first_slug = $data['original'];
									$curr_slug = $data['current'];
								}
								else
								{
									$first_slug = "<i>not provided</i>";
									$curr_slug =  "<i>not provided</i>";
								}
								echo WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Taxonomy Slug<br/><small class="text-off">Original: '.$first_slug.' • Current: '.$curr_slug.'</small>', 'value' => htmlentities($meta_obj && isset($meta_obj['slug']) ? $meta_obj['slug'] : ''), 'class' => 'wsko-metas-field-url', 'name' => 'url', 'placeholder' => 'The URL part between your domain and the post name', 'id' => 'wsko_meta_url_input'), true);
							}
							
							if ($type === 'post_type' || $type === 'post_tax')
							{
								?>
								<div class="wsko-border wsko-mb10 wsko-mt10 hidden-xs"></div>
								
								<div class="wsko-row form-group hidden-xs">
									<div class="wsko-col-sm-3 wsko-col-xs-12">
										<p>Automatic redirects</p>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">
										<div class="wsko-row"> <div class="col-sm-5"><label class="wsko-label small">Redirect from</label></div> <div class="col-sm-5"><label class="wsko-label small">Redirect to</label></div> <div class="col-sm-2"><label class="wsko-label small">Count</label></div> </div>
										<ul class="wsko-auto-redirect-wrapper"><?php
											if (isset($auto_redirects[$type][$arg]) && $auto_redirects[$type][$arg] && $auto_redirects[$type][$arg]['source'])
											{
												$auto_redirects = $auto_redirects[$type][$arg];
												foreach ($auto_redirects['source'] as $re => $link_snapshot)
												{
													?>
													<li>
														<div class="row">	
															<div class="col-sm-5">
																<?=home_url($re).'/*'?>
															</div>
															<div class="col-sm-5">
																<?=home_url($auto_redirects['target'].'/*')?>
															</div>
															<div class="col-sm-1">
																<?=count($link_snapshot)?>
															</div>	
															<div class="col-sm-1 align-right wsko-a-normalize">
																<?php WSKO_Class_Template::render_ajax_button('<i class="fa fa-times dark"></i>', 'remove_auto_redirect', array('type' => $type, 'arg' => $arg, 'key' => $re), array()); ?>
															</div>															
														</div>
													</li><?php
												}
											}
											else
											{
												?><li>No redirect found.</li><?php
											}
										?></ul>
									</div>
								</div>	
						<?php } ?>
					<?php } ?>
				<?php /*
				<div id="wsko_metas_<?=$unique_id?>_tab" class="tab-pane fade">
					<?php
					WSKO_Class_Template::render_meta_snippet('tab', $title, $url, $desc);
					?>
					<br><br>
					<form class="wsko-set-metas-form">
						<input class="form-control wsko-metas-field-title" name="title" placeholder="Title" value="<?=$meta_obj ? $meta_obj['title'] : ''?>">
						<div style="float:right">
							<button class="button" style="background-color: lightgreen;">Set</button>
						</div>
					</form>
				</div>	
				 */ ?>
			</div>
		</div>

		<?php if($type !== 'post_id' && $type !== 'post_term' && $meta_view !== 'links') { ?>
			<div class="wsko-col-sm-4 wsko-col-xs-12 wsko-metas-search-wrapper">
				<input type="text" class="wsko-metas-search-field form-control wsko-mb10" placeholder="Filter post fields">
				<!--<span class="font-unimportant">Use the identified dragables from the sidebox or add "%meta:meta_name%"/"%tax:tax_name%" to your text to display dynamic post data within your SEO data.</span>-->
				<p class="text-off pull-right"><small><i class="fa fa-compress"></i> Drag & Drop</small></p>
				<label><?=__('Site Attributes', 'wsko')?></label>
				<ul class="wsko-snippet-fields" style="list-style-type:none;">
					<li class="wsko-metas-placeholder" data-tag="%site:blog_name%" data-search="site title site name"><i class="fa fa-compress"></i> Site Name</li>
					<li class="wsko-metas-placeholder" data-tag="%site:blog_tagline%" data-search="site tagline site tag line"><i class="fa fa-compress"></i> Site Tagline</li>
				</ul>
				<?php
				switch ($type)
				{
					case 'post_id':
					case 'post_type':
					case 'other':
					?>
					<label><?=__('Post Attributes', 'wsko')?></label>
					<ul class="wsko-snippet-fields" style="list-style-type:none;">
						<li class="wsko-metas-placeholder" data-tag="%post:post_title%" data-search="post title"><i class="fa fa-compress"></i> Post Title</li>
						<li class="wsko-metas-placeholder" data-tag="%post:post_content%" data-search="post content"><i class="fa fa-compress"></i> Post Content</li>
						<li class="wsko-metas-placeholder" data-tag="%post:post_excerpt%" data-search="post excerpt"><i class="fa fa-compress"></i> Post Excerpt</li>
						<li class="wsko-metas-placeholder" data-tag="%post:post_author%" data-search="post author display name"><i class="fa fa-compress"></i> Post Author (Display Name)</li>
						<li class="wsko-metas-search-no-items wsko-text-off" style="display:none;">No matches found.</li>
					</ul>
					<label><?=__('Custom Fields', 'wsko')?></label>			
					<ul class="wsko-snippet-fields" style="list-style-type:none;">
						<?php
						if (!empty($metas))
						{
							foreach ($metas as $meta => $count)
							{
								?><li class="wsko-metas-placeholder" data-tag="%meta:<?=$meta?>%" data-search="<?=strtolower($meta)?>"><i class="fa fa-compress"></i> <?=$meta?></li><?php
							}
						}
						else
						{
							?><li>No custom fields found.</li><?php
						}
						?>
						<li class="wsko-metas-search-no-items wsko-text-off" style="display:none;">No matches found.</li>
					</ul>

					<label><?=__('Taxonomies', 'wsko')?></label>
					<ul class="wsko-snippet-fields" style="list-style-type:none;">
						<?php
						if (!empty($tax))
						{
							foreach ($tax as $t => $count)
							{
								?><li class="wsko-metas-placeholder" data-tag="%tax:<?=$t?>%" data-search="<?=strtolower($meta)?>"><i class="fa fa-compress"></i> <?=$t?></li><?php
							}
						}
						else
						{
							?><li>No taxonomies found.</li><?php
						}
						?>
						<li class="wsko-metas-search-no-items wsko-text-off" style="display:none;">No matches found.</li>
					</ul>
					<?php
					break;
					
					case 'post_tax':
					case 'post_term':
					?>
					<label><?=__('Term Attributes', 'wsko')?></label>
					<ul class="wsko-snippet-fields" style="list-style-type:none;">
						<li class="wsko-metas-placeholder" data-tag="%term:term_title%" data-search="term title"><i class="fa fa-compress"></i> Term Title</li>
						<li class="wsko-metas-placeholder" data-tag="%term:term_desc%" data-search="term description"><i class="fa fa-compress"></i> Term Description</li>
						<li class="wsko-metas-search-no-items" style="display:none;background-color:wheat;"><i>No matches found.</i></li>
					</ul>
					<label><?=__('Term Custom Fields', 'wsko')?></label>			
					<ul class="wsko-snippet-fields" style="list-style-type:none;">
						<?php
						if (!empty($metas))
						{
							foreach ($metas as $meta => $count)
							{
								?><li class="wsko-metas-placeholder" data-tag="%term_meta:<?=$meta?>%" data-search="<?=strtolower($meta)?>"><i class="fa fa-compress"></i> <?=$meta?></li><?php
							}
						}
						else
						{
							?><li>No custom fields found.</li><?php
						}
						?>
						<li class="wsko-metas-search-no-items" style="display:none;background-color:wheat;"><i>No matches found.</i></li>
					</ul><?php
					break;
				}
				?>
			</div>
		<?php } ?>	
	</div>
	<div class="wsko-metas-load-overlay" style="position:absolute;top:0px;width:100%;height:100%;opacity:0.7;background-color:white;display:none;">
		<?php echo WSKO_Class_Template::render_wsko_preloader(array('size' => 'big')); ?>
	</div>
</div>