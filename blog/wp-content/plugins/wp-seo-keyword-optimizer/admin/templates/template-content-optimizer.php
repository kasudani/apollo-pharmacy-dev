<?php
if (!defined('ABSPATH')) exit;

$post_id = isset($template_args['post_id']) ? $template_args['post_id'] : false;
$widget = isset($template_args['widget']) && $template_args['widget'] ? true : false;
$keywords = isset($template_args['keywords']) ? $template_args['keywords'] : false;
$preview = isset($template_args['preview']) ? $template_args['preview'] : false;
$open_tab = isset($template_args['open_tab']) ? $template_args['open_tab'] : false;
$uniqid = WSKO_Class_Helper::get_unique_id();
if ($post_id)
{
	//global $post;
	$post = get_post($post_id);
	$title_o = get_the_title($post->ID);
	$content = $preview && $preview['post_content'] ? $preview['post_content'] : $post->post_content;
	$title = $preview && $preview['post_title'] ? $tile_o = $preview['post_title'] : $post->post_title;
	$slug = $preview && $preview['post_slug'] ? $preview['post_slug'] : $post->post_name;
	$link = get_permalink($post_id);
	$edit_link = get_edit_post_link($post_id);
	
	$op_report = WSKO_Class_Onpage::get_onpage_report($post_id, $preview);
	$priority_keywords = WSKO_Class_Onpage::get_priority_keywords($post_id);
	$priority_keywords_count = 0;
	if ($priority_keywords)
	{
		foreach ($priority_keywords as $pk => $prio)
		{
			$priority_keywords[$pk] = array('prio' => $prio);
			if (isset($op_report['issues']['keyword_density'][$pk]))
			{
				$priority_keywords[$pk]['den'] = $op_report['issues']['keyword_density'][$pk]['density'];
				$priority_keywords[$pk]['den_type'] = $op_report['issues']['keyword_density'][$pk]['type'];
			}
		}
		$priority_keywords_count = count($priority_keywords);
	}
	
	$t_seo_data = WSKO_Class_Onpage::get_technical_seo_data($post_id);
	$table_keywords = array();
	$kw_count = 0;
	$sum_clicks = 0;
	$sum_imp = 0;
	$avg_pos = 0;
	if ($keywords)
	{
		foreach ($keywords as $kw)
		{
			$kw_data = array('clicks' => '-', 'pos' => '-', 'kw_den' => '-');
			if (isset($priority_keywords[$kw->keyval]))
			{
				$priority_keywords[$kw->keyval]['clicks'] = $kw->clicks;
				$priority_keywords[$kw->keyval]['pos'] = $kw->position;
			}
			$kw_count++;
			$sum_clicks += $kw->clicks;
			$sum_imp += $kw->impressions;
			$avg_pos += $kw->position;
			$table_keywords[] = array(array('value' => $kw->keyval), array('value' => $kw->position), array('value' => $kw->clicks), array('value' => $kw->impressions), array('order' => $kw->ctr, 'value' => round($kw->ctr, 2).'%'), array('value' => '<a href="#" class="wsko-co-add-priority-keyword-inline dark" data-toggle="tooltip" data-title="Add to Priority Keywords" data-post="'.$post_id.'" data-keyword="'.$kw->keyval.'" data-prio="1" data-nonce="'.wp_create_nonce('wsko_co_add_priority_keyword').'"><i class="fa fa-plus fa-fw"></i></a>'));
		}
		if ($kw_count)
			$avg_pos = round($avg_pos / $kw_count, 0);
	}
	$sitemap_excluded = false;
	$gen_params = WSKO_Class_Onpage::get_sitemap_params();
	if (isset($gen_params['excluded_posts']) && is_array($gen_params['excluded_posts']) && $gen_params['excluded_posts'])
		$sitemap_excluded = in_array($post_id, $gen_params['excluded_posts']);
	$last_crawl = false;
	$onpage_data = WSKO_Class_Onpage::get_onpage_data();
	if ($onpage_data && isset($onpage_data['global_analysis']['current_report']) && $onpage_data['global_analysis']['current_report'])
		$last_crawl = $onpage_data['global_analysis']['current_report']['started'];
	$crawl_data = WSKO_Class_Onpage::get_onpage_page_crawl_data($link);
	$onpage_score_progress = $crawl_data && $op_report ? ($crawl_data['onpage_score'] != 0 && $crawl_data['onpage_score'] != $op_report['onpage_score'] ? round(($op_report['onpage_score'] - $crawl_data['onpage_score']) / $crawl_data['onpage_score'] * 100, 2) : 0) : false;
	?>
	<div class="wsko-content-optimizer wsko_wrapper <?=$widget ? 'wsko-co-widget wsko-short-view-active' : 'wsko-co-modal'?>">
		<?php
		if (!$widget)
		{
			?>
			<div class="wsko-content-optimizer-progress-wrapper">
				<div class="pull-right">
				<?php
					WSKO_Class_Template::render_radial_progress('success', __('Content Score' ,'wsko'), array('val' => $op_report ? $op_report['onpage_score'] : 0, 'class' => 'hidden-xs' ));
					if ($crawl_data && $op_report && $crawl_data['onpage_score'] != $op_report['onpage_score'])
					{
						WSKO_Class_Template::render_radial_progress('success', __('Last Crawl' ,'wsko'), array('val' => $crawl_data['onpage_score'] ? $crawl_data['onpage_score'] : 0, 'class' => 'hidden-xs wsko-co-progress-ref', 'info' => $last_crawl?date('d.m.Y',$last_crawl):'never' ));
						/* 
						<small class="text-off">Last Crawl: <?=$crawl_data['onpage_score']?>% <?php WSKO_Class_Template::render_progress_icon($crawl_data['onpage_score'], $onpage_score_progress, array('tooltip' => ' %')); ?>
						<br/>
						from: <?php echo ($last_crawl?date('d.m.Y',$last_crawl):'never'); ?></small><?php
						*/
					}
					?>	
				</div>
				<!--div class="wsko-premium-wrapper">
					<a href="https://www.bavoko.tools/" target="_blank" class="wsko-pro-link">PRO FEATURE</a>
					<?php
					//WSKO_Class_Template::render_radial_progress('success', __('Technical' ,'wsko'), array('val' => '41', 'class' => 'blurred wsko-inline-block hidden-xs' ));
					//WSKO_Class_Template::render_radial_progress('success', __('Page Score' ,'wsko'), array('val' => '70', 'class' => 'blurred wsko-inline-block hidden-xs' ));
					?>	
				</div-->
			</div>
			<div class="panel-heading wsko-bg-gray">
				<h4 style="margin:5px 0px 0px;"><?=$title_o?esc_html($title_o):WSKO_Class_Helper::get_empty_page_title()?> <a class="wsko-small dark" target="_blank" href="<?=$edit_link?>"><i class="fa fa-edit fa-fw"></i></a></h4>
				<a href="<?=$link?>"><?=$link?></a><br/>
				 <small class="font-unimportant">Last edited: <?=get_post_modified_time('d.m.Y H:i', true, $post_id)?></small>
			</div>
			<?php
		}
		else
		{
			$box_height = WSKO_Class_Core::get_option('co_box_height_'.get_current_user_id());
			?><div class="wsko-resizable-wrapper wsko_wrapper" style="min-height:200px;max-height:300px;height:<?=$box_height?$box_height:'0'?>px" data-height="<?=$box_height?$box_height:'0'?>" data-nonce="<?=wp_create_nonce('wsko_co_change_height')?>">
				<div style="position:relative;height:100%;overflow-y:auto;overflow-x:hidden;" class="wsko-resizable-content"><?php
		}
		?>
			<div class="wsko-shadow <?=$widget ? '' : 'wsko-bg-gray'?>">
				<ul class="wsko-nav wsko-nav-main bsu-tabs m0" <?=$widget ? 'style="display:none;"' : ''?>>
				  <li><a class="wsko-nav-link <?=!$open_tab ? 'wsko-nav-link-active' : ''?>" href="#wsko_overview_<?=$uniqid?>"><?=__( 'Overview', 'wsko' ) ?></a></li>
				  <li><a class="wsko-nav-link <?=$open_tab === 'metas' ? 'wsko-nav-link-active' : ''?>" href="#wsko_metas_<?=$uniqid?>"><?=__( 'Metas', 'wsko' ) ?></a></li>
				  <li><a class="wsko-nav-link <?=$open_tab === 'social' ?  'wsko-nav-link-active' : ''?>" href="#wsko_snippets_<?=$uniqid?>"><?=__( 'Social Snippets', 'wsko' ) ?></a></li>
				  <li><a class="wsko-nav-link <?=$open_tab === 'keywords' ?  'wsko-nav-link-active' : ''?>" href="#wsko_keywords_<?=$uniqid?>"><?=__( 'Keywords', 'wsko' ) ?></a></li>
				  <li><a class="wsko-nav-link <?=$open_tab === 'technical' ?  'wsko-nav-link-active' : ''?>" href="#wsko_technical_<?=$uniqid?>"><?=__( 'Advanced', 'wsko' ) ?></a></li>
				  <?php if (!$widget) { ?><li><a class="wsko-nav-link <?=$open_tab === 'content' ?  'wsko-tab-active' : ''?>" href="#wsko_content_<?=$uniqid?>"><?=__( 'Content', 'wsko' ) ?></a></li><?php } ?>
				</ul>
				<?php if ($widget) { ?>
					<a href="#" class="wsko-resizable-wrapper-quick-up dark wsko-co-collapse-top text-off">
						<i class="fa fa-times"></i> Collapse 
					</a>
				<?php } ?>	
			</div>	
			<div class="wsko-co-main <?=$widget ? 'mt15' : 'wsko-panel-body'?>">	
				<div class="wsko-row">
					<div class="wsko-co-notifications-overlay" style="display:none;"></div>
				</div>	
				<div class="wsko-row" style="margin-right:-13px;margin-left:-13px;">
					<div class="wsko-col-sm-8 wsko-col-xs-12 wsko-widget-main">
						<div class="wsko-tab-content wsko-content-optimizer-inner">
						  <div id="wsko_overview_<?=$uniqid?>" class="wsko-tab <?=!$open_tab ?  'wsko-tab-active' : ''?>">
								<div class="wsko-content-optimizer-overview">
									<?php /* WSKO_Class_Template::render_template('admin/templates/onpage/view-url-check.php', array('url' => $link, 'suppress_success' => true)); */ ?>
									<div class="wsko-row">
									  <?php
										WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'fa' => '', 'title' => __('Keywords', 'wsko'), 'col' => 'wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12', 'custom' => $keywords ? ($kw_count && $kw_count != 0 ? $kw_count : '0') : '-'));
										WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'fa' => '', 'title' => __('Klicks', 'wsko'), 'col' => 'wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12', 'custom' => $keywords ? ($sum_clicks && $sum_clicks != 0 ? $sum_clicks : '0') : '-'));
										WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'fa' => '', 'title' => __('Impressions', 'wsko'), 'col' => 'wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12', 'custom' => $keywords ? ($sum_imp && $sum_imp != 0 ? $sum_imp : '0') : '-'));
										WSKO_Class_Template::render_panel(array('type' => 'hero-custom', 'fa' => '', 'title' => __('Avg. Position', 'wsko'), 'col' => 'wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12', 'custom' => $keywords ? ($avg_pos && $avg_pos != 0 ? $avg_pos : '0') : '-'));
										?>
									</div>
									<div class="wsko-co-onpage-issues-wrapper">
									<?php
									$issues = array();
									if ($op_report)
									{
										/*$html_error_str = "";
										if ($op_report['issues']['html_error_infos'])
										{
											foreach ($op_report['issues']['html_error_infos'] as $err)
											{
												$html_error_str .= ($err->message .' (Line: '.$err->line.', Column '.$err->column.'), ');
											}
										}
										$html_error_str = substr($html_error_str, 0, 300);
										switch ($op_report['issues']['html_errors'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'Your Content has HTML errors! '/ *.($html_error_str?WSKO_Class_Template::render_infoTooltip($html_error_str, 'info', true):'')* /, 'prio' => '1', 'group' => 'Content Length'); break;
											//case 1: $issues[] = array('type' => 'success', 'msg' => 'No HTML errors!', 'prio' => '1', 'group' => 'Content Length'); break;
										}*/

										//WSKO_Class_Template::render_panel(array('type' => 'issue', 'title' => __('Meta Title Issues', 'wsko'), 'col' => 'col-md-12 col-sm-12 col-xs-12'));
										switch ($op_report['issues']['title_length'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'Meta Title is not set'); break;
											case 1: $issues[] = array('type' => 'success', 'msg' => 'Meta Title Length ('.$op_report['title_length'].') is good!', 'prio' => '1', 'group' => 'Title'); break;
											case 2: $issues[] = array('type' => 'warning', 'msg' => 'Meta Title Length ('.$op_report['title_length'].') is too low', 'prio' => '1', 'group' => 'Title'); break;
											case 3: $issues[] = array('type' => 'warning', 'msg' => 'Meta Title Length ('.$op_report['title_length'].') is too high', 'prio' => '1', 'group' => 'Title'); break;
										}
										
										if ($op_report['issues']['title_length'] != 0) {
											switch ($op_report['issues']['title_prio1'])
											{
												case 0: $issues[] = array('type' => 'error', 'msg' => 'Meta Title has no Prio 1 Keyword', 'prio' => '1', 'group' => 'Title'); break;
												case 1: $issues[] = array('type' => 'success', 'msg' => 'Meta Title has a Prio 1 Keyword', 'prio' => '1', 'group' => 'Title'); break;
											}
										}
										
										//WSKO_Class_Template::render_panel(array('type' => 'issue', 'title' => __('Meta Description Issues', 'wsko'), 'col' => 'col-md-12 col-sm-12 col-xs-12'));
										switch ($op_report['issues']['desc_length'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'Meta Description is not set!', 'prio' => '1', 'group' => 'Description'); break;
											case 1: $issues[] = array('type' => 'success', 'msg' => 'Meta Description Length ('.$op_report['desc_length'].') is good!', 'prio' => '1', 'group' => 'Description'); break;
											case 2: $issues[] = array('type' => 'warning', 'msg' => 'Meta Description Length ('.$op_report['desc_length'].') is too low', 'prio' => '1', 'group' => 'Description'); break;
											case 3: $issues[] = array('type' => 'warning', 'msg' => 'Meta Description Length ('.$op_report['desc_length'].') is too high', 'prio' => '1', 'group' => 'Description'); break;
										}
										
										if ($op_report['issues']['desc_length'] != 0) {
											switch ($op_report['issues']['desc_prio1'])
											{
												case 0: $issues[] = array('type' => 'error', 'msg' => 'Meta Description has no Prio 1 Keyword!', 'prio' => '1', 'group' => 'Description'); break;
												case 1: $issues[] = array('type' => 'success', 'msg' => 'Meta Description has a Prio 1 Keyword!', 'prio' => '1', 'group' => 'Description'); break;
											}
										}	
										
										//WSKO_Class_Template::render_panel(array('type' => 'issue', 'title' => __('Content Issues', 'wsko'), 'col' => 'col-md-12 col-sm-12 col-xs-12'));
										switch ($op_report['issues']['word_count'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'There is no Content!', 'prio' => '1', 'group' => 'Content Length'); break;
											case 1: $issues[] = array('type' => 'success', 'msg' => 'Content length ('.$op_report['word_count'].') is good!', 'prio' => '1', 'group' => 'Content Length'); break;
											case 2: $issues[] = array('type' => 'error', 'msg' => 'Content length ('.$op_report['word_count'].') is under 50', 'prio' => '1', 'group' => 'Content Length'); break;
											case 3: $issues[] = array('type' => 'warning', 'msg' => 'Content length ('.$op_report['word_count'].') is under 100', 'prio' => '1', 'group' => 'Content Length'); break;
											case 4: $issues[] = array('type' => 'warning', 'msg' => 'Content length ('.$op_report['word_count'].') is under 200', 'prio' => '1', 'group' => 'Content Length'); break;
											case 5: $issues[] = array('type' => 'warning', 'msg' => 'Content length ('.$op_report['word_count'].') is under 400', 'prio' => '1', 'group' => 'Content Length'); break;
										}
										switch ($op_report['issues']['heading_h1_count'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'H1 not set', 'prio' => '1', 'group' => 'Headings'); break;
											//case 1: $issues[] = array('type' => 'success', 'msg' => 'H1 found!'); break;
											case 2: $issues[] = array('type' => 'error', 'msg' => 'There is more than one H1 used! ('.$op_report['tags']['h1'].')!', 'prio' => '1', 'group' => 'Headings', 'prio' => '1', 'group' => 'Headings'); break;
										}
										if ($op_report['issues']['heading_h1_count'] > 0) {
											switch ($op_report['issues']['heading_h1_prio1'])
											{
													case 0: $issues[] = array('type' => 'error', 'msg' => 'H1 has no Prio 1 Keyword!', 'prio' => '1', 'group' => 'Headings'); break;
													//case 1: $issues[] = array('type' => 'success', 'msg' => 'H1 has a Prio 1 Keyword!'); break;
											}
											if ($op_report['issues']['heading_h1_prio1'] != 0) {
												switch ($op_report['issues']['heading_h1_prio1_count'])
												{
													case 0: $issues[] = array('type' => 'warning', 'msg' => 'H1 does not have all Prio 1 Keywords!', 'prio' => '1', 'group' => 'Headings'); break;
													case 1: $issues[] = array('type' => 'success', 'msg' => 'H1 has all Prio 1 Keywords!', 'prio' => '1', 'group' => 'Headings'); break;
												}
											}	
										}
										if ($op_report['word_count'] > 250 && $op_report['word_count'] < 500)
										{
											switch ($op_report['issues']['heading_h2_250'])
											{
												case 0: $issues[] = array('type' => 'warning', 'msg' => 'H2 Count too low', 'prio' => '2', 'group' => 'Headings'); break;
												case 1: $issues[] = array('type' => 'success', 'msg' => 'H2 for over 250 words supplied', 'prio' => '2', 'group' => 'Headings'); break;
											}
										}
										if ($op_report['word_count'] > 500)
										{
											switch ($op_report['issues']['heading_h2_500'])
											{
												case 0: $issues[] = array('type' => 'warning', 'msg' => 'H2 Count too low', 'prio' => '2', 'group' => 'Headings'); break;
												case 1: $issues[] = array('type' => 'success', 'msg' => 'H2 for over 500 words supplied', 'prio' => '2', 'group' => 'Headings'); break;
											}
										}
										switch ($op_report['issues']['heading_h2h3_count'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'No H2/H3 found', 'prio' => '2', 'group' => 'Headings'); break;
											case 1: $issues[] = array('type' => 'success', 'msg' => 'H2/H3 found', 'prio' => '2', 'group' => 'Headings'); break;
										}
										
										//WSKO_Class_Template::render_panel(array('type' => 'issue', 'title' => __('Permalinks', 'wsko'), 'col' => 'col-md-12 col-sm-12 col-xs-12'));
										switch ($op_report['issues']['url_length'])
										{
											case 0: $issues[] = array('type' => 'warning', 'msg' => 'URL Length ('.$op_report['url_length'].') is too high', 'prio' => '3', 'group' => 'URL'); break;
											case 1: $issues[] = array('type' => 'success', 'msg' => 'URL Length ('.$op_report['url_length'].') is good', 'prio' => '3', 'group' => 'URL'); break;
										}
										switch ($op_report['issues']['url_prio1'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'URL has no Prio 1 Keyword', 'prio' => '3', 'group' => 'URL'); break;
											case 1: $issues[] = array('type' => 'success', 'msg' => 'URL has a Prio 1 Keyword', 'prio' => '3', 'group' => 'URL'); break;
										}
										
										//WSKO_Class_Template::render_panel(array('type' => 'issue', 'title' => __('Images', 'wsko'), 'col' => 'col-md-12 col-sm-12 col-xs-12'));
										switch ($op_report['issues']['media'])
										{
											case 0: $issues[] = array('type' => 'error', 'msg' => 'No Media Elements found', 'prio' => '3', 'group' => 'Media'); break;
											case 1: $issues[] = array('type' => 'success', 'msg' => 'Media Elements found', 'prio' => '3', 'group' => 'Media'); break;
										}
										
										if ($op_report['issues']['media'] != 0) {
											switch ($op_report['issues']['media_missing_alt'])
											{
												case 0: $issues[] = array('type' => 'error', 'msg' => 'Some of your pictures are missing alt-attributes', 'prio' => '3', 'group' => 'Media'); break;
												case 1: $issues[] = array('type' => 'success', 'msg' => 'All pictues have alt-tags', 'prio' => '3', 'group' => 'Media'); break;
											}
										}
										
										/*
										//WSKO_Class_Template::render_panel(array('type' => 'issue', 'title' => __('Keyword Density Issues', 'wsko'), 'col' => 'col-md-12 col-sm-12 col-xs-12'));
										foreach ($op_report['issues']['keyword_density'] as $pk => $iss)
										{
											switch ($iss['type'])
											{
												case 0: $issues[] = array('type' => 'error', 'msg' => 'Keyword density ('.$iss['density'].'%) of "'.$pk.'" is zero', 'prio' => '1', 'group' => 'Keywords'); break;
												case 1: $issues[] = array('type' => 'success', 'msg' => 'Keyword density ('.$iss['density'].'%) of "'.$pk.'" is good!', 'prio' => '1', 'group' => 'Keywords'); break;
												case 2: $issues[] = array('type' => 'warning', 'msg' => 'Keyword density ('.$iss['density'].'%) of "'.$pk.'" is too low', 'prio' => '1', 'group' => 'Keywords'); break;
												case 3: $issues[] = array('type' => 'warning', 'msg' => 'Keyword density ('.$iss['density'].'%) of "'.$pk.'" is too high', 'prio' => '1', 'group' => 'Keywords'); break;
											}
										}
										*/
										
										foreach($issues as $iss)
										{
											$iss['show_support'] = false;
											if ($iss['type'] == 'error')
												WSKO_Class_Template::render_notification($iss['type'], $iss);
										}
										foreach($issues as $iss)
										{
											$iss['show_support'] = false;
											if ($iss['type'] == 'warning')
												WSKO_Class_Template::render_notification($iss['type'], $iss);
										}
										foreach($issues as $iss)
										{
											$iss['show_support'] = false;
											if ($iss['type'] == 'success')
												WSKO_Class_Template::render_notification($iss['type'], $iss);
										}
									}
									else
									{
										WSKO_Class_Template::render_notification('error', array('msg' => 'Onpage Report could not be generated!'));
									}
									?>	
									</div>
								</div>	
						  </div>
						  <div id="wsko_metas_<?=$uniqid?>" class="wsko-tab <?=$open_tab === 'metas' ?  'wsko-tab-active' : ''?>">
							<?php 
							if (WSKO_Class_Onpage::seo_plugins_disabled())
							{
								WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('meta_view' => 'metas', 'post_id' => $post_id, 'type' => 'post_id'));
							}	
							else
							{
								WSKO_Class_Template::render_template('admin/templates/template-seo-plugins-disable.php', array());
							} ?>
						  </div>
						  <div id="wsko_snippets_<?=$uniqid?>" class="wsko-tab <?=$open_tab === 'social' ?  'wsko-tab-active' : ''?>">
							<?php 
							if (WSKO_Class_Onpage::seo_plugins_disabled())
							{
								WSKO_Class_Template::render_template('admin/templates/template-metas-view.php', array('meta_view' => 'social', 'post_id' => $post_id, 'type' => 'post_id'));
							}	
							else
							{
								WSKO_Class_Template::render_template('admin/templates/template-seo-plugins-disable.php', array());
							} ?>
						  </div>
						  <div id="wsko_keywords_<?=$uniqid?>" class="wsko-tab <?=$open_tab === 'keywords' ?  'wsko-tab-active' : ''?>">
							<?php 
							if (WSKO_Class_Search::get_se_token())
							{
								if (!WSKO_Class_Core::get_option('search_query_first_run'))
									WSKO_Class_Template::render_notification('warning', array('msg' => 'Your search data is being updated. Keywords are available after the first report is finished.'));
								else if ($keywords)
									WSKO_Class_Template::render_table(array('Keyword', 'Position', 'Clicks', 'Impressions', 'CTR', ''), $table_keywords, array('order' => array('col' => '2', 'dir' => 'desc'))); 
								else
									WSKO_Class_Template::render_template('admin/templates/template-no-data.php', array());
							}
							else
								WSKO_Class_Template::render_template('admin/templates/template-no-api.php', array());
							?>
						  </div>
						  <div id="wsko_technical_<?=$uniqid?>" class="wsko-tab <?=$open_tab === 'technical' ?  'wsko-tab-active' : ''?>">
							<div class="wsko-co-technical-wrapper">
								<div class="form-group wsko-row">
									<div class="wsko-col-sm-3 wsko-col-xs-12">
										<p>Canonical Tag</p>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">
										<select class="wsko-co-canonical-type wsko-form-control wsko-mb10">
											<option value="0">No Canonical</option>
											<option value="1" <?=((isset($t_seo_data['canonical']['type']) && $t_seo_data['canonical']['type'] == '1') || WSKO_Class_Core::get_setting('auto_canonical')) ? 'selected' :''?>>Self (<?=get_permalink($post_id)?>)</option>
											<option value="2" <?=isset($t_seo_data['canonical']['type']) && ($t_seo_data['canonical']['type'] == '2' || $t_seo_data['canonical']['type'] == '3') ? 'selected' :''?>>Post ID or URL</option>
											<!--option value="3" <?=isset($t_seo_data['canonical']['type']) && $t_seo_data['canonical']['type'] == '3' ? 'selected' :''?>>Relative URL (<?=home_url('/relative/url/')?>)</option-->
										</select>
										<input type="text" class="wsko-co-canonical-arg wsko-form-control wsko-mb10" placeholder="https://absolute.url/ or /relative/link/ or post-id" <?=!isset($t_seo_data['canonical']) || !$t_seo_data['canonical'] || $t_seo_data['canonical']['type'] == '1' ? 'style="display:none;"' : ' value="'.$t_seo_data['canonical']['arg'].'"'?>>
									</div>	
								</div>	
								<div class="form-group wsko-row">
									<div class="wsko-col-sm-3 wsko-col-xs-12">
										<p>Sitemap Generation</p>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">
										<label><input type="checkbox" class="wsko-co-sitemap-exclude wsko-form-control wsko-mb10" <?=$sitemap_excluded ? 'checked' : ''?>> Exclude from Sitemap</label>
									</div>	
								</div>	
								<div class="form-group wsko-row">
									<div class="wsko-col-sm-3 wsko-col-xs-12">						
										<p>Redirect To</p>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">	
										<p><label><input class="wsko-co-redirect-activate wsko-form-control" type="checkbox" <?=isset($t_seo_data['redirect']) && $t_seo_data['redirect'] ? 'checked' : ''?>> Activate Redirect</label></p>
										<input class="wsko-co-redirect-to wsko-form-control wsko-mb10" type="text" placeholder="/relative/link/ or https://external-domain.com/path " value="<?=isset($t_seo_data['redirect']['to']) ? $t_seo_data['redirect']['to'] : ''?>">
										<select class="wsko-co-redirect-type wsko-form-control wsko-mb10">
											<option value="1" <?=isset($t_seo_data['redirect']['type']) && $t_seo_data['redirect']['type'] == '1' ? 'selected' :''?>>301 (Permanent Redirect, SEO fiendly)</option>
											<option value="2" <?=isset($t_seo_data['redirect']['type']) && $t_seo_data['redirect']['type'] == '2' ? 'selected' :''?>>302 (Temporary Redirect)</option>
										</select>
									</div>	
								</div>								
								<div class="wsko-row">
									<div class="wsko-col-sm-3 wsko-col-xs-12">
										
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">
										<a href="#" class="wsko-co-save-technical button wsko-mb10" data-post="<?=$post_id?>" data-nonce="<?=wp_create_nonce('wsko_co_save_technicals')?>"><?=__('Save', 'wsko');?></a>
									</div>
								</div>
								
								<div class="wsko-border wsko-mb10 wsko-mt10"></div>
								
								<div class="form-group wsko-row wsko-co-redirect-from-wrapper">
									<div class="wsko-col-sm-3 wsko-col-xs-12">
										<p>Redirects from this post</p>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">									
										<div class="wsko-row">
											<div class="wsko-col-sm-12 wsko-col-xs-12">	
												<?php WSKO_Class_Template::render_ajax_beacon('wsko_check_redirect', array('post' => array('url' => $link, 'status_check' => false))); ?>
											</div>
										</div>
									</div>
								</div>			
								
								<div class="wsko-border wsko-mb10 wsko-mt10"></div>
								
								<div class="form-group wsko-row">
									<div class="wsko-col-sm-3 wsko-col-xs-12">
										<p>Auto Post Redirects</p>
										<small class="wsko-text-off">View redirects from old post urls' to the current url. Please check, if this option is activated in your <?php WSKO_Class_Template::render_page_link(WSKO_Controller_Onpage::get_instance(), "links",  'Permalink Settings', false, false)?></small>
									</div>
									<div class="wsko-col-sm-9 wsko-col-xs-12">	
										<?php $redirects = WSKO_Class_Onpage::get_auto_redirects('post_id');
										if ($redirects && isset($redirects[$post_id]))
										{
											$redirects = $redirects[$post_id];
											if ($redirects)
											{
												$reds = array();
												foreach($redirects as $key => $red)
												{
													$reds[] = '<i>'.$red.'</i> '.(current_user_can('manage_options') ? WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'remove_auto_redirect', array('type' => 'post_id', 'arg' => $post_id, 'key' => $key), array(), true) : '');
												}
												WSKO_Class_Template::render_notification('warning', array('msg' => 'This post has redirects from old links:', 'list' => $reds, 'notif-class' => 'wsko-post-redirects'));
											} 
											else {
												echo 'No redirects available';
											}
										}
										else {
											echo 'No redirects available';
										}										
										?>
									</div>
								</div>			
							</div>
						</div>
						  <?php if (!$widget) { ?>
							  <div id="wsko_content_<?=$uniqid?>" class="wsko-tab <?=$open_tab === 'content' ?  'wsko-tab-active' : ''?>">
								<?php
								?>
								<div class="wsko-co-update-content">
									<?=WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Post Title', 'value' => $title, 'class' => 'wsko-co-content-field-title wsko-form-control', 'nonce' => 'wsko_co_save_content'));?>
									<?=WSKO_Class_Template::render_form(array('type' => 'input', 'title' => 'Post Slug', 'value' => $slug, 'class' => 'wsko-co-content-field-slug wsko-form-control', 'nonce' => 'wsko_co_save_content'));?>
									<?=WSKO_Class_Template::render_form(array('type' => 'textarea', 'title' => 'Post Content', 'value' => $content, 'class' => 'wsko-co-content-field-content wsko-form-control', 'nonce' => 'wsko_co_save_content', 'rows' => '15'));?>
									<?=WSKO_Class_Template::render_form(array('type' => 'submit', 'title' => '','value' => $content, 'class' => 'wsko-co-save-content', 'nonce' => 'wsko_co_save_content', 'data-post' => $post_id));?>
									<?php /*<a href="#" class="wsko-co-save-content" data-post="<?=$post_id?>" data-nonce="<?=wp_create_nonce('wsko_co_save_content')?>">Save</a> */?>
								</div>
							  </div>
						  <?php } ?>
						</div>
					</div>
					<div class="wsko-col-sm-4 wsko-col-xs-12">
						<?php if ($widget) { ?>
							<div class="wsko-co-progress-wrapper wsko-align-center wsko-mb10" style="position:relative;">
								<?php
								WSKO_Class_Template::render_radial_progress('success', __('Content Score' ,'wsko'), array('val' => $op_report ? $op_report['onpage_score'] : '-', 'class' => 'wsko-inline-block hidden-xs' ));
								?>
								<!--div class="wsko-premium-wrapper">
									<a href="https://www.bavoko.tools/" target="_blank" class="wsko-pro-link">PRO FEATURE</a>
									<?php
									//WSKO_Class_Template::render_radial_progress('success', __('Technical' ,'wsko'), array('val' => '41', 'class' => 'blurred wsko-inline-block hidden-xs' ));
									//WSKO_Class_Template::render_radial_progress('success', __('Page Score' ,'wsko'), array('val' => '70', 'class' => 'blurred wsko-inline-block hidden-xs' ));
									?>	
								</div-->
							</div>
						<?php } ?>	
						<div class="wsko-keywords-long-view">
							<?php WSKO_Class_Template::render_template('admin/templates/template-co-sidebar-keywords.php', array('post_id' => $post_id, 'priority_keywords' => $priority_keywords));  ?>
						</div>
					</div>
				</div>	
				<div class="wsko-keywords-short-view">
							<?php WSKO_Class_Template::render_template('admin/templates/template-co-sidebar-keywords.php', array('post_id' => $post_id, 'priority_keywords' => $priority_keywords));  ?>
				</div>
				<div class="clearfix">
				</div>
			</div>	
		<?php
		if ($widget)		
		{
			?>
				</div>
				<div class="wsko-co-control-wrapper wsko-align-center">
					<div class="wsko-rezisable-wrapper-thumb text-off" style="margin-right: 15px;">
						<i class="fa fa-expand fa-fw"></i> Drag
					</div>
					<a href="#" class="wsko-resizable-wrapper-quick-down dark text-off">
						<i class="fa fa-arrows-alt fa-fw"></i> Expand
					</a>
					<a href="#" class="wsko-resizable-wrapper-quick-up dark text-off">
						<i class="fa fa-times"></i> Collapse
					</a>					
				</div>	
			</div><?php
		} ?>
		<?php /* <a class="wsko-expand-content-optimizer-link wsko-co-expand-icon dark text-off" href="#"><span class="wsko-expand-content-optimizer"><i class="fa fa-angle-down fa-fw fa-2x"></i></span></a>
		<p class="wsko-align-center" style="margin-bottom:0px;"><a class="wsko-expand-content-optimizer-link wsko-co-expand-icon-close dark text-off" href="#"><span class="wsko-expand-content-optimizer"><i class="fa fa-angle-up fa-fw fa-2x"></i></span></a></p> */ ?>
		<!--div class="wsko-co-notifications-overlay" style="position:absolute;bottom:10px;right:10px;"></div-->
	</div>
	<?php
}
else
{
	?>Post ID not set!<?php
}

?>