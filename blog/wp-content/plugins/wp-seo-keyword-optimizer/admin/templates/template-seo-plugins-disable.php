<?php
if (!defined('ABSPATH'))
	exit;

WSKO_Class_Template::render_notification('error', array('msg' => 'You need to disable your SEO plugins to gain access to the Onpage section. The following plugins have been detected:', 'list' => WSKO_Class_Import::get_active_seo_plugins(), 'subnote' => 'We are sorry for the inconvenience, but this prevents unexpected changes in your SEO rankings.'));
?>