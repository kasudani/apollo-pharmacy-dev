jQuery(document).ready(function($) {
	$(document).on('wsko_init_page', function(e) {
		wsko_set_kb_carousel();
	});
	
	function wsko_set_kb_carousel()
	{
		$('.wsko-carousel:not(.wsko-init)').addClass('wsko-init').each(function(index){
			var $this = $(this),
			id = uniqueID('wsko_carousel_'),
			$ca = $('<div id="'+id+'" class="carousel slide" data-ride="carousel"><ol class="carousel-indicators"></ol><div class="carousel-inner"></div><a class="carousel-control" href="#'+id+'" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Previous</span></a><a class="carousel-control" href="#'+id+'" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span></a></div>'),
			$ind = $ca.find('.carousel-indicators'),
			$list = $ca.find('.carousel-inner'),
			curr = 0;
			$this.find('.wsko-carousel-item').each(function(index){
				var img = $(this).find('img').attr('src'),
				title = $(this).find('p.title').text(),
				desc = $(this).find('p.desc').text();
				$ind.append('<li data-target="#'+id+'" data-slide-to="'+curr+'" '+(curr==0?'class="active"':'')+'></li>');
				$list.append('<div class="item '+(curr==0?'active':'')+'"><img src="'+img+'" alt="'+title+'"><div class="carousel-caption"><h3>'+title+'</h3><p>'+desc+'</p></div></div>');
				curr++;
			});
			$this.replaceWith($ca[0].outerHTML).carousel();
		});
	};
	var idCounter = 0;
	function uniqueID(prefix) {
		var id = '' + ++idCounter;
		return prefix ? prefix + id : id;
	};
	wsko_set_kb_carousel();
});