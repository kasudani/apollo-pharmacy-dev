jQuery(document).ready(function($){
	$('.wsko-import-backup:not(.wsko-init)').addClass('wsko-init').click(function(event){
		event.preventDefault();
		var $this = $(this);
		
		jQuery.wsko_post_file($this.closest('.wsko-import-backup-wrapper').find('.wsko-import-backup-file')[0].files[0], {action: 'wsko_import_configuration_backup', nonce: $this.data('nonce')}, false, function(res){
			
		}, function(){
			
		}, $this);
	});
});
