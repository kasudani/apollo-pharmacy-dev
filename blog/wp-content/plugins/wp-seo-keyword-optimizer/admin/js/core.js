jQuery(document).ready(function($)
{
	var co_parent_view = false
	jQuery.wsko_init_core = function ()
	{
		//General Controls
		$('.wsko-ajax-button:not(.wsko-ajax-init)').addClass('wsko-ajax-init').click(function(event){
			event.preventDefault();
			var $this = $(this),
			objData = $this.data(),
			data = {action: $this.data('action'), nonce: $this.data('nonce')};
			$.each(objData, function(k, v) {
				if (k.startsWith('wskoPost'))
					data[k.substr(8).toLowerCase()] = v;
			});
			if (!$this.data('alert') || confirm($this.data('alert')))
				jQuery.wsko_post_element(data, function(res){ if (res.success && $this.data('remove')) { $($this.data('remove')).fadeOut('fast', function(){ $(this).remove(); }); } }, false, $this, $this.data('no-reload') ? false : true);
		});
		
		//Content Optimizer
		$('.wsko-content-optimizer-link:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $link = $(this),
			$modal = $('#wsko_content_optimizer_modal').addClass('wsko-modal-active'),
			$content = $modal.find('.wsko-modal-content').show().html('');
			$modal.find('.wsko-modal-multi-container').hide();
			if (co_parent_view)
				$modal.find('.wsko-modal-multi-container-bar').show();
			else
				$modal.find('.wsko-modal-multi-container-bar').hide();
			$('#wsko_content_optimizer_modal .wsko-modal-loader').show();
			jQuery.wsko_post_element({action: 'wsko_get_content_optimizer', post: $link.data('post'), open_tab: $link.data('opentab'), nonce: wsko_data.content_optimizer_nonce}, 
				function(res){
					$('#wsko_content_optimizer_modal .wsko-modal-loader').hide();
					if (res.success)
					{
						$content.html(res.view);
						return true;
					}
					else
					{
						$content.html("Content Optimizer could not be loaded. Please try again.");
					}
				},
				function()
				{
					$content.html("A Server Error occured. Please try again.");
					$('#wsko_content_optimizer_modal .wsko-modal-loader').hide();
				}, false, false);
		});
		
		$('.wsko-content-optimizer-link').each(function(index){
			$(this).prependTo($(this).closest('.page-title'));
		});
		
		$('#wsko_content_optimizer_modal .wsko-modal-box:not(.wsko-init)').addClass('wsko-init').click(function(event){ event.stopPropagation(); });
		$('#wsko_content_optimizer_modal .wsko-modal-back-to-multi:not(.wsko-init)').addClass('wsko-init').click(function(event){ 
			event.preventDefault();
			var $modal = $('#wsko_content_optimizer_modal');
			$modal.find('.wsko-modal-content').hide();
			$modal.find('.wsko-modal-multi-container-bar').hide();
			$modal.find('.wsko-modal-multi-container').fadeIn();
		});
		$('#wsko_content_optimizer_modal:not(.wsko-init)').addClass('wsko-init').click(function(event){
			var $modal = $('#wsko_content_optimizer_modal').removeClass('wsko-modal-active');
			$modal.find('.wsko-modal-multi-container').hide();
			co_parent_view = false;
		});
		$('#wsko_content_optimizer_modal .wsko-modal-close:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $modal = $('#wsko_content_optimizer_modal').removeClass('wsko-modal-active');
			$modal.find('.wsko-modal-multi-container').hide();
			co_parent_view = false;
		});
		$('.wsko-content-optimizer-multi-link:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $link = $(this),
			$modal = $('#wsko_content_optimizer_modal').addClass('wsko-modal-active'),
			$multi_content = $modal.find('.wsko-modal-multi-container').show().html('');
			$modal.find('.wsko-modal-multi-container-bar').hide();
			$modal.find('.wsko-modal-loader').hide();
			$modal.find('.wsko-modal-content').hide();
			co_parent_view = true;
			var posts = $link.data('posts') ? $link.data('posts')/*JSON.parse($link.data('posts'))*/ : {};
			
			$multi_content.append('<p class="wsko-content-optimizer-multi-link-title"><label>'+$link.data('title')+'</label></p>');
			
			$.each(posts, function(k, val){
				$multi_content.append('<a href="#" class="wsko-content-optimizer-link wsko-multi-link dark" data-post="'+k+'">'+val.title+'<br/><small class="wsko-content-optimizer-multi-link-url text-off">'+val.url+'</small></a>');
			});
			jQuery.wsko_init_core();
		});
		
		//Keyword Monitoring
		$('.wsko-set-monitoring-keyword:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $this = $(this),
			set = $this.data('set') && $this.data('set') != 'false';
			jQuery.wsko_post_element({action: set ? 'wsko_add_monitoring_keyword' : 'wsko_remove_monitoring_keyword', keyword: $this.data('keyword'), nonce: set ? wsko_data.add_monitoring_keyword_nonce : wsko_data.remove_monitoring_keyword_nonce}, function(res){
					if (res.success)
					{
						$this.data('set', !set);
						if (set) {
							$this.find('i').removeClass('fa-star-o').addClass('fa-star');
							$this.attr('title', "Remove from 'Monitored Keywords'").attr('data-original-title', "Remove from 'Monitored Keywords'");
						} else {
							$this.find('i').removeClass('fa-star').addClass('fa-star-o');
							$this.attr('title', "Add to 'Monitored Keywords'").attr('data-original-title', "Add to 'Monitored Keywords'");
						}
						return true;
					}
				}, false, $this, false);
		});
		
		//Progress Bars
		$('.wsko-circle-progress:not(.wsko_init)').addClass('wsko_init').circliful();
		
		//Datatables
		$('.wsko-tables:not(.wsko-init,.wsko-ajax-tables)').addClass('wsko-init').each(function(index){
			var $this = $(this),
			order = false;
			if ($this.data('def-order') != undefined)
			{
				order = [[parseInt($this.data('def-order')), $this.data('def-orderdir')]];
			}
			$this.DataTable({
				"order": order ? order : [[ 0, "desc" ]],
				"pageLength": 25,
				"drawCallback": function( settings ) {
					jQuery.wsko_init_core();
				}
			});
		});
		if (jQuery.wsko_init_admin)
		{
			//Init Admin
			jQuery.wsko_init_admin();
		}
		
		//Notifications
		$('.wsko-setup-wrapper .wsko-setup-notifications-wrapper:not(.wsko-notif-init)').addClass('wsko-notif-init').click(function(event){ $(this).fadeOut(); });
		$('#wsko_content_optimizer_modal.wsko-modal-active .wsko-co-notifications-overlay:not(.wsko-notif-init)').addClass('wsko-notif-init').click(function(event){ $(this).fadeOut(); });
		$('#wsko_admin_view_ajax_notification:not(.wsko-notif-init)').addClass('wsko-notif-init').click(function(event){ $(this).fadeOut(); });
		$('.wsko-content-optimizer.wsko-co-widget .wsko-co-notifications-overlay:not(.wsko-notif-init)').addClass('wsko-notif-init').click(function(event){ $(this).fadeOut(); });
		
		//Code Highlighting
		$('.wsko-previewable-textarea:not(.wsko-init)').addClass('wsko-init').each(function(index) {
			var $this = $(this),
			highlights = $this.data('highlights'),
			$area = $this.find('textarea'),
			$preview = $this.find('.wsko-textarea-preview');
			console.log(highlights);
			if (!highlights)
				highlights = [];
			//highlights.push({color:'red', regex: /(&lt;\?)(.*?)(\?&gt;)/g, type: 2});
			//highlights.push({color:'red', regex: /(test)/g, type: 1});
			//highlights.push({color:'red', regex: /(<b>)(.*?)(<\/b>)/g, type: 2});
			$area.on('input propertychanged',function(){
				var preview_html = $(this).val();
				//preview_html = wsko_replace_html(preview_html);
				preview_html = preview_html.replace(/</g, '&lt;');
				preview_html = preview_html.replace(/>/g, '&gt;');
				preview_html = preview_html.replace(/\\r/g, '<br/>');
				preview_html = preview_html.replace(/\\n/g, '<br/>');
				$.each(highlights, function(k, v){
					switch(v.type)
					{
						case 1:
						preview_html = preview_html.replace(new RegExp(v.regex, 'gi'), function(match, p1, offest, string) { return '<p style="color:'+v.color+'">'+p1+'</p>'; });
						break;
						case 2:
						preview_html = preview_html.replace(new RegExp(v.regex, 'gi'), function(match, p1, p2, p3, offest, string) { return '<p style="color:'+v.color+'">'+p1+'</p>'+p2+'<p style="color:'+v.color+'">'+p3+'</p>'; });
						break;
						case 3:
						preview_html = preview_html.replace(new RegExp(v.regex, 'gi'), function(match, p1, p2, p3, p4, offest, string) { return '<p style="color:'+v.color+'">'+(p1?p1:'')+'</p>'+(p2?p2:'')+'<p style="color:'+v.color+'">'+(p3?p3:'')+(p4?p4:'')+'</p>'; });
						break;
					}
				});
				//console.log(preview_html);
				$preview.html(preview_html);
				$preview.scrollTop($(this).scrollTop());
			}).focus(function(){
				//$preview.hide();
			}).focusout(function(){
				$preview.show();
			}).on('scroll', function () {
				$preview.scrollTop($(this).scrollTop());
			});
		});
		
		//Knowledge Base
		$('.wsko-open-knowledge-base-article:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $link = $(this),
			$modal = $('#wsko_knowledge_base_modal').addClass('wsko-modal-active'),
			$content = $modal.find('.wsko-modal-content').html('');
			$('#wsko_knowledge_base_modal .wsko-modal-loader').show();
			jQuery.wsko_post_element({action: 'wsko_get_knowledge_base_article', article: $link.data('article'), nonce: wsko_data.knowledge_base_nonce}, 
				function(res){
					$('#wsko_knowledge_base_modal .wsko-modal-loader').hide();
					if (res.success)
					{
						$content.html(res.view);
						return true;
					}
					else
					{
						$content.html("Article could not be loaded");
					}
				},
				function()
				{
					$content.html("A Server Error occured. Please try again.");
					$('#wsko_knowledge_base_modal .wsko-modal-loader').hide();
				}, false, false);
		});
		$('#wsko_knowledge_base_modal .wsko-modal-box:not(.wsko-init)').addClass('wsko-init').click(function(event){ event.stopPropagation(); });
		$('#wsko_knowledge_base_modal:not(.wsko-init)').addClass('wsko-init').click(function(event){
			var $modal = $('#wsko_knowledge_base_modal').removeClass('wsko-modal-active');
		});
		$('#wsko_knowledge_base_modal .wsko-modal-close:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $modal = $('#wsko_knowledge_base_modal').removeClass('wsko-modal-active');
		});
		var timeout;
		var kb_instant_search = false;
		$('.wsko-search-knowledge-base:not(.wsko-init)').addClass('wsko-init').on('change keyup', function(event){
			var $this = $(this),
			$wrap = $this.closest('.wsko-search-knowledge-base-wrapper');//.find('.wsko-search-knowledge-base-list');
			var timeout = $(this).data('search_timeout');
			if (timeout)
				clearInterval(timeout);
			var cats = [];
			$wrap.find('.wsko-search-knowledge-base-cat').each(function(index){
				if ($(this).is(':checked'))
				{
					cats.push($(this).val());
				}
			});
			var $overlay = $wrap.closest('.wsko-knowledge-base-wrapper').find('.wsko-kb-loading-overlay').show();
			$this.data('search_timeout', setTimeout(function(){
				jQuery.wsko_post_element({action: 'wsko_search_knowledge_base', search:$this.val(), cats: cats, page: $wrap.find('.wsko-search-knowledge-base-page').val(), nonce:$this.data('nonce')},
					function(res){
						if (res.success)
						{
							$wrap.replaceWith($(res.view));
						}
						$overlay.hide();
						return true;
					}, function(){
							$overlay.hide();
						return true;
					}, false, false);
			}, kb_instant_search ? 1 : 1000));
		});
		$('.wsko-search-knowledge-base-cat:not(.wsko-init)').addClass('wsko-init').change(function(){
			var $this = $(this);
			if ($this.is(':checked'))
				$this.closest('label').addClass('wsko-kb-cat-selected');
			else
				$this.closest('label').removeClass('wsko-kb-cat-selected');
			kb_instant_search = true;
			$this.closest('.wsko-search-knowledge-base-wrapper').find('.wsko-search-knowledge-base').change();
			kb_instant_search = false;
		});
		$('.wsko-search-knowledge-base-page-set:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this),
			$wrap = $this.closest('.wsko-search-knowledge-base-wrapper'),
			$field = $wrap.find('.wsko-search-knowledge-base-page');
			if ($field.val() != $this.data('page'))
				$field.val($this.data('page'));
			kb_instant_search = true;
			$wrap.find('.wsko-search-knowledge-base').change();
			kb_instant_search = false;
		});
		$('.wsko-kb-rate-article:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this);
			if ($this.data('good') && $this.data('good') != 'false')
				rating = 'good';
			else
				rating = 'bad';
			jQuery.wsko_post_element({action: 'wsko_rate_knowledge_base_article', post: $this.data('post'), type: rating, nonce:$this.data('nonce')},
				function(res){
					if (res.success)
					{
						$this.closest('.kb-helpful').html('Thank you!');
					}
					return true;
				}, function(){
					return true;
				}, $this.closest('.kb-helpful'), false);
		});
		
		//Resizable Wrapper
		$('.wsko-resizable-wrapper:not(.wsko-init)').addClass('wsko-init').each(function(index) {
			var $this = $(this);
			$this.find('.wsko-rezisable-wrapper-thumb').mousedown(function(){
				jQuery.wsko_move_resizable_wrapper = [$this,$this.offset().top];
			});
			$this.find('.wsko-resizable-wrapper-quick-up').click(function(index){
				event.preventDefault();
				event.stopPropagation();
				$this.css("height", $this.data('height')); //$this.css("min-height"));
				
				if (!$('.wsko-co-widget').hasClass('wsko-short-view-active'))
					$('.wsko-co-widget').toggleClass('wsko-short-view-active');
					
				//$('	.wsko-co-widget .wsko-nav li a').removeClass('wsko-nav-link-active');
				//$('	.wsko-co-widget .wsko-tab').removeClass('wsko-tab-active');
				//$('	.wsko-co-widget .wsko-nav li:first-child a').addClass('wsko-nav-link-active');
				//$('	.wsko-co-widget .wsko-tab:first-child').addClass('wsko-tab-active');
				
				$(window).scrollTop($(window).scrollTop()-1); //dirty refresh
				$(window).scrollTop($(window).scrollTop()+1); //dirty refresh
			});
			$this.find('.wsko-resizable-wrapper-quick-down').click(function(index){
				event.preventDefault();
				event.stopPropagation();
				$this.css("height", $this.css("max-height"));
				
				if ($('.wsko-co-widget').hasClass('wsko-short-view-active'))
					$('.wsko-co-widget').toggleClass('wsko-short-view-active');
				
				$(window).scrollTop($(window).scrollTop()-1); //dirty refresh
				$(window).scrollTop($(window).scrollTop()+1); //dirty refresh
			});
		});
		
		//Do customs
		$(document).trigger("wsko_init_core");
	};
	
	$(document).mousedown(function(){
		
		if (jQuery.wsko_move_resizable_wrapper && jQuery.wsko_move_resizable_wrapper !== false)
		{
			return false;
		}
	}).mousemove(function(e){
		if (jQuery.wsko_move_resizable_wrapper && jQuery.wsko_move_resizable_wrapper !== false)
		{
			var new_height = e.pageY-jQuery.wsko_move_resizable_wrapper[1]+10;
			jQuery.wsko_move_resizable_wrapper[0].css("height", new_height).data('height', new_height);
		}
	}).mouseup(function(e){
		if (jQuery.wsko_move_resizable_wrapper && jQuery.wsko_move_resizable_wrapper !== false) 
		{
			$(window).scrollTop($(window).scrollTop()-1); //dirty refresh
			$(window).scrollTop($(window).scrollTop()+1); //dirty refresh
			jQuery.wsko_post_element({action: 'wsko_co_change_height', height:jQuery.wsko_move_resizable_wrapper[0].height(), nonce:jQuery.wsko_move_resizable_wrapper[0].data('nonce')},
				function(res){
					return true;
				}, function(){
					return true;
				}, false, false);
			jQuery.wsko_move_resizable_wrapper = false;
		}
	});
	//helpers
	function wsko_replace_html(string)
	{
		var buf = [];
		for (var i=string.length-1;i>=0;i--) {
			buf.unshift(['&#', string[i].charCodeAt(), ';'].join(''));
		}
		return buf.join('');
	}
	function wsko_add_element_ajax_result($el, success)
	{
		var $old = $el.find('.wsko-ajax-result');
		if ($old.length)
		{
			$old.each(function(index){ clearInterval($(this).data('timeout')); });
			$old.remove();
		}
		var $new = $('<i class="fa fa-'+(success?'check':'times')+' wsko-ajax-result"></i> ').prependTo($el);
		$new.data('timer', setTimeout(function(){
			$new.fadeOut();
		}, 3000));
	}
	
	//Notifications
	var notification_timeout_modal;
	var notification_timeout_admin;
	var notification_timeout_widget;
	var notification_timeout_setup;
	jQuery.wsko_notification = function(success, msg, title)
	{
		var $setup = $('.wsko-setup-wrapper .wsko-setup-notifications-wrapper');
		var $modal = $('#wsko_content_optimizer_modal.wsko-modal-active .wsko-co-notifications-overlay');
		var $admin = $('#wsko_admin_view_ajax_notification');
		var $widget = $('.wsko-content-optimizer.wsko-co-widget .wsko-co-notifications-overlay');
		if ($setup.length)
		{
			if (notification_timeout_setup)
			{
				clearTimeout(notification_timeout_setup);
				$setup.hide();
			}
		
			if (success)
				$setup.css('background-color', '#27ae60').css('z-index','1');
			else
				$setup.css('background-color', '#e74c3c').css('z-index','1');
			$setup.html(msg).fadeIn();
			$setup.prepend('<small class="text-off" style="float:right;cursor:pointer;"><i class="fa fa-times fa-fw"></i></small>');
			notification_timeout_setup = setTimeout(function(){ $setup.fadeOut('fast', function(){ $(this).css('z-index','-1'); }); }, 3000);
		}
		else if ($modal.length)
		{
			if (notification_timeout_modal)
			{
				clearTimeout(notification_timeout_modal);
				$modal.hide();
			}
		
			if (success)
				$modal.css('background-color', '#27ae60').css('z-index','1');
			else
				$modal.css('background-color', '#e74c3c').css('z-index','1');
			$modal.html(msg).fadeIn();
			$modal.prepend('<small class="text-off" style="float:right;cursor:pointer;"><i class="fa fa-times fa-fw"></i></small>');
			notification_timeout_modal = setTimeout(function(){ $modal.fadeOut('fast', function(){ $(this).css('z-index','-1'); }); }, 3000);
		}
		else if ($admin.length)
		{
			if (notification_timeout_admin)
			{
				clearTimeout(notification_timeout_admin);
				$admin.hide();
			}
		
			if (success)
				$admin.css('background-color', '#27ae60').css('z-index','1');
			else
				$admin.css('background-color', '#e74c3c').css('z-index','1');
			$admin.html(msg).fadeIn();
			$admin.prepend('<small class="text-off" style="float:right;cursor:pointer;"><i class="fa fa-times fa-fw"></i></small>');
			notification_timeout_admin = setTimeout(function(){ $admin.fadeOut('fast', function(){ $(this).css('z-index','-1'); }); }, 3000);
		}
		else if ($widget.length)
		{
			if (notification_timeout_widget)
			{
				clearTimeout(notification_timeout_widget);
				$widget.hide();
			}
		
			if (success)
				$widget.css('background-color', '#27ae60').css('z-index','1');
			else
				$widget.css('background-color', '#e74c3c').css('z-index','1');
			$widget.html(msg).fadeIn();
			$widget.prepend('<small class="text-off" style="float:right;cursor:pointer;"><i class="fa fa-times fa-fw"></i></small>');
			notification_timeout_widget = setTimeout(function(){ $widget.fadeOut('fast', function(){ $(this).css('z-index','-1'); }); }, 3000);
		}
	}
	
	//ajax
	if ($.wskoXhrPool == undefined)
	{
		$.wskoXhrPool = [];
		$.wskoXhrPool.abortAll = function() {
			$.each($.wskoXhrPool, function(i, jqXHR) {   //  cycle through list of recorded connection
				if (jqXHR)
					jqXHR.abort();  //  aborts connection
				$.wskoXhrPool.splice(i, 1); //  removes from list by index
			});
		}
	}
	jQuery.wsko_post = function(data, before, success, error)
	{
		$.ajax({
			url: wsko_data.ajaxurl,
			type: 'post',
			data: data,
			async: true,
			beforeSend: function(jqXHR)
			{
				$.wskoXhrPool.push(jqXHR);
				if (before)
					before();
			},
			success: function(res)
			{
				if (success)
					success(res);
				jQuery.wsko_init_core();
			},
			error: function()
			{
				if (error)
					error();
			},
			complete: function(jqXHR)
			{
				var i = $.wskoXhrPool.indexOf(jqXHR);   //  get index for current connection completed
				if (i > -1) $.wskoXhrPool.splice(i, 1); //  removes from list by index
			}
		});
	};
	jQuery.wsko_post_file = function(file, data, before, success, error, $btn)
	{
		var $loader;
		var formData = new FormData();
		formData.append('file', file);
		$.each(data, function(key, val){
			formData.append(key, val);
		});
		$.ajax({
			url: wsko_data.ajaxurl,
			type: 'post',
			data: formData,
			processData: false,
			contentType: false,
			beforeSend: function(jqXHR)
			{
				$loader = jQuery.wsko_set_element_ajax_loader($btn);
				$.wskoXhrPool.push(jqXHR);
				if (before)
					before();
			},
			success: function(res)
			{
				if ($loader)
					$loader.hide();
				if (res.success)
				{
					if ($btn && !($btn.is(':input')))
						wsko_add_element_ajax_result($btn, true);
					
					if (res.redirect)
						location.href = res.redirect;
					else
					{
						if (wsko_data.is_configured && jQuery.wsko_reload_lazy_page != undefined)
							jQuery.wsko_reload_lazy_page();
						else
							location.reload();
					}
				}
				else
				{
					if ($btn && !($btn.is(':input')))
						wsko_add_element_ajax_result($btn, false);
				}
				if (success)
					success(res);
				jQuery.wsko_init_core();
			},
			error: function()
			{
				if ($loader)
					$loader.hide();
				if ($btn && !($btn.is(':input')))
					wsko_add_element_ajax_result($btn, true);
				if (error)
					error();
			},
			complete: function(jqXHR)
			{
				var i = $.wskoXhrPool.indexOf(jqXHR);   //  get index for current connection completed
				if (i > -1) $.wskoXhrPool.splice(i, 1); //  removes from list by index
			}
		});
	};
	jQuery.wsko_post_element = function(data, succ, error, $btn, reload)
	{
		var $loader;
		jQuery.wsko_post(data, function(){
			$loader = jQuery.wsko_set_element_ajax_loader($btn);
		}, function(res){
			var succ_res = false;
			if(succ)
				succ_res = succ(res);
			if (!succ_res)
			{
				if (res.success)
				{
					if (res.msg)
						jQuery.wsko_notification(true, res.msg, "");
					else
						jQuery.wsko_notification(true, "Saved", "");
				}
				else
				{
					if (res.msg)
						jQuery.wsko_notification(false, res.msg, "");
					else
						jQuery.wsko_notification(false, "An undefined error occurred. Maybe your session has expired, refresh the page and try again.", "");
				}
			}
			if (res.success)
			{
				if ($btn && !($btn.is(':input')))
					wsko_add_element_ajax_result($btn, true);
				if (reload)
				{
					if (res.redirect)
						location.href = res.redirect;
					else
					{
						if (wsko_data.is_configured && jQuery.wsko_reload_lazy_page != undefined)
							jQuery.wsko_reload_lazy_page();
						else
							location.reload();
					}
				}
			}
			else
			{
				if ($btn && !($btn.is(':input')))
					wsko_add_element_ajax_result($btn, false);
			}
			if ($loader)
				$loader.hide();
		}, function(){
			var err_res = false;
			if (error)
				err_res = error();
			if (!jQuery.wsko_is_in_page_load && !err_res)
				jQuery.wsko_notification(false, "Server Error", "");
			if ($loader)
				$loader.hide();
			if ($btn && !($btn.is(':input')))
				wsko_add_element_ajax_result($btn, false);
		});
	};
	jQuery.wsko_set_element_ajax_loader = function($btn)
	{
		var $loader;
		if ($btn)
		{
			var $btn_r = $btn.is(':input')||$btn.is('button')?$btn.parent():$btn;
			$loader = $btn_r.find('.wsko-loader-small').length == 0 ? $('<div class="loader wsko-loader-small"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle></svg></div>').prependTo($btn_r) : $btn_r.find('.wsko-loader-small').show();
			$btn.find('.wsko-ajax-result').remove();
		}
		return $loader;
	};
	
	/* PROGRESS CIRCLE COMPONENT */
	(function ($) {

		$.fn.circliful = function (options, callback) {

			var settings = $.extend({
				// These are the defaults.
				startdegree: 0,
				fgcolor: "#556b2f",
				bgcolor: "#eee",
				fill: false,
				width: 15,
				dimension: 200,
				fontsize: 15,
				percent: 50,
				animationstep: 1.0,
				iconsize: '20px',
				iconcolor: '#999',
				border: 'default',
				complete: null,
				bordersize: 10
			}, options);

			return this.each(function () {

				var customSettings = ["fgcolor", "bgcolor", "fill", "width", "dimension", "fontsize", "animationstep", "endPercent", "icon", "iconcolor", "iconsize", "border", "startdegree", "bordersize"];

				var customSettingsObj = {};
				var icon = '';
				var endPercent = 0;
				var obj = $(this);
				var fill = false;
				var text, info;

				obj.addClass('circliful');

				checkDataAttributes(obj);

				if (obj.data('text') != undefined) {
					text = obj.data('text');

					if (obj.data('icon') != undefined) {
						icon = $('<i></i>')
							.addClass('fa ' + $(this).data('icon'))
							.css({
								'color': customSettingsObj.iconcolor,
								'font-size': customSettingsObj.iconsize
							});
					}

					if (obj.data('type') != undefined) {
						type = $(this).data('type');

						if (type == 'half') {
							addCircleText(obj, 'circle-text-half', (customSettingsObj.dimension / 1.45));
						} else {
							addCircleText(obj, 'circle-text', customSettingsObj.dimension);
						}
					} else {
						addCircleText(obj, 'circle-text', customSettingsObj.dimension);
					}
				}

				if ($(this).data("total") != undefined && $(this).data("part") != undefined) {
					var total = $(this).data("total") / 100;

					percent = (($(this).data("part") / total) / 100).toFixed(3);
					endPercent = ($(this).data("part") / total).toFixed(3)
				} else {
					if ($(this).data("percent") != undefined) {
						percent = $(this).data("percent") / 100;
						endPercent = $(this).data("percent")
					} else {
						percent = settings.percent / 100
					}
				}

				if ($(this).data('info') != undefined) {
					info = $(this).data('info');

					if ($(this).data('type') != undefined) {
						type = $(this).data('type');

						if (type == 'half') {
							addInfoText(obj, 0.9);
						} else {
							addInfoText(obj, 1.25);
						}
					} else {
						addInfoText(obj, 1.25);
					}
				}

				$(this).width(customSettingsObj.dimension + 'px');

				var canvas = $('<canvas></canvas>').attr({
					width: customSettingsObj.dimension,
					height: customSettingsObj.dimension
				}).appendTo($(this)).get(0);

				var context = canvas.getContext('2d');
				var container = $(canvas).parent();
				var x = canvas.width / 2;
				var y = canvas.height / 2;
				var degrees = customSettingsObj.percent * 360.0;
				var radians = degrees * (Math.PI / 180);
				var radius = canvas.width / 2.5;
				var startAngle = 2.3 * Math.PI;
				var endAngle = 0;
				var counterClockwise = false;
				var curPerc = customSettingsObj.animationstep === 0.0 ? endPercent : 0.0;
				var curStep = Math.max(customSettingsObj.animationstep, 0.0);
				var circ = Math.PI * 2;
				var quart = Math.PI / 2;
				var type = '';
				var fireCallback = true;
				var additionalAngelPI = (customSettingsObj.startdegree / 180) * Math.PI;

				if ($(this).data('type') != undefined) {
					type = $(this).data('type');

					if (type == 'half') {
						startAngle = 2.0 * Math.PI;
						endAngle = 3.13;
						circ = Math.PI;
						quart = Math.PI / 0.996;
					}
				}
			  
				/**
				 * adds text to circle
				 *
				 * @param obj
				 * @param cssClass
				 * @param lineHeight
				 */
				function addCircleText(obj, cssClass, lineHeight) {
					$("<span></span>")
						.appendTo(obj)
						.addClass(cssClass)
						.text(text)
						.prepend(icon)
						.css({
							'line-height': lineHeight + 'px',
							'font-size': customSettingsObj.fontsize + 'px'
						});
				}

				/**
				 * adds info text to circle
				 *
				 * @param obj
				 * @param factor
				 */
				function addInfoText(obj, factor) {
					$('<span></span>')
						.appendTo(obj)
						.addClass('circle-info-half')
						.css(
							'line-height', (customSettingsObj.dimension * factor) + 'px'
						)
						.text(info);
				}

				/**
				 * checks which data attributes are defined
				 * @param obj
				 */
				function checkDataAttributes(obj) {
					$.each(customSettings, function (index, attribute) {
						if (obj.data(attribute) != undefined) {
							customSettingsObj[attribute] = obj.data(attribute);
						} else {
							customSettingsObj[attribute] = $(settings).attr(attribute);
						}

						if (attribute == 'fill' && obj.data('fill') != undefined) {
							fill = true;
						}
					});
				}

				/**
				 * animate foreground circle
				 * @param current
				 */
				function animate(current) {

					context.clearRect(0, 0, canvas.width, canvas.height);

					context.beginPath();
					context.arc(x, y, radius, endAngle, startAngle, false);

					context.lineWidth = customSettingsObj.bordersize + 1;

					context.strokeStyle = customSettingsObj.bgcolor;
					context.stroke();

					if (fill) {
						context.fillStyle = customSettingsObj.fill;
						context.fill();
					}

					context.beginPath();
					context.arc(x, y, radius, -(quart) + additionalAngelPI, ((circ) * current) - quart + additionalAngelPI, false);

					if (customSettingsObj.border == 'outline') {
						context.lineWidth = customSettingsObj.width + 13;
					} else if (customSettingsObj.border == 'inline') {
						context.lineWidth = customSettingsObj.width - 13;
					}

					context.strokeStyle = customSettingsObj.fgcolor;
					context.stroke();

					if (curPerc < endPercent) {
						curPerc += curStep;
						requestAnimationFrame(function () {
							animate(Math.min(curPerc, endPercent) / 100);
						}, obj);
					}

					if (curPerc == endPercent && fireCallback && typeof(options) != "undefined") {
						if ($.isFunction(options.complete)) {
							options.complete();

							fireCallback = false;
						}
					}
				}

				animate(curPerc / 100);

			});
		};
	}(jQuery));
	
	//Init
	jQuery.wsko_init_core();
});