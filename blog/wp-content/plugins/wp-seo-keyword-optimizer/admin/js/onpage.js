jQuery(document).ready(function($){
	
	$('#wsko_check_redirect_form:not(.wsko_init)').addClass('wsko_init').submit(function(event){
		event.preventDefault();
		var $this = $(this),
		page = $this.find('.wsko-field-url').val(),
		status_check = $this.find('.wsko-field-status-check').is(':checked');
		
		if (page)
		{
			jQuery.wsko_post_element({action: 'wsko_check_redirect', url: page, status_check: status_check, nonce: $this.data('nonce')},
			function(res) {
				if (res.success)
					$('#wsko_check_redirect_results').html(res.view);
			},
			false,
			$this.find('.wsko-form-submit'), false);
		}
	});
	$('#wsko_save_robots:not(.wsko_init)').addClass('wsko_init').click(function(event){
		event.preventDefault();
		var $this = $(this);
		jQuery.wsko_post_element({action: 'wsko_save_robots', robots: $('#wsko_robots_field').val(), nonce: $this.data('nonce')}, false, false, $this, false);
	});
	$('#wsko_save_htaccess:not(.wsko_init)').addClass('wsko_init').click(function(event){
		event.preventDefault();
		var $this = $(this);
		jQuery.wsko_post_element({action: 'wsko_save_htaccess', htaccess: $('#wsko_htaccess_field').val(), nonce: $this.data('nonce')}, false, false, $this, false);
	});
	$('#wsko_add_redirect_form:not(.wsko_init)').addClass('wsko_init').submit(function(event){
		event.preventDefault();
		var $this = $(this),
		page = $this.find('.wsko-field-page').val(),
		redirect = $this.find('.wsko-field-redirect').val();
		
		if (page && redirect)
		{
			if (page.toLowerCase().indexOf('https://') !== -1 && !jQuery.wsko_get_ssl() && !confirm('You are using a link with "https", but SSL is not activated. Add redirect regardless?'))
				return;
			jQuery.wsko_post_element({action: 'wsko_add_redirect', comp: $this.find('.wsko-field-comp').val(), page: page, type: $this.find('.wsko-field-type').val(), comp_to: $this.find('.wsko-field-comp-to').val(), redirect_to: redirect, nonce: $this.data('nonce')}, false, false, $this.find('.wsko-save-btn'), true);
		}
	});
	$('#wsko_automatic_redirect_form:not(.wsko_init)').addClass('wsko_init').submit(function(event){
		event.preventDefault();
		var $this = $(this);
		jQuery.wsko_post_element({action: 'wsko_update_automatic_redirect', activate: $this.find('.wsko-field-activate').is(':checked'), type: $this.find('.wsko-field-type').val(), custom: $this.find('.wsko-field-custom').val(), nonce: $this.data('nonce')}, false, false, $this.find('.wsko-save-btn'), false);
	});
	$('.wsko-update-sitemap:not(.wsko_init)').addClass('wsko_init').click(function(event){
		event.preventDefault();
		var $this = $(this),
		types = [],
		stati = [];
		
		$('.wsko-sitemap-param-type').each(function(index){
			var $type = $(this);
			if($type.find('.wsko-sitemap-type-activate').is(':checked'))
			{
				types.push({name: $type.find('.wsko-sitemap-type-activate').val(), freq: $type.find('.wsko-sitemap-subparam-freq').val(), prio: $type.find('.wsko-sitemap-subparam-prio').val()});
			}
		});
		
		$('.wsko-sitemap-param-status').each(function(index){
			var $type = $(this);
			if($type.find('.wsko-sitemap-status-activate').is(':checked'))
			{
				stati.push($type.find('.wsko-sitemap-status-activate').val());
			}
		});
		if (types.length != 0)
			jQuery.wsko_post_element({action: 'wsko_update_sitemap', auto_generation: $('.wsko-sitemap-param-activate').is(':checked'), ping: $('.wsko-sitemap-param-ping').is(':checked'), types: types, stati: stati, excluded_posts: $('.wsko-sitemap-param-excposts').val(), nonce: $this.data('nonce')}, false, false, $this, true);
		else if (types.length == 0)
			jQuery.wsko_notification(false, 'Please select at least one Post Type', '');
	});
	$('.wsko-sitemap-type-activate:not(.wsko_init)').addClass('wsko_init').change(function(){
		var $wrap = $(this).closest('.wsko-sitemap-param-type');
		if ($(this).is(':checked'))
		{
			$wrap.find('.wsko-sitemap-subparam-freq').attr('disabled', false);
			$wrap.find('.wsko-sitemap-subparam-prio').attr('disabled', false);
		}
		else
		{
			$wrap.find('.wsko-sitemap-subparam-freq').attr('disabled', true);
			$wrap.find('.wsko-sitemap-subparam-prio').attr('disabled', true);
		}
	}).trigger("change");
	
	$('#wsko_add_redirect_form .wsko-field-comp:not(.wsko_init)').addClass('wsko_init').change(function(){
		var $this = $(this),
		val = $this.val();
		$('.wsko-redirect-type-infos').hide();
		$('.wsko-redirect-type-infos.wsko-infos-'+val).show();
		$('#wsko_add_redirect_form .wsko-field-page').attr('placeholder', $this.data('ph-'+val));
		if (val == 'exact')
		{
			$('#wsko_add_redirect_form .wsko-field-comp-to').attr('disabled', true).val('exact').trigger('change');
		}
		else
		{
			$('#wsko_add_redirect_form .wsko-field-comp-to').attr('disabled', false);
		}
	}).trigger("change");
	$('#wsko_add_redirect_form .wsko-field-comp-to:not(.wsko_init)').addClass('wsko_init').change(function(){
		var $this = $(this),
		val = $this.val();
		$('#wsko_add_redirect_form .wsko-field-redirect').attr('placeholder', $this.data('ph-'+val));
	}).trigger("change");
	
	$('#wsko_automatic_redirect_form .wsko-field-type:not(.wsko_init)').addClass('wsko_init').change(function(){
		if ($(this).val() == '3')
			$('#wsko_automatic_redirect_form .wsko-field-custom-wrapper').show();
		else
			$('#wsko_automatic_redirect_form .wsko-field-custom-wrapper').hide();
	}).trigger("change");
	$('.wsko-onpage-main-nav li a').click(function(){
		$('#wsko_admin_view_content_wrapper').scrollTop(0);
		$(window).scrollTop(0);
	});
});