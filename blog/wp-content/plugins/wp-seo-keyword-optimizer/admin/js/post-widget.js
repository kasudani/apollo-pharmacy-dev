jQuery(document).ready(function($){
	//Generals
	function wsko_set_generals()
	{
		//Tabs
		$('.wsko-nav .wsko-nav-link:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $this = $(this);
			$this.closest('.wsko-nav').find('.wsko-nav-link').removeClass('wsko-nav-link-active');
			$this.addClass('wsko-nav-link-active');
			var $tab = $($this.attr('href'));
			if ($tab.hasClass('wsko-tab'))
			{
				$tab.closest('.wsko-tab-content').children('.wsko-tab').removeClass('wsko-tab-active').hide();
				$tab.addClass('wsko-tab-active').fadeIn();
			}
		});
		
		$('.wsko-content-optimizer input').keypress(function(e) {
			if(e.which == 13) {
				$(this).change();
				return false;
			}
		});
	}
	
	//Metas
	function wsko_set_meta_elements()
	{
		$('.wsko-metas-search-field:not(.wsko-init)').addClass('wsko-init').on('keyup', function(event){
			var $this = $(this),
			val = $this.val();
			if (event.which == 13)
				return false;
			if (val)
			{
				val = val.toLowerCase();
				$this.closest('.wsko-metas-search-wrapper').find('.wsko-metas-placeholder').each(function(index){
					if ($(this).data('search').search(val) != -1)
						$(this).show();
					else
						$(this).hide();
				});
			}
			else
				$this.closest('.wsko-metas-search-wrapper').find('.wsko-metas-placeholder').show();
			
			$this.closest('.wsko-metas-search-wrapper').find('.wsko-snippet-fields').each(function(index){
				if ($(this).find('.wsko-metas-placeholder:visible').length == 0)
					$(this).find('.wsko-metas-search-no-items').show();
				else
					$(this).find('.wsko-metas-search-no-items').hide();
			});
		});
		$('.wsko-metas-field-hide-slug:not(.wsko-init)').addClass('wsko-init').change(function(){
			if ($(this).is(':checked'))
				$(this).closest('.wsko-set-metas-wrapper').find('.wsko-metas-field-url').attr('disabled', true);
			else
				$(this).closest('.wsko-set-metas-wrapper').find('.wsko-metas-field-url').attr('disabled', false);
		}).change();
		$('.wsko-hightlight-input:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this),
			$container = $this.closest($this.data('container'));
			$container.find('.wsko-hightlight-input').each(function(index){
				var $this = $(this);
				if ($this.data('highlight-timeout'))
					clearTimeout($this.data('highlight-timeout'));
				$container.find($this.data('input')).removeClass('wsko-input-highlight');
			});
			var $meta_field = $container.find($this.data('input')).focus().addClass('wsko-input-highlight');
			$meta_field.data('highlight-timeout', setTimeout(function(){$meta_field.removeClass('wsko-input-highlight');}, 2000));
		});
		
		$('.wsko-set-metas-wrapper:not(.wsko-init)').addClass('wsko-init').on('wsko_metas_submit',function(e){
			var $this = $(this),
			$container = $this.closest('.wsko-set-metas-wrapper');
			/*title = $this.find('.wsko-metas-field-title').val(),
			desc = $this.find('.wsko-metas-field-desc').val(),
			robots_ni = $this.find('.wsko-metas-field-robotsni').is(':checked'),
			robots_nf = $this.find('.wsko-metas-field-robotsnf').is(':checked');*/
			var data = $this.find(':input').serialize();
			if ($this.data('robots'))
			{
				if (!$this.find('.wsko-metas-field-robotsni').is(':checked'))
					data += (data?'&':'')+"robots_ni=false";
				if (!$this.find('.wsko-metas-field-robotsnf').is(':checked'))
					data += (data?'&':'')+"robots_nf=false";
			}
			if ($this.find('.wsko-metas-field-hide-slug').length)
			{
				if (!$this.find('.wsko-metas-field-hide-slug').is(':checked'))
					data += (data?'&':'')+"hide_slug=false";
			}
			$container.find('.wsko-metas-load-overlay').show();
			if ($this.data('new-links') && confirm("You have changed the permalink structure. Do you wan't BST to automatically create redirect rules for the old links?"))
				data += (data?'&':'')+"create_redirects=true";
			jQuery.wsko_post_element({action: 'wsko_set_metas', data: data, type: $container.data('type'), arg: $container.data('arg'), nonce: $container.data('nonce')},
				function(res){
					if (res.success)
					{
						$this.data('new-links', false);
						$container.replaceWith(res.new_view);
						wsko_set_meta_elements();
					}
					$container.find('.wsko-metas-load-overlay').hide();
				}, function() {
					$container.find('.wsko-metas-load-overlay').hide();
				}, false, false);
		}).find(':input:not(.wsko-metas-search-field)').change(function(){
			var $this = $(this),
			$wrapper = $this.closest('.wsko-set-metas-wrapper')
			if ($this.attr('name') == 'url' || $this.attr('name') == 'hide_slug')
				$wrapper.data('new-links', true);
			$wrapper.trigger('wsko_metas_submit');
		});
		$('.wsko-reset-metas-button:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this),
			$container = $this.closest('.wsko-set-metas-wrapper');
			$container.find('.wsko-metas-load-overlay').show();
			jQuery.wsko_post_element({action: 'wsko_set_metas', type: $this.data('type'), arg: $this.data('arg'), reset: true, nonce: $this.data('nonce')},
				function(res){
					if (res.success)
					{
						$container.replaceWith(res.new_view);
						wsko_set_meta_elements();
					}
					$container.find('.wsko-metas-load-overlay').hide();
				}, function() {
					$container.find('.wsko-metas-load-overlay').hide();
				}, false, false);
		});
		if (typeof(jQuery.ui.draggable) != 'undefined' && typeof(jQuery.ui.droppable) != 'undefined')
		{
			$('.wsko-metas-placeholder:not(.wsko-init)').addClass('wsko-init').draggable({revert: true, helper: "clone" });
			$('.wsko-metas-field-desc,.wsko-metas-field-title').not('.wsko-init').addClass('wsko-init').droppable({
				accept: '.wsko-metas-placeholder',
				drop: function( event, ui ) {
					var text = $(ui.draggable.eq(0)).data('tag'),
					element = $(this).get(0);
					if (document.selection) {  
						element.focus();  
						var sel = document.selection.createRange();  
						sel.text = text;  
						element.focus();  
					} else if (element.selectionStart || element.selectionStart === 0) {  
						var startPos = element.selectionStart;  
						var endPos = element.selectionEnd;  
						var scrollTop = element.scrollTop;  
						element.value = element.value.substring(0, startPos) + text +   
										 element.value.substring(endPos, element.value.length);  
						element.focus();  
						element.selectionStart = startPos + text.length;  
						element.selectionEnd = startPos + text.length;  
						element.scrollTop = scrollTop;  
					} else {  
						element.value += text;  
						element.focus();  
					}  
					$(element).change();
					/*$(this).
				  .addClass( "ui-state-highlight" )
				  .find( "p" )
					.html( "Dropped!" );*/
				}
			});
		}
	}
	
	//Keywords
	function wsko_set_keyword_elements()
	{
		$('.wsko-co-add-priority-keyword:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $pk = $(this).closest('.wsko-co-priority-keywords-container'),
			keyword = $pk.find('.wsko-co-keyword-input').val(),
			prio = $pk.find('.wsko-co-keyword-prio').val();
			jQuery.wsko_post_element({action: 'wsko_co_add_priority_keyword', post_id: $pk.data('post'), keyword: keyword, prio: prio, nonce: $pk.data('nonce')},
				function(res){
					if (res.success || res.view)
					{
						wsko_reload_issues($pk);
						$pk.closest('.wsko-content-optimizer').find('.wsko-co-priority-keyword-group[data-prio="'+prio+'"]').append(res.view).find('.wsko-co-keyword-group-no-items').hide();
						wsko_set_keyword_elements();
					}
				}, false, $pk, false);
		});
		$('.wsko-co-add-priority-keyword-inline:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $pk = $(this),
			prio = $pk.data('prio')
			jQuery.wsko_post_element({action: 'wsko_co_add_priority_keyword', post_id: $pk.data('post'), keyword: $pk.data('keyword'), prio: prio, nonce: $pk.data('nonce')},
				function(res){
					if (res.success || res.view)
					{
						wsko_reload_issues($pk);
						$pk.closest('.wsko-content-optimizer').find('.wsko-co-priority-keyword-group[data-prio="'+prio+'"]').append(res.view).find('.wsko-co-keyword-group-no-items').hide();
						wsko_set_keyword_elements();
					}
				}, false, $pk, false);
		});
		$('.wsko-co-delete-priority-keyword:not(.wsko_init)').addClass('wsko_init').click(function(event){
			event.preventDefault();
			var $pk = $(this).closest('.wsko-co-priority-keyword');
			jQuery.wsko_post_element({action: 'wsko_co_delete_priority_keyword', post_id: $pk.data('post'), keyword: $pk.data('keyword'), nonce: $pk.data('nonce')},
				function(res){
					if (res.success)
					{
						$pk = $pk.closest('.wsko-content-optimizer').find('.wsko-co-priority-keyword[data-keyword="'+$pk.data('keyword')+'"]');
						$group = $pk.fadeOut('fast', function(){ $(this).remove()}).closest('.wsko-co-priority-keyword-group');
						if ($group.find('.wsko-co-priority-keyword').length == 1)
							$group.find('.wsko-co-keyword-group-no-items').show();
					}
				}, false, $pk, false);
		});
		if (typeof(jQuery.ui.draggable) != 'undefined' && typeof(jQuery.ui.droppable) != 'undefined')
		{
			$('.wsko-co-priority-keyword-group:not(.wsko_init)').addClass('wsko_init').droppable({
				accept: '.wsko-co-priority-keyword,.wsko-co-keyword-draggable',
				drop: function( event, ui ){
					var $pk = $(ui.draggable.eq(0)),
					$target = $(this);
					$target.find('.wsko-co-priority-keyword-placeholder').remove();
					if ($pk.hasClass('wsko-co-keyword-draggable'))
					{
						jQuery.wsko_post_element({action: 'wsko_co_add_priority_keyword', post_id: $target.closest('.wsko-co-priority-keywords-container').data('post'), keyword: $pk.data('keyword'), prio: $target.data('prio'), nonce: $target.closest('.wsko-co-priority-keywords-container').data('nonce')},
							function(res){
								if (res.success || res.view)
								{
									$target.closest('.wsko-content-optimizer').find('.wsko-co-priority-keyword-group[data-prio="'+$target.data('prio')+'"]').append(res.view);
									wsko_set_keyword_elements();
								}
							}, false, $target, false);
					}
					else
					{
						jQuery.wsko_post_element({action: 'wsko_co_add_priority_keyword', post_id: $pk.data('post'), keyword: $pk.data('keyword'), prio: $target.data('prio'), nonce: $pk.closest('.wsko-co-priority-keywords-container').data('nonce')}, false, false, $pk, false);
					}
					$pk.css({'top': '0px', 'left': '0px'});
				},
				over: function(event, ui) {
					var $pk = $(ui.draggable.eq(0));
					$(this).append('<div class="wsko-co-priority-keyword-placeholder">'+$pk.data('keyword')+'</div>');
				},
				out: function(event, ui) {
				   $(this).find('.wsko-co-priority-keyword-placeholder').remove();
				}
			});
			$('.wsko-co-priority-keyword:not(.wsko_init)').addClass('wsko_init').each(function(index){ 
				var $this = $(this);
				$this.draggable({ containment: $this.closest('.wsko-content-optimizer'), revert: "invalid" });
			});
			$('.wsko-co-keyword-draggable:not(.wsko_init)').addClass('wsko_init').each(function(index){ 
				var $this = $(this);
				$this.draggable({ containment: $this.closest('.wsko-content-optimizer'), revert: true, scroll: false, helper: "clone", appendTo: $this.closest('.wsko-content-optimizer')});
			});
		}
		$('.wsko-co-keyword-input:not(.wsko_init)').addClass('wsko_init').each(function(index){
			var timeout;
			var $this = $(this),
			$suggests = $this.closest('.wsko-co-priority-keywords-container').find('.wsko-co-keyword-suggests');
			$this.on('keyup change', function(){
				if (timeout)
					clearTimeout(timeout);
				if ($this.val() != $this.data('old-val'))
				{
					timeout = setTimeout(function()
					{
						$suggests.html('<div class="wsko-loader-small"></div>');
						jQuery.wsko_post_element({action: 'wsko_co_get_keyword_suggests', keyword: $this.val(), nonce: $this.data('nonce')},
							function(res){
								if (res.success)
								{
									$this.data('old-val', $this.val());
									$suggests.html(res.view);
									$suggests.find('.wsko-co-keyword-suggestion').click(function(event){
										event.preventDefault();
										$this.val($(this).data('val'));
									});
									$suggests.find('.wsko-keyword-suggestion-add').click(function(event){
										event.preventDefault();
										event.stopPropagation();
										var $pk = $(this);
										jQuery.wsko_post_element({action: 'wsko_co_add_priority_keyword', post_id: $pk.closest('.wsko-co-priority-keywords-container').data('post'), keyword: $pk.data('keyword'), prio: $pk.data('prio'), nonce: $pk.closest('.wsko-co-priority-keywords-container').data('nonce')},
											function(res){
												if (res.success || res.view)
												{
													$pk.closest('.wsko-content-optimizer').find('.wsko-co-priority-keyword-group[data-prio="'+$pk.data('prio')+'"]').append(res.view).find('.wsko-co-keyword-group-no-items').hide();
													wsko_set_keyword_elements();
												}
											}, false, $pk, false);
										$pk.closest('.wsko-co-keyword-suggest-wrapper').find('input.wsko-co-keyword-input').focus();
									});
								}
								else
								{
									$suggests.html("");
								}
								return true;
							}, function(){
								$suggests.html("");
								return true;
						}, false, false);
					}, 1000);
				}
			}).keypress(function(e){
				if(e.which == 13) {
					$(this).closest('.wsko-co-keyword-suggest-wrapper').find('.wsko-co-add-priority-keyword').click();
				}
			});
		});
		$('.keyword-suggest-inner-wrapper:not(.wsko-init)').addClass('wsko-init').each(function(index){ 
			var $this = $(this)
			$input = $this.closest('.wsko-co-keyword-suggest-wrapper').find('input.wsko-co-keyword-input');
			var timeout;
			$input.focusout(function(event){
				timeout = setTimeout(function(){
					$this.hide();
				}, 500)
			});
			$input.focusin(function(event){
				if (timeout) clearTimeout(timeout);
				$this.show();
			});
		});
	}
	
	//Content
	function wsko_set_content_elements()
	{
		$('.wsko-co-save-content:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this),
			title = $this.closest('.wsko-co-update-content').find('.wsko-co-content-field-title').val(),
			content = $this.closest('.wsko-co-update-content').find('.wsko-co-content-field-content').val(),
			slug = $this.closest('.wsko-co-update-content').find('.wsko-co-content-field-slug').val();
			if (title && content)
			{
				jQuery.wsko_post_element({action: 'wsko_co_save_content', post_id: $this.data('post'), title: title, content: content, slug:slug, nonce: $this.data('nonce')}, false, false, $this, false);
			}
		});
	}
	
	//Content Optimizer Sticky Options
	function wsko_post_widget_sticky()
	{
		/*var iScrollPos = 0;
		$(window).scroll(function () {
			var iCurScrollPos = $(this).scrollTop();
			if (iCurScrollPos > iScrollPos) {
				$('.wsko-co-widget.wsko-short-view-active .bsu-tabs').fadeOut();
				//$('.wsko-content-optimizer').addClass('wsko-nav-hidden');
			} else {
				$('.wsko-co-widget.wsko-short-view-active .bsu-tabs').fadeIn();
				//$('.wsko-content-optimizer').removeClass('wsko-nav-hidden');
			}
			iScrollPos = iCurScrollPos;
		});
		
		$('.wsko-short-view-active .wsko-nav a').not('.wsko-init').addClass('wsko-init').click(function () {
			event.preventDefault();
			if ($(this).closest('.wsko-short-view-active').length)
			{
				$('.wsko-co-widget').toggleClass('wsko-short-view-active');	
			}
		});
		*/
		$('.wsko-co-widget.wsko-short-view-active .bsu-tabs').fadeIn();
	}

	//Technicals
	function wsko_set_technical_seo_elements()
	{
		$('.wsko-co-canonical-type:not(.wsko-init)').addClass('wsko-init').change(function(event){
			var $this = $(this);
			if ($this.val() == '2' || $this.val() == '3')
				$this.closest('.wsko-co-technical-wrapper').find('.wsko-co-canonical-arg').show();
			else
				$this.closest('.wsko-co-technical-wrapper').find('.wsko-co-canonical-arg').hide();
		});
		$('.wsko-co-save-technical:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this),
			$wrapper = $this.closest('.wsko-co-technical-wrapper'),
			canon_type = $wrapper.find('.wsko-co-canonical-type').val(),
			canon_arg = $wrapper.find('.wsko-co-canonical-arg').val(),
			activate_redirect = $wrapper.find('.wsko-co-redirect-activate').is(':checked'),
			redirect_type = $wrapper.find('.wsko-co-redirect-type').val(),
			redirect_to = $wrapper.find('.wsko-co-redirect-to').val(),
			sitemap_exclude = $wrapper.find('.wsko-co-sitemap-exclude').is(':checked');
			
			var data = {activate_redirect: activate_redirect, redirect_type: redirect_type, redirect_to: redirect_to, canon_type: canon_type, canon_arg: canon_arg, sitemap_exclude: sitemap_exclude};
			if ((canon_type == '2' || canon_type == '3') && (!canon_arg || canon_arg == ""))
			{
				jQuery.wsko_notification(false, 'You need to set a target for your canonical!', '');
				return;
			}
			jQuery.wsko_post_element({action: 'wsko_co_save_technicals', post_id: $this.data('post'), data: data, nonce: $this.data('nonce')}, false, false, $this, false);
		});
	}
	
	//Helpers
	function wsko_reload_issues($el)
	{
		//console.log('test');
		//jQuery.wsko_load_lazy_data_for_controller($el.closest('.wsko-content-optimizer').find('.wsko-co-onpage-issues-wrapper'), 'WSKO_Controller_Optimizer', false, 'onpage_issues', false);
	}
	
	$(document).on("wsko_init_core", function(e){
		wsko_set_generals();
		wsko_set_meta_elements();
		wsko_set_content_elements();
		wsko_set_keyword_elements();
		wsko_set_technical_seo_elements();
		wsko_post_widget_sticky();
	});
	wsko_set_generals();
	wsko_set_meta_elements();
	wsko_set_content_elements();
	wsko_set_keyword_elements();
	wsko_set_technical_seo_elements();
	wsko_post_widget_sticky();
	
});