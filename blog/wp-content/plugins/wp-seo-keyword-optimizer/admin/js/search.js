jQuery(document).ready(function($){
	$(document).on('wsko_init_page', function(e) {
		$('.wsko-remove-monitoring-keyword:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this);
			jQuery.wsko_post_element({action: 'wsko_remove_monitoring_keyword', keyword: $this.data('keyword'), nonce: $this.data('nonce')}, false, false, $this, true);
		});
	});
	$('#wsko_keyword_research_form:not(.wsko-init)').addClass('wsko-init').submit(function(event){
		event.preventDefault();
		var $this = $(this);
		if ($this.find('input[name="keyword"]').val())
		{
			var types = []
			$this.find('input[name="type[]"]:checked').each(function(index) {
				types.push($(this).val());
			});
			if (types.length > 0)
				jQuery.wsko_post_element({action: 'wsko_do_keyword_research', keyword: $this.find('input[name="keyword"]').val(), country: $this.find('select[name="country"]').val(), type: types, nonce: $this.data('nonce')}, false, false, $this, true);
		}
	});
	$('.wsko-close-keyword-research:not(.wsko-init)').addClass('wsko-init').click(function(event){
		event.preventDefault();
		$('#wsko_keyword_research_overlay').fadeOut();
	});
	$('.wsko-open-keyword-research:not(.wsko-init)').addClass('wsko-init').click(function(event){
		event.preventDefault();
		var $this = $(this);
		$('#wsko_keyword_research_container').html('').hide();
		$('#wsko_keyword_research_overlay').fadeIn();
		jQuery.wsko_post_element({action: 'wsko_get_keyword_research', id: $this.data('research')},
			function(res){
				if (res.success)
				{
					$('#wsko_keyword_research_container').show().html(res.view);
				}
				else
				{
					$('#wsko_keyword_research_container').show().html('<i class="fa fa-times fa-5x"></i>');
				} 
			}, false, $this, false);
	});
	$('.wsko-add-monitoring-keyword-input:not(.wsko-init)').addClass('wsko-init').keydown(function(e){
		if (e.which == 13)
		{
			$(this).closest('.wsko-keyword-monitoring-add').find('.wsko-add-monitoring-keyword').click();
		}
	});
	$('.wsko-add-monitoring-keyword:not(.wsko-init)').addClass('wsko-init').click(function(event){
		event.preventDefault();
		var $this = $(this),
		keyword = $this.closest('.wsko-keyword-monitoring-add').find('.wsko-add-monitoring-keyword-input').val();
		if (keyword)
			jQuery.wsko_post_element({action: 'wsko_add_monitoring_keyword', keyword: keyword, multi:true, nonce: $this.data('nonce')}, false, false, $this, true);
	});
});