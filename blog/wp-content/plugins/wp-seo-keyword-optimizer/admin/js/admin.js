jQuery(document).ready(function($)
{
	//Load necessaries
	google.charts.load('current', {'packages':['corechart', 'geochart'], 'mapsApiKey': wsko_admin_data.mapsApiKey});
	
	//Init
	jQuery.wsko_init_admin = function wsko_init_admin()
	{
		//Init Admin
		$('.wsko-ajax-tables:not(.wsko-init)').addClass('wsko-init').each(function(index){
			var $this = $(this),
			$wrapper = $this.parents('.wsko-table-wrapper'),
			columns = [];
			$this.find('thead tr th').each(function(index){
				columns.push({data: $(this).data('name')});
			});
			var order = false;
			if ($this.data('def-order') != undefined)
			{
				order = [[parseInt($this.data('def-order')), $this.data('def-orderdir')]];
			}
			var args = {
				"order": order ? order : [[ 0, "desc" ]],
				"processing": true,
				"serverSide": true,
				"searchDelay": 1000,
				"pageLength": 25,
				"columns": columns,
				"ajax": {
					"url": wsko_admin_data.ajaxurl,
					"type": "POST",
					"data": function(d) {
						d.action = $this.data('action');
						d.nonce = $this.data('nonce');
						if ($this.data('arg'))
							d.arg = $this.data('arg');
						d.wsko_controller = wsko_admin_data.controller;
						d.wsko_controller_sub = wsko_admin_data.subpage;
						d.custom_filter = [];
						$wrapper.find('.wsko-table-custom-filter').each(function(index){
							var $filter = $(this),
							$field = $filter.find('.wsko-table-custom-filter-input');
							d.custom_filter.push({key: $filter.data('name'), val: $field.is('[type="checkbox"]') ? $field.is(':checked') : $field.val(), comp: $filter.data('comp')});
						});
					},
				},
				"drawCallback": function( settings ) {
					jQuery.wsko_init_core();
				}
			};
			if ($this.data('change-search'))
			{
				args['oLanguage'] = {
					"sSearch": $this.data('change-search')
				}
			}
			var dataTable = $this.DataTable(args);
			$wrapper.find('.wsko-table-add-filter').click(function(event){
				var $filter = $(this),
				type = $filter.data('type');
				var arg = false;
				if (type == 'select')
					arg = $filter.data('values');
				else if (type == 'number_range')
					arg = $filter.data('max');
				wsko_add_table_filter($filter.data('name'), $filter.data('title'), type, arg, "", false);
			});
			$this.on('wsko_add_external_filter', function(e, arg){
				var args = arg.split(':'),
				$filter = $wrapper.find('.wsko-table-add-filter[data-name="'+args[0]+'"]'),
				val = "",
				type = $filter.data('type');
				if (args.length == 1)
					val = "";
				else if (args.length == 2)
					val = args[1];
				else if (args.length == 3)
					val = [args[1],args[2]];
				if ($filter.length != 0)
				{
					var arg = false;
					if (type == 'number_range')
						arg = $filter.data('max');
					wsko_add_table_filter(args[0], $filter.data('title'), type, arg, val, true);
				}
			});
			function wsko_add_table_filter(name, title, type, arg, value, replace)
			{
				if (value == undefined)
					value = "";
				var $old_filter = $wrapper.find('.wsko-table-custom-filter[data-name="'+name+'"]');
				if ($old_filter.length != 0)
				{
					if (replace)
					{
						$old_filter.remove();
					}
					else
						return;
				}
				var $temp = $('<div class="wsko-table-custom-filter row" data-name="'+name+'"></div>');
				switch(type)
				{
					case 'text':
						$temp.data('comp', 'co');
						$temp.append('<div class="col-sm-2 col-xs-3"><label>'+title+'</label></div>');
						$temp.append('<div class="col-sm-9 col-xs-8"><input class="form-control wsko-table-custom-filter-input" type="text" value="'+value+'"></div>');
						break;
					case 'exact':
						$temp.data('comp', 'eq');
						$temp.append('<div class="col-sm-2 col-xs-3"><label>'+title+'</label></div>');
						$temp.append('<div class="col-sm-9 col-xs-8"><input class="form-control wsko-table-custom-filter-input" type="text" value="'+value+'"></div>');
						break;
					case 'number':
						$temp.data('comp', 'eq');
						$temp.append('<div class="col-sm-2 col-xs-3"><label>'+title+'</label></div>');
						$temp.append('<div class="col-sm-9 col-xs-8"><input class="form-control wsko-table-custom-filter-input" type="number" value="'+value+'"></div>');
						break;
					case 'number_range':
						$temp.data('comp', 'ra');
						$temp.append('<div class="col-sm-2 col-xs-3"><label>'+title+'</label></div>');					
						$temp.append('<div class="col-sm-9 col-xs-8"><div class="wsko-range-slider" data-max="'+arg+'" data-val1="'+(value?value[0]:"")+'" data-val2="'+(value?value[1]:"")+'" data-label="true"><input class="wsko-range-slider-input wsko-table-custom-filter-input" type="hidden" value="'+(value?value[0]+':'+value[1]:"")+'"></div></div>');
						break;
					case 'select':
						$temp.data('comp', 'co');
						$temp.append('<div class="col-sm-2 col-xs-3"><label>'+title+'</label></div>');
						var input ='<div class="col-sm-9 col-xs-8"><select class="form-control wsko-table-custom-filter-input">';
						$.each(arg, function (k, v){
							input += '<option value="'+k+'">'+v+'</option>';
						});
						input += '</select></div>';
						$temp.append(input);
						break;
					case 'set':
						$temp.data('comp', 'set');
						$temp.append('<div class="col-sm-2 col-xs-3"><label>'+title+'</label></div>');
						$temp.append('<div class="col-sm-9 col-xs-8"><input class="form-control wsko-table-custom-filter-input" type="checkbox" '+(value=='on'?'checked':'')+'> Is set</div>');
						break;
				}
				$temp.append('<div class="col-sm-1 col-xs-1 align-right"><a href="#" class="wsko-delete-custom-filter pull-right"><i class="fa fa-times"></i></a></div>').find('.wsko-delete-custom-filter').click(function(event){
					$(this).parents('.wsko-table-custom-filter').fadeOut('fast', function(){ $(this).remove(); dataTable.ajax.reload(); $wrapper.find('.wsko-table-add-filter[data-name="'+name+'"]').removeClass('wsko-disabled'); });
				});
				$temp.find('.wsko-table-custom-filter-input').change(function(){ dataTable.ajax.reload(); });
				$wrapper.find('.wsko-table-filter-box').append($temp);
				$wrapper.find('.wsko-table-add-filter[data-name="'+name+'"]').addClass('wsko-disabled');
				jQuery.wsko_init_core();
				if (value != "" || type == 'select')
					dataTable.ajax.reload();
			}
		});
		$('.wsko-external-table-filter:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this);
			$($this.data('table')).trigger('wsko_add_external_filter', [$this.data('val')]);
		});
		$('.wsko-load-lazy-page:not(.wsko-init)').addClass('wsko-init').click(function(event){
			event.preventDefault();
			var $this = $(this);
			jQuery.wsko_load_lazy_page($this.data('controller'), $this.data('subpage'), $this.data('subtab'), $this.attr('href')+($this.data('subtab')?'&showtab='+$this.data('subtab'):''), false);
		});
		$('.wsko-ajax-input:not(.wsko-ap-init)').addClass('wsko-ap-init').each(function(){
			var $this = $(this);
			if ($this.is("input") || $this.is("select"))
			{
				
				$this.data('wsko-old-val', $this.val());
				if ($this.is('select'))
				{
					$this.change(function(){
						var val = $(this).find("option:selected").val(),
						reload = $(this).data('reload') ? true : false,
						reload_real = $(this).data('reload-real') ? true : false;
						
						if (val != $this.data('wsko-old-val'))
						{
							var old_val = $this.data('wsko-old-val');
							$this.data('wsko-old-val', val);
							jQuery.wsko_post_element({action: 'wsko_save_ajax_input', target: $this.data('wsko-target'), setting: $this.data('wsko-setting'), val: val, nonce: wsko_admin_data.save_ajax_input_nonce},
								function(res){
									$this.attr('disabled', false);
									if (!res.success)
										$this.val(old_val);
									else if (reload_real)
										location.reload(true);
								}, function(){
									$this.attr('disabled', false);
									$this.val(old_val);
								}, $this, reload_real ? false : reload);
						}
					});
				}
				else if ($this.is('[type="checkbox"]'))
				{
					$this.change(function(){
						var multi_parent = $this.data('multi-parent'),
						val = multi_parent ? [] : $this.is(':checked'),
						alert_m = $(this).data('alert'),
						alert_send = $(this).data('alert-send') ? true : false;
						
						if (multi_parent)
						{
							$this.closest(multi_parent).find('.wsko-ajax-input[type="checkbox"]:checked').each(function(index){
								val.push($(this).val());
							});
							val = val.join(',');
						}
						if (multi_parent || val != $this.data('wsko-old-val'))
						{
							var old_val = $this.data('wsko-old-val');
							$this.data('wsko-old-val', val);
							$this.attr('disabled', true);
							
							var alert_r = false,
							data = {action: 'wsko_save_ajax_input', target: $this.data('wsko-target'), setting: $this.data('wsko-setting'), val: val, nonce: wsko_admin_data.save_ajax_input_nonce};
							if (!alert_m || confirm(alert_m))
								alert_r = alert_m ? true : false;
							if (alert_m && alert_send)
								data['alert'] = alert_r;
							if (alert_m && !alert_send && !alert_r)
								return;
							jQuery.wsko_post_element(data,
								function(res){
									$this.attr('disabled', false);
									if (!res.success)
										$this.attr('checked', old_val && old_val != 'false' ? true : false);
								}, function(){
									$this.attr('disabled', false);
									$this.attr('checked', old_val && old_val != 'false' ? true : false);
								}, $this, $this.data('reload') ? true : false);
						}
					});
				}
				else
				{
					$this.focusout(function(){
						var val = $this.val();
						if (val != $this.data('wsko-old-val'))
						{
							var old_val = $this.data('wsko-old-val');
							$this.data('wsko-old-val', val);
							jQuery.wsko_post_element({action: 'wsko_save_ajax_input', target: $this.data('wsko-target'), setting: $this.data('wsko-setting'), val: val, nonce: wsko_admin_data.save_ajax_input_nonce},
								function(res){
									$this.attr('disabled', false);
									if (!res.success)
										$this.val(old_val);
								}, function(){
									$this.attr('disabled', false);
									$this.val(old_val);
								}, $this, false);
						}
					});
				}
			}
		});
		$('.wsko-range-slider:not(.wsko-init)').addClass('wsko-init').each(function(index){
			var $this = $(this),
			$slider = $this.prepend('<div class="wsko-slider-obj"></div>'),
			min = $this.data('min') || $this.data('min') == '0' ? $this.data('min') : 0,
			max = $this.data('max') || $this.data('max') == '0' ? $this.data('max') : 100;
			if (min == max)
				max++;
			var start = $this.data('val1') || $this.data('val1') == '0' ? $this.data('val1') : min,
			end = $this.data('val2') || $this.data('val2') == '0' ? $this.data('val2') : max;
			var slider = noUiSlider.create($slider.get(0), {
				start: [start, end],
				connect: true,
				step: 1,
				orientation: 'horizontal', // 'horizontal' or 'vertical'
				range: {
					'min': min,
					'max': max
				},
				format: wNumb({
					decimals: 0
				})
			}).on('change', function(){
				var val = $slider.get(0).noUiSlider.get();
				$this.find('.wsko-range-slider-input').val(val.join(':')).trigger('change');
				$this.find('.wsko-range-slider-from').html(Math.round(val[0] * 100) / 100);
				$this.find('.wsko-range-slider-to').html(Math.round(val[1] * 100) / 100);
			});
			if ($this.data('label'))
			{
				$slider.prepend('<span class="wsko-range-val"><span class="wsko-range-slider-from">'+min+'</span> - <span class="wsko-range-slider-to">'+max+'</span></span>');
				
				$this.find('.wsko-range-slider-from').html(Math.round(start * 100) / 100);
				$this.find('.wsko-range-slider-to').html(Math.round(end * 100) / 100);
			}
		});
		$('.wsko-admin-timespan-picker-form:not(.wsko-init)').addClass('wsko-init').each(function(index){
			var $this = $(this),
			$start_time = $this.find('.wsko-start-time'),
			$end_time = $this.find('.wsko-end-time');
			$this.submit(function(event){
				event.preventDefault();
				jQuery.wsko_post_element({
						wsko_controller: wsko_admin_data.controller,
						wsko_controller_sub: wsko_admin_data.subpage,
						start_time: $start_time.val(),
						end_time: $end_time.val(),
						action : 'wsko_set_timespan',
						nonce: wsko_admin_data.timespan_nonce
					}, function(res){
						if (res.success)
						{
							$(document).trigger("wsko_event_timespan_set");
						}
					}, false, $this.find('.wsko-loader'), true);
			});
			$this.find('.wsko-admin-timespan-picker').daterangepicker({
				startDate: moment.unix(parseInt(wsko_admin_data.timespan_start)).startOf('day'),
				endDate: moment.unix(parseInt(wsko_admin_data.timespan_end)).startOf('day'),
				//minDate: moment.unix(parseInt(wsko_admin_data.first_date)).startOf('day'),
				maxDate: moment().startOf('day').subtract(3, 'days'),
				ranges: {
				   //'Today': [moment(), moment()],
				   //'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				   'Last 7 Days': [moment().startOf('day').subtract(3 + 6, 'days'), moment().startOf('day').subtract(3, 'days')],
				   'Last 28 Days': [moment().startOf('day').subtract(3 + 27, 'days'), moment().startOf('day').subtract(3, 'days')],
				   'This Month': [moment().startOf('month'), moment().endOf('month')],
				   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}).on('apply.daterangepicker', function(ev, picker) {
				var $this = $(this);
				wsko_admin_data.timespan_start = picker.startDate.unix();//.utc().startOf('day').unix();
				wsko_admin_data.timespan_end = picker.endDate.unix();//.utc().startOf('day').unix();
				$this.find('.wsko-timespan-label').html(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
				$start_time.val(wsko_admin_data.timespan_start);
				$end_time.val(wsko_admin_data.timespan_end);
				$this.parents('.wsko-admin-timespan-picker-form').submit();
			});
			$this.find('.wsko-timespan-label').html(moment.unix(wsko_admin_data.timespan_start).format('MMMM D, YYYY') + ' - ' + moment.unix(wsko_admin_data.timespan_end).format('MMMM D, YYYY'));
			$start_time.val(wsko_admin_data.timespan_start);
			$end_time.val(wsko_admin_data.timespan_end);
		});
		$('.wsko-give-feedback:not(.wsko-init)').addClass('wsko-init').click(function(event) {
			event.preventDefault();
			
			var $this = $(this),
			$modal = $('#wsko_modal_feedback').modal('show');
			
			$modal.find('.wsko-feedback-msg').val('');
			$modal.find('.wsko-feedback-title').val('');
		});
		$('.wsko-give-rating:not(.wsko-init)').addClass('wsko-init').click(function(event) {
			event.preventDefault();
			
			var $this = $(this),
			$modal = $('#wsko_modal_rating').modal('show');
			
			//$modal.find('.wsko-feedback-msg').val('');
			//$modal.find('.wsko-feedback-title').val('');
		});
		$('.wsko-import-plugin:not(.wsko-init)').addClass('wsko-init').click(function(event) {
			event.preventDefault();
			
			var $this = $(this),
			$wrapper = $this.closest('.wsko-import-plugin-wrapper'),
			options = [];
			$wrapper.find('.wsko-import-plugin-option:checked').each(function(index){
				options.push($(this).val());
			});
			if (options.length)
			{
				jQuery.wsko_post_element({
						'plugin': $wrapper.data('plugin'),
						'options': options,
						'nonce': $wrapper.data('nonce'),
						action : 'wsko_import_plugin',
					}, false, false, $this, false);
			}
			else
			{
				jQuery.wsko_notification(false, 'Please select something to import', '');
			}
		});
		
		$('.wsko-admin-request-api-access:not(.wsko-init)').addClass('wsko-init').submit(function(event){
			event.preventDefault();
			var $this = $(this),
			code = $this.find('.wsko-token-field').val();
			$this.closest('.wsko-settings-api-box').find('.wsko-api-login-help-box').hide();
			$this.closest('.wsko-settings-api-box').find('.wsko-api-login-help-box-custom').hide().html('');
			if (code)
				jQuery.wsko_post_element({action: 'wsko_request_api_access', code: code, type: $this.data('api'), nonce: $this.data('nonce')}, 
				function(res) {
					if (!res.success) 
					{
						if (!res.err_view)
							$this.closest('.wsko-settings-api-box').find('.wsko-api-login-help-box').show();
						else
							$this.closest('.wsko-settings-api-box').find('.wsko-api-login-help-box-custom').show().html(res.err_view);
					}
				},
				false, $this.find('.wsko-request-btn'), true);
		});
		$('.wsko-api-twitter-set-profile-btn').click(function(event){
			event.preventDefault();
			var $this = $(this),
			profile = $this.parents('.wsko-api-twitter-set-profile').find('.wsko-api-twitter-set-profile-value').val();
			jQuery.wsko_post_element({action: 'wsko_set_twitter_profile', profile: profile, nonce: $this.data('nonce')}, false, false, $this, true);
		});
		$('#wsko_reset_configuration:not(.wsko-init)').addClass('wsko-init').click(function(event){
			var $this = $(this);
			if(confirm('Are you sure you want to delete your configuration? This will delete everything you have set in the plugin, including all your metas and Onpage data. Please note that this is only necessary if you want to delete the plugin forever and thus any data trace of it.'))
				jQuery.wsko_post_element({action: 'wsko_reset_configuration', delete_cache: $('#wsko_reset_opt_cache').is(':checked'), delete_backups: $('#wsko_reset_opt_backups').is(':checked'), nonce: $this.data('nonce')}, false, false, $this, true);
		});
		$('#wsko_modal_feedback .wsko-feedback-type-btn input').change(function(){
			if ($(this).is(':checked'))
			{
				if ($(this).val() == '1')
					$('#wsko_modal_feedback').find('.wsko-support-options').show();
				else
					$('#wsko_modal_feedback').find('.wsko-support-options').hide();
			}
		});
		
		//Resolve URLs
		var urlFields = [];
		$('.wsko-ajax-url-field:not(.wsko-ajax-loaded)').each(function(index){
			var $this = $(this);
			if ($this.parents('.wsko_tables').length == 0)
			{
				if (!urlFields.includes($this.data('url')))
					urlFields.push($this.data('url'));
				$this.addClass('wsko-ajax-load');
			}
		});
		if (urlFields.length > 0)
		{
			jQuery.wsko_post_element({action: 'wsko_resolve_url', urls: urlFields, nonce: wsko_admin_data.resolve_nonce},
				function(res){
					if (res.success)
					{
						$.each(res.urls, function(k, v){
							$('.wsko-ajax-url-field.wsko-ajax-load[data-url="'+k+'"]').html(v.title).removeClass('wsko-ajax-load').addClass('wsko-ajax-loaded');
						});
					}
					else
					{
						$('.wsko-ajax-url-field.wsko-ajax-load').html('<p style="color:red">Resolving Error</p>').removeClass('wsko-ajax-load').addClass('wsko-ajax-loaded');
					}
					return true;
				}, function(){
					$('.wsko-ajax-url-field.wsko-ajax-load').html('<p style="color:red">Resolving Error</p>').removeClass('wsko-ajax-load').addClass('wsko-ajax-loaded');
					return true;
				}, false, false);
		}
		
		$('.wsko-metas-bulk-collapse:not(.wsko_init)').addClass('wsko_init').each(function(index){
			var $this = $(this);
			$this.click(function(event){
				event.preventDefault();
				$this.closest('.wsko-set-metas-wrapper').find('ul.wsko-tabs-social-snippets').toggle();
				var old = $this.html();
				$this.html($this.data('toggle-heading'));
				$this.data('toggle-heading', old);
			});

		});
		
		//Externals
		$('[data-toggle="tooltip"]').tooltip();
		$('.selectpicker').selectpicker();

		//Do customs
		$(document).trigger("wsko_init_page");
	}
	
	//Mobile Navigation
	$('.mobile-navi-toggle').click(function() {
		$('.wsko_wrapper.mobile-navi').toggleClass('active');
	});
	
	//Statics
	$('#wsko_feedback_form').submit(function(event){
		event.preventDefault();
		
		var $this = $(this),
		$btn = $this.find('.wsko-feedback-submit'),
		name = $this.find('.wsko-feedback-name').val(),
		mail = $this.find('.wsko-feedback-email').val(),
		sub = $this.find('.wsko-feedback-title').val(),
		msg = $this.find('.wsko-feedback-msg').val(),
		append_reports = $this.find('.wsko-feedback-reports').is(':checked'),
		type = $('.wsko-feedback-type input:checked').val();
		
		if (type && mail && sub && msg)
			jQuery.wsko_post_element({action: 'wsko_send_feedback', name: name, email:mail, title:sub, message:msg, append_reports:append_reports, type: type, nonce:wsko_admin_data.feedback_nonce}, function(res){ $('#wsko_modal_feedback').modal('hide'); jQuery.wsko_notification(true, 'Message sent. Thank you!', ''); return true; }, false, $btn, false);
	});
	
	//Extensions
	jQuery.wsko_get_ssl = function()
	{
		return wsko_admin_data.ssl_enabled;
	}
	
	//lazy page loading
	jQuery.wsko_reload_lazy_page = function()
	{
		jQuery.wsko_load_lazy_page(wsko_admin_data.controller, wsko_admin_data.subpage, false, false, true);
	}
	
	jQuery.wsko_load_lazy_page = function(controller, subpage, tab, page_link, is_reload)
	{
		if (jQuery.wsko_is_in_page_load)
			jQuery.wsko_is_in_page_load_double = true;
		jQuery.wsko_is_in_page_load = true;
		$.wskoXhrPool.abortAll();
		jQuery.wsko_is_in_page_load_double = false;
		
		$('#wsko_admin_view_loading').show();
		var tab_states = [];
		if (is_reload)
		{
			$('.tab-pane').each(function(index){
				var $this = $(this);
				tab_states.push({id: $this.attr('id'), active:$this.hasClass('active')});
			});
		}
		
		jQuery.wsko_post_element({
				wsko_controller: controller,
				wsko_controller_sub: subpage,
				showtab: tab,
				action: 'wsko_load_lazy_page',
				nonce: wsko_admin_data.lazy_page_nonce
			},
			function(res){
				jQuery.wsko_is_in_page_load = false;
				$('#wsko_admin_view_loading').hide();
				if (res.success)
				{
					//remove previous actions
					$(document).off('wsko_init_page wsko_event_timespan_set');
					
					//Update globals
					wsko_admin_data.controller = controller;
					wsko_admin_data.subpage = subpage;
					
					//Update history
					if (page_link)
						window.history.replaceState({}, '', page_link);
					
					//Update view
					$('.wsko-admin-main-navbar-sub-panel').removeClass('wsko-active');
					$('.wsko-admin-main-navbar-item').removeClass('wsko-active');
					$('.wsko-admin-main-navbar-item[data-link="'+controller+'"]').addClass('wsko-active').find('.wsko-admin-main-navbar-sub-panel').addClass('wsko-active');
					
					$('.wsko-admin-main-sub-navbar-item').removeClass('wsko-active');
					$('.wsko-admin-main-sub-navbar-item[data-link="'+controller+"_"+subpage+'"]').addClass('wsko-active');
					
					$('#wsko_main_title').html(res.title);
					$('#wsko_main_breadcrump').html(res.breadcrump);
					
					$('#wsko_admin_view_timespan_wrapper').html(res.timespan);
					$('#wsko_admin_view_notification_wrapper').html(res.notif);
					$('#wsko_admin_view_content_wrapper').scrollTop(0);
					$('#wsko_admin_view_wrapper').html(res.view);
					$('#wsko_admin_view_content_footer_wrapper').html(res.footer);
					$('#wsko_admin_view_script_wrapper').html(res.scripts);
					
					$('#wsko_admin_ajax_notifications').html("");
					if (is_reload)
					{
						$.each(tab_states, function(k, v){
							if (v.active)
							{
								$('.nav-tabs a[href="#' + v.id + '"]').closest('li').addClass('active');
								$('#' + v.id).addClass('in active');
							}
							else
							{
								$('.nav-tabs a[href="#' + v.id + '"]').closest('li').removeClass('active');
								$('#' + v.id).removeClass('in active');
							}
						});
					}
				}
				else
				{
					jQuery.wsko_notification(false, "Page could not be loaded per AJAX! Attempting manual load...", "");
					if (page_link)
						window.location.href = page_link;
					else
						window.location.reload(true);
				}
				return true;
			}, function(){
				$('#wsko_admin_view_loading').hide();
				if (!jQuery.wsko_is_in_page_load_double)
					jQuery.wsko_notification(false, "Page could not be loaded!", "");
			}, false, false);
	}
	
	//lazy data loading
	jQuery.wsko_load_lazy_data = function($container, action, resetOld)
	{
		jQuery.wsko_load_lazy_data_for_controller($container, wsko_admin_data.controller, wsko_admin_data.subpage, action, resetOld);
	};
	jQuery.wsko_load_lazy_data_for_controller = function($container, controller, subpage, action, resetOld)
	{
		if (resetOld)
		{
			$container.find('.wsko-lazy-field').each(function(index){
				var $this = $(this);
				if ($this.data('wsko-old-html'))
					$this.html($this.data('wsko-old-html'));
			});
		}
		
		jQuery.wsko_post_element({
				wsko_controller: controller,
				wsko_controller_sub: subpage,
				wsko_action: action,
				action : 'wsko_load_lazy_data',
				nonce: wsko_admin_data.lazy_data_nonce
			}, 
			function(res)
			{
				if (res.success)
				{
					$.each(res.data, function(key, value){
						$container.find('.wsko-lazy-field[data-wsko-lazy-var="'+key+'"]').each(function(index){
							var $this = $(this);
							if (!$this.data('wsko-old-html'))
								$this.data('wsko-old-html', $this.html());
							$this.html(value);
							if ($this.data('wsko-lazy-wrapper'))
							{
								var $wrapper = $this.parents($this.data('wsko-lazy-wrapper')+'.wsko-lazy-wrapper');
								$wrapper.find('.wsko-lazy-wrapper-var').show();
								$wrapper.find('.wsko-lazy-wrapper-preview').hide();
							}
						});
					});
					
					//if (res.new_notifs)
						$('#wsko_admin_ajax_notifications').html(res.notif);
					return true;
				}
			}, false, false, false);
	}
});	