<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
use  Google\AdsApi\AdWords\AdWordsServices ;
use  Google\AdsApi\AdWords\AdWordsSessionBuilder ;
use  Google\AdsApi\Common\OAuth2TokenBuilder ;
class WSKO_AdminMenu
{
    static  $instance ;
    private  $wp_screen_prefix_nolevel = "admin_page_" ;
    private  $wp_screen_prefix_toplevel = "toplevel_page_" ;
    private  $wp_screen_prefix_sublevel = "bavoko-seo-tools_page_" ;
    public  $controller = null ;
    private  $controller_name = null ;
    private  $subpage_name = null ;
    private  $has_critical_errors = false ;
    public function __construct()
    {
        global  $pagenow ;
        if ( is_admin() ) {
            if ( WSKO_Class_Helper::check_user_permissions( false ) ) {
                
                if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
                    //AJAX Controller
                    
                    if ( isset( $_POST['wsko_controller'] ) && $_POST['wsko_controller'] ) {
                        
                        if ( $_POST['wsko_controller'] == WSKO_Controller_Dashboard::get_link( false, true ) || $_POST['wsko_controller'] == WSKO_Controller_Setup::get_link( false, true ) ) {
                            $this->get_controller( $this->wp_screen_prefix_toplevel . $_POST['wsko_controller'] );
                        } else {
                            
                            if ( $_POST['wsko_controller'] == WSKO_Controller_Download::get_link( false, true ) ) {
                                $this->get_controller( $this->wp_screen_prefix_nolevel . $_POST['wsko_controller'] );
                            } else {
                                $this->get_controller( $this->wp_screen_prefix_sublevel . $_POST['wsko_controller'] );
                            }
                        
                        }
                        
                        
                        if ( $this->controller != null ) {
                            //Check for errors
                            $this->has_critical_errors = WSKO_Class_Core::get_critical_errors();
                            if ( !$this->has_critical_errors ) {
                                //Init Controller
                                $this->controller->set_subpage( ( isset( $_POST['wsko_controller_sub'] ) ? $_POST['wsko_controller_sub'] : '' ) );
                            }
                        }
                    
                    }
                
                } else {
                    //Backend Controllers
                    if ( $pagenow == 'admin.php' ) {
                        add_action( 'current_screen', array( $this, 'load_controller' ) );
                    }
                    add_action( 'admin_menu', array( $this, 'menu_items' ) );
                    add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
                    add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts' ), 0 );
                    add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts_third_party' ), 999999 );
                    add_action( 'admin_footer', array( $this, 'admin_footer' ) );
                    add_filter(
                        'post_row_actions',
                        array( $this, 'add_content_optimizer_to_post_types' ),
                        10,
                        2
                    );
                    add_filter(
                        'page_row_actions',
                        array( $this, 'add_content_optimizer_to_post_types' ),
                        10,
                        2
                    );
                    $taxonomies = get_taxonomies( array(
                        'public' => true,
                    ), 'objects' );
                    if ( $taxonomies ) {
                        foreach ( $taxonomies as $tax ) {
                            add_action(
                                $tax->name . '_edit_form_fields',
                                array( $this, 'add_taxonomy_view' ),
                                500,
                                2
                            );
                        }
                    }
                }
            
            }
        }
    }
    
    public function add_taxonomy_view( $term )
    {
        //$term = get_term($term->term_id);
        
        if ( $term ) {
            ?><tr class="form-field">
				<th scope="row" valign="top"><label>BAVOKO SEO Tools</label><p style="opacity:.7;">Metas & Social Snippets</p></th>
				<td>
					<div class="wsko-term-meta-widget wsko_wrapper">
						<ul class="wsko-nav bsu-tabs bsu-tabs-sm">
							<li><a class="wsko-nav-link wsko-nav-link-active" href="#wsko_tax_metas_general">Metas</a></li>
							<li><a class="wsko-nav-link" href="#wsko_tax_metas_social">Social</a></li>
						</ul>
						<div class="wsko-tab-content">
							<div id="wsko_tax_metas_general" class="wsko-tab wsko-tab-active">
								<?php 
            WSKO_Class_Template::render_template( 'admin/templates/template-metas-view.php', array(
                'post_term' => $term->taxonomy . ':' . $term->term_id,
                'type'      => 'post_term',
                'meta_view' => 'metas',
            ) );
            ?>
							</div>
							<div id="wsko_tax_metas_social" class="wsko-tab">
								<?php 
            WSKO_Class_Template::render_template( 'admin/templates/template-metas-view.php', array(
                'post_term' => $term->taxonomy . ':' . $term->term_id,
                'type'      => 'post_term',
                'meta_view' => 'social',
            ) );
            ?>
							</div>
						</div>
					</div>
				</td>
			</tr><?php 
        }
    
    }
    
    public function add_content_optimizer_to_post_types( $actions, $post )
    {
        if ( in_array( $post->post_type, explode( ',', WSKO_Class_Core::get_setting( 'content_optimizer_post_types' ) ) ) ) {
            $actions['wsko_co'] = WSKO_Class_Template::render_content_optimizer_link( $post->ID );
        }
        return $actions;
    }
    
    public function load_controller()
    {
        
        if ( is_admin() ) {
            $screen = get_current_screen();
            
            if ( $screen ) {
                $this->get_controller( $screen->id );
                
                if ( $this->controller != null ) {
                    //Check for errors
                    $this->has_critical_errors = WSKO_Class_Core::get_critical_errors();
                    
                    if ( !$this->has_critical_errors ) {
                        //Init Controller Rendering
                        $this->controller_name = $this->controller->get_link( false, true );
                        if ( $this->controller->set_subpage( ( isset( $_GET['subpage'] ) ? $_GET['subpage'] : '' ) ) ) {
                            $this->subpage_name = $this->controller->get_current_subpage();
                        }
                        $this->controller->redirect();
                    }
                
                } else {
                    if ( $this->get_controller( $screen->id, true ) ) {
                        
                        if ( WSKO_Class_Core::is_configured() ) {
                            wp_redirect( WSKO_Controller_Dashboard::get_link() );
                            exit;
                        } else {
                            wp_redirect( WSKO_Controller_Setup::get_link() );
                            exit;
                        }
                    
                    }
                }
            
            }
        
        }
    
    }
    
    public function menu_items()
    {
        $is_configured = WSKO_Class_Core::is_configured();
        $is_admin = current_user_can( 'manage_options' );
        
        if ( $is_configured || $is_admin ) {
            if ( $is_configured ) {
                $hook = add_menu_page(
                    ( $is_configured ? 'WSKO - Dashboard' . WSKO_Controller_Dashboard::get_title_s() : null ),
                    'BAVOKO SEO Tools',
                    //Dont change!!!
                    'edit_posts',
                    WSKO_Controller_Dashboard::get_link( false, true ),
                    array( $this, 'render_controller' ),
                    '',
                    2
                );
            }
            $hook = add_submenu_page(
                ( $is_configured ? WSKO_Controller_Dashboard::get_link( false, true ) : null ),
                'BAVOKO SEO Tools - ' . WSKO_Controller_Dashboard::get_title_s(),
                WSKO_Controller_Dashboard::get_title_s(),
                'edit_posts',
                WSKO_Controller_Dashboard::get_link( false, true ),
                array( $this, 'render_controller' )
            );
            $hook = add_submenu_page(
                ( $is_configured ? WSKO_Controller_Dashboard::get_link( false, true ) : null ),
                'BAVOKO SEO Tools - ' . WSKO_Controller_Search::get_title_s(),
                WSKO_Controller_Search::get_title_s(),
                'edit_posts',
                WSKO_Controller_Search::get_link( false, true ),
                array( $this, 'render_controller' )
            );
            $hook = add_submenu_page(
                ( $is_configured ? WSKO_Controller_Dashboard::get_link( false, true ) : null ),
                'BAVOKO SEO Tools - ' . WSKO_Controller_Onpage::get_title_s(),
                WSKO_Controller_Onpage::get_title_s(),
                'edit_posts',
                WSKO_Controller_Onpage::get_link( false, true ),
                array( $this, 'render_controller' )
            );
            $hook = add_submenu_page(
                ( $is_configured ? WSKO_Controller_Dashboard::get_link( false, true ) : null ),
                'BAVOKO SEO Tools - ' . WSKO_Controller_Social::get_title_s(),
                WSKO_Controller_Social::get_title_s(),
                'edit_posts',
                WSKO_Controller_Social::get_link( false, true ),
                array( $this, 'render_controller' )
            );
            if ( $is_admin ) {
                $hook = add_submenu_page(
                    ( $is_configured ? WSKO_Controller_Dashboard::get_link( false, true ) : null ),
                    'BAVOKO SEO Tools - ' . WSKO_Controller_Settings::get_title_s(),
                    WSKO_Controller_Settings::get_title_s(),
                    'manage_options',
                    WSKO_Controller_Settings::get_link( false, true ),
                    array( $this, 'render_controller' )
                );
            }
            /*
            $hook = add_submenu_page(
            	$is_configured ? WSKO_Controller_Dashboard::get_link(false, true) : null,
            	'BAVOKO SEO Tools - '.WSKO_Controller_Knowledge::get_title_s(),
            	WSKO_Controller_Knowledge::get_title_s(),
            	'edit_posts',
            	WSKO_Controller_Knowledge::get_link(false, true),
            	array($this, 'render_controller')
            );
            */
        }
        
        if ( $is_admin ) {
            
            if ( $is_configured ) {
                $hook = add_submenu_page(
                    null,
                    'BAVOKO SEO Tools - ' . WSKO_Controller_Setup::get_title_s(),
                    WSKO_Controller_Setup::get_title_s(),
                    'manage_options',
                    WSKO_Controller_Setup::get_link( false, true ),
                    array( $this, 'render_controller' )
                );
            } else {
                $hook = add_menu_page(
                    'BAVOKO SEO Tools - ' . WSKO_Controller_Setup::get_title_s(),
                    'BAVOKO SEO Tools',
                    //Dont change!!!
                    'manage_options',
                    WSKO_Controller_Setup::get_link( false, true ),
                    array( $this, 'render_controller' ),
                    '',
                    2
                );
            }
        
        }
        $hook = add_submenu_page(
            null,
            'BAVOKO SEO Tools - ' . WSKO_Controller_Download::get_title_s(),
            WSKO_Controller_Download::get_title_s(),
            'manage_options',
            WSKO_Controller_Download::get_link( false, true ),
            array( $this, 'render_controller' )
        );
        $hook = add_submenu_page(
            null,
            'BAVOKO SEO Tools - Update',
            'Update',
            'edit_posts',
            'wsko_updated',
            array( $this, 'render_updated_page' )
        );
    }
    
    public function load_scripts( $hook )
    {
        global  $wsko_plugin_url, $wsko_plugin_path ;
        $script_data = array(
            'ajaxurl'                         => admin_url( 'admin-ajax.php' ),
            'content_optimizer_nonce'         => wp_create_nonce( 'wsko_get_content_optimizer' ),
            'add_monitoring_keyword_nonce'    => wp_create_nonce( 'wsko_add_monitoring_keyword' ),
            'remove_monitoring_keyword_nonce' => wp_create_nonce( 'wsko_remove_monitoring_keyword' ),
            'knowledge_base_nonce'            => wp_create_nonce( 'wsko_get_knowledge_base_article' ),
            'is_configured'                   => WSKO_Class_Core::is_configured(),
            'ssl_enabled'                     => WSKO_Class_Helper::ssl_enabled(),
        );
        //Local
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'jquery-ui-core' );
        wp_enqueue_script( 'jquery-ui-draggable' );
        wp_enqueue_script( 'jquery-ui-droppable' );
        wp_enqueue_script( 'jquery-ui-sortable' );
        wp_enqueue_script( 'jquery-effects-core' );
        wp_enqueue_script(
            'wsko_core_js',
            $wsko_plugin_url . 'admin/js/core.js',
            array(),
            WSKO_VERSION
        );
        wp_localize_script( 'wsko_core_js', 'wsko_data', $script_data );
        wp_enqueue_script(
            'wsko_post_js',
            $wsko_plugin_url . 'admin/js/post-widget.js',
            array(),
            WSKO_VERSION
        );
        wp_enqueue_style( 'font_awesome', $wsko_plugin_url . 'includes/font-awesome/css/font-awesome.min.css' );
        wp_enqueue_style(
            'wsko_roboto',
            $wsko_plugin_url . 'admin/css/fonts.css',
            array(),
            WSKO_VERSION
        );
        wp_enqueue_script( 'jquery_datatables', $wsko_plugin_url . 'includes/datatables/datatables.min.js' );
        //wp_enqueue_style('jquery_datatables_css', $wsko_plugin_url . 'includes/datatables/datatables.min.css');
        wp_enqueue_style( 'jquery_datatables_bootstrap', $wsko_plugin_url . 'includes/datatables/datatables.bootstrap.min.css' );
        wp_enqueue_script( 'jquery_datatables_bootstrap', $wsko_plugin_url . 'includes/datatables/datatables.bootstrap.min.js' );
        
        if ( $this->controller ) {
            //Materialize
            wp_enqueue_script(
                'wsko_materialize',
                $wsko_plugin_url . 'includes/materialize/js/materialize.js',
                array(),
                WSKO_VERSION
            );
            wp_enqueue_style(
                'wsko_materialize',
                $wsko_plugin_url . 'includes/materialize/css/materialize.css',
                array(),
                WSKO_VERSION
            );
            wp_enqueue_script(
                'wsko_materialize_nouislider',
                $wsko_plugin_url . 'includes/materialize/extras/noUiSlider/nouislider.js',
                array(),
                WSKO_VERSION
            );
            wp_enqueue_style(
                'wsko_materialize_nouislider',
                $wsko_plugin_url . 'includes/materialize/extras/noUiSlider/nouislider.css',
                array(),
                WSKO_VERSION
            );
            $this->controller->enqueue_scripts();
        }
    
    }
    
    public function load_scripts_third_party( $hook )
    {
        global  $wsko_plugin_url, $wp_scripts ;
        $has_g_charts = false;
        //$has_materialize = false;
        $has_bootstrap_js = false;
        $has_bootstrap_css = false;
        $has_moment = false;
        $has_b_datepicker = false;
        $has_roboto = false;
        foreach ( $wp_scripts->queue as $handle ) {
            if ( stripos( $wp_scripts->registered[$handle]->src, 'wp-seo-keyword-optimizer' ) !== false ) {
                continue;
            }
            if ( stripos( $wp_scripts->registered[$handle]->src, 'gstatic.com/charts/loader.js' ) !== false ) {
                $has_g_charts = true;
            }
            //if (stripos($wp_scripts->registered[$handle]->src, '/materialize.js') !== false || stripos($wp_scripts->registered[$handle]->src, '/materialize.min.js') !== false)
            //$has_materialize = true;
            if ( stripos( $wp_scripts->registered[$handle]->src, '/bootstrap.js' ) !== false || stripos( $wp_scripts->registered[$handle]->src, '/bootstrap.min.js' ) !== false ) {
                $has_bootstrap_js = true;
            }
            if ( stripos( $wp_scripts->registered[$handle]->src, '/bootstrap.css' ) !== false || stripos( $wp_scripts->registered[$handle]->src, '/bootstrap.min.css' ) !== false ) {
                $has_bootstrap_css = true;
            }
            if ( stripos( $wp_scripts->registered[$handle]->src, '/moment.js' ) !== false || stripos( $wp_scripts->registered[$handle]->src, '/moment.min.js' ) !== false ) {
                $has_moment = true;
            }
            if ( stripos( $wp_scripts->registered[$handle]->src, '/bootstrap-daterangepicker/' ) !== false ) {
                $has_b_datepicker = true;
            }
            if ( stripos( $wp_scripts->registered[$handle]->src, 'fonts.googleapis.com/css?family=Roboto' ) !== false ) {
                $has_roboto = true;
            }
        }
        wp_enqueue_style(
            'wsko_core_css',
            $wsko_plugin_url . 'admin/css/core.css',
            array(),
            WSKO_VERSION
        );
        
        if ( $this->controller ) {
            //CDN
            if ( !$has_g_charts ) {
                wp_enqueue_script( 'google_charts', 'https://www.gstatic.com/charts/loader.js' );
            }
            //3rd Party
            if ( !$has_bootstrap_js ) {
                wp_enqueue_script( 'bootstrap', $wsko_plugin_url . 'includes/bootstrap/js/bootstrap.min.js' );
            }
            if ( !$has_bootstrap_css ) {
                wp_enqueue_style( 'bootstrap', $wsko_plugin_url . 'includes/bootstrap/css/bootstrap.min.css' );
            }
            if ( !$has_moment ) {
                wp_enqueue_script( 'moment', $wsko_plugin_url . 'includes/moment/moment.js' );
            }
            
            if ( !$has_b_datepicker ) {
                wp_enqueue_script( 'bootstrap_datepicker', $wsko_plugin_url . 'includes/bootstrap-daterangepicker/daterangepicker.js' );
                wp_enqueue_style( 'bootstrap_datepicker', $wsko_plugin_url . 'includes/bootstrap-daterangepicker/daterangepicker.css' );
            }
            
            //Roboto Font
            if ( !$has_roboto ) {
                wp_enqueue_style(
                    'wsko_roboto',
                    $wsko_plugin_url . 'admin/css/fonts.css',
                    array(),
                    WSKO_VERSION
                );
            }
            $this->controller->enqueue_styles();
        }
    
    }
    
    public function render_controller()
    {
        
        if ( $this->controller ) {
            global  $wsko_plugin_path, $wsko_plugin_url ;
            
            if ( $this->has_critical_errors ) {
                ?><div id="wsko_admin_critical_error_wrapper" class="wsko-display-none-fix"><?php 
                global  $wp_version ;
                if ( isset( $this->has_critical_errors['incompatible_php'] ) && $this->has_critical_errors['incompatible_php'] ) {
                    WSKO_Class_Template::render_notification( 'error', array(
                        'msg'     => "You are running PHP " . (( PHP_VERSION ? PHP_VERSION : '(unversioned)' )) . ", which is not compatible with BAVOKO SEO Tools. We recommend you to update your PHP version to 7.x for a faster and more secure WordPress site!",
                        'subnote' => 'There are still WordPress plugins not working with PHP 7.x. If you can’t renounce them, you should at least update to PHP 5.6.',
                    ) );
                }
                if ( isset( $this->has_critical_errors['incompatible_wp'] ) && $this->has_critical_errors['incompatible_wp'] ) {
                    WSKO_Class_Template::render_notification( 'error', array(
                        'msg' => "You are running WP " . (( $wp_version ? $wp_version : '(unversioned)' )) . " which is not compatible with BAVOKO SEO Tools. We strongly recommend to update your WordPress!",
                    ) );
                }
                if ( isset( $this->has_critical_errors['content_dir_access'] ) && $this->has_critical_errors['content_dir_access'] ) {
                    WSKO_Class_Template::render_notification( 'error', array(
                        'msg'  => "Your Content Directory (" . WP_CONTENT_DIR . ") is not writable!",
                        'note' => "You can create a folder 'bst' in your content directory to skip this requirement.",
                    ) );
                }
                if ( isset( $this->has_critical_errors['bst_dir_access'] ) && $this->has_critical_errors['bst_dir_access'] ) {
                    WSKO_Class_Template::render_notification( 'error', array(
                        'msg'  => "BST can't access its folder 'bst' in your content directory (" . WP_CONTENT_DIR . ")!",
                        'list' => 'You will need to give write and read permission to WordPress on this folder.',
                    ) );
                }
                WSKO_Class_Template::render_notification( 'error', array(
                    'msg' => "Handle the errors above to gain access to the plugin",
                ) );
                ?></div><?php 
            } else {
                
                if ( !WSKO_Class_Core::is_configured() ) {
                    ?><div class="wsko-display-none-fix"><?php 
                    
                    if ( current_user_can( 'manage_options' ) ) {
                        $this->controller->view();
                        $this->controller->get_scripts();
                        global  $wsko_render_modals ;
                        $wsko_render_modals = true;
                    } else {
                        WSKO_Class_Template::render_notification( 'error', array(
                            'msg' => 'Only an Admin can setup the plugin!',
                        ) );
                    }
                    
                    ?></div><?php 
                } else {
                    global  $wsko_render_modals ;
                    $wsko_render_modals = true;
                    ?>
				<div id="wsko_admin_view_body" class="wsko_wrapper mobile-navi wsko-display-none-fix">
					<div id="wsko_admin_view_topbar">
						<div id="wsko_admin_view_topbar_icon" class="wsko_important_nav">
							<div class="wsko_logo">	<img src="<?php 
                    echo  $wsko_plugin_url . 'admin/img/logo.png' ;
                    ?>" /></div> BAVOKO SEO Tools <small class="text-off" style="color:#fff;">BETA</small>
							
						</div>
						<div id="wsko_admin_view_topbar_header">
							<span class="mobile-navi-toggle" style="display:none;"><a href="#" class="menu"><i class="fa fa-bars fa-fw fa-2x dark"></i></a></span>
							<div style="display:inline-block;margin-left:30px;">
								<h2 id="wsko_main_title"><?php 
                    echo  $this->controller->get_title() ;
                    ?></h2>
								<span id="wsko_main_breadcrump"><?php 
                    echo  $this->controller->get_breadcrump() ;
                    ?></span>
							</div>
							<div class="wsko_social_wrapper" style="display: inline-block; padding:10px; float:right;">
								<div id="wsko_admin_view_timespan_wrapper">
								<?php 
                    if ( $this->controller->uses_timespan ) {
                        WSKO_Class_Template::render_timespan_widget();
                    }
                    ?>
								</div>
							</div>
						</div>
					</div>
					<div id="wsko_admin_view_sidebar" class="wsko_important_nav">
						<div id="wsko_admin_view_sidebar_overlay" class="wsko-admin-main-navbar">
							<?php 
                    $controllers = array(
                        'WSKO_Controller_Dashboard',
                        'WSKO_Controller_Search',
                        'WSKO_Controller_Onpage',
                        'WSKO_Controller_Social'
                    );
                    if ( current_user_can( 'manage_options' ) ) {
                        $controllers[] = 'WSKO_Controller_Settings';
                    }
                    //$controllers[]= 'WSKO_Controller_Knowledge';
                    WSKO_Class_Template::render_main_navigation( $controllers, $this->controller_name, $this->subpage_name );
                    ?>
						</div>	
						<?php 
                    /* if (wsko_fs()->is_not_paying())
                    			{ 
                    				?><div style="position:absolute;bottom:20px;right:20px;">
                    					<a href="<?=wsko_fs()->get_upgrade_url()?>">Upgrade Now!</a>
                    				</div><?php
                    			} */
                    ?>
						
						<div class="wsko-sidebar-info-links">
							<ul>						
								<li><a class="wsko-give-feedback wsko-white wsko-inline-block">Support</a></li>
								<li><a href="https://www.bavoko.tools/knowledge_base/" target="_blank" class="wsko-white wsko-inline-block">Knowledge Base</a></li>
								<?php 
                    /* <a class="wsko-give-rating wsko-white wsko-inline-block">Rate</a> <span class="wsko-white mr5 ml5">•</span> */
                    ?>
							</ul>	
						</div>
					</div>
					<div id="wsko_admin_view_content">
						<div id="wsko_admin_view_ajax_notification"></div>
						<div id="wsko_admin_view_content_wrapper">
							<div id="wsko_notification_wrapper">
								<div id="wsko_admin_view_notification_wrapper" style="margin: auto 15px;">
									<?php 
                    $this->controller->notifications();
                    ?>
								</div>
								<div class="wsko_notification" id="wsko_admin_ajax_notifications"></div>
							</div>	
							<div id="wsko_admin_view_wrapper">
								<?php 
                    $this->controller->view();
                    ?>
							</div>
							<?php 
                    /*
                    <div id="wsko_admin_view_content_footer">
                    	<a class="waves-effect btn-flat" target="_blank" href="https://www.bavoko.services/wordpress/wsko-wordpress-seo-plugin/"><i style="padding-right:5px;" class="fa fa-info-circle" aria-hidden="true"></i> BST Documentation</a>
                    	<a class="waves-effect btn-flat" target="_blank" href="https://wordpress.org/support/plugin/wp-seo-keyword-optimizer"><i style="padding-right:5px;" class="fa fa-user" aria-hidden="true"></i> Support</a>
                    	<a class="waves-effect btn-flat wsko-give-feedback" href="#"><i style="padding-right:5px;" class="fa fa-comments" aria-hidden="true"></i> Feedback</a>
                    	<div id="wsko_admin_view_content_footer_wrapper"><?php $this->controller->content_footer(); ?></div>
                    </div>
                    <div class="fixed-action-btn">
                      <a class="btn-floating btn-floating-main">
                    	<i class="fa fa-question"></i>
                      </a>
                      <ul>
                    	<li data-toggle="tooltip" data-tooltip="I am a tooltip"><a class="btn-floating yellow darken-2"><i class="fa fa-university"></i></a></li>
                    	<li><a class="btn-floating green wsko-give-feedback"><i class="fa fa-commenting"></i></a></li>
                    	<li><a class="btn-floating blue"><i class="fa fa-thumbs-up"></i></a></li>
                      </ul>
                    </div>
                    */
                    ?>
							
							<div id="wsko_admin_view_loading">
								<div id="wsko_admin_view_loading_background"></div>
								<?php 
                    echo  WSKO_Class_Template::render_preloader( array(
                        'size' => 'big',
                    ) ) ;
                    ?> 
							</div>
							
							<p class="hidden-xs" style="text-align: right; padding: 0px 20px 0px 0px; max-width: 1600px; opacity: .7; margin:15px auto;">Made with <i class="fa fa-heart fa-fw"></i> in Berlin, Germany<p>
						</div>
					</div>
				</div>
				
				<div id="wsko_admin_view_script_wrapper">
					<?php 
                    $this->controller->get_scripts();
                    ?>
				</div><?php 
                }
            
            }
        
        } else {
            ?><div class="wsko-display-none-fix"><?php 
            WSKO_Class_Template::render_notification( 'error', array(
                'msg' => 'Controller could not be loaded!',
            ) );
            ?></div><?php 
        }
    
    }
    
    public function render_updated_page()
    {
        WSKO_Class_Template::render_template( 'admin/templates/info/frame-info.php', array() );
    }
    
    public function add_meta_boxes()
    {
        
        if ( WSKO_Class_Core::get_setting( 'activate_content_optimizer' ) ) {
            $wsko_post_types = get_post_types( array(), 'names' );
            foreach ( $wsko_post_types as $type ) {
                add_meta_box(
                    'wsko_post_metabox',
                    'BAVOKO SEO Tools',
                    array( &$this, 'post_meta_box_view' ),
                    $type,
                    'normal',
                    'high'
                );
            }
        }
    
    }
    
    public function post_meta_box_view()
    {
        global  $pagenow ;
        
        if ( $pagenow != 'post.php' ) {
            ?><p>Content Optimizer is available after first save.</p><?php 
            return;
        }
        
        global  $post ;
        
        if ( $post ) {
            ?><script type="text/javascript">
				jQuery(document).ready(function($){
					<?php 
            
            if ( is_plugin_active( 'js_composer/js_composer.php' ) && get_post_meta( $post->ID, '_wpb_vc_js_status', true ) ) {
                ?>$('#wsko_post_metabox').addClass('wsko-metabox-vc').insertAfter('#titlediv');<?php 
            } else {
                ?>$('#wsko_post_metabox').insertBefore('#wp-content-media-buttons');<?php 
            }
            
            ?>
					var changeTimout,
					post_title,
					post_content,
					post_slug;
					function wsko_reload_content_optimizer()
					{
						if (changeTimout)
							clearTimeout(changeTimout);
						
						changeTimout = setTimeout(function(){
							$('#wsko_content_optimizer_ajax_reload_overlay').show();
							var was_open = $('.wsko-co-widget').length && !$('.wsko-co-widget').hasClass('wsko-short-view-active');
							jQuery.wsko_post_element({action: 'wsko_get_content_optimizer', post: <?php 
            echo  $post->ID ;
            ?>, widget:true, preview:((post_title||post_content||post_slug) ? true : false), post_title: post_title, post_content:post_content, post_slug:post_slug ,nonce: wsko_data.content_optimizer_nonce}, 
								function(res){
									$('#wsko_content_optimizer_ajax_reload_overlay').hide();
									if (res.success)
									{
										$('#wsko_content_optimizer_ajax_reload_wrapper').html(res.view);
										if (was_open)
										{
											$('.wsko-co-widget').removeClass('wsko-short-view-active');
										}
										return true;
									}
									else
									{
										$('#wsko_content_optimizer_ajax_reload_wrapper').html("Content Optimizer could not be generated. Please try again");
									}
								},
								function()
								{
									$('#wsko_content_optimizer_ajax_reload_wrapper').html("A Server Error occured. Please try again.");
									$('#wsko_content_optimizer_ajax_reload_overlay').hide();
								}, false, false);
						}, 1000);
					}
					$('#titlewrap input[name="post_title"]').change(function(){
						post_title = $(this).val();
						wsko_reload_content_optimizer();
					});
					$('#wp-content-editor-container .wp-editor-area').change(function(){
						post_content = $(this).val();
						wsko_reload_content_optimizer();
					});
					$('#edit-slug-buttons .save.button').click(function(event){
						post_slug = $('#editable-post-name').text();
						wsko_reload_content_optimizer();
					});
					setTimeout(function(){ //late bind tinymce
						for (var i = 0; i < tinymce.editors.length; i++) {
							tinymce.editors[i].onChange.add(function (ed, e) {
								post_content = ed.getContent();
								wsko_reload_content_optimizer();
							});
						}
						wsko_reload_content_optimizer();
					}, 1000);
				});
			</script>
			
			<div id="wsko_content_optimizer_ajax_reload_wrapper">
				<?php 
            //echo WSKO_Controller_Optimizer::render($post->ID, true);
            ?>
			</div>
			<div id="wsko_content_optimizer_ajax_reload_overlay" style="position:absolute;top:0px;left:0px;width:100%;height:100%;background-color:white;text-align:center;padding-top:10px;z-index:99;display:none;">
				<i class="fa fa-spinner fa-pulse"></i> Loading
			</div>
			<?php 
        }
    
    }
    
    function admin_footer()
    {
        /*global $post, $pagenow;
        		if ($pagenow != 'post.php')
        			return;*/
        WSKO_Class_Template::render_template( 'admin/templates/modal-content-optimizer.php', array() );
        WSKO_Class_Template::render_template( 'admin/templates/modal-knowledge-base-article.php', array() );
        global  $wsko_render_modals ;
        
        if ( $wsko_render_modals ) {
            WSKO_Class_Template::render_template( 'admin/templates/modal-feedback.php', array() );
            //WSKO_Class_Template::render_template('admin/templates/modal-rating.php', array());
        }
    
    }
    
    private function get_controller( $name, $switched = false )
    {
        $this->controller = null;
        
        if ( WSKO_Class_Core::is_configured() ) {
            
            if ( $switched ) {
                if ( $name == $this->wp_screen_prefix_nolevel . WSKO_Controller_Setup::get_link( false, true ) ) {
                    return true;
                }
                return false;
            } else {
                switch ( $name ) {
                    case $this->wp_screen_prefix_toplevel . WSKO_Controller_Dashboard::get_link( false, true ):
                        $this->controller = WSKO_Controller_Dashboard::get_instance();
                        break;
                    case $this->wp_screen_prefix_sublevel . WSKO_Controller_Search::get_link( false, true ):
                        $this->controller = WSKO_Controller_Search::get_instance();
                        break;
                    case $this->wp_screen_prefix_sublevel . WSKO_Controller_Social::get_link( false, true ):
                        $this->controller = WSKO_Controller_Social::get_instance();
                        break;
                    case $this->wp_screen_prefix_sublevel . WSKO_Controller_Onpage::get_link( false, true ):
                        $this->controller = WSKO_Controller_Onpage::get_instance();
                        break;
                    case $this->wp_screen_prefix_sublevel . WSKO_Controller_Settings::get_link( false, true ):
                        $this->controller = WSKO_Controller_Settings::get_instance();
                        break;
                    case $this->wp_screen_prefix_sublevel . WSKO_Controller_Knowledge::get_link( false, true ):
                        $this->controller = WSKO_Controller_Knowledge::get_instance();
                        break;
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Download::get_link( false, true ):
                        $this->controller = WSKO_Controller_Download::get_instance();
                        break;
                }
            }
        
        } else {
            
            if ( $switched ) {
                switch ( $name ) {
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Dashboard::get_link( false, true ):
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Search::get_link( false, true ):
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Social::get_link( false, true ):
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Onpage::get_link( false, true ):
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Settings::get_link( false, true ):
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Knowledge::get_link( false, true ):
                    case $this->wp_screen_prefix_nolevel . WSKO_Controller_Download::get_link( false, true ):
                        return true;
                }
                return false;
            } else {
                if ( $name == $this->wp_screen_prefix_toplevel . WSKO_Controller_Setup::get_link( false, true ) ) {
                    $this->controller = WSKO_Controller_Setup::get_instance();
                }
            }
        
        }
        
        return $this->controller;
    }
    
    /*Singleton*/
    public static function get_instance()
    {
        if ( !isset( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}