=== BAVOKO SEO Tools - All-in-One WordPress SEO ===
Author URI: https://www.bavoko.tools
Plugin URI: https://www.bavoko.tools
Contributors: BAVOKO Tools
Tags: seo, google search console, onpage analysis, content optimization, social media
Tested up to: 4.9.6
Requires PHP: 5.5
Requires at least: 4.4
Stable tag: 2.0.9

Most extensive SEO Plugin: Google Ranking (Keywords), Onpage (Content) & Social Media Analysis and Optimization + Numerous SEO Tools.

== Description ==

BAVOKO SEO Tools is not only the most comprehensive SEO plugin with extensive data and powerful tools within the WordPress backend, but also the first one combining SEO analysis and optimization in a unique workflow.

[youtube https://www.youtube.com/watch?v=_s-HcePDwww ]

**Features and benefits at a glance:**

**Rankings**

* Google Search Console data in your WordPress backend
* More Data: Save up to 5,000 Keywords in your cache every day through Google’s API
* Comprehensive ranking analysis with clear charts and tables
* Keyword monitoring for your most important search terms

**Onpage**

* Configurable website crawler
* Extensive onpage analysis with tidy charts and tables
* More tools: Dynamic Metas, Bulk Metas Tool, Permalinks, Sitemap & Redirect Manager, .htaccess & robots.txt Editor

**Content**

* Optimized SEO workflow thanks to the intelligent plugin architecture
* Real time SEO Optimization Suggestions
* 2 SEO keywords per page (up to 10 in PRO)
* Numerous tools in the Content Optimizer: Metas, social snippets, page ranking analysis & more

**Social**

* Facebook & Twitter monitoring
* Dynamic social snippets for post types
* Facebook and Twitter snippets analysis

For further information please visit [BAVOKO.tools](https://www.bavoko.tools/)

**BAVOKO SEO Tools opens up completely new Possibilities for your WordPress SEO**

With BAVOKO SEO Tools, we not only set ourselves the goal of illustrating your most important SEO data within the WordPress backend, but also of simplifying the entire workflow for the following editing and optimization of your pages. Within the analysis views for rankings, onpage and social, the Content Optimizer allows you to edit the content and all relevant SEO settings of your individual posts in the same view, thereby saving valuable time. In addition to the extensive analysis and optimization options in BAVOKO SEO Tools Free, we also provide you with numerous other SEO tools, such as the dynamic metas, the Sitemaps Manager, or the Redirect Manager.

**Ranking analysis based on Google Search Console data**

Connect your Google Search Console Property to WordPress with our SEO Plugin to save up to 5,000 keywords per day in your backend and do extensive analysis with a larger dataset and clear charts.

More Advantages & Features

* Individually customizable analysis periods
* Ranking history in descriptive charts
* Analyze clicks, impressions, positions and CTR for page & keyword rankings in tables with specialized filtering options
* Seperate views for new and lost ranking keywords or pages
* Easily analyze your main keywords with the SEO Monitoring Tool

**Onpage**

The Onpage section of BAVOKO SEO Tools gives you access to a range of powerful tools for detailed tracking and quick editing of all the content aspects of your WordPress search engine optimization.

More Advantages & Features:

* Weekly Onpage Crawl
* Detailed analysis of important, content-related SEO aspects such as titles, descriptions, keyword density, headlines and more in descriptive charts & filterable tables
* Any improvement in onpage optimization can be made directly in any analysis view using the Content Optimizer
* Dynamic Metas Tool: Automatically generated meta titles and descriptions for each post type
* Bulk Metas Tool: Optimize tiles, descriptions, meta robots, URLs & Priority Keywords for different pages in the same view
* Redirect Manager: Manually set up 301 & 302 redirects for pages and directories, or establish automatic redirects of error pages
* Permalinks: Important permalink settings and automatic URL redirects
* Sitemaps: Easily generate a sitemap of your website that that will be automatically sent to Google and Bing after any change on your website, to guarantee a faster indexing
* Quick access to our .htaccess & robots.txt Editor

**Content Optimization based on your Onpage & Ranking Data**

With the Content Optimizer in our WordPress plugin BAVOKO SEO Tools, you have access to the most comprehensive SEO toolbox for optimizing your individual pages. It can be entered via the posts in each post type, as well as in a pop-up window within each analysis view. This allows you to open the Content Optimizer within the page tables with one click and to edit the respective post during the ranking, onpage or social analysis. You can then save the changes and edit the next page immediately afterwards to save valuable time and to foucs on the essential SEO tasks.
BAVOKO SEO Tools Free allows you to optimize each of your pages for 2 Priority Keywords (up to 10 Priority Keywords in the PRO version), which you can easily assign during the analysis with one click. In addition to the automatic SEO suggestions, that are determined based on your ranking and onpage data, BAVOKO SEO Tools also contains numerous other tools for your metas, social snippets, rankings and many more.

**Social**

Within BAVOKO SEO Tools you have the opportunity to analyze your Social Performance on Facebook and Twitter in clear charts.
Easily create Social Snippets for your individual pages or posts and also analyze, which of your social snippets still have potential for optimization. You can make any of these changes directly in the same view using the Content Optimizer.

**BAVOKO SEO Tools PRO**

In April 2018 we are going to publish the PRO version of our WordPress SEO plugin, which not only provides enhanced data sets, but also easy access to numerous other tools for your professional search engine optimization.

**The PRO version also offers the following Data & Features:**

**Search:** Search volume data, various keyword research tools (exact & similar keywords, WDF * IDF, full analysis), SEO competitor monitoring

**Content:** 10 priority keywords, more tools & data in the Content Optimizer

**Onpage:** Onpage crawl through our external BAVOKO crawler, additional technical & structural onpage analysis, semi-automated Internal Links Manager

**Performance:** Unique Theme, Plugin & Script Monitoring, as well as the ability to disable unnecessary requests on your pages to increase loading speed

**Backlinks:** Extensive backlink analysis of your own domain, as well as an integrated Spam & Disavow tool to notify Google about spam links.

With BAVOKO SEO Tools Free you already have access to the most extensive SEO Toolset available for WordPress, with which you can analyze your website - no matter the size - and optimize it for the search engines. Within the PRO version however, we not only offer more extensive data for a detailed SEO Analysis with the BAVOKO API, but also numerous other tools for a professional and comprehensive search engine optimization.


== Installation ==

= Installing the plugin via WordPress =
1. Sign in to WordPress with your admin account.
2. Navigate to plugins - install in your menu.
3. In the section search for plugins you can now find BAVOKO SEO Tools.
4. Clicking the install button starts the WordPress installation of your plugin.
5. After the installation you can activate your new software by clicking the activate now button. You also can activate the plugin afterwards by navigating to plugins - installed plugins.

= Installing the plugin via FTP =
If you have trouble installing the plugin via your WordPress admin section, you can also try to start the installation via FTP. Therefore you need an FTP program.
You can download the zip file containing the installation folder on our website.

1. Download the installation file of BAVOKO SEO Tools and save it on your PC.
2. Unpack the zip archive on your harddrive.
3. Use your FTP program to establish a connection to your web space.
4. Upload the installation file in the directory wp-content/plugins in your WordPress installation folder.
5. You can now activate the plugin with WordPress by navigating to plugins - installed plugins


== Frequently Asked Questions ==

Please read our [Documentation](https://www.bavoko.tools/knowledge_base/) for more information.

Requirements:

PHP 5.5+
WordPress 4.4+
SQL Database (Create Table permission)

== Screenshots ==

1. Example Workflow - Onpage > Title Analysis - Filter by too short Titles - Edit Pages to Optimize your Titles with 1 Click
2. Search Dashboard - Connect WordPress with your Google Search Console Property and analyze all data in your backend
3. Ranking Analysis - Analyze your rankings by pages and keywords in sortable / filterable tables
4. Analysis & Optimization - Optimize your pages with BAVOKO's Content Optimizer while analyzing your pages
5. Onpage Analysis - Analyze all important SEO aspects for your content in different charts and tables
6. Onpage Analysis - Meta Descriptions Example
7. Optimize your content - Automatic content suggestions and numerous SEO tools in the Content Optimizer
8. All SEO Tools you need - Get with BAVOKO SEO Tools the biggest SEO toolset - Sitemaps, Redirects, Dynamic Metas, Social Snippets and more!


== Changelog ==

= 2.0.9 =
* GDPR/Hotfix Update
* Fixed: Onpage history lost on next crawl
* Fixed: Onpage title/description/content length miscalulation with multi-byte characters
* Other: GDPR related changes (eg. cloud logging removed and google font changed to local storage)

= 2.0.8 =
* Hotfix: Minor stablelization update
* Fixed: Timespan widget related bugs
* Fixed: Some Table-Filters not working
* Fixed: Special chars in onpage metas being displayed with encoding errors
* Other: Other bug fixes, minor text changes and new notifications

= 2.0.7 =
* Hotfix: Minor stablelization update
* Fixed: Search field still not searching correctly and requests being sent too frequently
* Other: Text changes and minor bug fixes

= 2.0.6 =
* Added: Site name and tagline to dynamic metas
* Added: Edited flag to Onpage Crawl and reference between Crawl Score and Current Score
* Fixed: Search field not searching in data tables
* Fixed: Errors with Sitemap post exlusion in the Content Optimizer
* Fixed: Missing files from the last update are now included
* Fixed: Word Count being way to high with null bytes and white space
* Fixed: Content Optimizer view bug when there are no clicks
* Fixed: Search charts date ordering not working properly
* Changed: Split Sitemap generation and Google/Bing ping in seperate (hierarchical) settings and added dirty flag
* Changed: Grouped canonical post-id and link option
* Changed: Removed decimal places from position values in all Search related tables
* Removed: Facebook API, because Facebook decided to break everything with the last API update (we are working on a new implementation)
* Other: Minor bug fixes

= 2.0.5 =
* Added: Permlink Settings and "Category Base Hide" Option
* Added: Facebook/Twitter API and corresponding views under "Social"
* Added: Backup Import/Export System
* Added: Translation system
* Added: Locale "German"
* Added: Links to Social posts and pages
* Added: Background error reporting for opted in users
* Added: Enter Submit for "Monitoring Keywords" subpage
* Added: Current redirects view to Content Optimizer
* Added: Absolute reference values to all Search tables (tooltip)
* Added: Context to all Content Optimizer links (ex: opening the CO of a post in the "Priority Keywords" Table under "Onpage Analysis" will show the "Keywords" tab first, instead of "Overview")
* Added: Dynamic Metas search field for post attribute placeholders
* Added: Automatic redirect option for post slug changes
* Fixed: Onpage "Post Type"-Filter will now filter, when it is selected first
* Fixed: Lightweight Session Cache and Real Session Cache were switched
* Fixed: "Clear Session Cache" not working properly
* Fixed: Untitled posts not having a name in some views (duh)
* Fixed: Encoding errors with an Umlaut in the post slug
* Fixed: Conten Optimizer bugs when collapsed/collapsing
* Changed: Dashboard Style overhaul with new tables and charts (mainly Social and Onpage)
* Changed: 404 to 301 Settings are now under "Onpage/Permalinks"
* Changed: Removed "H1 Priority Keyword Density" issued and grouped both "H2 word count" togehter + more changes
* Changed: Default timespan
* Removed: "Your Content has HTML Errors" tooltip
* Removed: "WSKO has been updated" page
* Removed: Sug options for "post" and "page" post types, as they have no base
* Other: Minor Bug fixes and new Icons/Links

= 2.0.4 =
* Added: Register of Page redirects in Redirect Manager
* Added: Single Term Metas/Snippets and corresponding placeholders for the taxonomy and term snippets
* Added: Permalink section to Onpage Analysis
* Added: Configuration Backup-System, so you never loose your metas
* Added: Auto-Snippet and Post Thumbnail setting for your social metas (see Settings)
* Added: Lightweight Session Cache, which doesn't rely on PHPs Session system, but performs way slower (tweaking in progress)
* Added: Content Optimizer short view resize and new toggle functionality (set size will be saved per user, toggle up to have a better view on your post content)
* Fixed: Ajax-Inputs having wrong state, when AJAX is not successful
* Fixed: Content optimizer having an input with the attribute "required"
* Fixed: Word Count calculation
* Fixed: Possible wrong order in Search histories
* Fixed: Content-Score calculation for posts without keywords, post thumbnail alt-attribute, video-iframes as media elements (youtube, vimeo and dailymotion) and errors with heading calculations (h1,...)
* Fixed: 3rd party library conflicts
* Changed: The default meta description is now empty (instead of the post content), which will have Google pick a description if nothing is set and fixes errors with Enfold's Avia Builder Shortcodes
* Changed: Settings page structure
* Removed: Non-Latin Mode, because word count has been fixed
* Disabled: Customizable Content Optimizer timerange
* Other: Many bug fixes and minor changes

= 2.0.3 =
* Added: More error reports for potential authentication errors (use the support modal and append error reports to speedup the support process)
* Added: Support Button to all warning/error notifications
* Added: Error Reports - Clear button (reports will be auto removed after 7 days)
* Fixed: Visual Composer compatibility error
* Fixed: qTranslate-X compatibility error (integration coming soon)
* Fixed: Tabs switching to default, when reloading an admin page with AJAX

= 2.0.2 =
* Added: Error Reporting 2.0 - send your errors directly within the support modal
* Added: Onpage Analysis "Post Type" filter to every table
* Added: Sublabels to post types and roles in case they share the same label
* Added: Various help text and style elements
* Added: New notifications for Dashboard
* Added: Support buttons to setup page
* Changed: Keyword density for multiple words (longtail) will not take special chars between words such as ",.-_" and space into account
* Changed: "Disable Seo plugins" message
* Changed: Stacked progress bars width rounding calculation has recieved an additional digit to further blur the rounding errors
* Fixed: Redirect Manager "Help Text" message removed
* Fixed: Keywords not adding to both Content Optimizer view modes
* Fixed: CronJob Error Notification for Onpage Analysis
* Fixed: JS draggable error if it has been dequeued
* Fixed: Some Filter-Charts in the Onpage Analysis not filtering
* Fixed: Search - Monitoring columns Clicks and Position switched
* Other: Added Beta header

= 2.0.1 =
* Hotfix: Dev Switch for Search API was still enabled

= 2.0.0 (beta) =
* Beta Update - WSKO just became bigger...and "BST"
* Changed: Entire structure and design
* Added: Page Dasshboard - A quick overview of your current data and the newly added sections
* Added: Page Onpage - Tool-Group to optimize your Onpage SEO (Meta Titles, Sitemap, Redirects,...)
* Added: Page Social - Connect your Facebook and Twitter account with your WordPress to get even more data! (will be even more later)
* Added: AJAX loading system to smooth the workflow within our plugins pages
* Added: Session cache to increase performance
* Changed: Content Optimizer has recieved a complete overhaul and many new features related to the new sections
* Changed: Search is now its own thematic group and got some new interessting statistics like countries/device charts and lost/new keyword
* Reverted: All Settings, which will be reverted to factory state in this update (your previous cache will be imported when updating)
* Disabled: Error Reporting has been temporarily disabled, but will return in the next version with a more intuitive workflow
* Removed: All Options to disable 3rd party libraries
* Removed: iCheck and various parts of other libraries
* Other: Added a ton of new Charts and Tables aswell as featues, tools and options

= 1.2.11 =
* Adresses minor issues. Major issues and feature suggests are sheduled for the next update
* Added: Non-Latin Mode to fix a problem with non latin words and the corresponding "Page Length"
* Fixed: Error when submitting an empty token
* Fixed: Redirect to wrong page, when clearing the logs

= 1.2.10 =
* Hotfix: AJAX Error - Added an error message and solved bug where it fires, allthough a notice should be shown (Note that if you got this, the update will most likly end in a more specific error message)

= 1.2.9 =
* Core Update - Some settings will be reset, please review them after updating
* Added: Permission System - You can give view access to this plugin to any user role (settings, reports and any interaction with the cache are for admins only)
* Added: Contact us overlay and link
* Added: AJAX loading system, so you don't spend to much time in front of a white screen
* Fixed: Various causes of the infamous "Query Error" and CronJob related errors
* Fixed: First and/or last day missing for a selected time range (wrong timestamps)
* Fixed: Error on some HTTPS sites (see https://wordpress.org/support/topic/wrong-protocol-http-vs-https/)
* Changed: Unstructured iCheck implementation fixed
* Changed: Standard time range was one day off to Googles last 28 days view
* Changed: Minor range and label changes for the time range picker
* Changed: Settings tabs are now more structured
* Changed: Error reporting has moved to it's own page (for performance)
* Changed: Some new notifcations and help icons were added
* Changed: Query blocks will pause 1 second between calls to maintain the 5 QPS limit set by Google
* Changed: Accidential remenants of two woocommerce files removed

= 1.2.8 =
* Hotfix - Google API loading everywhere (and thus being detected in our own check)
* Added: Time range to Keyword Details
* Fixed: Token should now be persistent through minor updates (Some versions may still require a new token)
* Fixed: Update Cache manually not working with caching disabled
* Fixed: Database option being updated instead of added if they don't exist
* Other: Changed Error API Exception output, Refreshed CronJob so 1.2.7 changes also have an effect, "Position" delta is now an absolute value

= 1.2.7 =
* Added: Error Reporting - In case of an error activate error reporting on the settings page and attach these reports to your support request
* Fixed: CronJob Error - corrupted data sets
* Other: Removed CTR field from database
* Other: Stabillity improvements, more loading icons

= 1.2.6 =
* Hotfix - Wrong PHP-sections

= 1.2.5 =
* Fixed: PHP 5.5 is the minimum requirment (instead of 5.4)

= 1.2.4 =
* Compatibility: Fixed several design and functional errors with other plugins
* Fixed: CronJob error (not running in some cases and duplicating data)
* Fixed: Track Keywords Modal opening and closing again
* Fixed: Google API conflict in init-action
* Fixed: Timestamp error (local) with custom range
* Fixed: Scroll error with Track Keywords Modal
* Other: Added several security and compatibility procedures/notices

= 1.2.3 =
* Fixed: White Dashboard error due to already loaded old Google Client API (below v2.0)

= 1.2.2 =
* Fixed: Uninstall Flag not working

= 1.2.1 =
* Hotfix - Live-Mode - Errors and CTR value wrong in Caching-Mode

= 1.2 =
* New: Toggle Caching (Active Caching is recommended)
* New: Caching Controls (Delete Cache, see database size, automatic delete time, ...)
* New: Uninstall Flag - If you are about to uninstall this plugin completely, check the option in the settings. This will also get rid of any database trace, the plugin has created.
* New: Activation Page has more info now
* New: Refreshing the page while updating the cache will show a confirmation first
* Fixed: Loading Assets on all pages reduced to plugin pages and post.php
* Fixed: Google API namespacing
* Fixed: Update Cache now has a timeout treshold to get more data rows in a big request
* Fixed: Dashboard Error View (now with logout button)
* Fixed: Authentication Token must be set before submiting the form
* Changed: Reduced WordPress Options to a single one
* Changed: Content-Length SEO criteria now has no negative effect if the page length is over 1500
* Other: Some graphs were disabled in the live-data version, because they rely on cached data
* Other: Various fixes and security related changes

= 1.1.3 =
* Hotfix - Critical Activation Error

= 1.1.2 =
* New: Documentation-Link
* Fixed: Pageination-Error on Pages-Overview
* Fixed: CronJob not working

= 1.1.1 =
* Notice changes

= 1.1 =
* New: Caching - Save all data data from first installation
* New: URL-Idenitifcation due to high load moved to a lazy loading process (may take a while)
* New: Settings-Page
* New: Keywords in Top 10 - Added progress value from mirrored time span
* New: Ranking Keyword Count on Post Page
* New: Toggle Post Widget - Setting on Login-Page
* New: Keyword and Ranking History
* New: Reload-/Submit Confirmation for post page modal (Track Keywords)
* New: Data-Limits (Can be changed in settings) for Performance (Note: Limit will allways cut the worst keywords, because it is applied on the rows sorted by clicks)
* Fixed: New URL-Resolve method to include redirected URLs
* Fixed: Suggest-Field on Post Page reloads page on "Enter" press
* Fixed: "Keyword in URL"-Criteria not working for long tail keywords
* Fixed: "Meta-Title not used"-Criteria issued for pages with only "<title/>"-element and no meta-tags
* Fixed: SEO-Criteria not working on posts with embedded scripts
* Fixed: Suggest-Tool: Language by wordpress locale
* Compatibility: Added global script/link identifiers for additional use of WSPO (Update 1.1 in queue)
* Other: Performance-/Usabillity Improvements

= 1.0.1 = 
* Hotfix - Google Error - Logout

= 1.0 = 
* Initial release
