<?php
if (!defined('ABSPATH')) exit;

class WSKO_Class_Import
{
	private $seo_plugins = array(
		'add-meta-tags/add-meta-tags.php' => array('title' => 'Add Meta Tags', 'options' => false),
		'all-in-one-seo-pack/all_in_one_seo_pack.php' => array('title' => 'All in One SEO', 'options' => array('post_meta_title' => 'Post - Meta Titles', 'post_meta_desc' => 'Post - Meta Descriptions', 'post_meta_ni_nf' => 'Post - Meta Noindex/Nofollow')),
		'autodescription/autodescription.php' => array('title' => 'The SEO Framework', 'options' => array('post_meta_title' => 'Post - Meta Titles', 'post_meta_desc' => 'Post - Meta Descriptions', 'post_meta_ni_nf' => 'Post - Meta Noindex/Nofollow')),
		'headspace2/headspace.php' => array('title' => 'Headspace2', 'options' => array('post_meta_title' => 'Post - Meta Titles', 'post_meta_desc' => 'Post - Meta Descriptions')),
		'platinum-seo-pack/platinum_seo_pack.php' => array('title' => 'Platinum SEO', 'options' => array('post_meta_title' => 'Post - Meta Titles', 'post_meta_desc' => 'Post - Meta Descriptions', 'post_meta_ni_nf' => 'Post - Meta Noindex/Nofollow')),
		'seo-ultimate/seo-ultimate.php' => array('title' => 'SEO Ultimate', 'options' => array('post_meta_title' => 'Post - Meta Titles', 'post_meta_desc' => 'Post - Meta Descriptions', 'post_meta_ni_nf' => 'Post - Meta Noindex/Nofollow')),
		'squirrly-seo/squirrly.php' => array('title' => 'SEO Squirrly', 'options' => false),
		'wordpress-seo/wp-seo.php' => array('title' => 'Yoast SEO', 'options' => array('hide_category_slug' => 'General - Hide "category" base', 'post_meta_title' => 'Post - Meta Titles', 'post_meta_desc' => 'Post - Meta Descriptions', 'post_meta_ni_nf' => 'Post - Meta Noindex/Nofollow', 'post_focus_kw' => 'Post - Focus Keywords')),
		'wp-meta-seo/wp-meta-seo.php' => array('title' => 'WP Meta SEO', 'options' => array('post_meta_title' => 'Post - Meta Titles', 'post_meta_desc' => 'Post - Meta Descriptions')),
	);
	
	public static function get_active_seo_plugins()
	{
		global $wsko_active_seo_plugins;
		if ($wsko_active_seo_plugins)
			return $wsko_active_seo_plugins;
		
		$res = array();
		$int = new static();
		foreach($int->seo_plugins as $k => $pl)
		{
			if (is_plugin_active($k))
				$res[$pl['title']] = '<a href="'.admin_url('plugins.php').'" target="_blank">'.$pl['title'].'</a>';
		}
		$wsko_active_seo_plugins = $res;
		return $res;
	}
	
	public static function is_seo_plugin_active()
	{
		return WSKO_Class_Import::get_active_seo_plugins() ? true : false;
	}
	
	public static function get_importable_plugins()
	{
		$int = new static();
		$res = array();
		$plugins_path = WP_PLUGIN_DIR.'/';
		$plugins = $int->seo_plugins; //merge plugins
		foreach ($plugins as $k => $pl)
		{
			if ($pl['options'] && file_exists($plugins_path.$k))
			{
				$res[$k] = array('title' => $pl['title'], 'options' => $pl['options'], 'active' => is_plugin_active($k));
			}
		}
		return $res;
	}
	
	public static function import_plugin($plugin, $options)
	{
		$meta_title = false;
		$meta_desc = false;
		$meta_noindex = false;
		$meta_nofollow = false;
		$focus_kw = false;
		switch ($plugin)
		{
			case 'all-in-one-seo-pack/all_in_one_seo_pack.php':
				if (in_array('post_meta_title', $options))
					$meta_title = "_aioseop_title";
				if (in_array('post_meta_desc', $options))
					$meta_desc = "_aioseop_description";
				if (in_array('post_meta_ni_nf', $options))
				{
					$meta_noindex = "_aioseop_noindex";
					$meta_nofollow = "_aioseop_nofollow";
				}
			break;
			case 'headspace2/headspace.php':
				if (in_array('post_meta_title', $options))
					$meta_title = "_headspace_page_title";
				if (in_array('post_meta_desc', $options))
					$meta_desc = "_headspace_description";
			break;
			case 'platinum-seo-pack/platinum_seo_pack.php':
				if (in_array('post_meta_title', $options))
					$meta_title = "title";
				if (in_array('post_meta_desc', $options))
					$meta_desc = "description";
				if (in_array('post_meta_ni_nf', $options))
				{
					$meta_noindex = "robotsmeta";
					$meta_nofollow = "robotsmeta";
				}
			break;
			case 'autodescription/autodescription.php':
				if (in_array('post_meta_title', $options))
					$meta_title = "_genesis_title";
				if (in_array('post_meta_desc', $options))
					$meta_desc = "_genesis_description";
				if (in_array('post_meta_ni_nf', $options))
				{
					$meta_noindex = "_genesis_noindex";
					$meta_nofollow = "_genesis_nofollow";
				}
			break;
			case 'seo-ultimate/seo-ultimate.php':
				if (in_array('post_meta_title', $options))
					$meta_title = "_su_title";
				if (in_array('post_meta_desc', $options))
					$meta_desc = "_su_description";
				if (in_array('post_meta_ni_nf', $options))
				{
					$meta_noindex = "_su_meta_robots_noindex";
					$meta_nofollow = "_su_meta_robots_nofollow";
				}
			break;
			case 'wordpress-seo/wp-seo.php':
				if (in_array('hide_category_slug', $options))
				{
					$meta_info = get_option('wpseo_titles');
					if ($meta_info && is_array($meta_info) && isset($meta_info['stripcategorybase']) && $meta_info['stripcategorybase'])
						WSKO_Class_Core::save_setting('hide_category_slug', true);
				}
				if (in_array('post_meta_title', $options))
					$meta_title = "_yoast_wpseo_title";
				if (in_array('post_meta_desc', $options))
					$meta_desc = "_yoast_wpseo_metadesc";
				if (in_array('post_meta_ni_nf', $options))
				{
					$meta_noindex = "_yoast_wpseo_meta-robots-noindex";
					$meta_nofollow = "_yoast_wpseo_meta-robots-nofollow";
				}
				if (in_array('post_focus_kw', $options))
					$focus_kw = "_yoast_wpseo_focuskw";
			break;
			case 'wp-meta-seo/wp-meta-seo.php':
				if (in_array('post_meta_title', $options))
					$meta_title = "_metaseo_metatitle";
				if (in_array('post_meta_desc', $options))
					$meta_desc = "_metaseo_metadesc";
			break;
		}
		
		$offset = 0;
		$step = 500; //batch queries
		do
		{
			$query = new WP_Query(array(
				'posts_per_page' => $step,
				'offset' => $offset * $step,
				'post_type' => 'any',
				'fields' => 'ids'
			));
			$results = $query->posts;
			foreach ($results as $p)
			{
				$res = array();
				if ($meta_title)
				{
					$t = get_post_meta($p, $meta_title, true);
					if ($t)
						$res['title'] =  $t;
				}
				if ($meta_desc)
				{
					$t = get_post_meta($p, $meta_desc, true);
					if ($t)
						$res['desc'] =  $t;
				}
				if ($meta_noindex || $meta_nofollow)
				{
					$t = false;
					if ($meta_noindex)
					{
						$t2 = get_post_meta($p, $meta_noindex, true);
						if ($t2  && is_string($t2))
							$t2 = $t2 == "true" || $t2 == "1" || $t2 == "on" || strpos($t2, 'noindex') !== false;
					}
					if ($meta_nofollow)
					{
						$t3 = get_post_meta($p, $meta_nofollow, true);
						if ($t3 && is_string($t3))
							$t3 = $t3 == "true" || $t2 == "1" || $t2 == "on" || strpos($t3, 'nofollow') !== false;
					}
					
					if ($meta_noindex && $meta_nofollow)
						$t = $t2 ? ($t3 ? 3 : 2) : ($t3 ? 1 : 0);
					else if ($meta_noindex)
						$t = ($t2 ? 2 : 0);
					else if ($meta_nofollow)
						$t = ($t3 ? 1 : 0);
						
					if ($t !== false)
						$res['robots'] =  $t;
				}
				if ($focus_kw)
				{
					$t = get_post_meta($p, $focus_kw, true);
					if ($t)
						WSKO_Class_Onpage::add_priority_keyword($p, $t, 1);
				}
				
				if ($res)
					WSKO_Class_Onpage::set_post_meta($p, $res); 
			}
			$offset++;
		}
		while ($query->post_count >= $step);
		
	}
}
?>