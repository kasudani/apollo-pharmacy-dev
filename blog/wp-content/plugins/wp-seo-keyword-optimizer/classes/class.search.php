<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class WSKO_Class_Search
{
    public static  $ga_client_se ;
    public static  $ga_client_an ;
    public static  $ga_webmaster ;
    public static  $ga_analytics ;
    public static function get_new_ga_client( $clear = false )
    {
        global  $wsko_plugin_path ;
        WSKO_Class_Core::include_lib( 'google' );
        try {
            WSKO_Class_Search::check_token();
            $client = new Google_Client();
            
            if ( $clear == false ) {
                $client->setAccessType( 'offline' );
                $client->setApprovalPrompt( 'force' );
                $client->setApplicationName( 'BAVOKO SEO Tools' );
                $client->setAuthConfig( $wsko_plugin_path . 'includes/cred.json' );
                $client->setRedirectUri( 'urn:ietf:wg:oauth:2.0:oob' );
                //'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php');
            }
            
            $client->addScope( "https://www.googleapis.com/auth/webmasters.readonly" );
            //search data
            //$client->addScope("https://www.googleapis.com/auth/analytics.readonly"); //analytics data
            return $client;
        } catch ( Exception $error ) {
            return false;
        }
        return false;
    }
    
    public static function get_ga_client_se()
    {
        if ( self::$ga_client_se ) {
            return self::$ga_client_se;
        }
        $client = WSKO_Class_Search::get_new_ga_client();
        $token = WSKO_Class_Search::get_se_token();
        
        if ( $token ) {
            $client->setAccessToken( $token );
            $token = $client->getAccessToken();
        }
        
        return self::$ga_client_se = $client;
    }
    
    public static function get_ga_client_an()
    {
        if ( self::$ga_client_an ) {
            return self::$ga_client_an;
        }
        $client = WSKO_Class_Search::get_new_ga_client();
        $token = WSKO_Class_Search::get_an_token();
        
        if ( $token ) {
            $client->setAccessToken( $token );
            $token = $client->getAccessToken();
        }
        
        return self::$ga_client_an = $client;
    }
    
    public static function get_se_auth_url()
    {
        $client = WSKO_Class_Search::get_ga_client_se();
        if ( $client ) {
            return $client->createAuthUrl();
        }
        return false;
    }
    
    public static function get_an_auth_url()
    {
        $client = WSKO_Class_Search::get_ga_client_an();
        if ( $client ) {
            return $client->createAuthUrl();
        }
        return false;
    }
    
    public static function get_ga_webmaster()
    {
        if ( self::$ga_webmaster ) {
            return self::$ga_webmaster;
        }
        $client = WSKO_Class_Search::get_ga_client_se();
        
        if ( $client ) {
            $webmaster = new Google_Service_Webmasters( $client );
        } else {
            return false;
        }
        
        self::$ga_webmaster = $webmaster;
        return $webmaster;
    }
    
    public static function get_ga_analytics()
    {
        if ( self::$ga_analytics ) {
            return self::$ga_analytics;
        }
        $client = WSKO_Class_Search::get_ga_client_an();
        
        if ( $client ) {
            $analytics = new Google_Service_Analytics( $client );
        } else {
            return false;
        }
        
        self::$ga_analytics = $analytics;
        return $analytics;
    }
    
    public static function get_search_data()
    {
        $wsko_data = WSKO_Class_Core::get_data();
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) ) {
            return $wsko_data['search'];
        }
        return false;
    }
    
    public static function get_se_token()
    {
        $wsko_data = WSKO_Class_Core::get_data();
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) && isset( $wsko_data['search']['se_token'] ) && $wsko_data['search']['se_token'] ) {
            return $wsko_data['search']['se_token'];
        }
        return false;
    }
    
    public static function set_se_token( $token )
    {
        $wsko_data = WSKO_Class_Core::get_data();
        
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) ) {
            $wsko_data['search']['se_token'] = $token;
        } else {
            $wsko_data['search'] = array(
                'se_token' => $token,
            );
        }
        
        WSKO_Class_Core::save_data( $wsko_data );
    }
    
    public static function get_an_token()
    {
        $wsko_data = WSKO_Class_Core::get_data();
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) && isset( $wsko_data['search']['an_token'] ) && $wsko_data['search']['an_token'] ) {
            return $wsko_data['search']['an_token'];
        }
        return false;
    }
    
    public static function set_an_token( $token )
    {
        $wsko_data = WSKO_Class_Core::get_data();
        
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) ) {
            $wsko_data['search']['an_token'] = $token;
        } else {
            $wsko_data['search'] = array(
                'an_token' => $token,
            );
        }
        
        WSKO_Class_Core::save_data( $wsko_data );
    }
    
    public static function get_an_profile()
    {
        $wsko_data = WSKO_Class_Core::get_data();
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) && isset( $wsko_data['search']['an_profile'] ) && $wsko_data['search']['an_profile'] ) {
            return $wsko_data['search']['an_profile'];
        }
        return false;
    }
    
    public static function set_an_profile( $profile )
    {
        $wsko_data = WSKO_Class_Core::get_data();
        
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) ) {
            $wsko_data['search']['an_profile'] = $profile;
        } else {
            $wsko_data['search'] = array(
                'an_profile' => $profile,
            );
        }
        
        WSKO_Class_Core::save_data( $wsko_data );
    }
    
    public static function get_monitored_keywords()
    {
        $wsko_data = WSKO_Class_Core::get_data();
        if ( isset( $wsko_data['search'] ) && is_array( $wsko_data['search'] ) && isset( $wsko_data['search']['monitored_keywords'] ) && $wsko_data['search']['monitored_keywords'] ) {
            return $wsko_data['search']['monitored_keywords'];
        }
        return array();
    }
    
    public static function add_monitored_keyword( $keyword )
    {
        
        if ( $keyword ) {
            $wsko_data = WSKO_Class_Core::get_data();
            
            if ( !isset( $wsko_data['search'] ) ) {
                $wsko_data['search'] = array(
                    'monitored_keywords' => array( $keyword ),
                );
            } else {
                
                if ( isset( $wsko_data['search']['monitored_keywords'] ) ) {
                    if ( !in_array( $keyword, $wsko_data['search']['monitored_keywords'] ) ) {
                        $wsko_data['search']['monitored_keywords'][] = $keyword;
                    }
                } else {
                    $wsko_data['search']['monitored_keywords'] = array( $keyword );
                }
            
            }
            
            WSKO_Class_Core::save_data( $wsko_data );
        }
    
    }
    
    public static function remove_monitored_keyword( $keyword )
    {
        $wsko_data = WSKO_Class_Core::get_data();
        
        if ( isset( $wsko_data['search']['monitored_keywords'] ) ) {
            $key = array_search( $keyword, $wsko_data['search']['monitored_keywords'] );
            
            if ( $key !== false ) {
                unset( $wsko_data['search']['monitored_keywords'][$key] );
                WSKO_Class_Core::save_data( $wsko_data );
            }
        
        }
    
    }
    
    public static function check_token( $force_check = false )
    {
        global  $wsko_ga_token_checked ;
        
        if ( !$wsko_ga_token_checked || $force_check ) {
            $wsko_ga_token_checked = true;
            $token = WSKO_Class_Search::get_se_token();
            
            if ( $token ) {
                $client = WSKO_Class_Search::get_ga_client_se();
                
                if ( $client ) {
                    $client->setAccessToken( $token );
                    
                    if ( $client->isAccessTokenExpired() ) {
                        $client->refreshToken( $token['refresh_token'] );
                        $token_c = $client->getAccessToken();
                        if ( !isset( $token_c['refresh_token'] ) ) {
                            $token_c['refresh_token'] = $token['refresh_token'];
                        }
                        $client->setAccessToken( $token_c );
                        WSKO_Class_Search::set_se_token( $token_c );
                        self::$ga_client_se = $client;
                    }
                
                }
            
            }
        
        }
    
    }
    
    public static function check_se_access( $force = false )
    {
        global  $wsko_plugin_path ;
        $res = true;
        try {
            $client = WSKO_Class_Search::get_ga_client_se();
            $token = WSKO_Class_Search::get_se_token();
            
            if ( $client && $token && ($force || WSKO_Class_Core::get_option( 'search_query_first_run' )) ) {
                $webmaster = WSKO_Class_Search::get_ga_webmaster();
                
                if ( $webmaster ) {
                    $q = new Google_Service_Webmasters_SearchAnalyticsQueryRequest();
                    $q->setStartDate( date( "Y-m-d", time() - 60 * 60 * 24 ) );
                    $q->setEndDate( date( "Y-m-d", time() ) );
                    $q->setDimensions( array( 'query' ) );
                    $q->setRowLimit( '1' );
                    $q->setSearchType( 'web' );
                    $data = $webmaster->searchanalytics->query( WSKO_Class_Helper::get_host_base(), $q );
                    $rows = $data->getRows();
                    $res = false;
                }
            
            }
        
        } catch ( Exception $error ) {
            WSKO_Class_Helper::report_error( 'exception', 'Search - Access Check', $error );
        }
        return $res;
    }
    
    public static function check_an_access()
    {
        global  $wsko_plugin_path ;
        $res = true;
        return $res;
    }
    
    public static function get_empty_se_row( $keyword )
    {
        $obj = new stdClass();
        $obj->keyval = $keyword;
        $obj->clicks = 0;
        $obj->impressions = 0;
        $obj->position = 0;
        $obj->ctr = 0;
        $rows = array( $obj );
        return $rows[0];
    }
    
    public static function get_an_profiles()
    {
        return false;
    }
    
    public static function get_an_data( $dimension )
    {
        $rows = array();
        return $rows;
    }
    
    public static function get_an_query_data( $dimension )
    {
        return false;
    }
    
    public static function get_se_cache_keword_history( $start, $end, $keywords )
    {
        $cache_rows = WSKO_Class_Cache::get_cache_rows(
            $start,
            $end,
            array(
            'search' => array(
            'type'   => array(
            'join' => true,
            'eval' => '=0',
        ),
            'keyval' => array(
            'join' => true,
            'eval' => ' IN ("' . implode( '","', $keywords ) . '")',
        ),
        ),
        ),
            array(
            'fieldset' => 'search',
            'group'    => 'time',
            'vals'     => 'cast(time as date) as time,SUM(clicks) as clicks,SUM(impressions) as impressions,AVG(position) as position',
        ),
            false
        );
        return $cache_rows;
    }
    
    public static function get_se_cache_history_count( $start, $end )
    {
        $rows = WSKO_Class_Cache::get_cache_rows(
            $start,
            $end,
            array(
            'search' => array(
            'type' => array(
            'join' => true,
            'eval' => '=2',
        ),
        ),
        ),
            false,
            false
        );
        if ( $rows ) {
            return count( $rows );
        }
        return 0;
    }
    
    public static function get_se_cache_history_position( $start_time, $end_time )
    {
        $cache_rows = WSKO_Class_Cache::get_cache_rows(
            $start_time,
            $end_time,
            array(
            'search' => array(
            'type' => array(
            'join' => true,
            'eval' => "=0",
        ),
        ),
        ),
            array(
            'fieldset' => 'search',
            'group'    => 'time',
            'vals'     => 'cast(time as date) as time,SUM(case when position between 0 and 10 then 1 else 0 end),SUM(case when position between 10 and 20 then 1 else 0 end),SUM(case when position between 20 and 30 then 1 else 0 end),SUM(case when position between 30 and 40 then 1 else 0 end)
		,SUM(case when position between 40 and 50 then 1 else 0 end),SUM(case when position between 50 and 60 then 1 else 0 end),SUM(case when position between 60 and 70 then 1 else 0 end),SUM(case when position between 70 and 80 then 1 else 0 end)
		,SUM(case when position between 80 and 90 then 1 else 0 end),SUM(case when position between 90 and 100 then 1 else 0 end)',
        ),
            false
        );
        return $cache_rows;
    }
    
    public static function get_se_cache_history( $start_time, $end_time, $dimension )
    {
        $type = -1;
        switch ( $dimension ) {
            case 'query':
                $type = 0;
                break;
            case 'page':
                $type = 1;
                break;
        }
        $cache_rows = WSKO_Class_Cache::get_cache_rows(
            $start_time,
            $end_time,
            array(
            'search' => array(
            'type' => array(
            'join' => true,
            'eval' => "=" . $type,
        ),
        ),
        ),
            array(
            'fieldset' => 'search',
            'group'    => 'time',
            'vals'     => 'cast(time as date) as time,SUM(case when cache_id is null then 0 else 1 end) as count',
        ),
            false
        );
        return $cache_rows;
    }
    
    public static function get_an_cache_history( $start_time, $end_time, $step = 1 )
    {
        return array();
    }
    
    public static function get_se_properties()
    {
        $token = WSKO_Class_Search::get_se_token();
        
        if ( $token ) {
            $webmaster = WSKO_Class_Search::get_ga_webmaster();
            if ( $webmaster ) {
                return $webmaster->sites->listSites();
            }
        }
    
    }
    
    public static function get_se_cache_stats( $start, $end )
    {
        $res = array();
        $data = WSKO_Class_Cache::get_cache_rows_stats( $start, $end, array(
            array(
            'table_key'  => 'search',
            'result_key' => 'kw_sum',
            'filter'     => array(
            'type' => array(
            'eval' => '=0',
        ),
        ),
        ),
            array(
            'table_key'  => 'search',
            'result_key' => 'page_sum',
            'filter'     => array(
            'type' => array(
            'eval' => '=1',
        ),
        ),
        ),
            array(
            'table_key'  => 'search',
            'result_key' => 'date_sum',
            'filter'     => array(
            'type' => array(
            'eval' => '=2',
        ),
        ),
        ),
            array(
            'table_key'  => 'search',
            'result_key' => 'device_sum',
            'filter'     => array(
            'type' => array(
            'eval' => '=3',
        ),
        ),
        ),
            array(
            'table_key'  => 'search',
            'result_key' => 'country_sum',
            'filter'     => array(
            'type' => array(
            'eval' => '=4',
        ),
        ),
        )
        ) );
        if ( $data ) {
            foreach ( $data as $row ) {
                $res[$row->time] = $row;
            }
        }
        return $res;
    }
    
    public static function update_se_cache()
    {
        ignore_user_abort( true );
        set_time_limit( WSKO_LRS_TIMEOUT );
        WSKO_Class_Helper::refresh_wp_cache( 'cron', true );
        WSKO_Class_Cache::check_database();
        $token = WSKO_Class_Search::get_se_token();
        
        if ( $token ) {
            $client = WSKO_Class_Search::get_ga_client_se();
            if ( $client ) {
                if ( !WSKO_Class_Search::check_se_access( true ) ) {
                    try {
                        $start_i = 0;
                        $end_i = 87;
                        $start = WSKO_Class_Helper::get_midnight() - 60 * 60 * 24 * 3;
                        $timeout = 5;
                        $temp = WSKO_Class_Cache::get_cache_rows( $start - 60 * 60 * 24 * 87, $start, array(
                            'search' => array(
                            'type' => array(
                            'join' => true,
                            'eval' => "=2",
                        ),
                        ),
                        ) );
                        WSKO_Class_Core::save_option( 'search_query_running', true );
                        WSKO_Class_Core::save_option( 'search_query_last_start', time() );
                        $step = false;
                        
                        if ( !WSKO_Class_Core::get_option( 'search_query_first_run' ) ) {
                            $step = intval( WSKO_Class_Core::get_option( 'search_query_first_run_step' ) );
                            
                            if ( !$step ) {
                                WSKO_Class_Core::save_option( 'search_query_first_run_step', 1 );
                                $step = 1;
                            } else {
                                if ( $step < 1 && $step > 9 ) {
                                    $step = false;
                                }
                            }
                            
                            
                            if ( $step !== false ) {
                                $start_i = 10 * ($step - 1);
                                $end_i = $start_i + 10;
                                if ( $step == 9 ) {
                                    $end_i -= 3;
                                }
                            }
                        
                        }
                        
                        //for ($i = $start_i; $i < ($quick?30:87); $i++)
                        for ( $i = $start_i ;  $i < $end_i ;  $i++ ) {
                            WSKO_Class_Search::check_token( true );
                            $curr = $start - 60 * 60 * 24 * $i;
                            $curr_d = date( 'Y-m-d H:i:s', $curr );
                            $curr2 = $curr + 60 * 60 * 23 + 60 * 59 + 59;
                            $has_cache_row = false;
                            //($temp && !empty($temp));
                            if ( $temp ) {
                                foreach ( $temp as $t ) {
                                    if ( strtotime( $t->time ) == $curr ) {
                                        $has_cache_row = true;
                                    }
                                }
                            }
                            
                            if ( $has_cache_row ) {
                                $stats = WSKO_Class_Search::get_se_cache_stats( $curr, $curr );
                                
                                if ( !$stats || !isset( $stats[$curr_d] ) || !$stats[$curr_d]->date_sum ) {
                                    // || ($stats->kw_sum === 0 && $stats->page_sum === 0 && $stats->device_sum === 0 && $stats->country_sum === 0))
                                    WSKO_Class_Cache::delete_cache_row( $curr, array( 'search' ) );
                                    $has_cache_row = false;
                                }
                            
                            }
                            
                            
                            if ( !$has_cache_row ) {
                                //Clean and refetch
                                WSKO_Class_Cache::delete_cache_row( $curr, array( 'search' ) );
                                //$curr2 = $curr - (60 * 6-0 * 24);
                                sleep( 1 );
                                //Sleep to not hit 5 QPS Limit
                                $date_rows = WSKO_Class_Search::get_se_query_data(
                                    $curr,
                                    $curr2,
                                    'date',
                                    false
                                );
                                $page_rows = WSKO_Class_Search::get_se_query_data(
                                    $curr,
                                    $curr2,
                                    'page',
                                    false
                                );
                                $kw_rows = WSKO_Class_Search::get_se_query_data(
                                    $curr,
                                    $curr2,
                                    'query',
                                    false
                                );
                                $device_rows = WSKO_Class_Search::get_se_query_data(
                                    $curr,
                                    $curr2,
                                    'device',
                                    false
                                );
                                $country_rows = WSKO_Class_Search::get_se_query_data(
                                    $curr,
                                    $curr2,
                                    'country',
                                    false
                                );
                                
                                if ( $date_rows === -1 || $page_rows === -1 || $kw_rows === -1 || $device_rows === -1 || $country_rows === -1 ) {
                                    //A Query failed
                                    ob_start();
                                    ?>
									Keyword Rows: <?php 
                                    echo  ( is_array( $kw_rows ) ? ( empty($kw_rows) ? 'Set (Empty)' : 'Set' ) : $kw_rows ) ;
                                    ?><br/>
									Page Rows: <?php 
                                    echo  ( is_array( $page_rows ) ? ( empty($page_rows) ? 'Set (Empty)' : 'Set' ) : $page_rows ) ;
                                    ?><br/>
									Date Rows: <?php 
                                    echo  ( is_array( $date_rows ) ? ( empty($date_rows) ? 'Set (Empty)' : 'Set' ) : $date_rows ) ;
                                    ?><br/>
									Device Rows: <?php 
                                    echo  ( is_array( $device_rows ) ? ( empty($device_rows) ? 'Set (Empty)' : 'Set' ) : $device_rows ) ;
                                    ?><br/>
									Country Rows: <?php 
                                    echo  ( is_array( $country_rows ) ? ( empty($country_rows) ? 'Set (Empty)' : 'Set' ) : $country_rows ) ;
                                    ?><br/>
									<?php 
                                    $add = ob_get_clean();
                                    $timeout--;
                                    
                                    if ( $timeout == 0 ) {
                                        WSKO_Class_Helper::report_error(
                                            'error',
                                            'Search - Query Error',
                                            'The query for "' . date( 'Y-m-d', $curr ) . '" failed during the cache update, but all timeouts are used.',
                                            $add
                                        );
                                    } else {
                                        
                                        if ( $timeout > 0 ) {
                                            WSKO_Class_Helper::report_error(
                                                'warning',
                                                'Search - Query Error',
                                                'The query for "' . date( 'Y-m-d', $curr ) . '" failed during the cache update and will be pulled again.',
                                                $add
                                            );
                                            $i--;
                                        }
                                    
                                    }
                                    
                                    continue;
                                } else {
                                    
                                    if ( !$date_rows || empty($date_rows) ) {
                                        // || !$page_rows || !$kw_rows || !$device_rows || !$country_rows)
                                        //empty data
                                        WSKO_Class_Cache::set_cache_row( $curr, array( array(
                                            'set'   => 'search',
                                            'where' => array(
                                            'type' => '2',
                                        ),
                                            'rows'  => array( array(
                                            'keyval'      => date( 'd.m.Y 00:00:00', $curr ),
                                            'clicks'      => 0,
                                            'position'    => 0,
                                            'impressions' => 0,
                                        ) ),
                                        ) ) );
                                    } else {
                                        $kw_rows_c = array();
                                        foreach ( $kw_rows as $kw_row ) {
                                            $kw_rows_c[] = array(
                                                'keyval'      => $kw_row->keys[0],
                                                'clicks'      => $kw_row->clicks,
                                                'position'    => $kw_row->position,
                                                'impressions' => $kw_row->impressions,
                                            );
                                        }
                                        $page_rows_c = array();
                                        foreach ( $page_rows as $page_row ) {
                                            $page_rows_c[] = array(
                                                'keyval'      => $page_row->keys[0],
                                                'clicks'      => $page_row->clicks,
                                                'position'    => $page_row->position,
                                                'impressions' => $page_row->impressions,
                                            );
                                        }
                                        $date_rows_c = array();
                                        foreach ( $date_rows as $date_row ) {
                                            $date_rows_c[] = array(
                                                'keyval'      => $date_row->keys[0],
                                                'clicks'      => $date_row->clicks,
                                                'position'    => $date_row->position,
                                                'impressions' => $date_row->impressions,
                                            );
                                        }
                                        $device_rows_c = array();
                                        foreach ( $device_rows as $device_row ) {
                                            $device_rows_c[] = array(
                                                'keyval'      => $device_row->keys[0],
                                                'clicks'      => $device_row->clicks,
                                                'position'    => $device_row->position,
                                                'impressions' => $device_row->impressions,
                                            );
                                        }
                                        $country_rows_c = array();
                                        foreach ( $country_rows as $country_row ) {
                                            $country_rows_c[] = array(
                                                'keyval'      => $country_row->keys[0],
                                                'clicks'      => $country_row->clicks,
                                                'position'    => $country_row->position,
                                                'impressions' => $country_row->impressions,
                                            );
                                        }
                                        WSKO_Class_Cache::set_cache_row( $curr, array(
                                            array(
                                            'set'   => 'search',
                                            'where' => array(
                                            'type' => '0',
                                        ),
                                            'rows'  => $kw_rows_c,
                                        ),
                                            array(
                                            'set'   => 'search',
                                            'where' => array(
                                            'type' => '1',
                                        ),
                                            'rows'  => $page_rows_c,
                                        ),
                                            array(
                                            'set'   => 'search',
                                            'where' => array(
                                            'type' => '2',
                                        ),
                                            'rows'  => $date_rows_c,
                                        ),
                                            array(
                                            'set'   => 'search',
                                            'where' => array(
                                            'type' => '3',
                                        ),
                                            'rows'  => $device_rows_c,
                                        ),
                                            array(
                                            'set'   => 'search',
                                            'where' => array(
                                            'type' => '4',
                                        ),
                                            'rows'  => $country_rows_c,
                                        )
                                        ) );
                                        //ob_start();
                                        //var_dump(count($kw_rows_c), count($page_rows_c), count($date_rows_c), count($device_rows_c), count($country_rows_c));
                                        //$add = ob_get_clean();
                                        //WSKO_Class_Helper::report_error('info', 'Search - Query Report', 'The query for "' . date('Y-m-d', $curr) . '" ('.date('Y-m-d', $curr2).') was successfull and fetched the following rows:', $add);
                                    }
                                
                                }
                            
                            }
                        
                        }
                        
                        if ( $step !== false && $step < 9 ) {
                            WSKO_Class_Helper::refresh_wp_cache( 'cron', true );
                            WSKO_Class_Core::save_option( 'search_query_first_run_step', $step + 1 );
                            WSKO_Class_Core::save_option( 'search_query_running', false );
                            //WSKO_Class_Crons::bind_keyword_update(true); //continue as soon as possible
                        } else {
                            WSKO_Class_Helper::refresh_wp_cache( 'cron', true );
                            WSKO_Class_Core::save_option( 'search_query_first_run_step', false );
                            WSKO_Class_Core::save_option( 'search_query_running', false );
                            WSKO_Class_Core::save_option( 'search_query_first_run', true );
                        }
                    
                    } catch ( Exception $error ) {
                        WSKO_Class_Helper::report_error( 'exception', 'Search - Cache Error', $error );
                    }
                }
            }
        }
    
    }
    
    public static function update_an_cache()
    {
        ignore_user_abort( true );
        WSKO_Class_Cache::check_database();
    }
    
    public static function get_se_data(
        $start_time,
        $end_time,
        $dimension,
        $for_url = false
    )
    {
        
        if ( !$for_url ) {
            $type = -1;
            $orderby = "clicks DESC";
            switch ( $dimension ) {
                case 'query':
                    $type = 0;
                    break;
                case 'page':
                    $type = 1;
                    break;
                case 'date':
                    $type = 2;
                    $orderby = "keyval ASC";
                    break;
                case 'device':
                    $type = 3;
                    break;
                case 'country':
                    $type = 4;
                    break;
            }
            //if ($order)
            //	$orderby = $order;
            $filters = array(
                'type' => array(
                'join' => true,
                'eval' => "=" . $type,
            ),
            );
            //if ($search)
            //{
            //$filters['keyval'] =  array('join' => false, 'eval' => "LIKE '%" . $search . "%'");
            //}
            $cache_rows = WSKO_Class_Cache::get_cache_rows(
                $start_time,
                $end_time,
                array(
                'search' => $filters,
            ),
                array(
                'fieldset' => 'search',
                'group'    => 'keyval',
                'vals'     => 'keyval,SUM(clicks) as clicks,SUM(impressions) as impressions,AVG(position) as position',
            ),
                $orderby
            );
            $rows = array();
            foreach ( $cache_rows as $row ) {
                $row->position = round( $row->position, 0 );
                
                if ( $row->impressions > 0 ) {
                    //recalc ctr
                    $row->ctr = $row->clicks / $row->impressions * 100;
                } else {
                    $row->ctr = 0;
                }
                
                if ( $dimension === 'date' ) {
                    $row->temp_keyval = strtotime( $row->keyval );
                }
                $rows[$row->keyval] = $row;
            }
            if ( $dimension === 'date' ) {
                uasort( $rows, function ( $a, $b ) {
                    if ( $a->temp_keyval === $b->temp_keyval ) {
                        return 0;
                    }
                    return ( $a->temp_keyval > $b->temp_keyval ? 1 : -1 );
                } );
            }
            return $rows;
        } else {
            $query_rows = WSKO_Class_Search::get_se_query_data(
                $start_time,
                $end_time,
                $dimension,
                $for_url,
                false,
                false
            );
            $rows = array();
            if ( $query_rows && $query_rows !== -1 ) {
                foreach ( $query_rows as $row ) {
                    $row->position = round( $row->position, 0 );
                    $row->keyval = $row->keys[0];
                    unset( $row->keys );
                    $rows[$row->keyval] = $row;
                }
            }
            return $rows;
        }
    
    }
    
    public static function get_se_query_data(
        $start_time,
        $end_time,
        $dimension,
        $for_url = false,
        $order = false,
        $search = false
    )
    {
        $webmaster = WSKO_Class_Search::get_ga_webmaster();
        
        if ( $webmaster ) {
            $q = new Google_Service_Webmasters_SearchAnalyticsQueryRequest();
            $q->setStartDate( date( "Y-m-d", $start_time ) );
            $q->setEndDate( date( "Y-m-d", $end_time ) );
            $q->setDimensions( array( $dimension ) );
            $q->setRowLimit( WSKO_SEARCH_ROW_LIMIT );
            $q->setSearchType( 'web' );
            
            if ( $for_url || $search ) {
                $filters = array();
                
                if ( $for_url ) {
                    $filter = new Google_Service_Webmasters_ApiDimensionFilter();
                    $filter->setDimension( "page" );
                    $filter->setOperator( "equals" );
                    $filter->setExpression( $for_url );
                    array_push( $filters, $filter );
                }
                
                
                if ( $search ) {
                    $filter = new Google_Service_Webmasters_ApiDimensionFilter();
                    $filter->setDimension( $dimension );
                    $filter->setOperator( "contains" );
                    $filter->setExpression( $search );
                    array_push( $filters, $filter );
                }
                
                $filter_group = new Google_Service_Webmasters_ApiDimensionFilterGroup();
                $filter_group->setGroupType( 'and' );
                $filter_group->setFilters( $filters );
                $q->setDimensionFilterGroups( array( $filter_group ) );
            }
            
            try {
                $data = $webmaster->searchanalytics->query( WSKO_Class_Helper::get_host_base(), $q );
                
                if ( $data ) {
                    $rows = $data->getRows();
                    foreach ( $rows as $row ) {
                        $row->ctr = $row->ctr * 100;
                    }
                    if ( $order ) {
                        switch ( $order ) {
                            case 0:
                                //Key DESC
                                usort( $rows, function ( $a, $b ) {
                                    return strcmp( $a->keys[0], $b->keys[0] );
                                } );
                                $rows = array_reverse( $rows );
                                break;
                            case 1:
                                //Key ASC
                                usort( $rows, function ( $a, $b ) {
                                    return strcmp( $a->keys[0], $b->keys[0] );
                                } );
                                break;
                                /*case 2: //Clicks DESC is default
                                		break;*/
                            /*case 2: //Clicks DESC is default
                            		break;*/
                            case 3:
                                //Clicks ASC is default
                                $rows = array_reverse( $rows );
                                break;
                            case 4:
                                //Position DESC
                                usort( $rows, function ( $a, $b ) {
                                    if ( $a->position == $b->position ) {
                                        return 0;
                                    }
                                    return ( $a->position < $b->position ? 1 : -1 );
                                } );
                                break;
                            case 5:
                                //Position ASC
                                usort( $rows, function ( $a, $b ) {
                                    if ( $a->position == $b->position ) {
                                        return 0;
                                    }
                                    return ( $a->position < $b->position ? -1 : 1 );
                                } );
                                break;
                            case 6:
                                //Impression DESC
                                usort( $rows, function ( $a, $b ) {
                                    if ( $a->impressions == $b->impressions ) {
                                        return 0;
                                    }
                                    return ( $a->impressions < $b->impressions ? 1 : -1 );
                                } );
                                break;
                            case 7:
                                //Impression ASC
                                usort( $rows, function ( $a, $b ) {
                                    if ( $a->impressions == $b->impressions ) {
                                        return 0;
                                    }
                                    return ( $a->impressions < $b->impressions ? -1 : 1 );
                                } );
                                break;
                            case 8:
                                //Ctr DESC
                                usort( $rows, function ( $a, $b ) {
                                    if ( $a->ctr == $b->ctr ) {
                                        return 0;
                                    }
                                    return ( $a->ctr < $b->ctr ? 1 : -1 );
                                } );
                                break;
                            case 9:
                                //Ctr ASC
                                usort( $rows, function ( $a, $b ) {
                                    if ( $a->ctr == $b->ctr ) {
                                        return 0;
                                    }
                                    return ( $a->ctr < $b->ctr ? -1 : 1 );
                                } );
                                break;
                        }
                    }
                    foreach ( $rows as $key => $row ) {
                        $rows[$key]->position = round( $row->position );
                    }
                    return $rows;
                }
            
            } catch ( Exception $e ) {
                ob_start();
                var_dump( WSKO_Class_Search::get_se_token(), WSKO_Class_Search::get_ga_client_an()->isAccessTokenExpired() );
                WSKO_Class_Helper::report_error(
                    'exception',
                    'Search - Query Error',
                    $e,
                    ob_get_clean()
                );
            }
        }
        
        return -1;
    }

}