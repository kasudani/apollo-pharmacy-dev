<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class WSKO_Class_Crons
{
    public static function register_cronjobs()
    {
        
        if ( !wp_next_scheduled( 'wsko_cache_backlinks' ) ) {
            wp_schedule_event( WSKO_Class_Helper::get_midnight() + 60 * 10, 'daily', 'wsko_cache_backlinks' );
            //00:10
        }
        
        /*if (!wp_next_scheduled('wsko_check_timeout'))
        		{
        			wp_schedule_event(WSKO_Class_Helper::get_midnight() + (60*60*12), 'daily', 'wsko_check_timeout'); //12:00
        		}*/
        WSKO_Class_Crons::bind_social_cache();
        WSKO_Class_Crons::bind_daily_maintenance();
        WSKO_Class_Crons::bind_sitemap_generation();
        if ( WSKO_Class_Onpage::has_current_report() ) {
            WSKO_Class_Crons::bind_onpage_analysis();
        }
        WSKO_Class_Crons::bind_keyword_update();
    }
    
    public static function bind_social_cache( $rebind = false )
    {
        if ( $rebind ) {
            WSKO_Class_Crons::unbind_social_cache();
        }
        
        if ( !wp_next_scheduled( 'wsko_cache_social' ) ) {
            wp_schedule_event( WSKO_Class_Helper::get_midnight() + 60 * 20, 'daily', 'wsko_cache_social' );
            //00:30
        }
    
    }
    
    public static function unbind_social_cache()
    {
        wp_clear_scheduled_hook( 'wsko_cache_social' );
    }
    
    public static function bind_daily_maintenance()
    {
        
        if ( !wp_next_scheduled( 'wsko_daily_maintenance' ) ) {
            wp_schedule_event( WSKO_Class_Helper::get_midnight() + 60 * 60 * 12, 'daily', 'wsko_daily_maintenance' );
            //12:00
        }
    
    }
    
    public static function bind_sitemap_generation()
    {
        
        if ( !wp_next_scheduled( 'wsko_sitemap_generation' ) ) {
            wp_schedule_event( WSKO_Class_Helper::get_midnight() + 60 * 45, 'hourly', 'wsko_sitemap_generation' );
            //**:45
        }
    
    }
    
    public static function unbind_sitemap_generation()
    {
        wp_clear_scheduled_hook( 'wsko_sitemap_generation' );
    }
    
    public static function bind_onpage_analysis()
    {
        
        if ( !wp_next_scheduled( 'wsko_onpage_analysis' ) ) {
            wp_schedule_event( WSKO_Class_Helper::get_midnight() + 60 * 15, 'hourly', 'wsko_onpage_analysis' );
            //**:15
        }
    
    }
    
    public static function bind_keyword_update( $rebind = false )
    {
        if ( $rebind ) {
            WSKO_Class_Crons::unbind_keyword_update();
        }
        
        if ( !wp_next_scheduled( 'wsko_cache_keywords' ) ) {
            wp_schedule_event( WSKO_Class_Helper::get_midnight(), 'hourly', 'wsko_cache_keywords' );
            //**:00
        }
    
    }
    
    public static function unbind_keyword_update()
    {
        wp_clear_scheduled_hook( 'wsko_cache_keywords' );
    }
    
    public static function deregister_cronjobs()
    {
        wp_clear_scheduled_hook( 'wsko_cache_backlinks' );
        //wp_clear_scheduled_hook('wsko_check_timeout');
        wp_clear_scheduled_hook( 'wsko_premium_keyword_info' );
        wp_clear_scheduled_hook( 'wsko_onpage_analysis' );
        wp_clear_scheduled_hook( 'wsko_daily_maintenance' );
        WSKO_Class_Crons::unbind_sitemap_generation();
        WSKO_Class_Crons::unbind_keyword_update();
        WSKO_Class_Crons::unbind_social_cache();
    }
    
    public static function add_cronjobs()
    {
        
        if ( WSKO_Class_Core::is_configured() ) {
            $inst = WSKO_Class_Crons::get_instance();
            add_action( 'wsko_cache_keywords', array( $inst, 'cronjob_cache_keywords' ) );
            add_action( 'wsko_cache_backlinks', array( $inst, 'cronjob_cache_backlinks' ) );
            //add_action('wsko_check_timeout', array($inst, 'cronjob_check_timeout'));
            add_action( 'wsko_cache_social', array( $inst, 'cronjob_cache_social' ) );
            add_action( 'wsko_onpage_analysis', array( $inst, 'cronjob_onpage_analysis' ) );
            add_action( 'wsko_sitemap_generation', array( $inst, 'cronjob_sitemap_generation' ) );
            add_action( 'wsko_daily_maintenance', array( $inst, 'conjob_daily_maintenance' ) );
        }
    
    }
    
    function cronjob_cache_keywords()
    {
        if ( WSKO_Class_Helper::get_current_time() - WSKO_Class_Helper::get_midnight() > 60 * 60 ) {
            //skip first hour
            WSKO_Class_Search::update_se_cache();
        }
    }
    
    function cronjob_cache_backlinks()
    {
        WSKO_Class_Search::update_an_cache();
    }
    
    function cronjob_check_timeout()
    {
        /*$wsko_data = WSKO_Class_Core::get_data();
        		$wsko_data['timeout_check'] = time();
        		WSKO_Class_Core::save_data($wsko_data);
        		
        		set_time_limit(WSKO_LRS_TIMEOUT);
        		sleep(WSKO_LRS_TIMEOUT - 10);
        		
        		/*$wsko_data = WSKO_Class_Core::get_data();
        		unset($wsko_data['timeout_check']);
        		WSKO_Class_Core::save_data($wsko_data);*/
    }
    
    function cronjob_premium_keyword_info()
    {
    }
    
    function cronjob_cache_social()
    {
        WSKO_Class_Social::update_social_cache();
    }
    
    function cronjob_onpage_analysis()
    {
        WSKO_Class_Onpage::generate_onpage_analysis();
    }
    
    function cronjob_sitemap_generation()
    {
        
        if ( WSKO_Class_Core::get_setting( 'automatic_sitemap' ) ) {
            WSKO_Class_Onpage::generate_sitemap();
            
            if ( WSKO_Class_Core::get_option( 'sitemap_dirty' ) ) {
                WSKO_Class_Onpage::upload_sitemap();
                WSKO_Class_Core::save_option( 'sitemap_dirty', false );
            }
        
        }
    
    }
    
    function conjob_daily_maintenance()
    {
        try {
            //expired search cache
            
            if ( WSKO_Class_Core::get_setting( 'cache_time_limit' ) && WSKO_Class_Core::get_setting( 'cache_time_limit' ) > 90 ) {
                $start2 = strtotime( 'today' ) - 60 * 60 * 24 * WSKO_Class_Core::get_setting( 'cache_time_limit' );
                WSKO_Class_Cache::delete_cache_rows_before( $start2 );
            }
            
            //expired logs
            WSKO_Class_Helper::clear_logs();
            //auto backup
            $backups = get_option( 'wsko_backups' );
            $backup_freq = intval( WSKO_Class_Core::get_setting( 'conf_backup_interval' ) );
            if ( !$backup_freq || $backup_freq <= 0 ) {
                $backup_freq = 1;
            }
            $has_backup = false;
            
            if ( $backups ) {
                ksort( $backups );
                foreach ( $backups as $b ) {
                    if ( $b['time'] < time() - 60 * 60 * 24 * $backup_freq ) {
                        $has_backup = true;
                    }
                }
            }
            
            if ( !$has_backup ) {
                WSKO_Class_Core::backup_configuration( true );
            }
            //expired backups
            $backups = get_option( 'wsko_backups' );
            
            if ( $backups ) {
                ksort( $backups );
                $c = 0;
                $max_b = intval( WSKO_Class_Core::get_setting( 'conf_backup_limit' ) );
                if ( !$max_b || $max_b <= 0 ) {
                    $max_b = 7;
                }
                foreach ( $backups as $k => $b ) {
                    
                    if ( $b['auto'] ) {
                        $c++;
                        if ( $c > $max_b ) {
                            unset( $backups[$k] );
                        }
                    }
                
                }
                update_option( 'wsko_backups', $backups );
            }
        
        } catch ( Exception $ex ) {
            WSKO_Class_Helper::report_error( 'exception', 'Maintenance Error', $ex );
        }
    }
    
    //Singleton
    static  $instance ;
    public static function get_instance()
    {
        if ( !isset( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}