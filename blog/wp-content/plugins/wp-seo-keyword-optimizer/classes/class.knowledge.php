<?php
if (!defined('ABSPATH')) exit;

class WSKO_Class_Knowledge
{
	private static $kb_endpoint = 'https://www.bavoko.tools/kb-api.php';
	
	public static function rate_article($article, $type)
	{
		$data = WSKO_Class_Helper::post_to_url(static::$kb_endpoint, array('action' => 'rate_article', 'post' => $article, 'type' => $type));
		if ($data)
		{
			$data_ref = (array)json_decode($data);
			if ($data_ref && is_array($data_ref) && isset($data_ref['success']) && $data_ref['success'])
			{
				return true;
			}
		}
		return false;
	}
	
	public static function search_knowledge_base($search, $categories = array())
	{
		$articles = WSKO_Class_Knowledge::get_knowledge_base_articles();
		if ($search || $categories)
		{
			$search = WSKO_Class_Helper::densify_string($search);
			foreach ($articles as $k => $a)
			{
				if (($search && stripos(WSKO_Class_Helper::densify_string($a->title), $search) === false && stripos(WSKO_Class_Helper::densify_string($a->excerpt), $search) === false && stripos(WSKO_Class_Helper::densify_string($a->content), $search) === false) || ($categories && !array_intersect($categories, $a->categories)))
				{
					unset($articles[$k]);
				}
			}
		}
		return $articles;
	}
	
	public static function get_knowledge_base_articles()
	{
		$wsko_data = WSKO_Class_Core::get_data();
		$lang = WSKO_Class_Core::get_plugin_language();
		if (WSKO_Class_Core::get_option('knowledge_base_lang') != $lang || WSKO_Class_Core::get_option('last_knowledge_base_update') < (WSKO_Class_Helper::get_current_time()-(60*60*24)) || !isset($wsko_data['knowledge_base_articles']) || !$wsko_data['knowledge_base_articles'])
		{
			$data = WSKO_Class_Helper::post_to_url(static::$kb_endpoint, array('action' => 'list_articles', 'lang' => $lang));
			if ($data)
			{
				$data_ref = (array)json_decode($data);
				if ($data_ref && is_array($data_ref) && isset($data_ref['success']) && $data_ref['success'] && isset($data_ref['articles']) && $data_ref['articles'] && isset($data_ref['categories']) && $data_ref['categories'])
				{
					$res = array();
					foreach ($data_ref['articles'] as $d)
					{
						$d->data = false;
						$res[$d->id] = $d;
					}
					$wsko_data['knowledge_base_articles'] = $res;
					$wsko_data['knowledge_base_categories'] = $data_ref['categories'];
					WSKO_Class_Core::save_data($wsko_data);
					WSKO_Class_Core::save_option('last_knowledge_base_update', WSKO_Class_Helper::get_current_time());
					WSKO_Class_Core::save_option('knowledge_base_lang', $lang);
					return $res;
				}
			}
		}
		if (isset($wsko_data['knowledge_base_articles']) && $wsko_data['knowledge_base_articles'])
		{
			return $wsko_data['knowledge_base_articles'];
		}
		return false;
	}
	
	public static function get_knowledge_base_categories()
	{
		$wsko_data = WSKO_Class_Core::get_data();
		if (isset($wsko_data['knowledge_base_categories']) && $wsko_data['knowledge_base_categories'])
		{
			return $wsko_data['knowledge_base_categories'];
		}
		return false;
	}
	
	public static function get_knowledge_base_article_info($id)
	{
		$articles = WSKO_Class_Knowledge::get_knowledge_base_articles();
		if (isset($articles[$id]) && $articles[$id])
		{
			return $articles[$id];
			/*if (isset($articles[$id]->data) && $articles[$id]->data)
			{
				return $articles[$id];
			}
			
			$data = WSKO_Class_Helper::post_to_url(static::$kb_endpoint, array('action' => 'get_article', 'article' => $id));
			if ($data)
			{
				$data_ref = (array)json_decode($data);
				if ($data_ref && is_array($data_ref) && isset($data_ref['success']) && $data_ref['success'] && isset($data_ref['data']) && $data_ref['data'])
				{
					$articles[$id]->data = $data_ref['data'];
					$wsko_data = WSKO_Class_Core::get_data();
					$wsko_data['knowledge_base_articles'] = $articles;
					WSKO_Class_Core::save_data($wsko_data);
					return $articles[$id];
				}
			}*/
		}
		return false;
	}
}
?>