<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class WSKO_Class_Core
{
    static  $data ;
    static  $data_time ;
    public static function install()
    {
        //WSKO_Class_Crons::register_cronjobs();
        WSKO_Class_Cache::check_database();
    }
    
    public static function update()
    {
        WSKO_Class_Cache::check_database();
    }
    
    public static function deinstall()
    {
        WSKO_Class_Crons::deregister_cronjobs();
    }
    
    public static function setup()
    {
        WSKO_Class_Cache::check_database();
        $post_types = WSKO_Class_Helper::get_public_post_types( 'names' );
        WSKO_Class_Core::save_setting( 'onpage_include_post_types', 'post,page' );
        WSKO_Class_Onpage::set_sitemap_params( array(
            'types' => array(
            'post' => array(
            'freq' => 'monthly',
        ),
            'page' => array(
            'freq' => 'monthly',
        ),
        ),
            'stati' => array( 'publish' ),
        ) );
        WSKO_Class_Core::save_setting( 'content_optimizer_post_types', implode( ',', $post_types ) );
        WSKO_Class_Core::save_setting( 'auto_canonical', true );
        //WSKO_Class_Core::save_setting('auto_social_snippet', true);
        WSKO_Class_Core::save_setting( 'auto_social_thumbnail', true );
        WSKO_Class_Core::save_setting( 'activate_content_optimizer', true );
        WSKO_Class_Core::save_setting( 'auto_post_slug_redirects', true );
        WSKO_Class_Core::save_setting( 'automatic_sitemap_ping', true );
        WSKO_Class_Crons::deregister_cronjobs();
        WSKO_Class_Crons::register_cronjobs();
    }
    
    public static function init_classes( $force = false )
    {
        global  $wsko_initated, $wsko_plugin_path ;
        require_once $wsko_plugin_path . 'const.php';
        require_once $wsko_plugin_path . 'classes/class.helper.php';
        if ( $wsko_initated ) {
            return;
        }
        $wsko_initated = true;
        //if (WSKO_Class_Helper::is_wsko_ajax() || WSKO_Class_Helper::is_wsko_page() || WSKO_Class_Helper::is_wsko_cron())
        //WSKO_Class_Helper::activate_cloud_logging();
        require_once $wsko_plugin_path . 'classes/class.cache.php';
        require_once $wsko_plugin_path . 'classes/class.onpage.php';
        require_once $wsko_plugin_path . 'classes/class.social.php';
        require_once $wsko_plugin_path . 'classes/class.search.php';
        require_once $wsko_plugin_path . 'classes/class.template.php';
        require_once $wsko_plugin_path . 'classes/class.crons.php';
        require_once $wsko_plugin_path . 'classes/class.import.php';
        require_once $wsko_plugin_path . 'classes/class.knowledge.php';
        WSKO_Class_Core::check_version();
        require_once $wsko_plugin_path . 'admin/controller/controller.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.setup.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.dashboard.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.settings.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.knowledge.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.search.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.social.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.onpage.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.optimizer.php';
        require_once $wsko_plugin_path . 'admin/controller/controller.download.php';
        //init necessaries
        
        if ( WSKO_Class_Core::is_configured() ) {
            WSKO_Class_Crons::add_cronjobs();
            WSKO_Class_Onpage::register_hooks();
            WSKO_Controller_Dashboard::init_actions();
            WSKO_Controller_Search::init_actions();
            WSKO_Controller_Social::init_actions();
            WSKO_Controller_Onpage::init_actions();
            WSKO_Controller_Settings::init_actions();
            WSKO_Controller_Knowledge::init_actions();
            WSKO_Controller_Optimizer::init_actions();
            WSKO_Controller_Download::init_actions();
        } else {
            WSKO_Controller_Setup::init_actions();
            WSKO_Controller_Settings::init_actions();
            WSKO_Controller_Knowledge::init_actions();
        }
        
        
        if ( is_admin() ) {
            //if (!defined('DOING_AJAX') || !DOING_AJAX)
            //{
            require_once $wsko_plugin_path . 'admin/admin.php';
            //}
            //Load Admin
            add_action( 'plugins_loaded', function () {
                global  $wsko_plugin_path ;
                $inst = new WSKO_Class_Core();
                add_filter( 'plugin_locale', array( $inst, 'modify_plugin_locale' ) );
                load_plugin_textdomain( 'wsko', false, basename( dirname( dirname( __FILE__ ) ) ) . '/languages/' );
                remove_filter( 'plugin_locale', array( $inst, 'modify_plugin_locale' ) );
                WSKO_AdminMenu::get_instance();
            } );
            if ( WSKO_Class_Core::get_option( 'refresh_permalinks' ) ) {
                add_action( 'wp_loaded', function () {
                    flush_rewrite_rules();
                } );
            }
            add_action( 'init', function () {
                WSKO_Class_Helper::add_error_post_type();
            } );
            add_action( 'admin_init', function () {
            } );
        }
        
        if ( WSKO_Class_Core::is_configured() && WSKO_Class_Search::get_se_token() && WSKO_Class_Search::get_ga_client_se() && !WSKO_Class_Core::get_option( 'search_query_first_run' ) && !WSKO_Class_Core::get_option( 'search_query_running' ) && wp_next_scheduled( 'wsko_cache_keywords' ) > time() ) {
            WSKO_Class_Crons::bind_keyword_update( true );
        }
    }
    
    public function modify_plugin_locale( $locale )
    {
        $lang = WSKO_Class_Core::get_setting( 'plugin_lang' );
        
        if ( !$lang || $lang === 'auto' ) {
            return $locale;
        } else {
            return $lang;
        }
        
        //return $locale;
    }
    
    public static function check_version()
    {
        $version_pre = get_option( 'wsko_init' );
        
        if ( $version_pre && is_array( $version_pre ) && isset( $version_pre['version'] ) ) {
            $version_pre = $version_pre['version'];
        } else {
            $version_pre = false;
        }
        
        $wsko_data = WSKO_Class_Core::get_data();
        
        if ( $version_pre && version_compare( $version_pre, WSKO_VERSION, '<' ) ) {
            $update_needed = false;
            if ( version_compare( $version_pre, '2.0', '<' ) ) {
                //Reinit below 2.0
                $update_needed = true;
            }
            
            if ( version_compare( $version_pre, '2.0.4', '<' ) ) {
                delete_option( 'wsko_cache_snapshot' );
                wp_clear_scheduled_hook( 'wsko_cache_snapshot' );
                //remove old cache snapshot
                WSKO_Class_Crons::bind_daily_maintenance();
                $onpage_data = WSKO_Class_Onpage::get_onpage_data();
                
                if ( WSKO_Class_Onpage::has_current_report() ) {
                    unset( $onpage_data['global_analysis'] );
                    //['current_report']);
                    WSKO_Class_Onpage::set_onpage_data( $onpage_data );
                    wp_clear_scheduled_hook( 'wsko_onpage_analysis' );
                    WSKO_Class_Core::save_option( 'onpage_calc_changed', true );
                    WSKO_Class_Cache::clear_session_cache( false );
                }
                
                WSKO_Class_Core::save_setting( 'auto_social_snippet', true );
                WSKO_Class_Core::save_setting( 'auto_social_thumbnail', true );
                $wsko_data = WSKO_Class_Core::get_data( true );
                //update fields for further updates
            }
            
            
            if ( version_compare( $version_pre, '2.0.5', '<' ) ) {
                wp_clear_scheduled_hook( 'wsko_cache_expire' );
                $onpage_data = WSKO_Class_Onpage::get_onpage_data();
                
                if ( isset( $onpage_data['priority_kewords'] ) ) {
                    $onpage_data['priority_keywords'] = $onpage_data['priority_kewords'];
                    unset( $onpage_data['priority_kewords'] );
                    WSKO_Class_Onpage::set_onpage_data( $onpage_data );
                }
                
                if ( file_exists( WP_CONTENT_DIR . '/bst/.htaccess' ) ) {
                    unlink( WP_CONTENT_DIR . '/bst/.htaccess' );
                }
                wp_clear_scheduled_hook( 'wsko_check_timeout' );
                WSKO_Class_Core::save_setting( 'auto_post_slug_redirects', true );
                wsko_fs()->connect_again();
                $wsko_data = WSKO_Class_Core::get_data( true );
                //update fields for further updates
            }
            
            
            if ( version_compare( $version_pre, '2.0.6', '<' ) ) {
                $gen_params = WSKO_Class_Onpage::get_sitemap_params();
                if ( isset( $gen_params['excluded_posts'] ) ) {
                    unset( $gen_params['excluded_posts'] );
                }
                WSKO_Class_Onpage::set_sitemap_params( $gen_params );
                $wsko_data = WSKO_Class_Core::get_data( true );
                //update fields for further updates
            }
            
            
            if ( $update_needed ) {
                $wsko_data = WSKO_Class_Core::get_data_default();
            } else {
                $wsko_data['version'] = WSKO_VERSION;
            }
            
            if ( version_compare( $version_pre, WSKO_VERSION, '<' ) ) {
                $wsko_data['version_pre'] = $version_pre;
            }
            WSKO_Class_Core::save_data( $wsko_data );
        }
    
    }
    
    public static function is_configured()
    {
        $wsko_data = WSKO_Class_Core::get_data();
        return isset( $wsko_data['configured'] ) && $wsko_data['configured'];
    }
    
    public static function set_configured( $to = true )
    {
        
        if ( current_user_can( 'manage_options' ) ) {
            $wsko_data = WSKO_Class_Core::get_data();
            
            if ( $to ) {
                $wsko_data['configured'] = true;
            } else {
                unset( $wsko_data['configured'] );
            }
            
            WSKO_Class_Core::save_data( $wsko_data );
            return true;
        }
        
        return false;
    }
    
    public static function include_lib( $lib )
    {
        global  $wsko_plugin_path ;
        switch ( $lib ) {
            case 'google':
                if ( !defined( 'WSKO_GOOGLE_INCLUDE_FAILED' ) ) {
                    
                    if ( !class_exists( 'Google_Client' ) ) {
                        require_once $wsko_plugin_path . 'includes/google-api-php-client-2.1.0/vendor/autoload.php';
                        
                        if ( class_exists( 'Google_Client' ) ) {
                            define( 'WSKO_GOOGLE_INCLUDE_FAILED', false );
                            WSKO_Class_Search::check_token();
                        } else {
                            define( 'WSKO_GOOGLE_INCLUDE_FAILED', true );
                        }
                    
                    } else {
                        define( 'WSKO_GOOGLE_INCLUDE_FAILED', true );
                    }
                
                }
                break;
            case 'facebook':
                if ( !defined( 'WSKO_FACEBOOK_INCLUDE_FAILED' ) ) {
                    
                    if ( !class_exists( '\\Facebook\\Facebook' ) ) {
                        require_once $wsko_plugin_path . 'includes/php-graph-sdk-5.x/src/Facebook/autoload.php';
                        
                        if ( class_exists( '\\Facebook\\Facebook' ) ) {
                            define( 'WSKO_FACEBOOK_INCLUDE_FAILED', false );
                        } else {
                            define( 'WSKO_FACEBOOK_INCLUDE_FAILED', true );
                        }
                    
                    } else {
                        define( 'WSKO_FACEBOOK_INCLUDE_FAILED', true );
                    }
                
                }
                break;
            case 'twitter':
                if ( !defined( 'WSKO_TWITTER_INCLUDE_FAILED' ) ) {
                    
                    if ( !class_exists( 'TwitterAPIExchange' ) ) {
                        require_once $wsko_plugin_path . 'includes/twitter-exchange-api/TwitterAPIExchange.php';
                        
                        if ( class_exists( 'TwitterAPIExchange' ) ) {
                            define( 'WSKO_TWITTER_INCLUDE_FAILED', false );
                        } else {
                            define( 'WSKO_TWITTER_INCLUDE_FAILED', true );
                        }
                    
                    } else {
                        define( 'WSKO_TWITTER_INCLUDE_FAILED', true );
                    }
                
                }
                break;
        }
    }
    
    public static function get_current_controller()
    {
        return WSKO_AdminMenu::get_instance()->controller;
    }
    
    public static function get_critical_errors()
    {
        $res = array();
        global  $wp_version ;
        if ( version_compare( $wp_version, '4.4', '<' ) ) {
            $res['incompatible_wp'] = true;
        }
        if ( version_compare( PHP_VERSION, '5.5', '<' ) ) {
            $res['incompatible_php'] = true;
        }
        
        if ( !file_exists( WP_CONTENT_DIR . '/bst/' ) && !is_writable( WP_CONTENT_DIR ) ) {
            $res['content_dir_access'] = true;
        } else {
            if ( !file_exists( WP_CONTENT_DIR . '/bst/' ) ) {
                mkdir( WP_CONTENT_DIR . '/bst/' );
            }
            if ( !is_writable( WP_CONTENT_DIR . '/bst/' ) || !is_readable( WP_CONTENT_DIR . '/bst/' ) ) {
                $res['bst_dir_access'] = true;
            }
        }
        
        return ( empty($res) ? false : $res );
    }
    
    public static function get_data( $refetch = false )
    {
        if ( self::$data_time < time() ) {
            $refetch = true;
        }
        
        if ( !isset( self::$data ) || !self::$data || $refetch ) {
            $wsko_data = false;
            
            if ( $refetch ) {
                global  $wpdb ;
                $row = $wpdb->get_row( "SELECT option_value FROM {$wpdb->options} WHERE option_name='wsko_init' LIMIT 1" );
                if ( $row && is_object( $row ) ) {
                    $wsko_data = unserialize( $row->option_value );
                }
            } else {
                $wsko_data = get_option( 'wsko_init' );
            }
            
            
            if ( !$wsko_data || !is_array( $wsko_data ) ) {
                $wsko_data_t = WSKO_Class_Core::get_data_default();
                WSKO_Class_Core::save_data( $wsko_data_t );
                $wsko_data = $wsko_data_t;
            }
            
            self::$data = $wsko_data;
            self::$data_time = time() + 10;
        }
        
        return self::$data;
    }
    
    public static function save_data( $wsko_data )
    {
        update_option( 'wsko_init', $wsko_data );
        self::$data = $wsko_data;
    }
    
    public static function get_setting( $key )
    {
        $wsko_data = WSKO_Class_Core::get_data();
        if ( isset( $wsko_data['settings'] ) && is_array( $wsko_data['settings'] ) && isset( $wsko_data['settings'][$key] ) && $wsko_data['settings'][$key] ) {
            return $wsko_data['settings'][$key];
        }
    }
    
    public static function save_setting( $key, $val )
    {
        
        if ( $key ) {
            $wsko_data = WSKO_Class_Core::get_data();
            
            if ( isset( $wsko_data['settings'] ) && is_array( $wsko_data['settings'] ) ) {
                $wsko_data['settings'][$key] = $val;
            } else {
                $wsko_data['settings'] = array(
                    $key => $val,
                );
            }
            
            WSKO_Class_Core::save_data( $wsko_data );
        }
    
    }
    
    public static function get_option( $key )
    {
        /*$wsko_data = WSKO_Class_Core::get_data();
        		
        		if (isset($wsko_data['options']) && is_array($wsko_data['options']) && isset($wsko_data['options'][$key]) && $wsko_data['options'][$key])
        		{
        			return $wsko_data['options'][$key];
        		}
        		return false;*/
        $wsko_data = get_option( 'wsko_options' );
        if ( isset( $wsko_data ) && is_array( $wsko_data ) && isset( $wsko_data[$key] ) && $wsko_data[$key] ) {
            return $wsko_data[$key];
        }
        return false;
    }
    
    public static function save_option( $key, $val )
    {
        
        if ( $key ) {
            /*$wsko_data = WSKO_Class_Core::get_data();
            		
            		if (isset($wsko_data['options']) && is_array($wsko_data['options']))
            		{
            			$wsko_data['options'][$key] = $val;
            		}
            		else
            		{
            			$wsko_data['options'] = array($key => $val);
            		}
            		WSKO_Class_Core::save_data($wsko_data);*/
            $wsko_data = get_option( 'wsko_options' );
            
            if ( isset( $wsko_data ) && is_array( $wsko_data ) ) {
                $wsko_data[$key] = $val;
            } else {
                $wsko_data = array(
                    $key => $val,
                );
            }
            
            update_option( 'wsko_options', $wsko_data );
        }
    
    }
    
    public static function get_data_default()
    {
        return array(
            'version' => WSKO_VERSION,
        );
    }
    
    public static function backup_configuration( $auto = false )
    {
        $backups = get_option( 'wsko_backups' );
        if ( !$backups ) {
            $backups = array();
        }
        
        if ( $auto ) {
            $time = WSKO_Class_Helper::get_midnight();
        } else {
            $time = time();
        }
        
        $backup_data_r = get_option( 'wsko_init' );
        
        if ( $backup_data_r && is_array( $backup_data_r ) && isset( $backup_data_r['version'] ) ) {
            //dont save these
            if ( isset( $backup_data_r['search']['se_token'] ) ) {
                unset( $backup_data_r['search']['se_token'] );
            }
            if ( isset( $backup_data_r['search']['an_token'] ) ) {
                unset( $backup_data_r['search']['an_token'] );
            }
            if ( isset( $backup_data_r['onpage_data']['global_analysis']['new_report']['query_running'] ) ) {
                unset( $backup_data_r['onpage_data']['global_analysis']['new_report'] );
            }
            if ( isset( $backup_data_r['onpage_data']['global_analysis']['current_report'] ) ) {
                unset( $backup_data_r['onpage_data']['global_analysis']['current_report'] );
            }
            if ( isset( $backup_data_r['knowledge_base_articles'] ) ) {
                unset( $backup_data_r['knowledge_base_articles'] );
            }
            if ( isset( $backup_data_r['timeout_check'] ) ) {
                unset( $backup_data_r['timeout_check'] );
            }
            if ( isset( $backup_data_r['version'] ) ) {
                unset( $backup_data_r['version'] );
            }
            if ( isset( $backup_data_r['configured'] ) ) {
                unset( $backup_data_r['configured'] );
            }
            //--
            $backups[$time] = $backup_data = array(
                'time' => WSKO_Class_Helper::get_current_time(),
                'auto' => $auto,
                'data' => $backup_data_r,
            );
            update_option( 'wsko_backups', $backups );
        }
    
    }
    
    public static function get_configuration_backup( $key )
    {
        $backups = get_option( 'wsko_backups' );
        if ( isset( $backups[$key] ) ) {
            return $backups[$key];
        }
        return false;
    }
    
    public static function load_configuration_backup( $key )
    {
        $backup = WSKO_Class_Core::get_configuration_backup( $key );
        
        if ( $backup ) {
            $data = $backup['data'];
            $old_data = get_option( 'wsko_init' );
            
            if ( $old_data ) {
                $data = WSKO_Class_Helper::merge_array_deep( $data, $old_data );
                //$data = $data + $old_data;
            }
            
            update_option( 'wsko_init', $data );
        }
    
    }
    
    public static function import_configuration_backup( $data_str, $load = true )
    {
        
        if ( $data_str ) {
            $data = json_decode( $data_str, true );
            
            if ( $data && is_array( $data ) && isset( $data['time'] ) && isset( $data['auto'] ) && isset( $data['data'] ) && isset( $data['key'] ) ) {
                $key = $data['key'];
                unset( $data['key'] );
                
                if ( $load ) {
                    $new_data = $data['data'];
                    $old_data = get_option( 'wsko_init' );
                    if ( $old_data ) {
                        $new_data = WSKO_Class_Helper::merge_array_deep( $new_data, $old_data );
                    }
                    //$new_data = $new_data + $old_data;
                    update_option( 'wsko_init', $new_data );
                } else {
                    //add
                    $backups = get_option( 'wsko_backups' );
                    if ( !$backups ) {
                        $backups = array();
                    }
                    $backups[$key] = $data;
                    update_option( 'wsko_backups', $backups );
                }
                
                return true;
            }
        
        }
        
        return false;
    }
    
    public static function delete_configuration_backup( $key )
    {
        $backups = get_option( 'wsko_backups' );
        if ( !$backups ) {
            $backups = array();
        }
        
        if ( isset( $backups[$key] ) ) {
            unset( $backups[$key] );
            update_option( 'wsko_backups', $backups );
        }
    
    }
    
    public static function get_plugin_language()
    {
        $lang = WSKO_Class_Core::get_setting( 'plugin_lang' );
        
        if ( !$lang ) {
            $lang = get_user_locale();
            if ( !$lang ) {
                $lang = get_locale();
            }
        }
        
        switch ( $lang ) {
            case 'en_EN':
            case 'de_DE':
                break;
            default:
                $lang = 'en_EN';
                break;
        }
        return $lang;
    }

}