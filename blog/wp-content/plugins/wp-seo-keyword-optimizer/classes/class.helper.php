<?php
if (!defined('ABSPATH')) exit;

class WSKO_Class_Helper
{
	public static function is_wsko_cron()
	{
		if (defined('DOING_CRON') && DOING_CRON)
		{
			return true;
		}
		return false;
	}
	public static function is_wsko_ajax()
	{
		if (is_admin())
		{
			if (defined('DOING_AJAX') && DOING_AJAX && isset($_REQUEST['action']) && WSKO_Class_Helper::starts_with($_REQUEST['action'], 'wsko_'))
			{
				return true;
			}
		}
		return false;
	}
	public static function is_wsko_page()
	{
		if (is_admin())
		{
			global $pagenow;
			if ($pagenow === 'admin.php' && isset($_GET['page']) && WSKO_Class_Helper::starts_with($_GET['page'], 'wsko_'))
				return true;
			else if ($pagenow === 'edit.php' || $pagenow === 'post.php')
				return true;
		}
		return false;
	}
	public static function map_special_chars($str)
	{
		if ($str && is_scalar($str))
		{
			return str_replace(array("Ä", "Ö", "Ü", "ä", "ö", "ü", "ß", "´"),
							   array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", ""), $str);
		}
		return "";
	}
	public static function is_term_url($url, $tax)
	{
		$cats = get_terms(array('taxonomy' => $tax, 'hide_empty' => false));
		foreach ($cats as $ct)
		{
			if ($url === WSKO_Class_Helper::get_term_url($ct, $tax))
			{
				return $ct;
			}
		}
		return false;
	}
	public static function get_term_url($term_obj, $tax)
	{
		//if (is_scalar($term_obj) && $term_obj)
			//$term_obj = get_term_by('slug', $term_obj, $tax, OBJECT);
		if ($term_obj && is_object($term_obj))
		{
			return ($term_obj->parent?WSKO_Class_Helper::get_term_url(get_term($term_obj->parent, $tax), $tax).'/':'').$term_obj->slug;
		}
		return false;
	}
	public static function merge_array_deep($arr1, $arr2)
	{
		if (!is_array($arr1) || !is_array($arr2))
			return $arr1;

		foreach ($arr2 as $k => $v)
		{
			if (array_key_exists($k, $arr1))
			{
				if (is_array($arr1[$k]) && is_array($arr2[$k]))
				{
					$arr1[$k] = WSKO_Class_Helper::merge_array_deep($arr1[$k], $arr2[$k]);
				}
				else
				{
				}
			}
			else
			{
				$arr1[$k] = $arr2[$k];
			}
		}
		return $arr1;
	}
	public static function get_obj_rewrite_base($type, $arg)
	{
		$obj = false;
		if ($type === 'post_type')
			$obj = get_post_type_object($arg);
		else if ($type == 'post_tax')
			$obj = get_taxonomy($arg);
		if ($obj)
		{
			$meta_obj = WSKO_Class_Onpage::get_post_meta($arg, $type);
			$auto_redirects = WSKO_Class_Onpage::get_automatic_redirects();
			if (isset($auto_redirects[$type][$arg]['first_slug']) && $auto_redirects[$type][$arg]['first_slug'])
				$first_slug = $curr_slug = $auto_redirects[$type][$arg]['first_slug'];
			else
				$first_slug = $curr_slug = $obj && $obj->rewrite && $obj->rewrite['slug'] ? $obj->rewrite['slug'] : ($arg === 'page' || $arg === 'post' ? '/' : $arg);
			if ($meta_obj && isset($meta_obj['slug']) && $meta_obj['slug'])
				$curr_slug = $meta_obj['slug'];
			if (!$first_slug)
				$first_slug = '/';
			if (!$curr_slug)
				$curr_slug = '/';
			return array('original' => $first_slug, 'current' => $curr_slug);
		}
		return false;
	}
	public static function set_file_download_headers($filename, $size = false)
	{
		$size = intval($size);
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Transfer-Encoding: binary');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		if ($size)
			header('Content-Length: '.$size);
	}
	
	public static function get_http_status_codes($url, $urls, $follow = true)
	{
		$res = array();
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_NOBODY, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1) Gecko/20061024 BonEcho/2.0");

		$html = curl_exec($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$next_url = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
		curl_close($ch);
		$res[]= array('url' => $url, 'code' => $http_code);
		
		if ($follow && $next_url && ($http_code == 301 || $http_code == 302))
		{
			foreach ($urls as $p_url)
			{
				if ($p_url['url'] == $next_url)
				{
					$res[] = array('url' => $url, 'code' => 'loop');
					return $res;
				}
			}
				
			$codes = WSKO_Class_Helper::get_http_status_codes($next_url, $res, $follow);
			if ($codes)
			{
				foreach ($codes as $c)
				{
					$res[] = $c;
				}
			}
		}
		return $res;
	}
	public static function refresh_permalinks()
	{
		//flush_rewrite_rules();//update permalinks
		//global $wp_rewrite;
		//flush_rules();
		WSKO_Class_Core::save_option('refresh_permalinks', true);
	}
	public static function refresh_wp_cache($option, $all = true)
	{
		if ($option)
			wp_cache_get($option, 'options', true);
		if ($all)
		{
			wp_cache_get('alloptions', 'options', true);
			wp_cache_get('notoptions', 'options', true);
		}
		WSKO_Class_Core::get_data(true);
	}
	public static function get_public_post_types($output)
	{
		$names = false;
		if ($output == 'names')
			$names = true;
		$pts = get_post_types(array('public' => true), $output);
		foreach ($pts as $k => $pt)
		{
			$c_v = $k;
			if ($names)
				$c_v = $pt;
			if ($c_v == 'attachment')
				unset($pts[$k]);
		}
		return $pts;
	}
	public static function get_empty_page_title()
	{
		return '<i class="wsko-post-title-empty">Untitled</i>';
	}
	public static function get_default_page_title($title)
	{
		return $title.' - '.get_bloginfo('name');
	}
	public static function sanitize_meta($value, $filter = false)
	{
		if ($filter)
			$value = apply_filters($filter, $value);
		$preg_find = array('/\[[^|\/]vc_.*?\]/');
		$preg_rep = array('');
		$value = preg_replace($preg_find, $preg_rep, $value);
		return WSKO_Class_Helper::get_plain_string(WSKO_Class_Helper::handle_shortcodes($value));
	}
	public static function handle_shortcodes($value)
	{
		if (!is_scalar($value))
			return "";
		return strip_shortcodes(do_shortcode($value));
	}
	public static function get_code_highlighting_register($type)
	{
		switch ($type)
		{
			case 'html':
			return array(
				array('color' => 'orange', 'type' => 1, 'regex' => '(&lt;[^&gt;]*&gt;|&lt;|&gt;)'), //html tags
				array('color' => 'red', 'type' => 1, 'regex' => '(&lt;\?|&lt;\?=|&lt;\?php|\?&gt;)'), //php
				//array('color' => 'red', 'type' => 2, 'regex' => '(&lt;\?)(.*?)(\?&gt;)'), //php
				//array('color' => 'orange', 'type' => 3, 'regex' => '(&lt;.*&gt;)(.*)(&lt;\/.*&gt;)|(&lt;.*[\/]&gt;)'), //html tags
			);
			case 'robots':
			return array(
				array('color' => 'green', 'type' => 1, 'regex' => '(?<=[^s]|^)(allow:)'),
				array('color' => 'red', 'type' => 1, 'regex' => '(disallow:)'),
			);
			case 'htaccess':
			return array(
				array('color' => 'orange', 'type' => 1, 'regex' => '(RewriteBase|)'),
			);
		}
		return array();
	}
	
	public static function get_post_type_labels($post_types)
	{
		$res = array();
		foreach ($post_types as $pt)
		{
			$pt_o = get_post_type_object($pt);
			$res[$pt] = $pt_o->label;
		}
		return $res;
	}
	public static function densify_string($string)
	{
		return strtolower(str_replace(array(' ','-','.','_','.'), '', $string));
	}
	public static function get_post_titles($posts)
	{
		$res = array();
		foreach ($posts as $p)
		{
			$title = esc_html(get_the_title($p));
			if (!$title)
				$title = WSKO_Class_Helper::get_empty_page_title();
			$res[$p] = array('title'=>$title,'url'=>get_permalink($p));
		}
		return $res;
	}
	public static function get_real_post_content($post_id, $format = false)
	{
		$post = get_post($post_id);
		$c = $post->post_content;
		$c = apply_filters('the_content', $c);
		if ($format)
			$c = str_replace(']]>', ']]&gt;', $c);
		return $c;
	}
	public static function post_to_url($url, $data)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1) Gecko/20061024 BonEcho/2.0");

		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	public static function get_from_url($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1) Gecko/20061024 BonEcho/2.0");

		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	
	public static function prepare_data_table($data, $pagination = false, $filter = false, $sort = false, $params = array(), $is_formated_data = false)
	{
		if (empty($data))
			return $data;
		$filtered = 0;
		$total = 0;
		$result = array();
		$spec = isset($params['specific_keys']) ? $params['specific_keys'] : false;
		$format = isset($params['format']) ? $params['format'] : false;
		$search = isset($params['search']) && $params['search'] ? strtolower($params['search']) : false;
		if ($filter)
		{
			foreach ($filter as $k => $f)
			{
				if ($f['comp'] == 'set')
					$filter[$k]['val']= (!$f['val'] || $f['val'] == 'false' || $f['val'] == 'off') ? false : true;
				if ($f['comp'] == 'co')
					$filter[$k]['val']= strtolower($f['val']);
				if ($f['comp'] == 'ra' && !is_array($f['val']))
					$filter[$k]['val'] = explode(':', $f['val']);
			}
		}
		foreach ($data as $d)
		{
			if (is_object($d))
				$d = (array)$d;
			$total++;
			$passed_filter = true;
			if ($search)
			{
				$has_search = false;
				foreach ($d as $v)
				{
					$val = null;
					if ($is_formated_data)
					{
						if (isset($v['order']))
						{
							$val = $v['order'];
						}
						else
						{
							$val = $v['value'];
						}
					}
					else
					{
						$val = $v;
					}
					
					if ((is_string($val) && strpos(strtolower($val), $search) !== false))// || (is_scalar($val) && $val == $search))
					{
						$has_search = true;
						break;
					}
				}
				if (!$has_search)
					$passed_filter = false;
			}
			if ($passed_filter && $filter)
			{
				foreach ($filter as $f)
				{
					$val = null;
					if ($is_formated_data)
					{
						if (isset($d[$f['key']]['order']))
						{
							$val = $d[$f['key']]['order'];
						}
						else
						{
							$val = $d[$f['key']]['value'];
						}
					}
					else
					{
						$val = $d[$f['key']];
					}
					if ($val !== null)
					{
						switch ($f['comp'])
						{
							case 'eq':
							if ($val != $f['val'])
								$passed_filter = false;
							break;
							case 'co':
							if (strpos(strtolower($val), $f['val']) === false)
								$passed_filter = false;
							break;
							case 'ra':
							if ($val < $f['val'][0] || $val > $f['val'][1])
								$passed_filter = false;
							break;
							case 'gt':
							if ($val <= $f['val'])
								$passed_filter = false;
							break;
							case 'lt':
							if ($val >= $f['val'])
								$passed_filter = false;
							break;
							case 'set':
							if (($f['val'] && !$val) || (!$f['val'] && $val))
								$passed_filter = false;
							break;
						}
						
					}
				}
			}
			if (!$passed_filter)
				continue;
			$filtered++;
			$r = array();
			if ($spec)
			{
				foreach ($spec as $s)
				{
					if ($is_formated_data)
					{
						if ($sort && isset($d[$s]['order']) && $s == $sort['key'])
							$r[] = $d[$s];
						else
							$r[] = $d[$s]['value'];
					}
					else
						$r[] = $d[$s];
				}
			}
			else
			{
				foreach ($d as $k => $v)
				{
					if ($is_formated_data)
					{
						if ($sort && isset($d[$k]['order']) && $k == $sort['key'])
							$r[] = $v;
						else
							$r[] = $v['value'];
					}
					else
						$r[] = $v;
				}
			}
			$result[] = $r;
		}
		if ($sort)
		{
			global $wsko_temp_sort_operator;
			$wsko_temp_sort_operator = $sort;
			usort($result, function($a, $b) {
				global $wsko_temp_sort_operator;
				$a_v = isset($a[$wsko_temp_sort_operator['key']]['order']) ? $a[$wsko_temp_sort_operator['key']]['order'] : $a[$wsko_temp_sort_operator['key']];
				$b_v = isset($b[$wsko_temp_sort_operator['key']]['order']) ? $b[$wsko_temp_sort_operator['key']]['order'] : $b[$wsko_temp_sort_operator['key']];
				
				if ($a_v == $b_v)
					return 0;
				if ($wsko_temp_sort_operator['dir'] == 1)
					return $a_v < $b_v ? -1 : 1;
				else
					return $a_v > $b_v ? -1 : 1;
					
			});
		}
		if ($pagination)
		{
			$result = array_slice($result, $pagination['offset'], $pagination['count']);
		}
		foreach($result as $k => $r)
		{
			foreach($r as $k2 => $a)
			{
				if (is_array($a) && isset($a['value'])) $result[$k][$k2] = $a = $a['value'];
				if ($format && isset($format[$k2]))
				{
					if (is_scalar($format[$k2]))
					{
						switch ($format[$k2])
						{
							case 'url_resolve': $result[$k][$k2] = WSKO_Class_Template::render_url_resolve_field($a, true); break;
							case 'url': $result[$k][$k2] = WSKO_Class_Template::render_url_post_field_s($a, array(), true); break;
							case 'url_co': $result[$k][$k2] = WSKO_Class_Template::render_url_post_field_s($a, array('with_co' => true), true); break;
							case 'prog_rad': $result[$k][$k2] = WSKO_Class_Template::render_radial_progress('success', false, array('val' => $a), true); break;
							case 'date': $result[$k][$k2] = date('d.m.Y', $a); break;
							case 'datetime': $result[$k][$k2] = date('d.m.Y H:i', $a); break;
							case 'keyword': $result[$k][$k2] = WSKO_Class_Template::render_keyword_field($a, true); break;
						}
					}
					else
					{
						$result[$k][$k2] = $format[$k2]($a,$r);
					}
				}
			}
		}
		return array('filtered' => $filtered, 'total' => $total, 'data' => $result);
	}
	public static function get_plain_string($string)
	{
		if (!is_scalar($string))
			return "";
		$preg_find = array('/\[.*?\]/', '#<script(.*?)>(.*?)</script>#is', '#<style(.*?)>(.*?)</style>#is');
		$preg_rep = array('', '', '');
		$string = preg_replace($preg_find, $preg_rep, $string);
		return wp_strip_all_tags($string);
	}
	public static function get_word_count($string)
	{
		if (!$string)
			return 0;
		$parts = mb_split('[\s_"]', $string);
		$c = 0;
		foreach($parts as $p)
		{
			if ($p && !ctype_space($p))
				$c++;
		}
		return $c;
		//if (WSKO_Class_Core::get_setting('non_latin_mode'))
			//return count(mb_split('[\s_"]', $string));
		//else
			//return str_word_count($string, 0, '1234567890,.?!_-#');
	}
	public static function get_unique_id($prefix = false)
	{
		return uniqid($prefix);
	}
	public static function get_random_post($post_type, $count = 1)
	{
		$query = new WP_Query(array( 
			'post_type' => $post_type, 
			'posts_per_page' => $count,
			'orderby' => 'rand'
		));
		if ($query && !empty($query->posts))
		{
			if ($count > 1)
				return $query->posts;
			else
				return $query->posts[0];
		}
		return false;
	}
	public static function get_first_post($post_type)
	{
		$query = new WP_Query(array( 
			'post_type' => $post_type, 
			'posts_per_page' => 1,
		));
		if ($query && !empty($query->posts))
		{
			return $query->posts[0];
		}
		return false;
	}
	public static function url_get_info($urls)
	{
		$codes = array();
		$curls = array();
		$mh = curl_multi_init();
		foreach ($urls as $url)
		{
			if (!isset($curls[$url]))
			{
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_HEADER, true);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_NOBODY, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
				curl_setopt($ch, CURLOPT_TIMEOUT, 5);
				curl_setopt($ch, CURLOPT_VERBOSE, false);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1) Gecko/20061024 BonEcho/2.0");
				curl_multi_add_handle($mh, $ch);
				$curls[$url] = $ch;
			}
		}
		$active = null;
		do {
			$mrc = curl_multi_exec($mh, $active);
		} while ($active > 0);
		foreach ($curls as $url => $curl)
		{
			$codes[$url] = array('code' => curl_getinfo($curl, CURLINFO_HTTP_CODE), 'url' => curl_getinfo($curl, CURLINFO_EFFECTIVE_URL));
			curl_multi_remove_handle($mh, $curl);
		}
		return $codes;
	}
	
	public static function url_get_tags($url, $element)
	{
		$html = new DOMDocument();
		$html->recover = TRUE;
		@$html->loadHtmlFile($url);
		$xpath = new DOMXPath($html);
		$res = array();
		switch ($element)
		{
			case 'a-out':
			$nodelist = $xpath->query("//a");// "//a/@href"
			foreach ($nodelist as $n)
			{
				$href = $n->getAttribute('href');
				if ($href)
				{
					$first_char = substr($href, 0, 1);
					if ($first_char != '#' && $first_char != '/' && $first_char != '?')
						$res[]=array('href' => $href, 'anchor' => $n->nodeValue, 'nofollow' => $n->getAttribute('rel') == 'nofollow');
				}
			}
			break;
		}
		return $res;
	}
	
	public static function format_byte($byte)
	{
		if ($byte > 1000)
		{
			$byte = $byte / 1000;
			
			if ($byte > 1000)
			{
				$byte = round($byte / 1000, 2) . ' MB';
			}
			else
				$byte = round($byte, 2) . ' KB';
		}
		else
			$byte = round($byte, 2) . ' B';
		return $byte;
	}
	
	public static function url_get_title($url)
	{
		global $wsko_url_cache;
		
		if (!isset($wsko_url_cache))
			$wsko_url_cache = array(
					'post_types' => array(),
					'terms' => array(),
				);
			
		$res = new stdClass;
		$res->title = '';
		$res->type = 'unknown';
		$res->resolved = false;
		//$res->post_id = $postid;
		
		$postid = WSKO_Class_Helper::url_to_postid($url);
		
		if ($postid)
		{
			$t = get_the_title($postid);
			$res->title = $t ? $t : WSKO_Class_Helper::get_empty_page_title();//.' '. WSKO_Class_Template::render_content_optimizer_link($postid, array(), true);
			$res->title_empty = $t ? false : true;
			$res->type = 'post';
			$res->post_id = $postid;
			$res->resolved = true;
		}
		else 
		{
			$isType = false;
			$post_types = get_post_types(array(), 'names');
			foreach ($post_types as $type)
			{
				if (!isset($wsko_url_cache['post_types'][$type]))
					$url_c = $wsko_url_cache['post_types'][$type] = get_post_type_archive_link($type);
				else
					$url_c = $wsko_url_cache['post_types'][$type];
				
				if ($url_c == $url)
				{
					$type = get_post_type_object($type);
					$res->title = 'Archive - ' . $type->label;
					$res->type = 'archive';
					$res->resolved = true;
					$isType = true;
					break;
				}
			}
			
			if (!$isType)
			{
				$isTerm = false;
				$terms = get_terms();
				foreach ($terms as $term)
				{
					if (!isset($wsko_url_cache['terms'][(string)$term->term_id]))
						$url_c = $wsko_url_cache['terms'][(string)$term->term_id] = get_term_link($term->term_id, $term->taxonomy);
					else
						$url_c = $wsko_url_cache['terms'][(string)$term->term_id];
					
					if ($url_c == $url)
					{
						$res->title = 'Term Archive - ' . $term->name;
						$res->type = 'term';
						$res->resolved = true;
						$isTerm = true;
						break;
					}
				}
			
				if (!$isTerm)
				{
					$res->title = '<i class="wsko-post-not-found">No Wordpress-Post found.</i>';
				}
			}
		}
		
		return $res;
	}

	public static function url_to_postid($url)
	{
		global $wp_rewrite;

		$pid = url_to_postid($url);
		if ($pid)
			return $pid;
		
		$url = apply_filters('url_to_postid', $url);

		// First, check to see if there is a 'p=N' or 'page_id=N' to match against
		if ( preg_match('#[?&](p|page_id|attachment_id)=(\d+)#', $url, $values) )	{
			$id = absint($values[2]);
			if ( $id )
				return $id;
		}

		// Check to see if we are using rewrite rules
		$rewrite = $wp_rewrite->wp_rewrite_rules();

		// Not using rewrite rules, and 'p=N' and 'page_id=N' methods failed, so we're out of options
		if ( empty($rewrite) )
			return 0;

		// Get rid of the #anchor
		$url_split = explode('#', $url);
		$url = $url_split[0];

		// Get rid of URL ?query=string
		$url_split = explode('?', $url);
		$url = $url_split[0];

		// Add 'www.' if it is absent and should be there
		if ( false !== strpos(home_url(), '://www.') && false === strpos($url, '://www.') )
			$url = str_replace('://', '://www.', $url);

		// Strip 'www.' if it is present and shouldn't be
		if ( false === strpos(home_url(), '://www.') )
			$url = str_replace('://www.', '://', $url);

		// Strip 'index.php/' if we're not using path info permalinks
		if ( !$wp_rewrite->using_index_permalinks() )
			$url = str_replace('index.php/', '', $url);

		if ( false !== strpos($url, home_url()) ) {
			// Chop off http://domain.com
			$url = str_replace(home_url(), '', $url);
		} else {
			// Chop off /path/to/blog
			$home_path = parse_url(home_url());
			$home_path = isset( $home_path['path'] ) ? $home_path['path'] : '' ;
			$url = str_replace($home_path, '', $url);
		}

		// Trim leading and lagging slashes
		$url = trim($url, '/');

		$request = $url;
		// Look for matches.
		$request_match = $request;
		foreach ( (array)$rewrite as $match => $query) {
			// If the requesting file is the anchor of the match, prepend it
			// to the path info.
			if ( !empty($url) && ($url != $request) && (strpos($match, $url) === 0) )
				$request_match = $url . '/' . $request;

			if ( preg_match("!^$match!", $request_match, $matches) ) {
				// Got a match.
				// Trim the query of everything up to the '?'.
				$query = preg_replace("!^.+\?!", '', $query);

				// Substitute the substring matches into the query.
				$query = addslashes(WP_MatchesMapRegex::apply($query, $matches));

				// Filter out non-public query vars
				global $wp;
				parse_str($query, $query_vars);
				$query = array();
				foreach ( (array) $query_vars as $key => $value ) {
					if ( in_array($key, $wp->public_query_vars) )
						$query[$key] = $value;
				}

			// Taken from class-wp.php
			foreach ( $GLOBALS['wp_post_types'] as $post_type => $t )
				if ( $t->query_var )
					$post_type_query_vars[$t->query_var] = $post_type;

			foreach ( $wp->public_query_vars as $wpvar ) {
				if ( isset( $wp->extra_query_vars[$wpvar] ) )
					$query[$wpvar] = $wp->extra_query_vars[$wpvar];
				elseif ( isset( $_POST[$wpvar] ) && !is_array( $_POST[$wpvar] ) )
					$query[$wpvar] = sanitize_text_field($_POST[$wpvar]);
				elseif ( isset( $_GET[$wpvar] ) && !is_array( $_GET[$wpvar] ) )
					$query[$wpvar] = sanitize_text_field($_GET[$wpvar]);
				elseif ( isset( $query_vars[$wpvar] ) )
					$query[$wpvar] = $query_vars[$wpvar];

				if ( !empty( $query[$wpvar] ) ) {
					if ( ! is_array( $query[$wpvar] ) ) {
						$query[$wpvar] = (string) $query[$wpvar];
					} else {
						foreach ( $query[$wpvar] as $vkey => $v ) {
							if ( !is_object( $v ) ) {
								$query[$wpvar][$vkey] = (string) $v;
							}
						}
					}

					if ( isset($post_type_query_vars[$wpvar] ) ) {
						$query['post_type'] = $post_type_query_vars[$wpvar];
						$query['name'] = $query[$wpvar];
					}
				}
			}
				// Do the query
				$query = new WP_Query($query);
				if ( !empty($query->posts) && $query->is_singular )
					return $query->post->ID;
				else
					return 0;
			}
		}
		return 0;
	}
	
	public static function format_url($link)
	{
		if (WSKO_Class_Helper::starts_with($link, 'http://') || WSKO_Class_Helper::starts_with($link, 'https://'))
			$link = $link;
		else if (WSKO_Class_Helper::starts_with($link, WSKO_Class_Helper::get_host()))
			$link = home_url(substr($link, strlen(WSKO_Class_Helper::get_host())));
		else
			$link = home_url($link);
		return $link;
	}
	
	public static function starts_with($string, $query)
	{
		return substr($string, 0, strlen($query)) === $query;
	}
	
	public static function ends_with($string, $query)
	{
		return substr($string, strlen($string)-strlen($query)) === $query;
	}
	
	public static function get_host()
	{
		return $_SERVER['HTTP_HOST'];
	}
	public static function ssl_enabled()
	{
		return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && strtolower($_SERVER['HTTPS'] != "off")) ? true : false;
	}
	public static function get_host_base($append_slash = true)
	{
		return defined('WSKO_HOST_BASE') && WSKO_HOST_BASE ? WSKO_HOST_BASE : ((WSKO_Class_Helper::ssl_enabled()?'https':'http') . "://" . $_SERVER['HTTP_HOST'].($append_slash?'/':''));
	}
	
	public static function get_current_url($with_host = true)
	{
		$url = (WSKO_Class_Helper::ssl_enabled()?'https':'http') . "://" . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		if (!$with_host)
			$url = ltrim(str_replace(home_url(), '', $url), '/');
		return $url;
	}
	
	public static function get_effective_url($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1) Gecko/20061024 BonEcho/2.0");
		curl_exec($ch);
		$effective_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		curl_close($ch);
		return $effective_url;
	}
	
	public static function get_effective_urls($urls)
	{
		$eff_urls = array();
		$curls = array();
		$mh = curl_multi_init();
		foreach ($urls as $url)
		{
			if (!isset($curls[$url]))
			{
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_HEADER, true);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_NOBODY, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
				curl_setopt($ch, CURLOPT_TIMEOUT, 5);
				curl_setopt($ch, CURLOPT_VERBOSE, false);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1) Gecko/20061024 BonEcho/2.0");
				curl_multi_add_handle($mh, $ch);
				$curls[$url] = $ch;
			}
		}
		$active = null;
		do {
			$mrc = curl_multi_exec($mh, $active);
		} while ($active > 0);
		foreach ($curls as $url => $curl)
		{
			$eff_urls[$url] = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
			curl_multi_remove_handle($mh, $curl);
		}
		return $eff_urls;
	}
	
	public static function get_current_time()
	{
		return current_time('timestamp');
	}
	public static function get_midnight($time = false)
	{
		if (!$time)
			$time = WSKO_Class_Helper::get_current_time();
		if (is_numeric($time) || is_integer($time))
			$time = date('Y-m-d H:i:s', $time);
		$date = new DateTime($time);
		$date->setTime(0,0,0);
		return $date->getTimestamp();
	}
	
	public static function report_error($type, $title, $var, $additional = false)
	{
		switch ($type)
		{
			case 'exception':
				$type = 'error';
				$c = 'An exception occurred:<br/><br/><pre>' . $var->getMessage() . '</pre>';
				if ($additional)
					$c .= $additional;
				break;
				
			case 'error_dump':
				$type = 'error';
				ob_start();
				var_dump($var);
				$add = ob_get_clean();
				$c = 'An exception occurred:<br/><br/><pre>' . $add . '</pre>';
				if ($additional)
					$c .= $additional;
				break;
				
			case 'warning':
			case 'info':
			case 'error':
				$c = $var;
				if ($additional)
					$c .= '<br/><br/>' . $additional;
				break;
				
			default:
				return;
		}
		
		
		//if (WSKO_Class_Core::get_setting('activate_log'))
		//{
			$args = array(
				'post_type' => WSKO_POST_TYPE_ERROR,
				'post_status' => $type,
				'post_title' => $title,
				'post_content' => $c,
				'post_author' => 0
			);
			wp_insert_post($args);
		//}
	}
	
	public static function add_error_post_type()
	{
		register_post_type(WSKO_POST_TYPE_ERROR,
			array(
			'labels' => array( //just for completeness
				'name' => 'WSKO Log Report',
				'singular_name' => 'WSKO Log Report',
				'add_new' => 'Add a New WSKO Log Report',
				'add_new_item' => 'Add a New WSKO Log Report',
				'edit_item' => 'Edit WSKO Log Report',
				'new_item' => 'New WSKO Log Report',
				'view_item' => 'View WSKO Log Report',
				'search_items' => 'Search WSKO Log Reports',
				'not_found' => 'Nothing Found',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon' => ''
			),
			'description' => 'WSKO Log Reports',
			'public' => false,
			'exclude_from_search' => false,
			'publicly_queryable' => false,
			'show_ui' => false,
			'show_in_nav_menus' => false,
			'show_in_menu' => false,
			'show_in_admin_bar' => false,
			'has_archive' => false,
			'rewrite' => false, //array('slug' => 'p'),
			
			/*'query_var'          => true,
			'capability_type'    => 'post',
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'author')*/
		));
	}
	
	public static function clear_logs($all = false)
	{
		if (!WSKO_POST_TYPE_ERROR || !WSKO_Class_Helper::starts_with(WSKO_POST_TYPE_ERROR, 'wsko_')) //security
			return;
			
		$args = array('post_type' => WSKO_POST_TYPE_ERROR, 'post_status' => 'any', 'numberposts' => -1, 'fields' => 'ids');
		if (!$all)
		{
			$args['date_query'] = array(
				array(
					'column' => 'post_date_gmt',
					'before' => '1 week ago',
				)
			);
		}
		$msgs = get_posts($args);
		if ($msgs)
		{
			foreach ($msgs as $p)
			{
				wp_delete_post($p, true);
			}
		}
	}
	
	public static function format_reports()
	{
		$res = "";
		$msgs = get_posts(array('post_type' => WSKO_POST_TYPE_ERROR, 'post_status' => 'any', 'numberposts' => 20));
		if ($msgs)
		{
			foreach ($msgs as $p)
			{
				$res .= "Status: ".$p->post_status."<br/>".
						"Type:   ".$p->post_title."<br/>".
						"From:   ".get_the_date('d.m.Y H:i:s', $p->ID)."<br/>".
						"<br/><br/>".
						$p->post_content."<br/>".
						"<br/>---------------------------<br/><br/>";
			}
		}
		else
		{
			$res = "No Reports found.";
		}
		return $res;
	}
	
	public static function check_user_permissions($high_value = true)
	{
		$wsko_data = WSKO_Class_Core::get_data();
		
		$user = wp_get_current_user();
		if ($user && $user->ID)
		{
			if (current_user_can('manage_options'))
			{
				return true;
			}
			if (!$high_value)
			{
				$add_roles = explode(',', WSKO_Class_Core::get_setting('additional_permissions'));
				foreach($user->roles as $role)
				{
					if ($add_roles && in_array($role, $add_roles))
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public static function remove_caps()
	{
		global $wp_roles;
		$roles = $wp_roles->get_names(); 
		foreach ($roles as $k => $role)
		{
			$r = get_role($k);
			if ($r)
				$r->remove_cap(WSKO_VIEW_CAP);
		}
	}
	
	public static function is_php_func_enabled($func)
	{
		$disabled = explode(',', ini_get('disable_functions'));
		return !in_array($func, $disabled);
	}

	public static function get_country_name($countryCode)
	{
		$iso_array = array(
			'ABW'=>'Aruba',
			'AFG'=>'Afghanistan',
			'AGO'=>'Angola',
			'AIA'=>'Anguilla',
			'ALA'=>'Åland Islands',
			'ALB'=>'Albania',
			'AND'=>'Andorra',
			'ARE'=>'United Arab Emirates',
			'ARG'=>'Argentina',
			'ARM'=>'Armenia',
			'ASM'=>'American Samoa',
			'ATA'=>'Antarctica',
			'ATF'=>'French Southern Territories',
			'ATG'=>'Antigua and Barbuda',
			'AUS'=>'Australia',
			'AUT'=>'Austria',
			'AZE'=>'Azerbaijan',
			'BDI'=>'Burundi',
			'BEL'=>'Belgium',
			'BEN'=>'Benin',
			'BES'=>'Bonaire, Sint Eustatius and Saba',
			'BFA'=>'Burkina Faso',
			'BGD'=>'Bangladesh',
			'BGR'=>'Bulgaria',
			'BHR'=>'Bahrain',
			'BHS'=>'Bahamas',
			'BIH'=>'Bosnia and Herzegovina',
			'BLM'=>'Saint Barthélemy',
			'BLR'=>'Belarus',
			'BLZ'=>'Belize',
			'BMU'=>'Bermuda',
			'BOL'=>'Bolivia, Plurinational State of',
			'BRA'=>'Brazil',
			'BRB'=>'Barbados',
			'BRN'=>'Brunei Darussalam',
			'BTN'=>'Bhutan',
			'BVT'=>'Bouvet Island',
			'BWA'=>'Botswana',
			'CAF'=>'Central African Republic',
			'CAN'=>'Canada',
			'CCK'=>'Cocos (Keeling) Islands',
			'CHE'=>'Switzerland',
			'CHL'=>'Chile',
			'CHN'=>'China',
			'CIV'=>'Côte d\'Ivoire',
			'CMR'=>'Cameroon',
			'COD'=>'Congo, the Democratic Republic of the',
			'COG'=>'Congo',
			'COK'=>'Cook Islands',
			'COL'=>'Colombia',
			'COM'=>'Comoros',
			'CPV'=>'Cape Verde',
			'CRI'=>'Costa Rica',
			'CUB'=>'Cuba',
			'CUW'=>'Curaçao',
			'CXR'=>'Christmas Island',
			'CYM'=>'Cayman Islands',
			'CYP'=>'Cyprus',
			'CZE'=>'Czech Republic',
			'DEU'=>'Germany',
			'DJI'=>'Djibouti',
			'DMA'=>'Dominica',
			'DNK'=>'Denmark',
			'DOM'=>'Dominican Republic',
			'DZA'=>'Algeria',
			'ECU'=>'Ecuador',
			'EGY'=>'Egypt',
			'ERI'=>'Eritrea',
			'ESH'=>'Western Sahara',
			'ESP'=>'Spain',
			'EST'=>'Estonia',
			'ETH'=>'Ethiopia',
			'FIN'=>'Finland',
			'FJI'=>'Fiji',
			'FLK'=>'Falkland Islands (Malvinas)',
			'FRA'=>'France',
			'FRO'=>'Faroe Islands',
			'FSM'=>'Micronesia, Federated States of',
			'GAB'=>'Gabon',
			'GBR'=>'United Kingdom',
			'GEO'=>'Georgia',
			'GGY'=>'Guernsey',
			'GHA'=>'Ghana',
			'GIB'=>'Gibraltar',
			'GIN'=>'Guinea',
			'GLP'=>'Guadeloupe',
			'GMB'=>'Gambia',
			'GNB'=>'Guinea-Bissau',
			'GNQ'=>'Equatorial Guinea',
			'GRC'=>'Greece',
			'GRD'=>'Grenada',
			'GRL'=>'Greenland',
			'GTM'=>'Guatemala',
			'GUF'=>'French Guiana',
			'GUM'=>'Guam',
			'GUY'=>'Guyana',
			'HKG'=>'Hong Kong',
			'HMD'=>'Heard Island and McDonald Islands',
			'HND'=>'Honduras',
			'HRV'=>'Croatia',
			'HTI'=>'Haiti',
			'HUN'=>'Hungary',
			'IDN'=>'Indonesia',
			'IMN'=>'Isle of Man',
			'IND'=>'India',
			'IOT'=>'British Indian Ocean Territory',
			'IRL'=>'Ireland',
			'IRN'=>'Iran, Islamic Republic of',
			'IRQ'=>'Iraq',
			'ISL'=>'Iceland',
			'ISR'=>'Israel',
			'ITA'=>'Italy',
			'JAM'=>'Jamaica',
			'JEY'=>'Jersey',
			'JOR'=>'Jordan',
			'JPN'=>'Japan',
			'KAZ'=>'Kazakhstan',
			'KEN'=>'Kenya',
			'KGZ'=>'Kyrgyzstan',
			'KHM'=>'Cambodia',
			'KIR'=>'Kiribati',
			'KNA'=>'Saint Kitts and Nevis',
			'KOR'=>'Korea, Republic of',
			'KWT'=>'Kuwait',
			'LAO'=>'Lao People\'s Democratic Republic',
			'LBN'=>'Lebanon',
			'LBR'=>'Liberia',
			'LBY'=>'Libya',
			'LCA'=>'Saint Lucia',
			'LIE'=>'Liechtenstein',
			'LKA'=>'Sri Lanka',
			'LSO'=>'Lesotho',
			'LTU'=>'Lithuania',
			'LUX'=>'Luxembourg',
			'LVA'=>'Latvia',
			'MAC'=>'Macao',
			'MAF'=>'Saint Martin (French part)',
			'MAR'=>'Morocco',
			'MCO'=>'Monaco',
			'MDA'=>'Moldova, Republic of',
			'MDG'=>'Madagascar',
			'MDV'=>'Maldives',
			'MEX'=>'Mexico',
			'MHL'=>'Marshall Islands',
			'MKD'=>'Macedonia, the former Yugoslav Republic of',
			'MLI'=>'Mali',
			'MLT'=>'Malta',
			'MMR'=>'Myanmar',
			'MNE'=>'Montenegro',
			'MNG'=>'Mongolia',
			'MNP'=>'Northern Mariana Islands',
			'MOZ'=>'Mozambique',
			'MRT'=>'Mauritania',
			'MSR'=>'Montserrat',
			'MTQ'=>'Martinique',
			'MUS'=>'Mauritius',
			'MWI'=>'Malawi',
			'MYS'=>'Malaysia',
			'MYT'=>'Mayotte',
			'NAM'=>'Namibia',
			'NCL'=>'New Caledonia',
			'NER'=>'Niger',
			'NFK'=>'Norfolk Island',
			'NGA'=>'Nigeria',
			'NIC'=>'Nicaragua',
			'NIU'=>'Niue',
			'NLD'=>'Netherlands',
			'NOR'=>'Norway',
			'NPL'=>'Nepal',
			'NRU'=>'Nauru',
			'NZL'=>'New Zealand',
			'OMN'=>'Oman',
			'PAK'=>'Pakistan',
			'PAN'=>'Panama',
			'PCN'=>'Pitcairn',
			'PER'=>'Peru',
			'PHL'=>'Philippines',
			'PLW'=>'Palau',
			'PNG'=>'Papua New Guinea',
			'POL'=>'Poland',
			'PRI'=>'Puerto Rico',
			'PRK'=>'Korea, Democratic People\'s Republic of',
			'PRT'=>'Portugal',
			'PRY'=>'Paraguay',
			'PSE'=>'Palestinian Territory, Occupied',
			'PYF'=>'French Polynesia',
			'QAT'=>'Qatar',
			'REU'=>'Réunion',
			'ROU'=>'Romania',
			'RUS'=>'Russian Federation',
			'RWA'=>'Rwanda',
			'SAU'=>'Saudi Arabia',
			'SDN'=>'Sudan',
			'SEN'=>'Senegal',
			'SGP'=>'Singapore',
			'SGS'=>'South Georgia and the South Sandwich Islands',
			'SHN'=>'Saint Helena, Ascension and Tristan da Cunha',
			'SJM'=>'Svalbard and Jan Mayen',
			'SLB'=>'Solomon Islands',
			'SLE'=>'Sierra Leone',
			'SLV'=>'El Salvador',
			'SMR'=>'San Marino',
			'SOM'=>'Somalia',
			'SPM'=>'Saint Pierre and Miquelon',
			'SRB'=>'Serbia',
			'SSD'=>'South Sudan',
			'STP'=>'Sao Tome and Principe',
			'SUR'=>'Suriname',
			'SVK'=>'Slovakia',
			'SVN'=>'Slovenia',
			'SWE'=>'Sweden',
			'SWZ'=>'Swaziland',
			'SXM'=>'Sint Maarten (Dutch part)',
			'SYC'=>'Seychelles',
			'SYR'=>'Syrian Arab Republic',
			'TCA'=>'Turks and Caicos Islands',
			'TCD'=>'Chad',
			'TGO'=>'Togo',
			'THA'=>'Thailand',
			'TJK'=>'Tajikistan',
			'TKL'=>'Tokelau',
			'TKM'=>'Turkmenistan',
			'TLS'=>'Timor-Leste',
			'TON'=>'Tonga',
			'TTO'=>'Trinidad and Tobago',
			'TUN'=>'Tunisia',
			'TUR'=>'Turkey',
			'TUV'=>'Tuvalu',
			'TWN'=>'Taiwan, Province of China',
			'TZA'=>'Tanzania, United Republic of',
			'UGA'=>'Uganda',
			'UKR'=>'Ukraine',
			'UMI'=>'United States Minor Outlying Islands',
			'URY'=>'Uruguay',
			'USA'=>'United States',
			'UZB'=>'Uzbekistan',
			'VAT'=>'Holy See (Vatican City State)',
			'VCT'=>'Saint Vincent and the Grenadines',
			'VEN'=>'Venezuela, Bolivarian Republic of',
			'VGB'=>'Virgin Islands, British',
			'VIR'=>'Virgin Islands, U.S.',
			'VNM'=>'Viet Nam',
			'VUT'=>'Vanuatu',
			'WLF'=>'Wallis and Futuna',
			'WSM'=>'Samoa',
			'YEM'=>'Yemen',
			'ZAF'=>'South Africa',
			'ZMB'=>'Zambia',
			'ZWE'=>'Zimbabwe'
		);
		
		if (isset($iso_array[$countryCode]))
			return $iso_array[$countryCode];
		return "";
	}
}
?>