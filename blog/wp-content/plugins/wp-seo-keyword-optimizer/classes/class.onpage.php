<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class WSKO_Class_Onpage
{
    public static function seo_plugins_disabled()
    {
        return !WSKO_Class_Import::is_seo_plugin_active();
    }
    
    public static function register_hooks()
    {
        $inst = WSKO_Class_Onpage::get_instance();
        
        if ( WSKO_Class_Core::is_configured() && WSKO_Class_Onpage::seo_plugins_disabled() ) {
            add_filter( 'pre_get_document_title', array( $inst, 'get_seo_title' ), 20 );
            add_filter( 'wp_title', array( $inst, 'get_seo_title' ), 20 );
            add_action( 'wp_head', array( $inst, 'get_seo_head' ) );
            remove_action( 'wp_head', 'rel_canonical' );
            //remove default canonical
            add_action( 'template_redirect', array( $inst, 'redirect_pages' ), 0 );
            //add_action('redirect_canonical', array($inst, 'redirect_pages'), 1);
            add_action( 'save_post', function ( $post_id ) {
                $post_type = get_post_type( $post_id );
                $post_types = get_post_types( array(
                    'public' => true,
                ), 'names' );
                if ( in_array( $post_type, $post_types ) ) {
                    WSKO_Class_Core::save_option( 'sitemap_dirty', true );
                }
                WSKO_Class_Onpage::set_op_post_dirty( $post_id, 'save' );
            } );
            add_action(
                'transition_post_status',
                function ( $new_status, $old_status, $post ) {
                
                if ( $new_status != $old_status ) {
                    $post_type = get_post_type( $post->ID );
                    $post_types = get_post_types( array(
                        'public' => true,
                    ), 'names' );
                    if ( in_array( $post_type, $post_types ) ) {
                        WSKO_Class_Core::save_option( 'sitemap_dirty', true );
                    }
                }
            
            },
                10,
                3
            );
            add_filter(
                'register_post_type_args',
                array( $inst, 'change_post_type_slugs' ),
                1000,
                2
            );
            add_filter(
                'register_taxonomy_args',
                array( $inst, 'change_taxonomy_slugs' ),
                1000,
                3
            );
            //add_filter('pre_get_posts', array($inst, 'include_pages_without_slug'), 10);
            //add_filter('parse_query', array($inst, 'include_pages_without_slug_args'), 10);
            add_filter( 'request', array( $inst, 'include_pages_without_slug_args' ), 1000 );
            add_filter( 'single_template', array( $inst, 'redirect_single_templates' ), 10 );
            add_action(
                'save_post',
                array( $inst, 'check_auto_post_redirects' ),
                10,
                3
            );
            add_filter(
                'wp_insert_post_data',
                array( $inst, 'create_auto_post_redirects' ),
                10,
                2
            );
        }
    
    }
    
    public function check_auto_post_redirects( $post_id, $post_args, $update )
    {
        
        if ( $post_id && $update ) {
            $old_post = get_post( $post_id );
            if ( $old_post && !wp_is_post_revision( $post_id ) ) {
                WSKO_Class_Onpage::update_automatic_redirects(
                    'post_id',
                    $post_id,
                    get_permalink( $post_id ),
                    false
                );
            }
        }
    
    }
    
    public function create_auto_post_redirects( $post_args, $postarr )
    {
        
        if ( WSKO_Class_Core::get_setting( 'auto_post_slug_redirects' ) ) {
            $post_id = false;
            if ( isset( $postarr['ID'] ) && $postarr['ID'] ) {
                $post_id = $postarr['ID'];
            }
            
            if ( $post_id ) {
                $old_post = get_post( $post_id );
                
                if ( $old_post && !wp_is_post_revision( $post_id ) && $postarr['post_status'] !== 'draft' ) {
                    $old_link = get_permalink( $post_id );
                    WSKO_Class_Onpage::update_automatic_redirects(
                        'post_id',
                        $post_id,
                        false,
                        true
                    );
                }
            
            }
        
        }
        
        return $post_args;
    }
    
    public function redirect_single_templates( $single_template )
    {
        global  $post ;
        
        if ( $post ) {
            $pt = get_post_type_object( $post->post_type );
            
            if ( $pt ) {
                //$slug = $pt->rewrite ? $pt->rewrite['slug'] : $post->post_type;
                $new_tpl = get_stylesheet_directory() . '/single-' . $post->post_type . '.php';
                if ( file_exists( $new_tpl ) ) {
                    $single_template = $new_tpl;
                }
            }
        
        }
        
        return $single_template;
    }
    
    public function change_post_type_slugs( $args, $post_type )
    {
        $post_type_meta = WSKO_Class_Onpage::get_post_meta( $post_type, 'post_type' );
        if ( $post_type_meta ) {
            /*if (isset($post_type_meta['hide_slug']) && $post_type_meta['hide_slug'])
            		{
            			if (isset($post_type_meta['slug']) && $post_type_meta['slug'])
            				$args['has_archive'] = $post_type_meta['slug'];
            			else if (isset($args['rewrite']['slug']))
            				$args['has_archive'] = $args['rewrite']['slug'];
            			else
            				$args['has_archive'] = $post_type;
            			
            			$args['rewrite']['slug'] = '/';
            			$args['rewrite']['with_front'] = false;
            		}
            		else*/
            if ( isset( $post_type_meta['slug'] ) && $post_type_meta['slug'] ) {
                $args['rewrite']['slug'] = $post_type_meta['slug'];
            }
        }
        return $args;
    }
    
    public function change_taxonomy_slugs( $args, $tax, $obj )
    {
        $tax_meta = WSKO_Class_Onpage::get_post_meta( $tax, 'post_tax' );
        if ( $tax_meta ) {
            if ( isset( $tax_meta['slug'] ) && $tax_meta['slug'] ) {
                $args['rewrite']['slug'] = $tax_meta['slug'];
            }
        }
        return $args;
    }
    
    public function include_pages_without_slug_args( $wp_query )
    {
        if ( is_admin() || defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return $wp_query;
        }
        global  $wsko_first_query ;
        if ( $wsko_first_query ) {
            return $wp_query;
        }
        //$wsko_first_query = true;
        $hide_slug = WSKO_Class_Core::get_setting( 'hide_category_slug' );
        $is_cat = false;
        
        if ( $hide_slug && (isset( $wp_query['name'] ) || isset( $wp_query['pagename'] )) ) {
            $page_t = ( isset( $wp_query['name'] ) ? $wp_query['name'] : $wp_query['pagename'] );
            
            if ( $ct = WSKO_Class_Helper::is_term_url( $page_t, 'category' ) ) {
                $wp_query = array(
                    'category_name' => $ct->slug,
                );
                $is_cat = true;
            }
        
        }
        
        /*if (!$is_cat)
        		{
        			$url = trim(WSKO_Class_Helper::get_current_url(false), '/');
        			$post_types = WSKO_Class_Onpage::get_post_types_without_slug();
        			//unset($post_types['post']);
        			//unset($post_types['page']);
        			foreach($post_types as $pt)
        			{
        				$page = get_page_by_path($url, OBJECT, $pt);
        				if ($page)
        				{
        					if ($pt === 'page')
        						$wp_query = array('page' => '', 'pagename' => $url);
        					else if ($pt === 'post')
        						$wp_query = array('page' => '', 'name' => $url);
        					else
        						$wp_query = array('page' => '', 'post_type' => $pt, $pt => $url, $pt.'name' => $url, 'name' => $url);
        					break;
        				}
        			}
        		}*/
        return $wp_query;
    }
    
    public function get_post_types_without_slug()
    {
        $post_types_a = WSKO_Class_Onpage::get_post_meta_from( 'post_type' );
        $post_types = array( 'post', 'page' );
        foreach ( $post_types_a as $k => $pt ) {
            
            if ( $k && isset( $pt['hide_slug'] ) && $pt['hide_slug'] ) {
                $post_types[] = $k;
            } else {
                if ( ($k === 'post' || $k === 'page') && (!isset( $pt['hide_slug'] ) || !$pt['hide_slug']) ) {
                    unset( $post_types[( $k === 'post' ? 0 : 1 )] );
                }
            }
        
        }
        return $post_types;
    }
    
    public function redirect_pages()
    {
        if ( is_admin() || defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return;
        }
        $hide_slug = WSKO_Class_Core::get_setting( 'hide_category_slug' );
        $hide_slug_redirect = WSKO_Class_Core::get_setting( 'hide_category_slug_redirect' );
        if ( $hide_slug_redirect ) {
            
            if ( $hide_slug ) {
                
                if ( !is_home() && is_category() ) {
                    $target = home_url( WSKO_Class_Helper::get_term_url( get_term_by(
                        'slug',
                        get_query_var( 'category_name' ),
                        'category',
                        OBJECT
                    ), 'category' ) );
                    
                    if ( trim( $target, '/' ) != trim( WSKO_Class_Helper::get_current_url( true ), '/' ) ) {
                        wp_redirect( $target, 301 );
                        exit;
                    }
                
                }
            
            } else {
                $url = trim( WSKO_Class_Helper::get_current_url( false ), '/' );
                
                if ( $ct = WSKO_Class_Helper::is_term_url( $url, 'category' ) ) {
                    wp_redirect( get_term_link( $ct, 'category' ), 301 );
                    exit;
                }
            
            }
        
        }
        $redirect = WSKO_Class_Onpage::get_redirect_for_url( WSKO_Class_Helper::get_current_url() );
        
        if ( $redirect ) {
            wp_redirect( $redirect['to'], $redirect['code'] );
            exit;
        }
    
    }
    
    public static function get_redirect_for_url( $current_url, $test = false )
    {
        $reds = array();
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        $post_id = false;
        
        if ( $test ) {
            $post_id = WSKO_Class_Helper::url_to_postid( $current_url );
        } else {
            global  $post ;
            if ( $post && $post->ID && is_single() ) {
                $post_id = $post->ID;
            }
        }
        
        
        if ( $post_id ) {
            $technical_data = WSKO_Class_Onpage::get_technical_seo_data( $post_id );
            
            if ( $technical_data && isset( $technical_data['redirect'] ) && $technical_data['redirect'] ) {
                $link = WSKO_Class_Helper::format_url( $technical_data['redirect']['to'] );
                $reds[] = $res = array(
                    'type' => 'post',
                    'to'   => $link,
                    'code' => ( $technical_data['redirect']['type'] === 2 ? 302 : 301 ),
                    'data' => ( $test ? array(
                    'post' => $post_id,
                    're'   => $technical_data['redirect'],
                ) : false ),
                );
                if ( !$test ) {
                    return $res;
                }
            }
        
        }
        
        
        if ( isset( $onpage_data['redirects'] ) && $onpage_data['redirects'] ) {
            $page = $_SERVER['REQUEST_URI'];
            foreach ( $onpage_data['redirects'] as $key => $redirect ) {
                $hit = false;
                $page_link = WSKO_Class_Helper::format_url( $redirect['page'] );
                switch ( $redirect['comp'] ) {
                    //case 'starts_with': if (WSKO_Class_Helper::starts_with($current_url, $page_link)) $hit = true; break;
                    case 'exact':
                        if ( $page_link === $current_url || $page_link . '/' === $current_url ) {
                            $hit = true;
                        }
                        break;
                    case 'contains':
                        if ( stripos( $current_url, $redirect['page'] ) !== false ) {
                            $hit = true;
                        }
                        break;
                        //case 'replace': if (stripos($current_url, $redirect['page']) !== false) { $hit = true; $redirect['target'] = str_ireplace($redirect['page'], $redirect['target'], $current_url); } break;
                }
                
                if ( $hit ) {
                    if ( isset( $redirect['comp_to'] ) && $redirect['comp_to'] === 'replace' ) {
                        $redirect['target'] = str_ireplace( $redirect['page'], $redirect['target'], WSKO_Class_Helper::get_current_url() );
                    }
                    $reds[] = $res = array(
                        'type' => 'custom',
                        'to'   => WSKO_Class_Helper::format_url( $redirect['target'] ),
                        'code' => ( $redirect['type'] === 2 ? 302 : 301 ),
                        'data' => ( $test ? array(
                        'key' => $key,
                        're'  => $redirect,
                    ) : false ),
                    );
                    if ( !$test ) {
                        return $res;
                    }
                }
            
            }
        }
        
        if ( isset( $onpage_data['auto_redirects'] ) && $onpage_data['auto_redirects'] ) {
            foreach ( $onpage_data['auto_redirects'] as $type => $types ) {
                foreach ( $types as $arg => $redirects ) {
                    
                    if ( $type == 'post_id' ) {
                        foreach ( $redirects as $key => $link ) {
                            
                            if ( $link === $current_url ) {
                                $reds[] = $res = array(
                                    'type' => 'auto_post',
                                    'to'   => get_permalink( $arg ),
                                    'code' => 301,
                                    'data' => ( $test ? array(
                                    'type' => $type,
                                    'arg'  => $arg,
                                    'key'  => $key,
                                    'from' => $link,
                                    're'   => $redirects,
                                ) : false ),
                                );
                                if ( !$test ) {
                                    return $res;
                                }
                            }
                        
                        }
                    } else {
                        $sources = $redirects['source'];
                        if ( $sources ) {
                            foreach ( $sources as $slug => $link_snapshot ) {
                                foreach ( $link_snapshot as $p => $link ) {
                                    
                                    if ( $link === $current_url ) {
                                        $reds[] = $res = array(
                                            'type' => 'auto_post_type',
                                            'to'   => ( $type == 'post_tax' ? get_term_link( intval( $p ), $arg ) : get_permalink( intval( $p ) ) ),
                                            'code' => 301,
                                            'data' => ( $test ? array(
                                            'type' => $type,
                                            'arg'  => $arg,
                                            'slug' => $slug,
                                            're'   => $redirects,
                                        ) : false ),
                                        );
                                        if ( !$test ) {
                                            return $res;
                                        }
                                    }
                                
                                }
                            }
                        }
                    }
                
                }
            }
        }
        if ( is_404() && isset( $onpage_data['redirect_404'] ) && $onpage_data['redirect_404']['activate'] && !$test ) {
            switch ( $onpage_data['redirect_404']['type'] ) {
                case '1':
                    $res = array(
                        'type' => '404',
                        'to'   => home_url(),
                        'code' => 301,
                    );
                    if ( !$test ) {
                        return $res;
                    }
                    break;
                case '2':
                    $res = array(
                        'type' => '404',
                        'to'   => WSKO_Class_Helper::get_host_base(),
                        'code' => 301,
                    );
                    if ( !$test ) {
                        return $res;
                    }
                    break;
                case '3':
                    $res = array(
                        'type' => '404',
                        'to'   => WSKO_Class_Helper::format_url( $onpage_data['redirect_404']['custom'] ),
                        'code' => 301,
                    );
                    if ( !$test ) {
                        return $res;
                    }
                    break;
                case '4':
                    $res = array(
                        'type' => '404',
                        'to'   => dirname( dirname( $current_url ) ),
                        'code' => 301,
                    );
                    if ( !$test ) {
                        return $res;
                    }
                    break;
            }
        }
        if ( $test ) {
            return $reds;
        }
    }
    
    public static function update_automatic_redirects(
        $type,
        $arg,
        $new_slug,
        $add_new = true
    )
    {
        
        if ( $type && $arg ) {
            // && $new_slug)
            $onpage_data = WSKO_Class_Onpage::get_onpage_data();
            $redirects = array();
            if ( isset( $onpage_data['auto_redirects'] ) && $onpage_data['auto_redirects'] ) {
                $redirects = $onpage_data['auto_redirects'];
            }
            if ( !isset( $redirects[$type] ) ) {
                $redirects[$type] = array();
            }
            $old_slug = false;
            
            if ( $type == 'post_id' ) {
                //$old_slug = get_post_field('post_name', $arg);
                $old_link = get_permalink( intval( $arg ) );
                
                if ( !isset( $redirects[$type][$arg] ) ) {
                    $redirects[$type][$arg] = array();
                    if ( $add_new ) {
                        $redirects[$type][$arg] = array( $old_link );
                    }
                } else {
                    if ( $add_new && !in_array( $old_link, $redirects[$type][$arg] ) ) {
                        $redirects[$type][$arg][] = $old_link;
                    }
                }
                
                if ( $new_slug && ($key = array_search( $new_slug, $redirects[$type][$arg] )) !== false ) {
                    unset( $redirects[$type][$arg][$key] );
                }
                if ( !$redirects[$type][$arg] ) {
                    unset( $redirects[$type][$arg] );
                }
                $onpage_data['auto_redirects'] = $redirects;
                WSKO_Class_Onpage::set_onpage_data( $onpage_data );
            } else {
                $obj = false;
                
                if ( $type === 'post_type' ) {
                    $obj = get_post_type_object( $arg );
                } else {
                    if ( $type === 'post_tax' ) {
                        $obj = get_taxonomy( $arg );
                    }
                }
                
                
                if ( $obj ) {
                    $old_slug = ( $obj->rewrite ? $obj->rewrite['slug'] : '/' );
                    
                    if ( !isset( $redirects[$type][$arg] ) ) {
                        if ( $new_slug === false ) {
                            $new_slug = $old_slug;
                        }
                        $redirects[$type][$arg] = array(
                            'first_slug' => $old_slug,
                            'target'     => $new_slug,
                            'source'     => array(),
                        );
                    } else {
                        if ( $new_slug === false ) {
                            $new_slug = $redirects[$type][$arg]['first_slug'];
                        }
                        $redirects[$type][$arg]['target'] = $new_slug;
                    }
                    
                    if ( $add_new ) {
                        $redirects[$type][$arg]['source'][$old_slug] = WSKO_Class_Onpage::get_link_snapshot( $type, $arg );
                    }
                    if ( $new_slug !== false ) {
                        unset( $redirects[$type][$arg]['source'][$new_slug] );
                    }
                    if ( !$redirects[$type][$arg]['source'] ) {
                        unset( $redirects[$type][$arg] );
                    }
                    $onpage_data['auto_redirects'] = $redirects;
                    WSKO_Class_Onpage::set_onpage_data( $onpage_data );
                }
            
            }
        
        }
    
    }
    
    public static function remove_auto_redirect( $type, $arg, $key )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        $redirects = array();
        if ( isset( $onpage_data['auto_redirects'] ) && $onpage_data['auto_redirects'] ) {
            $redirects = $onpage_data['auto_redirects'];
        }
        
        if ( $key !== false ) {
            
            if ( $type === 'post_id' ) {
                
                if ( $redirects && isset( $redirects[$type][$arg][$key] ) ) {
                    unset( $redirects[$type][$arg][$key] );
                    if ( !$redirects[$type][$arg] ) {
                        unset( $redirects[$type][$arg] );
                    }
                }
            
            } else {
                
                if ( $redirects && isset( $redirects[$type][$arg]['source'][$key] ) ) {
                    unset( $redirects[$type][$arg]['source'][$key] );
                    if ( !$redirects[$type][$arg]['source'] ) {
                        unset( $redirects[$type][$arg] );
                    }
                }
            
            }
        
        } else {
            if ( $redirects && isset( $redirects[$type][$arg] ) ) {
                unset( $redirects[$type][$arg] );
            }
        }
        
        $onpage_data['auto_redirects'] = $redirects;
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
    }
    
    public static function get_link_snapshot( $type, $arg )
    {
        $res = array();
        
        if ( $type == 'post_type' ) {
            $query = new WP_Query( array(
                'posts_per_page' => -1,
                'fields'         => 'ids',
                'post_status'    => 'publish',
                'post_type'      => $arg,
            ) );
            foreach ( $query->posts as $p ) {
                $res[$p] = get_permalink( $p );
            }
        } else {
            
            if ( $type == 'post_tax' ) {
                //$args['tax_query'] = array(array('value' => $arg, 'compare' => 'EXISTS'));
                $query = get_terms( array(
                    'taxonomy'   => $arg,
                    'hide_empty' => false,
                ) );
                foreach ( $query as $t ) {
                    $res[$t->term_id] = get_term_link( $t, $arg );
                }
            }
        
        }
        
        return $res;
    }
    
    public static function get_automatic_redirects()
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( isset( $onpage_data['auto_redirects'] ) && $onpage_data['auto_redirects'] ) {
            return $onpage_data['auto_redirects'];
        }
        return false;
    }
    
    public function get_seo_title( $old_title )
    {
        if ( !WSKO_Class_Onpage::seo_plugins_disabled() ) {
            return;
        }
        global  $post, $wsko_onpage_analysis_test ;
        $metas = WSKO_Class_Onpage::get_post_metas( ( $wsko_onpage_analysis_test ? true : false ) );
        //$metas = WSKO_Class_Onpage::get_post_metas();
        
        if ( $metas && isset( $metas['title'] ) && $metas['title'] ) {
            $meta = WSKO_Class_Onpage::calculate_meta( $metas['title'] );
            if ( $meta ) {
                return $meta;
            }
        }
        
        
        if ( is_tax() || is_category() || is_tag() ) {
            $queried_object = get_queried_object();
            
            if ( $queried_object && $queried_object->taxonomy ) {
                $taxonomy = $queried_object->taxonomy;
                $term = $queried_object->term_id;
                
                if ( $term ) {
                    $term = get_term_by( 'id', $term, $taxonomy );
                    return WSKO_Class_Helper::get_default_page_title( $term->name );
                } else {
                    $tax = get_taxonomy( $taxonomy );
                    return WSKO_Class_Helper::get_default_page_title( $tax->label );
                }
            
            }
        
        } else {
            if ( $post ) {
                return WSKO_Class_Helper::get_default_page_title( get_the_title( $post->ID ) );
            }
        }
        
        return $old_title;
    }
    
    public function get_seo_head()
    {
        if ( !WSKO_Class_Onpage::seo_plugins_disabled() ) {
            return;
        }
        global  $post ;
        $auto_snippet = WSKO_Class_Core::get_setting( 'auto_social_snippet' );
        $auto_thumbnail = WSKO_Class_Core::get_setting( 'auto_social_thumbnail' );
        $m_title = "";
        $m_desc = "";
        $m_tw_title = "";
        $m_tw_desc = "";
        $m_tw_img = "";
        $m_tw_url = "";
        $m_og_title = "";
        $m_og_desc = "";
        $m_og_img = "";
        $m_og_url = "";
        $m_canon = "";
        $m_og_type = "";
        global  $wsko_onpage_analysis_test ;
        $metas = WSKO_Class_Onpage::get_post_metas( ( $wsko_onpage_analysis_test ? true : false ) );
        
        if ( $metas ) {
            
            if ( isset( $metas['desc'] ) && $metas['desc'] ) {
                $meta = WSKO_Class_Onpage::calculate_meta( $metas['desc'] );
                if ( $meta ) {
                    $m_desc = $meta;
                }
            }
            
            
            if ( isset( $metas['og_title'] ) && $metas['og_title'] ) {
                $meta = WSKO_Class_Onpage::calculate_meta( $metas['og_title'] );
                if ( $meta ) {
                    $m_og_title = $meta;
                }
            } else {
                
                if ( isset( $metas['title'] ) && $metas['title'] ) {
                    $meta = WSKO_Class_Onpage::calculate_meta( $metas['title'] );
                    if ( $meta ) {
                        $m_og_title = $meta;
                    }
                }
            
            }
            
            
            if ( isset( $metas['og_desc'] ) && $metas['og_desc'] ) {
                $meta = WSKO_Class_Onpage::calculate_meta( $metas['og_desc'] );
                if ( $meta ) {
                    $m_og_desc = $meta;
                }
            } else {
                
                if ( isset( $metas['desc'] ) && $metas['desc'] ) {
                    $meta = WSKO_Class_Onpage::calculate_meta( $metas['desc'] );
                    if ( $meta ) {
                        $m_og_desc = $meta;
                    }
                }
            
            }
            
            if ( isset( $metas['og_img'] ) && $metas['og_img'] ) {
                $m_og_img = $metas['og_img'];
            }
            
            if ( isset( $metas['tw_title'] ) && $metas['tw_title'] ) {
                $meta = WSKO_Class_Onpage::calculate_meta( $metas['tw_title'] );
                if ( $meta ) {
                    $m_tw_title = $meta;
                }
            } else {
                
                if ( isset( $metas['title'] ) && $metas['title'] ) {
                    $meta = WSKO_Class_Onpage::calculate_meta( $metas['title'] );
                    if ( $meta ) {
                        $m_tw_title = $meta;
                    }
                }
            
            }
            
            
            if ( isset( $metas['tw_desc'] ) && $metas['tw_desc'] ) {
                $meta = WSKO_Class_Onpage::calculate_meta( $metas['tw_desc'] );
                if ( $meta ) {
                    $m_tw_desc = $meta;
                }
            } else {
                
                if ( isset( $metas['desc'] ) && $metas['desc'] ) {
                    $meta = WSKO_Class_Onpage::calculate_meta( $metas['desc'] );
                    if ( $meta ) {
                        $m_tw_desc = $meta;
                    }
                }
            
            }
            
            if ( isset( $metas['tw_img'] ) && $metas['tw_img'] ) {
                $m_tw_img = $metas['tw_img'];
            }
            if ( isset( $metas['robots'] ) ) {
                switch ( $metas['robots'] ) {
                    case 1:
                        echo  '<meta name="robots" content="nofollow">' ;
                        break;
                    case 2:
                        echo  '<meta name="robots" content="noindex">' ;
                        break;
                    case 3:
                        echo  '<meta name="robots" content="noindex,nofollow">' ;
                        break;
                }
            }
        }
        
        
        if ( $post && $post->ID ) {
            //$content = substr(WSKO_Class_Helper::sanitize_meta($post->post_content, 'the_content'), 0, WSKO_ONPAGE_DESC_MAX-10);
            if ( !$m_title ) {
                $m_title = WSKO_Class_Helper::sanitize_meta( get_the_title( $post->ID ), false ) . ' - ' . get_bloginfo( 'name' );
            }
            //if (!$m_desc)
            //$m_desc = $content;
            
            if ( $auto_snippet ) {
                if ( !$m_tw_title ) {
                    $m_tw_title = $m_title;
                }
                if ( !$m_og_title ) {
                    $m_og_title = $m_title;
                }
                if ( !$m_tw_desc ) {
                    $m_tw_desc = $m_desc;
                }
                if ( !$m_og_desc ) {
                    $m_og_desc = $m_desc;
                }
            }
            
            
            if ( $auto_thumbnail && has_post_thumbnail( $post->ID ) ) {
                if ( ($auto_snippet || $m_tw_title || $m_tw_desc) && !$m_tw_img ) {
                    $m_tw_img = get_the_post_thumbnail_url( $post->ID );
                }
                if ( ($auto_snippet || $m_og_title || $m_og_desc) && !$m_og_img ) {
                    $m_og_img = get_the_post_thumbnail_url( $post->ID );
                }
            }
            
            $t_seo_data = WSKO_Class_Onpage::get_technical_seo_data( $post->ID );
            if ( $t_seo_data ) {
                if ( isset( $t_seo_data['canonical'] ) && $t_seo_data['canonical'] ) {
                    
                    if ( $t_seo_data['canonical']['type'] == '1' ) {
                        $m_canon = get_permalink( $post->ID );
                    } else {
                        
                        if ( $t_seo_data['canonical']['type'] == '2' ) {
                            $m_canon = get_permalink( $t_seo_data['canonical']['arg'] );
                        } else {
                            if ( $t_seo_data['canonical']['type'] == '3' ) {
                                $m_canon = WSKO_Class_Helper::format_url( $t_seo_data['canonical']['arg'] );
                            }
                        }
                    
                    }
                
                }
            }
            if ( !$m_canon && WSKO_Class_Core::get_setting( 'auto_canonical' ) ) {
                $m_canon = get_permalink( $post->ID );
            }
            
            if ( $auto_snippet || $m_og_title || $m_og_desc ) {
                $m_og_url = get_permalink( $post->ID );
                
                if ( get_post_format( $post->ID ) ) {
                    $m_og_type = "article";
                } else {
                    $m_og_type = "website";
                }
            
            }
            
            if ( $auto_snippet || $m_tw_title || $m_tw_desc ) {
                $m_tw_url = get_permalink( $post->ID );
            }
        }
        
        if ( $m_desc ) {
            echo  '<meta name="description" content="' . esc_attr( $m_desc ) . '">' ;
        }
        if ( $m_tw_title ) {
            echo  '<meta property="twitter:title" content="' . esc_attr( $m_tw_title ) . '">' ;
        }
        if ( $m_tw_desc ) {
            echo  '<meta property="twitter:description" content="' . esc_attr( $m_tw_desc ) . '">' ;
        }
        if ( $m_tw_img ) {
            echo  '<meta property="twitter:image" content="' . esc_attr( $m_tw_img ) . '">' ;
        }
        if ( $m_og_title ) {
            echo  '<meta property="og:title" content="' . esc_attr( $m_og_title ) . '">' ;
        }
        if ( $m_og_desc ) {
            echo  '<meta property="og:description" content="' . esc_attr( $m_og_desc ) . '">' ;
        }
        if ( $m_og_img ) {
            echo  '<meta property="og:image" content="' . esc_attr( $m_og_img ) . '">' ;
        }
        
        if ( $m_canon ) {
            echo  '<link rel="canonical" href="' . $m_canon . '"/>' ;
        } else {
            if ( WSKO_Class_Core::get_setting( 'auto_canonical' ) ) {
                echo  '<link rel="canonical" href="' . ((( isset( $_SERVER['HTTPS'] ) ? "https" : "http" )) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}") . '"/>' ;
            }
        }
        
        
        if ( $m_tw_url ) {
            echo  '<meta property="twitter:url" content="' . esc_attr( $m_tw_url ) . '">' ;
        } else {
            if ( $auto_snippet ) {
                echo  '<meta property="twitter:url" content="' . ((( isset( $_SERVER['HTTPS'] ) ? "https" : "http" )) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}") . '">' ;
            }
        }
        
        
        if ( $m_og_url ) {
            echo  '<meta property="og:url" content="' . esc_attr( $m_og_url ) . '">' ;
        } else {
            if ( $auto_snippet ) {
                echo  '<meta property="og:url" content="' . ((( isset( $_SERVER['HTTPS'] ) ? "https" : "http" )) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}") . '">' ;
            }
        }
        
        if ( $m_og_type ) {
            echo  '<meta property="og:type" content="' . $m_og_type . '">' ;
        }
    }
    
    public static function get_onpage_data()
    {
        $wsko_data = WSKO_Class_Core::get_data();
        
        if ( !isset( $wsko_data['onpage_data'] ) || !is_array( $wsko_data['onpage_data'] ) ) {
            $onpage_data = array(
                'post_metas'      => array(),
                'post_type_metas' => array(),
            );
        } else {
            $onpage_data = $wsko_data['onpage_data'];
        }
        
        return $onpage_data;
    }
    
    public static function set_onpage_data( $data )
    {
        $wsko_data = WSKO_Class_Core::get_data();
        $wsko_data['onpage_data'] = $data;
        WSKO_Class_Core::save_data( $wsko_data );
    }
    
    public static function get_technical_seo_data( $post_id = false )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        
        if ( $post_id ) {
            if ( isset( $onpage_data['technical_seo'][$post_id] ) ) {
                return $onpage_data['technical_seo'][$post_id];
            }
        } else {
            if ( isset( $onpage_data['technical_seo'] ) ) {
                return $onpage_data['technical_seo'];
            }
        }
        
        return false;
    }
    
    public static function set_technical_seo_data( $post_id, $data )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        
        if ( isset( $onpage_data['technical_seo'] ) && is_array( $onpage_data['technical_seo'] ) ) {
            
            if ( $data ) {
                $onpage_data['technical_seo'][$post_id] = $data;
            } else {
                unset( $onpage_data['technical_seo'][$post_id] );
            }
        
        } else {
            if ( $data ) {
                $onpage_data['technical_seo'] = array(
                    $post_id => $data,
                );
            }
        }
        
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
    }
    
    public static function get_onpage_analysis()
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( isset( $onpage_data['global_analysis'] ) ) {
            return $onpage_data['global_analysis'];
        }
        return false;
    }
    
    public static function has_current_report()
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( isset( $onpage_data['global_analysis']['current_report'] ) && $onpage_data['global_analysis']['current_report'] ) {
            return true;
        }
        return false;
    }
    
    public static function get_onpage_report( $post_id, $preview = false, $log_error = true )
    {
        try {
            $post_t = get_post( $post_id );
            global  $post ;
            $post = $post_t;
            setup_postdata( $post );
            $res = array(
                'onpage_score' => 100,
                'issues'       => array(),
            );
            $h1_pt = explode( ",", WSKO_Class_Core::get_setting( 'onpage_title_h1' ) );
            $h1_outside = in_array( $post->post_type, $h1_pt );
            $link = get_permalink( $post_id );
            
            if ( $preview && $preview['post_slug'] ) {
                $ends_with_dash = substr( $link, strlen( $link ) - 1 ) == '/';
                $link = explode( '/', rtrim( $link, '/' ) );
                array_pop( $link );
                $link[] = $preview['post_slug'];
                $link = implode( '/', $link ) . (( $ends_with_dash ? '/' : '' ));
            }
            
            $res['link'] = $link;
            ob_start();
            global  $wsko_onpage_analysis_test ;
            $wsko_onpage_analysis_test = true;
            get_header();
            do_action( 'wp_head' );
            //fallback header
            $wsko_onpage_analysis_test = false;
            $head = '<div>' . ob_get_clean() . '</div>';
            $dom = new DOMDocument();
            $dom->recover = TRUE;
            @$dom->loadHTML( $head );
            $title = "";
            $titles = $dom->getElementsByTagName( 'title' );
            if ( $titles->length ) {
                $title = utf8_decode( $titles->item( 0 )->nodeValue );
            }
            //$title = WSKO_Class_Onpage::get_instance()->get_seo_title($title);
            $desc = false;
            $og_title = false;
            $og_desc = false;
            $og_img = false;
            $tw_title = false;
            $tw_desc = false;
            $tw_img = false;
            $metas = $dom->getElementsByTagName( 'meta' );
            foreach ( $metas as $m ) {
                if ( $desc === false ) {
                    if ( $m->getAttribute( 'name' ) == "description" || $m->getAttribute( 'property' ) == "description" ) {
                        $desc = $m->getAttribute( 'content' );
                    }
                }
                if ( $og_title === false ) {
                    if ( $m->getAttribute( 'name' ) == "og:title" || $m->getAttribute( 'property' ) == "og:title" ) {
                        $og_title = $m->getAttribute( 'content' );
                    }
                }
                if ( $og_desc === false ) {
                    if ( $m->getAttribute( 'name' ) == "og:description" || $m->getAttribute( 'property' ) == "og:description" ) {
                        $og_desc = $m->getAttribute( 'content' );
                    }
                }
                if ( $og_img === false ) {
                    if ( $m->getAttribute( 'name' ) == "og:image" || $m->getAttribute( 'property' ) == "og:image" ) {
                        $og_img = $m->getAttribute( 'content' );
                    }
                }
                if ( $tw_title === false ) {
                    if ( $m->getAttribute( 'name' ) == "twitter:title" || $m->getAttribute( 'property' ) == "twitter:title" ) {
                        $tw_title = $m->getAttribute( 'content' );
                    }
                }
                if ( $tw_desc === false ) {
                    if ( $m->getAttribute( 'name' ) == "twitter:description" || $m->getAttribute( 'property' ) == "twitter:description" ) {
                        $tw_desc = $m->getAttribute( 'content' );
                    }
                }
                if ( $tw_img === false ) {
                    if ( $m->getAttribute( 'name' ) == "twitter:image" || $m->getAttribute( 'property' ) == "twitter:image" ) {
                        $tw_img = $m->getAttribute( 'content' );
                    }
                }
            }
            $res['meta_title'] = $title;
            $res['meta_desc'] = ( $desc ? $desc : "" );
            $res['meta_og_title'] = ( $og_title ? $og_title : "" );
            $res['meta_og_desc'] = ( $og_desc ? $og_desc : "" );
            $res['meta_og_img'] = ( $og_img ? $og_img : "" );
            $res['meta_tw_title'] = ( $tw_title ? $tw_title : "" );
            $res['meta_tw_desc'] = ( $tw_desc ? $tw_desc : "" );
            $res['meta_tw_img'] = ( $tw_img ? $tw_img : "" );
            $title_length = ( $title ? mb_strlen( $title ) : 0 );
            $desc_length = ( $desc ? mb_strlen( $desc ) : 0 );
            $content = WSKO_Class_Helper::handle_shortcodes( ( $preview && $preview['post_content'] ? $preview['post_content'] : WSKO_Class_Helper::get_real_post_content( $post_id ) ) );
            $content_plain = WSKO_Class_Helper::get_plain_string( $content );
            $content_length = ( $content_plain ? mb_strlen( $content_plain ) : 0 );
            $word_count = WSKO_Class_Helper::get_word_count( $content_plain );
            $res['issues']['html_errors'] = 1;
            libxml_use_internal_errors( true );
            $dom = new DOMDocument();
            $dom->recover = TRUE;
            $content = '<div>' . $content . (( has_post_thumbnail( $post_id ) ? get_the_post_thumbnail( $post_id ) : '' )) . '</div>';
            
            if ( $dom->loadHTML( $content ) ) {
                $h1s = $dom->getElementsByTagName( 'h1' );
                $count_h1 = $h1s->length + (( $h1_outside ? 1 : 0 ));
                $h2s = $dom->getElementsByTagName( 'h2' );
                $count_h2 = $h2s->length;
                $h3s = $dom->getElementsByTagName( 'h3' );
                $count_h3 = $h3s->length;
                $imgs = $dom->getElementsByTagName( 'img' );
                $videos = $dom->getElementsByTagName( 'video' );
                $iframes = $dom->getElementsByTagName( 'iframe' );
                $tags = array(
                    'h1' => $count_h1,
                    'h2' => $h2s->length,
                    'h3' => $h3s->length,
                    'h4' => $dom->getElementsByTagName( 'h4' )->length,
                    'h5' => $dom->getElementsByTagName( 'h5' )->length,
                    'h6' => $dom->getElementsByTagName( 'h6' )->length,
                );
            } else {
                $h1s = array();
                $count_h1 = 0 + (( $h1_outside ? 1 : 0 ));
                $h2s = array();
                $count_h2 = 0;
                $h3s = array();
                $count_h3 = 0;
                $imgs = false;
                $videos = false;
                $iframes = false;
                $tags = array(
                    'h1' => 0,
                    'h2' => 0,
                    'h3' => 0,
                    'h4' => 0,
                    'h5' => 0,
                    'h6' => 0,
                );
                $res['issues']['html_errors'] = 0;
            }
            
            $errors = libxml_get_errors();
            if ( $errors ) {
                $res['issues']['html_errors'] = 0;
            }
            $res['issues']['html_error_infos'] = $errors;
            libxml_clear_errors();
            $res['tags'] = $tags;
            $res['content'] = $content_plain;
            $res['content_length'] = $content_length;
            $res['word_count'] = $word_count;
            $has_prio1 = 0;
            $title_has_prio1 = false;
            $desc_has_prio1 = false;
            $url_has_prio1 = false;
            $h1_has_prio1 = 0;
            $prio1_h1_den = 0;
            $keyword_den = array();
            $kw_score = 0;
            $keywords = WSKO_Class_Onpage::get_priority_keywords( $post_id );
            
            if ( $keywords && !empty($keywords) ) {
                $kw_eff_sum = 0;
                $kw_eff_c = 0;
                foreach ( $keywords as $kw => $prio ) {
                    $den_type = 0;
                    $kw_eff = 0;
                    $kw_den = 0;
                    $kw_san = WSKO_Class_Helper::densify_string( $kw );
                    
                    if ( $word_count ) {
                        $kw_den = substr_count( WSKO_Class_Helper::densify_string( $content_plain ), WSKO_Class_Helper::densify_string( $kw ) );
                        if ( $kw_den ) {
                            $kw_den = $kw_den / $word_count * 100;
                        }
                    }
                    
                    switch ( $prio ) {
                        case 1:
                            
                            if ( $kw_den >= 0.5 && $kw_den < 1 ) {
                                $kw_eff = ($kw_den - 0.5) / 0.5 * 0.5;
                                $den_type = 2;
                            } else {
                                
                                if ( $kw_den >= 1 && $kw_den < 1.5 ) {
                                    $kw_eff = ($kw_den - 1) / 0.5 * 1;
                                    $den_type = 2;
                                } else {
                                    
                                    if ( $kw_den >= 1.5 && $kw_den < 3.5 ) {
                                        $kw_eff = 1;
                                        $den_type = 1;
                                    } else {
                                        
                                        if ( $kw_den >= 3.5 && $kw_den < 5 ) {
                                            $kw_eff = ($kw_den - 3.5) / 1.5 * 1;
                                            $den_type = 3;
                                        } else {
                                            
                                            if ( $kw_den >= 5 && $kw_den < 8 ) {
                                                $kw_eff = ($kw_den - 5) / 3 * 0.5;
                                                $den_type = 3;
                                            } else {
                                                $kw_eff = 1;
                                                $den_type = 3;
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                            
                            $kw_eff_sum += $kw_eff * 4;
                            $kw_eff_c += 4;
                            $has_prio1++;
                            if ( !$title_has_prio1 && (stripos( $title, $kw_san ) !== false || stripos( $title, $kw ) !== false) ) {
                                $title_has_prio1 = true;
                            }
                            if ( !$desc_has_prio1 && (stripos( $desc, $kw_san ) !== false || stripos( $desc, $kw ) !== false) ) {
                                $desc_has_prio1 = true;
                            }
                            if ( !$url_has_prio1 && stripos( str_replace( array( '/', '-', '_' ), '', $link ), $kw_san ) !== false ) {
                                $url_has_prio1 = true;
                            }
                            
                            if ( $h1_outside ) {
                                $h1_san = WSKO_Class_Helper::densify_string( $post->post_title );
                                
                                if ( stripos( $post->post_title, $kw ) !== false || stripos( $h1_san, $kw_san ) !== false ) {
                                    $prio1_h1_den += substr_count( $h1_san, $kw_san ) / WSKO_Class_Helper::get_word_count( $post->post_title );
                                    $h1_has_prio1++;
                                }
                            
                            }
                            
                            foreach ( $h1s as $h1 ) {
                                $h1_san = WSKO_Class_Helper::densify_string( $h1->nodeValue );
                                
                                if ( stripos( $h1->nodeValue, $kw ) !== false || stripos( $h1_san, $kw_san ) !== false ) {
                                    $prio1_h1_den += substr_count( $h1_san, $kw_san ) / WSKO_Class_Helper::get_word_count( $h1->nodeValue );
                                    $h1_has_prio1++;
                                }
                            
                            }
                            break;
                        case 2:
                            
                            if ( $kw_den >= 0.2 && $kw_den < 0.6 ) {
                                $kw_eff = ($kw_den - 0.2) / 0.4 * 0.5;
                                $den_type = 2;
                            } else {
                                
                                if ( $kw_den >= 0.6 && $kw_den < 1 ) {
                                    $kw_eff = ($kw_den - 0.6) / 0.4 * 1;
                                    $den_type = 2;
                                } else {
                                    
                                    if ( $kw_den >= 1 && $kw_den < 3.5 ) {
                                        $kw_eff = 1;
                                        $den_type = 1;
                                    } else {
                                        
                                        if ( $kw_den >= 3.5 && $kw_den < 5 ) {
                                            $kw_eff = ($kw_den - 3.5) / 1.5 * 1;
                                            $den_type = 3;
                                        } else {
                                            
                                            if ( $kw_den >= 5 && $kw_den < 8 ) {
                                                $kw_eff = ($kw_den - 5) / 3 * 0.5;
                                                $den_type = 3;
                                            } else {
                                                $kw_eff = 1;
                                                $den_type = 3;
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                            
                            $kw_eff_sum += $kw_eff * 2;
                            $kw_eff_c += 2;
                            break;
                        case 3:
                            
                            if ( $kw_den >= 0.1 && $kw_den < 0.3 ) {
                                $kw_eff = ($kw_den - 0.1) / 0.2 * 0.5;
                                $den_type = 2;
                            } else {
                                
                                if ( $kw_den >= 0.3 && $kw_den < 0.5 ) {
                                    $kw_eff = ($kw_den - 0.3) / 0.2 * 1;
                                    $den_type = 2;
                                } else {
                                    
                                    if ( $kw_den >= 0.5 && $kw_den < 3.5 ) {
                                        $kw_eff = 1;
                                        $den_type = 1;
                                    } else {
                                        
                                        if ( $kw_den >= 3.5 && $kw_den < 5 ) {
                                            $kw_eff = ($kw_den - 3.5) / 1.5 * 1;
                                            $den_type = 3;
                                        } else {
                                            
                                            if ( $kw_den >= 5 && $kw_den < 8 ) {
                                                $kw_eff = ($kw_den - 5) / 3 * 0.5;
                                                $den_type = 3;
                                            } else {
                                                $kw_eff = 1;
                                                $den_type = 3;
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                            
                            $kw_eff_sum += $kw_eff * 1;
                            $kw_eff_c += 1;
                            break;
                    }
                    $keyword_den[$kw] = array(
                        'density' => round( $kw_den, 2 ),
                        'type'    => $den_type,
                        'prio'    => $prio,
                    );
                }
                if ( $h1_has_prio1 ) {
                    $prio1_h1_den = round( $prio1_h1_den / $count_h1, 2 );
                }
                if ( $kw_eff_c > 0 ) {
                    $kw_score = $kw_eff_sum / $kw_eff_c;
                }
                if ( !$word_count ) {
                    $kw_score = 0;
                }
            }
            
            //if (!$keyword_den)
            //$keyword_den = array('no-keyword' => array('density' => 0, 'type' => 0, 'prio' => 1));
            $res['issues']['keyword_density'] = $keyword_den;
            $res['title_length'] = $title_length;
            $res['issues']['title_length'] = 1;
            $res['issues']['title_prio1'] = $has_prio1 && $title_has_prio1;
            $title_score = 1;
            
            if ( $title_length ) {
                
                if ( $title_length < WSKO_ONPAGE_TITLE_MIN ) {
                    $res['issues']['title_length'] = 2;
                    $title_score = 0.75;
                } else {
                    
                    if ( $title_length > WSKO_ONPAGE_TITLE_MAX ) {
                        $res['issues']['title_length'] = 3;
                        $title_score = 0.75;
                    } else {
                    }
                
                }
                
                if ( $has_prio1 && !$title_has_prio1 ) {
                    $title_score = 0.25;
                }
            } else {
                $res['issues']['title_length'] = 0;
                $title_score = 0;
            }
            
            $res['desc_length'] = $desc_length;
            $res['issues']['desc_length'] = 1;
            $res['issues']['desc_prio1'] = $has_prio1 && $desc_has_prio1;
            $desc_score = 1;
            
            if ( $desc_length ) {
                
                if ( $desc_length < WSKO_ONPAGE_DESC_MIN ) {
                    $res['issues']['desc_length'] = 2;
                    $desc_score = 0.5;
                } else {
                    
                    if ( $desc_length > WSKO_ONPAGE_DESC_MAX ) {
                        $res['issues']['desc_length'] = 3;
                        $desc_score = 0.5;
                    }
                
                }
                
                if ( $has_prio1 && !$desc_has_prio1 ) {
                    $desc_score = 0.5;
                }
            } else {
                $res['issues']['desc_length'] = 0;
                $desc_score = 0;
            }
            
            $res['issues']['url_length'] = 1;
            $res['issues']['url_prio1'] = $has_prio1 && $url_has_prio1;
            $res['url_length'] = $url_length = ( $link ? mb_strlen( str_ireplace( WSKO_Class_Helper::get_host_base( false ), '', $link ) ) : 0 );
            $url_score = 1;
            
            if ( $url_length > WSKO_ONPAGE_URL_MAX ) {
                $res['issues']['url_length'] = 0;
                $url_score = 0.5;
            }
            
            if ( !$has_prio1 || !$url_has_prio1 ) {
                $url_score = 0;
            }
            $res['issues']['heading_h1_prio1'] = ( $h1_has_prio1 ? 1 : 0 );
            $res['issues']['heading_h1_count'] = 0;
            $res['issues']['heading_h1_prio1_count'] = 0;
            $heading1_score = 0;
            
            if ( $count_h1 ) {
                $res['issues']['heading_h1_count'] = 1;
                $heading1_score = 1;
                
                if ( $has_prio1 && $h1_has_prio1 / $has_prio1 <= 0.5 ) {
                    $res['issues']['heading_h1_prio1_count'] = 0;
                    $heading1_score = 0.66;
                } else {
                    $res['issues']['heading_h1_prio1_count'] = 1;
                }
                
                
                if ( $count_h1 > 1 ) {
                    $res['issues']['heading_h1_count'] = 2;
                    
                    if ( $heading1_score > 0.66 ) {
                        $heading1_score -= 0.66;
                    } else {
                        $heading1_score = 0;
                    }
                
                }
            
            }
            
            $res['issues']['heading_h2_250'] = 1;
            $res['issues']['heading_h2_500'] = 1;
            $res['issues']['heading_h2h3_count'] = 0;
            $heading2_score = 0;
            
            if ( $count_h2 || $count_h3 ) {
                $res['issues']['heading_h2h3_count'] = 1;
                $heading2_score = 1;
                
                if ( !$count_h2 && $word_count > 500 ) {
                    $res['issues']['heading_h2_500'] = 0;
                    $heading2_score = 0;
                } else {
                    
                    if ( !$count_h2 && $word_count > 250 ) {
                        $res['issues']['heading_h2_250'] = 0;
                        $heading2_score = 0.5;
                    }
                
                }
            
            }
            
            $res['issues']['word_count'] = 1;
            $content_score = 1;
            
            if ( $word_count < 50 ) {
                $res['issues']['word_count'] = 2;
                $content_score = 0;
            } else {
                
                if ( $word_count < 100 ) {
                    $res['issues']['word_count'] = 3;
                    $content_score = 0.25;
                } else {
                    
                    if ( $word_count < 200 ) {
                        $res['issues']['word_count'] = 4;
                        $content_score = 0.5;
                    } else {
                        
                        if ( $word_count < 400 ) {
                            $res['issues']['word_count'] = 5;
                            $content_score = 0.75;
                        }
                    
                    }
                
                }
            
            }
            
            if ( !$word_count ) {
                $res['issues']['word_count'] = 0;
            }
            $res['issues']['media'] = 0;
            $res['issues']['media_missing_alt'] = 1;
            $media_score = 0;
            
            if ( $imgs && $imgs->length || $videos && $videos->length ) {
                $media_score = 1;
                if ( $imgs ) {
                    foreach ( $imgs as $img ) {
                        
                        if ( !$img->getAttribute( 'alt' ) ) {
                            $res['issues']['media_missing_alt'] = 0;
                            $media_score = 0.5;
                            break;
                        }
                    
                    }
                }
                $res['issues']['media'] = 1;
            }
            
            if ( $iframes && $iframes->length ) {
                foreach ( $iframes as $iframe ) {
                    $src = $iframe->getAttribute( 'src' );
                    if ( stripos( $src, 'youtube.' ) !== false || stripos( $src, 'youtu.be' ) !== false || stripos( $src, 'vimeo.com' ) !== false || stripos( $src, 'dailymotion.com' ) !== false || stripos( $src, 'dai.ly' ) !== false ) {
                        $res['issues']['media'] = 1;
                    }
                }
            }
            $res['onpage_score'] = round( ($kw_score * 0.25 + $title_score * 0.2 + $desc_score * 0.05 + $url_score * 0.05 + $heading1_score * 0.15 + $heading2_score * 0.1 + $content_score * 0.15 + $media_score * 0.05) * 100, 2 );
            wp_reset_postdata();
            return $res;
        } catch ( Exception $error ) {
            if ( $log_error ) {
                WSKO_Class_Helper::report_error( 'exception', 'Onpage - Analysis Error', $error );
            }
        }
        return false;
    }
    
    public static function generate_onpage_analysis()
    {
        if ( !WSKO_Class_Onpage::seo_plugins_disabled() ) {
            return;
        }
        ignore_user_abort( true );
        set_time_limit( WSKO_LRS_TIMEOUT );
        try {
            $onpage_data = WSKO_Class_Onpage::get_onpage_data();
            
            if ( isset( $onpage_data['global_analysis'] ) ) {
                
                if ( isset( $onpage_data['global_analysis']['new_report'] ) && $onpage_data['global_analysis']['new_report'] ) {
                    
                    if ( $onpage_data['global_analysis']['new_report']['query_expire'] > time() ) {
                        if ( !$onpage_data['global_analysis']['new_report']['local_running'] ) {
                            WSKO_Class_Onpage::finish_onpage_analysis();
                        }
                        return;
                    }
                
                } else {
                    if ( isset( $onpage_data['global_analysis']['current_report']['total_pages'] ) && $onpage_data['global_analysis']['current_report']['total_pages'] && (isset( $onpage_data['global_analysis']['current_report']['started'] ) && $onpage_data['global_analysis']['current_report']['started'] > time() - 60 * 60 * 24 * 7) ) {
                        return;
                    }
                }
                
                $onpage_data['global_analysis']['new_report'] = array(
                    'query_running' => true,
                    'query_expire'  => time() + 60 * 60 * 1,
                );
            } else {
                $onpage_data['global_analysis'] = array(
                    'new_report' => array(
                    'query_running' => true,
                    'local_running' => true,
                    'query_expire'  => time() + 60 * 60 * 1,
                ),
                );
            }
            
            WSKO_Class_Onpage::set_onpage_data( $onpage_data );
            $analysis = array(
                'started'      => time(),
                'onpage_score' => 0,
                'total_pages'  => 0,
                'posts'        => array(),
                'post_types'   => array(),
                'issues'       => array(
                'keyword_density'        => array(
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            )
            ),
                'title_length'           => array(
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            )
            ),
                'title_prio1'            => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'desc_length'            => array(
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            )
            ),
                'desc_prio1'             => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'heading_h1_count'       => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'heading_h1_prio1'       => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'heading_h1_prio1_count' => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'heading_h2_250'         => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'heading_h2_500'         => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'heading_h2h3_count'     => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'word_count'             => array(
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            ),
                array(
                'sum'   => 0,
                'posts' => array(),
            )
            ),
                'url_length'             => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'url_prio1'              => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'media'                  => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
                'media_missing_alt'      => array( array(
                'sum'   => 0,
                'posts' => array(),
            ), array(
                'sum'   => 0,
                'posts' => array(),
            ) ),
            ),
            );
            $title_lengths = array(
                'ok'        => 0,
                'too_long'  => 0,
                'too_short' => 0,
                'not_set'   => 0,
            );
            $desc_lengths = array(
                'ok'        => 0,
                'too_long'  => 0,
                'too_short' => 0,
                'not_set'   => 0,
            );
            $url_lengths = array(
                'ok'       => 0,
                'too_long' => 0,
            );
            $facebook_dist = array(
                'title_length' => array(
                0,
                0,
                0,
                0
            ),
                'desc_length'  => array(
                0,
                0,
                0,
                0
            ),
                'image'        => array( 0, 0 ),
            );
            $twitter_dist = array(
                'title_length' => array(
                0,
                0,
                0,
                0
            ),
                'desc_length'  => array(
                0,
                0,
                0,
                0
            ),
                'image'        => array( 0, 0 ),
            );
            $content_dist = array(
                '0-100'     => 0,
                '100-200'   => 0,
                '200-300'   => 0,
                '300-500'   => 0,
                '500-700'   => 0,
                '700-1000'  => 0,
                '1000-1500' => 0,
                '1500-2000' => 0,
                '2000-3000' => 0,
                '3000+'     => 0,
            );
            $heading_dist = array(
                "h1" => array(
                0,
                0,
                0,
                0,
                0,
                0
            ),
                "h2" => array(
                0,
                0,
                0,
                0,
                0,
                0
            ),
                "h3" => array(
                0,
                0,
                0,
                0,
                0,
                0
            ),
                "h4" => array(
                0,
                0,
                0,
                0,
                0,
                0
            ),
                "h5" => array(
                0,
                0,
                0,
                0,
                0,
                0
            ),
                "h6" => array(
                0,
                0,
                0,
                0,
                0,
                0
            ),
            );
            $kw_den_dist = array(
                'prio1' => array(
                0,
                0,
                0,
                0
            ),
                'prio2' => array(
                0,
                0,
                0,
                0
            ),
                'prio3' => array(
                0,
                0,
                0,
                0
            ),
            );
            $kw_dist = array(
                'prio1' => array( 0, 0 ),
                'prio2' => array( 0, 0 ),
                'prio3' => array( 0, 0 ),
            );
            $max_onapge_score = 0;
            $max_title_length = 0;
            $max_title_dupl = 0;
            $max_desc_length = 0;
            $max_desc_dupl = 0;
            $max_url_length = 0;
            $max_content_length = 0;
            $max_word_count = 0;
            $max_heading_count = 0;
            $ref_data = array();
            $t_titles = array();
            $t_desc = array();
            try {
                $post_types = explode( ',', WSKO_Class_Core::get_setting( 'onpage_include_post_types' ) );
                
                if ( $post_types ) {
                    $offset = 0;
                    $step = 500;
                    //batch queries
                    global  $post ;
                    $t_post = $post;
                    do {
                        $query = new WP_Query( array(
                            'posts_per_page' => $step,
                            'offset'         => $offset * $step,
                            'post_type'      => $post_types,
                        ) );
                        $results = $query->posts;
                        foreach ( $results as $res ) {
                            if ( $analysis['total_pages'] >= 1000 ) {
                                continue;
                            }
                            $post = $res;
                            $op_report = WSKO_Class_Onpage::get_onpage_report( $res->ID, false, false );
                            
                            if ( $op_report ) {
                                $title = $op_report['meta_title'];
                                $desc = $op_report['meta_desc'];
                                
                                if ( $op_report['meta_og_title'] ) {
                                    
                                    if ( $op_report['meta_og_title'] < WSKO_ONPAGE_FB_TITLE_MIN ) {
                                        $facebook_dist['title_length'][2]++;
                                    } else {
                                        
                                        if ( $op_report['meta_og_title'] > WSKO_ONPAGE_FB_TITLE_MAX ) {
                                            $facebook_dist['title_length'][3]++;
                                        } else {
                                            $facebook_dist['title_length'][1]++;
                                        }
                                    
                                    }
                                
                                } else {
                                    $facebook_dist['title_length'][0]++;
                                }
                                
                                
                                if ( $op_report['meta_og_desc'] ) {
                                    
                                    if ( $op_report['meta_og_desc'] < WSKO_ONPAGE_FB_DESC_MIN ) {
                                        $facebook_dist['desc_length'][2]++;
                                    } else {
                                        
                                        if ( $op_report['meta_og_desc'] > WSKO_ONPAGE_FB_DESC_MAX ) {
                                            $facebook_dist['desc_length'][3]++;
                                        } else {
                                            $facebook_dist['desc_length'][1]++;
                                        }
                                    
                                    }
                                
                                } else {
                                    $facebook_dist['desc_length'][0]++;
                                }
                                
                                
                                if ( $op_report['meta_og_img'] ) {
                                    $facebook_dist['image'][1]++;
                                } else {
                                    $facebook_dist['image'][0]++;
                                }
                                
                                
                                if ( $op_report['meta_tw_title'] ) {
                                    
                                    if ( $op_report['meta_tw_title'] < WSKO_ONPAGE_TW_TITLE_MIN ) {
                                        $twitter_dist['title_length'][2]++;
                                    } else {
                                        
                                        if ( $op_report['meta_tw_title'] > WSKO_ONPAGE_TW_TITLE_MAX ) {
                                            $twitter_dist['title_length'][3]++;
                                        } else {
                                            $twitter_dist['title_length'][1]++;
                                        }
                                    
                                    }
                                
                                } else {
                                    $twitter_dist['title_length'][0]++;
                                }
                                
                                
                                if ( $op_report['meta_tw_desc'] ) {
                                    
                                    if ( $op_report['meta_tw_desc'] < WSKO_ONPAGE_TW_DESC_MIN ) {
                                        $twitter_dist['desc_length'][2]++;
                                    } else {
                                        
                                        if ( $op_report['meta_tw_desc'] > WSKO_ONPAGE_TW_DESC_MAX ) {
                                            $twitter_dist['desc_length'][3]++;
                                        } else {
                                            $twitter_dist['desc_length'][1]++;
                                        }
                                    
                                    }
                                
                                } else {
                                    $twitter_dist['desc_length'][0]++;
                                }
                                
                                
                                if ( $op_report['meta_tw_img'] ) {
                                    $twitter_dist['image'][1]++;
                                } else {
                                    $twitter_dist['image'][0]++;
                                }
                                
                                $post_url = get_permalink( $res->ID );
                                $title_length = ( $title ? mb_strlen( $title ) : 0 );
                                $desc_length = ( $desc ? mb_strlen( $desc ) : 0 );
                                if ( $title_length > $max_title_length ) {
                                    $max_title_length = $title_length;
                                }
                                if ( $desc_length > $max_desc_length ) {
                                    $max_desc_length = $desc_length;
                                }
                                
                                if ( $title ) {
                                    
                                    if ( isset( $t_titles[$title] ) ) {
                                        $t_titles[$title]['dup_pages'][] = $post_url;
                                        $t_titles[$title]['dup_posts'][] = $res->ID;
                                        $t_titles[$title]['count']++;
                                    } else {
                                        $t_titles[$title] = array(
                                            'dup_pages' => array( $post_url ),
                                            'dup_posts' => array( $res->ID ),
                                            'count'     => 1,
                                            'length'    => $title_length,
                                        );
                                    }
                                
                                } else {
                                    $title_lengths['not_set']++;
                                }
                                
                                
                                if ( $desc ) {
                                    
                                    if ( isset( $t_desc[$desc] ) ) {
                                        $t_desc[$desc]['dup_pages'][] = $post_url;
                                        $t_desc[$desc]['dup_posts'][] = $res->ID;
                                        $t_desc[$desc]['count']++;
                                    } else {
                                        $t_desc[$desc] = array(
                                            'dup_pages' => array( $post_url ),
                                            'dup_posts' => array( $res->ID ),
                                            'count'     => 1,
                                            'length'    => $desc_length,
                                        );
                                    }
                                
                                } else {
                                    $desc_lengths['not_set']++;
                                }
                                
                                
                                if ( $op_report['url_length'] > WSKO_ONPAGE_URL_MAX ) {
                                    $url_lengths['too_long']++;
                                } else {
                                    $url_lengths['ok']++;
                                }
                                
                                if ( $op_report['url_length'] > $max_url_length ) {
                                    $max_url_length = $op_report['url_length'];
                                }
                                $content_plain = $op_report['content'];
                                $content_length = $op_report['content_length'];
                                $word_count = $op_report['word_count'];
                                if ( $content_length > $max_content_length ) {
                                    $max_content_length = $content_length;
                                }
                                if ( $word_count > $max_word_count ) {
                                    $max_word_count = $word_count;
                                }
                                
                                if ( !$word_count || $word_count < 100 ) {
                                    $content_dist['0-100']++;
                                } else {
                                    
                                    if ( $word_count < 200 ) {
                                        $content_dist['100-200']++;
                                    } else {
                                        
                                        if ( $word_count < 300 ) {
                                            $content_dist['200-300']++;
                                        } else {
                                            
                                            if ( $word_count < 500 ) {
                                                $content_dist['300-500']++;
                                            } else {
                                                
                                                if ( $word_count < 700 ) {
                                                    $content_dist['500-700']++;
                                                } else {
                                                    
                                                    if ( $word_count < 1000 ) {
                                                        $content_dist['700-1000']++;
                                                    } else {
                                                        
                                                        if ( $word_count < 1500 ) {
                                                            $content_dist['1000-1500']++;
                                                        } else {
                                                            
                                                            if ( $word_count < 2000 ) {
                                                                $content_dist['1500-2000']++;
                                                            } else {
                                                                
                                                                if ( $word_count < 3000 ) {
                                                                    $content_dist['2000-3000']++;
                                                                } else {
                                                                    $content_dist['3000+']++;
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                                
                                $tags = $op_report['tags'];
                                $max_h = max( $tags );
                                if ( $max_h > $max_heading_count ) {
                                    $max_heading_count = $max_h;
                                }
                                
                                if ( $tags['h1'] <= 5 ) {
                                    $heading_dist['h1'][$tags['h1']]++;
                                } else {
                                    $heading_dist['h1'][5]++;
                                }
                                
                                
                                if ( $tags['h2'] <= 5 ) {
                                    $heading_dist['h2'][$tags['h2']]++;
                                } else {
                                    $heading_dist['h2'][5]++;
                                }
                                
                                
                                if ( $tags['h3'] <= 5 ) {
                                    $heading_dist['h3'][$tags['h3']]++;
                                } else {
                                    $heading_dist['h3'][5]++;
                                }
                                
                                
                                if ( $tags['h4'] <= 5 ) {
                                    $heading_dist['h4'][$tags['h4']]++;
                                } else {
                                    $heading_dist['h4'][5]++;
                                }
                                
                                
                                if ( $tags['h5'] <= 5 ) {
                                    $heading_dist['h5'][$tags['h5']]++;
                                } else {
                                    $heading_dist['h5'][5]++;
                                }
                                
                                
                                if ( $tags['h6'] <= 5 ) {
                                    $heading_dist['h6'][$tags['h6']]++;
                                } else {
                                    $heading_dist['h6'][5]++;
                                }
                                
                                $onpage_score = $op_report['onpage_score'];
                                if ( $onpage_score > $max_onapge_score ) {
                                    $max_onapge_score = $onpage_score;
                                }
                                $analysis['onpage_score'] += $onpage_score;
                                $prio1_kw_den = array();
                                $prio2_kw_den = array();
                                $prio3_kw_den = array();
                                foreach ( $op_report['issues']['keyword_density'] as $pk => $iss ) {
                                    $kw_den_dist['prio' . $iss['prio']][$iss['type']]++;
                                    switch ( $iss['prio'] ) {
                                        case 1:
                                            $prio1_kw_den[] = '"' . $pk . '" (' . $iss['density'] . '%)';
                                            break;
                                        case 2:
                                            $prio2_kw_den[] = '"' . $pk . '" (' . $iss['density'] . '%)';
                                            break;
                                        case 3:
                                            $prio3_kw_den[] = '"' . $pk . '" (' . $iss['density'] . '%)';
                                            break;
                                    }
                                    $analysis['issues']['keyword_density'][$iss['type']]['sum']++;
                                    $analysis['issues']['keyword_density'][$iss['type']]['posts'][] = $res->ID;
                                }
                                
                                if ( $prio1_kw_den ) {
                                    $kw_dist['prio1'][1]++;
                                } else {
                                    $kw_dist['prio1'][0]++;
                                }
                                
                                
                                if ( $prio2_kw_den ) {
                                    $kw_dist['prio2'][1]++;
                                } else {
                                    $kw_dist['prio2'][0]++;
                                }
                                
                                
                                if ( $prio3_kw_den ) {
                                    $kw_dist['prio3'][1]++;
                                } else {
                                    $kw_dist['prio3'][0]++;
                                }
                                
                                $analysis['pages'][$post_url] = array(
                                    'onpage_score'          => $onpage_score,
                                    'title'                 => $title,
                                    'title_length'          => $title_length,
                                    'title_duplicates'      => 0,
                                    'title_duplicate_posts' => array(),
                                    'desc_length'           => $desc_length,
                                    'desc_duplicate_posts'  => array(),
                                    'desc_duplicates'       => 0,
                                    'count_h1'              => $tags['h1'],
                                    'count_h2'              => $tags['h2'],
                                    'count_h3'              => $tags['h3'],
                                    'count_h4'              => $tags['h4'],
                                    'count_h5'              => $tags['h5'],
                                    'count_h6'              => $tags['h6'],
                                    'word_count'            => $word_count,
                                    'content_length'        => $content_length,
                                    'url'                   => $post_url,
                                    'url_length'            => $op_report['url_length'],
                                    'post_id'               => $res->ID,
                                    'post_type'             => $res->post_type,
                                    'prio1_kw_den'          => implode( ', ', $prio1_kw_den ),
                                    'prio2_kw_den'          => implode( ', ', $prio2_kw_den ),
                                    'prio3_kw_den'          => implode( ', ', $prio3_kw_den ),
                                    'og_title_length'       => ( $op_report['meta_og_title'] ? mb_strlen( $op_report['meta_og_title'] ) : 0 ),
                                    'og_desc_length'        => ( $op_report['meta_og_desc'] ? mb_strlen( $op_report['meta_og_desc'] ) : 0 ),
                                    'og_img_provided'       => ( $op_report['meta_og_img'] ? 1 : 0 ),
                                    'tw_title_length'       => ( $op_report['meta_tw_title'] ? mb_strlen( $op_report['meta_tw_title'] ) : 0 ),
                                    'tw_desc_length'        => ( $op_report['meta_tw_desc'] ? mb_strlen( $op_report['meta_tw_desc'] ) : 0 ),
                                    'tw_img_provided'       => ( $op_report['meta_tw_img'] ? 1 : 0 ),
                                );
                                if ( !in_array( $res->post_type, $analysis['post_types'] ) ) {
                                    $analysis['post_types'][] = $res->post_type;
                                }
                                $analysis['issues']['title_length'][$op_report['issues']['title_length']]['sum']++;
                                $analysis['issues']['title_length'][$op_report['issues']['title_length']]['posts'][] = $res->ID;
                                $analysis['issues']['title_prio1'][$op_report['issues']['title_prio1']]['sum']++;
                                $analysis['issues']['title_prio1'][$op_report['issues']['title_prio1']]['posts'][] = $res->ID;
                                $analysis['issues']['desc_length'][$op_report['issues']['desc_length']]['sum']++;
                                $analysis['issues']['desc_length'][$op_report['issues']['desc_length']]['posts'][] = $res->ID;
                                $analysis['issues']['desc_prio1'][$op_report['issues']['desc_prio1']]['sum']++;
                                $analysis['issues']['desc_prio1'][$op_report['issues']['desc_prio1']]['posts'][] = $res->ID;
                                $analysis['issues']['heading_h1_count'][$op_report['issues']['heading_h1_count']]['sum']++;
                                $analysis['issues']['heading_h1_count'][$op_report['issues']['heading_h1_count']]['posts'][] = $res->ID;
                                $analysis['issues']['heading_h1_prio1'][$op_report['issues']['heading_h1_prio1']]['sum']++;
                                $analysis['issues']['heading_h1_prio1'][$op_report['issues']['heading_h1_prio1']]['posts'][] = $res->ID;
                                $analysis['issues']['heading_h1_prio1_count'][$op_report['issues']['heading_h1_prio1_count']]['sum']++;
                                $analysis['issues']['heading_h1_prio1_count'][$op_report['issues']['heading_h1_prio1_count']]['posts'][] = $res->ID;
                                //$analysis['issues']['heading_h1_prio1_den'][$op_report['issues']['heading_h1_prio1_den']['type']]['sum']++;
                                //$analysis['issues']['heading_h1_prio1_den'][$op_report['issues']['heading_h1_prio1_den']]['posts'][] = $res->ID;
                                $analysis['issues']['heading_h2_250'][$op_report['issues']['heading_h2_250']]['sum']++;
                                $analysis['issues']['heading_h2_250'][$op_report['issues']['heading_h2_250']]['posts'][] = $res->ID;
                                $analysis['issues']['heading_h2_500'][$op_report['issues']['heading_h2_500']]['sum']++;
                                $analysis['issues']['heading_h2_500'][$op_report['issues']['heading_h2_500']]['posts'][] = $res->ID;
                                $analysis['issues']['heading_h2h3_count'][$op_report['issues']['heading_h2h3_count']]['sum']++;
                                $analysis['issues']['heading_h2h3_count'][$op_report['issues']['heading_h2h3_count']]['posts'][] = $res->ID;
                                $analysis['issues']['word_count'][$op_report['issues']['word_count']]['sum']++;
                                $analysis['issues']['word_count'][$op_report['issues']['word_count']]['posts'][] = $res->ID;
                                $analysis['issues']['url_length'][$op_report['issues']['url_length']]['sum']++;
                                $analysis['issues']['url_length'][$op_report['issues']['url_length']]['posts'][] = $res->ID;
                                $analysis['issues']['url_prio1'][$op_report['issues']['url_prio1']]['sum']++;
                                $analysis['issues']['url_prio1'][$op_report['issues']['url_prio1']]['posts'][] = $res->ID;
                                $analysis['issues']['media'][$op_report['issues']['media']]['sum']++;
                                $analysis['issues']['media'][$op_report['issues']['media']]['posts'][] = $res->ID;
                                $analysis['issues']['media_missing_alt'][$op_report['issues']['media_missing_alt']]['sum']++;
                                $analysis['issues']['media_missing_alt'][$op_report['issues']['media_missing_alt']]['posts'][] = $res->ID;
                                $analysis['total_pages']++;
                                $ref_data[$res->ID] = $post_url;
                            }
                        
                        }
                        $offset++;
                    } while ($query->post_count >= $step);
                    $post = $t_post;
                    setup_postdata( $post );
                }
            
            } catch ( Exception $error ) {
                WSKO_Class_Helper::report_error( 'exception', 'Onpage - Analysis Error', $error );
            }
            WSKO_Class_Helper::refresh_wp_cache( 'cron', true );
            $onpage_data = WSKO_Class_Onpage::get_onpage_data();
            $onpage_data['global_analysis']['new_report']['ref_data'] = $ref_data;
            WSKO_Class_Onpage::set_onpage_data( $onpage_data );
            $title_dupl = array(
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            );
            $desc_dupl = array(
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            );
            
            if ( $analysis['total_pages'] > 0 ) {
                $analysis['onpage_score_avg'] = round( $analysis['onpage_score'] / $analysis['total_pages'], 2 );
            } else {
                $analysis['onpage_score_avg'] = 0;
            }
            
            foreach ( $t_titles as $k => $title ) {
                
                if ( $title['length'] < WSKO_ONPAGE_TITLE_MIN ) {
                    $title_lengths['too_short'] += $title['count'];
                } else {
                    
                    if ( $title['length'] > WSKO_ONPAGE_TITLE_MAX ) {
                        $title_lengths['too_long'] += $title['count'];
                    } else {
                        $title_lengths['ok'] += $title['count'];
                    }
                
                }
                
                
                if ( $title['count'] > 1 ) {
                    
                    if ( $title['count'] > 10 ) {
                        $ind = 10;
                    } else {
                        $ind = $title['count'] - 1;
                    }
                    
                    $title_dupl[$ind] += $title['count'];
                } else {
                    $title_dupl[0]++;
                }
                
                $pages = $title['dup_pages'];
                if ( $pages ) {
                    foreach ( $pages as $p ) {
                        $analysis['pages'][$p]['title_duplicates'] = $title['count'] - 1;
                        $posts = $title['dup_posts'];
                        if ( ($key = array_search( $analysis['pages'][$p]['post_id'], $posts )) !== false ) {
                            unset( $posts[$key] );
                        }
                        $analysis['pages'][$p]['title_duplicate_posts'] = $posts;
                    }
                }
                //unset($t_titles[$k]['dup_pages']);
                if ( $title['count'] > $max_title_dupl ) {
                    $max_title_dupl = $title['count'];
                }
            }
            foreach ( $t_desc as $k => $desc ) {
                
                if ( $desc['length'] < WSKO_ONPAGE_DESC_MIN ) {
                    $desc_lengths['too_short'] += $desc['count'];
                } else {
                    
                    if ( $desc['length'] > WSKO_ONPAGE_DESC_MAX ) {
                        $desc_lengths['too_long'] += $desc['count'];
                    } else {
                        $desc_lengths['ok'] += $desc['count'];
                    }
                
                }
                
                
                if ( $desc['count'] > 1 ) {
                    
                    if ( $desc['count'] > 10 ) {
                        $ind = 10;
                    } else {
                        $ind = $desc['count'] - 1;
                    }
                    
                    $desc_dupl[$ind] += $desc['count'];
                } else {
                    $desc_dupl[0]++;
                }
                
                $pages = $desc['dup_pages'];
                if ( $pages ) {
                    foreach ( $pages as $p ) {
                        $analysis['pages'][$p]['desc_duplicates'] = $desc['count'] - 1;
                        $posts = $desc['dup_posts'];
                        if ( ($key = array_search( $analysis['pages'][$p]['post_id'], $posts )) !== false ) {
                            unset( $posts[$key] );
                        }
                        $analysis['pages'][$p]['desc_duplicate_posts'] = $posts;
                    }
                }
                //unset($t_desc[$k]['dup_pages']);
                if ( $desc['count'] > $max_desc_dupl ) {
                    $max_desc_dupl = $desc['count'];
                }
            }
            $analysis['title_length_dist'] = $title_lengths;
            $analysis['title_duplicate_dist'] = $title_dupl;
            $analysis['titles_count'] = count( $t_titles );
            $analysis['desc_length_dist'] = $desc_lengths;
            $analysis['desc_duplicate_dist'] = $desc_dupl;
            $analysis['desc_count'] = count( $t_desc );
            $analysis['url_length_dist'] = $url_lengths;
            $analysis['heading_dist'] = $heading_dist;
            $analysis['facebook_meta_dist'] = $facebook_dist;
            $analysis['twitter_meta_dist'] = $twitter_dist;
            $analysis['content_length_dist'] = $content_dist;
            $analysis['priority_kw_den_dist'] = $kw_den_dist;
            $analysis['priority_kw_dist'] = $kw_dist;
            $analysis['max'] = array(
                'onpage_score'   => $max_onapge_score,
                'title_length'   => $max_title_length,
                'title_dupl'     => $max_title_dupl,
                'desc_length'    => $max_desc_length,
                'desc_dupl'      => $max_desc_dupl,
                'url_length'     => $max_url_length,
                'content_length' => $max_content_length,
                'word_count'     => $max_word_count,
                'heading_count'  => $max_heading_count,
            );
            WSKO_Class_Helper::refresh_wp_cache( 'cron', true );
            $onpage_data = WSKO_Class_Onpage::get_onpage_data();
            $onpage_data['global_analysis']['new_report']['local_data'] = $analysis;
            $onpage_data['global_analysis']['new_report']['local_running'] = false;
            WSKO_Class_Onpage::set_onpage_data( $onpage_data );
            //if (!wsko_fs()->can_use_premium_code())
            //{
            WSKO_Class_Onpage::finish_onpage_analysis();
            //}
        } catch ( Exception $error ) {
            WSKO_Class_Helper::report_error( 'exception', 'Onpage - Analysis Error', $error );
        }
    }
    
    public static function finish_onpage_analysis()
    {
        WSKO_Class_Helper::refresh_wp_cache( 'cron', true );
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        
        if ( isset( $onpage_data['global_analysis']['new_report']['local_data'] ) ) {
            if ( isset( $onpage_data['global_analysis']['current_report'] ) && $onpage_data['global_analysis']['current_report'] ) {
                $onpage_data['global_analysis']['last_onpage_score'] = $onpage_data['global_analysis']['current_report']['onpage_score_avg'];
            }
            $onpage_data['global_analysis']['current_report'] = $onpage_data['global_analysis']['new_report']['local_data'];
            $onpage_data['global_analysis']['new_report'] = false;
            if ( !isset( $onpage_data['global_analysis']['onpage_history'] ) ) {
                $onpage_data['global_analysis']['onpage_history'] = array();
            }
            $onpage_data['global_analysis']['onpage_history'][$onpage_data['global_analysis']['current_report']['started']] = $onpage_data['global_analysis']['current_report']['onpage_score_avg'];
            WSKO_Class_Onpage::set_onpage_data( $onpage_data );
            WSKO_Class_Onpage::reset_op_dirty_posts();
        }
    
    }
    
    public static function get_onpage_page_crawl_data( $url )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( $onpage_data && isset( $onpage_data['global_analysis']['current_report'] ) && $onpage_data['global_analysis']['current_report'] ) {
            if ( isset( $onpage_data['global_analysis']['current_report']['pages'][$url] ) ) {
                return $onpage_data['global_analysis']['current_report']['pages'][$url];
            }
        }
        return false;
    }
    
    public static function get_redirects()
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( isset( $onpage_data['redirects'] ) && is_array( $onpage_data['redirects'] ) ) {
            return $onpage_data['redirects'];
        }
        return array();
    }
    
    public static function get_page_redirects()
    {
        $tech_data = WSKO_Class_Onpage::get_technical_seo_data();
        
        if ( $tech_data ) {
            $res = array();
            foreach ( $tech_data as $post_id => $data ) {
                if ( isset( $data['redirect'] ) && $data['redirect'] ) {
                    $res[] = array(
                        'post' => $post_id,
                        'to'   => $data['redirect']['to'],
                        'type' => $data['redirect']['type'],
                    );
                }
            }
            return $res;
        }
        
        return array();
    }
    
    public static function get_auto_redirects( $type )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( isset( $onpage_data['auto_redirects'][$type] ) && $onpage_data['auto_redirects'][$type] ) {
            return $onpage_data['auto_redirects'][$type];
        }
        return false;
    }
    
    public static function set_redirect_404( $activate, $type, $custom )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        $onpage_data['redirect_404'] = array(
            'activate' => $activate,
            'type'     => $type,
            'custom'   => $custom,
        );
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
    }
    
    public static function add_redirect(
        $page,
        $comp,
        $redirect_to,
        $comp_to,
        $type
    )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        $redirects = WSKO_Class_Onpage::get_redirects();
        $redirects[] = array(
            'page'    => $page,
            'comp'    => $comp,
            'target'  => $redirect_to,
            'comp_to' => $comp_to,
            'type'    => $type,
        );
        $onpage_data['redirects'] = $redirects;
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
    }
    
    public static function remove_redirects( $redirects_rem )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        $redirects = WSKO_Class_Onpage::get_redirects();
        foreach ( $redirects as $k => $r ) {
            if ( in_array( $k, $redirects_rem ) ) {
                unset( $redirects[$k] );
            }
        }
        $onpage_data['redirects'] = $redirects;
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
    }
    
    public static function get_post_meta_from( $from )
    {
        $from_m = false;
        switch ( $from ) {
            case 'post_id':
                $from_m = 'post_metas';
                break;
            case 'post_type':
                $from_m = 'post_type_metas';
                break;
            case 'post_tax':
                $from_m = 'post_tax_metas';
                break;
            case 'post_term':
                $from_m = 'post_term_metas';
                break;
            case 'post_archive':
                $from_m = 'post_archive_metas';
                break;
            case 'other':
                $from_m = 'other_metas';
                break;
        }
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( isset( $onpage_data[( $from_m ? $from_m : 'post_metas' )] ) ) {
            return $onpage_data[( $from_m ? $from_m : 'post_metas' )];
        }
        return false;
    }
    
    public static function get_post_meta( $identifier, $from = false )
    {
        $metas_a = WSKO_Class_Onpage::get_post_meta_from( $from );
        
        if ( isset( $metas_a[$identifier] ) ) {
            $metas = $metas_a[$identifier];
            foreach ( $metas as $k => $m ) {
                if ( !$m ) {
                    unset( $metas[$k] );
                }
            }
            return $metas;
        }
        
        return false;
    }
    
    public static function set_post_meta( $identifier, $data, $from = false )
    {
        $from_m = false;
        switch ( $from ) {
            case 'post_id':
                $from_m = 'post_metas';
                break;
            case 'post_type':
                $from_m = 'post_type_metas';
                break;
            case 'post_tax':
                $from_m = 'post_tax_metas';
                break;
            case 'post_term':
                $from_m = 'post_term_metas';
                break;
            case 'post_archive':
                $from_m = 'post_archive_metas';
                break;
            case 'other':
                $from_m = 'other_metas';
                break;
        }
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        
        if ( isset( $onpage_data[( $from_m ? $from_m : 'post_metas' )] ) && is_array( $onpage_data[( $from_m ? $from_m : 'post_metas' )] ) ) {
            $onpage_data[( $from_m ? $from_m : 'post_metas' )][$identifier] = $data;
        } else {
            $onpage_data[( $from_m ? $from_m : 'post_metas' )] = array(
                $identifier => $data,
            );
        }
        
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
        if ( !$from_m || $from_m === 'post_metas' ) {
            WSKO_Class_Onpage::set_op_post_dirty( $identifier, 'metas' );
        }
    }
    
    public static function unset_post_meta( $identifier, $from = false )
    {
        $from_m = false;
        switch ( $from ) {
            case 'post_id':
                $from_m = 'post_metas';
                break;
            case 'post_type':
                $from_m = 'post_type_metas';
                break;
            case 'post_tax':
                $from_m = 'post_tax_metas';
                break;
            case 'post_archive':
                $from_m = 'post_archive_metas';
                break;
            case 'other':
                $from_m = 'other_metas';
                break;
        }
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        
        if ( isset( $onpage_data[( $from_m ? $from_m : 'post_metas' )] ) && is_array( $onpage_data[( $from_m ? $from_m : 'post_metas' )] ) ) {
            unset( $onpage_data[( $from_m ? $from_m : 'post_metas' )][$identifier] );
            if ( !$from_m || $from_m === 'post_metas' ) {
                WSKO_Class_Onpage::set_op_post_dirty( $identifier, 'metas' );
            }
        }
        
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
    }
    
    public static function get_post_metas( $preview = false )
    {
        $metas = false;
        $is_special = false;
        
        if ( !$preview ) {
            global  $wsko_post_meta ;
            if ( $wsko_post_meta ) {
                return $wsko_post_meta;
            }
            
            if ( is_front_page() && is_home() ) {
                // Default homepage
                $metas_t = WSKO_Class_Onpage::get_post_meta( 'home', 'other' );
                
                if ( $metas_t ) {
                    
                    if ( $metas ) {
                        $metas = $metas + $metas_t;
                    } else {
                        $metas = $metas_t;
                    }
                    
                    $is_special = true;
                }
            
            } else {
                
                if ( is_front_page() ) {
                    // static homepage
                    $metas_t = WSKO_Class_Onpage::get_post_meta( 'home', 'other' );
                    
                    if ( $metas_t ) {
                        
                        if ( $metas ) {
                            $metas = $metas + $metas_t;
                        } else {
                            $metas = $metas_t;
                        }
                        
                        $is_special = true;
                    }
                
                } else {
                    
                    if ( is_home() ) {
                        $metas_t = WSKO_Class_Onpage::get_post_meta( 'blog', 'other' );
                        
                        if ( $metas_t ) {
                            
                            if ( $metas ) {
                                $metas = $metas + $metas_t;
                            } else {
                                $metas = $metas_t;
                            }
                            
                            $is_special = true;
                        }
                    
                    } else {
                        
                        if ( is_post_type_archive() ) {
                            $post_type = get_post_type();
                            $metas_t = WSKO_Class_Onpage::get_post_meta( $post_type, 'post_archive' );
                            if ( $metas_t ) {
                                
                                if ( $metas ) {
                                    $metas = $metas + $metas_t;
                                } else {
                                    $metas = $metas_t;
                                }
                            
                            }
                            $is_special = true;
                        } else {
                            
                            if ( is_tax() || is_category() || is_tag() ) {
                                $queried_object = get_queried_object();
                                
                                if ( $queried_object && $queried_object->taxonomy ) {
                                    $taxonomy = $queried_object->taxonomy;
                                    $term = $queried_object->term_id;
                                    
                                    if ( $term ) {
                                        $metas_t = WSKO_Class_Onpage::get_post_meta( $taxonomy . ':' . $term, 'post_term' );
                                        if ( $metas_t ) {
                                            
                                            if ( $metas ) {
                                                $metas = $metas + $metas_t;
                                            } else {
                                                $metas = $metas_t;
                                            }
                                        
                                        }
                                    }
                                    
                                    $metas_t = WSKO_Class_Onpage::get_post_meta( $taxonomy, 'post_tax' );
                                    if ( $metas_t ) {
                                        
                                        if ( $metas ) {
                                            $metas = $metas + $metas_t;
                                        } else {
                                            $metas = $metas_t;
                                        }
                                    
                                    }
                                }
                                
                                $is_special = true;
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        }
        
        
        if ( !$is_special ) {
            global  $post ;
            
            if ( ($preview || is_singular()) && $post && $post->ID ) {
                $metas_t = WSKO_Class_Onpage::get_post_meta( $post->ID, 'post_id' );
                if ( $metas_t ) {
                    
                    if ( $metas ) {
                        $metas = $metas + $metas_t;
                    } else {
                        $metas = $metas_t;
                    }
                
                }
                $metas_t = WSKO_Class_Onpage::get_post_meta( $post->post_type, 'post_type' );
                if ( $metas_t ) {
                    
                    if ( $metas ) {
                        $metas = $metas + $metas_t;
                    } else {
                        $metas = $metas_t;
                    }
                
                }
            }
        
        }
        
        
        if ( !$preview ) {
            global  $wsko_post_meta ;
            if ( $metas ) {
                $wsko_post_meta = $metas;
            }
        }
        
        return $metas;
    }
    
    public static function calculate_meta( $text, $overrides = array() )
    {
        $override_post = ( isset( $overrides['post'] ) ? $overrides['post'] : false );
        $override_tax = ( isset( $overrides['tax'] ) ? $overrides['tax'] : false );
        $override_term = ( isset( $overrides['term'] ) ? $overrides['term'] : false );
        
        if ( $override_post ) {
            $post = get_post( $override_post );
        } else {
            global  $post ;
        }
        
        
        if ( $post ) {
            preg_match_all( '/%post:(.*?)%/s', $text, $matches );
            foreach ( $matches[1] as $k => $match ) {
                $temp = explode( ':', $match );
                if ( !empty($temp) ) {
                    switch ( $temp[0] ) {
                        case 'post_title':
                            $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( $post->post_title, false ), $text );
                            break;
                        case 'post_content':
                            $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( $post->post_content, false ), $text );
                            break;
                        case 'post_excerpt':
                            $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( $post->post_excerpt, false ), $text );
                            break;
                        case 'post_author':
                            $text = str_replace( $matches[0][$k], get_the_author_meta( 'display_name', $post->post_author ), $text );
                            break;
                    }
                }
            }
            preg_match_all( '/%meta:(.*?)%/s', $text, $matches );
            foreach ( $matches[1] as $k => $match ) {
                $temp = explode( ':', $match );
                if ( !empty($temp) ) {
                    $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( get_post_meta( $post->ID, $temp[0], true ) ), $text );
                }
            }
            preg_match_all( '/%tax:(.*?)%/s', $text, $matches );
            foreach ( $matches[1] as $k => $match ) {
                $temp = explode( ':', $match );
                
                if ( !empty($temp) ) {
                    $terms = wp_get_post_terms( $post->ID, $temp[0], array(
                        'fields' => 'names',
                    ) );
                    
                    if ( $terms ) {
                        $terms = implode( ', ', $terms );
                        $text = str_replace( $matches[0][$k], $terms, $text );
                    } else {
                        $text = str_replace( $matches[0][$k], '', $text );
                    }
                
                }
            
            }
        }
        
        
        if ( $override_tax || $override_term || is_tax() || is_category() || is_tag() ) {
            $queried_object = get_queried_object();
            $taxonomy = ( $override_tax ? $override_tax : (( $queried_object && $queried_object->taxonomy ? $queried_object->taxonomy : false )) );
            $term = ( $override_term ? $override_term : (( $queried_object && $queried_object->term_id ? $queried_object->term_id : false )) );
            
            if ( $taxonomy && $term ) {
                $term = get_term_by( 'id', $term, $taxonomy );
                
                if ( $term ) {
                    preg_match_all( '/%term:(.*?)%/s', $text, $matches );
                    foreach ( $matches[1] as $k => $match ) {
                        $temp = explode( ':', $match );
                        if ( !empty($temp) ) {
                            switch ( $temp[0] ) {
                                case 'term_title':
                                    $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( $term->name, 'the_title' ), $text );
                                    break;
                                case 'term_desc':
                                    $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( $term->description, 'the_content' ), $text );
                                    break;
                            }
                        }
                    }
                    preg_match_all( '/%term_meta:(.*?)%/s', $text, $matches );
                    foreach ( $matches[1] as $k => $match ) {
                        $temp = explode( ':', $match );
                        if ( !empty($temp) ) {
                            $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( get_term_meta( $term->term_id, $temp[0], true ) ), $text );
                        }
                    }
                }
            
            }
        
        }
        
        preg_match_all( '/%site:(.*?)%/s', $text, $matches );
        foreach ( $matches[1] as $k => $match ) {
            $temp = explode( ':', $match );
            if ( !empty($temp) ) {
                switch ( $temp[0] ) {
                    case 'blog_name':
                        $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( get_bloginfo(), false ), $text );
                        break;
                    case 'blog_tagline':
                        $text = str_replace( $matches[0][$k], WSKO_Class_Helper::sanitize_meta( get_bloginfo( 'description' ), false ), $text );
                        break;
                }
            }
        }
        return $text;
    }
    
    public static function get_sitemap_params()
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        $sitemap_params = array(
            'types' => array(),
            'stati' => array(),
            'posts' => array(),
        );
        if ( isset( $onpage_data['sitemap_params'] ) && $onpage_data['sitemap_params'] ) {
            $sitemap_params = $onpage_data['sitemap_params'];
        }
        return $sitemap_params;
    }
    
    public static function set_sitemap_params( $data )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        $onpage_data['sitemap_params'] = $data;
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
    }
    
    public static function generate_sitemap()
    {
        $sitemap_path = ABSPATH . '/sitemap.xml';
        $sitemap_real_path = ABSPATH . '/sitemap_bst.xml';
        $sitemap_exists = file_exists( $sitemap_path );
        $sitemap_real_exists = file_exists( $sitemap_real_path );
        if ( ($sitemap_exists && is_writable( $sitemap_path ) || !$sitemap_exists && is_writable( ABSPATH )) && ($sitemap_real_exists && is_writable( $sitemap_real_path ) || !$sitemap_real_exists && is_writable( ABSPATH )) ) {
            try {
                $gen_params = WSKO_Class_Onpage::get_sitemap_params();
                
                if ( $gen_params && $gen_params['types'] ) {
                    // && $gen_params['stati'])
                    $xml = new SimpleXMLElement( '<sitemapindex/>' );
                    $xml->addAttribute( 'xmlns:xmlns:xsi', 'http://www.sitemaps.org/schemas/sitemap/0.9' );
                    $xml->addAttribute( 'xsi:xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9' );
                    $xml->addAttribute( 'xmlns:xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9' );
                    $sitemap = $xml->addChild( 'sitemap' );
                    $sitemap->addChild( 'loc', home_url( 'sitemap_bst.xml' ) );
                    $sitemap->addChild( 'lastmod', date( 'c' ) );
                    $xml->asXML( $sitemap_path );
                    $xml = new SimpleXMLElement( '<urlset/>' );
                    $xml->addAttribute( 'xmlns:xmlns:xsi', 'http://www.sitemaps.org/schemas/sitemap/0.9' );
                    $xml->addAttribute( 'xsi:xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9' );
                    $xml->addAttribute( 'xmlns:xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9' );
                    $post_types = get_post_types( array(
                        'public' => true,
                    ), 'names' );
                    //objects
                    foreach ( $post_types as $k => $pt ) {
                        if ( !isset( $gen_params['types'][$pt] ) ) {
                            unset( $post_types[$k] );
                        }
                    }
                    
                    if ( $post_types ) {
                        $offset = 0;
                        $step = 500;
                        //batch queries
                        do {
                            $args = array(
                                'posts_per_page' => $step,
                                'offset'         => $offset * $step,
                                'post_type'      => $post_types,
                                'post_status'    => 'publish',
                                'fields'         => 'ids',
                            );
                            if ( isset( $gen_params['stati'] ) && $gen_params['stati'] ) {
                                $args['post_status'] = $gen_params['stati'];
                            }
                            if ( isset( $gen_params['excluded_posts'] ) && is_array( $gen_params['excluded_posts'] ) && $gen_params['excluded_posts'] ) {
                                $args['post__not_in'] = $gen_params['excluded_posts'];
                            }
                            $query = new WP_Query( $args );
                            $results = $query->posts;
                            foreach ( $results as $res ) {
                                $post_type = get_post_type( $res );
                                $freq = false;
                                $prio = false;
                                
                                if ( isset( $gen_params['types'][$post_type] ) ) {
                                    $freq = $gen_params['types'][$post_type]['freq'];
                                    $prio = $gen_params['types'][$post_type]['prio'];
                                }
                                
                                
                                if ( isset( $gen_params['posts'][$res] ) ) {
                                    if ( $gen_params['posts'][$res]['freq'] ) {
                                        $freq = $gen_params['posts'][$res]['freq'];
                                    }
                                    if ( $gen_params['posts'][$res]['prio'] ) {
                                        $prio = $gen_params['posts'][$res]['prio'];
                                    }
                                }
                                
                                switch ( $freq ) {
                                    case 'always':
                                        break;
                                    case 'hourly':
                                        break;
                                    case 'daily':
                                        break;
                                    case 'weekly':
                                        break;
                                    case 'monthly':
                                        break;
                                    case 'yearly':
                                        break;
                                    case 'never':
                                        break;
                                    default:
                                        $freq = 'never';
                                        break;
                                }
                                
                                if ( $prio > 1 ) {
                                    $prio = 1;
                                } else {
                                    if ( $prio < 0 ) {
                                        $prio = 0;
                                    }
                                }
                                
                                $url = $xml->addChild( 'url' );
                                $url->addChild( 'loc', get_permalink( $res ) );
                                $url->addChild( 'lastmod', date( 'c', get_post_modified_time( 'U', false, $res ) ) );
                                if ( $freq ) {
                                    $url->addChild( 'changefreq', $freq );
                                }
                                if ( $prio || $prio === 0 ) {
                                    $url->addChild( 'priority', $prio );
                                }
                            }
                            $offset++;
                        } while ($query->post_count >= $step);
                        $xml->asXML( $sitemap_real_path );
                        return true;
                    }
                
                }
            
            } catch ( Exception $ex ) {
                WSKO_Class_Helper::report_error( 'exception', 'Onpage - Sitemap Generation Error', $ex );
            }
        }
        return false;
    }
    
    public static function upload_sitemap()
    {
        try {
            
            if ( WSKO_Class_Core::get_setting( 'automatic_sitemap' ) && WSKO_Class_Core::get_setting( 'automatic_sitemap_ping' ) && file_exists( ABSPATH . '/sitemap.xml' ) ) {
                $response = WSKO_Class_Helper::post_to_url( 'http://www.google.com/webmasters/tools/ping', array(
                    'sitemap' => home_url( 'sitemap.xml' ),
                ), 'GET' );
                if ( !$response ) {
                    return false;
                }
                $response = WSKO_Class_Helper::post_to_url( 'http://www.bing.com/webmaster/ping.aspx', array(
                    'sitemap' => home_url( 'sitemap.xml' ),
                ), 'GET' );
                if ( !$response ) {
                    return false;
                }
                $onpage_data = WSKO_Class_Onpage::get_onpage_data();
                $onpage_data['last_sitemap_upload'] = time();
                WSKO_Class_Onpage::set_onpage_data( $onpage_data );
                return true;
            }
        
        } catch ( Exception $ex ) {
            WSKO_Class_Helper::report_error( 'exception', 'Onpage - Sitemap Upload Error', $ex );
        }
        return false;
    }
    
    public static function add_priority_keyword( $post_id, $keyword, $prio )
    {
        $prio = intval( $prio );
        $keyword = trim( strtolower( $keyword ) );
        
        if ( $post_id && $keyword && in_array( $prio, array( 1 ) ) ) {
            $onpage_data = WSKO_Class_Onpage::get_onpage_data();
            
            if ( !isset( $onpage_data['priority_keywords'] ) ) {
                $onpage_data['priority_keywords'] = array(
                    $post_id => array(
                    $keyword => $prio,
                ),
                );
                WSKO_Class_Onpage::set_op_post_dirty( $post_id, 'p_kw' );
            } else {
                
                if ( !isset( $onpage_data['priority_keywords'][$post_id] ) || !$onpage_data['priority_keywords'][$post_id] ) {
                    $onpage_data['priority_keywords'][$post_id] = array(
                        $keyword => $prio,
                    );
                    WSKO_Class_Onpage::set_op_post_dirty( $post_id, 'p_kw' );
                } else {
                    
                    if ( isset( $onpage_data['priority_keywords'][$post_id][$keyword] ) || count( array_keys( $onpage_data['priority_keywords'][$post_id], 1 ) ) < 2 ) {
                        $onpage_data['priority_keywords'][$post_id][$keyword] = $prio;
                        WSKO_Class_Onpage::set_op_post_dirty( $post_id, 'p_kw' );
                    }
                
                }
            
            }
            
            WSKO_Class_Onpage::set_onpage_data( $onpage_data );
        }
    
    }
    
    public static function get_priority_keywords( $post_id )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        
        if ( isset( $onpage_data['priority_keywords'] ) && $onpage_data['priority_keywords'] && isset( $onpage_data['priority_keywords'][$post_id] ) ) {
            $pks = $onpage_data['priority_keywords'][$post_id];
            foreach ( $pks as $k => $pk ) {
                if ( $pk != 1 ) {
                    unset( $pks[$k] );
                }
            }
            return $pks;
        }
        
        return false;
    }
    
    public static function remove_priority_keyword( $post_id, $keyword )
    {
        $onpage_data = WSKO_Class_Onpage::get_onpage_data();
        if ( isset( $onpage_data['priority_keywords'] ) && $onpage_data['priority_keywords'] && isset( $onpage_data['priority_keywords'][$post_id] ) ) {
            unset( $onpage_data['priority_keywords'][$post_id][$keyword] );
        }
        WSKO_Class_Onpage::set_onpage_data( $onpage_data );
        WSKO_Class_Onpage::set_op_post_dirty( $post_id, 'p_kw' );
    }
    
    public static function set_op_post_dirty( $post_id, $source )
    {
        $posts = WSKO_Class_Core::get_option( 'onpage_changed_posts' );
        if ( !is_array( $posts ) ) {
            $posts = array();
        }
        
        if ( !isset( $posts[$post_id] ) ) {
            $posts[$post_id] = array( $source );
        } else {
            $posts[$post_id][] = $source;
        }
        
        WSKO_Class_Core::save_option( 'onpage_changed_posts', $posts );
    }
    
    public static function get_op_post_dirty( $post_id )
    {
        $posts = WSKO_Class_Core::get_option( 'onpage_changed_posts' );
        if ( $posts && isset( $posts[$post_id] ) ) {
            return $posts[$post_id];
        }
        return false;
    }
    
    public static function reset_op_dirty_posts()
    {
        WSKO_Class_Core::save_option( 'onpage_changed_posts', array() );
    }
    
    //Singleton
    static  $instance ;
    public static function get_instance()
    {
        if ( !isset( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}