<?php
if (!defined('ABSPATH')) exit;

class WSKO_Class_Social
{
	public static $social_client_fb;
	
	public static function get_new_facebook_client()
	{
		WSKO_Class_Core::include_lib('facebook');
		try
		{
			$fb = new Facebook\Facebook(array(
			  'app_id' => WSKO_API_FACEBOOK_ID,
			  'app_secret' => WSKO_API_FACEBOOK_SECRET,
			  'default_graph_version' => 'v2.3',
			));
			return $fb;
		}
		catch (Exception $error)
		{
		}
		return false;
	}
	
	public static function get_facebook_client()
	{
		if (self::$social_client_fb)
			return self::$social_client_fb;
		$client = WSKO_Class_Social::get_new_facebook_client();
		return self::$social_client_fb = $client;
	}
	
	public static function get_twitter_client()
	{
		WSKO_Class_Core::include_lib('twitter');
		try
		{
			$twitter = new TwitterAPIExchange(array(
				'oauth_access_token' => WSKO_API_TWITTER_TOKEN,
				'oauth_access_token_secret' => WSKO_API_TWITTER_TOKEN_SECRET,
				'consumer_key' => WSKO_API_TWITTER_CONSUMER,
				'consumer_secret' => WSKO_API_TWITTER_CONSUMER_SECRET
			));
			return $twitter;
		}
		catch (Exception $error)
		{
		}
		return false;
	}
	
	public static function get_social_data($platform = false)
	{
		$wsko_data = WSKO_Class_Core::get_data();
		
		if (!isset($wsko_data['social_data']) || !is_array($wsko_data['social_data']))
			$social_data = array();
		else
			$social_data = $wsko_data['social_data'];
		
		return $platform ? (isset($social_data[$platform]) ? $social_data[$platform] : false) : $social_data;
	}
	
	public static function set_social_data($platform, $data)
	{
		$wsko_data = WSKO_Class_Core::get_data();
		if (!isset($wsko_data['social_data']) || !is_array($wsko_data['social_data']))
		{
			$wsko_data['social_data'] = array($platform => $data);
		}
		else
		{
			foreach($data as $k => $v)
			{
				$wsko_data['social_data'][$platform][$k] = $v;
			}
		}
		WSKO_Class_Core::save_data($wsko_data);
	}
	
	public static function get_social_logged_in($platform)
	{
		return WSKO_Class_Social::get_social_token($platform) ? true : false;
	}
	
	public static function get_social_token($platform, $user = false)
	{
		$wsko_data = WSKO_Class_Core::get_data();
		
		if (!isset($wsko_data['social_data']) || !is_array($wsko_data['social_data']))
			$social_data = array();
		else
			$social_data = $wsko_data['social_data'];
		
		return isset($social_data[$platform][$user ? 'user_token' : 'access_token']) && $social_data[$platform][$user ? 'user_token' : 'access_token'] ? $social_data[$platform][$user ? 'user_token' : 'access_token'] : false;
	}
	
	public static function set_social_token($platform, $token, $user = false)
	{
		$social_data = WSKO_Class_Social::get_social_data($platform);
		
		if ($social_data)
			$social_data[$user ? 'user_token' : 'access_token'] = $token;
		else
			$social_data = array($user ? 'user_token' : 'access_token' => $token);
		
		WSKO_Class_Social::set_social_data($platform, $social_data);
	}
	
	public static function get_facebook_profile()
	{
		$wsko_data = WSKO_Class_Core::get_data();
		
		if (isset($wsko_data['social_data']['facebook']) && is_array($wsko_data['social_data']['facebook']) && isset($wsko_data['social_data']['facebook']['profile']) && $wsko_data['social_data']['facebook']['profile'])
		{
			return $wsko_data['social_data']['facebook']['profile'];
		}
		return false;
	}
	
	public static function set_facebook_profile($profile)
	{
		$wsko_data = WSKO_Class_Core::get_data();
		
		if (isset($wsko_data['social_data']) && is_array($wsko_data['social_data']))
		{
			if (isset($wsko_data['social_data']['facebook']) && is_array($wsko_data['social_data']['facebook']))
				$wsko_data['social_data']['facebook']['profile'] = $profile;
			else
				$wsko_data['social_data']['facebook'] = array('profile' => $profile);
		}
		else
		{
			$wsko_data['social_data'] = array('facebook' => array('profile' => $profile));
		}
		WSKO_Class_Core::save_data($wsko_data);
	}
	
	public static function get_twitter_profile()
	{
		$wsko_data = WSKO_Class_Core::get_data();
		
		if (isset($wsko_data['social_data']['twitter']) && is_array($wsko_data['social_data']['twitter']) && isset($wsko_data['social_data']['twitter']['profile']) && $wsko_data['social_data']['twitter']['profile'])
		{
			return $wsko_data['social_data']['twitter']['profile'];
		}
		return false;
	}
	
	public static function set_twitter_profile($profile)
	{
		$wsko_data = WSKO_Class_Core::get_data();
		
		if (isset($wsko_data['social_data']) && is_array($wsko_data['social_data']))
		{
			if (isset($wsko_data['social_data']['twitter']) && is_array($wsko_data['social_data']['twitter']))
				$wsko_data['social_data']['twitter']['profile'] = $profile;
			else
				$wsko_data['social_data']['twitter'] = array('profile' => $profile);
		}
		else
		{
			$wsko_data['social_data'] = array('twitter' => array('profile' => $profile));
		}
		WSKO_Class_Core::save_data($wsko_data);
	}
	
	public static function get_facebook_long_token($short_token)
	{
		if ($short_token)
		{
			$client = WSKO_Class_Social::get_facebook_client();
			if ($client)
			{
				try
				{
					$request = $client->request('GET', 'oauth/access_token', array(
						'client_id'     => WSKO_API_FACEBOOK_ID,
						'client_secret' => WSKO_API_FACEBOOK_SECRET,
						'grant_type'    => 'fb_exchange_token',
						'fb_exchange_token' => $short_token,
					));
					$request->setAccessToken($short_token);
					$response = $client->getClient()->sendRequest($request);
					$node = $response->getGraphNode();
					return $node['access_token'];
				}
				catch (Exception $ex)
				{
					WSKO_Class_Helper::report_error('exception', 'Social - Facebook Long Token', $ex);
				}
			}
		}
		
		return false;
	}
	
	public static function check_facebook_access($user = false)
	{
		$res = true;
		try
		{
			$client = WSKO_Class_Social::get_facebook_client();
			$token = WSKO_Class_Social::get_social_token('facebook', $user);
			$selected_profile = WSKO_Class_Social::get_facebook_profile();
			if ($client && $token)
			{
				if ($user)
				{
					$request = $client->request('GET', '/me/accounts');
					$request->setAccessToken($token);
					$response = $client->getClient()->sendRequest($request);
					$edges = $response->getGraphEdge();
					$res = false;
				}
				else
				{
					$request = $client->request('GET', '/'.$selected_profile, array('fields' => 'name'));
					$request->setAccessToken($token);
					$response = $client->getClient()->sendRequest($request);
					$edges = $response->getGraphNode();
					$res = false;
				}
					
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Check Access', $error);
		}
		return $res;
	}
	
	public static function check_twitter_access()
	{
		return WSKO_Class_Social::get_twitter_data_query() ? false : true;
	}
	
	public static function get_facebook_profiles()
	{
		$res = false;
		try
		{
			$client = WSKO_Class_Social::get_facebook_client();
			$token = WSKO_Class_Social::get_social_token('facebook', true);
			if ($client && $token)
			{
				$request = $client->request('GET', '/me/accounts');
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$edges = $response->getGraphEdge();
				$rows = array();
				foreach ($edges as $page)
				{
					$data = $page->asArray();
					$rows[$data['id']] = array('id' => $data['id'], 'name' => $data['name'], 'access_token' => $data['access_token']);
				}
				return $rows;
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Profile Query', $error);
		}
		return $res;
	}
	
	public static function get_facebook_data()
	{
		$curr = WSKO_Class_Helper::get_midnight();
		$social = WSKO_Class_Cache::get_cache_row($curr, array('social' => array('type' => '=0')));
		return $social ? $social[0] : false;
	}
	
	public static function get_facebook_data_query()
	{
		try
		{
			$client = WSKO_Class_Social::get_facebook_client();
			$token = WSKO_Class_Social::get_social_token('facebook');
			$selected_profile = WSKO_Class_Social::get_facebook_profile();
			if ($client && $token)
			{
				$res = array('likes' => 0, 'engagement' => 0);
				$request = $client->request('GET', '/'.$selected_profile, array('fields' => 'fan_count'));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphNode();
				if ($page)
				{
					$data = $page->asArray();
					$res['likes'] = $data['fan_count'];
				}
				$request = $client->request('GET', '/'.$selected_profile.'/insights/page_post_engagements', array('date_preset' => 'yesterday'));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphEdge();
				if ($page)
				{
					$data = $page->asArray();
					if ($data && $data[0])
						$res['engagement'] = $data[0]['values'][0]['value'];
				}
				$request = $client->request('GET', '/'.$selected_profile.'/insights/page_impressions_unique', array('date_preset' => 'yesterday'));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphEdge();
				if ($page)
				{
					$data = $page->asArray();
					if ($data && $data[0])
						$res['reach'] = $data[0]['values'][0]['value'];
				}
				$request = $client->request('GET', '/'.$selected_profile.'/insights/page_views_total', array('date_preset' => 'yesterday'));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphEdge();
				if ($page)
				{
					$data = $page->asArray();
					if ($data && $data[0])
						$res['views'] = $data[0]['values'][0]['value'];
				}
				return $res;
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Page Info Query', $error);
		}
		return false;
	}
	
	public static function get_current_facebook_posts()
	{
		$social = WSKO_Class_Cache::get_cache_row(0, array('social' => array('type' => '=2')));
		return $social;
	}
	
	public static function get_current_facebook_posts_query()
	{
		try
		{
			$client = WSKO_Class_Social::get_facebook_client();
			$token = WSKO_Class_Social::get_social_token('facebook');
			$selected_profile = WSKO_Class_Social::get_facebook_profile();
			if ($client && $token)
			{
				$res = array();
				$request = $client->request('GET', '/'.$selected_profile.'/feed', array('since' => time()-(60*60*24*30), 'until' => time(), 'fields' => 'id,message,picture,created_time', 'count' => 100));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphEdge();
				if ($page)
				{
					$data = $page->asArray();
					if ($data)
					{
						foreach($data as $p)
						{
							$insights = WSKO_Class_Social::get_facebook_post_data_query($p['id']);
							$res[] = array('id' => $p['id'], 'created' => $p['created_time'] ? $p['created_time']->getTimestamp() : 0, 'content' => isset($p['message']) ? $p['message'] : '', 'img' => isset($p['picture']) ? $p['picture'] : false, 'likes' => $insights ? $insights['likes'] : 0, 'engagement' => $insights ? $insights['engagement'] : 0, 'reach' => $insights ? $insights['reach'] : 0);
						}
					}
				}
				return $res;
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Facebook Current Post Query', $error);
		}
		
		return false;
	}
	
	public static function get_facebook_post_data_query($id)
	{
		try
		{
			$client = WSKO_Class_Social::get_facebook_client();
			$token = WSKO_Class_Social::get_social_token('facebook');
			$selected_profile = WSKO_Class_Social::get_facebook_profile();
			if ($client && $token)
			{
				$res = array('engagement' => 0, 'reach' => 0, 'likes' => 0);
				$request = $client->request('GET', '/'.$id.'/comments', array('summary' => '1'));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphEdge();
				if ($page)
				{
					$data = $page->getMetaData();
					if ($data && isset($data['summary']['total_count']) && $data['summary']['total_count'])
					{
						$res['engagement'] += $data['summary']['total_count'];
					}
				}
				
				$request = $client->request('GET', '/'.$id, array('fields' => 'shares'));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphNode();
				if ($page)
				{
					$data = $page->asArray();
					if ($data)
					{
						if (isset($data['shares']['count']) && $data['shares']['count'])
						{
							$res['engagement'] += $data['shares']['count'];
						}
					}
				}
				
				$request = $client->request('GET', '/'.$id.'/insights', array('metric' => 'post_consumptions,post_impressions_unique,post_reactions_by_type_total'));
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				$page = $response->getGraphEdge();
				if ($page)
				{
					$data = $page->asArray();
					if ($data)
					{
						foreach($data as $attr)
						{
							//if ($attr['name'] == 'post_consumptions')
								//$res['engagement'] += $attr['values'][0]['value'] ? $attr['values'][0]['value'] : 0;
							if ($attr['name'] == 'post_impressions_unique')
								$res['reach'] = $attr['values'][0]['value'] ? $attr['values'][0]['value'] : 0;
							
							if ($attr['name'] == 'post_reactions_by_type_total')
							{
								$res['likes'] += $attr['values'][0]['value']['like'] ? $attr['values'][0]['value']['like'] : 0;
								$res['likes'] += $attr['values'][0]['value']['love'] ? $attr['values'][0]['value']['love'] : 0;
								$res['likes'] += $attr['values'][0]['value']['wow'] ? $attr['values'][0]['value']['wow'] : 0;
								$res['likes'] += $attr['values'][0]['value']['haha'] ? $attr['values'][0]['value']['haha'] : 0;
								$res['likes'] += $attr['values'][0]['value']['sorry'] ? $attr['values'][0]['value']['sorry'] : 0;
								$res['likes'] += $attr['values'][0]['value']['anger'] ? $attr['values'][0]['value']['anger'] : 0;
								$res['engagement'] += $res['likes'];
							}
						}
					}
				}
				return $res;
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Facebook Post Info Query', $error);
		}
		
		return false;
	}
	
	public static function revoke_facebook_access()
	{
		try
		{
			$client = WSKO_Class_Social::get_facebook_client();
			$token = WSKO_Class_Social::get_social_token('facebook', true);
			if ($client && $token)
			{
				$request = $client->request('DELETE', '/me/permissions');
				$request->setAccessToken($token);
				$response = $client->getClient()->sendRequest($request);
				WSKO_Class_Social::set_social_data('facebook', array('access_token' => false, 'user_token' => false, 'profile' => false));
				return true;
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Revoke Facebook Access', $error);
		}
		return false;
	}
	
	public static function get_facebook_login_url()
	{
		$client = WSKO_Class_Social::get_facebook_client();
		$helper = $client->getRedirectLoginHelper();

		$permissions = array('email', 'manage_pages', 'pages_show_list', 'read_insights'); // Optional permissions
		$loginUrl = $helper->getLoginUrl("https://www.bavoko.tools/redirect?redirect=".htmlspecialchars(WSKO_Class_Core::is_configured()?WSKO_Controller_Settings::get_link():WSKO_Controller_Setup::get_link())."&type=facebook", $permissions);
		$x = $loginUrl;

		$parsed = parse_url($x);
		$query = $parsed['query'];

		parse_str($query, $params);

		unset($params['response_type']);
		$loginUrl = http_build_query($params);
		return 'https://www.facebook.com/v2.3/dialog/oauth?response_type=token&'.htmlspecialchars($loginUrl);
	}
	
	public static function get_twitter_data()
	{
		$curr = WSKO_Class_Helper::get_midnight();
		$social = WSKO_Class_Cache::get_cache_row($curr, array('social' => array('type' => '=1')));
		return $social ? $social[0] : false;
	}
	
	public static function get_twitter_data_query()
	{
		try
		{
			$twitter = WSKO_Class_Social::get_twitter_client();
			$selected_profile = WSKO_Class_Social::get_twitter_profile();
			if ($twitter && $selected_profile)
			{
				$url = 'https://api.twitter.com/1.1/users/show.json';
				$getfield = '?screen_name='.$selected_profile;
				$requestMethod = 'GET';
				$response = $twitter->setGetfield($getfield)
					->buildOauth($url, $requestMethod)
					->performRequest();
				if ($response)
				{
					$response = (array)json_decode($response);
					if ($response)
					{
						return array('followers' => $response['followers_count']);
					}
				}
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Twitter Page Info Query', $error);
		}
		return false;
	}
	
	public static function get_current_twitter_posts()
	{
		$social = WSKO_Class_Cache::get_cache_row(0, array('social' => array('type' => '=3')));
		return $social;
	}
	
	public static function get_current_twitter_posts_query()
	{
		try
		{
			$twitter = WSKO_Class_Social::get_twitter_client();
			$selected_profile = WSKO_Class_Social::get_twitter_profile();
			if ($twitter && $selected_profile)
			{
				$res = array();
				$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
				$getfield = '?screen_name='.$selected_profile.'&count=200';
				$requestMethod = 'GET';
				$response = $twitter->setGetfield($getfield)
					->buildOauth($url, $requestMethod)
					->performRequest();
				if ($response)
				{
					$response = (array)json_decode($response);
					if ($response && (!isset($response['errors']) || !$response['errors']))
					{
						foreach ($response as $p)
						{
							$p = (array)$p;
							$urls = array();
							if (isset($p['entities']->urls))
								$urls = array_map(function($v){ return $v->url; }, $p['entities']->urls);
							$res[] = array('id' => $p['id'], 'created' => strtotime($p['created_at']), 'content' => isset($p['text']) ? $p['text'] : '', 'urls' => $urls, 'likes' => isset($p['favorite_count']) && $p['favorite_count'] ? $p['favorite_count'] : 0, 'engagement' => isset($p['retweet_count']) && $p['retweet_count'] ? $p['retweet_count'] : 0, 'reach' => 0);
						}
					}
				}
				return $res;
			}
		}
		catch (Exception $error)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Twitter Current Posts Query', $error);
		}
		return false;
	}
	
	public static function get_twitter_comment_count()
	{
	}
	
	public static function update_social_cache()
	{
		WSKO_Class_Cache::check_database();
		try
		{
			$fb_logged_in = !WSKO_Class_Social::check_facebook_access();
			$tw_logged_in = !WSKO_Class_Social::check_twitter_access();
			$curr = WSKO_Class_Helper::get_midnight();
			$social = WSKO_Class_Cache::get_cache_row($curr, array('social' => array()));
			if (true)//!$social || empty($social))
			{
				$res = array();
				$res2 = array();
				if ($fb_logged_in)
				{
					$facebook_data = WSKO_Class_Social::get_facebook_data_query();
					if ($facebook_data)
						$res[] = array('set'=>'social', 'where'=>array('type' => '0'), 'rows'=>array(array('data' => '', 'created' => 0, 'follower' => intval($facebook_data['likes']), 'engagement' => intval($facebook_data['engagement']), 'views' => intval($facebook_data['views']), 'reach' => intval($facebook_data['reach']))));
					$fb_rows = array();
					$facebook_posts = WSKO_Class_Social::get_current_facebook_posts_query();
					foreach ($facebook_posts as $p)
					{
						$fb_rows[] = array('data' => json_encode(array('id' => $p['id'], 'content' => $p['content'], 'img' => $p['img'])), 'created' => $p['created'], 'follower' => $p['likes'], 'engagement' => $p['engagement'], 'views' => 0, 'reach' => $p['reach']);
					}
					$res2[] = array('set'=>'social', 'where'=>array('type' => '2'), 'rows'=>$fb_rows);
				}
				if ($tw_logged_in)
				{
					$twitter_data = WSKO_Class_Social::get_twitter_data_query();
					if ($twitter_data)
						$res[] = array('set'=>'social', 'where'=>array('type' => '1'), 'rows'=>array('data' => '', 'created' => 0, array('follower' => intval($twitter_data['followers']), 'engagement' => 0, 'views' => 0, 'reach' => 0)));
					$tw_rows = array();
					$twitter_posts = WSKO_Class_Social::get_current_twitter_posts_query();
					foreach ($twitter_posts as $p)
					{
						$tw_rows[] = array('data' => json_encode(array('id' => $p['id'], 'content' => $p['content'], 'urls' => $p['urls'])), 'created' => $p['created'], 'follower' => $p['likes'], 'engagement' => $p['engagement'], 'views' => 0, 'reach' => $p['reach']);
					}
					$res2[] = array('set'=>'social', 'where'=>array('type' => '3'), 'rows'=>$tw_rows);
				}
				WSKO_Class_Cache::set_cache_row($curr, $res);
				WSKO_Class_Cache::set_cache_row(0, $res2);
				WSKO_Class_Core::save_option('social_first_cache_update', true);
			}
		}
		catch (Exception $ex)
		{
			WSKO_Class_Helper::report_error('exception', 'Social - Cache Update', $ex);
		}	
	}
	
	public static function get_facebook_history($start_time, $end_time)
	{
		$facebook_rows = WSKO_Class_Cache::get_cache_rows($start_time, $end_time, array('social' => array('type' => array('eval' => '=0'))), array(), false);
		return $facebook_rows;
	}
	
	public static function get_twitter_history($start_time, $end_time)
	{
		$twitter_rows = WSKO_Class_Cache::get_cache_rows($start_time, $end_time, array('social' => array('type' => array('eval' => '=1'))), array(), false);
		return $twitter_rows;
	}
	
	public static function get_fb_page_link()
	{
		return "https://www.facebook.com/profile.php?id=".WSKO_Class_Social::get_facebook_profile();
	}
	public static function get_fb_post_link($id)
	{
		if ($id)
		{
			$parts = explode('_', $id);
			if (count($parts) == 2)
				return "https://www.facebook.com/".$parts[0]."/posts/".$parts[1];
		}
		return false;
	}
	
	public static function get_tw_page_link()
	{
		return "https://twitter.com/".WSKO_Class_Social::get_twitter_profile();
	}
	public static function get_tw_post_link($id)
	{
		if ($id)
		{
			return "https://twitter.com/statuses/".$id;
		}
		return false;
	}
}
?>