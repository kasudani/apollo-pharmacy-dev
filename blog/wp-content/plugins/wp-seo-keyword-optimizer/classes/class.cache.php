<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class WSKO_Class_Cache
{
    private  $cache_rows_table = 'wsko_cache_days' ;
    private  $fieldsets = array(
        'search'    => array(
        'table'  => 'wsko_cache_data_se',
        'prefix' => 'se',
    ),
        'analytics' => array(
        'table'  => 'wsko_cache_data_an',
        'prefix' => 'an',
    ),
        'social'    => array(
        'table'  => 'wsko_cache_data_social',
        'prefix' => 'so',
    ),
    ) ;
    static  $temp_stats ;
    static  $session_cache = array() ;
    public static function check_database()
    {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        global  $wpdb ;
        $int = new static();
        $charset_collate = $wpdb->get_charset_collate();
        
        if ( $wpdb->get_var( 'SELECT COUNT(1) FROM information_schema.tables WHERE table_schema="' . $wpdb->dbname . '" AND table_name="' . $wpdb->prefix . $int->cache_rows_table . '"' ) == "0" ) {
            $sql = "CREATE TABLE " . $wpdb->prefix . $int->cache_rows_table . " (\r\n\t\t\t  id int NOT NULL AUTO_INCREMENT,\r\n\t\t\t  time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,\r\n\t\t\t  PRIMARY KEY (id)\r\n\t\t\t) {$charset_collate};";
            dbDelta( $sql );
        }
        
        
        if ( $wpdb->get_var( 'SELECT COUNT(1) FROM information_schema.tables WHERE table_schema="' . $wpdb->dbname . '" AND table_name="' . $wpdb->prefix . $int->fieldsets['search']['table'] . '"' ) == "0" ) {
            $sql = "CREATE TABLE " . $wpdb->prefix . $int->fieldsets['search']['table'] . " (\r\n\t\t\t  id int NOT NULL AUTO_INCREMENT,\r\n\t\t\t  cache_id int NOT NULL,\r\n\t\t\t  keyval text NOT NULL,\r\n\t\t\t  clicks int NOT NULL,\r\n\t\t\t  position double NOT NULL,\r\n\t\t\t  impressions int NOT NULL,\r\n\t\t\t  type tinyint NOT NULL,\r\n\t\t\t  PRIMARY KEY (id)\r\n\t\t\t) {$charset_collate};";
            dbDelta( $sql );
        }
        
        
        if ( $wpdb->get_var( 'SELECT COUNT(1) FROM information_schema.tables WHERE table_schema="' . $wpdb->dbname . '" AND table_name="' . $wpdb->prefix . $int->fieldsets['analytics']['table'] . '"' ) == "0" ) {
            $sql = "CREATE TABLE " . $wpdb->prefix . $int->fieldsets['analytics']['table'] . " (\r\n\t\t\t  id int NOT NULL AUTO_INCREMENT,\r\n\t\t\t  cache_id int NOT NULL,\r\n\t\t\t  domain text NOT NULL,\r\n\t\t\t  ip text NOT NULL,\r\n\t\t\t  url text NOT NULL,\r\n\t\t\t  target text NOT NULL,\r\n\t\t\t  last_check timestamp NOT NULL,\r\n\t\t\t  status tinyint NOT NULL,\r\n\t\t\t  follow tinyint NOT NULL,\r\n\t\t\t  anchor text NOT NULL,\r\n\t\t\t  first_seen timestamp NOT NULL,\r\n\t\t\t  last_seen timestamp NOT NULL,\r\n\t\t\t  PRIMARY KEY (id)\r\n\t\t\t) {$charset_collate};";
            dbDelta( $sql );
        }
        
        
        if ( $wpdb->get_var( 'SELECT COUNT(1) FROM information_schema.tables WHERE table_schema="' . $wpdb->dbname . '" AND table_name="' . $wpdb->prefix . $int->fieldsets['social']['table'] . '"' ) == "0" ) {
            $sql = "CREATE TABLE " . $wpdb->prefix . $int->fieldsets['social']['table'] . " (\r\n\t\t\t  id int NOT NULL AUTO_INCREMENT,\r\n\t\t\t  cache_id int NOT NULL,\r\n\t\t\t  data text NOT NULL,\r\n\t\t\t  created int(16) NOT NULL,\r\n\t\t\t  follower bigint NOT NULL,\r\n\t\t\t  engagement bigint NOT NULL,\r\n\t\t\t  views bigint NOT NULL,\r\n\t\t\t  reach bigint NOT NULL,\r\n\t\t\t  type tinyint NOT NULL,\r\n\t\t\t  PRIMARY KEY (id)\r\n\t\t\t) {$charset_collate};";
            dbDelta( $sql );
        }
        
        
        if ( $wpdb->get_var( 'SELECT COUNT(1) FROM information_schema.tables WHERE table_schema="' . $wpdb->dbname . '" AND table_name="' . $wpdb->prefix . 'wsko_cache"' ) == "1" && $wpdb->get_var( 'SELECT COUNT(1) FROM information_schema.tables WHERE table_schema="' . $wpdb->dbname . '" AND table_name="' . $wpdb->prefix . 'wsko_cache_rows"' ) == "1" ) {
            $wpdb->query( "INSERT INTO " . $wpdb->prefix . $int->cache_rows_table . ' (id,time) SELECT id,time FROM ' . $wpdb->prefix . 'wsko_cache' );
            $wpdb->query( "INSERT INTO " . $wpdb->prefix . $int->fieldsets['search']['table'] . ' (cache_id,keyval,clicks,position,impressions,type) SELECT cache_id,keyval,clicks,position,impressions,type FROM ' . $wpdb->prefix . 'wsko_cache_rows' );
            $wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . 'wsko_cache_rows' );
            $wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . 'wsko_cache' );
        }
    
    }
    
    public static function get_cache_stats()
    {
        if ( self::$temp_stats ) {
            return self::$temp_stats;
        }
        global  $wpdb ;
        $int = new static();
        $t_stats = array(
            'general'     => array(
            'days' => $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . $int->cache_rows_table ),
            'size' => $wpdb->get_var( 'SELECT SUM((data_length + index_length)) AS size
								  FROM information_schema.TABLES
								  WHERE table_schema="' . $wpdb->dbname . '" 
								  AND table_name="' . $wpdb->prefix . $int->cache_rows_table . '"' ),
        ),
            'data_tables' => array(),
        );
        $t_stats['general']['rows'] = $t_stats['general']['days'];
        foreach ( $int->fieldsets as $set => $data ) {
            $t_stats['data_tables'][$set] = array(
                'days' => $wpdb->get_var( 'SELECT COUNT(*) FROM (SELECT rt.id, rt.time, COUNT(dt.cache_id) as dataCount FROM ' . $wpdb->prefix . $int->cache_rows_table . ' rt LEFT JOIN ' . $wpdb->prefix . $int->fieldsets[$set]['table'] . ' dt ON rt.id=dt.cache_id GROUP BY rt.id, rt.time) AS tt WHERE tt.dataCount > 0' ),
                'rows' => $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . $int->fieldsets[$set]['table'] ),
                'size' => $wpdb->get_var( 'SELECT SUM((data_length + index_length)) AS size 
								  FROM information_schema.TABLES 
								  WHERE table_schema="' . $wpdb->dbname . '" 
								  AND table_name="' . $wpdb->prefix . $int->fieldsets[$set]['table'] . '"' ),
            );
            $t_stats['general']['rows'] += $t_stats['data_tables'][$set]['rows'];
            $t_stats['general']['size'] += $t_stats['data_tables'][$set]['size'];
        }
        self::$temp_stats = $t_stats;
        return self::$temp_stats;
    }
    
    public static function get_cache_rows(
        $start,
        $end,
        $field_sets,
        $groupby = false,
        $orderby = false
    )
    {
        global  $wpdb ;
        $int = new static();
        if ( !empty($wpdb->charset) && $wpdb->charset == 'utf8mb4' ) {
            $collate = 'COLLATE utf8mb4_bin';
        }
        $sql = "SELECT " . (( $groupby ? $groupby['vals'] : '*' )) . " FROM (SELECT * FROM " . $wpdb->prefix . $int->cache_rows_table;
        
        if ( $start && $end ) {
            $sql .= " WHERE time BETWEEN '" . date( 'Y-m-d H:i:s', $start ) . "' AND '" . date( 'Y-m-d H:i:s', $end ) . "'";
        } else {
            
            if ( $start === 0 || $end === 0 ) {
                $sql .= " WHERE time BETWEEN '" . (( $start ? date( 'Y-m-d H:i:s', $start ) : '0000-00-00 00:00:00' )) . "' AND '" . (( $end ? date( 'Y-m-d H:i:s', $end ) : '0000-00-00 00:00:00' )) . "'";
            } else {
                $sql .= " WHERE time = '0000-00-00 00:00:00'";
            }
        
        }
        
        $sql .= ") AS table_c";
        foreach ( $field_sets as $set => $filter ) {
            $tn = $int->fieldsets[$set]['prefix'];
            $sql .= " INNER JOIN " . $wpdb->prefix . $int->fieldsets[$set]['table'] . " AS " . $tn . " ON table_c.id=" . $tn . ".cache_id";
            foreach ( $filter as $f => $v ) {
                if ( isset( $v['join'] ) && $v['join'] ) {
                    $sql .= " AND " . $tn . "." . $f . $v['eval'];
                }
            }
        }
        $f = true;
        foreach ( $field_sets as $set => $filter ) {
            $tn = $int->fieldsets[$set]['prefix'];
            foreach ( $filter as $f => $v ) {
                
                if ( !isset( $v['join'] ) || !$v['join'] ) {
                    $sql .= (( $f ? ' WHERE ' : ' AND ' )) . $tn . "." . $f . $v['eval'];
                    $f = false;
                }
            
            }
            $sql .= (( $f ? ' WHERE ' : ' AND ' )) . $tn . ".cache_id IS NOT NULL";
            $f = false;
        }
        if ( $groupby ) {
            $sql .= " GROUP BY " . $groupby['group'];
        }
        if ( $orderby ) {
            $sql .= " ORDER BY " . $orderby;
        }
        return $wpdb->get_results( $sql );
    }
    
    public static function get_cache_rows_stats( $start, $end, $field_sets = array() )
    {
        global  $wpdb ;
        $int = new static();
        if ( !empty($wpdb->charset) && $wpdb->charset == 'utf8mb4' ) {
            $collate = 'COLLATE utf8mb4_bin';
        }
        $sql = "SELECT *";
        $i = 0;
        foreach ( $field_sets as $fs_data ) {
            $set = $fs_data['table_key'];
            $tn = $int->fieldsets[$set]['prefix'];
            $sql .= ",(SELECT COUNT(*) FROM " . $wpdb->prefix . $int->fieldsets[$set]['table'] . " " . $tn . $i . " WHERE " . $tn . $i . ".cache_id=table_c.id";
            foreach ( $fs_data['filter'] as $f => $v ) {
                $sql .= " AND " . $tn . $i . "." . $f . $v['eval'];
            }
            $sql .= ") " . $fs_data['result_key'];
            $i++;
        }
        $sql .= " FROM (SELECT * FROM " . $wpdb->prefix . $int->cache_rows_table;
        
        if ( $start && $end ) {
            $sql .= " WHERE time BETWEEN '" . date( 'Y-m-d H:i:s', $start ) . "' AND '" . date( 'Y-m-d H:i:s', $end ) . "'";
        } else {
            
            if ( $start === 0 || $end === 0 ) {
                $sql .= " WHERE time BETWEEN '" . (( $start ? date( 'Y-m-d H:i:s', $start ) : '0000-00-00 00:00:00' )) . "' AND '" . (( $end ? date( 'Y-m-d H:i:s', $end ) : '0000-00-00 00:00:00' )) . "'";
            } else {
                $sql .= " WHERE time = '0000-00-00 00:00:00'";
            }
        
        }
        
        $sql .= ") AS table_c";
        /*foreach ($field_sets as $set => $filter)
        		{
        			$tn = $int->fieldsets[$set]['prefix'];
        			$sql .= " INNER JOIN ".$wpdb->prefix.$int->fieldsets[$set]['table']." AS ".$tn." ON table_c.id=".$tn.".cache_id";
        			
        			foreach ($filter as $f => $v)
        			{
        				$sql .= " AND ".$tn.".".$f.$v['eval'];
        			}
        		}*/
        return $wpdb->get_results( $sql );
    }
    
    public static function has_cache_row( $day, $field_sets = array() )
    {
        $results = WSKO_Class_Cache::get_cache_row( $day, $field_sets );
        return $results && !empty($results);
    }
    
    public static function get_cache_row( $day, $field_sets = array() )
    {
        global  $wpdb ;
        $int = new static();
        if ( !empty($wpdb->charset) && $wpdb->charset == 'utf8mb4' ) {
            $collate = 'COLLATE utf8mb4_bin';
        }
        $sql = "SELECT * FROM (SELECT * FROM " . $wpdb->prefix . $int->cache_rows_table;
        
        if ( $day ) {
            $sql .= " WHERE time BETWEEN '" . date( 'Y-m-d 00:00:00', $day ) . "' AND '" . date( 'Y-m-d 23:59:59', $day ) . "'";
        } else {
            $sql .= " WHERE time = '0000-00-00 00:00:00'";
        }
        
        $sql .= ") AS table_c";
        foreach ( $field_sets as $set => $filter ) {
            $tn = $int->fieldsets[$set]['prefix'];
            $sql .= " LEFT JOIN " . $wpdb->prefix . $int->fieldsets[$set]['table'] . " AS " . $tn . " ON table_c.id=" . $tn . ".cache_id";
            foreach ( $filter as $f => $v ) {
                //if ($v['join'])
                $sql .= " AND " . $tn . "." . $f . $v;
            }
        }
        $f = true;
        foreach ( $field_sets as $set => $filter ) {
            $sql .= (( $f ? ' WHERE ' : ' AND ' )) . $tn . ".cache_id IS NOT NULL";
            $f = false;
        }
        return $wpdb->get_results( $sql );
    }
    
    public static function set_cache_row( $day, $fieldset_data = array() )
    {
        global  $wpdb ;
        $int = new static();
        $sql = "SELECT * FROM " . $wpdb->prefix . $int->cache_rows_table . " AS table_c";
        
        if ( $day ) {
            $sql .= " WHERE table_c.time BETWEEN '" . date( 'Y-m-d 00:00:00', $day ) . "' AND '" . date( 'Y-m-d 23:59:59', $day ) . "'";
        } else {
            $sql .= " WHERE table_c.time = '0000-00-00 00:00:00'";
        }
        
        $rows = $wpdb->get_results( $sql );
        $cache_row_id = false;
        if ( $rows && count( $rows ) > 0 ) {
            
            if ( count( $rows ) > 1 ) {
                WSKO_Class_Cache::delete_cache_row( $day, array(), true );
            } else {
                $cache_row_id = $rows[0]->id;
            }
        
        }
        
        if ( !$cache_row_id ) {
            $wpdb->insert( $wpdb->prefix . $int->cache_rows_table, array(
                'time' => ( $day ? date( 'Y-m-d', $day ) : '0000-00-00' ),
            ) );
            $cache_row_id = $wpdb->insert_id;
        }
        
        foreach ( $fieldset_data as $data ) {
            $wpdb->delete( $wpdb->prefix . $int->fieldsets[$data['set']]['table'], array_merge( array(
                'cache_id' => $cache_row_id,
            ), ( isset( $data['where'] ) ? $data['where'] : array() ) ) );
            foreach ( $data['rows'] as $row ) {
                if ( $row ) {
                    $wpdb->insert( $wpdb->prefix . $int->fieldsets[$data['set']]['table'], array_merge( $row, array_merge( ( isset( $data['where'] ) ? $data['where'] : array() ), array(
                        'cache_id' => $cache_row_id,
                    ) ) ) );
                }
            }
        }
    }
    
    public static function delete_cache_row( $day, $field_sets = array(), $complete = false )
    {
        global  $wpdb ;
        $int = new static();
        $sql = "SELECT * FROM " . $wpdb->prefix . $int->cache_rows_table . " AS table_c";
        
        if ( $day ) {
            $sql .= " WHERE table_c.time BETWEEN '" . date( 'Y-m-d 00:00:00', $day ) . "' AND '" . date( 'Y-m-d 23:59:59', $day ) . "'";
        } else {
            $sql .= " WHERE table_c.time = '0000-00-00 00:00:00'";
        }
        
        $rows = $wpdb->get_results( $sql );
        if ( $rows ) {
            foreach ( $rows as $row ) {
                foreach ( $int->fieldsets as $set => $fieldset ) {
                    if ( empty($field_sets) || in_array( $set, $field_sets ) || $complete ) {
                        $wpdb->delete( $wpdb->prefix . $fieldset['table'], array(
                            'cache_id' => $row->id,
                        ) );
                    }
                }
                if ( $complete ) {
                    $wpdb->delete( $wpdb->prefix . $int->cache_rows_table, array(
                        'id' => $row->id,
                    ) );
                }
            }
        }
    }
    
    public static function delete_cache_rows_before( $day )
    {
        global  $wpdb ;
        $int = new static();
        $sql = "SELECT * FROM " . $wpdb->prefix . $int->cache_rows_table . " AS table_c";
        $sql .= " WHERE table_c.time < '" . date( 'Y-m-d 00:00:00', $day ) . "' AND table_c.time != '0000-00-00 00:00:00'";
        $rows = $wpdb->get_results( $sql );
        if ( $rows ) {
            foreach ( $rows as $row ) {
                foreach ( $int->fieldsets as $fieldset ) {
                    $wpdb->delete( $wpdb->prefix . $fieldset['table'], array(
                        'cache_id' => $row->id,
                    ) );
                }
                $wpdb->delete( $wpdb->prefix . $int->cache_rows_table, array(
                    'id' => $row->id,
                ) );
            }
        }
    }
    
    public static function delete_cache_rows( $field_sets )
    {
        global  $wpdb ;
        $int = new static();
        $sql = "SELECT * FROM " . $wpdb->prefix . $int->cache_rows_table . " AS table_c";
        $rows = $wpdb->get_results( $sql );
        if ( $rows ) {
            foreach ( $rows as $row ) {
                foreach ( $int->fieldsets as $k => $fieldset ) {
                    if ( in_array( $k, $field_sets ) ) {
                        $wpdb->delete( $wpdb->prefix . $fieldset['table'], array(
                            'cache_id' => $row->id,
                        ) );
                    }
                }
            }
        }
    }
    
    public static function delete_cache()
    {
        
        if ( current_user_can( 'manage_options' ) ) {
            global  $wpdb ;
            $int = new static();
            if ( $int->cache_rows_table && substr( $int->cache_rows_table, 0, 5 ) == 'wsko_' ) {
                $wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . $int->cache_rows_table );
            }
            foreach ( $int->fieldsets as $f ) {
                $t = $f['table'];
                if ( $t && substr( $t, 0, 5 ) == 'wsko_' ) {
                    //security
                    $wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . $t );
                }
            }
        }
    
    }
    
    public static function prepare_session_cache()
    {
        if ( WSKO_Class_Core::is_configured() && !WSKO_Class_Core::get_setting( 'use_leightweight_cache' ) ) {
            if ( session_status() === PHP_SESSION_NONE && !headers_sent() ) {
                return session_start();
            }
        }
        return true;
    }
    
    public static function set_session_cache( $key, $data, $expire = 7200 )
    {
        if ( !is_scalar( $key ) ) {
            return;
        }
        $key_data = array(
            'expire' => time() + $expire,
            'data'   => $data,
        );
        
        if ( WSKO_Class_Core::get_setting( 'use_leightweight_cache' ) ) {
            $key_store = WSKO_Class_Cache::get_session_cache_path( $key );
            
            if ( $key_store ) {
                $res = file_put_contents( $key_store, json_encode( $key_data ) );
                if ( $res !== false ) {
                    static::$session_cache[$key] = $data;
                }
            }
        
        } else {
            $key = "wsko_" . $key;
            $_SESSION[$key] = $key_data;
            static::$session_cache[$key] = $data;
        }
    
    }
    
    public static function get_session_cache( $key )
    {
        if ( !is_scalar( $key ) ) {
            return false;
        }
        if ( isset( static::$session_cache[$key] ) ) {
            return static::$session_cache[$key];
        }
        
        if ( WSKO_Class_Core::get_setting( 'use_leightweight_cache' ) ) {
            $key_store = WSKO_Class_Cache::get_session_cache_path( $key );
            
            if ( $key_store && file_exists( $key_store ) ) {
                $cache = file_get_contents( $key_store );
                
                if ( $cache ) {
                    $data = json_decode( $cache );
                    
                    if ( $data && is_array( $data ) && isset( $data['expire'] ) && $data['expire'] > time() && isset( $data['data'] ) && $data['data'] ) {
                        static::$session_cache[$key] = $data['data'];
                        return $data['data'];
                    }
                
                }
            
            }
        
        } else {
            $key = "wsko_" . $key;
            if ( isset( $_SESSION[$key] ) && is_array( $_SESSION[$key] ) && isset( $_SESSION[$key]['expire'] ) && $_SESSION[$key]['expire'] > time() && isset( $_SESSION[$key]['data'] ) && $_SESSION[$key]['data'] ) {
                return $_SESSION[$key]['data'];
            }
        }
        
        return false;
    }
    
    public static function clear_session_cache( $key = false )
    {
        
        if ( $key ) {
            $key_file = WSKO_Class_Cache::get_session_cache_path( $key );
            if ( file_exists( $key_file ) && is_file( $key_file ) ) {
                unlink( $key_file );
            }
            $key = "wsko_" . $key;
            if ( isset( $_SESSION[$key] ) ) {
                unset( $_SESSION[$key] );
            }
        } else {
            $key_store = WSKO_Class_Cache::get_session_cache_path();
            
            if ( $key_store ) {
                $key_files = glob( $key_store . '*' );
                foreach ( $key_files as $f ) {
                    if ( is_file( $f ) ) {
                        unlink( $f );
                    }
                }
            }
            
            foreach ( $_SESSION as $k => $v ) {
                if ( WSKO_Class_Helper::starts_with( $k, 'wsko_' ) ) {
                    unset( $_SESSION[$k] );
                }
            }
        }
    
    }
    
    public static function get_session_cache_path( $key = false )
    {
        $key_store = WP_CONTENT_DIR . '/bst/cache/';
        
        if ( is_writable( $key_store ) && is_readable( $key_store ) ) {
            if ( !file_exists( $key_store ) || !is_dir( $key_store ) ) {
                mkdir( $key_store );
            }
            if ( !file_exists( $key_store . '.htaccess' ) ) {
                file_put_contents( $key_store . '.htaccess', 'deny from all' );
            }
            $key_store .= 'cache/';
            if ( !file_exists( $key_store ) || !is_dir( $key_store ) ) {
                mkdir( $key_store );
            }
            return $key_store . (( $key ? $key . '.cache' : '' ));
        }
        
        return false;
    }

}