<?php
if (!defined('ABSPATH')) exit;

class WSKO_Class_Template
{
	public static function render_post_dirty_icon($post, $params = array(), $return = true)
	{
		if ($return)
			ob_start();
		$dirty = WSKO_Class_Onpage::get_op_post_dirty($post);
		if ($dirty)
		{
			?><span class="wsko-op-post-dirty-icon" data-tooltip="The post has been modified since the last Onpage Crawl. OPS and table values may differ."><i class="fa fa-exclamation-circle"></i></span><?php
		}
		if ($return)
			return ob_get_clean();
	}
	public static function render_ajax_beacon($action, $params = array(), $return = false)
	{
		if ($return)
			ob_start();
		$post = isset($params['post']) ? $params['post'] : false;
		foreach ($post as $k => $v)
		{
			$post[$k] = $k.':"'.$v.'"';
		}
		$uniqid = WSKO_Class_Helper::get_unique_id('wsko_ajax_beacon_');
		?><div id="<?=$uniqid?>">
			<?php WSKO_Class_Template::render_preloader(array('size' => 'small')/* + ($align ? array('align' => $align) : array())*/); ?> 
		</div>
		<script type="text/javascript">
		jQuery(document).ready(function($){
			jQuery.wsko_post_element({action: '<?=$action?>',<?=$post?implode(',',$post).',':''?> nonce: '<?=wp_create_nonce($action)?>'},
				function(res){
					if (res.success)
					{
						$('#<?=$uniqid?>').html(res.view);
					}
					return true;
				}, function() {
				}, false, false);
		});
		</script>
		<?php
		if ($return)
			return ob_get_clean();
	}
	public static function render_keyword_field($keyword, $return = false)
	{
		if ($return)
			ob_start();
		$monitored_keywords = WSKO_Class_Search::get_monitored_keywords();
		?><?=$keyword?> <a href="#" class="wsko-set-monitoring-keyword ml3" data-toggle="tooltip" title="<?=in_array($keyword, $monitored_keywords) ? __("Remove from 'Monitored Keywords'" ,'wsko') : __("Add to 'Monitored Keywords'" ,'wsko')?>" data-keyword="<?=$keyword?>" data-set="<?=in_array($keyword, $monitored_keywords) ? 'false' : 'true'?>"><i class="fa fa-star<?=in_array($keyword, $monitored_keywords) ? '' : '-o'?>"></i></a><?php
		if ($return)
			return ob_get_clean();
	}
	public static function render_content_optimizer_multi_link($posts, $title, $params = array(), $return = false)
	{
		$msg = isset($params['msg']) ? $params['msg'] : false;
		if ($return)
			ob_start();
		?><a href="#" class="wsko-content-optimizer-multi-link" data-posts="<?=htmlspecialchars(json_encode($posts))?>" data-title="<?=$msg?>"><?=$title?></a><?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_priority_keyword_item($post_id, $keyword, $data = array(), $return = false)
	{
		if ($return)
			ob_start();
		?><li class="wsko-co-priority-keyword" data-post="<?=$post_id?>" data-keyword="<?=$keyword?>" data-nonce="<?=wp_create_nonce('wsko_co_delete_priority_keyword')?>"><span data-tooltip="<?=$keyword?>"><p class="wsko-prio-keyword-text wsko-inline-block"><?=$keyword?></p></span> <a href="#" class="wsko-co-delete-priority-keyword pull-right dark wsko-text-off"><i class="fa fa-times fa-fw"></i></a> <span class="wsko-prio-keyword-data pull-right wsko-text-off wsko-mr10"><span data-tooltip="Clicks" class="wsko-mr5"><i class="fa fa-mouse-pointer fa-fw wsko-small"></i><?=isset($data['clicks']) ? $data['clicks'] : '-'?></span> <span data-tooltip="Avg. Position" class="wsko-mr5"><i class="fa fa-bars fa-fw wsko-small"></i><?=isset($data['pos']) ? $data['pos'] : '-'?></span>  <span data-tooltip="Keyword Density" style="<?=isset($data['den_type']) ? ($data['den_type'] == 3 ? 'color:orange' : ($data['den_type'] == 1 ? 'color:#5cb85c' : 'color:#d9534f')) : ''?>">KD <?=isset($data['den']) ? $data['den'] : '-'?>%</span></span></li><?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_content_optimizer_link($post_id, $params = array(), $return = false)
	{
		if ($return)
			ob_start();
		$open_tab = isset($params['open_tab']) ? $params['open_tab'] : false;
		?><a href="#" class="wsko-content-optimizer-link ml3" data-post="<?=$post_id?>" <?=$open_tab?'data-opentab="'.$open_tab.'"':''?>><small class="wsko-content-optimizer-link-text" data-toggle="tooltip" data-title="Content Optimizer"><?=__('CO', 'wsko')?></small></a><?php
		if ($return)
			return ob_get_clean();
	}
	public static function render_meta_snippet($type, $title, $url, $desc, $params = array(), $return = false)
	{
		$img = isset($params['img']) ? $params['img'] : false;
		$no_title = isset($params['no_title']) && $params['no_title'] ? true : false;
		$no_desc = isset($params['no_desc']) && $params['no_desc'] ? true : false;
		
		if ($return)
			ob_start();
		switch ($type)
		{
			case 'tab':
				?>
				<div style="border-bottom:40px solid lightgray;border-left:20px solid transparent;border-right:20px solid transparent;height:0;">
					<div style="height:40px;width:100%;padding:10px;">
						<i class="fa fa-times" style="float:right;"></i>
						<img src="https://www.google.com/s2/favicons?domain=<?=urlencode(WSKO_Class_Helper::get_host_base())?>">
						<?=$title?>
					</div>
				</div>
				<?php
				break;
			case 'google':
				?>
				<div class="wsko-google-snippet wsko-p20 wsko-mb15 wsko-z-depth-1">
					<div id="wsko_google_meta_desktop">
						<div>
							<a href="#" class="wsko-google-snippet-top wsko-hightlight-input" data-container=".wsko-set-metas-wrapper" data-input="input[name='title']"><?=$title?></a><?=$no_title?' <span data-tooltip="Default title set" data-placement="bottom"><i class="fa fa-info fa-fw" style="display:inline-block;color:gray"></i></span>':''?><br/>
							<span class="wsko-google-snippet-url wsko-hightlight-input" data-container=".wsko-set-metas-wrapper" data-input="input[name='url']"><?=esc_attr($url)?></span><br/>
							<span class="wsko-google-snippet-desc wsko-hightlight-input" data-container=".wsko-set-metas-wrapper" data-input="textarea[name='desc']"><?=esc_attr($desc)?></span><?=$no_desc?' <span data-tooltip="No description set. Snippet may differ from real view!" data-placement="bottom"><i class="fa fa-info fa-fw" style="display:inline-block;color:gray"></i></span>':''?>
						</div>	
					</div>		
				</div>
				<?php
				break;
			case 'facebook':
				?>
				<div class="wsko-facebook-snippet wsko-z-depth-1">
					<div class="wsko-facebook-snippet-image">
						<?php if ($img) {  ?>
							<img src="<?=$img?>">
						<?php } else { ?>
							<div class="wsko-social-no-image text-off"><?=__('No Image found', 'wsko');?></div>
						<?php } ?>	
					</div>
					<div class="wsko-facebook-snippet-content">
						<span class="wsko-facebook-title"><?=esc_attr($title)?></span><br/>
						<span class="wsko-facebook-desc"><?=esc_attr($desc)?></span><br/>
						<span class="wsko-facebook-url font-unimportant">URL</span>
					</div>
				</div>
				<?php
				break;
			case 'twitter':
				?>
				<div class="wsko-twitter-snippet wsko-z-depth-1" style="border-radius:2px;">
					<div class="wsko-twitter-snippet-image">
						<?php if ($img) {  ?>
							<img src="<?=$img?>">
						<?php } else { ?>
							<div class="wsko-social-no-image text-off"><?=__('No Image found', 'wsko');?></div>
						<?php } ?>
					</div>
					<div class="wsko-twitter-snippet-content">
						<span class="wsko-twitter-title"><label class="m0"><?=esc_attr($title)?></label></span><br/>
						<span class="wsko-twitter-desc"><?=esc_attr($desc)?></span><br/>
						<span class="font-unimportant">URL</span>
					</div>
				</div>
				<?php
				break;
		}
		if ($return)
			return ob_get_clean();
	}
	public static function render_main_navigation($controllers, $current, $subpage, $return = false)
	{
		if ($return)
			ob_start();
		foreach ($controllers as $contr)
		{
			$contr = $contr::get_instance();
			if ($contr->subpages && is_array($contr->subpages) && !empty($contr->subpages))
			{
				?><div class="wsko-admin-main-navbar-item waves-effect panel <?=$current == $contr::get_link(false, true) ? 'wsko-active' : ''?>" data-link="<?=$contr::get_link(false, true)?>" style="height:auto;">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#wsko_admin_view_sidebar_overlay" href="#wsko_admin_<?=$contr::get_link(false, true)?>_subpages">
							<h4 class="panel-title">
								<i class="fa fa-<?=$contr->icon?> fa-fw wsko-text-icon"></i> <?=$contr->get_title()?> <i class="fa fa-angle-down" style="float:right;"></i>
							</h4>
						</a>
					</div>
					<div id="wsko_admin_<?=$contr::get_link(false, true)?>_subpages" class="wsko-admin-main-navbar-sub-panel panel-collapse collapse <?=$current == $contr::get_link(false, true) ? 'in' : ''?>">
						<div class="panel-body">
							<ul class="wsko-admin-main-sub-navbar">
								<?php
								$pages = $contr::get_subpages();
								foreach($pages as $k => $page)
								{
									?><li class="wsko-admin-main-sub-navbar-item <?=$subpage == $k ? 'wsko-active' : ''?>" data-link="<?=$contr::get_link(false, true).'_'.$contr::get_link($k, true, false)?>">
										<?php WSKO_Class_Template::render_page_link($contr, $k, $contr->get_subpage_title($k))?>
									</li><?php
								}
								?>
							</ul>
						</div>
					</div>
				</div><?php
			}
			else
			{
				?><div class="wsko-admin-main-navbar-item waves-effect <?=$current == $contr::get_link(false, true) ? 'wsko-active' : ''?>" data-link="<?=$contr::get_link(false, true)?>">
					<?php WSKO_Class_Template::render_page_link($contr, false, '<i class="fa fa-'.$contr->icon.' fa-fw wsko-text-icon"></i> '.$contr->get_title())?>
				</div><?php
			}
		}
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_notification($type, $params, $return = false)
	{
		if ($return)
			ob_start();
		
		$uniqid = WSKO_Class_Helper::get_unique_id('wsko_notice_');
		$prio = isset($params['prio']) && $params['prio'] ? $params['prio'] : false;
		$notifClass = isset($params['notif-class']) && $params['notif-class'] ? $params['notif-class'] : false;
		$group = isset($params['group']) && $params['group'] ? $params['group'] : false;
		$discardable = isset($params['discardable']) ? $params['discardable'] : false;
		$class = false;
		$add_support = isset($params['show_support']) && $params['show_support'] ? true : false;
		switch ($type)
		{
			case 'error': $class = "error"; if (!isset($params['show_support'])) $add_support = true; break;
			case 'warning': $class = "warning"; /*if (!isset($params['show_support'])) $add_support = true;*/ break;
			case 'info': $class = "info"; break;
			case 'success': $class = "success"; break;
			default: echo 'Not supported notification type!'; return;
		}
		if ($class)
		{
			if ($discardable)
			{
				$wsko_data = WSKO_Class_Core::get_data();
				if (isset($wsko_data['discard_notice']) && is_array($wsko_data['discard_notice']) && in_array($discardable, $wsko_data['discard_notice']))
				{
					if ($return)
						return "";
					else
						return;
				}
			}
			?><div id="<?=$uniqid?>" class="bs-callout wsko-notice wsko-notice-<?=$class?> <?=$notifClass?>">
				<?php
				if ($discardable)
				{
					?><span class="pull-right"><?php
						WSKO_Class_Template::render_ajax_button('<i class="fa fa-times"></i>', 'discard_notice', array('notice' => $discardable), array('no_reload' => true, 'no_button' => true, 'remove' => '#'.$uniqid));
					?></span><?php
				}
				if ($add_support)
				{
					?><a href="#" class="btn btn-flat btn-sm wsko-give-feedback pull-right">Support</a><?php
				}
				echo $params['msg'];
				if ($prio)
				{
					?><span class="pull-right badge wsko-badge badge-primary" data-toggle="tooltip" title="Priority"><?=$prio?></span><?php
				}
				if ($group)
				{
					?><span class="pull-right badge wsko-badge badge-default" data-toggle="tooltip" title="Issue Group"><?=$group?></span><?php
				}
				if (isset($params['list']) && $params['list'] && is_array($params['list']))
				{
					?><ul><?php
					foreach ($params['list'] as $list_item)
					{
						?><li style="margin-left:20px;list-style-type: disc;"><?php echo $list_item?></li><?php
					}
					?></ul><?php
				}
				if (isset($params['subnote']) && $params['subnote'])
				{
					?><p class="wsko_callout_note"><strong>Note:</strong> <?php echo $params['subnote']?></p><?php
				}
				?>
			</div><?php
		}
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_timespan_widget($return = false)
	{
		if ($return)
			ob_start();
		global $wsko_plugin_path;
		include($wsko_plugin_path. 'admin/templates/template-timespan-select.php');
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_api_statusbar($return = false)
	{
		if ($return)
			ob_start();
		?><a href="<?=WSKO_Controller_Settings::get_link('apis')?>" class="wsko-admin-api-statusbar wsko-load-lazy-page" data-controller="<?=WSKO_Controller_Settings::get_link(false, true)?>" data-subpage="<?=WSKO_Controller_Settings::get_link('apis', true, false)?>"><?php
			$api_message = "Critical Error!";
			$api_color = "red";
			if (WSKO_Class_Search::get_ga_client_se())
			{
				$api_message = "Credentials missing.";
				$api_color = "yellow";
				if (WSKO_Class_Search::get_se_token())
				{
					$api_message = "Credentials provided.";
					$api_color = "green";
				}
			}
			?><p class="wsko-admin-api-status-icon" data-toggle="tooltip" data-title="Google Search API: <?=$api_message?>"><i class="fa fa-search"></i> <i class="fa fa-circle" style="color:<?=$api_color?>"></i></p><?php
			$api_message = "Critical Error!";
			$api_color = "red";
			if (WSKO_Class_Search::get_ga_client_an())
			{
				$api_message = "Credentials missing.";
				$api_color = "yellow";
				if (WSKO_Class_Search::get_an_token())
				{
					$api_message = "Credentials provided.";
					$api_color = "green";
				}
			}
			?><p class="wsko-admin-api-status-icon" data-toggle="tooltip" data-title="Google Analytics API: <?=$api_message?>"><i class="fa fa-bar-chart"></i> <i class="fa fa-circle" style="color:<?=$api_color?>"></i></p><?php
			
			$api_color = "red";
			$api_message = "Critical Error!";
			?><p class="wsko-admin-api-status-icon" data-toggle="tooltip" data-title="Facebook API: <?=$api_message?>"><i class="fa fa-facebook-official"></i> <i class="fa fa-circle" style="color:<?=$api_color?>"></i></p><?php
			
			?><p class="wsko-admin-api-status-icon" data-toggle="tooltip" data-title="Twitter API: <?=$api_message?>"><i class="fa fa-twitter"></i> <i class="fa fa-circle" style="color:<?=$api_color?>"></i></p><?php
			
			?><p class="wsko-admin-api-status-icon" data-toggle="tooltip" data-title="LinkendIn API: <?=$api_message?>"><i class="fa fa-linkedin-square"></i> <i class="fa fa-circle" style="color:<?=$api_color?>"></i></p><?php
			
			?><p class="wsko-admin-api-status-icon" data-toggle="tooltip" data-title="Google Plus API: <?=$api_message?>"><i class="fa fa-google-plus-official"></i> <i class="fa fa-circle" style="color:<?=$api_color?>"></i></p>
		</a><?php
			
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_radial_progress($type, $header, $args = array(), $return = false)
	{ 
		if ($return)
		ob_start(); 
	
		$dataVal = isset($args['val']) && $args['val'] ? round ( $args['val'] ) : 0;
		$class = isset($args['class']) && $args['class'] ? $args['class'] : '';
		$info = isset($args['info']) && $args['info'] ? $args['info'] : '';
		?>
			<div class="wsko-circular-progress <?=$class?>">
				<p class="wsko-circular-progress-label"><?=$header?> <?php if ($info) { ?> <span data-tooltip="<?=$info?>"><i class="fa fa-info fa-fw"></i></span> <?php } ?></p>
				<div class="wsko-circle-progress" data-dimension="40" data-text="<?=$dataVal . '%'?>" data-fontsize="10" data-percent="<?=$dataVal?>" data-fgcolor="#30B455" data-bgcolor="#eee" data-width="10" data-bordersize="3" data-animationstep="2"></div>
			</div>	
		<?php
		
		if ($return)
		return ob_get_clean();
	}	
	
	public static function render_chart($type, $header, $data, $args = array(), $return = false)
	{
		global $wsko_plugin_url;
		
		if ($return)
			ob_start();
		$pixel_height = isset($args['pixel_height']) && $args['pixel_height'] ? intval($args['pixel_height']) : 280;
		$pixel_width = isset($args['pixel_width']) && $args['pixel_width'] ? intval($args['pixel_width']) : 495;
		$chart_height = isset($args['chart_height']) && $args['chart_height'] ? intval($args['chart_height']) : 60;
		$chart_width = isset($args['chart_width']) && $args['chart_width'] ? intval($args['chart_width']) : 85;
		$legend_pos = isset($args['legend_pos']) && $args['legend_pos'] ? $args['legend_pos'] : 'top';
		$legend_align = isset($args['legend_align']) && $args['legend_align'] ? $args['legend_align'] : 'end';		
		$isStacked = isset($args['isStacked']) && $args['isStacked'] ? true : false;
		$table_filter = isset($args['table_filter']) ? $args['table_filter'] : false;
		$chart_id = isset($args['chart_id']) ? $args['chart_id'] : '';
		$axisTitle = isset($args['axisTitle']) ? $args['axisTitle'] : '';
		$suffix = isset($args['suffix']) ? $args['suffix'] : false;
		
		$class = false;
		switch ($type)
		{
			case 'area': $class = "AreaChart"; break;
			case 'line': $class = "LineChart"; break;
			case 'column': $class = "ColumnChart"; break;
			case 'bar': $class = "BarChart"; break;
			case 'pie': $class = "PieChart"; break;
			case 'world': $class = "GeoChart"; break;
			default: return;
		}
		$uniqid  = WSKO_Class_Helper::get_unique_id('wsko_graph_');
		?>
		<div class="wsko-brand">
			<img src="<?=$wsko_plugin_url?>/admin/img/logo-bl.png" />
			<div class="wsko-chart-wrapper" id="<?=$uniqid?>"></div>
		</div>
		<style>
			div.wsko-chart-wrapper svg g:last-child,
			.google-visualization-tooltip { 
				/* pointer-events: none; */ 
				padding:0px;
				border-color:#ddd;
				-webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.06);
				box-shadow: 0 1px 5px rgba(0,0,0,0.06);
			}
		</style>
		<script type="text/javascript">
		jQuery(document).ready(function($){
			google.charts.setOnLoadCallback(function () {
				var options = {
					//width: <?=$pixel_width?>,
					width: '100%',
					height: <?=$pixel_height?>,
					chartArea: {  width: "<?=$chart_width?>%", height: "<?=$chart_height?>%" },
					tooltip: { isHtml: true },
				
				//Custom Options
				<?php if ($type == 'area' || $type == 'line') { ?>	
				  isStacked: <?=$isStacked ? 'true' : 'false'?>,
				  hAxis: {title: 'Date',  titleTextStyle: {color: '#888'}, textStyle: {color: '#888'}, gridlines: {color:'transparent'}, showTextEvery: 6},
				  vAxis: {minValue: 0, titleTextStyle: {color: '#888'}, textStyle: {color: '#888'}, gridlines: {color:'#eee'}, baselineColor: '#ccc'},	
				  backgroundColor: { fill:'transparent' },
				  pointSize: 3,				  
				<?php } else if ($type == 'bar') { ?>
				  hAxis: {title: 'Date',  titleTextStyle: {color: '#888'}, textStyle: {color: '#888'}, gridlines: {color:'#eee'}, baselineColor: '#ccc'},
				  vAxis: {minValue: 0, titleTextStyle: {color: '#888'}, textStyle: {color: '#888'}, gridlines: {color:'transparent'}},			
				  backgroundColor: { fill:'transparent' },				  
				<?php } else if ($type == 'column') {?>
				  isStacked: <?=$isStacked ? 'true' : 'false'?>,
				  hAxis: {title: '<?=$axisTitle?>',  titleTextStyle: {color: '#888'}, textStyle: {color: '#888'}, gridlines: {color:'transparent'}, showTextEvery: 2},
				  vAxis: {minValue: 0, titleTextStyle: {color: '#888'}, textStyle: {color: '#888'}, gridlines: {color:'#eee'}, baselineColor: '#ccc'},	
				  backgroundColor: { fill:'transparent' },				  
				<?php } ?>


				/* Colors */
				<?php if ($type == 'world') { ?>
				  colors: ['#fff', '#337ab7'],
				<?php } else if ($type == 'area') { ?>				
				  series: {
					  0:{color: '#337AB7', areaOpacity: 0.3},
					  1:{color: '#86c5f0', areaOpacity: 0.05}
				  },
				<?php } else if ($type == 'line') { ?>
				  colors: ['#4ea04e', '#5CB85C', '#88C859', '#C5D955', '#d9ca55', '#EAC050', '#EFAC4D', '#EB984E', '#E6854E', '#dc6b3e', '#d26839'],
				<?php } else { ?>				
				  colors: ['#337AB7', '#3498db', '#1ABC9C', '#2ECC71', '#16a085'],	
				<?php } ?> 
				
				/* Onpage Charts Color */
				<?php
				switch ($chart_id)
					{
						/* 'title_dupl': ?> series: {0:{color: '#5cb85c'}, 1:{color: '#f0ad4e'}, 2:{color: '#d9534f'}, 3:{color: '#d9534f'}, 4:{color: '#d9534f'}, 5:{color: '#d9534f'}, 6:{color: '#d9534f'}, 7:{color: '#d9534f'}, 8:{color: ''}}, <?php ; break; */
						/* case 'desc_dupl': ?> colors: ['#5cb85c', '#f0ad4e', '#d9534f', '#d9534f', '#d9534f', '#d9534f', '#d9534f', '#d9534f', '#d9534f'], <?php ; break; */
						case 'headings': ?> colors: ['#d9534f', '#27ae60', '#1ABC9C', '#2ECC71', '#16a085'], <?php ; break;
						case 'fb_snippet': ?> colors: ['#d9534f', '#5cb85c', '#f0ad4e', '#f0ad4e'], <?php ; break;
						case 'tw_snippet': ?> colors: ['#d9534f', '#5cb85c', '#f0ad4e', '#f0ad4e'], <?php ; break;
						case 'prio_keywords': ?> colors: ['#d9534f', '#5cb85c'], <?php ; break;
						/* case 'content_length': ?> colors: ['#d9534f', 'red', '#1ABC9C', '#2ECC71', '#16a085'], <?php ; break; */
					}	
				?>
				  legend: { position: '<?=$legend_pos?>', alignment: '<?=$legend_align?>', scrollArrows: {activeColor: '#337ab7',inactiveColor: '#ccc'}, pagingTextStyle: {color:'#337ab7'} },
				  animation: {"startup": true, duration: 1200, easing: 'out'}	
				};
				var data = google.visualization.arrayToDataTable([
					['<?=implode("','", $header)?>'],
					<?php foreach ($data as $row)
					{
						$first = true;
						?>[<?php foreach($row as $val)
						{	if (!$first) echo ',';
							$first = false;
							if (is_integer($val) || is_numeric($val))
								echo $val;
							else
								echo '"'.$val.'"';
						}?>],<?php
					} ?>
				]);

				<?php if ($suffix) { ?>
					var formatter = new google.visualization.NumberFormat(
						{suffix: ' <?=$suffix?>'});
					formatter.format(data, 1);
					formatter.format(data, 2);
					formatter.format(data, 3);
					formatter.format(data, 4);
					formatter.format(data, 5);
					formatter.format(data, 6);
					formatter.format(data, 7);
					formatter.format(data, 8);
					formatter.format(data, 9);
					formatter.format(data, 10);
				<?php } ?>

				var chart = new google.visualization.<?=$class?>($('#<?=$uniqid?>').get(0));
				var needsRedraw = true;
				function draw()
				{
					if (needsRedraw)
					{
						if ($('#<?=$uniqid?>').is(":visible"))
						{
							chart.draw(data, options);
							needsRedraw = false;
						}
					}
				}
				var resizeTO;
				function queue_redraw(instant)
				{
					if(resizeTO) clearTimeout(resizeTO);
					
					if (instant)
						draw()
					else
						resizeTO = setTimeout(function() {
							draw();
						}, 1000);
				}
				<?php if ($table_filter) { ?>
				var columnReg = JSON.parse('<?=json_encode($table_filter['value_matrix'])?>');
				google.visualization.events.addListener(chart, 'select', function (){
					var selection = chart.getSelection();
					for (var i = 0; i < selection.length; i++) {
						var item = selection[i];
						if (item.row != null && item.column != null)
						{
							if (columnReg && columnReg[item.column-1] && columnReg[item.column-1][item.row])
								$('<?=$table_filter['table']?>').trigger('wsko_add_external_filter', [columnReg[item.column-1][item.row]]);
						}
					}
				});
				<?php } ?>
				$(window).resize(function() {
					needsRedraw = true;
					queue_redraw();
				});
				//$(document).on('wsko_init_page', function() {
					//queue_redraw();
					$('#<?=$uniqid?>').parents('.tab-pane').each(function(index){
						var $pane = $(this);
						id = $pane.attr('id');
						if (id)
						{
							$('a[href="#'+id+'"][data-toggle="tab"]').on('shown.bs.tab', function(event){
								queue_redraw(true);
							});
						}
					});
					queue_redraw(true);
				//});
			});
		});
		</script><?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_table($header, $data, $params = array(), $return = false)
	{
		if ($return)
			ob_start();
		$id = isset($params['id']) && $params['id'] ? $params['id'] : false;
		$class = isset($params['class']) && $params['class'] ? $params['class'] : false;
		$ajax = isset($params['ajax']) && $params['ajax'] ? $params['ajax'] : false;
		$filter = isset($params['filter']) && $params['filter'] ? $params['filter'] : false;
		$order = isset($params['order']) && $params['order'] ? $params['order'] : false;
		$no_pages = isset($params['no_pages']) && $params['no_pages'] ? true : false;
		$change_search_text = isset($params['change_search_text']) ? $params['change_search_text'] : false;
		?>
		<div class="wsko-table-wrapper <?=$no_pages?'wsko-table-small':''?>">
			<?php
			if ($filter)
			{
				?><div style="position:relative;z-index:3;">
					<div class="dropdown" style="display:inline-block;">
						<button class="btn btn-flat dropdown-toggle" type="button" data-toggle="dropdown">Filter  <i class="fa fa-angle-down fa-fw"></i></button>
						<ul class="dropdown-menu">
							<?php
							foreach ($filter as $k => $f)
							{
								$add = "";
								if ($f['type'] == 'select' && !$f['values'])
									continue;
								switch ($f['type'])
								{
									case 'number_range': $add = (isset($f['min'])?' data-min="'.$f['min'].'"':'').(isset($f['max'])?' data-max="'.$f['max'].'"':''); break;
									case 'select': $add = ' data-values="'.htmlspecialchars(json_encode($f['values'])).'"'; break;
								}
								?><li><a href="#" class="wsko-table-add-filter" data-name="<?=$k?>" data-type="<?=$f['type']?>" data-title="<?=$f['title']?>"<?=$add?>><?=$f['title']?></a></li><?php
							}
							?>
						</ul>
					</div>
					
					<!-- if filter active -->
					<?php /* <button class="btn btn-flat" type="button">Clear Filter</button> */ ?>
					
					<div class="wsko-table-filter-box">
					</div>
				</div><?php
			}
			?>
			<table <?=$id ? 'id="'.$id.'"' : ''?> class="table table-striped table-bordered wsko-tables table-condensed <?=$class ? $class : ''?> <?=$ajax ? 'wsko-ajax-tables' : ''?>" <?=$change_search_text ? 'data-change-search="'.$change_search_text.'"' : ''?> <?=$order ? 'data-def-order="'.$order['col'].'" data-def-orderdir="'.$order['dir'].'"' : ''?> <?=$ajax ? 'data-action="'.$ajax['action'].'" data-nonce="'.wp_create_nonce($ajax['action']).'"'.(isset($ajax['arg'])?' data-arg="'.$ajax['arg'].'"':'') : ''?> cellspacing="0" width="100%">
				<thead>
					<tr>
					<?php
					if ($ajax)
					{
						foreach ($header as $k => $h)
						{
							?><th data-name="<?=$k?>"><?=$h?></th><?php
						}
					}
					else
					{
						?><th><?=implode("</th><th>", $header)?></th><?php
					}
					?>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th><?=implode("</th><th>", $header)?></th>
					</tr>
				</tfoot>
				<tbody>
					<?php
					if ($data)
					{
						foreach ($data as $row)
						{
							?><tr><?php
							foreach ($row as $value)
							{
								?><td <?=isset($value['class']) ? 'class="'.$value['class'].'"' : ''?> <?=isset($value['order']) ? 'data-order="'.$value['order'].'"' : ''?>><?=$value['value']?></td><?php
							}
							?></tr><?php
						}
					} ?>
				</tbody>
			</table>
		</div>
		<?php
		if ($return)
			return ob_get_clean();
	}
	

	public static function render_progress($title, $value, $max, $args = array(), $return = false)
	{
		if ($return)
			ob_start();
		$data = isset($args['data']) ? $args['data'] : false;
		$class = isset($args['class']) ? $args['class'] : false;
		$progress_class = isset($args['progress_class']) ? $args['progress_class'] : false;
		?>
		<div class="row wsko-progress-wrapper">
			<div class="col-sm-3 col-xs-12">
				<span><?=$title?></span>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="wsko-progress-bar progress <?=$class?>" data-toggle="tooltip" title="<?=$value?>" <?php foreach ($data as $k => $d) { echo ' data-'.$k.'="'.$d.'"'; } ?>>
				  <div class="progress-bar progress-bar-<?=$progress_class?>" role="progressbar" style="width: <?=(($max && $value) ? floor(($value / $max) * 100) : 0)?>%" aria-valuenow="<?=$value?>" aria-valuemin="0" aria-valuemax="<?=$max?>"></div>
				</div>
			</div>
		</div>			
		<?php
		if ($return)
			return ob_get_clean();
	}	

	public static function render_progress_stacked($title, $values, $max, $args = array(), $return = false)
	{
		if ($return)
			ob_start();
		$class = isset($args['class']) ? $args['class'] : false;
		?>
		<div class="row wsko-progress-wrapper">
			<div class="col-sm-3 col-xs-12">
				<span><?=$title?></span>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="wsko-progress-bar progress <?=$class?>" >
					<?php
					$sum = 0;
					foreach($values as $k => $v)
					{
						$sum += $values[$k]['width_val'] = $max ? floor(($v['value'] / $max) * 100 * 100) / 100 : 0;
					}
					if ($sum > 100)
						$add = -($sum - 100);
					else
						$add = 100 - $sum;
					foreach($values as $v)
					{
						$data = isset($v['data']) ? $v['data'] : false;
						$width_val = $v['width_val'];
						if ($add && $width_val && ($width_val+$add) > 0)
						{
							$width_val += $add;
							$add = 0;
						}
						?><div class="progress-bar progress-bar-<?=$v['class']?>" role="progressbar" <?php foreach ($data as $k => $d) { echo ' data-'.$k.'="'.$d.'"'; } ?> style="width: <?=$width_val?>%"><?=$v['title']?></div><?php
					} ?>
				</div>
			</div>
		</div>			
		<?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_progress_icon($val, $val_perc, $params = array(), $return = false)
	{
		$tooltip_add = isset($params['tooltip']) ? $params['tooltip'] : false;
		if ($return)
			ob_start();
		?><span class="wsko-progress-icon wsko-ml5 <?=($val_perc ? ($val_perc < 0 ? 'wsko-red' : 'wsko-green') : 'wsko-gray')?>" data-tooltip="<?=round($val, 2).($tooltip_add!==false?$tooltip_add:'')?>"><?php
		if ($val_perc < 0)
		{
			?><i class="fa fa-angle-down"></i><?php
		}
		else if ($val_perc > 0)
		{
			?><i class="fa fa-angle-up"></i><?php
		}
		else
		{
			?>-<?php
		}
		?> <?=$val_perc?>%</span><?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_template($template, $template_args = array(), $return = false)
	{
		if ($return)
			ob_start();
		global $wsko_plugin_path;
		include($wsko_plugin_path. $template);
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_preloader( $args = array(), $return = false )
	{
		$size = isset($args['size']) && $args['size'] ? $args['size'] : '';
		$align = isset($args['align']) && $args['align'] ? $args['align'] : '';
		
		if ($return)
			ob_start();
		?>

		<div class="align-<?=$align?> display-block">
			  <div class="preloader-wrapper <?=$size?> active">
				<div class="spinner-layer spinner-blue-only">
				  <div class="circle-clipper left">
					<div class="circle"></div>
				  </div><div class="gap-patch">
					<div class="circle"></div>
				  </div><div class="circle-clipper right">
					<div class="circle"></div>
				  </div>
				</div>
			  </div>
		</div>  
		<?php 
		if ($return)
			return ob_get_clean();		
	}
	
	public static function render_wsko_preloader( $args = array() )
	{
		$size = isset($args['size']) && $args['size'] ? $args['size'] : '';
		$align = isset($args['align']) && $args['align'] ? $args['align'] : '';
		
		ob_start();
		?>
		<div class="wsko-modal-loader">
		  <div class="loader">
			<svg class="circular" viewBox="25 25 50 50">
			  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
			</svg>
		  </div>
		</div> 
		<?php 
		return ob_get_clean();		
	}

	public static function render_infoTooltip($content, $type, $return = false) 
	{
		if ($return)
			ob_start();
				?>
				<span data-tooltip="<?=$content?>">
					<i class="fa fa-question fa-fw wsko-info"></i>
				</span>
				<?php		
		if ($return)
			return ob_get_clean();			
	}	

	public static function render_issueGroup($args = array()) 
	{
		$title = isset($args['title']) && $args['title'] ? $args['title'] : '';
		$items = isset($args['items']) ? $args['items'] : false;
		$col = isset($args['col']) && $args['col'] ? $args['col'] : '';
		$class = isset($args['class']) && $args['class'] ? $args['class'] : '';	
		$info = isset($args['info']) && $args['info'] ? $args['info'] : '';	
		$parent = isset($args['parent']) && $args['parent'] ? $args['parent'] : false;
		$active = isset($args['active']) && $args['active'] ? true : false;
		$uniqid  = WSKO_Class_Helper::get_unique_id('wsko_collapse_panel_');		
		?>
		<div class="bsu-panel bsu-panel-issue <?=$col?> panel panel-default p0">
			<div class="panel-heading">
				<h4 class="panel-title">			
					<a data-toggle="collapse" data-parent="#<?=$parent?>" href="#<?=$uniqid?>"><span class="pull-right"><i class="fa fa-angle-down font-unimportant"></i></span> 
						<?php /* <span class="icon"><i class="fa fa-fw fa-<?=$fa?>"></i></span> */ ?>
						<span><?=$title?> <?php if ($info) WSKO_Class_Template::render_infoTooltip($info, 'info'); ?></span>
					</a>
				</h4>	
			</div>
			<div id="<?=$uniqid?>" class="panel-collapse collapse <?=$active?'in':''?>">
				<div class="panel-body">
					<?php if ($items) {
						usort($items, function($a, $b){
							if ($a['class'] == $b['class'])
							{
								if ($a['count'] == $b['count'])
									return 0;
								return $a['count'] > $b['count'] ? -1 : 1;
							}
							if ($a['class'] == 'error')
								return -1;
							if ($a['class'] == 'warning')
								return 1;
						});
						foreach ($items as $i) {
							echo WSKO_Class_Template::render_panel(array('type' => 'issue', 'title' => $i['title'], 'issue' => $i['issue'], 'class' => $i['class']));		
							//echo WSKO_Class_Template::render_notification('success', array('msg' => $i['title']))		;				
						}
					} ?>
				</div>
			</div>	
		</div>
		<?php
	}		
	
	public static function render_panel($args = array(), $return = false)
	{
		$hasLink = false;
		$type = isset($args['type']) && $args['type'] ? $args['type'] : 'default';
		$title = isset($args['title']) && $args['title'] ? $args['title'] : '';
		$col = isset($args['col']) && $args['col'] ? $args['col'] : '';
		$class = isset($args['class']) && $args['class'] ? $args['class'] : '';
		$fa = isset($args['fa']) && $args['fa'] ? $args['fa'] : '';
		$custom = isset($args['custom']) ? $args['custom'] : '';
		$info = isset($args['info']) && $args['info'] ? $args['info'] : '';		
		
		if ($type == 'complex') {
			$lazyVar = isset($args['lazyVar']) && $args['lazyVar'] ? $args['lazyVar'] : '';		
			$lazyObj = explode ( ',' , $lazyVar);
		}
		else {
			$lazyVar = isset($args['lazyVar']) && $args['lazyVar'] ? $args['lazyVar'] : '';		
		}
		
		if ($return)
			ob_start();
		
		switch ($type) {
			case 'hero':
				if (isset($args['tableLink']) && is_array($args['tableLink']) && isset($args['tableLink']['link']) && isset($args['tableLink']['title']))
				{
					$hasLink = true;
					$tableLink = $args['tableLink']['link'];
					$tableAnchor = $args['tableLink']['title'];
				}
				?>
				<div class="bsu-panel bsu-panel-hero <?=$col?>">
					<div class="panel panel-default">
						<span class="pull-right icon"><i class="fa fa-<?=$fa?>"></i></span>
						<?php if ($hasLink) { ?>
							<a class="panel-link pull-right waves-effect btn-flat btn-sm" href="<?=$tableLink?>"><?=$tableAnchor?></a>
						<?php } ?>
						<div class="panel-inner">
							<?php if ($lazyVar) { 
									?> 
									<span class="wsko-label"> <?php
										WSKO_Class_Template::render_lazy_field($lazyVar, 'small');
									?> </span>
									<?php	
								} else { ?>
								<div></div>
							<?php } ?>	
							
							<p><?=$title?> <?php if ($info) WSKO_Class_Template::render_infoTooltip($info, 'info'); ?></p>
							<?php /* <?=$custom?> */ ?>
						</div>	
					</div>	
				</div>
				<?php
				break;
			case 'hero-custom':
				?>
				<div class="bsu-panel bsu-panel-hero <?=$col?>">
					<div class="panel panel-default">
						<span class="pull-right icon"><i class="fa fa-<?=$fa?>"></i></span>
						
						<div class="panel-inner">
							<span class="wsko-label"><?=$custom?></span>
							
							<p><?=$title?></p>
						</div>	
					</div>	
				</div>
				<?php
				break;
			case 'lazy':
				$panelBody = isset($args['panelBody']) && $args['panelBody'] ? true : false;
				if (isset($args['tableLink']) && is_array($args['tableLink']) && isset($args['tableLink']['link']) && isset($args['tableLink']['title']))
				{
					$hasLink = true;
					$tableLink = $args['tableLink']['link'];
					$tableAnchor = $args['tableLink']['title'];
				}
				?>
				<div class="bsu-panel <?=$col?>">
					<div class="panel panel-default">
						<?php if ($hasLink) { ?>
							<a class="panel-link pull-right waves-effect btn-flat btn-sm" href="<?=$tableLink?>"><?=$tableAnchor?></a>
						<?php } ?>	
						<p class="panel-heading m0"><?=$title?> <?php if ($info) WSKO_Class_Template::render_infoTooltip($info, 'info'); ?></p>
						<?=$custom?>
						<div class="<?=$panelBody ? 'panel-body' : ''?>">
							<?php WSKO_Class_Template::render_lazy_field($lazyVar, 'small', 'center'); ?>
						</div>	
					</div>
				</div>
				<?php
				break;				
			case 'table-simple':
			case 'table':
				if (isset($args['tableLink']) && is_array($args['tableLink']) && isset($args['tableLink']['link']) && isset($args['tableLink']['title']))
				{
					$hasLink = true;
					$tableLink = $args['tableLink']['link'];
					$tableAnchor = $args['tableLink']['title'];
				}
				?><div class="bsu-panel bsu-panel-table <?=$col?>">
					<div class="panel panel-default">
						<?php if ($hasLink) { ?>
							<a class="panel-link pull-right waves-effect btn-flat btn-sm" href="<?=$tableLink?>"><?=$tableAnchor?></a>
						<?php } ?>	
						<p class="panel-heading m0"><?=$title?></p>
						<?=$custom?>
						<div class="<?=$type == 'table-simple' ? 'wsko_table_simple' : ''?>">
						<?php WSKO_Class_Template::render_lazy_field($lazyVar, 'small', 'center'); ?>
						</div>	
					</div>
				</div>
				<?php	
				break;
			case 'complex':
				if (isset($args['tableLink']) && is_array($args['tableLink']) && isset($args['tableLink']['link']) && isset($args['tableLink']['title']))
				{
					$hasLink = true;
					$tableLink = $args['tableLink']['link'];
					$tableAnchor = $args['tableLink']['title'];
				}
				?><div class="bsu-panel bsu-panel-table <?=$col?>">
					<div class="panel panel-default">
						<?php if ($hasLink) { ?>
							<a class="panel-link pull-right waves-effect btn-flat btn-sm" href="<?=$tableLink?>"><?=$tableAnchor?></a>
						<?php } ?>	
						<p class="panel-heading m0"><?=$title?></p>
						
						<?php foreach ($lazyObj as $lazyVar) { ?>
							<div class="bsu-panel bsu-complex">
								<?php WSKO_Class_Template::render_lazy_field($lazyVar, 'small', 'center'); ?>
							</div>
						<?php } ?>
											
					</div>
				</div>
				<?php	
				break;
			case 'issue':		
				$issue = isset($args['issue']) ? $args['issue'] : false;		
				?>
					<div class="bs-callout wsko-notice">
							<?php /* <span class="icon"><i class="fa fa-fw fa-<?=$fa?>"></i></span> */ ?>
							<span><?=$title?> <?php if ($info) WSKO_Class_Template::render_infoTooltip($info, 'info'); ?></span>
							<span class="wsko-issue-link badge wsko-badge badge-default" style="float:none;"><?=$issue?></span>
							<span class="wsko-issue-link badge wsko-badge badge-<?=$class?>" style="float:none; margin-left:5px;"><?=$class == 'error' ? 'Prio 1' : 'Prio 2'?></span>
					</div>												
					<?php
				break;				
			case 'collapse':
				$parent = isset($args['parent']) && $args['parent'] ? $args['parent'] : false;
				$active = isset($args['active']) && $args['active'] ? true : false;
				$uniqid  = WSKO_Class_Helper::get_unique_id('wsko_collapse_panel_');
				?>
				<div class="bsu-panel panel panel-default <?=$class?>">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#<?=$parent?>" href="#<?=$uniqid?>"><span class="pull-right"><i class="fa fa-angle-down font-unimportant"></i></span> <?=$title?></a>
						</h4>
					</div>
					<div id="<?=$uniqid?>" class="panel-collapse collapse <?=$active?'in':''?>">
						<div class="panel-body">
							<?php 
							if ($lazyVar) {
								WSKO_Class_Template::render_lazy_field($lazyVar, 'small', 'center'); 
							} else if ($custom) {
								echo $custom;
							} 
								
							?>
						</div>
					</div>
				</div>
				<?php
				break;
			case 'progress':
				$items = isset($args['items']) ? $args['items'] : false;
				?>
				<div class="bsu-panel bsu-panel-progress <?=$col?>">
					<div class="panel panel-default">
						<?php if ($hasLink) { ?>
							<a class="panel-link pull-right waves-effect btn-flat btn-sm" href="<?=$tableLink?>"><?=$tableAnchor?></a>
						<?php } ?>	
						<p class="panel-heading m0"><?=$title?></p>
						<div class="panel-body">
							<?php
							foreach ($items as $i)
							{
								if (isset($i['stacked']) && $i['stacked'])
									WSKO_Class_Template::render_progress_stacked($i['title'], $i['value'], $i['max'], array('data' => isset($i['data'])?$i['data']:false, 'class' => isset($i['class'])?$i['class']:false));
								else
									WSKO_Class_Template::render_progress($i['title'], $i['value'], $i['max'], array('data' => isset($i['data'])?$i['data']:false, 'class' => isset($i['class'])?$i['class']:false, 'progress_class' => $i['progress_class']));
							}
							?>
						</div>
					</div>	
				</div>
				<?php
				break;	
			case 'custom':
				?>
				<div class="bsu-panel bsu-panel-chart <?=$col?>">
					<div class="panel panel-default">
						<p class="panel-heading m0"><?=$title?> <?php if ($info) WSKO_Class_Template::render_infoTooltip($info, 'info'); ?></p>
						<?=$custom?>
					</div>
				</div>
				<?php	
				break;			
		}
		if ($return)
			return ob_get_clean();		
	}
	
	public static function render_form($args = array(), $return = false)
	{
		$type = isset($args['type']) && $args['type'] ? $args['type'] : 'default';  //input, textarea, editor, select, checkbox, radio
		$id = isset($args['id']) && $args['id'] ? $args['id'] : '';	
		$name = isset($args['name']) && $args['name'] ? $args['name'] : '';			
		$title = isset($args['title']) && $args['title'] ? $args['title'] : '';
		$subtitle = isset($args['subtitle']) && $args['subtitle'] ? $args['subtitle'] : '';		
		$value = isset($args['value']) && $args['value'] ? $args['value'] : '';		
		$placeholder = isset($args['placeholder']) && $args['placeholder'] ? $args['placeholder'] : '';	
		$rows = isset($args['rows']) && $args['rows'] ? $args['rows'] : '';			
		$class = isset($args['class']) && $args['class'] ? $args['class'] : '';	
		$nonce = isset($args['nonce']) && $args['nonce'] ? $args['nonce'] : '';	
		$dataPost = isset($args['data-post']) && $args['data-post'] ? $args['data-post'] : '';	
		$progressBar = isset($args['progressBar']) && $args['progressBar'] ? true : false;	
		$disabled = isset($args['disabled']) && $args['disabled'] ? 'disabled' : '';	
		//$default = isset($args['default']) && $args['default'] ? $args['default'] : '';		
		//$col = isset($args['col']) && $args['col'] ? $args['col'] : '';
		//$fa = isset($args['fa']) && $args['fa'] ? $args['fa'] : '';
		if ($progressBar) { 
			$progressID = isset($args['progressID']) && $args['progressID'] ? $args['progressID'] : '';	
			$progressType = isset($args['progressType']) && $args['progressType'] ? $args['progressType'] : '';	
		}	
		
		
		if ($return)
			ob_start();
		
		?>
			<div class="wsko-row row form-group">
				<div class="wsko-col-sm-3 wsko-col-xs-12 col-sm-3 col-xs-12">
					<p><?=$title?></p>
					<small class="font-unimportant"><?=$subtitle?></small>
				</div>
				<div class="wsko-col-sm-9 wsko-col-xs-12 col-sm-9 col-xs-12">
					<?php
					switch ($type) {
					case 'input':
						?>
						<input class="form-control wsko-form-control <?=$class?>" <?=$disabled?> id="<?=$id?><?=$progressBar&&$progressID ? '-' . $progressID : '';?>" name="<?=$name?>" placeholder="<?=$placeholder?>" value="<?=$value?>">
						<?php
						break;
					case 'textarea':
						?>
						<textarea class="form-control wsko-form-control <?=$class?>" <?=$disabled?> id="<?=$id?><?=$progressBar&&$progressID ? '-' . $progressID : '';?>" name="<?=$name?>" placeholder="<?=$placeholder?>" rows="<?=$rows?>"><?=$value?></textarea> 
						<?php
						break;	
					case 'previewable_textarea':
						$highlights = isset($args['highlights']) && $args['highlights'] ? $args['highlights'] : '';
						?>
						<div class="wsko-previewable-textarea" data-highlights="<?=$highlights ? htmlspecialchars(json_encode($highlights)) : ''?>">
							<textarea <?=$disabled?> id="<?=$id?>" name="<?=$name?>" placeholder="<?=$placeholder?>" rows="<?=$rows?>"><?=$value?></textarea> 
							<div class="wsko-textarea-preview">
							</div>
						</div>
						<?php
					break;
					case 'editor':
						?>
						<?php wp_editor( $value, $id ); ?> 
						<?php
						break;	
					case 'select':
						?>
						
						<?php
						break;	
					case 'checkbox':
						?>
						
						<?php
						break;	
					case 'radio':
						?>
						
						<?php
						break;
					case 'multi':
						?>
						
						<?php
						break;						
					case 'submit':
						?>
						<button class="button <?=$class?>" id="<?=$id?>" data-post="<?=$dataPost?>" data-nonce="<?=wp_create_nonce($nonce)?>"><?=__( 'Save', 'wsko' ) ?></button>
						<?php
						break;						
					}
					
					/* Progress Bar */
					if ($progressBar) { 
					
						$dataMax;
						$dataMin;
						
						switch ($progressType) {
							case 'google_title':
									$dataMax = WSKO_ONPAGE_TITLE_MAX;
									$dataMin = WSKO_ONPAGE_TITLE_MIN;
								break;
							case 'google_desc':
									$dataMax = WSKO_ONPAGE_DESC_MAX;
									$dataMin = WSKO_ONPAGE_DESC_MIN;
								break;
							case 'fb_title':
									$dataMax = WSKO_ONPAGE_FB_TITLE_MAX;
								break;
							case 'fb_desc':
									$dataMax = WSKO_ONPAGE_FB_DESC_MAX;
								break;
							case 'tw_title':
									$dataMax = WSKO_ONPAGE_TW_TITLE_MAX;
								break;
							case 'tw_title':
									$dataMax = WSKO_ONPAGE_TW_DESC_MAX;
								break;								
						}
						?>
						<div style="height: 8px;">
							<progress id="wsko_progress_<?=$progressID?>" class="wsko-progress" value="" data-min="<?=$dataMin?>" max="<?=$dataMax?>"></progress>
						</div>
						<script>
							
							jQuery(document).ready(function($)
							{	
								$('#<?=$id?>-<?=$progressID?>').on('keyup', function() {
									var $val = $(this).val().length;
									var $progress = $('#wsko_progress_<?=$progressID?>');
									var $max = $progress.attr('max');
									var $min = $progress.attr('data-min');
									
									$progress.attr( 'value', $val );
									
									if ($val < $min) {
										$progress.removeClass('wsko-success wsko-error').addClass('wsko-warning');
									}
									if ($val >= $min) {
										$progress.removeClass('wsko-warning wsko-error').addClass('wsko-success');
									}
									if ($val > $max) {
										$progress.removeClass('wsko-success wsko-warning').addClass('wsko-error');
									}
								}).trigger('keyup') ;	
							});	
						</script>
					<?php } ?>
				</div>
			</div>
		<?php

		if ($return)
			return ob_get_clean();		
	}
	

	public static function render_ajax_button($title, $action, $post_params = array(), $args = array(), $return = false)
	{
		if ($return)
			ob_start();
		$alert = isset($args['alert']) ? $args['alert'] : false;
		$no_button = isset($args['no_button']) && $args['no_button'] ? true : false;
		$no_reload = isset($args['no_reload']) && $args['no_reload'] ? true : false;
		$remove = isset($args['remove']) ? $args['remove'] : false;
		?>
		<a class="<?=$no_button?'':'button'?> wsko-ajax-button" href="#" data-action="<?='wsko_'.$action?>" data-nonce="<?=wp_create_nonce('wsko_'.$action)?>"<?php foreach ($post_params as $key => $param) { ?> data-wsko-post-<?=$key?>="<?=$param?>"<?php } ?> <?=$alert?'data-alert="'.$alert.'"':''?> <?=$no_reload?'data-no-reload="true"':''?> <?=$remove?'data-remove="'.$remove.'"':''?>><i class="wsko-load-icon fa fa-spinner fa-pulse" style="display:none"></i> <?=$title?></a>
		<?php
		if ($return)
			return ob_get_clean();
	}
	public static function render_revoke_api_button($api, $return = false)
	{
		if ($return)
			ob_start();
		WSKO_Class_Template::render_ajax_button('Revoke Access/Logout', 'revoke_api_access', array('api' => $api), array('alert' => "Your credentials will be deleted. Are you sure?"), false);
		if ($return)
			return ob_get_clean();
	}
	public static function render_recache_api_button($api, $return = false)
	{
		if ($return)
			ob_start();
		WSKO_Class_Template::render_ajax_button('Update Cache Manually', 'update_api_cache', array('api' => $api), array('alert' => "Be advised: A CronJob is allready waiting to update your cache, just wait a few minutes. Please only make use of this button, when you are sure that your CronJob has failed and you don't want to wait another hour."), false);
		if ($return)
			return ob_get_clean();
	}
	public static function render_delete_api_cache_button($api, $return = false)
	{
		if ($return)
			ob_start();
		WSKO_Class_Template::render_ajax_button('Clear Cache', 'delete_api_cache', array('api' => $api), array('alert' => "Your API cache will be completely deleted. Are you sure?"), false);
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_url_resolve_field($url, $return = false)
	{
		if ($return)
			ob_start();
		?>
		<span><p class="wsko-ajax-url-field" data-url="<?=$url?>"><i class="fa fa-spinner fa-pulse"></i></p><a class="font-unimportant" href="<?=$url?>"><?=$url?></a></span>
		<?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_url_post_field($post_id, $params = array(), $return = false)
	{
		if ($return)
			ob_start();
		$url = get_permalink($post_id);
		WSKO_Class_Template::render_url_post_field_s($url, $params, false);
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_url_post_field_s($url, $params = array(), $return = false)
	{
		if ($return)
			ob_start();
		$with_optimizer = isset($params['with_co']) && $params['with_co'] ? true : false;
		$open_tab = isset($params['open_tab']) ? $params['open_tab'] : false;
		$title_d = WSKO_Class_Helper::url_get_title($url);
		if ($title_d->type == "post")
		{
			?><span>
				<?php if ($with_optimizer)
				{
					?><p style="float:right"><?php WSKO_Class_Template::render_content_optimizer_link($title_d->post_id, array('open_tab' => $open_tab), false); ?></p><?php
				} ?>
				<a href="#" class="wsko-content-optimizer-link wsko-co-no-style" data-post="<?=$title_d->post_id?>"><?=$title_d->title_empty?$title_d->title:esc_html($title_d->title)?></a><br/>
				<p class="font-unimportant"><?=$url?> <a href="<?=$url?>" target="_blank"><i class="fa fa-link"></i></a></p>
			</span><?php
		}
		else
		{
			?><span><?=$title_d->title?><br/><p class="font-unimportant"><?=$url?> <a href="<?=$url?>" target="_blank"><i class="fa fa-link"></i></a></p></span><?php
		}
		
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_url_field($title, $url, $return = false)
	{
		if ($return)
			ob_start();
		?>
		<span><p><?=$title?></p><p class="font-unimportant"><?=$url?></p></span>
		<?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_page_link($controller, $subpage, $text, $button = false, $return = false)
	{
		if ($return)
			ob_start();
		$tab = false;
		if (strpos($subpage, '#') !== false)
		{
			$parts = explode('#', $subpage);
			$subpage = $parts[0];
			$tab = $parts[1];
		}
		?><a href="<?=$controller::get_link($subpage)?>" class="<?=$button ? 'button' : ''?> <?=!$subpage ? 'wsko-link' : ''?> wsko-load-lazy-page" data-controller="<?=$controller::get_link(false, true)?>" <?=$subpage ? 'data-subpage="'.$controller::get_link($subpage, true, false).'"' : ''?> <?=$tab ? 'data-subtab="'.$tab.'"' : ''?>><?=$text?></a><?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_lazy_field($var, $size, $align = false, $return = false)
	{
		if ($return)
			ob_start();
		?><div class="wsko-lazy-field" data-wsko-lazy-var="<?=$var?>">
			<?php WSKO_Class_Template::render_preloader(array('size' => $size) + ($align ? array('align' => $align) : array())); ?> 
		</div><?php
		if ($return)
			return ob_get_clean();
	}
	
	public static function render_feedback($return = false)
	{
		if ($return)
			ob_start();
		?>
			<div class="feedback-panel hidden-xs">
				<div class="row">
					<div class="col-sm-7">
						<p style="line-height: 24px;">How do you like BAVOKO SEO Tools?</p>
					</div>
					<div class="col-sm-5 align-right">
						<a class="wsko-give-feedback btn-flat btn-sm"><i class="fa fa-thumbs-up fa-fw"></i> Like</a>
						<a class="wsko-give-feedback btn-flat btn-sm"><i class="fa fa-thumbs-down fa-fw"></i> Dislike</a>
					</div>	
				</div>		
			</div>
		<?php
		
		if ($return)
			return ob_get_clean();
	}	
}
?>