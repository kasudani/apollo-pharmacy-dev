<?php

/*
Plugin Name: 	BAVOKO SEO Tools
Plugin URI: 	http://www.bavoko.tools/
Description: 	The Most Comprehensive All-in-One WordPress SEO Plugin!
Version: 		2.0.9
Author: 		BAVOKO Tools
Text Domain: 	wsko
Domain Path: 	/languages
Author URI: 	http://www.bavoko.tools/
License:     	GPL2 or later

@fs_premium_only /admin/templates/backlinks/, /admin/templates/search/page-competitors.php, /admin/templates/search/page-research.php, /admin/templates/search/view-keyword-research-report.php, /admin/templates/onpage/view-analysis-canon.php, /admin/templates/onpage/view-analysis-index.php, /admin/templates/onpage/view-analysis-resources.php, /admin/templates/onpage/view-analysis-robots.php, /admin/templates/settings/view-api-analytics.php, /admin/controller/controller.backlinks.php, /admin/controller/controller.performance.php, /classes/class.premium.php
*/
/*
BAVOKO SEO Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
BAVOKO SEO Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
*/
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
define( 'WSKO_VERSION', '2.0.9' );
function wsko_fs()
{
    global  $wsko_fs ;
    
    if ( !isset( $wsko_fs ) ) {
        require_once dirname( __FILE__ ) . '/includes/freemius/start.php';
        $wsko_fs = fs_dynamic_init( array(
            'id'             => '1608',
            'slug'           => 'wp-seo-keyword-optimizer',
            'type'           => 'plugin',
            'public_key'     => 'pk_44907806b1b63319b99263a8ca3a4',
            'is_premium'     => false,
            'has_addons'     => false,
            'has_paid_plans' => false,
            'menu'           => array(
            'slug'    => false,
            'contact' => false,
            'support' => false,
        ),
            'is_live'        => true,
        ) );
        //$wsko_fs->add_filter('connect_message_on_update', 'wsko_custom_connect_message_on_update', 10, 6);
        $wsko_fs->add_filter( 'permission_list', 'wsko_fre_permissions' );
    }
    
    return $wsko_fs;
}

function wsko_fre_permissions( $permissions )
{
    $permissions['newsletter'] = array(
        'icon-class' => 'dashicons dashicons-email-alt',
        'label'      => 'Newsletter',
        'desc'       => 'Updates, announcements, marketing, no spam',
        'priority'   => 25,
    );
    return $permissions;
}

/*function wsko_custom_connect_message_on_update($message, $user_first_name, $product_title, $user_login, $site_link, $freemius_link)
{
	return sprintf(
		__( 'Hey %1$s', 'my-text-domain' ) . ',<br>' .
		__( 'Please help us improve %2$s! If you opt-in, some data about your usage of %2$s will be sent to %5$s. If you skip this, that\'s okay! %2$s will still work just fine.', 'my-text-domain' ),
		$user_first_name,
		'<b>' . $product_title . '</b>',
		'<b>' . $user_login . '</b>',
		$site_link,
		$freemius_link
	);
}*/
wsko_fs();
do_action( 'wsko_fs_loaded' );
global  $wpdb, $wsko_plugin_path, $wsko_plugin_url ;
$wsko_plugin_path = dirname( __FILE__ ) . '/';
$wsko_plugin_url = plugins_url( '', __FILE__ ) . '/';
require_once $wsko_plugin_path . 'classes/class.core.php';
WSKO_Class_Core::init_classes();
function wsko_install_plugin()
{
    WSKO_Class_Core::init_classes( true );
    if ( WSKO_Class_Core::is_configured() ) {
        WSKO_Class_Crons::register_cronjobs();
    }
    WSKO_Class_Core::install();
    //update_option('wsko_do_activation_redirect', true);
}

register_activation_hook( __FILE__, 'wsko_install_plugin' );
function wsko_deinstall_plugin()
{
    WSKO_Class_Core::init_classes( true );
    WSKO_Class_Core::deinstall();
}

register_deactivation_hook( __FILE__, 'wsko_deinstall_plugin' );
function wsko_update_plugin( $upgrader_object, $options )
{
    WSKO_Class_Core::init_classes( true );
    $current_plugin_path_name = plugin_basename( __FILE__ );
    if ( $options['action'] == 'update' && $options['type'] == 'plugin' ) {
        if ( $options['plugins'] ) {
            foreach ( $options['plugins'] as $pl ) {
                
                if ( $pl == $current_plugin_path_name ) {
                    WSKO_Class_Core::update();
                    $wsko_data = WSKO_Class_Core::get_data();
                    if ( isset( $wsko_data['version_pre'] ) && $wsko_data['version_pre'] && version_compare( $wsko_data['version_pre'], "2.0", "<" ) ) {
                        update_option( 'wsko_do_activation_redirect', true );
                    }
                }
            
            }
        }
    }
}

add_action(
    'upgrader_process_complete',
    'wsko_update_plugin',
    10,
    2
);
function wsko_activation_redirect( $plugin )
{
    
    if ( get_option( 'wsko_do_activation_redirect' ) && !WSKO_Class_Core::get_option( 'activation_page_shown' ) ) {
        WSKO_Class_Core::save_option( 'activation_page_shown', true );
        delete_option( 'wsko_do_activation_redirect' );
        wp_redirect( admin_url( 'admin.php?page=wsko_updated' ) );
        exit;
    }

}

add_action( 'admin_init', 'wsko_activation_redirect' );