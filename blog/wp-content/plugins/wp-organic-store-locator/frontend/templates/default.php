<?php 
global $wpsl_settings, $wpsl;

$output         = $this->get_custom_css(); 
$autoload_class = ( !$wpsl_settings['autoload'] ) ? 'class="wpsl-not-loaded"' : '';

$output .= '<div id="wpsl-wrap">' . "\r\n";




$output .= '<div class="header_for_find_store"><div class="container"><div class="location-search-block"><div class="row">';
    $output .= '';

$output .= "\t" . '<div class="wpsl-search wpsl-clearfix ' . $this->get_css_classes() . '">' . "\r\n";
$output .= "\t\t" . '<div>' . "\r\n";


    $output .= "\t\t\t" . '<form autocomplete="off" class="contact-form">' . "\r\n";

    $output .= "\t\t\t" . '<div class="form-group has-feedback">' . "\r\n";

    $output .= '<div class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-3 col-xs-12">';
    $output .= "\t\t\t\t" . '<h6 class="find-store">' . esc_html( $wpsl->i18n->get_translation( 'search_label', __( 'Your location', 'wpsl' ) ) ) . '</h6>' . "\r\n";
    $output .= '</div>';


    $output .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><div class="form-group has-feedback">';

    $output .= "\t\t\t\t" . '<input id="wpsl-search-input" type="text" value="' . apply_filters( 'wpsl_search_input', '' ) . '" name="wpsl-search-input" placeholder="" aria-required="true" class="form-control" />' . "\r\n";

    $output .= '<i class="fa fa-crosshairs form-control-feedback"></i>';

    $output .= "\t\t\t" . '</div>' . "\r\n";

    $output .= '</div></div>';

            if ( $wpsl_settings['radius_dropdown'] || $wpsl_settings['results_dropdown']  ) {
                $output .= "\t\t\t" . '<div class="wpsl-select-wrap hidden-lg hidden-md hidden-sm hidden-xs">' . "\r\n";

                if ( $wpsl_settings['radius_dropdown'] ) {
                    $output .= "\t\t\t\t" . '<div id="wpsl-radius">' . "\r\n";
                    $output .= "\t\t\t\t\t" . '<label for="wpsl-radius-dropdown">' . esc_html( $wpsl->i18n->get_translation( 'radius_label', __( 'Search radius', 'wpsl' ) ) ) . '</label>' . "\r\n";
                    $output .= "\t\t\t\t\t" . '<select id="wpsl-radius-dropdown" class="wpsl-dropdown" name="wpsl-radius">' . "\r\n";
                    $output .= "\t\t\t\t\t\t" . $this->get_dropdown_list( 'search_radius' ) . "\r\n";
                    $output .= "\t\t\t\t\t" . '</select>' . "\r\n";
                    $output .= "\t\t\t\t" . '</div>' . "\r\n";
                }

                if ( $wpsl_settings['results_dropdown'] ) {
                    $output .= "\t\t\t\t" . '<div id="wpsl-results">' . "\r\n";
                    $output .= "\t\t\t\t\t" . '<label for="wpsl-results-dropdown">' . esc_html( $wpsl->i18n->get_translation( 'results_label', __( 'Results', 'wpsl' ) ) ) . '</label>' . "\r\n";
                    $output .= "\t\t\t\t\t" . '<select id="wpsl-results-dropdown" class="wpsl-dropdown" name="wpsl-results">' . "\r\n";
                    $output .= "\t\t\t\t\t\t" . $this->get_dropdown_list( 'max_results' ) . "\r\n";
                    $output .= "\t\t\t\t\t" . '</select>' . "\r\n";
                    $output .= "\t\t\t\t" . '</div>' . "\r\n";
                } 

                $output .= "\t\t\t" . '</div>' . "\r\n";
            }

            if ( $wpsl_settings['category_filter'] ) {
                $output .= $this->create_category_filter();
            }

    $output .= "\t\t\t\t" . '<div class="wpsl-search-btn-wrap"><input id="wpsl-search-btn" type="submit" value="' . esc_attr( $wpsl->i18n->get_translation( 'search_btn_label', __( 'View All Stores', 'wpsl' ) ) ) . '"></div>' . "\r\n";

    $output .= "\t\t" . '</form>' . "\r\n";



$output .= '</div></div></div></div>';




$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t" . '</div>' . "\r\n";




    
$output .= "\t" . '<div id="wpsl-gmap" class="wpsl-gmap-canvas"></div>' . "\r\n";

$output .= "\t" . '<div id="wpsl-result-list" class="scrollbar-style">' . "\r\n";


$output .= "\t\t" . '<div id="wpsl-stores" '. $autoload_class .'>' . "\r\n";
$output .= "\t\t\t" . '<ul></ul>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";

$output .= "\t\t" . '<div id="wpsl-direction-details">' . "\r\n";
$output .= "\t\t\t" . '<ul></ul>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";


$output .= "\t" . '</div>' . "\r\n";

if ( $wpsl_settings['show_credits'] ) { 
    $output .= "\t" . '<div class="wpsl-provided-by">'. sprintf( __( "Search provided by %sWP Store Locator%s", "wpsl" ), "<a target='_blank' href='https://wpstorelocator.co'>", "</a>" ) .'</div>' . "\r\n";
}

$output .= '</div>' . "\r\n";

return $output;